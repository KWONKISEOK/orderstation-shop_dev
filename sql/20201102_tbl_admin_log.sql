ALTER TABLE `tbl_admin_log_2020`
	ADD COLUMN `al_depth1` VARCHAR(50) NULL DEFAULT '' COMMENT '1뎁스 메뉴' AFTER `al_url`,
	ADD COLUMN `al_depth2` VARCHAR(50) NULL DEFAULT '' COMMENT '2뎁스 메뉴' AFTER `al_depth1`,
	CHANGE COLUMN `al_subject` `al_subject` VARCHAR(50) NULL DEFAULT '' COMMENT '작업내역 명칭' COLLATE 'utf8_general_ci' AFTER `al_depth2`,
	CHANGE COLUMN `al_content` `al_content` VARCHAR(50) NULL DEFAULT '' COMMENT '설명' COLLATE 'utf8_general_ci' AFTER `al_subject`;


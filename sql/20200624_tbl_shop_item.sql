ALTER TABLE `tbl_shop_item`
	CHANGE COLUMN `it_supply_price` `it_supply_price` INT(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT '매입가' AFTER `it_trans_bundle`,
	CHANGE COLUMN `it_supply_ori_price` `it_supply_ori_price` INT(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT '매입가 ori' AFTER `it_supply_price`,
	CHANGE COLUMN `it_supply_temp_price` `it_supply_temp_price` INT(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT '매입가 temp' AFTER `it_supply_ori_price`,
	CHANGE COLUMN `it_drug_price` `it_drug_price` INT(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT '약국공급가' AFTER `it_dodome_price`,
	ADD COLUMN `it_drug_ori_price` INT(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT '약국공급가 ori' AFTER `it_drug_price`,
	CHANGE COLUMN `it_incen` `it_incen` INT(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT '약국마진' AFTER `it_drug_ori_price`;

ALTER TABLE `tbl_shop_item`
    ADD COLUMN `it_period_min` INT(10) NULL DEFAULT '2' COMMENT '배송횟수_최솟값' AFTER `it_period_week`,
	ADD COLUMN `it_period_max` INT(10) NULL DEFAULT '5' COMMENT '배송횟수_최댓값' AFTER `it_period_min`;
ALTER TABLE `tbl_price_change_history`
	CHANGE COLUMN `before_it_drug_price` `before_it_drug_price` INT(10) NOT NULL DEFAULT '0' COMMENT '변경전 약국공급가' AFTER `before_it_price`,
	ADD COLUMN `before_it_drug_ori_price` INT(10) NOT NULL DEFAULT '0' COMMENT '변경전 약국공급가(회색)' AFTER `before_it_drug_price`,
	CHANGE COLUMN `before_it_incen` `before_it_incen` INT(10) NOT NULL DEFAULT '0' COMMENT '변경전 약국마진' AFTER `before_it_drug_ori_price`,
	CHANGE COLUMN `before_it_supply_price` `before_it_supply_price` INT(10) NOT NULL DEFAULT '0' COMMENT '변경전 매입가' AFTER `before_it_dodome_price`,
	CHANGE COLUMN `after_it_drug_price` `after_it_drug_price` INT(10) NOT NULL DEFAULT '0' COMMENT '변경후 약국공급가' AFTER `after_it_price`,
	ADD COLUMN `after_it_drug_ori_price` INT(10) NOT NULL DEFAULT '0' COMMENT '변경후 약국공급가(회색)' AFTER `after_it_drug_price`,
	CHANGE COLUMN `after_it_incen` `after_it_incen` INT(10) NOT NULL DEFAULT '0' COMMENT '변경후 약국마진' AFTER `after_it_drug_ori_price`,
	CHANGE COLUMN `after_it_supply_price` `after_it_supply_price` INT(10) NOT NULL DEFAULT '0' COMMENT '변경후 매입가' AFTER `after_it_dodome_price`;
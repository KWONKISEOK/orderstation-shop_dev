CREATE TABLE `tbl_shop_order_address_company` (
      `ad_id` INT(11) NOT NULL AUTO_INCREMENT COMMENT '배송지번호',
      `it_company` INT(11) NOT NULL COMMENT '사내수령지',
      `ad_subject` VARCHAR(50) NOT NULL COMMENT '배송지명' COLLATE 'utf8_general_ci',
      `ad_name` VARCHAR(50) NOT NULL COMMENT '수령인이름' COLLATE 'utf8_general_ci',
      `ad_tel` VARCHAR(50) NOT NULL DEFAULT '0' COMMENT '수령인전화' COLLATE 'utf8_general_ci',
      `ad_hp` VARCHAR(50) NOT NULL DEFAULT '0' COMMENT '수령인휴대폰' COLLATE 'utf8_general_ci',
      `ad_zip1` VARCHAR(3) NOT NULL COMMENT '배송지 우편번호 앞자리' COLLATE 'utf8_general_ci',
      `ad_zip2` VARCHAR(3) NOT NULL COMMENT '배송지 우편번호 뒷자리' COLLATE 'utf8_general_ci',
      `ad_addr1` VARCHAR(255) NOT NULL COMMENT '배송지 기본주소' COLLATE 'utf8_general_ci',
      `ad_addr2` VARCHAR(255) NOT NULL COMMENT '배송지 상세주소' COLLATE 'utf8_general_ci',
      `ad_addr3` VARCHAR(255) NOT NULL COMMENT '배송지 주소참고항목' COLLATE 'utf8_general_ci',
      `ad_jibeon` VARCHAR(255) NOT NULL COMMENT '배송지 지번주소' COLLATE 'utf8_general_ci',
      `ad_trans_memo` VARCHAR(255) NULL DEFAULT NULL COMMENT '배송메시지' COLLATE 'utf8_general_ci',
      PRIMARY KEY (`ad_id`) USING BTREE,
      INDEX `it_company` (`it_company`) USING BTREE
)
    COMMENT='사내수령지'
COLLATE='utf8_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=4
;

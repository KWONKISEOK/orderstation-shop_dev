CREATE TABLE `tbl_cash` (
                            `ca_id` INT(11) NOT NULL AUTO_INCREMENT COMMENT '적립금번호',
                            `mb_id` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '회원아이디' COLLATE 'utf8_general_ci',
                            `ca_datetime` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '지급일시',
                            `ca_content` VARCHAR(255) NOT NULL DEFAULT '' COMMENT '적립금내역' COLLATE 'utf8_general_ci',
                            `ca_cash` INT(11) NOT NULL DEFAULT '0' COMMENT '지급적립금',
                            `ca_use_cash` INT(11) NOT NULL DEFAULT '0' COMMENT '사용적립금',
                            `ca_expired` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '적립금만료',
                            `ca_expire_date` DATE NOT NULL DEFAULT '0000-00-00' COMMENT '적립금 만료일',
                            `ca_mb_cash` INT(11) NOT NULL DEFAULT '0' COMMENT '지급당시 회원적립금',
                            `ca_rel_table` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '관련 테이블' COLLATE 'utf8_general_ci',
                            `ca_rel_id` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '관련 아이디' COLLATE 'utf8_general_ci',
                            `ca_rel_action` VARCHAR(255) NOT NULL DEFAULT '' COMMENT '관련 작업' COLLATE 'utf8_general_ci',
                            `ca_rel_po_id` VARCHAR(10) NOT NULL DEFAULT '' COMMENT '관련 적립금 번호' COLLATE 'utf8_general_ci',
                            `ca_rel_od_id` VARCHAR(25) NOT NULL DEFAULT '' COMMENT '관련 주문 번호' COLLATE 'utf8_general_ci',
                            PRIMARY KEY (`ca_id`) USING BTREE,
                            INDEX `index1` (`mb_id`, `ca_rel_table`, `ca_rel_id`, `ca_rel_action`) USING BTREE,
                            INDEX `index2` (`ca_expire_date`) USING BTREE
)
    COMMENT='적립금관리'
COLLATE='utf8_general_ci'
ENGINE=InnoDB
ROW_FORMAT=COMPACT
AUTO_INCREMENT=1433
;
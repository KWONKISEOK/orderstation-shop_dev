ALTER TABLE `tbl_shop_order`
    ADD COLUMN `od_company_yn` VARCHAR(1) NULL DEFAULT 'N' COMMENT '사내배송여부' AFTER `od_multi_yn`;

ALTER TABLE `tbl_shop_order`
    ADD COLUMN `od_company_send` INT(11) NOT NULL DEFAULT '0' COMMENT '사내수령배송비할인' AFTER `od_send_coupon`;
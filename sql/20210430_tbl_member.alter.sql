ALTER TABLE `tbl_member`
    ADD COLUMN `mb_grade` TINYINT(4) NOT NULL DEFAULT '1' COMMENT '회원등급' AFTER `mb_level`,
    ADD COLUMN `mb_cash` INT(11) NOT NULL DEFAULT '0' COMMENT '적립금' AFTER `mb_point`,
    ADD COLUMN `mb_extra` INT(2) NOT NULL DEFAULT '0' COMMENT '이관회원구분(10:일반,11:HC)' AFTER `mb_referee`;
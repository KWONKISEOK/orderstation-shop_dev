CREATE TABLE `tbl_shop_item_auth` (
	`it_id` VARCHAR(50) NULL COMMENT '상품코드' COLLATE 'utf8_general_ci',
	`mb_id` VARCHAR(50) NULL COMMENT '회원아이디' COLLATE 'utf8_general_ci'
)
COMMENT='특정 상품 회원별 접근권한 부여 테이블'
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;

ALTER TABLE `item_excel_fileinfo`
	ADD COLUMN `f_gubun` CHAR(4) NOT NULL DEFAULT 'w' COMMENT '타입 구분 w 등록, u 수정' AFTER `f_name`,
	CHANGE COLUMN `f_memo` `f_memo` VARCHAR(2000) NOT NULL COMMENT '메모사항' COLLATE 'utf8_general_ci' AFTER `f_gubun`;

ALTER TABLE `tbl_shop_item_excel`
	ADD COLUMN `it_id` VARCHAR(20) NULL DEFAULT '' COMMENT '상품 ID' AFTER `f_idx`,
	CHANGE COLUMN `ca_id2` `ca_id2` VARCHAR(255) NOT NULL DEFAULT '' COMMENT '카테고리2' COLLATE 'utf8_general_ci' AFTER `ca_id`;
ALTER TABLE `tbl_shop_item_excel`
	ADD INDEX `it_id` (`it_id`);
ALTER TABLE `tbl_member`
	ADD COLUMN `comp_name_mod` VARCHAR(255) NOT NULL DEFAULT '' COMMENT '공급사명 승인요청' AFTER `comp_name`,
	ADD COLUMN `comp_brand_mod` VARCHAR(255) NOT NULL DEFAULT '' COMMENT '브랜드 승인요청' AFTER `comp_brand`,
	ADD COLUMN `comp_limit_mod` VARCHAR(255) NOT NULL DEFAULT '' COMMENT '최소주문금액 승인요청' AFTER `comp_limit`,
	ADD COLUMN `comp_tel_mod` VARCHAR(255) NOT NULL DEFAULT '' COMMENT '전화 승인요청' AFTER `comp_tel`,
	ADD COLUMN `comp_ceo_name_mod` VARCHAR(255) NOT NULL DEFAULT '' COMMENT '대표자명 승인요청' AFTER `comp_ceo_name`,
	ADD COLUMN `comp_md_name_mod` VARCHAR(255) NOT NULL DEFAULT '' COMMENT '담당자명 승인요청' AFTER `comp_md_name`,
	ADD COLUMN `comp_md_tel_mod` VARCHAR(255) NOT NULL DEFAULT '' COMMENT '담당자전화 승인요청' AFTER `comp_md_tel`,
	ADD COLUMN `comp_order_time_mod` VARCHAR(255) NOT NULL DEFAULT '' COMMENT '주문시간 승인요청' AFTER `comp_order_time`,
	ADD COLUMN `comp_trans_time_mod` VARCHAR(255) NOT NULL DEFAULT '' COMMENT '배송시간 승인요청' AFTER `comp_trans_time`,
	ADD COLUMN `comp_change_status` VARCHAR(1) NOT NULL DEFAULT '0' COMMENT '공급사정보수정 승인여부(0:미승인,1:승인)' AFTER `od_billkey`,
	ADD COLUMN `comp_charge_datetime_1` DATETIME NULL COMMENT '공급사정보수정 승인요청날짜' AFTER `od_billkey`,
	ADD COLUMN `comp_charge_datetime_2` DATETIME NULL COMMENT '공급사정보수정 승인완료날짜' AFTER `comp_charge_datetime_1`;
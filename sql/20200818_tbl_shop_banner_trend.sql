CREATE TABLE `tbl_shop_banner_trend` (
	`bn_id` INT(11) NOT NULL AUTO_INCREMENT COMMENT '배너번호',
	`bn_event_id` VARCHAR(50) NOT NULL DEFAULT '' COMMENT '이벤트 ID' COLLATE 'utf8_general_ci',
	`bn_event_name` VARCHAR(50) NOT NULL DEFAULT '' COMMENT '이벤트 설명' COLLATE 'utf8_general_ci',
	`bn_tabimg` VARCHAR(50) NOT NULL DEFAULT '' COMMENT '링크' COLLATE 'utf8_general_ci',
	`bn_pcimg` VARCHAR(50) NOT NULL DEFAULT '' COMMENT 'PC이미지' COLLATE 'utf8_general_ci',
	`bn_mimg` VARCHAR(50) NOT NULL DEFAULT '' COMMENT '모바일이미지' COLLATE 'utf8_general_ci',
	`bn_hit` INT(11) NOT NULL DEFAULT '0' COMMENT '조회수',
	`bn_order` INT(11) NOT NULL DEFAULT '0' COMMENT '출력순서',
	`bn_use` CHAR(1) NOT NULL DEFAULT 'Y' COMMENT '사용유무 Y N' COLLATE 'utf8_general_ci',
	`bn_wdate` DATETIME NULL DEFAULT '0000-00-00 00:00:00' COMMENT '등록일시',
	`bn_udate` DATETIME NULL DEFAULT '0000-00-00 00:00:00' COMMENT '수정일시',
	PRIMARY KEY (`bn_id`) USING BTREE,
	INDEX `bn_event_id` (`bn_event_id`) USING BTREE
)
COMMENT='요즘은 이런게 대세 관리'
COLLATE='utf8_general_ci'
ENGINE=InnoDB
ROW_FORMAT=COMPACT
;

ALTER TABLE `tbl_shop_item`
	ADD COLUMN `it_explan_mod` MEDIUMTEXT NULL COMMENT '수정요청상품설명' AFTER `it_img10_mod`,
	ADD COLUMN `it_mobile_explan_mod` MEDIUMTEXT NULL COMMENT '수정요청모바일상품설명' AFTER `it_explan_mod`;
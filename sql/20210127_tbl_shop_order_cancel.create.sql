CREATE TABLE `tbl_shop_order_cancel` (
  `od_id` BIGINT(20) NOT NULL COMMENT '주문번호',
  `od_receipt_price` INT(11) NOT NULL DEFAULT '0' COMMENT '결제금액',
  `od_send_cost` INT(11) NOT NULL DEFAULT '0',
  `od_send_cost2` INT(11) NOT NULL DEFAULT '0',
  `od_send_coupon` INT(11) NOT NULL DEFAULT '0',
  `od_receipt_point` INT(11) NOT NULL DEFAULT '0',
  `od_coupon` INT(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`od_id`) USING BTREE
)
  COMMENT='주문 취소 데이터 백업'
  COLLATE='utf8_general_ci'
  ENGINE=InnoDB
;

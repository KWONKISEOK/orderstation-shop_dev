DELIMITER $$
CREATE DEFINER=`onkdbuser`@`%` PROCEDURE `SP_P_22`(
	IN `GetYear` VARCHAR(4)
)
LANGUAGE SQL
NOT DETERMINISTIC
CONTAINS SQL
SQL SECURITY DEFINER
COMMENT 'ERP 정기주문 매출현황'
BEGIN

	 select
	  a.view_month,
	  ifnull(b.tot_price_1, 0) as tot_price_1, /* OS ERP 정산 금액*/
	  ifnull(b.tot_price_2 + b.tot_price_2_1, 0) as tot_price_2,
	  ifnull(b.price_avg, 0) as price_avg,
	  ifnull(b.tot_cnt_1, 0) as tot_cnt_1,
	  ifnull(b.tot_cnt_2, 0) as tot_cnt_2,
	  ifnull(b.cnt_avg, 0) as cnt_avg,
	  ifnull(b.tot_price_3, 0) as tot_price_3,
	  ifnull(b.tot_cnt_3, 0) as tot_cnt_3,
	  ifnull(b.is_tot_price_1, 0) as is_tot_price_1,
	  ifnull(b.is_tot_price_2, 0) as is_tot_price_2,
	  ifnull(b.is_tot_price_3, 0) as is_tot_price_3,
	  ifnull(b.calculate_price, 0) as calculate_price,
	  ifnull(b.no_calculate_price, 0) as no_calculate_price
	from
	  (
		select '1' as view_month union all
		select '2' as view_month union all
		select '3' as view_month union all
		select '4' as view_month union all
		select '5' as view_month union all
		select '6' as view_month union all
		select '7' as view_month union all
		select '8' as view_month union all
		select '9' as view_month union all
		select '10' as view_month union all
		select '11' as view_month union all
		select '12' as view_month
	  ) as a
	  left join(
		select
		  y.od_date1_month,
		  y.tot_price_1,
		  y.tot_price_2,
		  round(((y.tot_price_2 + y.tot_price_2_1) / y.tot_price_1) * 100, 2) as price_avg,
		  y.tot_price_2_1,
		  y.tot_cnt_1,
		  y.tot_cnt_2,
		  round((y.tot_cnt_2 / y.tot_cnt_1) * 100, 2) as cnt_avg,
		  round(y.tot_price_1 / y.tot_cnt_1) as tot_price_3,
		  round(y.tot_price_2 / y.tot_cnt_2) as tot_cnt_3,
		  y.is_tot_price_1,
		  y.is_tot_price_2,
		  y.is_tot_price_3,
		  y.calculate_price,
		  y.no_calculate_price
		from
		  (
			select
			  x.od_date1_month,
			  sum( case when x.service_type is null then x.total_price else 0 end) as tot_price_1 , /* os 전체 매출 */
			  sum( case when x.service_type is null and x.mb_referee = '5' then x.total_price else 0 end ) as tot_price_2 , /*얼라이언스 약국 매출*/
			  sum( case when x.service_type is null and x.mb_type='0' and x.pharm_custno in (select pharm_custno from tbl_member where mb_type='1' and mb_referee='5') then x.total_price else 0 end ) as tot_price_2_1,
			  sum( case when x.service_type is null and x.od_id != '' then 1 else 0 end ) as tot_cnt_1 , /* os 전체 건수 */
			  sum( case when x.service_type is null and x.mb_referee = '5' then 1 else 0 end ) as tot_cnt_2, /* 얼라이언스 약국 건수 */
			  sum( case when x.service_type ='B' then x.total_price else 0 end) as is_tot_price_1, /*이즈브레 전체매출*/
			  sum( case when x.service_type ='B' and x.mb_recommend in (select mb_id from tbl_member where mb_type='1' and mb_referee='5') then x.total_price else 0 end) as is_tot_price_2, /*이즈브레 얼라 매출*/
			  sum( case when x.service_type ='B' and x.mb_recommend ='' then x.total_price else 0 end) as is_tot_price_3, /*이즈브레 가입고객매출*/
			  sum( x.calculate_price) AS calculate_price,
			  sum( x.no_calculate_price) AS no_calculate_price
			from
			  (
				SELECT
				  od_id,
				  SUM(total_price) AS total_price,
				  SUM(calculate_price) AS calculate_price,
				  SUM(no_calculate_price) AS no_calculate_price,
				  MAX(od_date1_month) AS od_date1_month,
				  MAX(mb_referee) AS mb_referee,
				  mb_recommend,
				  service_type,
				  mb_type,
				  pharm_custno
				FROM
				  (
					select
					  a.od_id,
					  b.it_id,
					  ((b.od_total_supply_price / od_period_cnt) + b.od_send_cost) as total_price, /* 정기주문 매출 확인위한 조건 처리 */
					  case when c.od_date3 IS NOT null then (b.od_total_supply_price / od_period_cnt) + b.od_send_cost ELSE 0 END as calculate_price, /* 정기주문 매출 확인위한 조건 처리 */
					  case when c.od_date3 IS null then (b.od_total_supply_price / od_period_cnt) + b.od_send_cost ELSE 0 END as no_calculate_price, /* 정기주문 매출 확인위한 조건 처리 */
					  month(c.od_date1) as od_date1_month,
					  e.mb_referee,
					  e.mb_recommend,
					  b.service_type as service_type,
					  e.mb_type,
					  a.pharm_custno
					from
					  tbl_shop_order a
					  inner join tbl_shop_order_detail b on b.od_id = a.od_id
					  inner join tbl_shop_order_receiver c on c.od_id = b.od_id and c.od_num = b.od_num
					  inner join tbl_shop_item d on d.it_id = b.it_id
					  left join tbl_member e on a.mb_id = e.mb_id
					where
					  DATE(c.od_date1) BETWEEN CONCAT(GetYear, '-01-01') AND concat(GetYear, '-12-31')
					  and c.od_status != '취소'
					  and instr(c.od_status, '반품') <= 0
					  and not (a.od_settle_case = '가상계좌' and c.od_pay_yn = 'N')
					  /* 달콩 행복set 제외하기 */
					  and b.it_id not in('S00603', 'S00604')

					  /* 정기주문 조건 걸기 */
					  AND b.od_period_yn = 'Y'
					  AND b.service_type IS NULL
				  ) AS Temp
				GROUP BY
				  od_id
			  ) as x
			group by
			  od_date1_month
		  ) as y
		) as b on a.view_month = b.od_date1_month;

END $$
DELIMITER ;
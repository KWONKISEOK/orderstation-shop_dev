CREATE TABLE `tbl_stplat`
(
    `st_id`      TINYINT(8) NOT NULL AUTO_INCREMENT,
    `st_title`   VARCHAR(50) NOT NULL COMMENT '약관명' COLLATE 'utf8_general_ci',
    `st_content` TEXT        NOT NULL COMMENT '약관내용' COLLATE 'utf8_general_ci',
    `st_date`    DATETIME    NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '등록일',
    PRIMARY KEY (`st_id`) USING BTREE
) COMMENT='약관 관리'
COLLATE='utf8_general_ci'
ENGINE=InnoDB;

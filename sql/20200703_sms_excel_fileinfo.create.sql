CREATE TABLE `sms_excel_fileinfo` (
	`f_idx` INT(11) NOT NULL AUTO_INCREMENT COMMENT '고유값',
	`f_status` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '1 완료 0 대기',
	`f_name` VARCHAR(100) NOT NULL DEFAULT '' COMMENT '업로드 파일이름' COLLATE 'utf8_general_ci',
	`f_content` TEXT NULL COMMENT '문자 내용' COLLATE 'utf8_general_ci',
	`f_regid` VARCHAR(50) NOT NULL DEFAULT '' COMMENT '업로드 아이디' COLLATE 'utf8_general_ci',
	`f_regip` VARCHAR(50) NOT NULL DEFAULT '' COMMENT '업로드 아이피' COLLATE 'utf8_general_ci',
	`f_regdate` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '등록일',
	PRIMARY KEY (`f_idx`) USING BTREE
)
COMMENT='문자발송 엑셀업로드'
COLLATE='utf8_general_ci'
ENGINE=InnoDB
ROW_FORMAT=COMPACT
;

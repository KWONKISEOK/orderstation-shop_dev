CREATE TABLE `tbl_member_promotion` (
	`idx` INT(11) NOT NULL AUTO_INCREMENT,
	`mb_id` VARCHAR(20) NULL COMMENT '회원 ID' COLLATE 'utf8_general_ci',
	PRIMARY KEY (`idx`) USING BTREE,
	INDEX `mb_id` (`mb_id`) USING BTREE
)
COMMENT='프로모션 회원 관리'
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;

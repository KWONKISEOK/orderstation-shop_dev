CREATE TABLE `tbl_charge_info_mod` (
	`Charge_Idx` INT(11) NOT NULL AUTO_INCREMENT,
	`mb_id` VARCHAR(20) NOT NULL COMMENT '공급사아이디' COLLATE 'utf8_general_ci',
	`Charge_Task` VARCHAR(50) NULL DEFAULT NULL COMMENT '업무' COLLATE 'utf8_general_ci',
	`Charge_Name` VARCHAR(50) NULL DEFAULT NULL COMMENT '담당자명' COLLATE 'utf8_general_ci',
	`Charge_Bigo` VARCHAR(100) NULL DEFAULT NULL COMMENT '비고' COLLATE 'utf8_general_ci',
	`Charge_Tel` VARCHAR(50) NULL DEFAULT NULL COMMENT '담당자명' COLLATE 'utf8_general_ci',
	`Charge_Hp` VARCHAR(50) NULL DEFAULT NULL COMMENT '담당자명' COLLATE 'utf8_general_ci',
	`Charge_Email` VARCHAR(50) NULL DEFAULT NULL COMMENT '담당자명' COLLATE 'utf8_general_ci',
	`Charge_RegDate` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '등록일',
	`DelYn` VARCHAR(1) NOT NULL COMMENT '삭제여부' COLLATE 'utf8_general_ci',
	`Charge_DelDate` DATETIME NULL DEFAULT NULL COMMENT '삭제일',
	PRIMARY KEY (`Charge_Idx`) USING BTREE
)
COMMENT='공급사관리 담당자정보 승인요청'
COLLATE='utf8_unicode_ci'
ENGINE=InnoDB
;

ALTER TABLE `tbl_shop_order`
	ADD COLUMN `od_bank_date` DATE NOT NULL DEFAULT 0 COMMENT '입금기한' AFTER `od_bank_account`;

ALTER TABLE `tbl_shop_order`
	CHANGE COLUMN `od_bank_date` `od_bank_date` DATETIME NOT NULL DEFAULT '0000-00-00' COMMENT '입금기한' AFTER `od_bank_account`;
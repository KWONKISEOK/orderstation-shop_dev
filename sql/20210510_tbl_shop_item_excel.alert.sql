ALTER TABLE `tbl_shop_item_excel`
    ADD COLUMN `it_multisend` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '다중배송' AFTER `it_use`,
	ADD COLUMN `it_cash_type` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '적립금지급' AFTER `it_multisend`;
BEGIN


	 DECLARE GetDate_3 VARCHAR(10); /* 전주 시작날짜 */
	 DECLARE GetDate_4 VARCHAR(10); /* 전주 끝날짜 */
	 DECLARE GetDate_5 VARCHAR(10); /* 전년 시작날짜 */
	 DECLARE GetDate_6 VARCHAR(10); /* 전년 끝날짜 */

	 set GetDate_3 = (select date_add(GetDate_1, interval -(GetDateDiff+1) day));
	 set GetDate_4 = (select date_add(GetDate_2, interval -(GetDateDiff+1) day));
	 set GetDate_5 = (select date_add(GetDate_1, interval -365 day));
	 set GetDate_6 = (select date_add(GetDate_2, interval -365 day));

	 /*
	 os 브랜드관 - > 60
	 라이프매니지 -> 30
	 뷰티매니지 -> 40
	 푸드매니지 -> 10
	 홈매니지 -> 20
	 od_date1 -> 주문일
	 */
	 select
		 sum( case when DATE(x.od_date1) BETWEEN GetDate_1 AND GetDate_2 then total_price else 0 end ) as total_price_1 ,
		 sum( case when DATE(x.od_date1) BETWEEN GetDate_3 AND GetDate_4 then total_price else 0 end ) as total_price_2 ,
		 sum( case when DATE(x.od_date1) BETWEEN GetDate_5 AND GetDate_6 then total_price else 0 end ) as total_price_3 ,
		 sum( case when DATE(x.od_date1) BETWEEN GetDate_1 AND GetDate_2 then 1 else 0 end ) as total_cnt_1 ,
		 sum( case when DATE(x.od_date1) BETWEEN GetDate_3 AND GetDate_4 then 1 else 0 end ) as total_cnt_2 ,
		 sum( case when DATE(x.od_date1) BETWEEN GetDate_5 AND GetDate_6 then 1 else 0 end ) as total_cnt_3
	 from(

		/* 상품건수 대신 주문건수로 구해주기 위해서... it_id 대신에 od_id 를 카운트 해준다.   */
		SELECT od_id , SUM(total_price) AS total_price , MAX(od_date1) AS od_date1 FROM(

			 select a.od_id ,
		 		case when od_period_yn = 'Y' then  b.od_total_drug_price / od_period_cnt else b.od_total_drug_price end as total_price ,
				c.od_date1
			 from tbl_shop_order a inner join tbl_shop_order_detail b on b.od_id = a.od_id
			 							  inner join tbl_shop_order_receiver c on c.od_id = b.od_id and c.od_num = b.od_num
			 							  inner join tbl_shop_item d on d.it_id = b.it_id
			 where DATE(c.od_date1) BETWEEN GetDate_5 AND GetDate_2 and  c.od_status != '취소' and instr( c.od_status , '반품' ) <= 0
			 /*and left( d.ca_id , 2 ) in('10','20','30','40','60','70')*/
			 and not ( a.od_settle_case = '가상계좌' and c.od_pay_yn = 'N' ) /* 입금확인중 안나오게 */
			 /*이즈브레 앱 주문 제외*/
		 	 and b.service_type is null
			 /* 달콩 행복set 제외하기 */
			 and b.it_id not in('S00603','S00604')

		) AS Temp GROUP BY od_id

	 ) as x ;


END
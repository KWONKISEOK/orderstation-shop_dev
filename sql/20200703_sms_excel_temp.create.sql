CREATE TABLE `sms_excel_temp` (
	`idx` INT(11) NOT NULL AUTO_INCREMENT COMMENT '테이블 고유값',
	`f_idx` VARCHAR(50) NOT NULL DEFAULT '' COMMENT '고유값' COLLATE 'utf8_general_ci',
	`mb_id` VARCHAR(100) NOT NULL DEFAULT '' COMMENT '아이디' COLLATE 'utf8_general_ci',
	`mb_name` VARCHAR(2000) NOT NULL DEFAULT '' COMMENT '이름' COLLATE 'utf8_general_ci',
	`mb_sex` VARCHAR(50) NOT NULL DEFAULT '' COMMENT '성별' COLLATE 'utf8_general_ci',
	`mb_birth` VARCHAR(50) NOT NULL DEFAULT '' COMMENT '생년월일' COLLATE 'utf8_general_ci',
	`buy_ct` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '구매유무',
	`mb_hp` VARCHAR(50) NOT NULL DEFAULT '' COMMENT '휴대폰' COLLATE 'utf8_general_ci',
	`f_regdate` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '등록일',
	PRIMARY KEY (`idx`) USING BTREE
)
COMMENT='문자발송 엑셀업로드'
COLLATE='utf8_general_ci'
ENGINE=InnoDB
ROW_FORMAT=COMPACT
;

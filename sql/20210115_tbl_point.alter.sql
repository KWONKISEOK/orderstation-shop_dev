ALTER TABLE `tbl_point`
  ADD COLUMN `po_rel_pid` VARCHAR(10) NOT NULL DEFAULT '' COMMENT '관련 포인트 번호' AFTER `po_rel_action`;

ALTER TABLE `tbl_point`
  CHANGE COLUMN `po_rel_pid` `po_rel_po_id` VARCHAR(10) NOT NULL DEFAULT '' COMMENT '관련 포인트 번호' COLLATE 'utf8_general_ci' AFTER `po_rel_action`,
  ADD COLUMN `po_rel_od_id` VARCHAR(25) NOT NULL DEFAULT '' COMMENT '관련 주문 번호' AFTER `po_rel_po_id`;
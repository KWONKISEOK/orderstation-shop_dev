<?php

# 변수에 한글이 포함될 경우 현재 서버의 locale을 확인하여 변경해주도록 한다.
$locale = shell_exec('locale -a | grep ko_KR');
if (strpos($locale, 'ko_KR') !== false) {
    putenv("LANG=ko_KR.UTF-8");
    setlocale(LC_ALL, 'ko_KR.utf8');
}

$item = $_POST['keyword'];
$sort = $_POST['sort'];

$list = array();

if (!empty($item)) {
    $result = json_decode(exec('python3 naver_crawling.py '.$item.' '.$sort), true);

    $product_list = $result['product_list'];
    $mall_name = $result['mall_name'];
    $product_count_info = $result['product_count_info'];
    $product_price = $result['product_price'];
    $delivery_price = $result['delivery_price'];
    $review_cnt = $result['review_cnt'];
    $purchaseCnt = $result['purchaseCnt'];
    $openDate = $result['openDate'];

    for ($i = 0; $i < count($product_list); $i++) {
        $list[$i]['product_list'] = $product_list[$i];
        $list[$i]['mall_name'] = $mall_name[$i];
        if (strpos($product_count_info[$i], '|') !== false) {
            $temp_str = explode('|', $product_count_info[$i]);
            $list[$i]['product_count_info'] = $temp_str;
        } else {
            $list[$i]['product_count_info'] = $product_count_info[$i];
        }
        $list[$i]['product_price'] = $product_price[$i];
        $list[$i]['delivery_price'] = $delivery_price[$i];
        $list[$i]['review_cnt'] = $review_cnt[$i];
        $list[$i]['purchaseCnt'] = $purchaseCnt[$i];
        $list[$i]['openDate'] = $openDate[$i];
    }
}

$ExcelTitle = urlencode( $item ."_". date( "YmdHis" ) );

header( "Content-type: application/vnd.ms-excel" );
header( "Content-type: application/vnd.ms-excel; charset=utf-8" );
header( "Content-Disposition: attachment; filename = " . $ExcelTitle . ".xls" );
header( "Content-Description: PHP4 Generated Data" );
header('Set-Cookie: fileDownload=true; path=/'); # 추가해야 해당 파일이 열려있는 것을 인식하여 콜백을 제대로 받을 수 있음
print( "<meta http-equiv=\"Content-Type\" content=\"application/vnd.ms-excel; charset=utf-8\">" );

?>

<table class="table">
    <thead>
    <tr style="background: #f3f3f3">
        <th>상품명</th>
        <th>판매처</th>
        <th>상품 정보</th>
        <th width="100">상품 가격</th>
        <th width="120">배송가격 정보</th>
        <th width="80">리뷰</th>
        <th width="80">구매건수</th>
        <th width="80">등록일</th>
    </tr>
    </thead>
    <tbody id="lists">
    <?php
    // HTML TABLE CREATE
    if (count($list) > 0) {
        foreach ($list as $k => $v) {
            echo '<tr>';
            echo '<td>'.$v['product_list'].'</td>';
            echo '<td>'.$v['mall_name'].'</td>';
            if (is_array($v['product_count_info'])) {
                echo '<td>';
                foreach ($v['product_count_info'] as $txt) {
                    echo '<button class="btn btn-primary btn-xs" style="margin-top: 5px;">'.$txt.'</button>&nbsp;';
                }
                echo '</td>';
            } else if (!empty($v['product_count_info'])) {
                echo '<td><button class="btn btn-primary btn-xs">'.$v['product_count_info'].'</button></td>';
            } else {
                echo '<td> - </td>';
            }
            echo '<td>'.number_format($v['product_price']).'</td>';
            echo '<td>'.number_format($v['delivery_price']).'</td>';
            echo '<td>'.number_format($v['review_cnt']).'</td>';
            echo '<td>'.number_format($v['purchaseCnt']).'</td>';
            echo '<td>'.date('Y.m', strtotime($v['openDate'])).'</td>';
            echo '</tr>';
        }
    } else {
        echo '<tr><td colspan="8">검색 결과가 없습니다.</td></tr>';
    }
    ?>
    </tbody>
</table>




<?php
include_once(PYTHON_ADM_PATH.'/config.php');
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo PYTHON_ADM_URL; ?>//plugins/images/favicon.png">
    <title>상품 모니터링 - O&K</title>
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo PYTHON_ADM_URL; ?>/common/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Menu CSS -->
    <link href="<?php echo PYTHON_ADM_URL; ?>/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
    <!-- animation CSS -->
    <link href="<?php echo PYTHON_ADM_URL; ?>/common/css/animate.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo PYTHON_ADM_URL; ?>/common/css/style.css?ver=20201207" rel="stylesheet">
    <!-- color CSS -->
    <link href="<?php echo PYTHON_ADM_URL; ?>/common/css/colors/default.css" id="theme" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- jQuery -->
    <script src="/python-admin/plugins/bower_components/jquery/dist/jquery.min.js"></script>
</head>

<body class="fix-header">
<!-- ============================================================== -->
<!-- Preloader -->
<!-- ============================================================== -->
<div class="preloader">
    <svg class="circular" viewBox="25 25 50 50">
        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
    </svg>
</div>
<!-- ============================================================== -->
<!-- Wrapper -->
<!-- ============================================================== -->
<div id="wrapper">
    <!-- ============================================================== -->
    <!-- Topbar header - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <nav class="navbar navbar-default navbar-static-top m-b-0">
        <div class="navbar-header">
            <div class="top-left-part">
                <!-- Logo -->
                <a class="logo" href="<?php echo PYTHON_ADM_URL; ?>/index.php">
                    <!-- Logo icon image, you can use font-icon also -->
                    <b>
                        <img src="<?php echo PYTHON_ADM_URL; ?>/plugins/images/admin-logo.png" alt="home" class="dark-logo"/>
                        <img src="<?php echo PYTHON_ADM_URL; ?>/plugins/images/admin-logo-dark.png" alt="home" class="light-logo"/>
                    </b>
                    <!-- Logo text image you can use text also -->
                    <span class="hidden-xs" >
<!--                        <img src="--><?php //echo PYTHON_ADM_URL; ?><!--/plugins/images/admin-text.png" alt="home" class="dark-logo"/>-->
<!--                        <img src="--><?php //echo PYTHON_ADM_URL; ?><!--/plugins/images/admin-text-dark.png" alt="home" class="light-logo"/>-->
                        <h4 style="color: #000;display: inline;font-weight: 600;">Admin</h4>
                    </span>
                </a>
            </div>
            <!-- /Logo -->
            <ul class="nav navbar-top-links navbar-right pull-right">
                <li>
                    <a class="nav-toggler open-close waves-effect waves-light hidden-md hidden-lg"
                       href="javascript:void(0)"><i class="fa fa-bars"></i></a>
                </li>
                <!--<li>
                    <form role="search" class="app-search hidden-sm hidden-xs m-r-10">
                        <input type="text" placeholder="Search..." class="form-control"> <a href=""><i
                                class="fa fa-search"></i></a> </form>
                </li>-->
            </ul>
        </div>
        <!-- /.navbar-header -->
        <!-- /.navbar-top-links -->
        <!-- /.navbar-static-side -->
    </nav>
    <!-- End Top Navigation -->
    <!-- ============================================================== -->
    <!-- Left Sidebar - style you can find in sidebar.scss  -->
    <!-- ============================================================== -->
    <div class="navbar-default sidebar" role="navigation">
        <div class="sidebar-nav slimscrollsidebar">
            <div class="sidebar-head">
                <h3><span class="fa-fw open-close"><i class="ti-close ti-menu"></i></span> <span
                        class="hide-menu">Menu</span></h3>
            </div>
            <ul class="nav" id="side-menu">
                <li style="padding: 70px 0 0;">
                    <a href="<?php echo PYTHON_ADM_URL; ?>/dashboard.php" class="waves-effect">
                        <i class="fa fa-caret-right" aria-hidden="true"></i> Dashboard
                    </a>
                </li>
                <li>
                    <a href="<?php echo PYTHON_ADM_URL; ?>/naver_crawling.php" class="waves-effect">
                        <i class="fa fa-caret-right" aria-hidden="true"></i> Naver crawling
                    </a>
                </li>
                <li>
                    <a href="<?php echo PYTHON_ADM_URL; ?>/tongsinga.php" class="waves-effect">
                        <i class="fa fa-caret-right" aria-hidden="true"></i> 통신가
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Left Sidebar -->
    <!-- ============================================================== -->

    <!-- ============================================================== -->
    <!-- Page Content (각 페이지 파일에서 include) -->
    <!-- ============================================================== -->
    <div id="page-wrapper">

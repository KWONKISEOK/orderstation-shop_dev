<!-- /.container-fluid -->
<!--<footer class="footer text-center"> 2017 &copy; Ample Admin brought to you by wrappixel.com</footer>-->
<footer class="footer text-center"> 2020 &copy; Taejeon O&K </footer>
</div>
<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<!-- Loading -->
<div id="loading">
    <img id="loading-image" src="<?php echo PYTHON_ADM_URL ?>/img/loading.gif" alt="Loading..." />
</div>

<style>
    #loading {
        width: 100%;
        height: 100%;
        top: 0px;
        left: 0px;
        position: fixed;
        display: none;
        opacity: 0.7;
        background-color: #fff;
        z-index: 9999;
        text-align: center;
    }

    #loading-image {
        position: absolute;
        top: 50%;
        left: 50%;
        z-index: 100;
    }
</style>


<!-- Bootstrap Core JavaScript -->
<script src="/python-admin/common/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="/python-admin/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
<!--slimscroll JavaScript -->
<script src="/python-admin/common/js/jquery.slimscroll.js"></script>
<!--Wave Effects -->
<script src="/python-admin/common/js/waves.js"></script>
<!-- Custom Theme JavaScript -->
<script src="/python-admin/common/js/custom.min.js"></script>
</body>

</html>
<?php

### Data Lake DB Connect ###
$host = 'onk-database-datalake-instance-1.cyy8jt4en2pr.ap-northeast-2.rds.amazonaws.com';
$db_user = 'dbadmin';
$db_pass = 'CfTW4EaffFKQbAmSaqq7';
$db_name = 'tjerp';

$datalake_db = mysqli_connect($host, $db_user, $db_pass, $db_name);
if (mysqli_connect_errno()) {
    die('Connect Error: '.mysqli_connect_error());
}
sql_set_charset('utf8', $datalake_db);



// 한페이지에 보여줄 행, 현재페이지, 총페이지수, URL
function custom_get_paging($write_pages, $cur_page, $total_page, $url, $add="")
{
    //$url = preg_replace('#&amp;page=[0-9]*(&amp;page=)$#', '$1', $url);
    $url = preg_replace('#&amp;page=[0-9]*#', '', $url) . '&amp;page=';

    $str = '<li class="page-item">';
    if ($cur_page > 1) {
        $str .= '<a href="'.$url.'1'.$add.'" class="page-link">처음</a>'.PHP_EOL;
    }

    $start_page = ( ( (int)( ($cur_page - 1 ) / $write_pages ) ) * $write_pages ) + 1;
    $end_page = $start_page + $write_pages - 1;

    if ($end_page >= $total_page) $end_page = $total_page;

    if ($start_page > 1) $str .= '<a href="'.$url.($start_page-1).$add.'" class="page-link">이전</a>'.PHP_EOL;

    if ($total_page > 1) {
        for ($k=$start_page;$k<=$end_page;$k++) {
            if ($cur_page != $k)
                $str .= '<a href="'.$url.$k.$add.'" class="page-link">'.$k.'</a>'.PHP_EOL;
            else
                $str .= '<a class="page-link" style="background: #7460ee;color:#fff;"><strong>'.$k.'</strong></a>'.PHP_EOL;
        }
    }

    if ($total_page > $end_page) $str .= '<a href="'.$url.($end_page+1).$add.'" class="page-link">다음</a>'.PHP_EOL;

    if ($cur_page < $total_page) {
        $str .= '<a href="'.$url.$total_page.$add.'" class="page-link">맨끝</a>'.PHP_EOL;
    }
    $str .= '</li>';

    if ($str)
        return "<nav aria-label=\"Page navigation\" style=\"text-align: center;\"><ul class=\"pagination justify-content-center\">{$str}</ul></nav>";
    else
        return "";
}

function number_sign ($value) {
    if ($value > 0) {
        return '+'.number_format($value);
    } else {
        return number_format($value);
    }
}

function tsg_number_format ($price) {
    if ($price == 0 || empty($price)) {
        return '-';
    } else {
        return number_format(($price));
    }
}
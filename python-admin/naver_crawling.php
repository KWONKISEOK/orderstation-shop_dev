<?php
include_once('../common.php');
include_once('common/top.php');

?>

<div class="container-fluid">
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">NAVER Crawling</h4>
        </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <!--<li>
                    <a href="#">Dashboard</a>
                </li>-->
                <li class="active">Crawling Link : https://shopping.naver.com/</li>
            </ol>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /row -->
    <div class="row">
        <div class="col-sm-12">
            <div class="white-box">
                <h3 class="box-title">LIST</h3>
                <div>
                    <form name="rform" class="form-inline" method="post" onsubmit="return false;">
                        <div class="form-group">
                            <input type="text" class="form-control" id="keyword" name="keyword" value="<?php echo $item; ?>" placeholder="검색어를 입력해주세요.">
                        </div>
                        <button type="button" class="btn btn-primary" id="search_btn" onclick="form_submit()">검색</button>
                        <button type="button" class="btn btn-success" onclick="excel_down();"><i class="fa fa-file-excel-o" aria-hidden="true"></i> 엑셀다운로드</button>

                        <div class="form-group" style="float: right; padding-top: 10px;">
                            <i class="fa fa-sort-amount-desc" aria-hidden="true"></i>
                            &nbsp;
                            <input type="radio" id="sort_opt1" name="sort" value="rel" onclick="form_submit()" checked>
                            <label for="sort_opt1">네이버 랭킹순</label>&nbsp;
                            <input type="radio" id="sort_opt2" name="sort" value="price_asc" onclick="form_submit()">
                            <label for="sort_opt2">낮은 가격순</label>&nbsp;
                            <input type="radio" id="sort_opt3" name="sort" value="price_dsc" onclick="form_submit()">
                            <label for="sort_opt3">높은 가격순</label>&nbsp;
                            <input type="radio" id="sort_opt4" name="sort" value="date" onclick="form_submit()">
                            <label for="sort_opt4">등록일순</label>&nbsp;
                            <input type="radio" id="sort_opt5" name="sort" value="review" onclick="form_submit()">
                            <label for="sort_opt5">리뷰 많은순</label>&nbsp;
                        </div>
                    </form>
                </div>
                <div class="table-responsive" style="margin-top: 20px;">
                    <table class="table">
                        <thead>
                        <tr style="background: #f3f3f3">
                            <th>상품명</th>
                            <th>판매처</th>
                            <th>상품 정보</th>
                            <th width="100">상품 가격</th>
                            <th width="120">배송가격 정보</th>
                            <th width="80">리뷰</th>
                            <th width="80">구매건수</th>
                            <th width="80">등록일</th>
                        </tr>
                        </thead>
                        <tbody id="lists">
                            <tr>
                                <td colspan="8" align="center" style="padding: 50px;">검색어를 입력해주세요.</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- /.row -->
</div>

<script src="<?php echo G5_ADMIN_URL; ?>/jqueryfileDownload.js"></script>

<script type="text/javascript">
    $(function () {
        $("#keyword").keypress(function (e) {
            if (e.keyCode == 13){
                form_submit();
            }
        });
    });

    function form_submit() {
        var keyword = $('input[name=keyword]');
        var sort = $('input:radio[name=sort]:checked');

        if (sort.val() != "" && keyword.val() == "") {
            $('input[name=keyword]').focus();
            return false;
        }

        if (keyword.val() == "") {
            alert("검색어를 입력해주세요.");
            $('input[name=keyword]').focus();
            return false;
        }

        $('#loading').show();

        $.post("<?php echo PYTHON_ADM_URL; ?>/ajax.naver_crawling.php", $("form[name=rform]").serialize(), function (data) {
            $("#lists").html(data);
            $('#loading').hide();
        });
    }

    function excel_down() {
        var keyword = $('input[name=keyword]');
        var sort = $('input:radio[name=sort]:checked');

        if (keyword.val() == "") {
            alert("검색어를 입력해주세요.");
            $('input[name=keyword]').focus();
            return false;
        }

        $.fileDownload("<?php echo PYTHON_ADM_URL; ?>/naver_crawling_excel.php",{
            httpMethod:"POST",
            data:$("form[name=rform]").serialize(),
            successCallback: function(url){
                $('#loading').hide();
            },
            failCallback: function(responseHtml,url){
                $('#loading').hide();
            },
            prepareCallback: function() {
                $('#loading').show();
            }
        });
    }
</script>


<?php
include_once('common/bottom.php');
?>
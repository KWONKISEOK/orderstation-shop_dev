<?php

# 변수에 한글이 포함될 경우 현재 서버의 locale을 확인하여 변경해주도록 한다.
$locale = shell_exec('locale -a | grep ko_KR');
if (strpos($locale, 'ko_KR') !== false) {
    putenv("LANG=ko_KR.UTF-8");
    setlocale(LC_ALL, 'ko_KR.utf8');
}

$item = $_POST['keyword'];
$sort = $_POST['sort'];
$list = array();

if (!empty($item)) {
    $result = json_decode(exec('python3 naver_crawling.py '.$item.' '.$sort), true);

    $product_list = $result['product_list'];
    $mall_name = $result['mall_name'];
    $product_count_info = $result['product_count_info'];
    $product_price = $result['product_price'];
    $delivery_price = $result['delivery_price'];
    $review_cnt = $result['review_cnt'];
    $purchaseCnt = $result['purchaseCnt'];
    $openDate = $result['openDate'];
    $adcrUrl = $result['adcrUrl'];
    $crUrl = $result['crUrl'];

    for ($i = 0; $i < count($product_list); $i++) {
        $list[$i]['product_list'] = $product_list[$i];
        $list[$i]['mall_name'] = $mall_name[$i];
        if (strpos($product_count_info[$i], '|') !== false) {
            $temp_str = explode('|', $product_count_info[$i]);
            $list[$i]['product_count_info'] = $temp_str;
        } else {
            $list[$i]['product_count_info'] = $product_count_info[$i];
        }
        $list[$i]['product_price'] = $product_price[$i];
        $list[$i]['delivery_price'] = $delivery_price[$i];
        $list[$i]['review_cnt'] = $review_cnt[$i];
        $list[$i]['purchaseCnt'] = $purchaseCnt[$i];
        $list[$i]['openDate'] = $openDate[$i];
        if (strlen($adcrUrl[$i]) > 0) {
            $list[$i]['Url'] = $adcrUrl[$i];
        } else {
            $list[$i]['Url'] = $crUrl[$i];
        }

    }
}

// HTML TABLE CREATE
if (count($list) > 0) {
    foreach ($list as $k => $v) {
        echo '<tr>';
        echo '<td><a href="'.$v['Url'].'" target="_blank">'.$v['product_list'].'</a></td>';
        echo '<td>'.$v['mall_name'].'</td>';
        if (is_array($v['product_count_info'])) {
            echo '<td>';
            foreach ($v['product_count_info'] as $txt) {
                echo '<button class="btn btn-primary btn-xs" style="margin-top: 5px;">'.$txt.'</button>&nbsp;';
            }
            echo '</td>';
        } else if (!empty($v['product_count_info'])) {
            echo '<td><button class="btn btn-primary btn-xs">'.$v['product_count_info'].'</button></td>';
        } else {
            echo '<td> - </td>';
        }
        echo '<td>'.number_format($v['product_price']).'</td>';
        echo '<td>'.number_format($v['delivery_price']).'</td>';
        echo '<td>'.number_format($v['review_cnt']).'</td>';
        echo '<td>'.number_format($v['purchaseCnt']).'</td>';
        echo '<td>'.date('Y.m', strtotime($v['openDate'])).'</td>';
        echo '</tr>';
    }
} else {
    echo '<tr><td colspan="5">검색 결과가 없습니다.</td></tr>';
}
?>
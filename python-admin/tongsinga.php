<?php
include_once('../common.php');
include_once('common/common.lib.php');
include_once('common/top.php');

$tsg_table = 'TSG_MALL_INFO';

$mall_type = $_GET['mall_type'];
$sca = $_GET['sca'];
$realtime_chk = $_GET['realtime_chk'];

if (empty($mall_type)) $mall_type = 'H';
if (empty($realtime_chk)) $realtime_chk = 'N';

switch ($mall_type) {
    case 'H':
        $mall_name = '한미몰';
        break;
    case 'D':
        $mall_name = '더샵';
        break;
    case 'P':
        $mall_name = '팜스넷';
        break;
}

$where = " where 1=1";
$where .= " and MALL_GB='$mall_type'";

if ($stx != '') {
    if ($sca == 'ITMNO') {
        $where .= " and ITMNO='$stx'";
    } else if ($sca == 'ITM_NM') {
        $where .= " and ITM_NM like '%$stx%'";
    }
}


$sql = "select * from {$tsg_table} {$where}";
$total_count = sql_num_rows(sql_query($sql, false, $datalake_db));

$sql = "select SYSIDATE as d, date_format(SYSITIME, \"%l\") as t, date_format(SYSITIME, \"%p\") as l from {$tsg_table} where MALL_GB='$mall_type' order by SYSIDATE, SYSITIME asc limit 1";
$udate = sql_fetch($sql, false, $datalake_db);

$udate_d = date('Y년 m월 d일', strtotime($udate['d']));
$udate_t = $udate['t'];
($udate['l'] == 'AM') ? $udate_l = '오전' : $udate_l = '오후';

$update_time = $udate_d.' '.$udate_l.' '.$udate_t.'시';

$write_page = 5;
$rows = 5; // 페이지 row 설정
$total_page = ceil($total_count / $rows); // 전체 페이지 계산
if ($page < 1) $page = 1;

$qstr .= '&mall_type='.$mall_type.'&realtime_chk='.$realtime_chk;

?>

    <style>
        #listTable table tr th {text-align: center;font-weight: 900;vertical-align: middle; padding: 8px 8px;}
        #listTable table tr td {text-align: center; vertical-align: middle; padding: 8px 8px;}
        #listTable table .first {background: #e4e7ea}
        #listLoading {
            background: #fbfbfb;
            text-align: center;
            padding: 120px;
            margin-top: 20px;
        }
    </style>

    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">통신가 (<?php echo $mall_name; ?>)</h4>
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <!--<li>
                        <a href="#">Dashboard</a>
                    </li>-->
<!--                    <li class="active">Crawling Link : https://shopping.naver.com/</li>-->
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    <form name="rform" class="form-inline" method="get" action="./tongsinga.php">
                        <input type="hidden" name="page" value="<?php echo $page; ?>">

                        <div class="btn-group" data-toggle="buttons" style="padding-bottom: 15px;">
                            <label class="btn btn-default <?php if ($mall_type == 'H') echo 'active'; ?>">
                                <input type="radio" name="mall_type" id="mall_type1" value="H" autocomplete="off" <?php if ($mall_type == 'H') echo 'checked'; ?> onchange="mall_change(this.value);">한미몰
                            </label>
                            <label class="btn btn-default <?php if ($mall_type == 'D') echo 'active'; ?>">
                                <input type="radio" name="mall_type" id="mall_type2" value="D" autocomplete="off" <?php if ($mall_type == 'D') echo 'checked'; ?> onchange="mall_change(this.value);">더샵
                            </label>
                            <label class="btn btn-default <?php if ($mall_type == 'P') echo 'active'; ?>">
                                <input type="radio" name="mall_type" id="mall_type3" value="P" autocomplete="off" <?php if ($mall_type == 'P') echo 'checked'; ?> onchange="mall_change(this.value);">팜스넷
                            </label>
                        </div>
                        <h3 class="box-title">검색결과(<?php echo number_format($total_count); ?>건)</h3>
                        <div>
                            <div class="form-group">
                                <select name="sca" class="form-control">
                                    <option value="ITM_NM" <?php if ($sca == 'ITM_NM') echo 'selected'; ?>>상품명</option>
                                    <?php if ($mall_type != 'P') { ?>
                                    <option value="ITMNO" <?php if ($sca == 'ITMNO') echo 'selected'; ?>>ERP CODE</option>
                                    <?php } ?>
                                </select>
                                <input type="text" class="form-control" id="stx" name="stx" value="<?php echo $stx; ?>" placeholder="검색어를 입력해주세요.">
                            </div>
                            <button type="submit" class="btn btn-primary" id="search_btn">검색</button>
                            <a href="./tongsinga.php" class="btn btn-default">검색 초기화</a>

                            <span style="margin-left: 15px;">
                                <input type="checkbox" name="realtime_chk" id="realtime_chk" value="Y" onclick="form_realtime_check();" <?php echo get_checked($realtime_chk, 'Y'); ?>>
                                <label for="realtime_chk">실시간 조회</label>
                            </span>

                            <div class="form-group" style="float: right; padding-top: 15px;">
                                업데이트됨 <span style="color: #7460ee;"><b><?php echo $update_time; ?></b></span>
                            </div>
                        </div>
                    </form>
                    <div id="listLoading" style="display: none;">
                        <img src="/img/loading.gif" alt="">
                    </div>
                    <div id="listTable" class="table-responsive" style="display: none; margin-top: 20px;">
                    </div>
                    <?php if ($total_count > 0) { ?>
                        <?php echo custom_get_paging($write_page, $page, $total_page, "{$_SERVER['SCRIPT_NAME']}?$qstr&amp;page="); ?>
                    <?php } ?>
                </div>

            </div>
        </div>
        <!-- /.row -->
    </div>

    <script src="<?php echo G5_ADMIN_URL; ?>/jqueryfileDownload.js"></script>

    <script type="text/javascript">
        $(function () {
            $("#keyword").keypress(function (e) {
                if (e.keyCode == 13) {
                    form_submit();
                }
            });

            ts_lists();
        });

        function ts_lists() {

            $('#listLoading').show();

            $.ajax({
                url: "<?php echo PYTHON_ADM_URL; ?>/ajax.tongsinga.php",
                method: "GET",
                data: $('form[name=rform]').serialize(),
                success: function(res) {
                    $("#listLoading").hide();

                    $("#listTable").html(res);
                    $("#listTable").show();
                }
            });
        }

        function mall_change(type) {
            if (type != '') {
                location.href = "<?php echo PYTHON_ADM_URL; ?>/tongsinga.php?mall_type=" + type;
            }
        }

        function form_submit() {
            $("form[name=rform]").submit();
        }

        function form_realtime_check() {

            if($("input[name=realtime_chk]").is(":checked") === true) {
                if (confirm('선택하신 검색 옵션을 사용하는 경우 조회하는 모든 상품은 실시간으로 조회됩니다. 사용하시겠습니까?')) {
                    form_submit();
                } else {
                    $("input[name=realtime_chk]").prop("checked", false);
                }
            } else {
                form_submit();
            }

        }

        // 통신사 실시간 조회 및 업데이트 처리
        function ts_update(mall_type, mall_code, target) {
            var ts_id = $(target).closest('tr').data('ts');

            $('#loading').show();

            $.post(
                "<?php echo PYTHON_ADM_URL; ?>/ajax.tongsinga_update.php",
                {mall_type: mall_type, mall_code: mall_code},
                function (data) {
                    re = JSON.parse(data);
                    var num = 1;
                    $.each(re, function (k, v) {
                        // 상품명
                        //$('[data-ts='+ts_id+'] .product_name').html(v.product_name);
                        // 공급가
                        $('[data-ts='+ts_id+'] .gg_price'+num).html(v.ggs_price + ' <font color="red">(' + v.ggs_price_compare + ')</font>');
                        // 세일가
                        $('[data-ts='+ts_id+'] .sale_price'+num).html(v.ggs_sale_price  + ' <font color="red">(' + v.ggs_sale_price_compare + ')</font>');
                        // 재고
                        $('[data-ts='+ts_id+'] .jg'+num).html(v.ggs_jg  + ' <font color="red">(' + v.ggs_jg_compare + ')</font>');
                        // 시간
                        $('[data-ts='+ts_id+'] .update_date').html(v.ggs_update);
                        //$('[data-ts='+ts_id+']').css('color', 'red');

                        num++;
                    });

                    $('#loading').hide();
                });
        }

    </script>


<?php
include_once('common/bottom.php');
?>
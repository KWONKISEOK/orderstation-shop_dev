<?php
include_once('../common.php');
include_once('common/common.lib.php');

$tsg_table = 'TSG_MALL_INFO';

$host = "http://203.251.44.107/ONKService";
$method = "/api/crawler/prices";

$url = $host.$method;

$sql = "select * from {$tsg_table} where MALL_GB!='P' limit 20";
$result = sql_fetch_result($sql, $datalake_db);

$curl_data = array();
foreach ($result as $k => $row) {

    $curl_data[$k] = array(
        "ItmNo" => $row['ITMNO'],
        "MallGB" => $row['MALL_GB'],
        "MallCode" => $row['MALL_CODE'],
        "CompNm1" => $row['COMP_NM1'],
        "CompNm2" => $row['COMP_NM2'],
        "TjCost" => intval($row['TJ_COST']),
        "CompCost1" => intval($row['COMP_COST1']),
        "CompCost2" => intval($row['COMP_COST2']),
        "TjSaleCost" => intval($row['TJ_SALE_COST']),
        "CompSaleCost1" => intval($row['COMP_SALE_COST1']),
        "CompSaleCost2" => intval($row['COMP_SALE_COST2']),
        "TjStock" => intval($row['TJ_STOCK']),
        "CompStock1" => intval($row['COMP_STOCK1']),
        "CompStock2" => intval($row['COMP_STOCK2'])
    );

}

$json_data = json_encode($curl_data, JSON_UNESCAPED_UNICODE);

$header = array(
    'Content-Type: application/json',
    'Content-Length: ' . strlen($json_data)
);

$ch = curl_init();                                 //curl 초기화
curl_setopt($ch, CURLOPT_HTTPHEADER, $header); // Header 선언해야 json 전송 가능
curl_setopt($ch, CURLOPT_URL, $url);               //URL 지정하기
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);    //요청 결과를 문자열로 반환
curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);      //connection timeout 10초
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);   //원격 서버의 인증서가 유효한지 검사 안함

curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);       //POST data
curl_setopt($ch, CURLOPT_POST, true);              //true시 post 전송


$response = curl_exec($ch);
$errcode = curl_error($ch);

curl_close($ch);

$json = json_decode($response, true);

debug2($response);
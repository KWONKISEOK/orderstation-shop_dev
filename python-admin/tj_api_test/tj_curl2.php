<?php
include_once('../common.php');
include_once('common/common.lib.php');

$host = "http://203.251.44.107/ONKService";
$method = "/api/crawler/price";

$url = $host.$method;

$data = array(
    "ItmNo" => "string",
    "MallGB" => "string",
    "MallCode" => "string",
    "CompNm1" => "한글",
    "CompNm2" => "한글22",
    "TjCost" => 100,
    "CompCost1" => 200,
    "CompCost2" => 210,
    "TjSaleCost" => 0,
    "CompSaleCost1" => 0,
    "CompSaleCost2" => 0,
    "TjStock" => 0,
    "CompStock1" => 0,
    "CompStock2" => 0
);

$json_data = json_encode($data, JSON_UNESCAPED_UNICODE);

//debug2($json_data);
//exit;

$header = array(
    'Content-Type: application/json',
    'Content-Length: ' . strlen($json_data)
);

$ch = curl_init();                                 //curl 초기화
curl_setopt($ch, CURLOPT_HTTPHEADER, $header); // Header 선언해야 json 전송 가능
curl_setopt($ch, CURLOPT_URL, $url);               //URL 지정하기
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);    //요청 결과를 문자열로 반환
curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);      //connection timeout 10초
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);   //원격 서버의 인증서가 유효한지 검사 안함

curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);       //POST data
curl_setopt($ch, CURLOPT_POST, true);              //true시 post 전송


$response = curl_exec($ch);
$errcode = curl_error($ch);

curl_close($ch);

$json = json_decode($response, true);

echo "<xmp>";
print_r($json);
echo "</xmp>";
//print_r($result);

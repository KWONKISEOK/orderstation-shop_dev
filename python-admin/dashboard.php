<?php
include_once('../common.php');
include_once('common/top.php');
?>

<div class="container-fluid">
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4   col-sm-4 col-xs-12">
            <h4 class="page-title">Dashboard</h4> </div>
        <!--<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="#">Dashboard</a></li>
            </ol>
        </div>-->
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <!-- ============================================================== -->
    <!-- Different data widgets -->
    <!-- ============================================================== -->
    <!-- .row -->
    <!-- ============================================================== -->
    <!-- chat-listing & recent comments -->
    <!-- ============================================================== -->
    <div class="row">
        <!-- .col -->
        <div class="col-md-12 col-lg-8 col-sm-12">
            <div class="white-box">
                <h3 class="box-title">Update 내역</h3>
                <div class="comment-center p-t-10">
                    <div class="comment-body">
                        <div class="mail-contnet">
                            <h5>ver 0.1 - Naver 크롤링 연동</h5>
                            <span class="time">2020.08.14</span>
                            <br/>
                            <span class="mail-desc">
                                기존에 사용되던 Naver 쇼핑 크롤링을 PHP와 연동하여 크롤링 결과를 즉시 확인 가능하도록 리스트 페이지 구축
                            </span>
                        </div>
                    </div>
                    <div class="comment-body">
                        <div class="mail-contnet">
                            <h5>ver 0.2 - Naver 크롤링 연동</h5>
                            <span class="time">2020.08.26</span>
                            <br/>
                            <span class="mail-desc">
                                상품 검색 결과 엑셀다운로드 및 검색 정렬 옵션 기능 추가
                            </span>
                        </div>
                    </div>
                    <div class="comment-body">
                        <div class="mail-contnet">
                            <h5>ver 0.3 - Naver 크롤링 연동</h5>
                            <span class="time">2020.09.01</span>
                            <br/>
                            <span class="mail-desc">
                                상품명 클릭 시 해당 상품의 링크 연결 추가
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-12">
            <div class="panel">
                <div class="sk-chat-widgets">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Python 패키지
                        </div>
                        <div class="panel-body">
                            <ul class="chatonline">
                                <li>numpy</li>
                                <li>BeautifulSoup4</li>
                                <li>bs4 requests</li>
                                <li>pandas</li>
                                <li>urllib3</li>
                                <li>parse</li>
                                <li>html5lib</li>
                                <li>lxml</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
include_once('common/bottom.php');
?>
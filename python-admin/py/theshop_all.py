import requests
from bs4 import BeautifulSoup
import time
import re
import logging
import set_log
import sys

# sys.stdout = open('../python-admin/py/theshop_output.txt','w')

# 로그 함수 호출
set_log.get_logging()

# 상품 몰 코드(마스터 코드)
master_code_ls=[]

# 상품 코드(erp 코드)
erp_code_ls=[]

# 상품명 상세
product_name_ls=[]

# 몰코드 키값
product_key_ls = []

# 공급사 이름
ggs_name1_ls = []
ggs_name2_ls = []
ggs_name3_ls = []

# 공급가
ggs_price1_ls = []
ggs_price2_ls = []
ggs_price3_ls = []

# 세일가
ggs_sale_price1_ls = []
ggs_sale_price2_ls = []
ggs_sale_price3_ls = []

# 재고
ggs_jg1_ls = []
ggs_jg2_ls = []
ggs_jg3_ls = []

# 상품명
product_name_ls2 = []

# 로그인할 유저정보를 넣어줍시다. (모두 문자열입니다!)
LOGIN_INFO = {
    'loginType': '01',
    'userId': 'okpharm1',
    'userPwd': 'okpharm1!@#'
}

# Session 생성, with 구문 안에서 유지
with requests.Session() as s:

    # Header SET
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36'
    }

    login_req = s.post('https://www.shop.co.kr/front/api/auth/loginJsonp', headers=headers, data=LOGIN_INFO)
    # logging.debug(login_req.text)
    
    #response 정상확인
    #보통 상태값이 200이면 성공한 상태를 의미함.
    if login_req.status_code==200 and login_req.ok:

        logging.debug("로그인 성공")

        first_cokk=login_req.cookies.get_dict()
        
        #페이지가 최대 100 페이지라고 생각하고 for문을 돌림.
        for j in range(1,100,1) :
            
            #에러가 나기 전까지 시도해라.
            try :
                
                #페이지 호출 정보
                PAGE_INFO = {
                    'goodsInfoDataBean.currentPage': str(j),
                    'goodsInfoDataBean.orderBy': 'saleCnt',
                    'ctgCd': 'OTC',
                    'goodsInfoDataBean.searchSellerCd': '11324'
                }

                PAGE_COKK = {
                    # 'SCOUTER':str(first_cokk['SCOUTER']),
                    # 'userId':'okpharm1',
                    # 'USER_SELLER_AREA':'50011%2C12151',
                    # 'PASSWORD_MODI_CHECK1000099226':'0',
                    # 'TS01ab530a':'0156a1c66cad8d91f2c8271a3db757067950213162051d3a3e27fb1883ca2227ef148fe4d29f5c08c1ccf8951f9f41b165d30c47a66f11c3fa9020eb93969856d297542b955216121691c84e430b9fade9cc1cbe52a0d063e202f8057cf7808c4955602fb85fe3b402bcbafe49449f0d4207a3b01b4ab0dc05542067a8f82f6db9c76d9b0f4181f3af30915686cc7376e422fb00105e169031e498342a317da6a2e04306a250508d12b23d3fdc21b76a1cb4776b1c39be333f5b56ef822cc5ff78fdeb571e',
                    # 'USER_AREA_INFO1000099226':'Y^%EC%84%9C%EC%9A%B8^%EC%A2%85%EB%A1%9C%EA%B5%AC^%EA%B3%B5%ED%8F%89%EB%8F%99^',
                    'USER_BEAN':str(first_cokk['USER_BEAN']),
                    # 'USER_MALL_TYPE':'2',
                    # 'USER_LOGIN_DOMAIN_ID':'www',
                    # 'TS01df1835':'0156a1c66c30a5179181bf61283d06bdc246a4f630ece127c5c5cc5ba07cf5d918af9cd75097322a4c7a5dfc62ebcb67d2f008ac5a32048199c90b81c6313083636130e012'
                }
                
                #페이지 호출
                PAGE_req = s.post('http://www.shop.co.kr/ajax/goodsSearchListEgAjax.do',data=PAGE_INFO, cookies=PAGE_COKK)

                #페이지 호출해서 받아온 정보를 html 파싱용 스크립트(?)로 전환
                PAGE_soup=BeautifulSoup(PAGE_req.text,'html.parser')

                #html 파싱 중 tr 태그명 중 class값이 hasLink인 값들을 전부 가져와라
                product_num_ls = PAGE_soup.find_all('tr', attrs={'class': 'hasLink'})

                #위에서 가져온 값들을 기준으로 원하는 값을 추출하기 위한 for문
                #위에는 페이지 전체에 대한 정보 중 긁어오고 싶은 상품코드가 포함된 정보를 가져옴.
                for i in range (0,len(product_num_ls),1) :

                    #상품 마스터 코드 긁어오기
                    product_num=product_num_ls[i]['alt']

                    #상품 마스터 코드 가져온 걸 리스트에 넣어라.
                    master_code_ls.append(product_num)

                    #위에서 상품 마스터 코드를 가져왔으니 ERP 상품코드도 호출해서 받아오자.
                    
                    #상품 정보 호출을 위한 정보
                    prod_INFO = {
                        'goodsInfoDataBean.goodsMasterCd': str(product_num),
                        'goodsInfoDataBean.monoGoodsYn': 'N',
                        'goodsInfoDataBean.searchSellerCd': '11324'
                    }

                    #상품 정보 호출
                    prod_req = s.post('http://www.shop.co.kr/ajax/goodsDtail.do',data=prod_INFO, cookies=PAGE_COKK)

                    #상품 정보 호출로 받아온 값을 html 파싱용 스크립트(?)로 전환
                    prod_soup=BeautifulSoup(prod_req.text,'html.parser')

                    #html 파싱 중 input 태그명 중 id값이 recentGoodsCode인 값들을 전부 가져와라
                    prduct_ls=prod_soup.find_all('input', attrs={'id': 'recentGoodsCode'})

                    #상품 ERP 코드 추출
                    product_erp_code=prduct_ls[0]['value']

                    #상품 ERP 코드 리스트에 보관
                    erp_code_ls.append(product_erp_code)
                    
                    #상품명
                    product_view_name=product_num_ls[i].find_all('td', attrs={'class': 'left'})[0].text
                    
                    #상품명 리스트에 보관
                    product_name_ls.append(product_view_name)
                
            # 만일 위에 코드가 오류가 뜬다면 -> 아마 페이지가 마지막 페이지까지 호출한 뒤 더이상 긁어올 페이지가 없어서 뜨는 오류라고 생각함.
            # 위 가정을 전제로 오류가 발생한다면 코드를 중지해라.
            except :
                print("에러는 " + str(j) + "번째 페이지 입니다.")
                break

            logging.debug("상품 리스트 정보 호출 완료")    
                
        for j in range(0, len(master_code_ls), 1):
            
            # 상품 정보 호출을 위한 정보
            prod_INFO = {
                'goodsInfoDataBean.goodsMasterCd': str(master_code_ls[j]),
                'goodsInfoDataBean.monoGoodsYn': 'N',
                'goodsInfoDataBean.searchSellerCd': '11324'
            }

            # 상품 정보 호출
            prod_req = s.post('http://www.shop.co.kr/ajax/goodsDtail.do', data=prod_INFO, cookies=PAGE_COKK)

            # 상품 정보 호출로 받아온 값을 html 파싱용 스크립트(?)로 전환
            prod_soup = BeautifulSoup(prod_req.text, 'html.parser')

            # 몰 코드 키값 저장
            product_key_ls.append(master_code_ls[j])

            # 상품명 추출
            product_name=prod_soup.find_all('input', attrs={'id': 'recentGoodsNm'})[0]['value']

            # 상품명 저장
            product_name_ls2.append(str(product_name))


            for i in range(0, 3, 1):

                if i == 0:

                    # 티제이팜일텐데 확인차 추출

                    # 공급사 추출
                    ggs_ls = prod_soup.find_all('td', attrs={'id': 'choiceGoodsCd'})
                    ggs_name = ggs_ls[i].text
                    ggs_name = ggs_name.replace("\n", "")
                    ggs_name = ggs_name.replace("\r", "")
                    ggs_name = ggs_name.replace("\t", "")
                    ggs_name = ggs_name.replace(" ", "")

                    ggs_name1_ls.append(ggs_name)

                    # 공급가 추출
                    ggg_ls = prod_soup.find_all('td', attrs={'class': 'price'})
                    gg_price = ggg_ls[i].text
                    gg_price = gg_price.replace("\n", "")
                    gg_price = gg_price.replace("\r", "")
                    gg_price = gg_price.replace("\t", "")
                    gg_price = gg_price.replace(" ", "")

                    ggs_price1_ls.append(gg_price)

                    # 세일가
                    sale_ls = prod_soup.find_all('td', attrs={'class': 'sale'})

                    # 세일가가 있을 때
                    try:
                        # 세일가 추출
                        sale_price = sale_ls[i].text
                        sale_price = sale_price.replace("\n", "")
                        sale_price = sale_price.replace("\r", "")
                        sale_price = sale_price.replace("\t", "")
                        sale_price = sale_price.replace(" ", "")
                        sale_price = sale_price.replace("\xa0", "")

                        ggs_sale_price1_ls.append(sale_price)

                        # 재고 - 세일가가 있을 때 순서 3 / 7 / 11
                        jg1 = prod_soup.find_all('td')[3].text
                        jg1 = jg1.replace("\n", "")
                        jg1 = jg1.replace("\r", "")
                        jg1 = jg1.replace("\t", "")
                        jg1 = jg1.replace(" ", "")
                        jg1 = jg1.replace("\xa0", "")

                        ggs_jg1_ls.append(jg1)

                    # 세일가가 없을 때
                    except:
                        # 세일가 추출
                        sale_price = ''
                        ggs_sale_price1_ls.append(sale_price)

                        # 재고 - 세일가가 없을 때 순서 2 / 5 / 8
                        jg1 = prod_soup.find_all('td')[2].text
                        jg1 = jg1.replace("\n", "")
                        jg1 = jg1.replace("\r", "")
                        jg1 = jg1.replace("\t", "")
                        jg1 = jg1.replace(" ", "")
                        jg1 = jg1.replace("\xa0", "")

                        ggs_jg1_ls.append(jg1)


                # 최저가 2곳에 대한 정보 가져오기
                else:
                    #상품 긁어오는게 끝날때(=에러날때)
                    try:
                        #공급가가 있을때
                        try :
                            # 공급사 추출
                            ggs_ls = prod_soup.find_all('td', attrs={'id': 'choiceGoodsCd'})
                            ggs_name = ggs_ls[i].text
                            ggs_name = ggs_name.replace("\n", "")
                            ggs_name = ggs_name.replace("\r", "")
                            ggs_name = ggs_name.replace("\t", "")
                            ggs_name = ggs_name.replace(" ", "")

                            # 공급가 추출
                            ggg_ls = prod_soup.find_all('td', attrs={'class': 'price'})
                            gg_price = ggg_ls[i].text
                            gg_price = gg_price.replace("\n", "")
                            gg_price = gg_price.replace("\r", "")
                            gg_price = gg_price.replace("\t", "")
                            gg_price = gg_price.replace(" ", "")

                            if i == 1:
                                ggs_name2_ls.append(ggs_name)
                                ggs_price2_ls.append(gg_price)
                            else:
                                ggs_name3_ls.append(ggs_name)
                                ggs_price3_ls.append(gg_price)

                            # 세일가
                            sale_ls = prod_soup.find_all('td', attrs={'class': 'sale'})

                            # 세일가가 있을 때
                            try:
                                # 세일가 추출
                                sale_price = sale_ls[i].text
                                sale_price = sale_price.replace("\n", "")
                                sale_price = sale_price.replace("\r", "")
                                sale_price = sale_price.replace("\t", "")
                                sale_price = sale_price.replace(" ", "")
                                sale_price = sale_price.replace("\xa0", "")

                                if i == 1:
                                    k = 7
                                else:
                                    k = 11

                                # 재고 - 세일가가 있을 때 순서 3 / 7 / 11
                                jg2 = prod_soup.find_all('td')[k].text
                                jg2 = jg2.replace("\n", "")
                                jg2 = jg2.replace("\r", "")
                                jg2 = jg2.replace("\t", "")
                                jg2 = jg2.replace(" ", "")
                                jg2 = jg2.replace("\xa0", "")

                                if i == 1:
                                    ggs_sale_price2_ls.append(sale_price)
                                    ggs_jg2_ls.append(jg2)
                                else:
                                    ggs_sale_price3_ls.append(sale_price)
                                    ggs_jg3_ls.append(jg2)

                            # 세일가가 없을 때
                            except:
                                # 세일가 추출
                                sale_price = ''

                                if i == 1:
                                    k = 5
                                else:
                                    k = 8

                                # 재고 - 세일가가 없을 때 순서 2 / 5 / 8
                                jg2 = prod_soup.find_all('td')[k].text
                                jg2 = jg2.replace("\n", "")
                                jg2 = jg2.replace("\r", "")
                                jg2 = jg2.replace("\t", "")
                                jg2 = jg2.replace(" ", "")

                                if i == 1:
                                    ggs_sale_price2_ls.append(sale_price)
                                    ggs_jg2_ls.append(jg2)
                                else:
                                    ggs_sale_price3_ls.append(sale_price)
                                    ggs_jg3_ls.append(jg2)
                        except :
                            # 공급사 추출
                            ggs_name = ''

                            # 공급가 추출                                
                            gg_price = ''

                            if i == 1:
                                ggs_name2_ls.append(ggs_name)
                                ggs_price2_ls.append(gg_price)
                            else:
                                ggs_name3_ls.append(ggs_name)
                                ggs_price3_ls.append(gg_price)

                            # 세일가 추출
                            sale_price = ''

                            # 재고 
                            jg2 = ''

                            if i == 1:
                                ggs_sale_price2_ls.append(sale_price)
                                ggs_jg2_ls.append(jg2)
                            else:
                                ggs_sale_price3_ls.append(sale_price)
                                ggs_jg3_ls.append(jg2)

                    except:
                        break
                
logging.debug("상품 정보 호출 완료")


import json
result_arr = {
    'MALL_CODE':master_code_ls,
    'ITMNO':erp_code_ls,
    'ITM_NM':product_name_ls,
    'ITM_NM2':product_name_ls2,
    'TJ_NM':ggs_name1_ls,
    'COMP_NM1':ggs_name2_ls,
    'COMP_NM2':ggs_name3_ls,
    'TJ_COST':ggs_price1_ls,
    'COMP_COST1':ggs_price2_ls,
    'COMP_COST2':ggs_price3_ls,
    'TJ_SALE_COST':ggs_sale_price1_ls,
    'COMP_SALE_COST1':ggs_sale_price2_ls,
    'COMP_SALE_COST2':ggs_sale_price3_ls,
    'TJ_STOCK':ggs_jg1_ls,
    'COMP_STOCK1':ggs_jg2_ls,
    'COMP_STOCK2':ggs_jg3_ls
    # 'key':product_key_ls
}

result = result_arr
print(json.dumps(result))
# print(product_name_ls)

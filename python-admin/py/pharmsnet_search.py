import requests
from bs4 import BeautifulSoup
import time
import re
import sys
import json
import set_log

# 로그 함수 호출
set_log.get_logging()

# 검색어 지정
input_data = sys.argv[1] # 검색어

# input data를 리스트로 변형
input_ls=input_data.split(",")

#상품 몰 코드(마스터 코드)
master_code_ls=[]

#상품명
product_name_ls=[]

# 공급사 이름
ggs_name1_ls = []
ggs_name2_ls = []
ggs_name3_ls = []

# 공급가
ggs_price1_ls = []
ggs_price2_ls = []
ggs_price3_ls = []

# 할인가
ggs_sale_price1_ls = []
ggs_sale_price2_ls = []
ggs_sale_price3_ls = []

# 재고
ggs_jg1_ls = []
ggs_jg2_ls = []
ggs_jg3_ls = []

# 단위
ggs_dw1_ls = []
ggs_dw2_ls = []
ggs_dw3_ls = []


with requests.Session() as s:

    # 첫 로그인 페이지에 로그인 요청 -> 세션 값을 얻기 위해
    login_req = s.post('http://www.pharmsnet.com/')
    
    if login_req.status_code == 200 and login_req.ok:
        
        # 첫 페이지의 로그인 JSESSIONID 값 저장
        cookie_login=login_req.cookies.get_dict()

        login_data1 ={'ID': 'supptest',
                      'PASSWD': 'dufma2020'}
                
        home_req = s.post('http://www.pharmsnet.com/web/include/jsp/loginCheck.jsp', data=login_data1, cookies=cookie_login)
        
        cokk={'JSESSIONID': str(cookie_login['JSESSIONID']),
              'PMSID': ''}
        home1_req = s.post('http://www.pharmsnet.com/web/main/jsp/b_main.jsp',cookies=cokk)
        
        
        page_data ={'RegiontypE': 'T',
                    'IssueflaG': 'Y',
                    'SugSetUp': 'Y',
                    'ExceptSupp': '51110,67482',
                    'ShapE': 'O',
                    'topSearchTypeMenu': 'itemname',
                    'PrctypE': 'T',
                    'supplierList': '51273'}
        
        page_req = s.post('http://www.pharmsnet.com/web/mall/jsp/b_mall.jsp',data=page_data, cookies=cokk)
        
        page_soup=BeautifulSoup(page_req.text,'html.parser')
                
        for mall_code in input_ls :
            
            product_list_data ={'itemid': str(mall_code),
                                'shape': 'O',
                                'f_supplierid': '51273'}
            
            product_list_req = s.post('http://www.pharmsnet.com/web/mall/jsp/b_mall_detail.jsp',data=product_list_data, cookies=cokk)
            
            product_list_soup=BeautifulSoup(product_list_req.text,'html.parser')
            
            for k in range(0,3,1):
                
                try : 
                    #공급사 명
                    ggs_name=product_list_soup.find_all('span', attrs={'id': re.compile("^suppLst_CompanyName")})[k].text

                    #공급가
                    gg_price=product_list_soup.find_all('tr',attrs={'class': 'dtsuptop'})[k].find_all('td')[2].text
                    gg_price=gg_price.replace("\n", "").replace("\r", "").replace("\t", "").replace(" ", "").replace("\xa0", "").replace('원','')

                    #할인가
                    sale_price=product_list_soup.find_all('tr',attrs={'class': 'dtsuptop'})[k].find_all('td')[3].text
                    sale_price=sale_price.replace("\n", "").replace("\r", "").replace("\t", "").replace(" ", "").replace("\xa0", "").replace('원','')

                    #재고
                    jg1=product_list_soup.find_all('tr',attrs={'class': 'dtsuptop'})[k].find_all('td')[4].text
                    jg1=jg1.replace("\n", "").replace("\r", "").replace("\t", "").replace(" ", "").replace("\xa0", "").replace('개','')

                    #주문단위
                    dw1=product_list_soup.find_all('tr',attrs={'class': 'dtsuptop'})[k].find_all('td')[5].text
                    dw1=dw1.replace("\n", "").replace("\r", "").replace("\t", "").replace(" ", "").replace('개','')
                
                except :
                    ggs_name=''
                    gg_price=''
                    sale_price=''
                    jg1=''
                    dw1=''

                if k == 0 :
                    ggs_name1_ls.append(ggs_name)                   

                    ggs_price1_ls.append(gg_price)
                    
                    ggs_sale_price1_ls.append(sale_price)
                    
                    ggs_jg1_ls.append(jg1)

                    ggs_dw1_ls.append(dw1)
                    
                elif k == 1 :
                    ggs_name2_ls.append(ggs_name)                   

                    ggs_price2_ls.append(gg_price)
                    
                    ggs_sale_price2_ls.append(sale_price)
                    
                    ggs_jg2_ls.append(jg1)

                    ggs_dw2_ls.append(dw1)
                else :
                    ggs_name3_ls.append(ggs_name)                   

                    ggs_price3_ls.append(gg_price)
                    
                    ggs_sale_price3_ls.append(sale_price)
                             
                    ggs_jg3_ls.append(jg1)

                    ggs_dw3_ls.append(dw1)


# 티제이팜
tj = {
    #'product_key':product_key_ls,
    'product_name':product_name_ls,
    'ggs_name':ggs_name1_ls,
    'ggs_price':ggs_price1_ls,
    'ggs_sale_price':ggs_sale_price1_ls,
    'ggs_jg':ggs_jg1_ls,
    'ggs_measure':ggs_dw1_ls
}
# 최저가 1순위
r1 = {
    #'product_key':product_key_ls,
    'product_name':product_name_ls,
    'ggs_name':ggs_name2_ls,
    'ggs_price':ggs_price2_ls,
    'ggs_sale_price':ggs_sale_price2_ls,
    'ggs_jg':ggs_jg2_ls,
    'ggs_measure':ggs_dw2_ls
}
# 최저가 2순위
r2 = {
    #'product_key':product_key_ls,
    'product_name':product_name_ls,
    'ggs_name':ggs_name3_ls,
    'ggs_price':ggs_price3_ls,
    'ggs_sale_price':ggs_sale_price3_ls,
    'ggs_jg':ggs_jg3_ls,
    'ggs_measure':ggs_dw3_ls
}

result_arr = []
result_arr.append(tj)
result_arr.append(r1)
result_arr.append(r2)

result = result_arr
print(json.dumps(result))

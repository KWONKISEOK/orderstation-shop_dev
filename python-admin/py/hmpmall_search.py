import requests
from bs4 import BeautifulSoup
import time
import re
import json
import logging
import set_log
import sys

# 로그 함수 호출
set_log.get_logging()

# 검색어 지정
input_data = sys.argv[1] # 검색어

# input data를 리스트로 변형
input_ls=input_data.split(",")

# 상품 Mall 코드 리스트
product_key_ls=[]

# 상품 ERP 코드 리스트
erp_code_ls=[]

# 티제이팜 이름 리스트
ggs_name1_ls=[]

# 최저가 1 판매 공급가 이름 리스트
ggs_name2_ls=[]

# 최저가 2 판매 공급가 이름 리스트
ggs_name3_ls=[]

# 티제이팜 공급가 리스트
ggs_price1_ls=[]

# 최저가 1 판매 공급가 리스트
ggs_price2_ls=[]

# 최저가 2 판매 공급가 리스트
ggs_price3_ls=[]

# 티제이팜 세일가 리스트
ggs_sale_price1_ls=[]

# 최저가 1 판매 세일가 리스트
ggs_sale_price2_ls=[]

# 최저가 2 판매 세일가 리스트
ggs_sale_price3_ls=[]

# 재고 리스트
ggs_jg1_ls=[]
ggs_jg2_ls=[]
ggs_jg3_ls=[]

# 상품명 리스트
product_name_ls=[]


# 로그인할 유저정보를 넣어줍시다. (모두 문자열입니다!)
LOGIN_INFO = {
    'memberId': 'okpharm1',
    'memberPassword': 'okpharm1!@#',
    'mallDivCode': '1040001'
}

# Session 생성, with 구문 안에서 유지
with requests.Session() as s:
    
    
    # 로그인 프로세스
    # 1. 첫 페이지의 JSESSIONID 저장
    # 2. 첫 페이지의 JSESSIONID 값을 요청변수로 DWRSESSIONID 추출
    # 3. JSESSIONID 값을 변환
    # 4. 변환된 JSESSIONID 과 DWRSESSIONID 을 쿠키값으로 요청해야 로그인, 상품 정보등 요청 가능
    
    
    # 첫 로그인 페이지에 로그인 요청 -> 세션 값을 얻기 위해
    login_req = s.post('https://www.hmpmall.co.kr/login.do', data=LOGIN_INFO)
    
    # 로그인 성공했을 시
    if login_req.status_code == 200 and login_req.ok:
        
        # 첫 페이지의 로그인 JSESSIONID 값 저장
        cookie_login=login_req.cookies.get_dict()
        cokk={'JSESSIONID': str(cookie_login['JSESSIONID'])}
        
        # DWRSESSIONID 값을 구하기 위한 요청
        dwrr_info ='''
callCount=1
c0-scriptName=__System
c0-methodName=generateId
c0-id=00
batchId=0
instanceId=0
page=%2Flogin.do
scriptSessionId=
'''

        # DWRSESSIONID 값 추출을 위한 호출
        dwrr_req = s.post('https://www.hmpmall.co.kr/dwr/call/plaincall/__System.generateId.dwr', data=dwrr_info, cookies=cokk)
        
        # DWRSESSIONID 값 저장
        dwr_text=dwrr_req.text
        dwrsession_1=re.findall('"(.*?)"',dwr_text)[-1]
        
        # JSESSIONID 값 변환을 위한 호출 정보
        jses_info='''
callCount=1
nextReverseAjaxIndex=0
c0-scriptName=common/Login
c0-methodName=execute
c0-id=0
c0-e1=string:
c0-e2=string:okpharm1
c0-e3=string:okpharm1!%40%23
c0-param0=Object_Object:{mallDivCode:reference:c0-e1, memberId:reference:c0-e2, memberPassword:reference:c0-e3}
batchId=1
instanceId=0
page=%2Flogin.do
scriptSessionId=''' + str(dwrsession_1) + '/c*5wvnn-qyC51ayc6'
        
        # 첫 페이지의 JSESSIONID 값 과 DWRSESSIONID 을 쿠키로 저장하여 요청
        cokk2 ={'JSESSIONID': str(cookie_login['JSESSIONID']),
                'DWRSESSIONID': str(dwrsession_1)}
        
        # JSESSIONID 값 변환을 위한 호출
        change_jess = s.post('https://www.hmpmall.co.kr/dwr/call/plaincall/common/Login.execute.dwr', data=jses_info, cookies=cokk2)
        
        # 변환된 JSESSIONID 값 저장
        final_cookie=change_jess.cookies.get_dict()
        final_jesession=final_cookie['JSESSIONID']
        
        # 변환된 JSESSIONID 값과 DWRSESSIONID 값을 쿠키로 저장
        final_cok={'JSESSIONID': str(final_jesession),
                   'DWRSESSIONID': str(dwrsession_1)}
        
        
        ####################### 상품 정보 전처리 과정
        
        # 입력 변수 2개 이상일 수 있으니 for문 적용
        
        for i in range (0,len(input_ls),1):
            
            # 상품 관련 데이터 호출 정보
            prod_data = {'productMasterId': str(input_ls[i]),
                         'fromGubun': '2480000',
                         'orderByProductSeller': 'ORDERBY_BPCO_ID',
                         'orderbySellerId': '134',
                         'groupCategoryNumber': '2',
                         'imgPath': '/statics/imgs',
                         'mgPathX': '/statics/imgs'}
        
            # 상품 정보 호출
            prod_req = requests.post('https://www.hmpmall.co.kr/search/SearchProductSellerListJson.do', params=prod_data, cookies=final_cok)
            
            # 상품 정보 json으로 변형
            product_inform=json.loads(prod_req.text)
            
            # 추후 상품 코드 정보를 키값을 이용하기 위해 리스트에 조회하는 상품 코드 리스트에 저장
            product_key_ls.append(input_ls[i])
            
            # 상품명 추출
            # logging.debug(product_inform['productBasicInfo'])
            product_name=product_inform['productBasicInfo']['productName']
            # product_name=product_name + ' ' + product_inform['productBasicInfo']['productStandard']
            
            # 상품명 리스트에 저장
            product_name_ls.append(str(product_name))
            
            # 상품 정보 중 티제이팜 및 최저가 2개 정리
            for j in range(0,3,1):
                
                #티제이팜 인 경우(항상 처음 값은 티제이팜)
                if j == 0 :
                    
                    # erp code, 공급사 명, 공급가, 세일가, 재고 크롤링
                    try :
                        erp_code=product_inform['sellerSaleProductList'][j]['sellerProductNumber']
                        ggs_name1=product_inform['sellerSaleProductList'][j]['sellerName']
                        ggs_price1=product_inform['sellerSaleProductList'][j]['productUnitPrice']
                        ggs_sale_price1=product_inform['sellerSaleProductList'][j]['discountPrice']
                        ggs_jg1=product_inform['sellerSaleProductList'][j]['stockQuantity']
                    
                    # 위 try 진행했을 때 오류가 나면 티제이 정보가 없는 경우이기에 공란으로 해서 저장
                    except :
                        erp_code=''
                        ggs_name1=''
                        ggs_price1=''
                        ggs_sale_price1=''
                        ggs_jg1=''
                    
                    # 각 해당하는 변수들을 리스트에 적재
                    erp_code_ls.append(erp_code)
                    ggs_name1_ls.append(ggs_name1)
                    ggs_price1_ls.append(ggs_price1)
                    ggs_sale_price1_ls.append(ggs_sale_price1)
                    ggs_jg1_ls.append(ggs_jg1)
                    
                    
                # 최저가 1, 2 업체 가져오기        
                else : 
                    
                    # 최저가 1, 2가 있는 경우
                    try :
                        ggs_name2=product_inform['sellerSaleProductList'][j]['sellerName']
                        ggs_price2=product_inform['sellerSaleProductList'][j]['productUnitPrice']
                        ggs_sale_price2=product_inform['sellerSaleProductList'][j]['discountPrice']
                        ggs_jg2=product_inform['sellerSaleProductList'][j]['stockQuantity']                   
                    
                    # 없는 경우 공란
                    except :
                        ggs_name2=''
                        ggs_price2=''
                        ggs_sale_price2=''
                        ggs_jg2=''
                    
                    # 각 최저가 1일때, 2일때에 맞춰 리스트 저장
                    if j==1 :
                        ggs_name2_ls.append(ggs_name2)
                        ggs_price2_ls.append(ggs_price2)
                        ggs_sale_price2_ls.append(ggs_sale_price2)
                        ggs_jg2_ls.append(ggs_jg2)
                    else :
                        ggs_name3_ls.append(ggs_name2)
                        ggs_price3_ls.append(ggs_price2)
                        ggs_sale_price3_ls.append(ggs_sale_price2)
                        ggs_jg3_ls.append(ggs_jg2)

# 티제이팜
tj = {
    'product_key':product_key_ls,
    'product_name':product_name_ls,
    'ggs_name':ggs_name1_ls,
    'ggs_price':ggs_price1_ls,
    'ggs_sale_price':ggs_sale_price1_ls,
    'ggs_jg':ggs_jg1_ls
}
# 최저가 1순위
r1 = {
    'product_key':product_key_ls,
    'product_name':product_name_ls,
    'ggs_name':ggs_name2_ls,
    'ggs_price':ggs_price2_ls,
    'ggs_sale_price':ggs_sale_price2_ls,
    'ggs_jg':ggs_jg2_ls,
}
# 최저가 2순위
r2 = {
    'product_key':product_key_ls,
    'product_name':product_name_ls,
    'ggs_name':ggs_name3_ls,
    'ggs_price':ggs_price3_ls,
    'ggs_sale_price':ggs_sale_price3_ls,
    'ggs_jg':ggs_jg3_ls
}

result_arr = [];
result_arr.append(tj)
result_arr.append(r1)
result_arr.append(r2)

result = result_arr
print(json.dumps(result))
import requests
from bs4 import BeautifulSoup
import time
import re
import sys
import json
import logging
import set_log

# 로그 함수 호출
set_log.get_logging()

# 검색어 지정
input_data = sys.argv[1] # 검색어

# input data를 리스트로 변형
input_ls=input_data.split(",")

# 상품 Mall 코드 리스트
product_key_ls = []

# 공급사 이름
ggs_name1_ls = []
ggs_name2_ls = []
ggs_name3_ls = []

# 공급가
ggs_price1_ls = []
ggs_price2_ls = []
ggs_price3_ls = []

# 세일가
ggs_sale_price1_ls = []
ggs_sale_price2_ls = []
ggs_sale_price3_ls = []

# 재고 리스트
ggs_jg1_ls = []
ggs_jg2_ls = []
ggs_jg3_ls = []

# 상품명 리스트
product_name_ls = []

# 로그인할 유저정보를 넣어줍시다. (모두 문자열입니다!)
LOGIN_INFO = {
    'loginType': '01',
    'userId': 'okpharm1',
    'userPwd': 'okpharm1!@#'
}

# Session 생성, with 구문 안에서 유지
with requests.Session() as s:

    # Header SET
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36'
    }

    login_req = s.post('https://www.shop.co.kr/front/api/auth/loginJsonp', headers=headers, data=LOGIN_INFO)

    # response 정상확인
    # 보통 상태값이 200이면 성공한 상태를 의미함.
    if login_req.status_code == 200 and login_req.ok:

        logging.debug("로그인 성공")

        first_cokk=login_req.cookies.get_dict()

        # 에러가 나기 전까지 시도해라.
        try:

            for j in range(0, len(input_ls), 1):

                # 상품 정보 호출을 위한 정보
                prod_INFO = {
                    'goodsInfoDataBean.goodsMasterCd': str(input_ls[j]),
                    'goodsInfoDataBean.monoGoodsYn': 'N',
                    'goodsInfoDataBean.searchSellerCd': '11324'
                }

                PAGE_COKK = {
                    'USER_BEAN':str(first_cokk['USER_BEAN'])
                }

                # 상품 정보 호출
                prod_req = s.post('http://www.shop.co.kr/ajax/goodsDtail.do', data=prod_INFO, cookies=PAGE_COKK)

                # 상품 정보 호출로 받아온 값을 html 파싱용 스크립트(?)로 전환
                prod_soup = BeautifulSoup(prod_req.text, 'html.parser')

                # 몰 코드 키값 저장
                product_key_ls.append(input_ls[j])

                # 상품명 추출
                product_name=prod_soup.find_all('table', attrs={'class': 'table_style6'})[0].find_all('td', attrs={'class': 'left'})[0].text
                # logging.debug(product_name)
                # product_name=prod_soup.find_all('input', attrs={'id': 'recentGoodsNm'})[0]['value']

                # 상품명 저장
                product_name_ls.append(str(product_name))


                for i in range(0, 3, 1):

                    if i == 0:

                        # 티제이팜일텐데 확인차 추출

                        # 공급사 추출
                        ggs_ls = prod_soup.find_all('td', attrs={'id': 'choiceGoodsCd'})
                        ggs_name = ggs_ls[i].text
                        ggs_name = ggs_name.replace("\n", "")
                        ggs_name = ggs_name.replace("\r", "")
                        ggs_name = ggs_name.replace("\t", "")
                        ggs_name = ggs_name.replace(" ", "")

                        ggs_name1_ls.append(ggs_name)

                        # 공급가 추출
                        ggg_ls = prod_soup.find_all('td', attrs={'class': 'price'})
                        gg_price = ggg_ls[i].text
                        gg_price = gg_price.replace("\n", "")
                        gg_price = gg_price.replace("\r", "")
                        gg_price = gg_price.replace("\t", "")
                        gg_price = gg_price.replace(" ", "")

                        ggs_price1_ls.append(gg_price)

                        # 세일가
                        sale_ls = prod_soup.find_all('td', attrs={'class': 'sale'})

                        # 세일가가 있을 때
                        try:
                            # 세일가 추출
                            sale_price = sale_ls[i].text
                            sale_price = sale_price.replace("\n", "")
                            sale_price = sale_price.replace("\r", "")
                            sale_price = sale_price.replace("\t", "")
                            sale_price = sale_price.replace(" ", "")

                            ggs_sale_price1_ls.append(sale_price)

                            # 재고 - 세일가가 있을 때 순서 3 / 7 / 11
                            jg1 = prod_soup.find_all('td')[3].text
                            jg1 = jg1.replace("\n", "")
                            jg1 = jg1.replace("\r", "")
                            jg1 = jg1.replace("\t", "")
                            jg1 = jg1.replace(" ", "")

                            ggs_jg1_ls.append(jg1)

                        # 세일가가 없을 때
                        except:
                            # 세일가 추출
                            sale_price = ''
                            ggs_sale_price1_ls.append(sale_price)

                            # 재고 - 세일가가 없을 때 순서 2 / 5 / 8
                            jg1 = prod_soup.find_all('td')[2].text
                            jg1 = jg1.replace("\n", "")
                            jg1 = jg1.replace("\r", "")
                            jg1 = jg1.replace("\t", "")
                            jg1 = jg1.replace(" ", "")

                            ggs_jg1_ls.append(jg1)


                    # 최저가 2곳에 대한 정보 가져오기
                    else:
                        #상품 긁어오는게 끝날때(=에러날때)
                        try:
                            #공급가가 있을때
                            try :
                                # 공급사 추출
                                ggs_ls = prod_soup.find_all('td', attrs={'id': 'choiceGoodsCd'})
                                ggs_name = ggs_ls[i].text
                                ggs_name = ggs_name.replace("\n", "")
                                ggs_name = ggs_name.replace("\r", "")
                                ggs_name = ggs_name.replace("\t", "")
                                ggs_name = ggs_name.replace(" ", "")

                                # 공급가 추출
                                ggg_ls = prod_soup.find_all('td', attrs={'class': 'price'})
                                gg_price = ggg_ls[i].text
                                gg_price = gg_price.replace("\n", "")
                                gg_price = gg_price.replace("\r", "")
                                gg_price = gg_price.replace("\t", "")
                                gg_price = gg_price.replace(" ", "")

                                if i == 1:
                                    ggs_name2_ls.append(ggs_name)
                                    ggs_price2_ls.append(gg_price)
                                else:
                                    ggs_name3_ls.append(ggs_name)
                                    ggs_price3_ls.append(gg_price)

                                # 세일가
                                sale_ls = prod_soup.find_all('td', attrs={'class': 'sale'})

                                # 세일가가 있을 때
                                try:
                                    # 세일가 추출
                                    sale_price = sale_ls[i].text
                                    sale_price = sale_price.replace("\n", "")
                                    sale_price = sale_price.replace("\r", "")
                                    sale_price = sale_price.replace("\t", "")
                                    sale_price = sale_price.replace(" ", "")

                                    if i == 1:
                                        k = 7
                                    else:
                                        k = 11

                                    # 재고 - 세일가가 있을 때 순서 3 / 7 / 11
                                    jg2 = prod_soup.find_all('td')[k].text
                                    jg2 = jg2.replace("\n", "")
                                    jg2 = jg2.replace("\r", "")
                                    jg2 = jg2.replace("\t", "")
                                    jg2 = jg2.replace(" ", "")

                                    if i == 1:
                                        ggs_sale_price2_ls.append(sale_price)
                                        ggs_jg2_ls.append(jg2)
                                    else:
                                        ggs_sale_price3_ls.append(sale_price)
                                        ggs_jg3_ls.append(jg2)

                                # 세일가가 없을 때
                                except:
                                    # 세일가 추출
                                    sale_price = ''

                                    if i == 1:
                                        k = 5
                                    else:
                                        k = 8

                                    # 재고 - 세일가가 없을 때 순서 2 / 5 / 8
                                    jg2 = prod_soup.find_all('td')[k].text
                                    jg2 = jg2.replace("\n", "")
                                    jg2 = jg2.replace("\r", "")
                                    jg2 = jg2.replace("\t", "")
                                    jg2 = jg2.replace(" ", "")

                                    if i == 1:
                                        ggs_sale_price2_ls.append(sale_price)
                                        ggs_jg2_ls.append(jg2)
                                    else:
                                        ggs_sale_price3_ls.append(sale_price)
                                        ggs_jg3_ls.append(jg2)
                            except :
                                # 공급사 추출
                                ggs_name = ''

                                # 공급가 추출
                                gg_price = ''

                                if i == 1:
                                    ggs_name2_ls.append(ggs_name)
                                    ggs_price2_ls.append(gg_price)
                                else:
                                    ggs_name3_ls.append(ggs_name)
                                    ggs_price3_ls.append(gg_price)

                                # 세일가 추출
                                sale_price = ''

                                # 재고
                                jg2 = ''

                                if i == 1:
                                    ggs_sale_price2_ls.append(sale_price)
                                    ggs_jg2_ls.append(jg2)
                                else:
                                    ggs_sale_price3_ls.append(sale_price)
                                    ggs_jg3_ls.append(jg2)

                        except:
                            break



        # 만일 위에 코드가 오류가 뜬다면 -> 아마 페이지가 마지막 페이지까지 호출한 뒤 더이상 긁어올 페이지가 없어서 뜨는 오류라고 생각함.
        # 위 가정을 전제로 오류가 발생한다면 코드를 중지해라.
        except:
            print("에러는 " + str(j) + "번째 상품 입니다.")


# 티제이팜
tj = {
    'product_key':product_key_ls,
    'product_name':product_name_ls,
    'ggs_name':ggs_name1_ls,
    'ggs_price':ggs_price1_ls,
    'ggs_sale_price':ggs_sale_price1_ls,
    'ggs_jg':ggs_jg1_ls
}
# 최저가 1순위
r1 = {
    'product_key':product_key_ls,
    'product_name':product_name_ls,
    'ggs_name':ggs_name2_ls,
    'ggs_price':ggs_price2_ls,
    'ggs_sale_price':ggs_sale_price2_ls,
    'ggs_jg':ggs_jg2_ls,
}
# 최저가 2순위
r2 = {
    'product_key':product_key_ls,
    'product_name':product_name_ls,
    'ggs_name':ggs_name3_ls,
    'ggs_price':ggs_price3_ls,
    'ggs_sale_price':ggs_sale_price3_ls,
    'ggs_jg':ggs_jg3_ls
}

result_arr = []
result_arr.append(tj)
result_arr.append(r1)
result_arr.append(r2)

result = result_arr
print(json.dumps(result))

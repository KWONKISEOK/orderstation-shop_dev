import requests
from bs4 import BeautifulSoup
import time
import re
import json
import logging
import set_log
import sys

# sys.stdout = open('../python-admin/py/hmpmall_output.txt','w')

# 로그 함수 호출
set_log.get_logging()

#상품 몰 코드(마스터 코드)
master_code_ls=[]

# 상품명 상세
product_name_ls=[]

# 몰코드 키값
product_key_ls=[]

#상품 코드(erp 코드)
erp_code_ls=[]

# 공급사 이름
ggs_name1_ls=[]
ggs_name2_ls=[]
ggs_name3_ls=[]

# 공급가
ggs_price1_ls=[]
ggs_price2_ls=[]
ggs_price3_ls=[]

# 세일가
ggs_sale_price1_ls=[]
ggs_sale_price2_ls=[]
ggs_sale_price3_ls=[]

# 재고
ggs_jg1_ls=[]
ggs_jg2_ls=[]
ggs_jg3_ls=[]

# 상품명
product_name_ls2=[]

# 로그인할 유저정보를 넣어줍시다. (모두 문자열입니다!)
LOGIN_INFO = {
    'memberId': 'okpharm1',
    'memberPassword': 'okpharm1!@#',
    'mallDivCode': '1040001'
}

# Session 생성, with 구문 안에서 유지
with requests.Session() as s:
    
    
    # 로그인 프로세스
    # 1. 첫 페이지의 JSESSIONID 저장
    # 2. 첫 페이지의 JSESSIONID 값을 요청변수로 DWRSESSIONID 추출
    # 3. JSESSIONID 값을 변환
    # 4. 변환된 JSESSIONID 과 DWRSESSIONID 을 쿠키값으로 요청해야 로그인, 상품 정보등 요청 가능
    
    # 첫 로그인 페이지에 로그인 요청 -> 세션 값을 얻기 위해
    login_req = s.post('https://www.hmpmall.co.kr/login.do', data=LOGIN_INFO)
     
    # 로그인 성공했을 시    
    if login_req.status_code == 200 and login_req.ok:
        
        # 첫 페이지의 로그인 JSESSIONID 값 저장
        cookie_login=login_req.cookies.get_dict()
        cokk={'JSESSIONID': str(cookie_login['JSESSIONID'])}
        
        # DWRSESSIONID 값을 구하기 위한 요청
        dwrr_info ='''
callCount=1
c0-scriptName=__System
c0-methodName=generateId
c0-id=00
batchId=0
instanceId=0
page=%2Flogin.do
scriptSessionId=
'''

        # DWRSESSIONID 값 추출을 위한 호출
        dwrr_req = s.post('https://www.hmpmall.co.kr/dwr/call/plaincall/__System.generateId.dwr', data=dwrr_info, cookies=cokk)
        
        # DWRSESSIONID 값 저장
        dwr_text=dwrr_req.text
        dwrsession_1=re.findall('"(.*?)"',dwr_text)[-1]
        
        # JSESSIONID 값 변환을 위한 호출 정보
        jses_info='''
callCount=1
nextReverseAjaxIndex=0
c0-scriptName=common/Login
c0-methodName=execute
c0-id=0
c0-e1=string:
c0-e2=string:okpharm1
c0-e3=string:okpharm1!%40%23
c0-param0=Object_Object:{mallDivCode:reference:c0-e1, memberId:reference:c0-e2, memberPassword:reference:c0-e3}
batchId=1
instanceId=0
page=%2Flogin.do
scriptSessionId=''' + str(dwrsession_1) + '/c*5wvnn-qyC51ayc6'
        
        
        # 첫 페이지의 JSESSIONID 값 과 DWRSESSIONID 을 쿠키로 저장하여 요청
        cokk2 ={'JSESSIONID': str(cookie_login['JSESSIONID']),
                'DWRSESSIONID': str(dwrsession_1)}
        
        # JSESSIONID 값 변환을 위한 호출
        change_jess = s.post('https://www.hmpmall.co.kr/dwr/call/plaincall/common/Login.execute.dwr', data=jses_info, cookies=cokk2)
        
        # 변환된 JSESSIONID 값 저장
        final_cookie=change_jess.cookies.get_dict()
        final_jesession=final_cookie['JSESSIONID']

        # 변환된 JSESSIONID 값과 DWRSESSIONID 값을 쿠키로 저장
        final_cok={'JSESSIONID': str(final_jesession),
                   'DWRSESSIONID': str(dwrsession_1)}        
        
        
        ####################### 상품 정보 전처리 과정
        
        # 입력 변수 2개 이상일 수 있으니 for문 적용
        
        
        for i in range(1,158,1) :
            
            try :
                
                # 상품 관련 데이터 호출 정보                
                prod_list_data = {
                    'sellerId': '134',
                    'groupCategoryNumber': '2',
                    'skip': str(i),
                    'max': '20',
                    'makingId': 'productName',
                    'paramSellerId': '134',
                    'fromGubun': '2480000',
                    'orderFieldName': 'ORDER_QTY',
                    'orderType': 'DESC'
                }
                
                # 상품 정보 호출
                prod_list_req = requests.post('https://www.hmpmall.co.kr/search/searchTwoStepListData.do', params=prod_list_data, cookies=final_cok)
                
                # 상품 정보 json으로 변형
                product_list=json.loads(prod_list_req.text)
                
                for j in range (0,10,1) :
                    try :
                        #몰코드, 상품명 크롤링
                        master_code=product_list['productList'][j]['productMasterId']
                        product_name=product_list['productList'][j]['viewProductName']
                        master_code_ls.append(master_code)
                        product_name_ls.append(product_name)
                    
                    except :
                        break
            except :
                break
        
        for k in range (0,len(master_code_ls),1):
            
            prod_data = {'productMasterId': str(master_code_ls[k]),
                         'fromGubun': '2480000',
                         'orderByProductSeller': 'ORDERBY_BPCO_ID',
                         'orderbySellerId': '134',
                         'groupCategoryNumber': '2',
                         'imgPath': '/statics/imgs',
                         'mgPathX': '/statics/imgs'}
        
            # 상품 정보 호출
            prod_req = requests.post('https://www.hmpmall.co.kr/search/SearchProductSellerListJson.do', params=prod_data, cookies=final_cok)
            product_inform=json.loads(prod_req.text)
            
            product_key_ls.append(master_code_ls[k])
            
            product_name=product_inform['productBasicInfo']['productName']
            
            product_name_ls2.append(str(product_name))
            
            for q in range(0,3,1):
                
                if q == 0 :
                    
                    try :
                        erp_code=product_inform['sellerSaleProductList'][q]['sellerProductNumber']
                        ggs_name1=product_inform['sellerSaleProductList'][q]['sellerName']
                        ggs_price1=product_inform['sellerSaleProductList'][q]['productUnitPrice']
                        ggs_sale_price1=product_inform['sellerSaleProductList'][q]['discountPrice']
                        ggs_jg1=product_inform['sellerSaleProductList'][q]['stockQuantity']
                        
                    except :
                        erp_code=''
                        ggs_name1=''
                        ggs_price1=''
                        ggs_sale_price1=''
                        ggs_jg1=''
                    
                    erp_code_ls.append(erp_code)
                    ggs_name1_ls.append(ggs_name1)
                    ggs_price1_ls.append(ggs_price1)
                    ggs_sale_price1_ls.append(ggs_sale_price1)
                    ggs_jg1_ls.append(ggs_jg1)
                    

                else : 
                    
                    try :
                        ggs_name2=product_inform['sellerSaleProductList'][q]['sellerName']
                        ggs_price2=product_inform['sellerSaleProductList'][q]['productUnitPrice']
                        ggs_sale_price2=product_inform['sellerSaleProductList'][q]['discountPrice']
                        ggs_jg2=product_inform['sellerSaleProductList'][q]['stockQuantity']                   
                    
                    except :
                        ggs_name2=''
                        ggs_price2=''
                        ggs_sale_price2=''
                        ggs_jg2=''
                        
                    if q==1 :
                        ggs_name2_ls.append(ggs_name2)
                        ggs_price2_ls.append(ggs_price2)
                        ggs_sale_price2_ls.append(ggs_sale_price2)
                        ggs_jg2_ls.append(ggs_jg2)
                    else :
                        ggs_name3_ls.append(ggs_name2)
                        ggs_price3_ls.append(ggs_price2)
                        ggs_sale_price3_ls.append(ggs_sale_price2)
                        ggs_jg3_ls.append(ggs_jg2)
        

logging.debug("상품 정보 호출 완료")


import json
result_arr = {
    'MALL_CODE':master_code_ls,
    'ITMNO':erp_code_ls,
    'ITM_NM':product_name_ls,
    'ITM_NM2':product_name_ls2,
    'TJ_NM':ggs_name1_ls,
    'COMP_NM1':ggs_name2_ls,
    'COMP_NM2':ggs_name3_ls,
    'TJ_COST':ggs_price1_ls,
    'COMP_COST1':ggs_price2_ls,
    'COMP_COST2':ggs_price3_ls,
    'TJ_SALE_COST':ggs_sale_price1_ls,
    'COMP_SALE_COST1':ggs_sale_price2_ls,
    'COMP_SALE_COST2':ggs_sale_price3_ls,
    'TJ_STOCK':ggs_jg1_ls,
    'COMP_STOCK1':ggs_jg2_ls,
    'COMP_STOCK2':ggs_jg3_ls
    # 'key':product_key_ls
}

result = result_arr
print(json.dumps(result))
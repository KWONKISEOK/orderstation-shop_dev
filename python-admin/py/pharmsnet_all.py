import requests
from bs4 import BeautifulSoup
import time
import re
import json
import time
import set_log

# 로그 함수 호출
set_log.get_logging()

#상품 몰 코드(마스터 코드)
master_code_ls=[]

#상품명
product_name_ls=[]

# 공급사 이름
ggs_name1_ls = []
ggs_name2_ls = []
ggs_name3_ls = []

# 공급가
ggs_price1_ls = []
ggs_price2_ls = []
ggs_price3_ls = []

# 할인가
ggs_sale_price1_ls = []
ggs_sale_price2_ls = []
ggs_sale_price3_ls = []

# 재고
ggs_jg1_ls = []
ggs_jg2_ls = []
ggs_jg3_ls = []

# 단위
ggs_dw1_ls = []
ggs_dw2_ls = []
ggs_dw3_ls = []


with requests.Session() as s:

    # 첫 로그인 페이지에 로그인 요청 -> 세션 값을 얻기 위해
    login_req = s.post('http://www.pharmsnet.com/')
    
    if login_req.status_code == 200 and login_req.ok:
        
        # 첫 페이지의 로그인 JSESSIONID 값 저장
        cookie_login=login_req.cookies.get_dict()

        login_data1 ={'ID': 'supptest',
                      'PASSWD': 'dufma2020'}
                
        home_req = s.post('http://www.pharmsnet.com/web/include/jsp/loginCheck.jsp', data=login_data1, cookies=cookie_login)
        
        cokk={'JSESSIONID': str(cookie_login['JSESSIONID']),
              'PMSID': ''}
        home1_req = s.post('http://www.pharmsnet.com/web/main/jsp/b_main.jsp',cookies=cokk)
        
        
        page_data ={'RegiontypE': 'T',
                    'IssueflaG': 'Y',
                    'SugSetUp': 'Y',
                    'ExceptSupp': '51110,67482',
                    'ShapE': 'O',
                    'topSearchTypeMenu': 'itemname',
                    'PrctypE': 'T',
                    'supplierList': '51273'}
        
        page_req = s.post('http://www.pharmsnet.com/web/mall/jsp/b_mall.jsp',data=page_data, cookies=cokk)
        
        page_soup=BeautifulSoup(page_req.text,'html.parser')
        
        for i in range(0,80,1) :
            
            page_data ={'ShapE': 'O',
                        'RegiontypE': 'T',
                        'IssueflaG': 'Y',
                        'KeyfielD': 'searchitemname',
                        'PrctypE': 'T',
                        'SorT': 'rank',
                        'SupplieriD': '51273',
                        'FirstSearch': 'Y',
                        'ViewTypeFlag': '01',
                        'WitemFlag': 'N',
                        'MemberTypeFlag': '008',
                        'SupplyPriceIdx': '1',
                        'ExceptSupplierid': '51110,67482',
                        'BuyeridPHP': '44922',
                        'pg': str(i)}
            
            try :
                page_req = s.post('http://www.pharmsnet.com/web/mall/php/b_sangpummall_supplierid.php',data=page_data, cookies=cokk)
            
            except :
                print(i , "번째 페이지 오류")
                break 
                
            page_soup=BeautifulSoup(page_req.text,'html.parser')
            
            
            for j in range(0,20,1) :
                
                try :
                    a=page_soup.find_all('tr', attrs={'id': 'mallcell'+str(j+1)})[0]['onclick']
                    items = re.findall('\(([^)]+)', a)

                    #몰코드
                    prod_num=items[0].split(",")[0]

                    #상품명
                    prod_name=page_soup.find_all('div', attrs={'class': 'itemnameWrapDiv'})[j].text

                    master_code_ls.append(prod_num)
                    product_name_ls.append(prod_name)
                
                except :
                    break
                
        for mall_code in master_code_ls :
            
            product_list_data ={'itemid': str(mall_code),
                                'shape': 'O',
                                'f_supplierid': '51273'}
            
            product_list_req = s.post('http://www.pharmsnet.com/web/mall/jsp/b_mall_detail.jsp',data=product_list_data, cookies=cokk)
            
            product_list_soup=BeautifulSoup(product_list_req.text,'html.parser')
            
            for k in range(0,3,1):
                
                try : 
                    #공급사 명
                    ggs_name=product_list_soup.find_all('span', attrs={'id': re.compile("^suppLst_CompanyName")})[k].text

                    #공급가
                    gg_price=product_list_soup.find_all('tr',attrs={'class': 'dtsuptop'})[k].find_all('td')[2].text
                    gg_price=gg_price.replace("\n", "").replace("\r", "").replace("\t", "").replace(" ", "").replace("\xa0", "").replace('원','')

                    #할인가
                    sale_price=product_list_soup.find_all('tr',attrs={'class': 'dtsuptop'})[k].find_all('td')[3].text
                    sale_price=sale_price.replace("\n", "").replace("\r", "").replace("\t", "").replace(" ", "").replace("\xa0", "").replace('원','')

                    #재고
                    jg1=product_list_soup.find_all('tr',attrs={'class': 'dtsuptop'})[k].find_all('td')[4].text
                    jg1=jg1.replace("\n", "").replace("\r", "").replace("\t", "").replace(" ", "").replace("\xa0", "").replace('개','')

                    #주문단위
                    dw1=product_list_soup.find_all('tr',attrs={'class': 'dtsuptop'})[k].find_all('td')[5].text
                    dw1=dw1.replace("\n", "").replace("\r", "").replace("\t", "").replace(" ", "").replace('개','')
                
                except :
                    ggs_name=''
                    gg_price=''
                    sale_price=''
                    jg1=''
                    dw1=''

                if k == 0 :
                    ggs_name1_ls.append(ggs_name)                   

                    ggs_price1_ls.append(gg_price)
                    
                    ggs_sale_price1_ls.append(sale_price)
                    
                    ggs_jg1_ls.append(jg1)

                    ggs_dw1_ls.append(dw1)
                    
                elif k == 1 :
                    ggs_name2_ls.append(ggs_name)                   

                    ggs_price2_ls.append(gg_price)
                    
                    ggs_sale_price2_ls.append(sale_price)
                    
                    ggs_jg2_ls.append(jg1)

                    ggs_dw2_ls.append(dw1)
                else :
                    ggs_name3_ls.append(ggs_name)                   

                    ggs_price3_ls.append(gg_price)
                    
                    ggs_sale_price3_ls.append(sale_price)
                             
                    ggs_jg3_ls.append(jg1)

                    ggs_dw3_ls.append(dw1)

                    
import json
result_arr = {
    'MALL_CODE':master_code_ls,
    'ITM_NM':product_name_ls,
    'TJ_NM':ggs_name1_ls,
    'COMP_NM1':ggs_name2_ls,
    'COMP_NM2':ggs_name3_ls,
    'TJ_COST':ggs_price1_ls,
    'COMP_COST1':ggs_price2_ls,
    'COMP_COST2':ggs_price3_ls,
    'TJ_SALE_COST':ggs_sale_price1_ls,
    'COMP_SALE_COST1':ggs_sale_price2_ls,
    'COMP_SALE_COST2':ggs_sale_price3_ls,
    'TJ_STOCK':ggs_jg1_ls,
    'COMP_STOCK1':ggs_jg2_ls,
    'COMP_STOCK2':ggs_jg3_ls,
    'TJ_MEASURE':ggs_dw1_ls,
    'COMP_MEASURE1':ggs_dw2_ls,
    'COMP_MEASURE2':ggs_dw3_ls,
    # 'key':product_key_ls
}

result = result_arr
print(json.dumps(result))

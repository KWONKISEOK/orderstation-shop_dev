<?php
include_once('../common.php');
include_once('common/common.lib.php');
include_once ('../lib/tjpharm.lib.php');

$tsg_table = 'TSG_MALL_INFO';

$mall_type = $_GET['mall_type'];
$sca = $_GET['sca'];
$realtime_chk = $_GET['realtime_chk'];

if (empty($mall_type)) $mall_type = 'H';
if (empty($realtime_chk)) $realtime_chk = 'N';

switch ($mall_type) {
    case 'H':
        $mall_name = '한미몰';
        break;
    case 'D':
        $mall_name = '더샵';
        break;
    case 'P':
        $mall_name = '팜스넷';
        break;
}

$where = " where 1=1";
$where .= " and MALL_GB='$mall_type'";

if ($stx != '') {
    if ($sca == 'ITMNO') {
        $where .= " and ITMNO='$stx'";
    } else if ($sca == 'ITM_NM') {
        $where .= " and ITM_NM like '%$stx%'";
    }
}


$sql = "select * from {$tsg_table} {$where}";
$total_count = sql_num_rows(sql_query($sql, false, $datalake_db));

$rows = 5; // 페이지 row 설정
if ($page < 1) $page = 1;
$from_record = ($page - 1) * $rows; // 시작 열을 구함


?>

<table class="table" id="tsList">
    <thead>
    <tr style="background: #f3f3f3;">
        <!--<th rowspan="2" style="vertical-align: middle;">몰코드</th>-->
        <th rowspan="2">ERP코드</th>
        <th rowspan="2">상품명</th>
        <th rowspan="2">업체명</th>
        <th colspan="3"><?php echo $mall_name; ?></th>
        <th rowspan="2">실시간 조회</th>
    </tr>
    <tr style="background: #f3f3f3">
        <th>공급가</th>
        <th>세일가</th>
        <th>재고</th>
    </tr>
    </thead>
    <tbody id="lists">
    <?php
    $sql = "select * from {$tsg_table} {$where} limit $from_record, $rows";
    $result = sql_fetch_result($sql, $datalake_db);

    if ($realtime_chk == "Y") {
        foreach($result as $k => $row) {
            tsg_update($row['MALL_CODE'], $row['MALL_GB']);
        }

        $sql2 = "select * from {$tsg_table} {$where} limit $from_record, $rows";
        $result2 = sql_fetch_result($sql2, $datalake_db);
    } else {
        $result2 = $result;
    }

    $rowspan = 3;
    $rowspan_text = 'rowspan="'.$rowspan.'"';
    foreach($result2 as $k => $row) {

        $comp_nm1 = preg_replace('/\s+/', '', $row['COMP_NM1']);
        $comp_nm2 = preg_replace('/\s+/', '', $row['COMP_NM2']);

        ?>
        <tr data-ts="<?php echo $row["SEQ"]; ?>">
            <td <?php echo $rowspan_text; ?>><?php echo (!empty($row['ITMNO'])) ? $row['ITMNO'] : '-'; ?></td>
            <td class="product_name" <?php echo $rowspan_text; ?>><?php echo $row['ITM_NM']; ?></td>
            <td class="first"><?php echo $row['TJ_NM']; ?></td>
            <td class="first gg_price1"><?php echo tsg_number_format($row['TJ_COST']);?></td>
            <td class="first sale_price1"><?php echo tsg_number_format($row['TJ_SALE_COST']);?></td>
            <td class="first jg1"><?php echo tsg_number_format($row['TJ_STOCK']);?></td>
            <td <?php echo $rowspan_text; ?>>
                <button type="button" class="btn btn-primary ts-update-btn" style="margin-bottom: 10px;" onclick="ts_update('<?php echo $row["MALL_GB"]; ?>', '<?php echo $row["MALL_CODE"]; ?>', this);">조회</button>
                <br>(<span class="update_date"><?php echo $row['SYSIDATE'].' '.$row['SYSITIME']; ?></span>)
            </td>
        </tr>
        <tr data-ts="<?php echo $row["SEQ"]; ?>">
            <td><?php echo (!empty($comp_nm1)) ? $comp_nm1 : '-'; ?></td>
            <td class="gg_price2"><?php echo (!empty($comp_nm1)) ? tsg_number_format($row['COMP_COST1']) : '-';?></td>
            <td class="sale_price2"><?php echo (!empty($comp_nm1)) ? tsg_number_format($row['COMP_SALE_COST1']) : '-';?></td>
            <td class="jg2"><?php echo (!empty($comp_nm1)) ? tsg_number_format($row['COMP_STOCK1']) : '-';?></td>
        </tr>
        <tr data-ts="<?php echo $row["SEQ"]; ?>">
            <td><?php echo (!empty($comp_nm2)) ? $comp_nm2 : '-'; ?></td>
            <td class="gg_price3"><?php echo (!empty($comp_nm2)) ? tsg_number_format($row['COMP_COST2']) : '-';?></td>
            <td class="sale_price3"><?php echo (!empty($comp_nm2)) ? tsg_number_format($row['COMP_SALE_COST2']) : '-';?></td>
            <td class="jg3"><?php echo (!empty($comp_nm2)) ? tsg_number_format($row['COMP_STOCK2']) : '-';?></td>
        </tr>
        <?php $rowspan--;} ?>

    <?php if ($total_count == 0) { ?>
        <tr>
            <td colspan="7" style="font-weight: 700;padding: 50px;">검색결과가 없습니다.</td>
        </tr>
    <?php } ?>
    </tbody>
</table>

<script>
    $(function() {
        //$('.ts-update-btn').click();
    });
</script>
# -*- coding: utf-8 -*-

import json
import numpy as np
from bs4 import BeautifulSoup
import requests
from urllib.request import urlopen
import pandas as pd
from urllib import parse
import sys
# import io
# sys.stdout = io.TextIOWrapper(sys.stdout.detach(), encoding = 'utf-8')
# sys.stderr = io.TextIOWrapper(sys.stderr.detach(), encoding = 'utf-8')

# 검색어 지정
input_word = sys.argv[1] # 검색어
sort_word = sys.argv[2] # 정렬 옵션

# 크롤링 할 URL 세팅
url_1 = 'https://search.shopping.naver.com/search/all?frm=NVSHATC&origQuery='
url_2 = '&pagingIndex=1&pagingSize=80&productSet=total&query='
url_3 = '&sort='
url_4 = '&timestamp=&viewType=list'

# 한글 단어 html 인코딩
keyword=parse.quote(input_word)

# 크롤링 할 full URL 지정
url=url_1 + keyword + url_2 + keyword + url_3 + sort_word + url_4

# 해당 URL 구성 코드 가져오기 및 json 코드 추출
html = requests.get(url).text
soup = BeautifulSoup(html, 'lxml')
ret = requests.get(url)
page = BeautifulSoup(ret.text, 'html.parser')
jsn = page.find_all('script', {"type":'application/json'})
mainscript = soup.find("script", {"id": "__NEXT_DATA__"}).string
mainscript = mainscript.encode('utf-8')
jsn = json.loads(mainscript.decode('utf-8'))

# 추출 자료 파싱
a=jsn['props']['pageProps']['initialState']['products']['list']


# 상품명 리스트
product_list=[]

for i in range(0,len(jsn['props']['pageProps']['initialState']['products']['list']),1):
    try :
        k=a[i]['item']['productTitle']
    except :
        k=a[i]['item']['productName']

    product_list.append(k)


# 판매처 리스트
mall_name=[]

for i in range(0,len(jsn['props']['pageProps']['initialState']['products']['list']),1):
    try :
        k=a[i]['item']['mallName']
    except :
        try :
            k=a[i]['item']['lowMallList'][0]['chnlName'] + "- 최저가"
        except :
            k=a[i]['item']['lowMallList'][0]['name'] + "- 최저가"

    mall_name.append(k)


# 판매 상품 개수 관련 추가 정보
product_count_info=[]

for i in range(0,len(jsn['props']['pageProps']['initialState']['products']['list']),1):
    try :
        k=a[i]['item']['characterValue']
    except :
        k=""

    product_count_info.append(k)


# 상품 가격
product_price=[]

for i in range(0,len(jsn['props']['pageProps']['initialState']['products']['list']),1):
    try :
        k=a[i]['item']['price']
    except :
        k="X"

    product_price.append(k)

# 배송 가격
delivery_price=[]

for i in range(0,len(jsn['props']['pageProps']['initialState']['products']['list']),1):
    try :
        k=a[i]['item']['deliveryFeeContent']
    except :
        k='노출 X'

    delivery_price.append(k)

# 리뷰 수
review_cnt=[]

for i in range(0,len(jsn['props']['pageProps']['initialState']['products']['list']),1):
    try :
        k=a[i]['item']['reviewCount']
    except :
        k=0

    review_cnt.append(k)

# 구매 수
purchaseCnt=[]

for i in range(0,len(jsn['props']['pageProps']['initialState']['products']['list']),1):
    try :
        k=a[i]['item']['purchaseCnt']
    except :
        k=0

    purchaseCnt.append(k)

# 등록일
openDate=[]

for i in range(0,len(jsn['props']['pageProps']['initialState']['products']['list']),1):
    try :
        k=a[i]['item']['openDate']
    except :
        k=0

    openDate.append(k)

# 상품링크 - 광고
adcrUrl=[]

for i in range(0,len(jsn['props']['pageProps']['initialState']['products']['list']),1):
    try :
        k=a[i]['item']['adcrUrl']
    except :
        k=''

    adcrUrl.append(k)

# 상품링크 - 일반
crUrl=[]

for i in range(0,len(jsn['props']['pageProps']['initialState']['products']['list']),1):
    try :
        k=a[i]['item']['crUrl']
    except :
        k=''

    crUrl.append(k)

# 추출 (JSON)
result = {
    'product_list':product_list,
    'mall_name':mall_name,
    'product_count_info':product_count_info,
    'product_price':product_price,
    'delivery_price':delivery_price,
    'review_cnt':review_cnt,
    'purchaseCnt':purchaseCnt,
    'openDate':openDate,
    'adcrUrl':adcrUrl,
    'crUrl':crUrl
    }
print(json.dumps(result))

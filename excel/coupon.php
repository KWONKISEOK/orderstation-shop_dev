<?php

include_once('../common.php');
include_once('../lib/PHPExcel.php');

if ($_FILES) {
    $_table = "{$g5['g5_shop_coupon_table']}";

    $objPHPExcel = new PHPExcel();

    // 엑셀 데이터를 담을 배열을 선언한다.
    $allData = array();
    $itemData = array();

    // 파일의 저장형식이 utf-8일 경우 한글파일 이름은 깨지므로 euc-kr로 변환
    $filename = iconv("UTF-8", "EUC-KR", $_FILES['excelFile']['tmp_name']);

    try {

        // 업로드한 PHP 파일 로드
        $objPHPExcel = PHPExcel_IOFactory::load($filename);

        $extension = strtoupper(pathinfo($filename, PATHINFO_EXTENSION));
        $sheetsCount = $objPHPExcel->getSheetCount();

        // 시트Sheet별로 읽기
        for ($sheet = 0; $sheet < $sheetsCount; $sheet++) {

            $objPHPExcel->setActiveSheetIndex($sheet);
            $activesheet = $objPHPExcel->getActiveSheet(); // 현재 시트
            $highestRow = $activesheet->getHighestRow(); // 마지막 행
            $highestColumn = $activesheet->getHighestColumn(); // 마지막 컬럼

            // 한줄읽기
            for ($row = 2; $row <= $highestRow; $row++) {
                $range_str = "A" . $row . ":" . $highestColumn . $row;
                $rowData = $activesheet->rangeToArray($range_str, NULL, TRUE, FALSE); // $rowData가 한줄의 데이터를 셀별로 배열처리 된다.

                foreach ($rowData as $k => $v) {
                    $cursor = 0;

                    $itemData = array(
                        "mb_id" => $v[$cursor]
                    );
                }

                if ($itemData['mb_id'])
                    $allData[$row] = $itemData;
            }
        }
    } catch (exception $exception) {
        echo $exception;
    }

    // 배열 인덱스 정렬
    $cnt = 0;
    foreach($allData as $key=>$val)
    {
        unset($allData[$key]);
        $new_key = $cnt;
        $allData[$new_key] = $val;
        $cnt++;
    }

    if (count($allData) > 0) {
        /*************** 트랜잭션 관련 ****************/
        $error_cnt = 0;
        mysqli_autocommit($g5['connect_db'], false);
        /*************** 트랜잭션 관련 ****************/

        foreach ($allData as $key => $value) {

            $cp_id = get_coupon_id();

            if (!empty($value['mb_id'])) {

                $sql = " select cp_id from $_table where cp_id='$cp_id' limit 1";
                $chk = sql_fetch($sql);
                
                if ($chk['cp_id'] != '') {
                    $cp_id = get_coupon_id();
                }

                $sql = " INSERT INTO $_table
                    ( cp_id, cp_subject, cp_method, mb_id, cp_start, cp_end, cp_type, cp_price, cp_trunc, cp_minimum, cp_maximum, cp_datetime)
                    VALUES
                    ( '$cp_id', '웰컴백! 첫구매 응원 쿠폰', '2', '$value[mb_id]', '2021-03-02', '2021-08-31', '0', '5000', '1', '30000', '0', '".G5_TIME_YMDHIS."') ";

                sql_query($sql);

                /*************** 트랜잭션 관련 ****************/
                if( mysqli_errno($g5['connect_db']) ){
                    $error_cnt += 1;
                }
                /*************** 트랜잭션 관련 ****************/
            }
        }

        /*************** 트랜잭션 관련 ****************/
        if ($error_cnt > 0) {
            mysqli_rollback($g5['connect_db']);
            mysqli_close($g5['connect_db']);
            echo "
            <script>
                alert('데이터베이스의 에러로 인해 롤백되었습니다.');
                history.back();
            </script>
            ";
            exit;
        } else {
            mysqli_commit($g5['connect_db']);
        }
        /*************** 트랜잭션 관련 ****************/
    }

    unset($_FILES);
    unset($_POST);
}


?>

<h3>쿠폰 주기</h3>

<form action="" method="post" enctype="multipart/form-data">
    <input type="hidden" name="t1" value="check">
    <input type="file" name="excelFile" value="">
    <input type="submit" id="upload" value="파일 첨부">
</form>

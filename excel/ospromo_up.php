<?php

include_once('../common.php');
include_once('../lib/PHPExcel.php');

if ($_FILES) {
    $_table = "point_member_temp_new";
    $expire_date = "2021-06-30";

    $objPHPExcel = new PHPExcel();

    // 엑셀 데이터를 담을 배열을 선언한다.
    $allData = array();
    $itemData = array();

    // 파일의 저장형식이 utf-8일 경우 한글파일 이름은 깨지므로 euc-kr로 변환
    $filename = iconv("UTF-8", "EUC-KR", $_FILES['excelFile']['tmp_name']);

    try {

        // 업로드한 PHP 파일 로드
        $objPHPExcel = PHPExcel_IOFactory::load($filename);

        $extension = strtoupper(pathinfo($filename, PATHINFO_EXTENSION));
        $sheetsCount = $objPHPExcel->getSheetCount();

        // 시트Sheet별로 읽기
        for ($sheet = 0; $sheet < $sheetsCount; $sheet++) {

            $objPHPExcel->setActiveSheetIndex($sheet);
            $activesheet = $objPHPExcel->getActiveSheet(); // 현재 시트
            $highestRow = $activesheet->getHighestRow(); // 마지막 행
            $highestColumn = $activesheet->getHighestColumn(); // 마지막 컬럼

            // 한줄읽기
            for ($row = 2; $row <= $highestRow; $row++) {
                $range_str = "A" . $row . ":" . $highestColumn . $row;
                $rowData = $activesheet->rangeToArray($range_str, NULL, TRUE, FALSE); // $rowData가 한줄의 데이터를 셀별로 배열처리 된다.

                foreach ($rowData as $k => $v) {
                    $cursor = 0;
                    $_target = $v[$cursor++]; // 약국 코드
                    $recomm = sql_fetch("select mb_id, mb_email from tbl_member where pharm_custno='" . $_target . "'");

                    $itemData = array(
                        "opt_1" => $recomm['mb_id'], // 약국 ID
                        "opt_2" => $v[$cursor++], // 약국 이름
                        "opt_3" => $v[$cursor++], // 대표자
                        "opt_4" => PHPExcel_Style_NumberFormat::toFormattedString($v[$cursor++], PHPExcel_Style_NumberFormat::FORMAT_DATE_YYYYMMDD2), // 포인트 유효기간 현재는 사용안함
                        "opt_5" => $recomm['mb_email'], // 이메일
                        "opt_6" => $v[$cursor++], // 포인트
                        "opt_7" => $v[$cursor++], // 프로모션 생성 ID
                        "opt_8" => $v[$cursor++], // 프로모션 생성 PW
                    );
                }

                $allData[$row] = $itemData;
            }
        }
    } catch (exception $exception) {
        echo $exception;
    }
//    debug2($allData); exit;

    if (count($allData) > 0) {
        // TABLE 초기화
        sql_query('truncate table ' . $_table);

        foreach ($allData as $key => $value) {
            $sql = "insert into " . $_table . " (opt_1, opt_2, opt_3, opt_4, opt_5, opt_6, opt_7, opt_8) 
                    value ('$value[opt_1]', '$value[opt_2]', '$value[opt_3]', '$value[opt_4]', '$value[opt_5]', '$value[opt_6]', '$value[opt_7]', '$value[opt_8]')
                  ";
            sql_query($sql);
        }

        $sql = "select * from " . $_table;
        $result = sql_query($sql);
        while ($row = sql_fetch_array($result)) {
            // 기존 회원인지 신규 회원인지 체크 후 insert update 구분하도록 한다.
            $sql = "SELECT mb_id, mb_point FROM tbl_member WHERE mb_id='$row[opt_7]'";
            $id_chk = sql_fetch($sql);

            $point = $row[opt_6];
            if ($id_chk == true) {
                $res = true;
                $mb_point = $point + $id_chk['mb_point'];
            } else {
                $sql = "INSERT INTO tbl_member(mb_id, mb_password, mb_name, mb_nick, mb_nick_date, mb_email, mb_level, mb_type, mb_recommend, mb_datetime, mb_point) 
                    SELECT 
                       opt_7, PASSWORD(opt_8), opt_3, opt_3, '0000-00-00', opt_5, 1, 0, opt_1, NOW(), opt_6 ELSE 0 END 
                    FROM " . $_table . " WHERE opt_7='$row[opt_7]';";
                $res = sql_query($sql);
                $mb_point = $point;
            }

            if ($res == true && intval($row['opt_6']) > 0) {
                $sql = "INSERT INTO tbl_point(mb_id , po_datetime , po_content , po_point , po_use_point , po_expired , po_expire_date , po_mb_point )
                    SELECT mb_id , NOW() , 'OS 약사 프로모션포인트' , '$point' , 0 , 0 , '$expire_date' , '$mb_point' FROM tbl_member WHERE mb_id='" . $row[opt_7] . "'";
                sql_query($sql);
                echo $sql.'<br>';
            } else {
                echo "error ".$row[opt_7];
                exit;
            }
        }
    }

    unset($_FILES);
    unset($_POST);
}


?>

<form action="" method="post" enctype="multipart/form-data">
    <input type="hidden" name="t1" value="check">
    <input type="file" name="excelFile" value="">
    <input type="submit" id="upload" value="파일 첨부">
</form>

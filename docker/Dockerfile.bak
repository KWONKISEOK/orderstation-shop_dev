FROM php:5.6.40-apache

# 환경 설정
ENV APACHE_DOCUMENT_ROOT /var/www/html
ENV TZ Asia/Seoul
ENV LANG C.UTF-8

# 새로운 패키지 버전 확인
RUN apt-get update --fix-missing
RUN apt-get upgrade -y

# 패키지 설치
RUN docker-php-ext-install mysqli
RUN docker-php-ext-install pdo pdo_mysql
RUN docker-php-ext-install sockets
RUN apt-get update && \
    apt-get install -y libfreetype6-dev libjpeg-dev && \
    docker-php-ext-install mbstring && \
    docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ && \
    docker-php-ext-install gd
RUN apt-get update && apt-get install -y \
    zlib1g-dev \
    libzip-dev \
    libpng-dev \
    libxpm-dev \
    libwebp-dev
RUN apt-get install -y libghc-iconv-dev
RUN apt-get install -y libssl-dev openssl
RUN apt-get install -y libmcrypt-dev
RUN apt-get install -y vim
RUN docker-php-ext-install zip
RUN docker-php-ext-install mcrypt

# python3 설치 및 패키지 설치
RUN apt-get install -y python3 python3-pip python3-dev build-essential
RUN pip3 install numpy && \
    pip3 install BeautifulSoup4 && \
    pip3 install bs4 requests && \
    pip3 install pandas && \
    pip3 install urllib3 && \
    pip3 install parse && \
    pip3 install html5lib && \
    pip3 install lxml

# 패키지 설치시 사용된 임시 파일들 삭제
RUN apt-get clean \
  && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

#COPY php/php.ini /usr/local/etc/php/php.ini
#ADD 000-default.conf /etc/apache2/sites-enabled/000-default.conf

# 서버 character set 변경
RUN apt-get update
RUN apt-get install -y locales
RUN sed -i '/ko_KR/s/^# //g' /etc/locale.gen && \
    locale-gen
ENV LANG ko_KR.UTF-8
#ENV LANGUAGE ko_KR:ko
#ENV LC_ALL ko_KR.UTF-8

# 아파치 모듈 활성화(rewirte, headers)
RUN a2enmod rewrite
RUN a2enmod headers
RUN a2enmod ssl

# 아파치 데몬 재시작(현재 연결 유지)
RUN apache2ctl -k graceful

# 컨테이너 실행시 실행될 명령
#CMD ["/bin/bash"]

#EXPOSE 80
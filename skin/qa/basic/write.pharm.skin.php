<?php
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가

// add_stylesheet('css 구문', 출력순서); 숫자가 작을 수록 먼저 출력됨
add_javascript(G5_POSTCODE_JS, 0);  
add_stylesheet('<link rel="stylesheet" href="'.$qa_skin_url.'/style.css">', 0);
?>
<style>
a.btn_cancel {
	padding: 0; 
}
</style>
<section id="bo_w">
    <h2>1:1문의 작성</h2>
    <!-- 게시물 작성/수정 시작 { -->
    <form name="fwrite" id="fwrite" action="<?php echo $action_url ?>" onsubmit="return fwrite_submit(this);" method="post" enctype="multipart/form-data" autocomplete="off">
    <input type="hidden" name="w" value="<?php echo $w ?>">
    <input type="hidden" name="qa_id" value="<?php echo $qa_id ?>">
    <input type="hidden" name="sca" value="<?php echo $sca ?>">
    <input type="hidden" name="stx" value="<?php echo $stx ?>">
    <input type="hidden" name="page" value="<?php echo $page ?>">
    <?php
    $option = '';
    $option_hidden = '';
    $option = '';

    if ($is_dhtml_editor) {
        $option_hidden .= '<input type="hidden" name="qa_html" value="1">';
    } else {
        $option .= "\n".'<input type="checkbox" id="qa_html" name="qa_html" onclick="html_auto_br(this);" value="'.$html_value.'" '.$html_checked.'>'."\n".'<label for="qa_html">html</label>';
    }

    echo $option_hidden;
    ?>
    <div class="form_01">
        <ul>
            

            <li class="bo_w_sbj" style="width:630px;float:left; margin-left:290px;">
                <label for="qa_subject" class="sound_only">약국명<strong class="sound_only" >필수</strong></label>
                
                    <input type="text" name="qa_1" value="<?php echo get_text($write['qa_1']); ?>" id="qa_1" required class="frm_input full_input required" size="50" maxlength="255" placeholder="약국명" style="width:100%">
                
            </li>
			
			<li class="bo_w_sbj" style="width:630px;float:left; margin-left:290px;">
                <label for="qa_subject" class="sound_only">약사님 성함<strong class="sound_only">필수</strong></label>
                
                    <input type="text" name="qa_2" value="<?php echo get_text($write['qa_2']); ?>" id="qa_2" required class="frm_input full_input required" size="50" maxlength="255" placeholder="약사님 성함" style="width:100%">
                
            </li>
			
			<li class="bo_w_sbj" style="width:630px;float:left; margin-left:290px;">
                <label for="qa_subject" class="sound_only">전화번호<strong class="sound_only">필수</strong></label>
                <input type="text" name="qa_3" value="<?php echo get_text($write['qa_3']); ?>" id="qa_3" required class="frm_input full_input required" size="50" maxlength="11" placeholder="전화번호 ( - 없이 숫자만 입력해주세요. )" onkeypress="onlyNumber();" style="width:100%">
                
            </li>
			
			<li class="bo_w_sbj" style="width:630px;float:left; margin-left:290px;">
                <label for="qa_subject" class="sound_only">약국 주소<strong class="sound_only">필수</strong></label>
                
                    <input type="text" name="qa_4" value="<?php echo get_text($write['qa_4']); ?>" id="qa_4" required class="frm_input full_input required" size="50" maxlength="255" placeholder="약국주소" style="width:100%">
                
            </li>
			
			<li class="bo_w_sbj" style="width:630px;float:left; margin-left:290px;">
                <label for="qa_subject" class="sound_only">약국 연락처</label>
                <input type="text" name="qa_5" value="<?php echo get_text($write['qa_5']); ?>" id="qa_5" class="frm_input full_input" size="50" maxlength="15" placeholder="약국 연락처 ( - 없이 숫자만 입력해주세요. )" onkeypress="onlyNumber();" style="width:100%">
                
            </li>
			
			<li class="bo_w_sbj" style="width:630px;float:left; margin-left:290px;">
                <label for="qa_subject" class="sound_only">메일 주소</label>
                    <input type="text" name="qa_subject" value="<?php echo get_text($write['qa_subject']); ?>" id="qa_subject" class="frm_input full_input" size="50" maxlength="255" placeholder="메일주소" style="width:100%">
              
            </li>

            <?php if ($option) { ?>
            <li>
                옵션
                <?php echo $option; ?>
            </li>
            <?php } ?>
			<li class="bo_w_flie" style="width:630px;float:left; margin-left:290px;">
			<div>
			첨부파일 1
			</div>
			</li>
			
            <li class="bo_w_flie" style="width:630px;float:left; margin-left:290px;" >
                <div class="file_wr">
                    <label for="bf_file_1" class="lb_icon"><i class="fa fa-download" aria-hidden="true"></i><span class="sound_only"> 파일 #1</span></label>
                    <input type="file" name="bf_file[1]" id="bf_file_1" title="파일첨부 1 :  용량 <?php echo $upload_max_filesize; ?> 이하만 업로드 가능" class="frm_file">
                    <?php if($w == 'u' && $write['qa_file1']) { ?>
                    <input type="checkbox" id="bf_file_del1" name="bf_file_del[1]" value="1"> <label for="bf_file_del1"><?php echo $write['qa_source1']; ?> 파일 삭제</label>
                    <?php } ?>
                </div>
            </li>
			<li class="bo_w_flie" style="width:630px;float:left; margin-left:290px;">
			<div>
			첨부파일 2
			</div>
			</li>
            <li class="bo_w_flie" style="width:630px;float:left; margin-left:290px;">
                <div class="file_wr">
                    <label for="bf_file_2" class="lb_icon"><i class="fa fa-download" aria-hidden="true"></i><span class="sound_only"> 파일 #2</span></label>
                    <input type="file" name="bf_file[2]" id="bf_file_2" title="파일첨부 2 :  용량 <?php echo $upload_max_filesize; ?> 이하만 업로드 가능" class="frm_file">
                    <?php if($w == 'u' && $write['qa_file2']) { ?>
                    <input type="checkbox" id="bf_file_del2" name="bf_file_del[2]" value="1"> <label for="bf_file_del2"><?php echo $write['qa_source2']; ?> 파일 삭제</label>
                    <?php } ?>
                </div>
            </li>
        </ul>
    </div>
	

    <div class="btn_confirm" style="width:630px;float:left; margin-left:290px; margin-bottom:50px; margin-top:20px;">
        <button type="submit" value="작성완료" id="btn_submit" accesskey="s" class="btn_submit btn"><i class="fa fa-check" aria-hidden="true"></i> 작성완료 </button>

    </div>
    </form>


    <script>
	
   $("input[name=ad_sel_addr]").on("click", function() {
        var addr = $(this).val().split(String.fromCharCode(30));

        if (addr[0] == "same") {
            gumae2baesong();
        } else {
            if(addr[0] == "new") {
                for(i=0; i<10; i++) {
                    addr[i] = "";
                }
            }

            var f = document.forderform;
            f.od_b_name.value        = addr[0];
            //f.od_b_tel.value         = addr[1];
            f.od_b_hp.value          = addr[2];
            f.od_b_zip.value         = addr[3] + addr[4];
            f.od_b_addr1.value       = addr[5];
            f.od_b_addr2.value       = addr[6];
            f.od_b_addr3.value       = addr[7];
            f.od_b_addr_jibeon.value = addr[8];
            f.ad_subject.value       = addr[9];

            var zip1 = addr[3].replace(/[^0-9]/g, "");
            var zip2 = addr[4].replace(/[^0-9]/g, "");

            var code = String(zip1) + String(zip2);

            if(zipcode != code) {
                calculate_sendcost(code);
            }
        }
    });
	
	/* 추가부분 */
   $('.content').hide();
   
	
	$(function(){
     $('.content2').click(function(){
       $('.content').show();
	    $('.content2').hide();
     });
   });
   
   $(function(){
     $('.content').click(function(){
       $('.content').hide();
	    $('.content2').show();
     });
   });
   
	
	// 1. 숫자만 입력받게 하는 방법
    function onlyNumber() {
        if ((event.keyCode < 48) || (event.keyCode > 57))
            event.returnValue = false;
    }

    function html_auto_br(obj)
    {
        if (obj.checked) {
            result = confirm("자동 줄바꿈을 하시겠습니까?\n\n자동 줄바꿈은 게시물 내용중 줄바뀐 곳을<br>태그로 변환하는 기능입니다.");
            if (result)
                obj.value = "2";
            else
                obj.value = "1";
        }
        else
            obj.value = "";
    }

    function fwrite_submit(f)
    {
        <?php echo $editor_js; // 에디터 사용시 자바스크립트에서 내용을 폼필드로 넣어주며 내용이 입력되었는지 검사함   ?>

        var subject = "";
        var content = "";
        $.ajax({
            url: g5_bbs_url+"/ajax.filter.php",
            type: "POST",
            data: {
                "subject": f.qa_subject.value,
                "content": f.qa_content.value
            },
            dataType: "json",
            async: false,
            cache: false,
            success: function(data, textStatus) {
                subject = data.subject;
                content = data.content;
            }
        });

        if (subject) {
            alert("제목에 금지단어('"+subject+"')가 포함되어있습니다");
            f.qa_subject.focus();
            return false;
        }

        if (content) {
            alert("내용에 금지단어('"+content+"')가 포함되어있습니다");
            if (typeof(ed_qa_content) != "undefined")
                ed_qa_content.returnFalse();
            else
                f.qa_content.focus();
            return false;
        }

        <?php if ($is_hp) { ?>
        var hp = f.qa_hp.value.replace(/[0-9\-]/g, "");
        if(hp.length > 0) {
            alert("휴대폰번호는 숫자, - 으로만 입력해 주십시오.");
            return false;
        }
        <?php } ?>

        document.getElementById("btn_submit").disabled = "disabled";

        return true;
    }
    </script>
</section>
<!-- } 게시물 작성/수정 끝 -->
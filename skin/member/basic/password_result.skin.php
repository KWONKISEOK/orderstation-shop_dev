<?php
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가

// add_stylesheet('css 구문', 출력순서); 숫자가 작을 수록 먼저 출력됨
add_stylesheet('<link rel="stylesheet" href="'.$member_skin_url.'/style.css">', 0);

// 토크 생성 
$token = md5(uniqid(rand(), true)); 
set_session("ss_token", $token); 

$mb_kind = $_POST['mb_kind'];
$mb_id = $_POST['mb_id'];

$mb_hp = $_POST['mb_hp1'].$_POST['mb_hp2'].$_POST['mb_hp3'];

?>
<style>

input[type=text], input[type=password] {
    height: 25px;
    line-height: 28px;
    padding: 2px 4px;
	width:120px;
    border: 1px solid #d5d5d5;
    color: #353535;
    font-size: 12px;
}
.btn_wt_s {
    vertical-align: middle;
    padding: 0 7px;
    border: 1px solid #787878;
    color: #333;
    line-height: 28px;
    background: #ffffff;
    background: -moz-linear-gradient(top, #ffffff 0%, #f3f3f3 100%);
    background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#ffffff), color-stop(100%,#f3f3f3));
    background: -webkit-linear-gradient(top, #ffffff 0%,#f3f3f3 100%);
    background: -o-linear-gradient(top, #ffffff 0%,#f3f3f3 100%);
    background: -ms-linear-gradient(top, #ffffff 0%,#f3f3f3 100%);
    background: linear-gradient(to bottom, #ffffff 0%,#f3f3f3 100%);
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#f3f3f3',GradientType=0 );
}
.btn-base {
    display: inline-block;
    border-radius: 3px;
    /*line-height: 300%;*/
    font-size: 12px;
}
.btn_org {
    border: 1px solid #db7a0c;
    color: #fff;
	cursor:pointer;
    background: #f08f21;
    background: -moz-linear-gradient(top, #f08f21 0%, #dc7a0b 100%);
    background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#f08f21), color-stop(100%,#dc7a0b));
    background: -webkit-linear-gradient(top, #f08f21 0%,#dc7a0b 100%);
    background: -o-linear-gradient(top, #f08f21 0%,#dc7a0b 100%);
    background: -ms-linear-gradient(top, #f08f21 0%,#dc7a0b 100%);
    background: linear-gradient(to bottom, #f08f21 0%,#dc7a0b 100%);
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f08f21', endColorstr='#dc7a0b',GradientType=0 );
}
a.btn_wt {
    border: 1px solid #adadad;
    color: #333;
    background: #ffffff;
    background: -moz-linear-gradient(top, #ffffff 0%, #f3f3f3 100%);
    background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#ffffff), color-stop(100%,#f3f3f3));
    background: -webkit-linear-gradient(top, #ffffff 0%,#f3f3f3 100%);
    background: -o-linear-gradient(top, #ffffff 0%,#f3f3f3 100%);
    background: -ms-linear-gradient(top, #ffffff 0%,#f3f3f3 100%);
    background: linear-gradient(to bottom, #ffffff 0%,#f3f3f3 100%);
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#f3f3f3',GradientType=0 );
}
.btn_w {
    text-align: center;
    margin-top: 8px;
}
.btn_w a {
    width: 30%;
}
.btn_submit {
	padding:10px;
}
a.btn_cancel {
padding:10px;
}

select {
    -webkit-appearance: none;
    -moz-appearance: none;
    appearance: none;
    font-size: 12px;

    border: #C8CACC 1px solid;
    padding: 6px 25px 6px 10px;
}
select, input, img, button {
    vertical-align: middle;
}

#check_msg, #hp_send_msg, #hp_certify_msg {
	color:#ff8800;
}
#box {
	margin:10px 0 10px 0;
	font-size:1.2em;
	text-align:center;
	padding:10px;
	 border-radius: 3px;
	 background: #fff;
}
.box-content {
	padding:6px;
	background: #fff;
    border-radius: 3px;
}

.box-content table tr {
	height: 50px;
}

.padding20 {
    padding: 20px;
}
</style>

<!-- 회원정보 찾기 시작 { -->
<div id="find_info" class="new_win">
    <h1 id="win_title"><i class="fa fa-newspaper-o" aria-hidden="true"></i> <?php echo $g5['title'] ?></h1>
    <div class="new_win_con">
        <form name="fpasswordlost" action="password_update.php" onsubmit="return form_submit(this);"  method="post" autocomplete="off">
		<input type="hidden" name="mb_id" value="<?php echo $mb_id;?>" />

			<?php if($mb_kind == 'id') {
			// ID찾기
			$sql =  "select mb_id from tbl_member where mb_name = '$mb_id' and replace(mb_hp,'-','')= '$mb_hp' limit 1";

			$row = sql_fetch($sql);
			$mb_id_find = $row['mb_id'] ;
			
			
			?>
			<div id="box">
				<h3 class="padding20"> <?php echo $mb_id;?>님의 ID는 <?php echo $mb_id_find; ?> 입니다.</h3>
			</div>
			
			<div style="overflow: hidden; margin-top: 15px;">
				<div style="text-align: center;">
					<!--<input type="button" onClick="javascript:location.href='<?php //echo G5_BBS_URL ?>/login.php';" value="회원 로그인" class="btn_submit" />-->
					<!--<button onclick="location.href='<?php echo G5_BBS_URL ?>/login.php';return false;" class="btn_submit">회원 로그인</button>-->
					<input type="button" onClick="javascript:self.close();" value="회원 로그인" class="btn_submit" />
				</div>
			</div>
			<?php } else { 
			
			
			
			?>

			<div id="box">
				<h3><?php echo $mb_id;?> 님의 비밀번호를 변경하세요.</h3>
			</div>
			
			<div class="box-content">
			<p style="text-align:center;">비밀번호는 6~20자의 최소 1개의 숫자, 영문자를 포함.</p>
			<table>
			<tr>
				<th scope="row">비밀번호</th>
				<td>
					<input type="password" name="mb_passwd1" id="mb_passwd1" maxlength="20" class="inputTypeText" />
				
				</td>
			</tr>
			<tr>
				<th scope="row">비밀번호 확인</th>
				<td>
					<input type="password" name="mb_passwd2" id="mb_passwd2" maxlength="20" class="inputTypeText" />
				</td>
			</tr>
			</table>
			</div>

			
		 

			<div style="overflow: hidden; margin-top: 15px;">
				<div style="float:left;">
					<!--<input type="button" onClick="javascript:location.href='<?php //echo G5_BBS_URL ?>/login.php';" value="회원 로그인" class="btn_submit" />-->
					<input type="button" onClick="javascript:self.close();" value="회원 로그인" class="btn_submit" />
				</div>
				<div style="float:right;">
					<input type="submit" value="비밀번호변경" class="btn_submit"> 
				</div>			
			</div>

			<?php }  ?>
		
    </form>

</div>

<script>

// submit 최종 폼체크
function form_submit(f)
{
		
	if(chkSpace(f.mb_passwd1.value)){
		alert("\n비밀번호를 입력해 주세요..");
		f.mb_passwd1.focus();
		return false;
	}
	if(chkSpace(f.mb_passwd2.value)){
		alert("\n비밀번호를 입력해 주세요..");
		f.mb_passwd2.focus();
		return false;
	}
	if(mb_passwd_check()==false) {
		f.mb_passwd1.focus();
		return false;
	}
	return true;
}
// 입력값이 NULL 인지 체크
function chkSpace(strValue){
	var flag=true;
	if (strValue!=""){
		for (var i=0; i < strValue.length; i++){
			if (strValue.charAt(i) != " "){
				flag=false;
				break;
			}
		}
	}
	return flag;
}
// 비밀번호
function mb_passwd_check()
{
	//valid check
	if ( document.fpasswordlost.mb_passwd1.value.length <= 0 )
	{
		alert("\n비밀번호를 입력하세요.");
		return false;
	}
	
	var isID = /^(?=.*[a-zA-Z])(?=.*\d).{6,20}$/;
	if( !isID.test(document.fpasswordlost.mb_passwd1.value) ) {
		alert("비밀번호는 6~20자의 최소 1개의 숫자, 영문자 를 포함해야 합니다."); 
		//str.focus();
		return false; 
	}
	
	if ( document.fpasswordlost.mb_passwd2.value.length <= 0 )
	{
		alert("\n비밀번호를 한번 더 입력하세요.");
		document.fpasswordlost.mb_passwd2.value="";
		return false;
	}

		if( document.fpasswordlost.mb_passwd1.value != document.fpasswordlost.mb_passwd2.value)
	{
		alert("\n비밀번호를 정확히 입력하세요.");
		document.fpasswordlost.mb_passwd1.value="";
		document.fpasswordlost.mb_passwd2.value="";
		return false;
	}

	//if ( _valid_length_check(2) ) return false;
	return true;
}

</script>

<?php







include_once(G5_PATH.'/tail.sub.php');
?>
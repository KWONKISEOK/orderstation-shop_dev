<!-- 2019.04.11 회원가입방법 버튼, 이미지 추가 -->

<?php
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가

// add_stylesheet('css 구문', 출력순서); 숫자가 작을 수록 먼저 출력됨
add_stylesheet('<link rel="stylesheet" href="'.$member_skin_url.'/style.css">', 0);
?>

<!-- 회원가입약관 동의 시작 { -->

<!-- <div> -->
<div>

    <?php
    // 소셜로그인 사용시 소셜로그인 버튼
    //@include_once(get_social_skin_path().'/social_register.skin.php');
    ?>

    <form  name="fregister" id="fregister" action="<?php echo $register_action_url ?>" onsubmit="return fregister_submit(this);" method="POST" autocomplete="off">
		<input type="hidden" name="provider" value="<?=$provider?>"/>

    <p>회원가입약관 및 개인정보처리방침안내의 내용에 동의하셔야 회원가입 하실 수 있습니다.</p>
    <div id="fregister_chkall">
        <label for="chk_all">전체선택</label>
        <input type="checkbox" name="chk_all"  value="1"  id="chk_all">

    </div>
    <section id="fregister_term">
        <h2><i class="fa fa-check-square-o" aria-hidden="true"></i> 회원가입약관&nbsp;&nbsp;&nbsp; <small ><span class="btnView">내용보기</span></small></h2>
        <textarea readonly id="stipulation" style="display:none;"><?php echo get_text($config['cf_stipulation']) ?></textarea>
        <fieldset class="fregister_agree">
            <label for="agree11">회원가입약관의 내용에 동의합니다.</label>
            <input type="checkbox" name="agree" value="1" id="agree11">
        </fieldset>
    </section>

    <section id="fregister_private">
        <h2><i class="fa fa-check-square-o" aria-hidden="true"></i> 개인정보처리방침안내</h2>
        <div>
            <table>
                <caption>개인정보처리방침안내</caption>
                <thead>
                <tr>
                    <th>목적</th>
                    <th>항목</th>
                    <th>보유기간</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>이용자 식별 및 본인여부 확인</td>
                    <td>아이디, 이름, 비밀번호</td>
                    <td>회원 탈퇴 시까지</td>
                </tr>
                <tr>
                    <td>고객서비스 이용에 관한 통지,<br>CS대응을 위한 이용자 식별</td>
                    <td>연락처 (이메일, 휴대전화번호)</td>
                    <td>회원 탈퇴 시까지</td>
                </tr>
                </tbody>
            </table>
        </div>

        <fieldset class="fregister_agree">
            <label for="agree21">개인정보처리방침안내의 내용에 동의합니다.</label>
            <input type="checkbox" name="agree2" value="1" id="agree21">
        </fieldset>
    </section>

<!--
    <div class="btn_confirm">
        <input type="submit" class="btn_submit" value="회원가입">
    </div>
-->

<!-- 추가(변경) -->
   <div class="btn_confirm">
	    <button type="button" class="content2" style="background-color: #0000A5; height:50px; width:131.99px;  border-radius:3px; color:white; font-size: 1.083em; font-weight: bold;">회원가입방법</button>
		&nbsp
		<input type="submit" class="btn_submit" value="회원가입">
    </div>
	<br>
    </form>
</div>

<div class="btn_confirm" >
	<div class="content" style="align:center !important; text-align:center !important; margin-right:40% !important;">
	<img src="../img/os_join.jpg" name='img1' id='img1'>
	<br><br><Br>
	<button type="button" class="btn btn-primary"; style="margin-left:94.5%;" >닫기</button>
	</div>
	<br>
</div>

    <script>
	/* 추가부분 */
   $('.content').hide();
   
   
   $(function(){
     $('.content').click(function(){
       $('.content').hide();
     });
   });
	
	$(function(){
     $('.content2').click(function(){
       $('.content').show();
     });
   });
   
	$(".btnView").click(function() {

		$("#stipulation").css( "display", "block");
	 });

    function fregister_submit(f)
    {
        if (!f.agree.checked) {
            alert("회원가입약관의 내용에 동의하셔야 회원가입 하실 수 있습니다.");
            f.agree.focus();
            return false;
        }

        if (!f.agree2.checked) {
            alert("개인정보처리방침안내의 내용에 동의하셔야 회원가입 하실 수 있습니다.");
            f.agree2.focus();
            return false;
        }

        return true;
    }
    
    jQuery(function($){
        // 모두선택
        $("input[name=chk_all]").click(function() {
            if ($(this).prop('checked')) {
                $("input[name^=agree]").prop('checked', true);
            } else {
                $("input[name^=agree]").prop("checked", false);
            }
        });
    });

    </script>
<!-- } 회원가입 약관 동의 끝 -->

<?php
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가

// add_stylesheet('css 구문', 출력순서); 숫자가 작을 수록 먼저 출력됨
add_stylesheet('<link rel="stylesheet" href="'.$member_skin_url.'/style.css">', 0);


?>
<style>
body {
    background: #e7e7e7;
}
#sns_login .sns-icon {
	width:260px;
}
#mb_login #login_info a {
	width:180px;
}

.mbskin h1 {
	margin: 10px 0 30px;
}
</style>

<!-- 로그인 시작 { -->
<div id="mb_login" class="mbskin">
	<br>
	<!--<a href="/shop/"><img src="/theme/onk/img/top_logo_new.jpg" width="200" alt="오엔케이" title=""></a>-->

    <h1>검사지 등록</h1>

    <form name="flogin" action="<?php echo $login_action_url ?>" onsubmit="return flogin_submit(this);" method="post">
    <input type="hidden" name="url" value="<?php echo $login_url ?>">

    <fieldset id="login_fs">
        <legend>회원로그인</legend>
        <label for="login_pw" class="sound_only">비밀번호<strong class="sound_only"> 필수</strong></label>
        <input type="password" name="mb_password" id="login_pw" required class="frm_input required" style="width:290px;height:30px;" size="20" maxLength="20" placeholder="비밀번호">
        
		<input type="submit" value="로그인" class="btn_submit" style="width:300px;">

    </fieldset>

    <div style="height:30px;">&nbsp;</div>

    </form>

</div>

<script>
$(function(){
    $("#login_auto_login").click(function(){
        if (this.checked) {
            this.checked = confirm("자동로그인을 사용하시면 다음부터 회원아이디와 비밀번호를 입력하실 필요가 없습니다.\n\n공공장소에서는 개인정보가 유출될 수 있으니 사용을 자제하여 주십시오.\n\n자동로그인을 사용하시겠습니까?");
        }
    });
});

function flogin_submit(f)
{

	//휴면계정체크
    return true;
}
</script>
<!-- } 로그인 끝 -->

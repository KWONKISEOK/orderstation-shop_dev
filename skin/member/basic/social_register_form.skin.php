<?php
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가
include_once('./_common.php');

// add_stylesheet('css 구문', 출력순서); 숫자가 작을 수록 먼저 출력됨
add_stylesheet('<link rel="stylesheet" href="'.$member_skin_url.'/style.css">', 0);

// 토크 생성 
$token = md5(uniqid(rand(), true)); 
set_session("ss_token", $token); 


//소셜가입페이지는 가입일때만 가능하다.
/*
if($w != '') {
	echo "잘못된접근입니다.";
	exit;
}
*/

if($w == '') {

	$user_email		= explode('@', $user_email);
	if (count($user_email) >= 1) {
		$user_email1 = $user_email[0];
		$user_email2 = $user_email[1];
	}

}

if($w == 'u') {

	$user_id    = $member['mb_id'];


	$mb_hp		= explode('-', $member['mb_hp']);
	if (count($mb_hp) >= 2) {
		$mb_hp1 = $mb_hp[0];
		$mb_hp2 = $mb_hp[1];
		$mb_hp3 = $mb_hp[2];
	}

	$user_email		= explode('@', $member['mb_email']);
	if (count($user_email) >= 1) {
		$user_email1 = $user_email[0];
		$user_email2 = $user_email[1];
	}

}

?>

<!-- 회원정보 입력/수정 시작 { -->

<script src="<?php echo G5_JS_URL ?>/jquery.register_form.js"></script>
<?php if($config['cf_cert_use'] && ($config['cf_cert_ipin'] || $config['cf_cert_hp'])) { ?>
<script src="<?php echo G5_JS_URL ?>/certify.js?v=<?php echo G5_JS_VER; ?>"></script>
<?php } ?>

<form id="fregisterform" name="fregisterform" action="<?php echo $register_action_url ?>" onsubmit="return fregisterform_submit(this, 0);" method="post" enctype="multipart/form-data" autocomplete="off">
<input type="hidden" name="w" value="<?php echo $w ?>">
<input type="hidden" name="url" value="<?php echo $urlencode ?>">
<input type="hidden" name="agree" value="<?php echo $agree ?>">
<input type="hidden" name="agree2" value="<?php echo $agree2 ?>">
<input type="hidden" name="cert_type" value="<?php echo $member['mb_certify']; ?>">
<input type="hidden" name="cert_no" value="">

 <div id="register_form"  class="form_01">   

<style>
#container {
width:1200px;
}

input[type=text], input[type=password] {
    height: 28px;
    line-height: 28px;
    padding: 2px 4px;
    border: 1px solid #d5d5d5;
    color: #353535;
    font-size: 12px;
}
/*
input[type='text'] input[type='password'] {
    margin: 3px 0px;
    padding: 2px;
    outline: 0px;
    border: 1px solid rgb(147, 149, 152);
    border-image: none;
    height: 24px;
   
    line-height: 15px;
    font-size: 13px;
  
}
*/

.btn_wt_s {
    vertical-align: middle;
    padding: 0 7px;
    border: 1px solid #787878;
    color: #333;
    line-height: 28px;
    background: #ffffff;
    background: -moz-linear-gradient(top, #ffffff 0%, #f3f3f3 100%);
    background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#ffffff), color-stop(100%,#f3f3f3));
    background: -webkit-linear-gradient(top, #ffffff 0%,#f3f3f3 100%);
    background: -o-linear-gradient(top, #ffffff 0%,#f3f3f3 100%);
    background: -ms-linear-gradient(top, #ffffff 0%,#f3f3f3 100%);
    background: linear-gradient(to bottom, #ffffff 0%,#f3f3f3 100%);
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#f3f3f3',GradientType=0 );
}
.btn-base {
    display: inline-block;
    border-radius: 3px;
    /*line-height: 300%;*/
    font-size: 12px;
}
.btn_org {
    border: 1px solid #db7a0c;
    color: #fff;
	cursor:pointer;
    background: #f08f21;
    background: -moz-linear-gradient(top, #f08f21 0%, #dc7a0b 100%);
    background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#f08f21), color-stop(100%,#dc7a0b));
    background: -webkit-linear-gradient(top, #f08f21 0%,#dc7a0b 100%);
    background: -o-linear-gradient(top, #f08f21 0%,#dc7a0b 100%);
    background: -ms-linear-gradient(top, #f08f21 0%,#dc7a0b 100%);
    background: linear-gradient(to bottom, #f08f21 0%,#dc7a0b 100%);
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f08f21', endColorstr='#dc7a0b',GradientType=0 );
}
a.btn_wt {
    border: 1px solid #adadad;
    color: #333;
    background: #ffffff;
    background: -moz-linear-gradient(top, #ffffff 0%, #f3f3f3 100%);
    background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#ffffff), color-stop(100%,#f3f3f3));
    background: -webkit-linear-gradient(top, #ffffff 0%,#f3f3f3 100%);
    background: -o-linear-gradient(top, #ffffff 0%,#f3f3f3 100%);
    background: -ms-linear-gradient(top, #ffffff 0%,#f3f3f3 100%);
    background: linear-gradient(to bottom, #ffffff 0%,#f3f3f3 100%);
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#f3f3f3',GradientType=0 );
}
.btn_w {
    text-align: center;
    margin-top: 8px;
}
.btn_w a {
    width: 30%;
}
.btn_submit {
	padding:10px;
}
a.btn_cancel {
padding:10px;
}


.medisearch .btn {
    text-align: center;
    width: 90%;
    color: #333;
    font-weight: bold;
}

.t_medi {
    width: 100%;
    border: 1px solid #b5a591;
    border-collapse: collapse;
    font-size: 12px;
}
.mt10 {
    margin-top: 10px;
}
.t_medi tr {
   height:30px;
}
.t_medi th {
    padding: 0;
    background: #faf7f3;
    border-right: 1px solid #e2dcd4;
    color: #333;
}
.t_medi td {
    padding: 10px;
    color: #555;
}

select {
    -webkit-appearance: none;
    -moz-appearance: none;
    appearance: none;
    font-size: 12px;
	height:28px;
    border: #C8CACC 1px solid;
    padding: 6px 25px 6px 10px;
}
select, input, img, button {
    vertical-align: middle;
}

#check_msg, #hp_send_msg, #hp_certify_msg {
	color:#ff8800;
}
</style>


				<?php if($w=='') { ?>
					<input type="hidden" id="mb_id_check" name="mb_id_check" value="N">
					<input type="hidden" id="mb_hp_check" name="mb_hp_check" value="N">
					<?php } else { ?>
					<input type="hidden" id="mb_id_check" name="mb_id_check" value="Y">
					<input type="hidden" id="mb_hp_check" name="mb_hp_check" value="Y">
				<?php } ?>

				<input type="hidden" name="mb_id" id="mb_id" value="<?=$user_id?>"  />
				<input type="hidden" id="mb_passwd1" name="mb_passwd1" value="">
				<input type="hidden" id="mb_passwd2" name="mb_passwd2" value="">
				<input type="hidden" id="mb_sex" name="mb_sex" value="">
				<input type="hidden" id="mb_birth1" name="mb_birth1" value="">
				<input type="hidden" id="mb_birth2" name="mb_birth2" value="">
				<input type="hidden" id="mb_birth3" name="mb_birth3" value="">
				<input type="hidden" name="social_regist" value="Y" /> 
				<input type="hidden" name="provider_name" value="<?=$provider_name?>" /> 

				<div class="ec-base-table typeWrite">					
					<h2>회원기본정보</h2>
					<table border="1" summary="">
					<caption>회원 기본정보</caption>
							<colgroup>
					<col style="width:150px;"/>
					<col style="width:auto;"/>
					</colgroup>
						<tr>
							<th scope="row">성명</th>
							<td>
								<p><input type="text" name="mb_name" id="mb_name" value="<?php echo $member['mb_name']; ?>"  class="inputTypeText" /></p>
							</td>
						</tr>
						<tr>
							<th scope="row">이메일</th>
							<td>
								<input type="text" name="mb_email1" id="mb_email1" class="inputTypeText" value="<?php echo $user_email1; ?>" /> @ <input type="text" name="mb_email2" id="mb_email2" class="inputTypeText" value="<?php echo $user_email2; ?>" />
								
									<select class="selc w60" name="mb_semail" id="mb_semail" onChange="fnc_semail1(this.value);">
										<option value="" selected>직접입력</option>
										<option value="naver.com">naver.com</option>
										<option value="daum.net">daum.net</option>
										<option value="nate.com">nate.com</option>
										<option value="hotmail.com">hotmail.com</option>
										<option value="yahoo.com">yahoo.com</option>
										<option value="empas.com">empas.com</option>
										<option value="korea.com">korea.com</option>
										<option value="dreamwiz.com">dreamwiz.com</option>
										<option value="gmail.com">gmail.com</option>
									</select>
									<br>
									※ 비밀번호 분실 시, 임시 비밀번호를 발급받기 위해 필요한 정보입니다. 정확하게 입력해 주세요.
							</td>
						</tr> 
						<tr>
							<th scope="row">휴대폰</th>
							<td>
								<?php if($w=='') { ?>
							
									<select name="mb_hp1" id="mb_hp1" class="inputTypeText" />
										<option value="">선택
										<option value="010" >010
										<option value="011" >011
										<option value="016" >016
										<option value="017" >017
										<option value="018" >018
										<option value="019" >019
									</select>
									<input type="text" name="mb_hp2" id="mb_hp2" maxlength="4"  class="inputTypeText"  />
									<input type="text" name="mb_hp3" id="mb_hp3" maxlength="4" class="inputTypeText" />
									<input type=button value='인증번호 전송' class="btn-base btn_wt_s" onclick="hp_certify(this.form);"> 
									<span id="hp_send_msg"></span>
								<br/>

									<input type="text" name="mb_hp_certify" id="mb_hp_certify" value=""  class="inputTypeText" />
									<input type=button value='확인' class="btn-base btn_wt_s" onclick="hp_certify_check(this.form);"> 
									<span id="hp_certify_msg"></span>
							
								<?php } else { ?>

									<select name="mb_hp1" id="mb_hp1" class="inputTypeText" />
										<option value="">선택
										<option value="010" <?php if($mb_hp1=='010') echo 'selected';?> >010
										<option value="011" <?php if($mb_hp1=='011') echo 'selected';?> >011
										<option value="016" <?php if($mb_hp1=='016') echo 'selected';?> >016
										<option value="017" <?php if($mb_hp1=='017') echo 'selected';?> >017
										<option value="018" <?php if($mb_hp1=='018') echo 'selected';?> >018
										<option value="019" <?php if($mb_hp1=='019') echo 'selected';?> >019
									</select>
									<input type="text" name="mb_hp2" id="mb_hp2" maxlength="4" style="ime-mode:inactive" class="inputTypeText" value="<?php echo $mb_hp2; ?>"  />
									<input type="text" name="mb_hp3" id="mb_hp3" maxlength="4" style="ime-mode:inactive"  class="inputTypeText" value="<?php echo $mb_hp3; ?>"  />

								<?php }  ?>
							
							</td>
						</tr>
					</table>
					<br>
					<h2>오더스테이션 모바일의 이벤트, 쿠폰, 할인특가 등의 다양한 정보를 이메일, 문자,푸쉬알람으로 받으시겠습니까?</h2>
	
					<table border="1" summary="">
					<caption>회원 기본정보</caption>
							<colgroup>
					<col style="width:150px;"/>
					<col style="width:auto;"/>
						<tr>
							<th scope="row">SMS수신</th>
							<td>
								<p>
									<label><input type="radio" name="mb_sms" id="mb_sms1" value="1" <?php if($member['mb_sms'] =='1') echo 'checked';?>/> 동의</label>
									<label><input type="radio" name="mb_sms" id="mb_sms2" value="0" <?php if($member['mb_sms'] =='0') echo 'checked';?>/> 동의 안함</label>
								</p>
							</td>
						</tr>
						<tr>
							<th scope="row">E-mail수신</th>
							<td>
								<p>
									<label><input type="radio" name="mb_mailling" id="mb_mailling1" value="1" <?php if($member['mb_mailling'] =='1') echo 'checked';?>/> 동의</label>
									<label><input type="radio" name="mb_mailling" id="mb_mailling2" value="0" <?php if($member['mb_mailling'] =='0') echo 'checked';?> /> 동의 안함</label>
								</p>
							</td>
						</tr>
						<tr>
							<th scope="row">스마트폰 푸쉬알림</th>
							<td>
								<p>
									<label><input type="radio" name="mb_push" id="mb_push1" value="1" <?php if($member['mb_push'] =='1') echo 'checked';?>/> 동의</label>
									<label><input type="radio" name="mb_push" id="mb_push2" value="0" <?php if($member['mb_push'] =='0') echo 'checked';?> /> 동의 안함</label>
								</p>
							</td>
						</tr>
					</table>
					<!-- <p class="btn_w">
						<a href="javascript:Save_Click();" class="btn btn_org">추천약사선택</a>
						 <a href="/main02.asp" class="btn-base btn_wt">취소</a> 
					</p> -->
					<br>
						<h2 class="sub_h2_tit mt15">나의 추천 약사 등록하기</h2>
						<table border="1" summary="">
					<caption>회원 기본정보</caption>
							<colgroup>
					<col style="width:auto;"/>
							<tbody><tr>
								<td class="td_bg">
									<ul class="listtype01">
										<li>고객님의 동네 약국의 약사님을 등록해 주세요.</li>
										<li>선택하신 약사님이 고객님의 추천 약사로 지정됩니다.	</li>
										<li>지정된 추천 약사가 고객님의 건강을 컨설팅 해드립니다.</li>
									</ul>
								</td>
							</tr>
							<!--
							<tr>
								<td>
									<p><select name="" id="" class="selc w100"><option value="">약국을 선택해주세요.</option></select></p>
								</td>
							</tr>
							//-->
							<tr>
								<td>
									<p class="medisearch">

										<!--<a href="javascript:npopupLayer('pop_pharmacy.asp?obj1=frm&obj2=str_tuserid&obj3=str_tname',380,428)" class="btn btn_wt_s">검색</a>//-->
										<div id="sod_bsk_list"> <strong>추천약사 검색해서 지정하기</strong></div>
									</p>

									<iframe src="./register_pharmacy.php" frameborder="0" width="100%" height="240" marginwidth="0" marginheight="0"></iframe>


								</td>
							</tr>
						</tbody>
						</table>
						<table class="t_medi mt10">
							<colgroup>
								<col width="30%">
								<col width="70%">
							</colgroup>
							<tbody>
								<tr>
									<th>추천 약사</th>
									<td > 
									<?php if($w=='') { ?>
		
									<input type="hidden" name="mb_recommend" id="mb_recommend" >
									<span id="mb_recommend_name"></span>
									<?php } else { 
										$rmb = get_member($member['mb_recommend']);
									?>
									<input type="hidden" name="mb_recommend" id="mb_recommend" value="<?php echo $member['mb_recommend'];?>">
									<span id="mb_recommend_name"><?php echo $rmb['pharm_name'];?> <?php echo $rmb['mb_name'];?></span>
									<?php } ?>
									
									</td>
								</tr>
							</tbody>
						</table>
						<!-- <p class="btn_w">
							<a href="javascript:Save_Click();" class="btn btn_org">가입하기</a>
							<a href="/main02.asp" class="btn btn_wt">취소</a>
						</p> -->

					</form>
				</div>
				<br>











	<div class="btn_confirm">
		<a href="<?php echo G5_URL ?>" class="btn-base btn_cancel">취소</a>
		<input type="submit" value="<?php echo $w==''?'회원가입':'정보수정'; ?>" id="btn_submit" class="btn-base btn_submit" accesskey="s">
	</div>
</div>
</form>

<script> 

var Msg_1 = "<?=$var_msg_1?>";
var Msg_2 = "<?=$var_msg_2?>";
var Msg_3 = "<?=$var_msg_3?>";
var Msg_4 = "<?=$var_msg_4?>";

function hp_certify(f) { 

	var GetYn = OrderMemSearch();

	if( GetYn == "B" ){
		alert(Msg_1);
		return false;
	}else if( GetYn == "O" ){
		alert(Msg_2);
		return false;	
	}else if( GetYn == "N" ){
		alert(Msg_4);
		return;
	}

	if(f.mb_hp1.selectedIndex==0){
		alert("\n휴대폰을 선택해 주세요.");
		f.mb_hp1.focus();
		return false;
	}
	if(chkSpace(f.mb_hp2.value)){
		alert("\n휴대폰을 입력해 주세요.");
		f.mb_hp2.focus();
		return false;
	}
	if(chkSpace(f.mb_hp3.value)){
		alert("\n휴대폰을 입력해 주세요.");
		f.mb_hp3.focus();
		return false;
	}


	var pattern = /^01[0-9][-]{0,1}[0-9]{3,4}[-]{0,1}[0-9]{4}$/; 
	var mb_hp = f.mb_hp1.value + f.mb_hp2.value + f.mb_hp3.value;
		
	if(!pattern.test(mb_hp)){ 
		alert("핸드폰 번호가 입력되지 않았거나 번호가 틀립니다.\n\n핸드폰 번호를 *** 개인정보보호를 위한 휴대폰번호 노출방지 *** 또는 *** 개인정보보호를 위한 휴대폰번호 노출방지 *** 과 같이 입력해 주십시오."); 
		f.mb_hp2.select(); 
		f.mb_hp2.focus(); 
		return; 
	} else {

		$.post("/bbs/ajax.mb_hp_check.php",
			{mode:'send-register', mb_hp : mb_hp, token:'<?=$token?>'},
		function(data) {
			  var json =  $.parseJSON(data);

			  if(json.type == 'ok') {
				  //$('#hp_send_msg').html('전송하였습니다.' + json.certify);
				  $('#hp_send_msg').html(json.msg);
			  } else {
				  //$('#hp_send_msg').html('실패하였습니다.');
				  $('#hp_send_msg').html(json.msg);
			  }
		});
	}

	//win_open("/bbs/hp_certify.php?hp="+mb_hp+"&token=<?=$token?>", "hiddenframe"); 
} 
function win_open(url, name, option) 
{ 
    var popup = window.open(url, name, option); 
    popup.focus(); 

}
function hp_certify_check(f) { 

	var GetYn = OrderMemSearch();

	if( GetYn == "B" ){
		alert(Msg_1);
		return false;
	}else if( GetYn == "O" ){
		alert(Msg_2);
		return false;	
	}else if( GetYn == "N" ){
		alert(Msg_4);
		return;
	}

	if(chkSpace(f.mb_hp_certify.value)){
		alert("\n인증번호를 입력해 주세요.");
		f.mb_hp_certify.focus();
		return false;
	}
	var mb_hp_certify = f.mb_hp_certify.value ;

	//win_open("/bbs/hp_certify_check.php?mb_hp_certify="+mb_hp_certify, "hiddenframe"); 
	$.post("/bbs/ajax.mb_hp_check.php",
			{mode:'check', mb_hp_certify : mb_hp_certify},
		function(data) {
			  var json =  $.parseJSON(data);

			  if(json.type == 'ok') {
				  $('#mb_hp_check').val('Y');
				  //$('#hp_certify_msg').html('인증되었습니다.');
				  $('#hp_certify_msg').html(json.msg);
			  } else {
				  $('#mb_hp_check').val('N');
				  //$('#hp_certify_msg').html('인증번호가 틀립니다. 다시입력해주세요.' + json.msg);
				  $('#hp_certify_msg').html(json.msg);
			  }
		});

} 

//숫자체크
function isnum(NUM) {
	for(var i=0;i<NUM.length;i++){
		achar = NUM.substring(i,i+1);
		if( achar < "0" || achar > "9" ){
			return false;
		}
	}
	return true;
}

// 입력값이 NULL 인지 체크
function chkSpace(strValue){
	var flag=true;
	if (strValue!=""){
		for (var i=0; i < strValue.length; i++){
			if (strValue.charAt(i) != " "){
				flag=false;
				break;
			}
		}
	}
	return flag;
}

</script> 


    <script>

	// 선택사항수정
    $(".mod_options").click(function() {
      
        var $this = $(this);
        close_btn_idx = $(".mod_options").index($(this));

        $.post(
            "./register_pharmacy.php",
            function(data) {
                $("#mod_option_frm").remove();
                $this.after("<div id=\"mod_option_frm\"></div>");
                $("#mod_option_frm").html(data);
            }
        );
    });

	 // 옵션수정 닫기
    $(document).on("click", "#mod_option_close", function() {
        $("#mod_option_frm").remove();
        $(".mod_options").eq(close_btn_idx).focus();
    });
	// 옵션수정 닫기
    function closeLayer() {
        $("#mod_option_frm").remove();
        $(".mod_options").eq(close_btn_idx).focus();
    }


    // 추천약사
    $("#mb_recommend_pop").click(function() {

		if(!fregisterform_submit(fregisterform, 1)) {
			return ;
		}
        
        var $this = $(this);
        close_btn_idx = $("#mb_recommend_pop").index($(this));

        $.post(
             "./register_pharmacy.php",
            function(data) {
                $("#mod_option_frm").remove();
                $this.after("<div id=\"mod_option_frm\"></div>");
                $("#mod_option_frm").html(data);
            }
        );
    });



	function onOnlyAlphaNumber(obj) {
		str = obj.value; 
		len = str.length; 
		ch = str.charAt(0);
		for(i = 0; i < len; i++) { 
			ch = str.charAt(i); 
			if( (ch >= '0' && ch <= '9') || (ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z') ) { 
			continue; 
			} else { 
			alert("영문과 숫자만 입력이 가능합니다.");
			obj.value="";
			obj.focus();
			return false; 
		} 
		}
		return true; 
	}

    // submit 최종 폼체크
    function fregisterform_submit(f, kind)
    {
		if(chkSpace(f.mb_id.value)){
        	alert("\n아이디를 입력해 주세요..");
            f.mb_id.focus();
            return false;
	   	}


		if(chkSpace(f.mb_name.value)){
        	alert("\n성명을 입력해 주세요..");
            f.mb_name.focus();
            return false;
	    }
		
		if (f.mb_email1.value == "" ) {
			alert("\n이메일를 입력해 주세요..");
			f.mb_email1.focus();
			return false;
		}
		if(chkSpace(f.mb_email2.value)){
			alert("\n이메일를 입력해 주세요..");
			f.mb_email2.focus();
			return false;
		}
		if(mb_email_check(f.mb_email1.value+"@"+f.mb_email2.value)==false) {
			alert("\n정확한 이메일 주소를 입력하세요.");
			f.mb_email1.focus();
			return false;
		}
		
		if(f.mb_hp1.selectedIndex==0){
        	alert("\n휴대폰을 선택해 주세요.");
            f.mb_hp1.focus();
            return false;
		}
		if(chkSpace(f.mb_hp2.value)){
        	alert("\n휴대폰을 입력해 주세요.");
            f.mb_hp2.focus();
            return false;
		}
		if(chkSpace(f.mb_hp3.value)){
        	alert("\n휴대폰을 입력해 주세요.");
            f.mb_hp3.focus();
            return false;
		}
		
		if (f.mb_hp_check.value=="N") {
			alert("\휴대폰 인증을 진행하세요.");
			return false;
		}
		/*
		if(f.mb_birth2.selectedIndex==0){
        	alert("\n생년월일을 선택해 주세요.");
            f.mb_birth2.focus();
            return false;
		}
		if(f.mb_birth3.selectedIndex==0){
        	alert("\n생년월일을 선택해 주세요.");
            f.mb_birth3.focus();
            return false;
		}
		*/
		if(f.mb_sms[0].checked==false&&f.mb_sms[1].checked==false){
	      	alert("\nSMS 수신여부를 선택해 주세요.");
	        f.mb_sms[0].focus();
	      	return false;
		}
		if(f.mb_mailling[0].checked==false&&f.mb_mailling[1].checked==false){
	      	alert("\n이메일 수신여부를 선택해 주세요.");
	        f.mb_mailling[0].focus();
	      	return false;
		}
		if(f.mb_push[0].checked==false&&f.mb_push[1].checked==false){
	      	alert("\n스마트폰 푸쉬알림 수신여부를 선택해 주세요.");
	        f.mb_push[0].focus();
	      	return false;
		}
		<?// if( $member['mb_type'] != "1" ){ ?>
		if (f.mb_recommend.value=="") {
			alert("\추천약사를 지정하세요.");
			return false;
		}
		<?// } ?>

        return true;
    }
	// 비밀번호
	function mb_passwd_check()
	{
	    //valid check
		if ( document.fregisterform.mb_passwd1.value.length <= 0 )
		{
			alert("\n비밀번호를 입력하세요.");
			return false;
		}
		
		var isID = /^(?=.*[a-zA-Z])(?=.*\d).{6,20}$/;
		if( !isID.test(document.fregisterform.mb_passwd1.value) ) {
			alert("비밀번호는 6~20자의 최소 1개의 숫자, 영문자 를 포함해야 합니다."); 
			//str.focus();
			return false; 
		}
		
		if ( document.fregisterform.mb_passwd2.value.length <= 0 )
		{
			alert("\n비밀번호를 한번 더 입력하세요.");
			document.fregisterform.mb_passwd2.value="";
			return false;
		}

			if( document.fregisterform.mb_passwd1.value != document.fregisterform.mb_passwd2.value)
		{
			alert("\n비밀번호를 정확히 입력하세요.");
			document.fregisterform.mb_passwd1.value="";
			document.fregisterform.mb_passwd2.value="";
			return false;
		}

		//if ( _valid_length_check(2) ) return false;
		return true;
	}
	function mb_name_check() {
	   	if ( _valid_value_check() )    return false;
		if (! isKorean(document.fregisterform.mb_name)) {
			alert("\n이름은 한글만 입력하세요.");
			fregisterform.mb_name.value="";
	        	return false;
		}
		return true;
	}

	function mb_email_check(mb_email) {
	   	if(!isValidEmail(mb_email)){
			return false;
		}
		return true;
	}
	function _valid_value_check() {
	    	str = document.fregisterform.str_name.value;
		var strbyte = 0;
		for (i = 0; i < str.length; i++) {
			var code = str.charCodeAt(i);
			var ch   = str.substr(i,1).toUpperCase();
			code     = parseInt(code);
			if ( code >= 0 && code  <= 47 ) {
				document.fregisterform.str_name.value="";
				alert("특수문자는 사용하실 수 없습니다.");
				return 1;
			}
		}
		return 0;
	}

	// 입력값이 한글로 되어있는지 체크 (추가)
	function isKorean(input) {
		for(i=0; i<input.value.length; i++){
			var CodeNum = input.value.charCodeAt(i);
			if (CodeNum < 128){
				flag = false;
			} else {
				//alert("한글 아님");
				flag = true;
			}
		}
		return flag;
	}
	// 입력값이 이메일 형식인지 체크
	function isValidEmail(input) {
	//    var format = /^(\S+)@(\S+)\.([A-Za-z]+)$/;
	    var format = /^((\w|[\-\.])+)@((\w|[\-\.])+)\.([A-Za-z]+)$/;
	    return isValidFormat(input,format);
	}
	// 입력값이 사용자가 정의한 포맷 형식인지 체크
	function isValidFormat(input,format) {
		  if (input.search(format) != -1) {
	        return true; //올바른 포맷 형식
	    }
	    return false;
	}
	function fnc_semail1(str_value) {
		if (str_value == "") {
			document.fregisterform.mb_email2.value = "";
		} else {
			document.fregisterform.mb_email2.value = str_value;
		}
	}

	function OrderMemSearch(){
	
		var frm = document.fregisterform;
		var SendYn_1 = "Y";
		var SendYn_2 = "Y";
		var SendYn_3 = "Y";

		var mb_hp1		= frm.mb_hp1.value;
		var mb_hp2		= frm.mb_hp2.value;
		var mb_hp3		= frm.mb_hp3.value;

		if( mb_hp1 == "" || mb_hp2 == "" || mb_hp3 == "" ){
			SendYn_1 = "N";
			SendYn_2 = "N";
			SendYn_3 = "N";
		}

		if( SendYn_1 == "Y" && SendYn_2 == "Y" && SendYn_3 == "Y" ){

			$.ajax({

				  url: '/bbs/order_mem_check.php',
				  type: 'post',
				  async: false,
				  data: {
						"mb_hp1"	:	mb_hp1,
						"mb_hp2"	:	mb_hp2,
						"mb_hp3"	:	mb_hp3
				  },
				  dataType: 'text',
				  success: function(data) {

					ReturnValue= data.trim();

				  }

			});

			return ReturnValue;

		}else{

			return "N";		

		}
	
	}

    </script>

<!-- } 회원정보 입력/수정 끝 -->
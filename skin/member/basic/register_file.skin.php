<!-- 2019.04.11 회원가입방법 버튼, 이미지 추가 -->

<?php
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가

// add_stylesheet('css 구문', 출력순서); 숫자가 작을 수록 먼저 출력됨
add_stylesheet('<link rel="stylesheet" href="'.$member_skin_url.'/style.css">', 0);

?>

<!-- 회원가입약관 동의 시작 { -->

<!-- <div> -->
<div style="padding-left:200px">

    <?php
    // 소셜로그인 사용시 소셜로그인 버튼
    //@include_once(get_social_skin_path().'/social_register.skin.php');
    ?>

    <form name="frm" id="frm" action="<?php echo $register_action_url ?>" method="post" enctype="multipart/form-data">
		<input type="hidden" name="mb_password" value="<?=$mb_password?>"/>
		<section id="fregister_private">
			<div>
				<table>
					<thead>
					<tr>
						<th style='background-color:#ebebeb !important; border:none !important;color:black; height:50px; font-size:16px;'>검사지 파일등록</th>
					</tr>
					</thead>
					<tbody>
					<tr>
						<td>
							<input type="file" name="reg_file[]" id="reg_file" style="width:80%; font-size:13px !important;"/>
							<button type="button" style="background-color: #35B8EA; width:131.99px; height:35px;border-radius:3px; color:white; font-size: 1.083em; font-weight: bold;" onclick="javascript:FileSend();">파일 등록</button>
							<br><br><font color='black'><STRONG>&nbsp&nbsp파일 선택 후 파일 등록 버튼을 클릭해주세요.</STRONG></font><br>
							<div style="background-color:#ebebeb; margin-top:15px; margin-bottom:15px;">
							
							<font color=#ED0000><strong>결과지 업로드 유의사항<br><br></font><font>
1) 결과지 파일명은 '바코드_이름_전화번호_생년월일' 형식으로 업로드해주세요.<br><br>
2) 동일한 파일 재업로드시 알림톡이 재발송 되지 않습니다. 담당자에게 전화 주세요. (02-2054-8853 전지혜)<br><br>
3) 파일명 형식을 변경하지 마세요. (ex) 바코드_이름_전화번호_생년월일_rev01<br><br>
4) 개인정보이므로 결과지가 바뀌지 않도록 주의부탁드립니다.<br><br></strong></font>
							</div>
						</td>
					</tr>
				</table>
				<br>
				<table>
					<thead>
					<tr>
						<th style='background-color:#ebebeb !important; border:none !important;color:#000000; height:50px; font-size:16px;'>업로드 파일목록</th>
					</tr>
					<?
					$sql = " select * from tbl_regfile ";
					$result = sql_query($sql);		
					?>
					<? if( $result->num_rows > 0 ){ ?>
						<? for ($i=0; $row=sql_fetch_array($result); $i++) { ?>
					<tr>
						<td>
							<?=$row["file_name"]?> &nbsp; 업로드일시 : <?=$row["reg_time"]?> &nbsp;&nbsp;&nbsp;
							<button type="button" style="background-color: #0000A5; height:35px; width:131.99px;  border-radius:3px; color:white; font-size: 1.083em; font-weight: bold;" onclick="javascript:FileDown(<?=$row["file_idx"]?>);">다운로드</button>						
						</td>
					</tr>
						<? } ?>
					<? } ?>
					</tbody>
				</table>
			</div>

		</section>

    </form>

	<form name="filefrm">
		<input type="hidden" name="file_idx" value=""/>
	</form>

</div>

<script>

function FileSend(){

	var fileInput = document.getElementById("reg_file");
	 
	var files = fileInput.files;
	var file;
	var file_cnt = 0;
	var maxSize  = 100 * 1024 * 1024
	var fileSize = 0;
	var browser=navigator.appName;
	 
	for (var i = 0; i < files.length; i++) {
		 
		file = files[i];

		if( file != '' ){
			file_cnt++;
			if (browser=="Microsoft Internet Explorer")
			{
				var oas = new ActiveXObject("Scripting.FileSystemObject");
				fileSize = fileSize+oas.getFile( file.value ).size;
			}
			// 익스플로러가 아닐경우
			else
			{
				fileSize = fileSize+file.size;
			}
		}

	}

	if( file_cnt == 0){
		alert("검사지 파일을 등록해주세요");
		return;
	}
	
	if(fileSize > maxSize){
        alert("첨부파일 사이즈는 최대 100MB 이내로 등록 가능합니다.");
        return;
    }

	document.frm.submit();

}

function FileDown( GetIdx ){

	var frm = document.filefrm;

	frm.file_idx.value = GetIdx;
	frm.method = "post";
	frm.action = "./regfile_download.php";
	frm.submit();


}

</script>




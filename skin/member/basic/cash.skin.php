<?php
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가

// add_stylesheet('css 구문', 출력순서); 숫자가 작을 수록 먼저 출력됨
add_stylesheet('<link rel="stylesheet" href="'.$member_skin_url.'/style.css">', 0);

?>
<body style="min-width: 583px">

<div id="cash" class="new_win">
    <h1 id="win_title"><i class="fa fa-database" aria-hidden="true"></i> <?php echo $g5['title'] ?></h1>

    <div class="new_win_con list_01">

        <ul>
            <?php
            $sum_cash1 = $sum_cash2 = $sum_cash3 = 0;

            $sql = " select * , ( ca_expire_date < LEFT( NOW() , 10 )  ) as expired_yn
                        {$sql_common}
                        {$sql_order}
                        limit {$from_record}, {$rows} ";

            $result = sql_query($sql);
            for ($i=0; $row=sql_fetch_array($result); $i++) {
                $cash1 = $cash2 = 0;
                if ($row['ca_cash'] > 0) {
                    $cash1 = '+' .number_format($row['ca_cash']);
                    $sum_cash1 += $row['ca_cash'];
                } else {
                    $cash2 = number_format($row['ca_cash']);
                    $sum_cash2 += $row['ca_cash'];
                }

                $ca_content = $row['ca_content'];

                $expr = '';
                //if($row['ca_expired'] == 1)
				if($row['expired_yn'] == 1 || $row['ca_expired'] == 1){
                    $expr = ' txt_expired';
				}
            ?>
            <li>
                <div class="cash_top">
                    <span class="cash_tit"><?php echo $ca_content; ?></span>
                    <?php if($cash1){?>
                        <span class="cash_in"><?php echo $cash1?></span>
                    <?php }else {?>
                        <span class="cash_out"><?php echo $cash2?></span>
                    <?php }?>

                </div>
                <span class="cash_date1"><i class="fa fa-clock-o" aria-hidden="true"></i> <?php echo $row['ca_datetime']; ?></span>
                <span class="cash_date<?php echo $expr; ?>">
                    <?if ($row['expired_yn'] == 1 || $row['ca_expired'] == 1) { ?>
						<? if( $row['ca_cash'] < 0 ){ ?>
						사용
						<?}else{?>
						만료 <?=substr(str_replace('-', '', $row['ca_expire_date']), 2); ?>
						<? } ?>
                    <?}else{
						echo $row['ca_expire_date'] == '9999-12-31' ? '&nbsp;' : $row['ca_expire_date'];
					  }
					?>
                </span>
            </li>
            <?php
            }

            if ($i == 0)
                echo '<li class="empty_li">자료가 없습니다.</li>';
            else {
                if ($sum_cash1 > 0)
                    $sum_cash1 = "+" . number_format($sum_cash1);
                $sum_cash2 = number_format($sum_cash2);
            }
            ?>

            <li class="cash_status">
                소계<span><?=number_format($member['mb_cash'])?></span>
            </li>
        </ul>

    </div>

</body>
    <?php echo get_paging(G5_IS_MOBILE ? $config['cf_mobile_pages'] : $config['cf_write_pages'], $page, $total_page, $_SERVER['SCRIPT_NAME'].'?'.$qstr.'&amp;page='); ?>

    <button type="button" onclick="javascript:window.close();" class="btn_close">창닫기</button>
</div>
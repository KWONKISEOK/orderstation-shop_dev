<?php
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가

// add_stylesheet('css 구문', 출력순서); 숫자가 작을 수록 먼저 출력됨
//add_stylesheet('<link rel="stylesheet" href="'.G5_SHOP_CSS_URL.'/style.css">', 0);

$c_status=0; //장바구니이동전 팝업위한 변수
?>
<!-- 2017 리뉴얼한 테마 적용 스크립트입니다. 기존 스크립트를 오버라이드 합니다. -->
<script src="<?php echo G5_JS_URL; ?>/shop.override.js?ver=<?php echo G5_JS_VER; ?>"></script>
<?php if($config['cf_kakao_js_apikey']) { ?>
<script src="//developers.kakao.com/sdk/js/kakao.min.js"></script>
<?php } ?>

<style>
    #sit_right {
        position: absolute;
        float: right;
        margin-left: 1080px;

        height: auto !important;
        height: 355px;
        min-height: 355px;
    }

    .boxarea {
        width: 120px;
        margin-top: 1px;
        right: 0;
        height: 100%;
        background: #f6f6f6;
        background: -webkit-linear-gradient(#f6f6f6, #fff);
        background: -o-linear-gradient(#f6f6f6, #fff);
        background: -moz-linear-gradient(#f6f6f6, #fff);
        background: linear-gradient(#f6f6f6, #fff);
    }

    .boxarea .block {
        border-top: 1px dotted #ddd;
        width: 90px;
        margin: 0 auto;
        padding: 20px 0;
        text-align: center;
    }

    .boxarea .list_count {
        font-size: 12px;
        font-weight: bold;
        padding: 0 0 3px 0;
    }

</style>

<form name="fitem" method="post" action="<?php echo $action_url; ?>" onsubmit="return fitem_submit(this);">
    <input type="hidden" name="it_id[]" value="<?php echo $it_id; ?>">
    <input type="hidden" name="sw_direct">
    <input type="hidden" name="url">
    <input type="hidden" name="c_status" value="<?php echo $c_status ?>">
    <input type="hidden" name="member_id" value="<?php echo $member['mb_id'] ?>">
    <div id="sit_ov_wrap">
    <div class="product-box">
        <!-- 상품이미지  -->
        <div class="product-view-box">
            <div class="gallery-wrap">
                <h2 class="sound_only">상품 이미지 보기</h2>
                <div class="gallery">
                    <?php
                    $big_img_count = 0;
                    $thumbnails = array();
                    for($i=1; $i<=10; $i++) {
                        if(!$it['it_img'.$i])
                            continue;

                        $img = get_it_thumbnail($it['it_img'.$i], $default['de_mimg_width'], $default['de_mimg_height']);

                        if($img) {
                            // 썸네일
                            $thumb = get_it_thumbnail($it['it_img'.$i], $default['de_mimg_width'], $default['de_mimg_height']);
                            $thumbnails[] = $thumb;
                            $big_img_count++;

                            echo '<a href="'.G5_SHOP_URL.'/largeimage.php?it_id='.$it['it_id'].'&amp;no='.$i.'" target="_blank" class="popup_item_image" style="display:none;">'.$img.'</a>';
                        }
                    }

                    if($big_img_count == 0) {
                        echo '<img src="'.G5_SHOP_URL.'/img/no_image.gif" alt="">';
                    }
                    ?>
                </div>
                <!-- [D] 썸네일 -->

                <?php
                // 썸네일
                $thumb1 = true;
                $thumb_count = 0;
                $total_count = count($thumbnails);
                if($total_count > 0) {
                    echo '<div class="gallery-list">';

                    $num = 1;
                    foreach($thumbnails as $val) {
                        $thumb_count++;
                        $sit_pvi_last ='';
                        $on = '';
                        if ($thumb_count % 5 == 0) $on = ' on';
                        echo '<button class="thumb thumb'.$num.$on.'">';
                        echo $val;
                        echo '</button>';
                        $num++;
                    }
                    echo '</div>';
                }
                ?>

            </div>
            <div class="product-grade-box">
                <div class="product-grade">
                    <div class="product-star-wrap">
                        <?php
                        // 평점을 소수점이하는 별도로 표시하기 위해 아래와 같이 변수 선언
                        $floor_score = floor($star_score);
                        $decimal_score = $star_score - $floor_score;
                        ?>

                        <?php for ($i=0; $i<$floor_score; $i++) { ?>
                            <span class="product-star">
                                <!-- [D] inline 스타일로 퍼센트 수치 조정 -->
                                <span class="product-star-value" style="width: 100%"></span>
                            </span>
                        <?php } ?>

                        <?php if ($decimal_score > 0) { ?>
                            <span class="product-star">
                                <?php
                                $decimal_score_per = $decimal_score * 100;
                                ?>
                                <!-- [D] inline 스타일로 퍼센트 수치 조정 -->
                                <span class="product-star-value" style="width:<?php echo $decimal_score_per; ?>%"></span>
                            </span>
                        <?php } ?>
                    </div>
                    
                    <span class="sound_only">별점 5점 중 <em><?php echo $star_score; ?></em>점</span>
                    <span class="grade"><?php echo $star_score; ?></span><span class="total-review">(<?php echo $item_use_count; ?>건)</span>
                </div>
                <div class="share-sns-box">
                    <?php echo get_sns_share_link('facebook_new', $sns_url, $sns_title, '');?>
                    <?php echo get_sns_share_link('kakaotalk_new', $sns_url, $sns_title, '');?>
                    <a class="icon share-url" href="javascript:copy_to_clipboard('<?php echo G5_SHOP_URL.'/item.php?it_id='.$it['it_id'];?>');" title="url"></a>
                </div>
                <script type='text/javascript'>
                    //<![CDATA[
                    // // 사용할 앱의 JavaScript 키를 설정해 주세요.
                    Kakao.init("<?php echo $config['cf_kakao_js_apikey']; ?>");
                    // // 카카오링크 버튼을 생성합니다. 처음 한번만 호출하면 됩니다.
                    Kakao.Link.createDefaultButton({
                        container: '#kakao-link-btn',
                        objectType: 'feed',
                        content: {
                            title: '<?php echo $it['it_name'];?>',
                            description: '',
                            imageUrl: '<?php echo get_it_imageurl($it['it_id']);?>',
                            link: {
                                mobileWebUrl: '<?php echo $sns_url;?>',
                                webUrl: '<?php echo $sns_url;?>'
                            }
                        },
                        /*social: {
                         likeCount: 286,
                         commentCount: 45,
                         sharedCount: 845
                        },*/
                        buttons: [{
                            title: '상품 보기',
                            link: {
                                mobileWebUrl: '<?php echo $sns_url;?>',
                                webUrl: '<?php echo $sns_url;?>'
                            }
                        }]
                    });
                    //]]>
                </script>
            </div>
        </div>
        <!-- 상품 요약정보(정기주문)  -->
        <div class="product-detail-box">
            <a class="product-brand-link" href="/shop/listcomp.php?comp=<?php echo $it['comp_code']; ?>">
                <span>브랜드관</span>
                <i class="icon icon-brand-link">
                    <span class="sound_only">더보기</span>
                </i>
            </a>
            <h2 class="product-title"><?php echo stripslashes($it['it_name']); ?> <span class="sound_only">요약정보 및 구매</span></h2>
            <?php if ($is_drug == true) { ?>
            <p class="product-price-order">약국공급가</p>
            <?php } ?>
            <div class="product-price-set">
                <span class="product-sale-price">
                    <?php if (!$it['it_use']) { // 판매가능이 아닐 경우 ?>
                        <span class="price"><?php echo display_price(get_price($it)); ?></span><span class="unit">원</span>
                    <?php } else if ($it['it_tel_inq']) { // 전화문의일 경우 ?>
                        <span class="price">전화문의</span>
                    <?php } else { // 전화문의가 아닐 경우 ?>
                        <?php if ($is_drug == true) { ?>
                            <span class="price"><?php echo display_price($it['it_drug_price']); ?></span><span class="unit">원</span>~
                            <input type="hidden" id="it_drug_price" value="<?php echo $it['it_drug_price']; ?>">
                        <?php } else { ?>
                            <span class="price"><?php echo display_price(get_price($it)); ?></span><span class="unit">원</span>~
                        <?php } ?>
                        <input type="hidden" id="it_price" value="<?php
                        if($is_drug == true){
                            echo $it['it_drug_price'];
                        }else{
                            echo get_price($it);
                        } ?>">
                    <?php } ?>

                </span>
                <div class="product-original-group">
                    <span class="product-original-price">
                        <?php if ($is_drug == true) { ?>
                        <span class="price"><?php
                            if($it['it_cust_price'] > 0){
                                echo number_format($it['it_cust_price']);
                            }else{
                                echo display_price(get_price($it));
                            } ?>
                        </span>
                        <?php } else { ?>
                            <?php if ($it['it_cust_price']) { ?>
                                <span class="price"><?php echo display_price($it['it_cust_price']); ?></span><span class="unit">원</span>
                            <?php } ?>
                        <?php } ?>
                    </span>
                    <?php if ($is_drug == true) { ?>
                    <!-- 할인정보 툴팁 / 툴팁 열릴때 .is-opened 추가(스크립트 하기 적용됨) -->
                    <div class="tooltip-wrapper">
                        <a class="open-tooltip icon icon-noti" href="javascript:;"></a>
                        <div class="tooltip-layer">
                            <!-- 
                                *
                                *
                                수정화면
                                *
                                *
                            -->
                            <h2 class="tooltip-title">결제 혜택 안내</h2>
                            <div class="tooltip-cont">
                                <div class="tooltip-payment">
                                    <div class="divide">
                                        <dl class="price-info price-info-type2">
                                            <dt class="title">소비자가</dt>
                                            <dd class="desc">
                                                <span class="price"><?php
                                                    if($it['it_cust_price'] > 0){
                                                        echo number_format($it['it_cust_price']);
                                                    }else{
                                                        echo display_price(get_price($it));
                                                    }
                                                    ?></span>
                                                <span class="unit">원</span>
                                            </dd>
                                        </dl>
                                    </div>
                                    <div class="divide" >
                                         <dl class="price-info price-info-type2" id="item_discount">
                                            <dt class="title">상품할인</dt>
                                            <dd class="desc">
                                                <span class="price">
                                                    <input type="hidden" class="item_discount" value="<?php echo $it['it_cust_price'] ?>">
                                                  - <?php echo number_format($it['it_cust_price'] - $it['it_price']);?></span>
                                                <span class="unit">원</span>
                                            </dd>
                                        </dl>
                                        <div class="divide_item">
                                        <dl class="price-info price-info-type2">
                                            <dt class="title">포인트</dt>
                                            <dd class="desc">
                                                <span class="price">- <?php echo number_format($it['it_price'] - $it['it_drug_price']); ?></span>
                                                <span class="unit">원</span>
                                            </dd>
                                        </dl>
                                        </div>
                                    </div>
                                    <div class="divide">
                                        <dl class="price-info price-info-type2 price-info-result">
                                            <dt class="title">총 약국 결제 금액</dt>
                                            <dd class="desc">
                                                <span class="price"><?php echo number_format($it['it_drug_price']); ?></span>
                                                <span class="unit">원</span>
                                            </dd>
                                        </dl>
                                    </div>
                                </div>
                            </div>
                            <div class="tooltip-foot">
                                <button type="button" class="close-tooltip btn-tooltip">확인</button>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>

            </div>
            <div class="product-info-box">
                <dl class="list-style">
                    <dt class="title">배송정보</dt>
                    <dd class="desc">
                        <?php
                        $ct_send_cost_label = '배송비';
                        $code_comp=$it['comp_code'];

                        if ($it['it_sc_type'] == 1) {
                            $sc_method = '무료배송';
                        } else {
                            $sc_method = '유료배송';

                            /**
                             * 배송비 결제 방식 - 현재 선불 밖에 없으므로 주석 처리
                             */
                            /*if ($it['it_sc_method'] == 1) {
                                $sc_method = '수령후 지불';
                            } else if ($it['it_sc_method'] == 2) {
                                $ct_send_cost_label = '<label for="ct_send_cost">배송비결제</label>';
                                $sc_method = '<select name="ct_send_cost" id="ct_send_cost">
                                          <option value="0">주문시 결제</option>
                                          <option value="1">수령후 지불</option>
                                      </select>';
                            } else {
                                $sc_method = '유료배송';
                            }*/

                            if ($it['it_sc_type'] == '2') {
                                $trans_memo = number_format($it['it_sc_price']).'원';
                                $trans_memo2 = ' ※ 동일 판매자 상품 '.number_format($it['it_sc_minimum']).'원 이상 구매 시 무료';
                            }
                            if ($it['it_sc_type'] == '3'){
                                $sc_method2 = ' ※ 총 주문금액 관계없이 배송비 유료상품';
                                $trans_memo = number_format($it['it_sc_price']).'원';
                                $trans_memo2 = ' ※ 동일 판매자 상품 묶음배송 가능';
                            }
                            if ($it['it_sc_type'] == '4') {
                                $trans_memo = number_format($it['it_sc_price']).'원';
                                $trans_memo2 = ' ※ '.$it['it_sc_qty'].'개 단위로 배송비 '.number_format($it['it_sc_price']).'원 추가 부과';
                            }
                        }
                        ?>
                        <p>[ <?php echo $sc_method; ?> ] &nbsp <?php echo $trans_memo; ?>
                        <?php if ($it['it_sc_type'] != 1) { ?>
                        <br><?php echo $trans_memo2;?> <br><?php echo $sc_method2 ?></p>
                        <?php } ?>

                    </dd>
                </dl>
                <?php if($it['it_company'] != 0 && ($member['mb_referee'] == 4 || $member['mb_type'] == 9)){ ?>
                    <dl class="list-style">
                        <dt class="title">사내수령가능지점</dt>
                        <dd class="desc">
                            <input type="checkbox" name="it_company" value="1" <? if (preg_match('/1/',$it['it_company'])){ ?>checked<? } ?> onclick="return false;" >
                            <label for="it_company">태전약품 전주</label>
                            <input type="checkbox" name="it_company" value="2" <? if (preg_match('/2/',$it['it_company'])){ ?>checked<? } ?> onclick="return false;" >
                            <label for="it_company">티제이팜 평택</label>
                            <input type="checkbox" name="it_company" value="3" <? if (preg_match('/3/',$it['it_company'])){ ?>checked<? } ?> onclick="return false;" >
                            <label for="it_company">오엔케이 서울</label>
                        </dd>
                    </dl>
                <?php }?>
                <dl class="list-style">
                    <dt class="title">카드혜택</dt>
                    <dd class="desc">무이자 할부 혜택
                        <!-- 툴팁 시작 -->
                        <!-- 툴팁 열릴때 .is-opened 추가(스크립트 하기 적용됨) -->
                        <div class="tooltip-wrapper">
                            <a class="open-tooltip icon icon-noti" href="javascript:;"></a>
                            <div class="tooltip-layer">
                                <h2 class="tooltip-title">무이자 할부 혜택 안내</h2>
                                <div class="tooltip-cont">
                                    <?php echo CardInfoText3(); ?>
                                </div>
                                <div class="tooltip-foot">
                                    <button type="button" class="close-tooltip btn-tooltip">확인</button>
                                </div>
                            </div>
                        </div>
                    </dd>
                </dl>

                <?php
                $item_point = get_price_cash($it['it_point_type'], $it['it_price'], $member['mb_grade']);
               if($it['it_point_type'] == 1 && ($member['mb_referee'] == 4 || $member['mb_extra'] == 11)) {
                   if(true){ ?>
                <dl class="list-style">
                    <dt class="title">적립금</dt>
                    <dd class="desc"><?php echo $item_point?> 원
                    </dd>
                </dl>
            <?php }
               }?>
            </div>

            <!-- 옵션 선택(상품) -->
            <!-- [D] 옵션이 펼쳐진 경우 .show 추가 (하단 스크립트 적용됨) -->
            <?php if($option_item_new) { ?>
            <?php echo $option_item_new; // 선택옵션 ?>
            <script>
                /* =====================================
                * dropdown (임의 작성입니다. 작업시 지워주세요)
                * =====================================*/
                $('.option-dropdown').on({
                    click: function () {       
                        try {
                            if ($(this).attr('id') == 'get_item_options_1') { //상위메뉴                  
                                $(get_item_options).find('.dropdown-menu').stop().toggleClass('show').slideUp(200);                                           
                            } else if ($(this).attr('id') == 'get_item_options') { //하위메뉴
                                $(get_item_options_1).find('.dropdown-menu').stop().toggleClass('show').slideUp(200);                                                   
                            }
                        }
                        finally {                                
                            $(this).find('.dropdown-menu').stop().toggleClass('show').slideToggle(200);            
                            $(this).toggleClass('show');                                            
                        }
                    }
                });
            </script>
            <?php } ?>
            <!-- 선택한 상품 목록 (약사용) -->

            <?php if ($is_orderable) { ?>
            <div class="product-selected-group show" id="it_sel_option">
                <!-- 선택 상품 옵션 VIEW (JS) -->
                <?php
                // 옵션이 없는 경우 선택 상품이 즉시 보이도록 처리
                if(!$option_item) {
                    if(!$it['it_buy_min_qty'])
                        $it['it_buy_min_qty'] = 1;
                    ?>
                    <div class="product-selected-box is-opened">
                        <input type="hidden" name="io_type[<?php echo $it_id; ?>][]" value="0">
                        <input type="hidden" name="io_id[<?php echo $it_id; ?>][]" value="">
                        <input type="hidden" name="io_value[<?php echo $it_id; ?>][]" value="<?php echo $it['it_name']; ?>">
                        <input type="hidden" class="io_price" value="0">
                        <input type="hidden" class="io_stock" value="<?php echo $it['it_stock_qty']; ?>">
                        <span class="selected-item"><?php echo $it['it_name']; ?></span>
                        <!--<button type="button" class="icon icon-pop-close" title="닫기"></button>-->
                        <div class="input-count-box">
                            <button type="button" class="count-minus sit_qty_minus" title="빼기"><span class="sound_only">감소</span></button>
                            <span class="count-text">
                                <input type="text" id="ct_qty_<?php echo $i; ?>" title="수량입력" name="ct_qty[<?php echo $it_id; ?>][]" value="<?php echo $it['it_buy_min_qty']; ?>" size="5" class="input-default input-count">
                            </span>
                            <button type="button" class="count-plus sit_qty_plus" title="더하기"><span class="sound_only">증가</span></button>
                        </div>
                        <span class="selected-price">
                            <span class="price"><?php
                                if($is_drug == true){
                                    echo number_format($it['it_drug_price']*$it['it_buy_min_qty']);
                                }else{
                                    echo number_format($it['it_price']*$it['it_buy_min_qty']);
                                }
                                ?></span><span class="unit">원</span>
                        </span>
                    </div>
                    <script>
                        $(function() {
                            price_calculate();
                        });
                    </script>
                <?php } ?>
            </div>
            <?php } ?>
            <p class="guide-sm">* 발급받은 쿠폰 적용 가격은 결제 페이지에서 확인 가능합니다.</p>


            <?php if (($it['it_period_yn'] == 'Y' || $it['it_period_yn'] == 'O')) { ?>
            <link rel="stylesheet" href="//code.jquery.com/ui/1.8.18/themes/base/jquery-ui.css" />
            <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
            <script>

                $.datepicker.setDefaults({
                    dateFormat: 'yy-mm-dd',
                    prevText: '이전 달',
                    nextText: '다음 달',
                    monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
                    monthNamesShort: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
                    dayNames: ['일', '월', '화', '수', '목', '금', '토'],
                    dayNamesShort: ['일', '월', '화', '수', '목', '금', '토'],
                    dayNamesMin: ['일', '월', '화', '수', '목', '금', '토'],
                    showMonthAfterYear: true,
                    yearSuffix: '년',
                    beforeShowDay: function(date){
                        var day = date.getDay();
                        return [(day != 0 && day != 6)];}
                });

                $(function() {
                    $(".datepickerbutton").datepicker({minDate: 0});
                });
            </script>
            <!-- 옵션 선택 (공통-정기주문) -->
            <div class="product-option-group">
                <!-- 상위 옵션 선택 -->
                <div class="product-option-box option-main">
                    <dl class="list-style">
                        <dt class="title">정기배송</dt>
                        <dd class="desc">
                            <div class="form-radio radio-sm">
                                <input type="radio" name="ct_period_yn" id="ct_period_yny" value="Y">
                                <label for="ct_period_yny">신청하기</label>
                            </div>
                            <input type="hidden" id="period_y" value="<?php echo $it['it_period_yn'] ?>">
                            <?php if( $it['it_period_yn'] != 'O'){ ?>
                            <div class="form-radio radio-sm">
                                <input type="radio" name="ct_period_yn" id="ct_period_ynn" value="N" checked>
                                <label for="ct_period_ynn">신청안함</label>
                            </div>
                            <?php } ?>
                        </dd>
                    </dl>
                </div>
                <!-- 하위 옵션 선택 -->
                <div class="product-option-box option-sub" id="ct_period_area" style="display: none;">
                    <dl class="list-style">
                        <dt class="title"><?php echo ($it['it_id']=="S02014") ? '결제날짜' : '배송날짜';?></dt>
                        <dd class="desc">
                            <input type="text" name="ct_period_sdate" class="input-default input-date datepickerbutton" size="10" value="<?php echo date('Y-m-d');?>" readonly/>
                        </dd>
                    </dl>
                    <dl class="list-style">
                        <dt class="title"><?php echo ($it['it_id']=="S02014") ? '결제주기' : '배송주기';?></dt>
                        <dd class="desc">
                            <div class="form-radio radio-sm">
                                <input type="radio" name="ct_period_week" value="1" id="delivery-1week" checked="checked">
                                <label for="delivery-1week">1주 간격</label>
                            </div>
                            <div class="form-radio radio-sm">
                                <input type="radio" name="ct_period_week" value="2" id="delivery-2week">
                                <label for="delivery-2week">2주 간격</label>
                            </div>
                            <div class="form-radio radio-sm">
                                <input type="radio" name="ct_period_week" value="3" id="delivery-3week">
                                <label for="delivery-3week">3주 간격</label>
                            </div>
                            <div class="form-radio radio-sm">
                                <input type="radio" name="ct_period_week" value="4" id="delivery-4week">
                                <label for="delivery-4week">4주 간격</label>
                            </div>
                            <div class="form-radio radio-sm">
                                <input type="radio" name="ct_period_week" value="5" id="delivery-select">
                                <label for="delivery-select">날짜 지정</label>
                            </div>
                        </dd>
                    </dl>
                    <?php if ($it['it_id'] == "S02014") { ?>
                        <dl class="list-style">
                            <dt class="title">결제횟수</dt>
                            <dd class="desc">
                                <div class="input-count-box">
                                    <button type="button" class="count-minus" title="빼기" onclick="fnc_suselect3(0,'ct_period_cnt')"></button>
                                    <span class="count-text">
                                        <input type="text" name="ct_period_cnt" id="ct_period_cnt" title="수량입력" maxlength="2" size="3" onKeyPress="onlyNumber()" value="5" readonly class="input-default input-count">
                                    </span>
                                    <button type="button" class="count-plus" title="더하기" onclick="fnc_suselect3(1,'ct_period_cnt')"></button>
                                </div>
                                <!--<span class="input-guide">1주마다 1박스씩 1회 배송됩니다.</span>-->
                            </dd>
                        </dl>
                    <?php } else { ?>
                        <dl class="list-style">
                            <dt class="title">배송횟수</dt>
                            <dd class="desc">
                                <div class="input-count-box">
                                    <input type="hidden" class="it_period_min" value="<?php echo $it['it_period_min'] ?>" >
                                    <button type="button" class="count-minus" title="빼기" onclick="fnc_suselect4(0,'ct_period_cnt')"></button>
                                    <span class="count-text">
                                        <input type="text" name="ct_period_cnt" id="ct_period_cnt" title="수량입력" maxlength="2" size="3" onKeyPress="onlyNumber()" value="<?php echo round(($it['it_period_min']+$it['it_period_max'])/2) ?>" readonly class="input-default input-count">
                                    </span>
                                    <input type="hidden" class="it_period_max" value="<?php echo $it['it_period_max'] ?>" >
                                    <button type="button" class="count-plus" title="더하기" onclick="fnc_suselect4(1,'ct_period_cnt')"></button>
                                </div>
                                <!--<span class="input-guide">1주마다 1박스씩 1회 배송됩니다.</span>-->
                            </dd>
                        </dl>
                    <?php } ?>

                    <dl class="list-style">
                        <dt class="title">배송메모</dt>
                        <dd class="desc">
                            <div class="input-count-box">
                                <input type="text" name="ct_period_memo" id="ct_period_memo" size="50" maxlength="50" value="" class="input-default">
                            </div>
                        </dd>
                    </dl>
                </div>
            </div>
            <?php } ?>


            <!-- 합계금액-->    
            <div class="product-price-box">
                <p class="product-price-title">상품 금액 합계</p>
                <p class="product-price-desc"><span class="total-price"><?php                    
                        if($is_drug == true){
                            echo number_format($it['it_drug_price']*$it['it_buy_min_qty']);
                        } else{
                            echo number_format($it['it_price']*$it['it_buy_min_qty']);
                        }?></span><span class="unit">원</span></p>
            </div>
             <!-- 버튼목록 -->
            <div class="btn-set btn-set-lg">
                <button class="btn-default btn-orange-border" value="장바구니" type="submit" onclick="document.pressed=this.value;">장바구니</button>
                <button class="btn-default btn-orange" value="바로구매" type="submit" onclick="document.pressed=this.value;" >바로구매</button>
                <button class="btn-default btn-like <?php if (get_wishlist_my_item_check($it['it_id'])) echo 'is-active'; ?>" title="좋아요" type="button" onclick="item_wish(document.fitem, '<?php echo $it['it_id']; ?>');">
                    <input type="hidden" id="wishValue" value="<?php echo (get_wishlist_my_item_check($it['it_id'])) ? 'd' : 'w'; ?>">
                    <i class="icon icon-heart"></i>
                </button>
            </div>            
            <!-- 네이버페이 버튼시작 -->        
            <?php if ($naverpay_button_js  && $it['it_price'] > 0) { ?>
            <div class="itemform-naverpay"><?php echo $naverpay_request_js.$naverpay_button_js; ?></div>
            <?php } ?>
            
            <!-- 네이버페이 끝 
            <script type="text/javascript" >//<![CDATA[
                naver.NaverPayButton.apply({
                BUTTON_KEY: "19B7DF5B-8391-4489-858C-986C2E94FCB6", // 네이버페이에서 제공받은 버튼 인증 키 입력
                TYPE: "E", // 버튼 모음 종류 설정
                COLOR: 1, // 버튼 모음의 색 설정
                COUNT: 2, // 버튼 개수 설정. 구매하기 버튼만 있으면(장바구니 페이지) 1, 찜하기 버튼도 있으면(상품 상세 페이지) 2를 입력.
                ENABLE: "Y", // 품절 등의 이유로 버튼 모음을 비활성화할 때에는 "N" 입력
                "":""
                });
            //]]></script> 
            -->


             <!-- 앱 다운로드-->
            <div class="app-download-box">
                언제 어디서나 간편한 주문! 오더 스테이션 앱
                <a href="http://mb2.taejeongroup.com/mobile_app.html" class="app-download" target="_blank"><span>앱 다운로드</span><i class="icon icon-appdownload"></i></a>
            </div>
        </div>
    </div>

    <section id="sit_right">
        <div class="boxarea" style="display: none;">
            <div class="block likeButton likePrd likePrd_12">

                <i class="fa fa-commenting-o" aria-hidden="true"></i>
                <span class="sound_only">리뷰</span> <?php echo $it['it_use_cnt']; ?>
                <span class="st_bg"></span>
                <i class="fa fa-heart-o" aria-hidden="true"></i>
                <span class="sound_only">위시</span> <?php echo get_wishlist_count_by_item($it['it_id']); ?>
            </div>

            <div class="block ">
                <span class="title">공유하기</span>

                <div class="social">

                    <?php $str = get_sns_share_link('facebook', $sns_url, $sns_title, '/theme/onk/img/icon_facebook.gif');
                    $str .= '&nbsp;';
                    $str .= get_sns_share_link('twitter', $sns_url, $sns_title, '/theme/onk/img/icon_twitter.gif');
                    echo $str;
                    ?>
                    <?php echo get_sns_share_link('kakaotalk', $sns_url, $sns_title, G5_THEME_URL.'/img/kakaolink_btn.png'); ?>
                    <script type='text/javascript'>
                        //<![CDATA[
                        // // 사용할 앱의 JavaScript 키를 설정해 주세요.
                        Kakao.init("<?php echo $config['cf_kakao_js_apikey']; ?>");
                        // // 카카오링크 버튼을 생성합니다. 처음 한번만 호출하면 됩니다.
                        Kakao.Link.createDefaultButton({
                            container: '#kakao-link-btn',
                            objectType: 'feed',
                            content: {
                                title: '<?php echo $it['it_name'];?>',
                                description: '',
                                imageUrl: '<?php echo get_it_imageurl($it['it_id']);?>',
                                link: {
                                    mobileWebUrl: '<?php echo $sns_url;?>',
                                    webUrl: '<?php echo $sns_url;?>'
                                }
                            },
                            /*social: {
                             likeCount: 286,
                             commentCount: 45,
                             sharedCount: 845
                            },*/
                            buttons: [{
                                title: '상품 보기',
                                link: {
                                    mobileWebUrl: '<?php echo $sns_url;?>',
                                    webUrl: '<?php echo $sns_url;?>'
                                }
                            }]
                        });
                        //]]>
                    </script>
                    <ul class="xans-element- xans-product xans-product-customsns xans-record-">
                        <li></li>
                    </ul>
                </div>
            </div>

            <div class="block">
                <span class="title">
                    <a href="#prdReview">상품리뷰</a>
                </span>
                <div class="list_count"><?php echo $item_use_count; ?>개</div>
                <div class="xans-element- xans-product xans-product-additional ">
                </div>
            </div>

            <div class="block">
                <span class="title">
                    <a href="#prdQnA">상품문의</a>
                </span>
                <div class="list_count"><?php echo $item_qa_count; ?>개</div>
                <div class="xans-element- xans-product xans-product-additional ">
                </div>
            </div>
        </div>
    </section>
    </div>
</form>

<script>
    /* =====================================
    *  갤러리 (임의 작성입니다. 작업시 지워주세요)
    * =====================================*/
    galleryFadeinOut();

    function galleryFadeinOut() {
        var gallery = $(".gallery"); // 큰 이미지
        var galleryList = $(".gallery-list .thumb"); // 썸네일 이미지
        $(".gallery-list .thumb").click(function() {
            var imgHref = $(this).find('img').attr('src');
            gallery.find('img').attr('src', imgHref)
            return false;
        });
    }
</script>
<script>
    /* =====================================
    * tooltip (임의 작성입니다. 작업시 지워주세요)
    * =====================================*/
    tooltipFunc();
    function tooltipFunc() {
        $(document).on('click', '.open-tooltip',  function () {
            $('.tooltip-wrapper').removeClass('is-opened');
            $(this).parent('.tooltip-wrapper').addClass('is-opened');
        });
        $(document).on('click', '.close-tooltip',  function () {
            $('.tooltip-wrapper').removeClass('is-opened');
        });
        $('body').click(function (e) {
            if ($('.tooltip-wrapper').hasClass('is-opened')) {
                if (!$('.tooltip-wrapper').has(e.target).length) {
                    $('.tooltip-wrapper').removeClass('is-opened');
                }
            }
        });
    }
</script>

<script>

    // 상품보관
    function item_wish(f, it_id) {
        var f_data = $(f).serialize();
        $.ajax({
            type: "POST",
            data: f_data + "&it_id=" + it_id + "&mode=" + $('#wishValue').val(),
            url: g5_url+"/shop/ajax.wishupdate.php",
            cache: false,
            success: function(result) {
                if (result == 'w') {
                    $('.btn-like').addClass('is-active');
                    $('#wishValue').val('d');
                } else if (result == 'd') {
                    $('.btn-like').removeClass('is-active');
                    $('#wishValue').val('w');
                } else {
                    alert(result);
                }
            }
        });

    }

    // 추천메일
    function popup_item_recommend(it_id) {
        if (!g5_is_member) {
            if (confirm("회원만 추천하실 수 있습니다."))
                document.location.href = "<?php echo G5_BBS_URL; ?>/login.php?url=<?php echo urlencode(G5_SHOP_URL."/item.php?it_id=$it_id"); ?>";
        }
        else {
            url = "./itemrecommend.php?it_id=" + it_id;
            opt = "scrollbars=yes,width=616,height=420,top=10,left=10";
            popup_window(url, "itemrecommend", opt);
        }
    }

    // 재입고SMS 알림
    function popup_stocksms(it_id) {
        url = "<?php echo G5_SHOP_URL; ?>/itemstocksms.php?it_id=" + it_id;
        opt = "scrollbars=yes,width=616,height=420,top=10,left=10";
        popup_window(url, "itemstocksms", opt);
    }

    function fnc_suselect2(str_gbn, obj) {
        if (str_gbn == 0) {
            if (parseInt(document.getElementById(obj).value) > 2) {
                document.getElementById(obj).value = parseInt(document.getElementById(obj).value) - 1;
            }
        } else {
            if (parseInt(document.getElementById(obj).value) < 5) {
                document.getElementById(obj).value = parseInt(document.getElementById(obj).value) + 1;
            }
        }
    }

    function fnc_suselect3(str_gbn, obj) {
        if (str_gbn == 0) {
            if (parseInt(document.getElementById(obj).value) > 2) {
                document.getElementById(obj).value = parseInt(document.getElementById(obj).value) - 1;
            }
        } else {
            if (parseInt(document.getElementById(obj).value) < 12) {
                document.getElementById(obj).value = parseInt(document.getElementById(obj).value) + 1;
            }
        }
    }

    // 배송횟수 설정
    function fnc_suselect4(str_gbn, obj) {

        var it_period_min = $(".it_period_min").val();
        var it_period_max = $(".it_period_max").val();

        if (str_gbn == 0) {
            if (parseInt(document.getElementById(obj).value) > it_period_min) {
                document.getElementById(obj).value = parseInt(document.getElementById(obj).value) - 1;
            }
        } else {
            if (parseInt(document.getElementById(obj).value) < it_period_max) {
                document.getElementById(obj).value = parseInt(document.getElementById(obj).value) + 1;
            }
        }
    }

    function onlyNumber(plus_keycode) {
        if (plus_keycode == "") {
            if (window.event.keyCode >= 48 && window.event.keyCode <= 57) {
            } else {
                window.event.keyCode = "";
            }
        } else {
            if ((window.event.keyCode >= 48 && window.event.keyCode <= 57) || window.event.keyCode == plus_keycode) {
            } else {
                window.event.keyCode = "";
            }
        }
    }

</script>

<script>
$(function () {

    var period_y =  $("#period_y").val();
    if( period_y== 'O'){
        $("#ct_period_yny").attr("checked", true);
        $("#ct_period_area").show();
    }


    // 상품이미지 첫번째 링크
    //$("#sit_pvi_big a:first").addClass("visible");
    $(".gallery a:first").show();

    // 상품이미지 미리보기 (썸네일에 마우스 오버시)
    /*$("#sit_pvi .img_thumb").bind("mouseover focus", function(){
        var idx = $("#sit_pvi .img_thumb").index($(this));
        $("#sit_pvi_big a.visible").removeClass("visible");
        $("#sit_pvi_big a:eq("+idx+")").addClass("visible");
    });*/

    // 상품이미지 크게보기
    $(".popup_item_image").click(function () {
        var url = $(this).attr("href");
        var top = 10;
        var left = 10;
        var opt = 'scrollbars=yes,top=' + top + ',left=' + left;
        popup_window(url, "largeimage", opt);
        return false;
    });
    $("input[name=ct_period_yn]").click(function () {
        if ($(this).val() == "Y") {
            //$("#ct_period_table").show();
            $("#ct_period_area").show();
        } else {
            //$("#ct_period_table").hide();
            $("#ct_period_area").hide();
        }
    });
    /*
    $("#CardInfoText").click(function(){

       $("#card_info2").css("display", "none");
       $("#card_info").slideToggle(300);

    });
    $("#CardInfoText2").click(function(){

       $("#card_info").css("display", "none");
       $("#card_info2").slideToggle(300);

    });
    */
    $("#CardInfoText3").click(function () {
        $("#card_info3").slideToggle(300);
    });
    $(".shopping").click(function () {
        $("#modalLayer").fadeOut("slow");
    });

    $(".gocart").click(function () {
        if ($("input[name=ct_period_yn]:checked").val() != 'Y') {
            location.replace("<?php echo G5_SHOP_URL.'/cart.php'?>");
        } else {
            location.replace("<?php echo G5_SHOP_URL.'/cart.php?period=y'?>");
        }
    });


    // 결제 혜택 안내 팝업
    if($(".item_discount").val() > 0) {
        document.getElementById("item_discount").style.display="";
    }else{
        document.getElementById("item_discount").style.display="none";
    }

});

function fsubmit_check(f) {
    // 판매가격이 0 보다 작다면
    if (document.getElementById("it_price").value < 0) {
        alert("전화로 문의해 주시면 감사하겠습니다.");
        return false;
    }

    if ($(".product-selected-box").length < 1) {
        alert("상품의 선택옵션을 선택해 주십시오.");
        return false;
    }

    var val, io_type, result = true;
    var sum_qty = 0;
    var min_qty = parseInt(<?php echo $it['it_buy_min_qty']; ?>);
    var max_qty = parseInt(<?php echo $it['it_buy_max_qty']; ?>);
    var $el_type = $("input[name^=io_type]");

    $("input[name^=ct_qty]").each(function (index) {
        val = $(this).val();

        if (val.length < 1) {
            alert("수량을 입력해 주십시오.");
            result = false;
            return false;
        }

        if (val.replace(/[0-9]/g, "").length > 0) {
            alert("수량은 숫자로 입력해 주십시오.");
            result = false;
            return false;
        }

        if (parseInt(val.replace(/[^0-9]/g, "")) < 1) {
            alert("수량은 1이상 입력해 주십시오.");
            result = false;
            return false;
        }

        io_type = $el_type.eq(index).val();
        if (io_type == "0")
            sum_qty += parseInt(val);
    });


    if (!result) {
        return false;
    }

    if (min_qty > 0 && sum_qty < min_qty) {
        alert("선택옵션 개수 총합 " + number_format(String(min_qty)) + "개 이상 주문해 주십시오.");
        return false;
    }

    if (max_qty > 0 && sum_qty > max_qty) {
        alert("선택옵션 개수 총합 " + number_format(String(max_qty)) + "개 이하로 주문해 주십시오.");
        return false;
    }

    return true;
}

// 바로구매, 장바구니 폼 전송
function fitem_submit(f) {
    f.action = "<?php echo $action_url; ?>";
    f.target = "";

    var member = $("input[name=member_id]").val();
    var period = $("input[name=ct_period_yn]:checked").val();

    if (document.pressed == "장바구니") {
        f.sw_direct.value = 0;
    } else { // 바로구매
        f.sw_direct.value = 1;
        if (period == 'Y' && member =='') {
            alert("정기배송은 회원전용 서비스 입니다. \n로그인 후 이용하여 주십시오.");
        }
    }

    // 판매가격이 0 보다 작다면
    if (document.getElementById("it_price").value < 0) {
        alert("전화로 문의해 주시면 감사하겠습니다.");
        return false;
    }

    if ($(".product-selected-box").length < 1) {
        alert("상품의 선택옵션을 선택해 주십시오.");
        return false;
    }

    var val, io_type, result = true;
    var sum_qty = 0;
    var min_qty = parseInt(<?php echo $it['it_buy_min_qty']; ?>);
    var max_qty = parseInt(<?php echo $it['it_buy_max_qty']; ?>);
    var $el_type = $("input[name^=io_type]");

    $("input[name^=ct_qty]").each(function (index) {
        val = $(this).val();

        if (val.length < 1) {
            alert("수량을 입력해 주십시오.");
            result = false;
            return false;
        }

        if (val.replace(/[0-9]/g, "").length > 0) {
            alert("수량은 숫자로 입력해 주십시오.");
            result = false;
            return false;
        }

        if (parseInt(val.replace(/[^0-9]/g, "")) < 1) {
            alert("수량은 1이상 입력해 주십시오.");
            result = false;
            return false;
        }

        io_type = $el_type.eq(index).val();
        if (io_type == "0")
            sum_qty += parseInt(val);
    });

    if (!result) {
        return false;
    }

    if (min_qty > 0 && sum_qty < min_qty) {
        alert("선택옵션 개수 총합 " + number_format(String(min_qty)) + "개 이상 주문해 주십시오.");
        return false;
    }

    if (max_qty > 0 && sum_qty > max_qty) {
        alert("선택옵션 개수 총합 " + number_format(String(max_qty)) + "개 이하로 주문해 주십시오.");
        return false;
    }

    if (document.pressed == "장바구니") {

        f.c_status.value = '1';
        $.ajax({
            type: "POST",
            url: "<?php echo G5_SHOP_URL; ?>/cartupdate.php",
            data: $(f).serialize(),
            error: function (error) {
                alert("장바구니에 담기지 않았습니다");
                return false;
            },
            success: function (data) {

                var marginLeft = $(".modalContent").outerWidth() / 2;
                var marginTop = $(".modalContent").outerHeight() / 2;

                $('#right_cart2').html(data);

                if(data > 0){
                    if (period == 'Y' && member =='') {
                        alert("정기배송은 회원전용 서비스 입니다. \n로그인 후 이용하여 주십시오.");
                        window.location.href = "<?php echo G5_BBS_URL?>/login.php?period="+period;
                    } else {
                        $("#modalLayer").fadeIn("slow");
                        $(".modalContent").css({"margin-top": -marginTop, "margin-left": -marginLeft});

                    }
                }
                return false;
            }
        });
        return false;
    } else {
        return true; //바로구매
    }
}

</script>


<style>
.modal{
   width:90%; 
   height:90%; 
   position:fixed; 
   left:0; 
   top:0; 
   z-index:10000;
   }

#modalLayer{
   display:none; 
   }
#modalLayer .modalContent{
   width:300px; 
   height:150px; 
   padding:10px 10px 10px 10px;
   border:1px solid #EAEAEA; 
   position:fixed; 
   left:50%; 
   top:45%; 
   z-index:99; 
   background:#fff; 
   }
#modalLayer .modalContent button{
   border-radius:3px;
   padding:15px 15px;
   cursor:pointer;
   }
#modalLayer .content {
   margin-top:10px;
   text-align:center;
   }
#modalLayer .content p{
   font-weight:700;
   padding:20px;
   }
#modalLayer .content .shopping {
   color:#fff;
   padding:10px 15px;
   background:#FF8224;
   margin-right:10px;
   }
#modalLayer .content .gocart {
   border-radius:3px;
   color:black;
   padding:10px 15px;
   background:#d9d9d9;
   }

</style>

<div id="modalLayer">
  <div class="modal">
   <div class="modalContent">   
    <div class="content">
      <p><strong>선택하신 상품을 장바구니에 담았습니다.</strong></p>
      <button type="button" id="shopping" class="shopping">계속 쇼핑하기</button>
      <button type="button" id="gocart" class="gocart">장바구니 바로가기</button>
    </div>
   </div>
  </div>
</div>
<?php
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가

// add_stylesheet('css 구문', 출력순서); 숫자가 작을 수록 먼저 출력됨
//add_stylesheet('<link rel="stylesheet" href="'.G5_SHOP_CSS_URL.'/style.css">', 0);


$c_status=0; //장바구니이동전 팝업위한 변수
?>
<?php if($config['cf_kakao_js_apikey']) { ?>
<script src="//developers.kakao.com/sdk/js/kakao.min.js"></script>
<?php } ?>

<form name="fitem" method="post" action="<?php echo $action_url; ?>" onsubmit="return fitem_submit(this);">
<input type="hidden" name="it_id[]" value="<?php echo $it_id; ?>">
<input type="hidden" name="sw_direct">
<input type="hidden" name="url">
<input type="hidden" name="c_status" value="<?php echo $c_status ?>;">

<div id="sit_ov_wrap">
	
    <!-- 상품이미지 미리보기 시작 { -->
    <div id="sit_pvi">
        <div id="sit_pvi_big">
        <?php
        $big_img_count = 0;
        $thumbnails = array();
        for($i=1; $i<=10; $i++) {
            if(!$it['it_img'.$i])
                continue;

            $img = get_it_thumbnail($it['it_img'.$i], $default['de_mimg_width'], $default['de_mimg_height']);

            if($img) {
                // 썸네일
                $thumb = get_it_thumbnail($it['it_img'.$i], 60, 60);
                $thumbnails[] = $thumb;
                $big_img_count++;

                echo '<a href="'.G5_SHOP_URL.'/largeimage.php?it_id='.$it['it_id'].'&amp;no='.$i.'" target="_blank" class="popup_item_image">'.$img.'</a>';
            }
        }

        if($big_img_count == 0) {
            echo '<img src="'.G5_SHOP_URL.'/img/no_image.gif" alt="">';
        }
        ?>
        </div>
        <?php
        // 썸네일
        $thumb1 = true;
        $thumb_count = 0;
        $total_count = count($thumbnails);
        if($total_count > 0) {
            echo '<ul id="sit_pvi_thumb">';
            foreach($thumbnails as $val) {
                $thumb_count++;
                $sit_pvi_last ='';
                if ($thumb_count % 5 == 0) $sit_pvi_last = 'class="li_last"';
                    echo '<li '.$sit_pvi_last.'>';
                    echo '<a href="'.G5_SHOP_URL.'/largeimage.php?it_id='.$it['it_id'].'&amp;no='.$thumb_count.'" target="_blank" class="popup_item_image img_thumb">'.$val.'<span class="sound_only"> '.$thumb_count.'번째 이미지 새창</span></a>';
                    echo '</li>';
            }
            echo '</ul>';
        }
        ?>
        
        <!-- 다른 상품 보기 시작 { -->
        <!-- <div id="sit_siblings">
            <?php
            if ($prev_href || $next_href) {
                $prev_title = '<i class="fa fa-caret-left" aria-hidden="true"></i> '.$prev_title;
                $next_title = $next_title.' <i class="fa fa-caret-right" aria-hidden="true"></i>';

                echo $prev_href.$prev_title.$prev_href2;
                echo $next_href.$next_title.$next_href2;
            } else {
                echo '<span class="sound_only">이 분류에 등록된 다른 상품이 없습니다.</span>';
            }
            ?>
            <a href="<?php echo G5_SHOP_URL; ?>/largeimage.php?it_id=<?php echo $it['it_id']; ?>&amp;no=1" target="_blank" class="popup_item_image "><i class="fa fa-search-plus" aria-hidden="true"></i><span class="sound_only">확대보기</span></a>
        </div> -->
        <!-- } 다른 상품 보기 끝 -->

        <!-- <div id="sit_star_sns">
            <?php if ($star_score) { ?>
            <span class="sound_only">고객평점</span> 
            <img src="<?php echo G5_SHOP_URL; ?>/img/s_star<?php echo $star_score?>.png" alt="" class="sit_star" width="100">
            별<?php echo $star_score?>개
            <?php } ?>
            <span class="st_bg"></span> <i class="fa fa-commenting-o" aria-hidden="true"></i><span class="sound_only">리뷰</span> <?php echo $it['it_use_cnt']; ?>
            <span class="st_bg"></span> <i class="fa fa-heart-o" aria-hidden="true"></i><span class="sound_only">위시</span> <?php echo get_wishlist_count_by_item($it['it_id']); ?>
            <button type="button" class="btn_sns_share"><i class="fa fa-share-alt" aria-hidden="true"></i><span class="sound_only">sns 공유</span></button>
            <div class="sns_area"><?php echo $sns_share_links; ?> <a href="javascript:popup_item_recommend('<?php echo $it['it_id']; ?>');" id="sit_btn_rec"><i class="fa fa-envelope-o" aria-hidden="true"></i><span class="sound_only">추천하기</span></a></div>
        </div> -->
        <script>
        $(".btn_sns_share").click(function(){
            $(".sns_area").show();
        });
        $(document).mouseup(function (e){
            var container = $(".sns_area");
            if( container.has(e.target).length === 0)
            container.hide();
        });


        </script>
    </div>
    <!-- } 상품이미지 미리보기 끝 -->

	
    <!-- 상품 요약정보 및 구매 시작 { -->

    <section id="sit_ov" class="2017_renewal_itemform">
        <h2 id="sit_title"><?php echo stripslashes($it['it_name']); ?> <span class="sound_only">요약정보 및 구매</span></h2>
        <p id="sit_desc"><?php echo $it['it_basic']; ?></p>
        <?php if($is_orderable) { ?>
        <p id="sit_opt_info">
            상품 선택옵션 <?php echo $option_count; ?> 개, 추가옵션 <?php echo $supply_count; ?> 개
        </p>
        <?php } ?>
        <div class="sit_info">
		

            <table class="sit_ov_tbl">
            <colgroup>
                <col class="grid_3">
                <col>
            </colgroup>
            <tbody>
            <?php if ($it['it_maker']) { ?>
            <tr>
                <th scope="row">제조사</th>
                <td><?php echo $it['it_maker']; ?></td>
            </tr>
            <?php } ?>

            <?php if ($it['it_origin']) { ?>
            <tr>
                <th scope="row">원산지</th>
                <td><?php echo $it['it_origin']; ?></td>
            </tr>
            <?php } ?>

            <?php if ($it['it_brand']) { ?>
            <tr>
                <th scope="row">브랜드</th>
                <td><?php echo $it['it_brand']; ?></td>
            </tr>
            <?php } ?>

            <?php if ($it['it_model']) { ?>
            <tr>
                <th scope="row">규격</th>
                <td><?php echo $it['it_model']; ?></td>
            </tr>
            <?php } ?>

            <?php if (!$it['it_use']) { // 판매가능이 아닐 경우 ?>
            <tr>
                <th scope="row">판매가격</th>
                <td><!--판매중지--><?php echo display_price(get_price($it)); ?>원</td>
            </tr>
            <?php } else if ($it['it_tel_inq']) { // 전화문의일 경우 ?>
            <tr>
                <th scope="row">판매가격</th>
                <td>전화문의</td>
            </tr>
            <?php } else { // 전화문의가 아닐 경우?>
            <?php if ($it['it_cust_price']) { ?>
           
            <?php } // 시중가격 끝 ?>

            <tr>
                <th scope="row">
				<span class="sct_price_title">소비자가</span>
				
				</th>
                <td>
				<style>
					.sct_price_title {
						font-size:13;font-weight:bold
					}
					.sct_price {
						color: #000;
						font-size: 13;
						font-weight: 700;
						/*letter-spacing: -1px;
						font-family: verdana;*/
					}
				</style>
					
                    <span class="sct_price"><?php echo display_price(get_price($it)); ?>원</span>&nbsp;
					<?php if ($it['it_cust_price']) { ?>
						<span style="text-decoration: line-through;"><?php echo display_price($it['it_cust_price']); ?></span> 
					<?php } ?>
                    <input type="hidden" id="it_price" value="<?php echo get_price($it); ?>">
					<?php if($member['mb_type'] == '1' || $member['mb_type'] == '9') { ?>
					&nbsp;
					포인트 <?php echo number_format($it['it_incen']); ?>
					
					<?php } ?>
                </td>
            </tr>
            <?php } ?>

            <?php
            /* 재고 표시하는 경우 주석 해제
            <tr>
                <th scope="row">재고수량</th>
                <td><?php echo number_format(get_it_stock_qty($it_id)); ?> 개</td>
            </tr>
            */
            ?>

            
            <?php
            $ct_send_cost_label = '배송비';
			$code_comp=$it['comp_code'];

            if($it['it_sc_type'] == 1)
                $sc_method = '무료배송';
            else {
                if($it['it_sc_method'] == 1)
                    $sc_method = '수령후 지불';
                else if($it['it_sc_method'] == 2) {
                    $ct_send_cost_label = '<label for="ct_send_cost">배송비결제</label>';
                    $sc_method = '<select name="ct_send_cost" id="ct_send_cost">
                                      <option value="0">주문시 결제</option>
                                      <option value="1">수령후 지불</option>
                                  </select>';
                }
                else
                    $sc_method = '';


				if($it['it_sc_type'] =='2') $trans_memo = number_format($it['it_sc_price']).'원 ( '.number_format($it['it_sc_minimum']).'원 이상 무료 )';
				if($it['it_sc_type'] =='3') $trans_memo = number_format($it['it_sc_price']).'원';
				if($it['it_sc_type'] =='4') $trans_memo = number_format($it['it_sc_price']).'원 <br>(본 상품은 '.$it['it_sc_qty'].'개 단위 묶음포장 상품으로 '.$it['it_sc_qty']. '개 단위로 배송비 '. number_format($it['it_sc_price']) .'원이 추가 부과됩니다.)';
            }
            ?>
            <tr>
                <th><?php echo $ct_send_cost_label; ?></th>
                <td><?php echo $sc_method; ?> <font color='#FF3300'> <?php echo $trans_memo; ?></font> </td>
            </tr>

            <?php if($it['it_order_close_time']) { ?>
            <tr>
                <th>배송안내</th>
                <td>
					<font color='#FF3300'><?php echo $it['it_order_close_time']; ?>&nbsp; <?php echo $it['it_release_day']; ?></font> 
					<? if($it['it_multisend']){ ?>
					<div style="margin-top:5px;">다중 배송상품인 경우 다중배송상품 <br> 배송 수량을 미리 설정 바랍니다</div>
					<? } ?>
				</td>
            </tr>
            <?php } ?>
            <tr>
                <td colspan="2">
					<div class="sod_option_btn">
						<!--
						<button type="button" class="mod_options" id="CardInfoText">카드무이자할부</button>
						<button type="button" class="mod_options" id="CardInfoText2">부분무이자할부</button>
						<?=CardInfoText();?>
						<?=CardInfoText2();?>
						-->
						<button type="button" style="padding: 0 7px;border: 1px solid #ff8800;color: #ff8800;background: #fff;height: 23px;line-height: 21px;margin: 5px 0 0;" id="CardInfoText3_1" onclick="location.href='/shop/listcomp.php?comp= <?php echo $code_comp; ?>' ">브랜드상품모아보기</button>
						<button type="button" style="padding: 0 7px;border: 1px solid #38b2b9;color: #38b2b9;background: #fff;height: 23px;line-height: 21px;margin: 5px 0 0;" id="CardInfoText3">카드무이자할부 안내</button>
						<?=CardInfoText3();?>
					</div>
				</td>
            </tr>
			<!--
            <?php if($it['it_buy_max_qty']) { ?>
            <tr>
                <th>최대구매수량</th>
                <td><?php echo number_format($it['it_buy_max_qty']); ?> 개</td>
            </tr>
            <?php } ?>
			-->
            </tbody>
            </table>
			
        </div>
		



        <?php
        if($option_item) {
        ?>
        <!-- 선택옵션 시작 { -->
        <section class="sit_option">
            <h3>선택옵션</h3>
 
            <?php // 선택옵션
            echo $option_item;
            ?>
        </section>
        <!-- } 선택옵션 끝 -->
        <?php
        }
        ?>

        <?php
        if($supply_item) {
        ?>
        <!-- 추가옵션 시작 { -->
        <section  class="sit_option">
            <h3>추가옵션</h3>
            <?php // 추가옵션
            echo $supply_item;
            ?>
        </section>
        <!-- } 추가옵션 끝 -->
        <?php
        }
        ?>

        <?php if ($is_orderable) { ?>
        <!-- 선택된 옵션 시작 { -->
        <section id="sit_sel_option">
            <h3>선택된 옵션</h3>
            <?php
            if(!$option_item) {
                if(!$it['it_buy_min_qty'])
                    $it['it_buy_min_qty'] = 1;
            ?>
            <ul id="sit_opt_added">
                <li class="sit_opt_list">
                    
                    <input type="hidden" name="io_type[<?php echo $it_id; ?>][]" value="0">
                    <input type="hidden" name="io_id[<?php echo $it_id; ?>][]" value="">
                    <input type="hidden" name="io_value[<?php echo $it_id; ?>][]" value="<?php echo $it['it_name']; ?>">
                    <input type="hidden" class="io_price" value="0">
                    <input type="hidden" class="io_stock" value="<?php echo $it['it_stock_qty']; ?>">
                    <div class="opt_name">
                        <span class="sit_opt_subj"><?php echo $it['it_name']; ?></span>
                    </div>
                    <div class="opt_count">
                        <label for="ct_qty_<?php echo $i; ?>" class="sound_only">수량</label>
                       <button type="button" class="sit_qty_minus"><i class="fa fa-minus" aria-hidden="true"></i><span class="sound_only">감소</span></button>
                        <input type="text" name="ct_qty[<?php echo $it_id; ?>][]" value="<?php echo $it['it_buy_min_qty']; ?>" id="ct_qty_<?php echo $i; ?>" class="num_input" size="5">
                        <button type="button" class="sit_qty_plus"><i class="fa fa-plus" aria-hidden="true"></i><span class="sound_only">증가</span></button>
                        <span class="sit_opt_prc">+0원</span>
                    </div>
                </li>
            </ul>
            <script>
            $(function() {
                price_calculate();
            });
            </script>
            <?php } ?>
        </section>
        <!-- } 선택된 옵션 끝 -->

        <!-- 총 구매액 -->
        <div id="sit_tot_price"></div>
        <?php } ?>

        <?php if($is_soldout) { ?>
			<? //품절 상태인데 타임세일상품이면 문구를 다르게 보여주기 위해서....
			if( $it['it_type4'] == "1" && $time_sale_yn == "N" ){ ?>
			<p id="sit_ov_soldout">당일 오전 11:00~12:00, 15:00~16:00 에만 구매 가능한 상품입니다.</p>
			<?}else{?>
			<p id="sit_ov_soldout">상품의 재고가 부족하여 구매할 수 없습니다.</p>
			<?}?>
        <?php } ?>

		<? //품절상태가 아니고 타임세일 상품이면서 시간범위를 벗어날때
		if( $it['it_type4'] == "1" && $time_sale_yn == "N" && !$is_soldout ){ 
			$is_orderable = false; //주문 불가능하게 만들기
		?>
		<p id="sit_ov_soldout">당일 오전 11:00~12:00, 15:00~16:00 에만 구매 가능한 상품입니다.</p>
		<? } ?>

		<?php if ($it['it_period_yn'] == 'Y') { ?>
		<style>
		/* table정의 */
		.tbl-list_new {
			line-height: 18px; margin:10px auto;
			border-top:2px solid #6e6e6e; 
		}
		.tbl-list_new th{padding:5px 6px 5px 6px;}
		.tbl-list_new td{padding:5px 6px 5px 6px; }
		.tbl-list_new .bgcolor1{ background:#f6f6f6;}
		.tbl-list_new .bgcolor2{ background:#ffffff;}
		.tbl-list_new .bgcolor3{ background:#deefff;}
		.tbl-list_new .first{border-left:1px solid #c7c7c7;}
		.tbl-list_new .last{border-right:1px solid #c7c7c7;}
		.tbl-list_new .top{border-top:1px solid #c7c7c7; border-bottom:1px solid #c7c7c7;}
		.tbl-list_new .bottom{border-bottom:1px solid #c7c7c7;}
		.tbl-list_new .bottom02{border-bottom:2px solid #c7c7c7;}
		.tbl-list_new .left{ text-align:left;}
		.tbl-list_new .center{ text-align:center;}

		</style>
		<link rel="stylesheet" href="//code.jquery.com/ui/1.8.18/themes/base/jquery-ui.css" />
		<script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
		<script>
			$.datepicker.setDefaults({
				dateFormat: 'yy-mm-dd',
				prevText: '이전 달',
				nextText: '다음 달',
				monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
				monthNamesShort: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
				dayNames: ['일', '월', '화', '수', '목', '금', '토'],
				dayNamesShort: ['일', '월', '화', '수', '목', '금', '토'],
				dayNamesMin: ['일', '월', '화', '수', '목', '금', '토'],
				showMonthAfterYear: true,
				yearSuffix: '년',
				beforeShowDay: function(date){
					var day = date.getDay();
					return [(day != 0 && day != 6)];}
			});

			$(function() {
				$(".datepickerbutton").datepicker({minDate: 0});
			});
		</script>
		<div class="sub_pd_top_ct_rf_03">
			<ul>
				<li style="font-size:12px"><strong>정기주문 :</strong> 
				    <input type="radio" name="ct_period_yn" id="ct_period_yny" value="Y"> <label for="ct_period_yny">신청함</label>
					<input type="radio" name="ct_period_yn" id="ct_period_ynn" value="N" onBlur="" checked> <label for="ct_period_ynn">신청안함</label>
				</li>
			</ul>
		</div>           
			   
		<table width="100%" cellpadding="0" cellspacing="0" class="tbl-list_new" id="ct_period_table" style="display:none;">
		  <tr>
			<td width="142" align="center" class="bgcolor1 bottom last font_bold">배송시작일</td>
			<td align="left" bgcolor="#e9f2da" class="bgcolor2 bottom"><input type="text" name="ct_period_sdate" id="ct_period_sdate" class="datepickerbutton"  size="10" value="<?php echo date('Y-m-d');?>" readonly></td>
		  </tr>
		  <tr>
			<td align="center" class="bottom last font_bold bgcolor1">배송주기</td>
			<td align="left" class="bottom bgcolor2">
			<? if($it_id == "S00687"||$it_id == "S00688"){ ?>
			  <input name="ct_period_week" id="ct_period_week2" type="radio" value="2" checked>
			  <label for="ct_period_week2">2주마다</label>&nbsp;&nbsp;
			<? }else{ ?>
			  <input name="ct_period_week" id="ct_period_week1" type="radio" value="1" checked>
			  <label for="ct_period_week1">매주</label>&nbsp;&nbsp;
			  <input name="ct_period_week" id="ct_period_week2" type="radio" value="2">
			  <label for="ct_period_week2">2주마다</label>&nbsp;&nbsp;
			  <input name="ct_period_week" id="ct_period_week3" type="radio" value="3">
			  <label for="ct_period_week3">3주마다</label>&nbsp;&nbsp;
			  <input name="ct_period_week" id="ct_period_week4" type="radio" value="4">
			  <label for="ct_period_week4">4주마다</label>
			  <input name="ct_period_week" id="ct_period_week5" type="radio" value="5">
			  <label for="ct_period_week5">지정일자마다</label>
			<? } ?>
			</td>
		  </tr>
		  <tr>
			<td align="center" class="bottom last font_bold bgcolor1">배송횟수</td>
			<td align="left" class="bottom bgcolor2">
				<!--
				<a href="javascript:fnc_suselect2(0,'ct_period_cnt')">
					<span style="font-size:1.4em;">&nbsp;-&nbsp;</span>
				</a>
				-->
				<input type="text" name="ct_period_cnt" id="ct_period_cnt" style="ime-mode:disabled;text-align:right;border:none;border-right:0px; border-top:0px; boder-left:0px; boder-bottom:0px;" maxlength="2" size="3" onKeyPress="onlyNumber()" value="5" readonly>
				<!--
				<a href="javascript:fnc_suselect2(1,'ct_period_cnt')">
					<span style="font-size:1.4em;">&nbsp;+&nbsp;</span>
				</a>
				-->
				회 (<font color="#FF0000">주문수량 * 배송횟수</font>)
			</td>
		  </tr>
		  <tr>
			<td align="center" class="bottom last font_bold bgcolor1">배송요구사항</td>
			<td align="left" class="bottom bgcolor2"><input type="text" name="ct_period_memo" id="ct_period_memo" size="50" maxlength="50" value=""></td>
		  </tr>
		</table>
		<br>
		<?php } else {?>
			

		<?php } ?>









        <div id="sit_ov_btn">
            <?php if ($is_orderable) { ?>
            <button type="submit" onclick="document.pressed=this.value;" value="바로구매" id="sit_btn_buy" class="first  btn btn-big btn-red btn-buy" >바로구매</button>
            <button type="submit" onclick="document.pressed=this.value;" value="장바구니" id="sit_btn_cart" class=" btn btn-big btn-cart ">장바구니</button>
            <?php } ?>
            <?php if(!$is_orderable && $it['it_soldout'] && $it['it_stock_sms']) { ?>
            <a href="javascript:popup_stocksms('<?php echo $it['it_id']; ?>');" id="sit_btn_alm"><i class="fa fa-bell-o" aria-hidden="true"></i> 재입고알림</a>
            <?php } ?>
			<?php if ($is_orderable) { ?>
            <a href="javascript:item_wish(document.fitem, '<?php echo $it['it_id']; ?>');" id="sit_btn_wish" class="btn btn-big btn-wish"><i class="fa fa-heart-o" aria-hidden="true"></i></a>
            <?php } ?>
            <?php if($naverpay_button_js && $is_orderable){ ?>
            <div class="itemform-naverpay"><?php echo $naverpay_request_js.$naverpay_button_js; ?></div>
            <?php } ?>
        </div>

        <script>
        // 상품보관
        function item_wish(f, it_id)
        {
            f.url.value = "<?php echo G5_SHOP_URL; ?>/wishupdate.php?it_id="+it_id;
            f.action = "<?php echo G5_SHOP_URL; ?>/wishupdate.php";
            f.submit();
        }

        // 추천메일
        function popup_item_recommend(it_id)
        {
            if (!g5_is_member)
            {
                if (confirm("회원만 추천하실 수 있습니다."))
                    document.location.href = "<?php echo G5_BBS_URL; ?>/login.php?url=<?php echo urlencode(G5_SHOP_URL."/item.php?it_id=$it_id"); ?>";
            }
            else
            {
                url = "./itemrecommend.php?it_id=" + it_id;
                opt = "scrollbars=yes,width=616,height=420,top=10,left=10";
                popup_window(url, "itemrecommend", opt);
            }
        }

        // 재입고SMS 알림
        function popup_stocksms(it_id)
        {
            url = "<?php echo G5_SHOP_URL; ?>/itemstocksms.php?it_id=" + it_id;
            opt = "scrollbars=yes,width=616,height=420,top=10,left=10";
            popup_window(url, "itemstocksms", opt);
        }

		function fnc_suselect2(str_gbn,obj) {
			if (str_gbn==0) {
				if (parseInt(document.getElementById(obj).value) > 1) {
					document.getElementById(obj).value = parseInt(document.getElementById(obj).value) -1;
				}
			} else {
				if (parseInt(document.getElementById(obj).value) < 5) {
					document.getElementById(obj).value = parseInt(document.getElementById(obj).value) +1;
				}
			}
		}
		function onlyNumber(plus_keycode)
		{
			if (plus_keycode == ""){
				if( window.event.keyCode >= 48 && window.event.keyCode <= 57) {
				} else {
					window.event.keyCode = "";
				}
			}else{
				if( (window.event.keyCode >= 48 && window.event.keyCode <= 57) || window.event.keyCode == plus_keycode ) {
				} else {
					window.event.keyCode = "";
				}
			}
		}



        </script>
    </section>
    <!-- } 상품 요약정보 및 구매 끝 -->


	<section id="sit_right">
	<div class="boxarea">
			<div class="block likeButton likePrd likePrd_12">
				
				 <i class="fa fa-commenting-o" aria-hidden="true"></i><span class="sound_only">리뷰</span> <?php echo $it['it_use_cnt']; ?>
            <span class="st_bg"></span> <i class="fa fa-heart-o" aria-hidden="true"></i><span class="sound_only">위시</span> <?php echo get_wishlist_count_by_item($it['it_id']); ?>
			</div>

			<div class="block ">
				<span class="title">공유하기</span>

				<div class="social">
				
				<?php $str = get_sns_share_link('facebook', $sns_url, $sns_title, '/theme/onk/img/icon_facebook.gif');
				      $str .= '&nbsp;';
				       $str .= get_sns_share_link('twitter', $sns_url, $sns_title, '/theme/onk/img/icon_twitter.gif');
				echo $str;

				

				?>
				<?php echo get_sns_share_link('kakaotalk', $sns_url, $sns_title, G5_THEME_URL.'/img/kakaolink_btn.png'); ?>
<script type='text/javascript'>
  //<![CDATA[
    // // 사용할 앱의 JavaScript 키를 설정해 주세요.
    Kakao.init("<?php echo $config['cf_kakao_js_apikey']; ?>");
    // // 카카오링크 버튼을 생성합니다. 처음 한번만 호출하면 됩니다.
    Kakao.Link.createDefaultButton({
      container: '#kakao-link-btn',
      objectType: 'feed',
      content: {
    	title: '<?php echo $it['it_name'];?>',
    	description: '',
    	imageUrl: '<?php echo get_it_imageurl($it['it_id']);?>',
    	link: {
    	  mobileWebUrl: '<?php echo $sns_url;?>',
    	  webUrl: '<?php echo $sns_url;?>'
    	}
      },
      /*social: {
    	likeCount: 286,
    	commentCount: 45,
    	sharedCount: 845
      },*/
      buttons: [{
    	title: '상품 보기',
    	link: {
    	  mobileWebUrl: '<?php echo $sns_url;?>',
    	  webUrl: '<?php echo $sns_url;?>'
        }
      }]
    });
  //]]>
</script>				
				<ul class="xans-element- xans-product xans-product-customsns xans-record-"><li></li>
</ul>
</div>
			</div>

			<div class="block">
				<span class="title"><a href="#prdReview">상품리뷰</a></span>
				<div class="list_count"><?php echo $item_use_count; ?>개</div>
				<div class="xans-element- xans-product xans-product-additional ">
</div>
			</div>

			<div class="block">
				<span class="title"><a href="#prdQnA">상품문의</a></span>
				<div class="list_count"><?php echo $item_qa_count; ?>개</div>
				<div class="xans-element- xans-product xans-product-additional ">
</div>
			</div>
		</div>
	</section>
	<style>
#sit_right {
    position: absolute;
    float: right;
	margin-left:1080px;
   
    height: auto !important;
    height: 355px;
    min-height: 355px;
}
.boxarea {
   width: 120px;
   margin-top:1px;
    right: 0;
    height: 100%;
    background: #f6f6f6;
    background: -webkit-linear-gradient(#f6f6f6,#fff);
    background: -o-linear-gradient(#f6f6f6,#fff);
    background: -moz-linear-gradient(#f6f6f6,#fff);
    background: linear-gradient(#f6f6f6,#fff);
}
.boxarea .block {
    border-top: 1px dotted #ddd;
    width: 90px;
    margin: 0 auto;
    padding: 20px 0;
    text-align: center;
}
.boxarea .list_count {
    font-size: 12px;
    font-weight: bold;
    padding: 0 0 3px 0;
}

	</style>


</div>

</form>


<script>
$(function(){
    // 상품이미지 첫번째 링크
    $("#sit_pvi_big a:first").addClass("visible");

    // 상품이미지 미리보기 (썸네일에 마우스 오버시)
    $("#sit_pvi .img_thumb").bind("mouseover focus", function(){
        var idx = $("#sit_pvi .img_thumb").index($(this));
        $("#sit_pvi_big a.visible").removeClass("visible");
        $("#sit_pvi_big a:eq("+idx+")").addClass("visible");
    });

    // 상품이미지 크게보기
    $(".popup_item_image").click(function() {
        var url = $(this).attr("href");
        var top = 10;
        var left = 10;
        var opt = 'scrollbars=yes,top='+top+',left='+left;
        popup_window(url, "largeimage", opt);

        return false;
    });
	$("input[name=ct_period_yn]").click(function(){
		if($(this).val() == "Y"){
			$("#ct_period_table").show();	
		}else{
			$("#ct_period_table").hide();	
		}
	
	});
	/*
	$("#CardInfoText").click(function(){

		$("#card_info2").css("display", "none");   
		$("#card_info").slideToggle(300);
	
	});
	$("#CardInfoText2").click(function(){

		$("#card_info").css("display", "none");   
		$("#card_info2").slideToggle(300);
	
	});
	*/
	$("#CardInfoText3").click(function(){

		$("#card_info3").slideToggle(300);
	
	});
	
	$(".shopping").click(function(){
    $("#modalLayer").fadeOut("slow");
	});	
  
	$(".gocart").click(function(){
	 if($("input[name=ct_period_yn]:checked").val() != 'Y'){
	location.replace("<?php echo G5_SHOP_URL.'/cart.php'?>");
	 }else
	 {
		 location.replace("<?php echo G5_SHOP_URL.'/cart.php?period=y'?>");
	 }
	}); 

});

function fsubmit_check(f)
{
    // 판매가격이 0 보다 작다면
    if (document.getElementById("it_price").value < 0) {
        alert("전화로 문의해 주시면 감사하겠습니다.");
        return false;
    }

    if($(".sit_opt_list").size() < 1) {
        alert("상품의 선택옵션을 선택해 주십시오.");
        return false;
    }

    var val, io_type, result = true;
    var sum_qty = 0;
    var min_qty = parseInt(<?php echo $it['it_buy_min_qty']; ?>);
    var max_qty = parseInt(<?php echo $it['it_buy_max_qty']; ?>);
    var $el_type = $("input[name^=io_type]");

    $("input[name^=ct_qty]").each(function(index) {
        val = $(this).val();

        if(val.length < 1) {
            alert("수량을 입력해 주십시오.");
            result = false;
            return false;
        }

        if(val.replace(/[0-9]/g, "").length > 0) {
            alert("수량은 숫자로 입력해 주십시오.");
            result = false;
            return false;
        }

        if(parseInt(val.replace(/[^0-9]/g, "")) < 1) {
            alert("수량은 1이상 입력해 주십시오.");
            result = false;
            return false;
        }

        io_type = $el_type.eq(index).val();
        if(io_type == "0")
            sum_qty += parseInt(val);
    });
	

    if(!result) {
        return false;
    }

    if(min_qty > 0 && sum_qty < min_qty) {
        alert("선택옵션 개수 총합 "+number_format(String(min_qty))+"개 이상 주문해 주십시오.");
        return false;
    }

    if(max_qty > 0 && sum_qty > max_qty) {
        alert("선택옵션 개수 총합 "+number_format(String(max_qty))+"개 이하로 주문해 주십시오.");
        return false;
    }

    return true;
}

// 바로구매, 장바구니 폼 전송
function fitem_submit(f)
{
    f.action = "<?php echo $action_url; ?>";
    f.target = "";

    if (document.pressed == "장바구니") {
        f.sw_direct.value = 0;
    } else { // 바로구매
        f.sw_direct.value = 1;
    }

    // 판매가격이 0 보다 작다면
    if (document.getElementById("it_price").value < 0) {
        alert("전화로 문의해 주시면 감사하겠습니다.");
        return false;
    }

    if($(".sit_opt_list").size() < 1) {
        alert("상품의 선택옵션을 선택해 주십시오.");
        return false;
    }

    var val, io_type, result = true;
    var sum_qty = 0;
    var min_qty = parseInt(<?php echo $it['it_buy_min_qty']; ?>);
    var max_qty = parseInt(<?php echo $it['it_buy_max_qty']; ?>);
    var $el_type = $("input[name^=io_type]");

    $("input[name^=ct_qty]").each(function(index) {
        val = $(this).val();

        if(val.length < 1) {
            alert("수량을 입력해 주십시오.");
            result = false;
            return false;
        }

        if(val.replace(/[0-9]/g, "").length > 0) {
            alert("수량은 숫자로 입력해 주십시오.");
            result = false;
            return false;
        }

        if(parseInt(val.replace(/[^0-9]/g, "")) < 1) {
            alert("수량은 1이상 입력해 주십시오.");
            result = false;
            return false;
        }

        io_type = $el_type.eq(index).val();
        if(io_type == "0")
            sum_qty += parseInt(val);
    });
	$("input[name^=io_id]").each(function(index) {
        val = $(this).val();
        if(val == '188018') {
			if($("input[name=ct_period_yn]:checked").val() == 'N') {
				alert("정기주문상품입니다. 정기주문을 신청해주세요.");
				result = false;
				return false;
			}
        }
    });

    if(!result) {
        return false;
    }

    if(min_qty > 0 && sum_qty < min_qty) {
        alert("선택옵션 개수 총합 "+number_format(String(min_qty))+"개 이상 주문해 주십시오.");
        return false;
    }

    if(max_qty > 0 && sum_qty > max_qty) {
        alert("선택옵션 개수 총합 "+number_format(String(max_qty))+"개 이하로 주문해 주십시오.");
        return false;
    }

          if (document.pressed == "장바구니") {

        f.c_status.value='1';
		$.ajax({ 
		type:"POST",		
		url :"<?php echo G5_SHOP_URL; ?>/cartupdate.php",
		data: $(f).serialize(),
		error : function(error) {
		alert("장바구니에 담기지 않았습니다");
		return false;
			},
		success: function (data){

			var marginLeft = $(".modalContent").outerWidth()/2;
            var marginTop = $(".modalContent").outerHeight()/2; 
	
			$("#modalLayer").fadeIn("slow");
            $(".modalContent").css({"margin-top" : -marginTop, "margin-left" : -marginLeft});
			//console.log("팝업띄우기 " + document.pressed );
		    $('#right_cart2').html(data);
		return false;			

	}		
		});

        return false;
    } else {
        return true;
    }
}

</script>
<style>
.modal{
	width:100%; 
	height:100%; 
	position:fixed; 
	left:0; 
	top:0; 
	z-index:10000;
	}

#modalLayer{
	display:none; 
	}
#modalLayer .modalContent{
	width:350px; 
	height:190px; 
	padding:40px 10px 10px 10px;
	border:1px solid #EAEAEA; 
	position:fixed; 
	left:50%; 
	top:45%; 
	z-index:99; 
	background:#fff; 
	}
#modalLayer .modalContent button{
	border-radius:3px;
	padding:10px 15px;
	cursor:pointer;
	}
#modalLayer .content {
	margin-top:10px;
	text-align:center;
	}
#modalLayer .content p{
	font-weight:700;
	padding:20px;
	margin-bottom:15px;
	}
#modalLayer .content .shopping {
	color:#fff;
	padding:10px 15px;
	background:#FF8224;
	margin-right:10px;
	}
#modalLayer .content .gocart {
	border-radius:3px;
	color:black;
	padding:10px 15px;
	background:#d9d9d9;
	}

</style>
<?php /* 2017 리뉴얼한 테마 적용 스크립트입니다. 기존 스크립트를 오버라이드 합니다. */ ?>
<script src="<?php echo G5_JS_URL; ?>/shop.override.js?ver=<?php echo G5_JS_VER; ?>"></script>
<div id="modalLayer">
  <div class="modal">
   <div class="modalContent">   
	 <div class="content">
		<p><strong>선택하신 상품을 장바구니에 담았습니다.</strong></p>
		<button type="button" id="shopping" class="shopping">계속 쇼핑하기</button>
		<button type="button" id="gocart" class="gocart">장바구니 바로가기</button>
	 </div>
   </div>
  </div>
</div>
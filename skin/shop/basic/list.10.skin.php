<?php
include_once('./_common.php');
include_once(G5_THEME_SHOP_PATH.'/shop.head.php');

if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가

global $member;


// add_stylesheet('css 구문', 출력순서); 숫자가 작을 수록 먼저 출력됨
add_stylesheet('<link rel="stylesheet" href="'.G5_SHOP_SKIN_URL.'/style.css">', 0);
?>
<style>
.sct_img {   
	margin:-1px;
    border: 1px  rgba(153,153,153,0.1);
}
.sct_img:hover {   
    border: 1px  rgba(153,153,153,0.6);
}

.event_trans {
	font-size:15px;
	width:45px;
	height:45px;
	line-height:20px;
	display:inline;
	z-index:100;
	position:absolute;
	top:9px;
	right:66px;
	background:#ff8100;
	color:#fff;
	text-align:center;
	border-radius:50px;
	font-weight:bold;
	letter-spacing:1px;
	box-sizing:border-box;
	box-shadow:inset 0px 0px 2px 0px rgba(189,189,189,1);
}
</style>

<!-- <div class="xans-element- xans-product xans-product-listnormal ec-base-product">           
<ul class="prdList grid5"> -->
<!-- 상품진열 10 시작 { -->
<?php
// is_drug=true 일 경우
// 포인트 보여짐, 오피스디포 카테고리 보여짐
$is_drug = false;
if($member['mb_type'] === '1' || $member['mb_type'] === '9') {
	$is_drug = true;
} else if($member['mb_type'] == ''){
    $ismember = "logout";
}


for ($i=1; $row=sql_fetch_array($result); $i++) {


	if($row['it_cust_price'] > 0) {
		$is_sale=true;
	} else {
		$is_sale=false;
	}

    if ($this->list_mod >= 2) { // 1줄 이미지 : 2개 이상
        if ($i%$this->list_mod == 0) $sct_last = 'sct_last'; // 줄 마지막
        else if ($i%$this->list_mod == 1) $sct_last = 'sct_clear'; // 줄 첫번째
        else $sct_last = '';
    } else { // 1줄 이미지 : 1개
        $sct_last = 'sct_clear';
    }

    if ($i == 1) {
        if ($this->css) {
            echo "<ul class=\"{$this->css}\">\n";
        } else {
            echo "<ul class=\"sct sct_10\">\n";
        }
    }
    if (get_item_auth_member($member['mb_id'], $row['it_id']) == false && get_item_auth_check($row['it_id'], 1) == true) {
        echo "";
    } else {
        echo "<li class=\"sct_li {$sct_last}\" style=\"width:{$this->img_width}px\">\n";

        echo "<div class=\"sct_img\">\n";

        $array_t = array ('S01754', 'S01753', 'S01752', 'S01751', 'S01750');
        if( in_array($row['it_id'] ,$array_t) ){
            echo" <span class=\"event_trans\">추가할인</span> ";
        }
        if( $row['it_id'] == "S01755"){
            echo" <span class=\"event_trans\">타임세일</span> ";
        }


        if ($this->href) {
            echo "<a href=\"{$this->href}{$row['it_id']}\">\n";
        }

        if ($this->view_it_img) {
            echo get_it_image($row['it_id'], $this->img_width, $this->img_height, '', '', stripslashes($row['it_name']))."\n";
        }

        if ($this->href) {
            echo "</a>\n";
        }
        $whishlist_item_1 = get_wishlist_my_item_check($row['it_id']) ? 'is-active' : '';
        $whishlist_item_2 = get_wishlist_my_item_check($row['it_id']) ? 'd' : 'w';


        echo "<button id='{$row['it_id']}' value='{$row['it_id']}' class='icon icon-heart {$whishlist_item_1}' title='좋아요' type='button' onclick=\"item_wish(document.fitem,'{$row['it_id']}');\"></button>";
        echo "<input type='hidden' id='wishValue' value='{$whishlist_item_2}'>";
        echo "<input type='hidden' id='ismember' value='{$ismember}'>";


        if ($this->view_it_id) {
            echo "<div class=\"sct_id\">&lt;".stripslashes($row['it_id'])."&gt;</div>\n";
        }

        if ($this->view_it_brand) {
            if($row['it_brand']){
                echo "<p class=\"thumbnail-brand-name\">{$row['it_brand']}</p>";
            } else{
                echo "<p class=\"thumbnail-brand-name\">ㅤㅤㅤㅤㅤㅤㅤㅤㅤㅤ</p>";
            }

        }

        if ($this->href) {
            echo "<div class=\"thumbnail-product-name\"><a href=\"{$this->href}{$row['it_id']}\">";
        }

        if ($this->view_it_name) {
            echo stripslashes($row['it_name'])."\n";
        }

        if ($this->href) {
            echo "</a></div>\n";
        }

        //if ($this->view_it_basic && $row['it_basic']) {
        //    echo "<div class=\"sct_basic\">".stripslashes($row['it_basic'])."</div>\n";
        //}
        if ($this->view_it_price) {
            $it_price = $row['it_cust_price'] > 0 ? number_format($row['it_cust_price']) : number_format($row['it_price']);
            $data_prod_price = $row['it_drug_price'] > 0 ? $row['it_drug_price'] : $row['it_drug_ori_price'];
            $data_prod_custom = $row['it_cust_price'] > 0 ? $row['it_cust_price'] : $row['it_price'];
        echo "<div class='sct_cost'>";
            // 공급자가 표시
            if ($is_drug) {
                echo "<span class='sct_price_title'>약국공급가</span>";
                    echo "<div class='pay-area'>";
                        echo "<span class='pay-price'>";
                                 echo "<span class='price'>".display_price($row['it_drug_price'])." 원</span>";
                        echo "</span>";
                        echo "<span class='original-price'>";
                              echo "<span class='price'>".$it_price."</span> ";
                              echo "<span class='unit'>원</span>";
                        echo "</span>";
                        echo "<span class='discount-percent' data-prod-price=\"{$data_prod_price}\" data-prod-custom=\"{$data_prod_custom}\"></span>";
                   echo "</div>";
            } else {
                // 소비자가
                echo "<span class='sct_price_title'>소비자가</span>";
                     echo "<div class='pay-area'>";
                        echo "<span class='pay-price'>";
                             echo "<span class='price'>".display_price($row['it_price'])."</span>";
                             echo "<span class='unit'>원</span>";
                        echo " </span>";
                if ($row['it_cust_price'] > 0) {
                     echo " <span class='original-price'><span class='price'> ".number_format($row['it_cust_price'])."</span><span class='unit'>원</span></span>";
                     echo "<span class='discount-percent' data-prod-price=\"{$row['it_price']}\" data-prod-custom=\"{$row['it_cust_price']}\"></span>";
                }
                     echo "</div>";
            }
              echo "<p class='deliver-info'>";
                if($is_drug) {
                    echo "<span class='info'>포인트";
                     if($row['it_cust_price'] >0 && $row['it_drug_ori_price']>0) {
                         echo number_format($row['it_cust_price'] - $row['it_drug_ori_price']);
                     } else {
                         echo number_format($row['it_price'] - $row['it_drug_price']);
                     }
                     echo "</span>";
                }

                if($row['it_sc_type'] == "1"){
                    echo "<span class='info'>무료배송</span>";
                } else {
                    echo "<span class='info''>유료배송</span>";
                }
                echo "</p>";



              echo "<div class='panel-set'>";
                if ($row['it_type3'] == 1) {
                    echo "<span class='panel panel-new'>NEW</span>";
                }
                if ($row['it_type1'] == 1) {
                    echo "<span class='panel panel-best'>BEST</span>";
                }
                if ($row['it_type2'] == 1) {
                    echo "<span class='panel panel-reco'>추천상품</span>";
                }
                if ($row['it_supply_subject'] == 1) {
                    echo "<span class='panel panel-gift'>추가증정</span>";
                }
                if ($row['it_company'] != 0 && ($member['mb_referee'] == 4 || $member['mb_type'] == 9)) {
                    echo "<span class='panel panel-company'>사내수령</span>";
                }
              echo "</div>";

            }


        echo "</div>\n";


        }

        //if ($this->view_it_icon) {
        //    echo "<div class=\"sct_icon\">".item_icon($row)."</div>\n";s
        //}
        ?>
   <!--     <div class="icon">
            <div class="promotion">
                <?php /*if( $row['it_type1']==1) { */?>
                <img src="<?php /*echo G5_THEME_URL;*/?>/img/icon/icon_best.jpg" class="icon_img" alt="Best"/>
                <?php /*} */?>
                <?php /*if( $row['it_type2']==1) { */?>
                <img src="<?php /*echo G5_THEME_URL;*/?>/img/icon/icon_recommand.jpg" class="icon_img" alt="추천"/>
                <?php /*} */?>
                <?php /*if( $row['it_type3']==1) { */?>
                <img src="<?php /*echo G5_THEME_URL;*/?>/img/icon/icon_new.jpg" class="icon_img" alt="New"/>
                <?php /*} */?>
            </div>
        </div>-->
        <?php
        echo "</li>\n";

}

//if ($i > 1) echo "</ul>\n";
?>
<!-- 
		</ul>
	</div> -->
<script>
    // 상품보관
    function item_wish(f, it_id) {

        var f_data = $(f).serialize();
        $.ajax({
            type: "POST",
            data: f_data + "&it_id=" + it_id + "&mode=" + $('#wishValue').val(),
            url: g5_url+"/shop/ajax.wishupdate.php",
            cache: false,
            success: function(result) {
                if (result == 'w') {
                    document.getElementById(it_id).className = 'icon icon-heart is-active';
                    $('#wishValue').val('d');
                   /* location.replace('/shop/wishlist.php');*/
                } else if (result == 'd') {
                    document.getElementById(it_id).className = 'icon icon-heart';
                    $('#wishValue').val('w');
                } else{
                    if($('#ismember').val()){
                        alert('회원 전용 서비스 입니다.');
                        location.replace('/bbs/login.php');
                    }
                }
            }
        });
    }
</script>
<?php
if($i == 1) echo "<p class=\"sct_noitem\">등록된 상품이 없습니다.</p>\n";
?>
<!-- } 상품진열 10 끝 -->
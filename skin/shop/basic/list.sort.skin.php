<?php
if (!defined("_GNUBOARD_")) exit; // 개별 페이지 접근 불가

$sct_sort_href = $_SERVER['SCRIPT_NAME'].'?';
if($ca_id)
    $sct_sort_href .= 'ca_id='.$ca_id;
else if($ev_id)
    $sct_sort_href .= 'ev_id='.$ev_id;
if($skin)
    $sct_sort_href .= '&amp;skin='.$skin;
$sct_sort_href .= '&amp;sort=';

// add_stylesheet('css 구문', 출력순서); 숫자가 작을 수록 먼저 출력됨
add_stylesheet('<link rel="stylesheet" href="'.G5_SHOP_CSS_URL.'/style.css">', 0);
?>

<!-- 상품 정렬 선택 시작 { -->
<!-- <section id="sct_sort">
    <h2>상품 정렬</h2>
    <ul id="ssch_sort">
        <li><a href="<?php echo $sct_sort_href; ?>it_sum_qty&amp;sortodr=desc">판매많은순</a></li>
        <li><a href="<?php echo $sct_sort_href; ?>it_price&amp;sortodr=asc">낮은가격순</a></li>
        <li><a href="<?php echo $sct_sort_href; ?>it_price&amp;sortodr=desc">높은가격순</a></li>
        <li><a href="<?php echo $sct_sort_href; ?>it_use_avg&amp;sortodr=desc">평점높은순</a></li>
        <li><a href="<?php echo $sct_sort_href; ?>it_use_cnt&amp;sortodr=desc">후기많은순</a></li>
        <li><a href="<?php echo $sct_sort_href; ?>it_update_time&amp;sortodr=desc">최근등록순</a></li>
    </ul>
</section> -->
<!-- } 상품 정렬 선택 끝 -->
<style>
 .function {
    overflow: hidden;
    text-align: right;
    line-height: 38px;
    margin-bottom: 20px;
}
.prdCount {
    float: left;
    color: #888888;
    font-size: 12px;
    letter-spacing: 1px;
}
.xans-product-normalmenu ul#type li {
    display: inline;
    border: 1px solid #e9e9e9;
    padding: 3px 10px 4px;
    font-size: 11px;
}
.xans-product-normalmenu ul#type li a{
    color: #9d9d9d;
}
.xans-product-normalmenu ul#type li .active {
    color: #ff8800;
}
</style>
<div class="xans-product-normalmenu function">
	<p class="prdCount"><strong><?php echo $total_count;?></strong>개의 상품이 등록되어 있습니다.</p>
	<ul id="type" class="xans-element- xans-product xans-product-orderby">
		<li class="xans-record-"><a href="<?php echo $sct_sort_href; ?>it_sum_qty&amp;sortodr=desc"><span class="<?php if($sort=='it_sum_qty' && $sortodr=='desc') echo 'active';?>">판매많은순</span></a></li>
		<li class="xans-record-"><a href="<?php echo $sct_sort_href; ?>it_price&amp;sortodr=asc"><span class="<?php if($sort=='it_price' && $sortodr=='asc') echo 'active';?>">낮은가격순</span></a></li>
		<li class="xans-record-"><a href="<?php echo $sct_sort_href; ?>it_price&amp;sortodr=desc"><span class="<?php if($sort=='it_price' && $sortodr=='desc') echo 'active';?>">높은가격순</span></a></li>
		<li class="xans-record-"><a href="<?php echo $sct_sort_href; ?>it_use_avg&amp;sortodr=desc"><span class="<?php if($sort=='it_use_avg' && $sortodr=='desc') echo 'active';?>">평점높은순</span></a></li>
		<li class="xans-record-"><a href="<?php echo $sct_sort_href; ?>it_use_cnt&amp;sortodr=desc"><span class="<?php if($sort=='it_use_cnt' && $sortodr=='desc') echo 'active';?>">후기많은순</span></a></li>
		<li class="xans-record-"><a href="<?php echo $sct_sort_href; ?>it_update_time&amp;sortodr=desc"><span class="<?php if($sort=='it_update_time' && $sortodr=='desc') echo 'active';?>">최근등록순</span></a></li>
	</ul>
</div>
<?php
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가

// add_stylesheet('css 구문', 출력순서); 숫자가 작을 수록 먼저 출력됨
add_stylesheet('<link rel="stylesheet" href="'.G5_SHOP_CSS_URL.'/style.css">', 0);
?>
<script src="<?php echo G5_JS_URL; ?>/viewimageresize.js"></script>
<script src="<?php echo G5_JS_URL; ?>/jquery.marquee.min.js"></script>
<script src="https://releases.flowplayer.org/7.2.7/flowplayer.min.js"></script>
<script src="https://www.youtube.com/iframe_api"></script>
<link rel="stylesheet" href="https://releases.flowplayer.org/7.2.7/skin/skin.css">
<style>

.youtube {
	  width:900px;
	  height:454px;
	  background-color: #fff;
	  background-size:cover;
	  margin-left:150px;

}
.flowplayer {
	  width:900px;
	  height:auto;
	  background-color: #fff;
	  background-size:cover;
	  margin-left:150px;
	  background-image: url("../data/editor/player_thumb.JPG");

}

.tab_tit {
text-align: center;
}
.tab_tit li {
	width:20%;
}
.tab_tit li button {
    display: block;
    position: relative;
    height: 40px;
    margin-left: -1px;
    background-color: #f7f7f7;
    border: 1px solid #cdcdcd;
    border-bottom: 1px solid #000;
    color: #666;
    text-align: center;
	width:100%;
    z-index: 1;
}
li button .item_qa_count, li button .item_use_count {
    display: inline-block;
    background: #aaa;
    padding: 0 5px;
    line-height: 23px;
    border-radius: 15px;
    color: #fff;
    min-width: 23px;
    font-size: 12px;
}
.sellingpoint {
	padding:10px 150px 10px 150px;

}
.sellingpoint table {
	text-align:left;
	width:1000;
}
.sellingpoint tr td {
	padding : 6px;
	border: 1px solid #444444;
}
.sellingpoint th {
	background-color:#efefef;
}
.sellingpoint td {
	text-align:left;
}

</style>

<!--
<div class="ec-base-tab">
			<ul class="menu">
<li class="selected"><a href="#prdDetail">상품상세정보</a></li>
				<li><a href="#prdInfo">상품구매안내</a></li>
				<li><a href="#prdRelation">관련상품</a></li>
				<li><a href="#prdReview">구매후기 (<span class="count">1</span>)</a></li>
				<li><a href="#prdQnA">상품문의 (<span class="count">0</span>)</a></li>
			</ul>
</div>
-->

<?php
$sql = "select a.it_id,b.it_player,b.it_player_you from {$g5['g5_shop_item_table']} a,{$g5['g5_shop_item_player_table']} b where a.it_id='$it_id' and b.it_id='$it_id' and a.it_only_mobile_display = 0";
$it_p = sql_fetch($sql); ?>
<section id="sit_info">
    <ul class="tab_tit sanchor">
	
        <li><button type="button" rel="#sit_inf" class=" sanchor_on">상품정보</button></li>
        <li><button type="button" rel="#sit_use">사용후기 <span class="item_use_count"><?php echo $item_use_count; ?></span>  </button> </li>
        <li><button type="button" rel="#sit_qa">상품문의 <span class="item_qa_count"><?php echo $item_qa_count; ?></span></button> </li>
        <li><button type="button" rel="#sit_dvr">배송정보</button></li>
		<li><button type="button" rel="#sit_ex">교환정보</button></li>
	
    </ul>
	<ul class="tab_con">
		<li id="sit_inf">
		
			<h2>상품 정보</h2> 
					<?php if ($it_p['it_player_you'] == 0 and $it_p['it_player'] != "") {//유튜브아니면 ?>
					
					
					
						  <p align="center"><div><div class="flowplayer">
							<video autoplay data-title="Autoplay" controls width="900px">
									<source type='video/mp4;codecs="avc1.42E01E, mp4a.40.2"' src="<?php echo $it_p['it_player'] ; ?>">
								</video>
						  </div>
						  </div>
						  <?php if($it_p['it_id']=="307004"){?>
					<p align="center"></p><div><div class="flowplayer">
							<video> 
									<source type='video/mp4;codecs="avc1.42E01E, mp4a.40.2"' src="https://www.orderstation.co.kr/data/editor/playfile/5문5답_보이로 체온계_MH6.mp4">
								</video>
						  </div>
						  </div>
						  <?}?>
						   <?php if($it_p['it_id']=="S02015"){?>
					<p align="center"></p><div><div class="flowplayer">
							<video> 
									<source type='video/mp4;codecs="avc1.42E01E, mp4a.40.2"' src="https://www.orderstation.co.kr/data/editor/playfile/네블라이저_한글사용.mp4">
								</video>
						  </div>
						  </div>
						  <?}?>
						  
		<?php  } if($it_p['it_player_you'] == 1 && $it_p['it_player'] != ""){  ?>
		<center>
			<div id="video-placeholder" style="width:900px;"></div>
		</center>
		<!--<p align="center"><div><div class="youtube">
		<iframe id=it_players type="text/html" width="900px" height="454px" src ="<?php echo $it_p['it_player'].'?autoplay=0&rel=0'; ?>"></iframe>
		</div>
	</div>-->
		<?php } ?>
		
		
			<?php if ($it['it_basic']) { // 상품 기본설명 ?>
			<h3>상품 기본설명</h3>
			
			<div id="sit_inf_basic">
				 <?php echo $it['it_basic']; ?>
			</div>
			<?php } ?>

			<?php if ( $it['it_explan'] || $it['it_opt_box'] || $it['it_box_txt'] ) { // 상품 상세설명 ?>
			<h3>상품 상세설명</h3>
			<? if( !empty($it['it_box_txt']) ){ ?>
			<!--웹 버전 박스 -->
			<div class="choice_wrap" style="border:0px solid #000;margin-left:auto;margin-right:auto;">
				<div class="choice_box1">
					<div class="box1_left"> <span>선택</span><a>1</a></div>
					<div class="box1_right">
						<div class="br_left1">
							<span><?=$it['it_name']?></span> 
							<?=$it["it_box_txt"]?>
						</div>
						<div class="br_right1">
							<img src="<?php echo G5_DATA_URL; ?>/item/<?php echo $it["it_box_img"]; ?>" >
							<? 
							//echo get_it_thumbnail2($row2["io_img"], 0, 170); 
							?>
						</div>
					</div>
				</div>
			</div>
			<!--웹 버전 박스-->
			<? } ?>

			<? if( $it['it_opt_box'] == "1" ){ ?>

			<?
			$sql2 = " select * from {$g5['g5_shop_item_option_table']} where io_type = '0' and it_id = '$it_id' and io_use = '1' order by io_no asc ";
			$result2 = sql_query($sql2);
				
				for($i=0; $row2=sql_fetch_array($result2); $i++) {
			?>
			<!--웹 버전 박스 -->
			<div class="choice_wrap" style="border:0px solid #000;margin-left:auto;margin-right:auto;">
				<div class="choice_box1">
					<div class="box1_left"> <span>선택</span><a><?=$i+1?></a></div>
					<div class="box1_right">
						<div class="br_left1">
							<span><?=str_replace("선택".($i+1).")","",$row2["io_name"])?></span> 
							<?=$row2["io_txt"]?>
						</div>
						<div class="br_right1">
							<img src="<?php echo G5_DATA_URL; ?>/option/<?php echo $row2["io_img"]; ?>" >
							<? 
							//echo get_it_thumbnail2($row2["io_img"], 0, 170); 
							?>
						</div>
					</div>
				</div>
			</div>
			<!--웹 버전 박스-->
			<?
				}	
			}
			?>

			<div id="sit_inf_explan" style="margin-left:auto;margin-right:auto;">

				<!-- 밑에 이미지를 올리기위해 div 추가 -->
				<div class="img_wrpa1">
					<?
					//결합이미지 사용일때.
					if( $it['it_orderbox_yn'] == "Y" ){
						$changecode = "
							<div class=\"up_divwrap\">
								<i class=\"fa  fa-plus-circle\"></i>
								<a class='marquee' > ".$it['it_orderbox_txt']." </a><img src=\"/img/over_img.png\" class='over_img'>
							</div>			
						";
						$it['it_explan'] = str_replace( "{결합이미지}" , $changecode , $it['it_explan'] );					
					}else{
						$it['it_explan'] = str_replace( "{결합이미지}" , "" , $it['it_explan'] );										
					}
					?>
					<?php echo conv_content($it['it_explan'], 1); ?>
				</div>
				<!-- 밑에 이미지를 올리기위해 div 추가 -->

				<br/>

				<div class="sellingpoint">
				<?php echo conv_content($it['it_sellingpoint'], 1); ?>
				</div>

				<?php
				if ($it['it_info_value']) { // 상품 정보 고시
					$info_data = unserialize(stripslashes($it['it_info_value']));
					if(is_array($info_data)) {
						$gubun = $it['it_info_gubun'];
						$info_array = $item_info[$gubun]['article'];
				?>
				<h3>상품 정보 고시</h3>
				<table id="sit_inf_open">
				<colgroup>
					<col class="grid_4">
					<col>
				</colgroup>
				<tbody>
				<?php
				foreach($info_data as $key=>$val) {
					$ii_title = $info_array[$key][0];
					$ii_value = $val;
				?>
				<tr>
					<?php if($ii_value != "@"){?>
					<th scope="row"><?php echo $ii_title; ?></th>
					<td><?php echo $ii_value; ?></td>
					<?php } ?>				</tr>
				<?php } //foreach?>
				</tbody>
				</table>
				<!-- 상품정보고시 end -->
				<?php
					} else {
						if($is_admin) {
							echo '<p>상품 정보 고시 정보가 올바르게 저장되지 않았습니다.<br>config.php 파일의 G5_ESCAPE_FUNCTION 설정을 addslashes 로<br>변경하신 후 관리자 &gt; 상품정보 수정에서 상품 정보를 다시 저장해주세요. </p>';
						}
					}
				} //if
				?>


			</div>
			<?php } ?>



		</li>
		<!-- } 상품 정보 끝 -->

		<!-- 사용후기 시작 { -->
		<li id="sit_use">
			<h2>사용후기</h2>
			<?php //echo pg_anchor('use'); ?>

			<div id="itemuse"><?php include_once(G5_SHOP_PATH.'/itemuse.php'); ?></div>
		</li>
		<!-- } 사용후기 끝 -->

		<!-- 상품문의 시작 { -->
		<li id="sit_qa">
			<h2>상품문의</h2>
			<?php //echo pg_anchor('qa'); ?>

			<div id="itemqa"><?php include_once(G5_SHOP_PATH.'/itemqa.php'); ?></div>
		</li>
		<!-- } 상품문의 끝 -->

		<?php if ($default['de_baesong_content']) { // 배송정보 내용이 있다면 ?>
		<!-- 배송정보 시작 { -->
		<li id="sit_dvr">
			<h2>배송정보</h2>
			<?php //echo pg_anchor('dvr'); ?>

			<?php echo conv_content($default['de_baesong_content'], 1); ?>
		</li>
		<!-- } 배송정보 끝 -->
		<?php } ?>


		<?php if ($default['de_change_content']) { // 교환/반품 내용이 있다면 ?>
		<!-- 교환/반품 시작 { -->
		<li id="sit_ex">
			<h2>교환/반품</h2>
			<?php //echo pg_anchor('ex'); ?>

			<?php echo conv_content($default['de_change_content'], 1); ?>
		</li>
		<!-- } 교환/반품 끝 -->
		<?php } ?>
	</ul>
</section>

<script>
    $(".tab_con>li").hide();
    $(".tab_con>li:first").show();   
    $(".tab_tit li button").click(function(){
        $(".tab_tit li button").removeClass("sanchor_on");
        $(this).addClass("sanchor_on");
        $(".tab_con>li").hide();

        $($(this).attr("rel")).show();

    });
function onYouTubeIframeAPIReady() {

	var player;

    player = new YT.Player('video-placeholder', {
		 height: 450,
        videoId: '<?php echo $it_p['it_player']?>',
        playerVars: {rel: 0} //추천영상 안보여주게 설정
    });
}

onYouTubeIframeAPIReady();
</script>




<script>
$(window).on("load", function() {
   $("#sit_inf_explan").viewimageresize2();
});

$('.marquee').marquee({
	//speed in milliseconds of the marquee
	duration: 8000,
	//gap in pixels between the tickers
	gap: 50,
	//time in milliseconds before the marquee will start animating
	delayBeforeStart: 1000,
	//'left' or 'right'
	direction: 'left',
	//true or false - should the marquee be duplicated to show an effect of continues flow
	duplicated: true , 
	startVisible:true

});
</script>
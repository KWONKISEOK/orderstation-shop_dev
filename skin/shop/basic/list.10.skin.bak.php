<?php
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가

// add_stylesheet('css 구문', 출력순서); 숫자가 작을 수록 먼저 출력됨
add_stylesheet('<link rel="stylesheet" href="'.G5_SHOP_SKIN_URL.'/style.css">', 0);
?>
<style>
.sct_img {   
	margin:-1px;
    border: 1px solid rgba(153,153,153,0.1);
}
.sct_img:hover {   
    border: 1px solid rgba(153,153,153,0.6);
}

.sct_10 .sct_cost .sct_incen {
    display: block;
    color: #999;
    font-size:12px ;
    margin-top: 3px;
}
.sct_10 .sct_cost .sct_price {
    /*color: #000;
    font-size: 1em;
	font-weight: 700;
	letter-spacing: -1px;
    font-family: verdana;*/
	color: #000;
    font-size: 13px;
	font-weight: bold;
}
.sct_10 .sct_cost .sct_desc {
	margin: 3px 10px 10px 10px;
    color: #999;
    font-size: 0.92em;
}
.sct_10 .sct_cost .sct_price_won {
	vertical-align:top;
}
.event_trans {
	font-size:15px;
	width:45px;
	height:45px;
	line-height:20px;
	display:inline;
	z-index:100;
	position:absolute;
	top:9px;
	right:66px;
	background:#ff8100;
	color:#fff;
	text-align:center;
	border-radius:50px;
	font-weight:bold;
	letter-spacing:1px;
	box-sizing:border-box;
	box-shadow:inset 0px 0px 2px 0px rgba(189,189,189,1);
}
</style>

<!-- <div class="xans-element- xans-product xans-product-listnormal ec-base-product">           
<ul class="prdList grid5"> -->
<!-- 상품진열 10 시작 { -->
<?php
// is_drug=true 일 경우
// 포인트 보여짐, 오피스디포 카테고리 보여짐
$is_drug = false;
if($member['mb_type'] === '1' || $member['mb_type'] === '9') {
	$is_drug = true;
}

for ($i=1; $row=sql_fetch_array($result); $i++) {

	if($row['it_cust_price'] > 0) {
		$is_sale=true;
	} else {
		$is_sale=false;
	}

    if ($this->list_mod >= 2) { // 1줄 이미지 : 2개 이상
        if ($i%$this->list_mod == 0) $sct_last = 'sct_last'; // 줄 마지막
        else if ($i%$this->list_mod == 1) $sct_last = 'sct_clear'; // 줄 첫번째
        else $sct_last = '';
    } else { // 1줄 이미지 : 1개
        $sct_last = 'sct_clear';
    }

    if ($i == 1) {
        if ($this->css) {
            echo "<ul class=\"{$this->css}\">\n";
        } else {
            echo "<ul class=\"sct sct_10\">\n";
        }
    }
    if (get_item_auth_member($member['mb_id'], $row['it_id']) == false && get_item_auth_check($row['it_id'], 1) == true) {
        echo "";
    } else {
        echo "<li class=\"sct_li {$sct_last}\" style=\"width:{$this->img_width}px\">\n";

        echo "<div class=\"sct_img\">\n";

        $array_t = array ('S01754', 'S01753', 'S01752', 'S01751', 'S01750');
        if( in_array($row['it_id'] ,$array_t) ){
            echo" <span class=\"event_trans\">추가할인</span> ";
        }
        if( $row['it_id'] == "S01755"){
            echo" <span class=\"event_trans\">타임세일</span> ";
        }
        if ($is_sale) {
            echo "<span class=\"discount_rate\" data-prod-custom=\"{$row['it_cust_price']}\" data-prod-price=\"{$row['it_price']}\"></span>";
            if ($row['it_sc_type'] == "1") {
                echo "<span class=\"free_trans\">무료배송</span> ";
                if ($row['it_multisend'] == "1") {
                    echo "<span class=\"multi_trans_2\">다중배송</span> ";
                }
            } else {
                if ($row['it_multisend'] == "1") {
                    echo "<span class=\"multi_trans_1\">다중배송</span> ";
                }
            }
        } else {
            if ($row['it_sc_type'] == "1") {
                echo "<span class=\"free_trans_new\">무료배송</span> ";
                if ($row['it_multisend'] == "1") {
                    echo "<span class=\"multi_trans_1\">다중배송</span> ";
                }
            } else {
                if ($row['it_multisend'] == "1") {
                    echo "<span class=\"multi_trans_0\">다중배송</span> ";
                }
            }
        }


        if ($this->href) {
            echo "<a href=\"{$this->href}{$row['it_id']}\">\n";
        }

        if ($this->view_it_img) {
            echo get_it_image($row['it_id'], $this->img_width, $this->img_height, '', '', stripslashes($row['it_name']))."\n";
        }

        if ($this->href) {
            echo "</a>\n";
        }

        echo "</div>\n";

        if ($this->view_it_id) {
            echo "<div class=\"sct_id\">&lt;".stripslashes($row['it_id'])."&gt;</div>\n";
        }

        if ($this->href) {
            echo "<div class=\"sct_txt\"><a href=\"{$this->href}{$row['it_id']}\">\n";
        }

        if ($this->view_it_name) {
            echo stripslashes($row['it_name'])."\n";
        }

        if ($this->href) {
            echo "</a></div>\n";
        }

        //if ($this->view_it_basic && $row['it_basic']) {
        //    echo "<div class=\"sct_basic\">".stripslashes($row['it_basic'])."</div>\n";
        //}
        if ($this->view_it_price) {

         ;
            // 공급자가 표시
            if ($is_drug) {
                echo "<li class=\"xans-record-\">";
                echo "<span class='sct_price_title'>약국공급가</span>";
                    echo "<div class=\"pay-area\">";
                       echo "<span class='pay-price'>";
                            echo "<span class='pay-price'>".number_format($row['it_drug_price'])."</span>";
                            echo " <span class=\"unit\">원</span>";
                       echo "</span>";
                       echo "<span class=\"original-price\">";
                             echo "<span class=\"price\"> ".  $row['it_cust_price'] > 0 ? number_format($row['it_cust_price']) : number_format($row['it_price'])."</span>";
                             echo "<span class=\"unit\">원</span>";
                       echo "</span>";
                       // 할인
                       echo "<span class=\"discount-percent\" 
                                    data-prod-price=\"".($row['it_drug_ori_price'] > 0) ?$row['it_drug_ori_price'] : $row['it_drug_price'] ." \"
                                    data-prod-custom=\"".$row['it_cust_price'] > 0 ?$row['it_cust_price'] : $row['it_price'] ."\">";
                       echo "</span>";
                    echo "</div>";
                echo "</li>";
            } else {
                if($is_sale)
                    echo "<span style=\"text-decoration: line-through;font-size:12px;\">".number_format($row['it_cust_price'])."</span> ";
                echo "<span class=\"sct_price\">".display_price(get_price($row), $row['it_tel_inq'])."</span><span class=\"sct_price_won\"> 원</span>\n";
            }

            echo "<span class=\"sct_desc desc_content w250\"> ".$row['it_desc']."</span>";

            echo "</div>\n";
            echo "   </div>";
                echo"</li>";

        }

        //if ($this->view_it_icon) {
        //    echo "<div class=\"sct_icon\">".item_icon($row)."</div>\n";s
        //}
        ?>
        <div class="icon">
            <div class="promotion">
                <?php if( $row['it_type1']==1) { ?>
                <img src="<?php echo G5_THEME_URL;?>/img/icon/icon_best.jpg" class="icon_img" alt="Best"/>
                <?php } ?>
                <?php if( $row['it_type2']==1) { ?>
                <img src="<?php echo G5_THEME_URL;?>/img/icon/icon_recommand.jpg" class="icon_img" alt="추천"/>
                <?php } ?>
                <?php if( $row['it_type3']==1) { ?>
                <img src="<?php echo G5_THEME_URL;?>/img/icon/icon_new.jpg" class="icon_img" alt="New"/>
                <?php } ?>
            </div>
        </div>
        <?php
        echo "</li>\n";
	}
}

//if ($i > 1) echo "</ul>\n";
?>
<!-- 
		</ul>
	</div> -->

<?php
if($i == 1) echo "<p class=\"sct_noitem\">등록된 상품이 없습니다.</p>\n";
?>
<!-- } 상품진열 10 끝 -->
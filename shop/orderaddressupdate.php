<?php
include_once('./_common.php');

if ($is_guest)
    die('회원 로그인 후 이용해 주십시오.');

$mode = $_POST['mode'];
$ad_id = $_POST['ad_id'];
$ad_zip1 = substr($_POST['ad_zip'], 0, 3);
$ad_zip2 = substr($_POST['ad_zip'], 3);
$ad_default = ($_POST['ad_default'] == 'Y') ? 1 : 0;

if ($mode == "list") {
    $count = count($_POST['chk']);

    if (!$count) {
        alert('수정하실 항목을 하나이상 선택하세요.');
    }

    if ($is_member && $count) {
        for ($i = 0; $i < $count; $i++) {
            // 실제 번호를 넘김
            $k = (int)$_POST['chk'][$i];
            $ad_id = (int)$_POST['ad_id'][$k];

            $ad_subject = clean_xss_tags($_POST['ad_subject'][$k]);

            $sql = " update {$g5['g5_shop_order_address_table']}
                    set ad_subject = '$ad_subject' ";

            if (!empty($_POST['ad_default']) && $ad_id == $_POST['ad_default']) {
                sql_query(" update {$g5['g5_shop_order_address_table']} set ad_default = '0' where mb_id = '{$member['mb_id']}' ");

                $sql .= ", ad_default = '1' ";
            }

            $sql .= " where ad_id = '".$ad_id."'
                    and mb_id = '{$member['mb_id']}' ";

            sql_query($sql);
        }
    }
} else if ($mode == "write") {

    if ($ad_id == '' || empty($ad_id)) {
        // 신규등록
        if ($ad_default)
            sql_query(" update {$g5['g5_shop_order_address_table']} set ad_default = '0' where mb_id = '{$member['mb_id']}' ");
        
        $sql = " insert into {$g5['g5_shop_order_address_table']}
				set mb_id = '{$member['mb_id']}' ,
				    ad_subject = '{$_POST['ad_subject']}',
				    ad_default = '{$ad_default}',
					ad_name = '{$_POST['ad_name']}',
					ad_tel = '{$_POST['ad_tel']}',
					ad_hp = '{$_POST['ad_hp']}',
					ad_zip1 = '$ad_zip1',
					ad_zip2 = '$ad_zip2',
					ad_addr1 = '{$_POST['ad_addr1']}',
					ad_addr2 = '{$_POST['ad_addr2']}',
					ad_addr3 = '{$_POST['ad_addr3']}',
					ad_jibeon = '{$_POST['ad_jibeon']}'
                ";
        sql_query($sql);
    } else {
        // 수정

        if ($ad_default)
            sql_query(" update {$g5['g5_shop_order_address_table']} set ad_default = '0' where mb_id = '{$member['mb_id']}' ");

        $sql = " update {$g5['g5_shop_order_address_table']}
				set mb_id = '{$member['mb_id']}' ,
				    ad_subject = '{$_POST['ad_subject']}',
				    ad_default = '{$ad_default}',
					ad_name = '{$_POST['ad_name']}',
					ad_tel = '{$_POST['ad_tel']}',
					ad_hp = '{$_POST['ad_hp']}',
					ad_zip1 = '$ad_zip1',
					ad_zip2 = '$ad_zip2',
					ad_addr1 = '{$_POST['ad_addr1']}',
					ad_addr2 = '{$_POST['ad_addr2']}',
					ad_addr3 = '{$_POST['ad_addr3']}',
					ad_jibeon = '{$_POST['ad_jibeon']}'
                where ad_id='{$ad_id}'";
        sql_query($sql);
    }
}

goto_url(G5_SHOP_URL.'/orderaddress.php?multi_gubun='.$multi_gubun);
?>
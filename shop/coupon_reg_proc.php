<?php
include_once('./_common.php');

if($is_guest)
    die('회원 로그인 후 이용해 주십시오.');


if( $member['mb_type'] != 0 && $member['mb_id'] != "admin" ) {
	echo "<script>
			alert('소비자 회원만 접속 가능합니다.');
		  </script>
		 ";
	exit;
}

/*if( empty($cp_1) || empty($cp_2) || empty($cp_3) || empty($cp_4) ){
	echo "<script>
			alert('쿠폰번호 16자리를 빠짐없이 입력해주세요.');
		  </script>
		 ";
	exit;
}*/

$cp_1 = substr($cp_all, 0, 4);
$cp_2 = substr($cp_all, 4, 4);
$cp_3 = substr($cp_all, 8, 4);
$cp_4 = substr($cp_all, 12, 4);

$cp_id = strtoupper($cp_1)."-".strtoupper($cp_2)."-".strtoupper($cp_3)."-".strtoupper($cp_4);

//등록되어있는 쿠폰인지 검사.
$sql = " 
		 select count(cp_id) as cnt from tbl_shop_coupon_rand_txt AS A INNER JOIN tbl_shop_coupon_rand AS B ON A.parent_cp_no = B.cp_no 
		 where cp_id = '".$cp_id."' and use_yn = 'N' and cp_method != '4'
	   ";
$row = sql_fetch($sql);

if( $row["cnt"] == 0 ){
	echo "<script>
			alert('이미 등록된 쿠폰이거나 사용할 수 없는 쿠폰입니다.');
		  </script>
		 ";
	exit;
}else if( $row["cnt"] > 1 ){
	echo "<script>
			alert('중복된 쿠폰번호가 존재합니다. 관리자에게 문의해주십시요.');
		  </script>
		 ";
	exit;
}else if( $row["cnt"] == 1 ){

		/*************** 트랜잭션 관련 ****************/
		$error_cnt = 0;
		mysqli_autocommit($g5['connect_db'], false);
		/*************** 트랜잭션 관련 ****************/

		$sql = " INSERT INTO tbl_shop_coupon( 
					cp_id, 
					cp_subject, 
					cp_method, 
					cp_target, 
					mb_id, 
					cp_start, 
					cp_end, 
					cp_type, 
					cp_price, 
					cp_trunc, 
					cp_minimum, 
					cp_maximum, 
					cp_datetime, 
					cp_duplicate 
				)
				SELECT 
					A.cp_id,
					B.cp_subject,
					B.cp_method,
					B.cp_target,
					'".$member['mb_id']."' as mb_id,
					B.cp_start,
					B.cp_end,
					B.cp_type,
					B.cp_price,
					B.cp_trunc,
					B.cp_minimum,
					B.cp_maximum,
					B.cp_datetime,
					B.cp_duplicate 
				FROM tbl_shop_coupon_rand_txt AS A INNER JOIN tbl_shop_coupon_rand AS B ON A.parent_cp_no = B.cp_no
				WHERE A.cp_id = '".$cp_id."' AND A.use_yn = 'N' ";
		sql_query($sql);

		/*************** 트랜잭션 관련 ****************/
		if( mysqli_errno($g5['connect_db']) ){
			$error_cnt += 1;
		}
		/*************** 트랜잭션 관련 ****************/


		$sql = " update tbl_shop_coupon_rand_txt set use_yn = 'Y' , mb_id = '".$member['mb_id']."' , use_datetime = now() where cp_id = '".$cp_id."' ";
		sql_query($sql);

		/*************** 트랜잭션 관련 ****************/
		if( mysqli_errno($g5['connect_db']) ){
			$error_cnt += 1;
		}
		/*************** 트랜잭션 관련 ****************/


		/*************** 트랜잭션 관련 ****************/
		if ($error_cnt > 0) {
			mysqli_rollback($g5['connect_db']);
			mysqli_close($g5['connect_db']);
			echo "
			<script>
				alert('데이터베이스의 에러로 인해 롤백되었습니다.');
			</script>
			";
			exit;
		} else {
			mysqli_commit($g5['connect_db']);
		} 	
		/*************** 트랜잭션 관련 ****************/

		echo "<script>
				alert('쿠폰등록이 완료되었습니다.');
				parent.document.frm.reset();
			  </script>
			 ";
		exit;

}else{

	echo "<script>
			alert('알수없는 오류입니다.');
		  </script>
		 ";
	exit;

}
?>
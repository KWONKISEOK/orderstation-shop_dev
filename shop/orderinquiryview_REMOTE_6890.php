<?php
include_once('./_common.php');

$od_id = isset($od_id) ? preg_replace('/[^A-Za-z0-9\-_]/', '', strip_tags($od_id)) : 0;

if( isset($_GET['ini_noti']) && !isset($_GET['uid']) ){
    goto_url(G5_SHOP_URL.'/orderinquiry.php');
}

// 불법접속을 할 수 없도록 세션에 아무값이나 저장하여 hidden 으로 넘겨서 다음 페이지에서 비교함
$token = md5(uniqid(rand(), true));
set_session("ss_token", $token);

//달콩 이벤트세트 구매한 회원 쿠폰 발행해주기;
//EventDalcongCoupon($member['mb_name'],$member['mb_id'],$member['mb_type']);

if (!$is_member) {
    if (get_session('ss_orderview_uid') != $_GET['uid'])
        alert("직접 링크로는 주문서 조회가 불가합니다.\\n\\n주문조회 화면을 통하여 조회하시기 바랍니다.", G5_SHOP_URL);
}

$sql = "select * from {$g5['g5_shop_order_table']} where od_id = '$od_id' {$service_type_and_query} ";
if($is_member && !$is_admin)
    $sql .= " and mb_id = '{$member['mb_id']}' ";
$od = sql_fetch($sql);
if (!$od['od_id'] || (!$is_member && md5($od['od_id'].$od['od_time'].$od['od_ip']) != get_session('ss_orderview_uid'))) {
    alert("조회하실 주문서가 없습니다.", G5_SHOP_URL);
}

// 결제방법
$settle_case = $od['od_settle_case'];

if (G5_IS_MOBILE) {
    include_once(G5_MSHOP_PATH.'/orderinquiryview.php');
    return;
}

// 테마에 orderinquiryview.php 있으면 include
if(defined('G5_THEME_SHOP_PATH')) {
    $theme_inquiryview_file = G5_THEME_SHOP_PATH.'/orderinquiryview.php';
    if(is_file($theme_inquiryview_file)) {
        include_once($theme_inquiryview_file);
        return;
        unset($theme_inquiryview_file);
    }
}

$g5['title'] = '주문상세내역';
include_once('./_head.php');


/*
 * 회원/비회원 노출 구분필요

if(!G5_IS_MOBILE) {
	echo '<div class="site-wrap">
	<div id="aside">';
	include_once(G5_SHOP_PATH.'/asidemy.php');
	echo '</div><div id="container">';
}
*/
if(!G5_IS_MOBILE) {
	echo '<div class="site-wrap">';
	echo '<div id="container">';
}



// LG 현금영수증 JS
if($od['od_pg'] == 'lg') {
    if($default['de_card_test']) {
    echo '<script language="JavaScript" src="http://pgweb.uplus.co.kr:7085/WEB_SERVER/js/receipt_link.js"></script>'.PHP_EOL;
    } else {
        echo '<script language="JavaScript" src="http://pgweb.uplus.co.kr/WEB_SERVER/js/receipt_link.js"></script>'.PHP_EOL;
    }
}
?>

<!-- 주문상세내역 시작 { -->
<div id="sod_fin">
    <div class="breadcrumb">
        <ul>
            <li><a href="#">장바구니</a></li>
            <li><a href="#">주문/결제</a></li>
            <li class="active"><a href="#">주문완료</a></li>
        </ul>
    </div>
    <div class="order-step-3">
        <div class="left-section">
            <div class="order-result-top">
                <p class="order-desc">고객님의 주문이 정상적으로 완료되었습니다.</p>
                <p class="order-number"><span class="label">주문번호</span><span class="desc"><?php echo $od_id; ?></span></p>
            </div>
            <section id="sod_fin_list">
                <h3 class="title-box">
                    <span class="left-side title-main">주문 상품</span>
                </h3>

                <?php
                $st_count1 = $st_count2 = 0;
                $custom_cancel = false;

                $sql = " select it_id, it_name, od_send_cost, 0 it_sc_type
                            from {$g5['g5_shop_order_detail_table']}
                            where od_id = '$od_id' {$service_type_and_query}
                            group by it_id
                            order by od_id "; 
                $result = sql_query($sql);
                ?>
                <div class="table-body-border">
                    <table>
                        <colgroup>
                            <col width="*">
                            <col width="120px">
                            <col width="120px">
                            <col width="120px">
                            <col width="120px">
                            <col width="120px">
                        </colgroup>
                        <thead>
                            <tr>
                                <th scope="col" id="th_itname">상품정보</th>
                                <th scope="col" id="th_itqty">총수량</th>
                                <th scope="col" id="th_itsum">소비자가</th>
                                <th scope="col" id="th_itsd">배송비</th>
                                <th scope="col">소계</th>
                                <th scope="col">상태</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $order_receiver_cnt = 0;
                            $return_possible = ""; //2차옵션 상품일경우 반품을 막기위한 조치

                            for($i=0; $row=sql_fetch_array($result); $i++) {
                                $image = get_it_image($row['it_id'], 70, 70);

                                /*$sql = " select ct_id, it_name, ct_option, ct_qty, ct_price, ct_point, ct_status, io_type, io_price
                                            from {$g5['g5_shop_cart_table']}
                                            where od_id = '$od_id'
                                            and it_id = '{$row['it_id']}'
                                            order by io_type asc, ct_id asc ";*/

                                $sql = " select a.od_id, a.it_name, a.od_option, a.od_qty qty, b.od_qty, a.od_price, od_total_sale_price, od_total_incen, b.od_status, b.od_period_date, a.od_period_cnt, a.od_send_cost,b.od_delivery_company,b.od_invoice, a.od_num , b.od_bill_tno , a.io_id, b.od_pay_yn
                                            from {$g5['g5_shop_order_detail_table']} a, tbl_shop_order_receiver b
                                            where a.od_id = b.od_id
                                            and a.od_num = b.od_num
                                            and a.od_id = '$od_id'
                                            and it_id = '{$row['it_id']}' {$service_type_and_query_a} ";

                                $res = sql_query($sql);

                                $ReceiptHtml = "";
                            
                                for($k=0; $opt=sql_fetch_array($res); $k++) {

                                    //2차옵션 상품이 끼어있으면 반품신청을 못한다.
                                    if( strpos( $opt['io_id'] , chr(30) ) !== false ){
                                        $return_possible = "false";
                                    }

                                    if( !empty($opt['od_bill_tno']) ) {
                                        $receipt_script = 'window.open(\'https://iniweb.inicis.com/DefaultWebApp/mall/cr/cm/mCmReceipt_head.jsp?noTid='.$opt['od_bill_tno'].'&noMethod=1\',\'receipt\',\'width=430,height=700\');';
                                        $link_script = "&nbsp;<a href=\"javascript:;\" onclick=".$receipt_script.">[영수증 출력]</a>";
                                        $ReceiptHtml .= "<tr><td>".get_text($opt['od_option'])."-".get_text($opt['od_period_date'])."&nbsp;".$link_script."</td></tr>";
                                    }

                                    if($k == 0) {
                            ?>
                            <tr>
                                <td>
                                    <div class="item-detail">
                                        <div class="sod_img"><?php echo $image; ?> </div>
                                    </div>
                                </td>
                                <!-- <td headers="th_itname" class="td_bdno">
                                    <? 
                                    if( $opt['od_status'] == "완료" || $opt['od_status'] == "반품반려" ){ 
                                        $order_receiver_cnt++;
                                    ?>
                                    <input type="checkbox" name="return_od_num[]" value="<?=$opt['od_num']?>">
                                    <? } ?>
                                    <a href="./item.php?it_id=<?php echo $row['it_id']; ?>"><?php echo $row['it_name']; ?></a>
                                </td> -->
                                <td headers="th_itopt" class="td_bdno"><?php echo get_text($opt['od_option']); ?> - <?php echo get_text($opt['od_period_date']); ?></td>
                                <td headers="th_itqty" class="td_mngsmall"><?php echo number_format($opt['od_qty']); ?></td>
                                <!--<td headers="th_itprice" class="td_numbig"><?php echo number_format($opt['od_price']); ?></td>-->
                                <td headers="th_itsum" class="td_numbig">
                                <?php
                                if($opt[od_period_cnt] > 0) {
                                    $subprice= $opt['od_total_sale_price'] / $opt[od_period_cnt];
                                    $subincen= $opt['od_total_incen'] / $opt[od_period_cnt];
                                } else {
                                    $subprice=$opt['od_total_sale_price'];
                                    $subincen= $opt['od_total_incen'] ;
                                }
                                    echo number_format($subprice); 
                            
                                ?></td>
                                <!-- <td headers="th_itpt" class="td_numbig"><?php echo number_format($point); ?></td> -->
                                <td headers="th_itsd" class="td_dvr"><?php echo number_format($opt['od_send_cost']); ?></td>
                                <td headers="th_itst" class="td_mngsmall">
                                    <?php
                                    if ($opt['od_status'] == "주문" && $opt['od_pay_yn'] == "Y") {
                                        echo '입금';
                                    } else {
                                        echo $opt['od_status'];
                                    }
                                    ?>
                                </td>
                            </tr>
                            <?php } ?>
                            <?php
                                    $tot_point       += $point;

                                    $st_count1++;
                                    if($opt['od_status'] == '주문' || (($opt['od_status'] == '입금') && ($od['od_settle_case'] == '신용카드')))
                                        $st_count2++;
                                }
                            }

                            // 주문 상품의 상태가 모두 주문이면 고객 취소 가능
                            if($st_count1 > 0 && $st_count1 == $st_count2)
                                $custom_cancel = true;
                            ?>
                        </tbody>
                    </table>
                </div>
            
            </section>
            <section class="">
                <h3 class="title-box">
                    <span class="left-side title-main">배송지/결제 정보</span>
                    <span class="right-side title-sub"><button type="button" class="btn-default btn-white-border">주문 취소</span>
                </h3>
                <div class="payment-result-box">
                    <div class="left-side">
                        <ul class="payment-box">
                            <li class="payment-list">
                                <p class="title">받으시는 분</p>
                                <p class="desc">오에스</p>
                            </li>
                            <li class="payment-list">
                                <p class="title">휴대폰 번호</p>
                                <p class="desc">010-1234-5678</p>
                            </li>
                            <li class="payment-list">
                                <p class="title">배송주소</p>
                                <p class="desc">
                                    <span class="br">[03994]</span>
                                    <span class="br">서울특별시 마포구 동교로 210</span>
                                    <span class="br">태전빌딩 2층</span>
                                </p>
                            </li>
                            <li class="payment-list">
                                <p class="title">배송 메시지</p>
                                <p class="desc">부재 시 경비실에 맡겨주세요.<br/>항상 안전하게 배송해주셔서 감사합니다.</p>
                            </li>
                        </ul>
                        <ul class="payment-box">
                            <li class="payment-list">
                                <p class="title">주문하시는 분</p>
                                <p class="desc">오에스</p>
                            </li>
                            <li class="payment-list">
                                <p class="title">휴대폰 번호</p>
                                <p class="desc">010-1234-5678</p>
                            </li>
                            <li class="payment-list">
                                <p class="title">이메일</p>
                                <p class="desc">orderstation@gmail.com</p>
                            </li>
                        </ul>
                    </div>
                    <div class="right-side">
                        <ul class="payment-box bg-box">
                            <li class="payment-list">
                                <p class="title">결제 수단</p>
                                <p class="desc">
                                    <span class="br">신용카드</span>
                                    <span class="br">현대카드(1234-****-****-****)</span>
                                    <span class="br">일시불</span>
                                </p>
                            </li>
                            <li class="payment-list">
                                <p class="title">결제 일시</p>
                                <p class="desc">2020-11-01 20:03:21</p>
                            </li>
                            <li class="payment-list">
                                <p class="title">총 상품 금액</p>
                                <p class="desc"><span class="amount">7,560</span><span class="unit">원</span></p>
                            </li>
                            <li class="payment-list">
                                <p class="title">배송비</p>
                                <p class="desc">무료</p>
                            </li>
                            <li class="payment-list">
                                <p class="title">최종 결제 금액</p>
                                <p class="desc"><span class="amount total-amount">7,560</span><span class="unit">원</span></p>
                            </li>
                            <li class="payment-list">
                                <p class="title">쿠폰 할인</p>
                                <p class="desc"><span class="amount">0</span><span class="unit">원</span></p>
                            </li>
                            <li class="payment-list">
                                <p class="title">적립 포인트</p>
                                <p class="desc"><span class="amount">500</span><span class="unit">원</span></p>
                            </li>
                            <li class="payment-list">
                                <p class="title">영수증</p>
                                <p class="desc"><button class="btn-default btn-white-border" type="button"><span>영수증 출력</span></button></p>
                            </li>
                        </ul>
                    </div>
                </div>
            </section>
            <div class="btn-set btn-set-md">
                <button class="btn-default btn-white"><span>주문 내역 조회</span></button>
                <button class="btn-default btn-orange"><span>쇼핑 계속하기</span></button>
            </div>
        </div>
    </div>
</div>
<!-- } 주문상세내역 끝 -->

<script>

history.pushState(null, document.title, location.href);
window.addEventListener('popstate', function (event) {
    //history.pushState(null, document.title, '/shop');
    location.href = '<?php echo G5_SHOP_URL;?>'
});

$(function() {
    $("#sod_sts_explan_open").on("click", function() {
        var $explan = $("#sod_sts_explan");
        if($explan.is(":animated"))
            return false;

        if($explan.is(":visible")) {
            $explan.slideUp(200);
            $("#sod_sts_explan_open").text("상태설명보기");
        } else {
            $explan.slideDown(200);
            $("#sod_sts_explan_open").text("상태설명닫기");
        }
    });

    $("#sod_sts_explan_close").on("click", function() {
        var $explan = $("#sod_sts_explan");
        if($explan.is(":animated"))
            return false;

        $explan.slideUp(200);
        $("#sod_sts_explan_open").text("상태설명보기");
    });
    $("#return_explan_open").on("click", function() {
        var $explan = $("#return_explan");
        if($explan.is(":animated"))
            return false;

        if($explan.is(":visible")) {
            $explan.slideUp(200);
            $("#return_explan_open").text("반품반려 이유보기");
        } else {
            $explan.slideDown(200);
            $("#return_explan_open").text("반품반려 이유닫기");
        }
    });

    $("#return_explan_close").on("click", function() {
        var $explan = $("#return_explan");
        if($explan.is(":animated"))
            return false;

        $explan.slideUp(200);
        $("#return_explan_open").text("반품반려 이유보기");
    });

	<? if( $order_receiver_cnt > 0 ){ ?>

	$("#cancel_memo").change(function(){

		var selected = $(this).find('option:selected');
        var extra = selected.data('pay_yn'); 

		if ( extra == "Y" ){
			$("#return_text_info").css("display","");
			$("#return_method_info").css("display","");
		}else{
			$("#return_text_info").css("display","none");
			$("#return_method_info").css("display","none");		
			$("input:checkbox[id='return_check_yn']").prop("checked", false);
			$("input:radio[name='od_return_method']").prop("checked", false);
		}

		if( $(this).val() == "기타" ){
			$("#memo_space").css("display","");
		}else{
			$("#memo_space").css("display","none");		
			$("#client_memo").val("");
		}
	});

	<? } ?>

});

function fcancel_check(f)
{
	<?if( $order_receiver_cnt > 0 ){?>

        var $ct_chk = $("input[name^=return_od_num]");
        var chked_cnt = $ct_chk.filter(":checked").size();
		if( chked_cnt == 0 ){
			alert("반품하실 상품을 체크해주세요.");
			return false;
		}
		//체크한 상품 문자열 연결해서 넘기기
		var chk = $("input[name^=return_od_num]:checked").map(function() {
			return this.value;
		}).get().join("|");

		$("#return_od_num_check").val(chk);

		var selected = $("#cancel_memo").find('option:selected');
        var pay_yn = selected.data('pay_yn'); 

		var Msg_1 = "주문을 정말 반품하시겠습니까?";
		var Msg_2 = "반품사유를 입력해 주십시오.";

		if( f.return_check_yn.checked == false && pay_yn == "Y" ){
			alert("배송료 부담 확인여부를 체크해주세요.");
			return false;
		}
		if( pay_yn == "Y" && !document.getElementsByName("od_return_method")[0].checked && !document.getElementsByName("od_return_method")[1].checked ){
			alert("왕복 배송비 결제 방법을 선택해주세요.");
			return false;
		}
	<?}else{?>
		var Msg_1 = "주문을 정말 취소하시겠습니까?";
		var Msg_2 = "취소사유를 입력해 주십시오.";
	<?}?>
    if(!confirm(Msg_1))
        return false;

    var memo = f.cancel_memo.value;
    if(memo == "") {
        alert(Msg_2);
        return false;
    }

    return true;
}
</script>

<?php
include_once('./_tail.php');
?>
<?php
include_once('./_common.php');

$pattern = '#[/\'\"%=*\#\(\)\|\+\&\!\$~\{\}\[\]`;:\?\^\,]#';

$it_id  = preg_replace($pattern, '', $_POST['it_id']);
//$opt_id = preg_replace($pattern, '', $_POST['opt_id']);
$opt_id = addslashes(sql_real_escape_string(preg_replace(G5_OPTION_ID_FILTER, '', $_POST['opt_id'])));
$idx    = preg_replace('#[^0-9]#', '', $_POST['idx']);
$sel_count = preg_replace('#[^0-9]#', '', $_POST['sel_count']);

// 분류사용, 상품사용하는 상품의 정보를 얻음
//$sql = " select a.*, b.ca_name, b.ca_use from {$g5['g5_shop_item_table']} a, {$g5['g5_shop_category_table']} b where a.it_id = '$it_id' and a.ca_id = b.ca_id ";
if (! G5_IS_MOBILE) {
	$sql = " select a.*, b.ca_name, b.ca_use from {$g5['g5_shop_item_table']} a, {$g5['g5_shop_category_table']} b where a.it_only_mobile_display = 0 and a.it_id = '$it_id' and a.ca_id = b.ca_id ";
} else {
	$sql = " select a.*, b.ca_name, b.ca_use from {$g5['g5_shop_item_table']} a, {$g5['g5_shop_category_table']} b where a.it_id = '$it_id' and a.ca_id = b.ca_id ";
}

$it = sql_fetch($sql);
if (!$it['it_id'])
    alert('자료가 없습니다.');

$subject = $it['it_option_subject'];
$subj = explode(',', $subject);
$subj_count = count($subj);

/*
옵션명 비슷한 부분 오류 수정
수정자 : IT FOR ONE
수정 내용 : and io_id like '$opt_id%' => and io_id like '$opt_id".chr(30)."'
*/
$sql = " select * from {$g5['g5_shop_item_option_table']}
                where io_type = '0'
                  and it_id = '$it_id'
                  and io_use = '1'
                  and io_id like '$opt_id".chr(30)."%'
                order by io_no asc ";
$result = sql_query($sql);

$str = '<option value="">'.$subj[1].'</option>';
$opt = array();

for($i=0; $row=sql_fetch_array($result); $i++) {
    $val = explode(chr(30), $row['io_id']);
	$valname = explode(chr(30), $row['io_name']);
    $key = $idx + 1;

    if(!strlen($val[$key]))
        continue;

    $continue = false;
    foreach($opt as $v) {
        if(strval($v) === strval($val[$key])) {
            $continue = true;
            break;
        }
    }
    if($continue)
        continue;

    $opt[] = strval($val[$key]);

    if($key + 1 < $sel_count) {
        $str .= PHP_EOL.'<option value="'.$val[$key].'">'.$val[$key].'</option>';
    } else {

        if($row['io_price'] >= 0)
			
			if($row['io_period_rate1'] > 0) {
				//상품의 기본가
				$sql = " select it_price
							from {$g5['g5_shop_item_table']}
							where it_id = '$it_id'  ";
				$row2 = sql_fetch($sql);
				$it_price = (int)$row2['it_price'];
			
				$io_period_rate = (int)$row['io_period_rate1'];
				$io_period_cnt1 = (int)$row['io_period_cnt1'];
				

				$price = (($it_price + $row['io_price'] ) * ($io_period_rate*0.01)) * $io_period_cnt1;
				$price = '&nbsp;&nbsp;+ '.number_format($price).'원';
				

			} else {
				$price = '&nbsp;&nbsp;+ '.number_format($row['io_price']).'원';
			}



            
        else
            $price = '&nbsp;&nbsp; '.number_format($row['io_price']).'원';

		if($member['mb_type']==1) {
			$price = $price.','.number_format($row['io_incen']).'P';
		}

        $io_stock_qty = get_option_stock_qty($it_id, $row['io_id'], $row['io_type']);

        if($io_stock_qty < 1)
            $soldout = '&nbsp;&nbsp;[품절]';
        else
            $soldout = '';

        // 옵션 2번쨰
		$str .= PHP_EOL.'<option value="'.$val[$key].','.$row['io_price'].','.$io_stock_qty.','.$row['io_name']."&nbsp;".$val[$key].'">'.$val[$key].$price.$soldout.'</option>';
    }
}

echo $str;
?>
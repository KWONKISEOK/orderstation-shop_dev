<?php
include_once('./_common.php');

if (G5_IS_MOBILE) {
    include_once(G5_MSHOP_PATH.'/myincom.php');
    return;
}

define("_MYINCOM_", true);


if($sdate == '') {
	$sdate = date( "Y-m" ).'-01';
	$edate = date( "Y-m-d" );
}


// 테이블의 전체 레코드수만 얻음
$sql = " select count(*) cnt , sum(od_total_incen) as sum_total_incen
		  from tbl_shop_order a, tbl_shop_order_detail b, tbl_shop_order_receiver c
		where a.od_id = b.od_id 
		 and a.od_id = c.od_id 
		 and b.od_num = c.od_num 
		 and a.od_gubun = '3'
		 and a.pharm_custno = '{$member['pharm_custno']}'
		 and substr(c.od_date1, 1, 10) >= '$sdate'
		 and substr(c.od_date1, 1, 10) <= '$edate'
		 and c.od_seq = 1
		 and c.od_status = '완료'
		 and c.od_pay_yn = 'Y' and b.it_id not in('S00603','S00604','S01756')   ";
$row = sql_fetch($sql);
$total_count = $row['cnt'];
$sum_total_incen = $row['sum_total_incen'];
$over_sum = "N"; //3만원이 넘는지 체크

if (  $sum_total_incen > 30000 ) {
	$incen2 = ( $sum_total_incen * 0.003 ) * 10;
	$incen3 = ( $incen2 * 0.01 ) * 10;

	if ( $incen2 >= 1000 ) {
		$sum_total_incen =  $sum_total_incen  - ( $incen2  +  $incen3  );
	}
	$over_sum = "Y";
} 

$rows = $config['cf_page_rows'];
$total_page  = ceil($total_count / $rows);  // 전체 페이지 계산
if ($page < 1) { $page = 1; } // 페이지가 없으면 첫 페이지 (1 페이지)
$from_record = ($page - 1) * $rows; // 시작 열을 구함


$g5['title'] = '개인소득지급관리';
include_once('./_head.php');

if(!G5_IS_MOBILE) {
	echo '<div class="site-wrap">
	<div id="aside">&nbsp;';
	include_once(G5_SHOP_PATH.'/asidemy.php');
	echo '</div><div id="container">';
}
if ((!$bo_table || $w == 's' ) && !defined('_INDEX_')) { ?><div id="wrapper_title"><?php echo $g5['title'] ?></div><?php } 


?>

<!-- 주문 내역 시작 { -->
<div id="sod_v">
    <p id="sod_v_info">개인소득지급관리 내역을 조회하실 수 있습니다.</p>

 

<link rel="stylesheet" href="//code.jquery.com/ui/1.8.18/themes/base/jquery-ui.css" />
<script src="//code.jquery.com/ui/1.8.18/jquery-ui.min.js"></script>

<script>
	function IncomExcelDown(){
		document.frm1.method = "post";
		document.frm1.action = "/shop/myincom_excel.php";
		document.frm1.submit();
		document.frm1.method = "get";
		document.frm1.action = "/shop/myincom.php";
	}
</script>

<form name="frm1" method="get" action="/shop/myincom.php" style="margin:0">	

	<input type="hidden" name="sum_total_incen" value="<?=number_format( floor($sum_total_incen) )?>"/>

	<div id="search-box">
		<span class="btndate" id="today">오늘</span>&nbsp;
		<span class="btndate" id="bday15">15일</span>&nbsp;
		<span class="btndate" id="bmonth1">1개월</span>&nbsp;
		<span class="btndate" id="bmonth3">3개월</span>&nbsp;&nbsp;
          <input type="text" name="sdate" id="sdate" style="width:90px; text-align:center" class="datepickerbutton" value="<?php echo $sdate;?>" readonly>
          ~
          <input type="text" name="edate" id="edate" style="width:90px; text-align:center" class="datepickerbutton" value="<?php echo $edate;?>" readonly>
          <button class="btndate">조회</button>

		  &nbsp;<input type="button" onclick="javascript:IncomExcelDown();" value="엑셀다운로드">
	</div>

</form>
<script>
    $.datepicker.setDefaults({
        dateFormat: 'yy-mm-dd',
        prevText: '이전 달',
        nextText: '다음 달',
        monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
        monthNamesShort: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
        dayNames: ['일', '월', '화', '수', '목', '금', '토'],
        dayNamesShort: ['일', '월', '화', '수', '목', '금', '토'],
        dayNamesMin: ['일', '월', '화', '수', '목', '금', '토'],
        showMonthAfterYear: true,
        yearSuffix: '년'
    });

    $(function() {
        $(".datepickerbutton").datepicker();
    });

	function setDate(kind) {
		var today = '<?php echo date("Y-m-d"); ?>';
		var bday15 = '<?php echo date("Y-m-d", strtotime('-15 days')); ?>';
		var bmonth1 = '<?php echo date("Y-m-d", strtotime('-1 month')); ?>';
		var bmonth3 = '<?php echo date("Y-m-d", strtotime('-3 month')); ?>';

		if(kind==1) {
			$("#sdate").val(today);
			$("#edate").val(today);
		}
		if(kind==2) {
			$("#sdate").val(bday15);
			$("#edate").val(today);
		}
		if(kind==3) {
			$("#sdate").val(bmonth1);
			$("#edate").val(today);
		}
		if(kind==4) {
			$("#sdate").val(bmonth3);
			$("#edate").val(today);
		}

	}
	$( "#today" ).click(function() {
	  setDate(1);
	});
	$( "#bday15" ).click(function() {
	  setDate(2);
	});
	$( "#bmonth1" ).click(function() {
	  setDate(3);
	});
	$( "#bmonth3" ).click(function() {
	  setDate(4);
	});

</script>

<div style="margin-bottom:10px;"><strong>실 지급액 : <?=number_format( floor($sum_total_incen) )?></strong></div>
<div class="tbl_head03 tbl_wrap">
    <table>
    <thead>
    <tr>
        <th scope="col">주문일자</th>
        <th scope="col">주문번호</th>
		<th scope="col">고객명</th>
        <th scope="col">상품명</th>
        <th scope="col">출하가</th>
        <th scope="col">수량</th>
		<th scope="col">총금액</th>
		<th scope="col">포인트</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $sql = " select c.od_date1, a.od_id, a.od_name, b.it_id, b.it_name, b.od_option, b.od_qty, b.od_price, b.od_total_sale_price, b.od_total_incen, c.od_status
				  from tbl_shop_order a, tbl_shop_order_detail b, tbl_shop_order_receiver c
				where a.od_id = b.od_id 
				 and a.od_id = c.od_id 
				 and b.od_num = c.od_num 
				 and a.od_gubun = '3'
				 and a.pharm_custno = '{$member['pharm_custno']}'
				 and substr(c.od_date1, 1, 10) >= '$sdate'
				 and substr(c.od_date1, 1, 10) <= '$edate'
				 and c.od_seq = 1
				 and c.od_status = '완료'
				 and c.od_pay_yn = 'Y'  and b.it_id not in('S00603','S00604','S01756')
				  order by c.od_date1 desc
				  limit $from_record, $rows
				 
				  ";

    $result = sql_query($sql);
    for ($i=0; $row=sql_fetch_array($result); $i++)
    {
	?>
    
    <tr>
        <td><?php echo substr($row['od_date1'],0,10); ?></td>
		<td><?php echo $row['od_id']; ?></td>
		<td><?php echo $row['od_name']; ?></td>
		<td class="text_left"><?php echo $row['it_name']; ?></td>
		<td><?php echo $row['od_price']; ?></td>
		<td class="td_numbig"><?php echo $row['od_qty']; ?></td>
        <td class="td_numbig"><?php echo display_price($row['od_total_sale_price']); ?></td>
        <td class="td_numbig text_right"><?php echo number_format($row['od_total_incen']); ?>P</td>
    </tr>
	<?php } ?>
    </tbody>
    </table>
</div>
<!-- } 주문 내역 목록 끝 -->

    <?php 
	$qstr = "sdate=".urlencode($sdate)."&amp;edate=$edate";
	echo get_paging($config['cf_write_pages'], $page, $total_page, "{$_SERVER['SCRIPT_NAME']}?$qstr&amp;page="); ?>
</div>
<!-- } 주문 내역 끝 -->

<?php
include_once('./_tail.php');
?>

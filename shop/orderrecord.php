<?php
include_once('./_common.php');

if (G5_IS_MOBILE) {
    include_once(G5_MSHOP_PATH.'/orderinquiry.php');
    return;
}

define("_ORDERINQUIRY_", true);

$od_pwd = get_encrypt_string($od_pwd);

// 회원인 경우
if ($is_member)
{
    $sql_common = " from {$g5['g5_shop_order_table']} where mb_id = '{$member['mb_id']}' ";
}
else if ($od_id && $od_pwd) // 비회원인 경우 주문서번호와 비밀번호가 넘어왔다면
{
    $sql_common = " from {$g5['g5_shop_order_table']} where od_id = '$od_id' and od_pwd = '$od_pwd' ";
}
else // 그렇지 않다면 로그인으로 가기
{
    goto_url(G5_BBS_URL.'/login.php?url='.urlencode(G5_SHOP_URL.'/orderinquiry.php'));
}

// 테이블의 전체 레코드수만 얻음
$sql = " select count(*) as cnt " . $sql_common;
$row = sql_fetch($sql);
$total_count = $row['cnt'];

// 비회원 주문확인시 비회원의 모든 주문이 다 출력되는 오류 수정
// 조건에 맞는 주문서가 없다면
if ($total_count == 0)
{
    if ($is_member) // 회원일 경우는 메인으로 이동
        alert('주문이 존재하지 않습니다.', G5_SHOP_URL);
    else // 비회원일 경우는 이전 페이지로 이동
        alert('주문이 존재하지 않습니다.');
}

$rows = $config['cf_page_rows'];
$total_page  = ceil($total_count / $rows);  // 전체 페이지 계산
if ($page < 1) { $page = 1; } // 페이지가 없으면 첫 페이지 (1 페이지)
$from_record = ($page - 1) * $rows; // 시작 열을 구함




$g5['title'] = '정산조회';
include_once('./_head.php');

if(!G5_IS_MOBILE) {
	echo '<div class="site-wrap">
	<div id="aside">&nbsp;';
	include_once(G5_SHOP_PATH.'/asidemy.php');
	echo '</div><div id="container">';
}
if ((!$bo_table || $w == 's' ) && !defined('_INDEX_')) { ?><div id="wrapper_title"><?php echo $g5['title'] ?></div><?php } 

?>

<!-- 주문 내역 시작 { -->
<div id="sod_v">
    <p id="sod_v_info">정산하신 내역을 조회하실 수 있습니다.</p>

    <?php
    $limit = " limit $from_record, $rows ";
?>


<div class="tbl_head03 tbl_wrap">
    <table>
    <thead>
    <tr>
        <th scope="col">주문일자</th>
        <th scope="col">주문번호</th>
		<th scope="col">고객명</th>
        <th scope="col">상품명</th>
        <th scope="col">출하가</th>
        <th scope="col">수량</th>
		<th scope="col">총금액</th>
		<th scope="col">포인트</th>
    </tr>
    </thead>
    <tbody>
	<?php
    $sql = " select a.od_id, a.od_time, a.od_ip, a.od_misu, a.od_gubun, a.od_cart_count,
	                b.it_id, b.it_name, b.od_option, b.od_price, b.od_qty, b.od_total_price, b.od_total_incen, b.od_status
               from {$g5['g5_shop_order_table']} a, {$g5['g5_shop_order_detail_table']} b
              where a.od_id = b.od_id
			    and a.mb_id = '{$member['mb_id']}'
              order by a.od_id desc
              $limit ";
    $result = sql_query($sql);
    for ($i=0; $row=sql_fetch_array($result); $i++)
    {
        $uid = md5($row['od_id'].$row['od_time'].$row['od_ip']);

		$od_id = $row['od_id'];
	?>
    
    <tr>
        <td><?php echo substr($row['od_time'],0,10); ?></td>
		<td><a href="<?php echo G5_SHOP_URL; ?>/orderinquiryview.php?od_id=<?php echo $row['od_id']; ?>&amp;uid=<?php echo $uid; ?>"><?php echo $row['od_id']; ?></a></td>
		<td>홍길동</td>
		<td class="text_left"><?php echo $row['it_name']; ?></td>
		<td><?php echo $row['od_price']; ?></td>
		<td class="td_numbig"><?php echo $row['od_qty']; ?></td>
        <td class="td_numbig"><?php echo display_price($row['od_total_price']); ?></td>
        <td class="td_numbig text_right"><?php echo number_format($row['od_total_incen']); ?>P</td>
        
    </tr>
	<?php } ?>
  
    </tbody>
    </table>
</div>
<!-- } 주문 내역 목록 끝 -->

    <?php echo get_paging($config['cf_write_pages'], $page, $total_page, "{$_SERVER['SCRIPT_NAME']}?$qstr&amp;page="); ?>
</div>
<!-- } 주문 내역 끝 -->

<?php
include_once('./_tail.php');
?>

<?php
include_once('./_common.php');

if (G5_IS_MOBILE) {
    include_once(G5_MSHOP_PATH.'/myrecord.php');
    return;
}

define("_MYRECORD_", true);

if($sdate == '') {
	$sdate = date( "Y-m" ).'-01';
	$edate = date( "Y-m-d" );
}


// 테이블의 전체 레코드수만 얻음
$sql = " select count(*) cnt
		  from tbl_shop_order a, tbl_shop_order_detail b, tbl_shop_order_receiver c
		where a.od_id = b.od_id 
		 and a.od_id = c.od_id 
		 and b.od_num = c.od_num 
		 and a.mb_id = '{$member['mb_id']}'
		 and substr(c.od_date1, 1, 10) >= '$sdate'
		 and substr(c.od_date1, 1, 10) <= '$edate'
		 and c.od_seq = 1
		 and c.od_status = '완료'";
$row = sql_fetch($sql);
$total_count = $row['cnt'];

$rows = $config['cf_page_rows'];
$total_page  = ceil($total_count / $rows);  // 전체 페이지 계산
if ($page < 1) { $page = 1; } // 페이지가 없으면 첫 페이지 (1 페이지)
$from_record = ($page - 1) * $rows; // 시작 열을 구함




$g5['title'] = '정산조회';
include_once('./_head.php');

if(!G5_IS_MOBILE) {
	echo '<div class="site-wrap">
	<div id="aside">&nbsp;';
	include_once(G5_SHOP_PATH.'/asidemy.php');
	echo '</div><div id="container">';
}
if ((!$bo_table || $w == 's' ) && !defined('_INDEX_')) { ?><div id="wrapper_title"><?php echo $g5['title'] ?></div><?php } 

?>

<!-- 주문 내역 시작 { -->
<div id="sod_v">
    <p id="sod_v_info">정산하신 내역을 조회하실 수 있습니다.</p>

    <?php
    $limit = " limit $from_record, $rows ";
?>

<link rel="stylesheet" href="//code.jquery.com/ui/1.8.18/themes/base/jquery-ui.css" />
<script src="//code.jquery.com/ui/1.8.18/jquery-ui.min.js"></script>


<form name="frm1" method="get" action="/shop/myrecord.php" style="margin:0">	

	<div id="search-box">
		<span class="btndate" id="today">오늘</span>&nbsp;
		<span class="btndate" id="bday15">15일</span>&nbsp;
		<span class="btndate" id="bmonth1">1개월</span>&nbsp;
		<span class="btndate" id="bmonth3">3개월</span>&nbsp;&nbsp;
          <input type="text" name="sdate" id="sdate" style="width:90px; text-align:center" class="datepickerbutton" value="<?php echo $sdate;?>" readonly>
          ~
          <input type="text" name="edate" id="edate" style="width:90px; text-align:center" class="datepickerbutton" value="<?php echo $edate;?>" readonly>
          <button class="btndate">조회</button>
	</div>

</form>
<script>
    $.datepicker.setDefaults({
        dateFormat: 'yy-mm-dd',
        prevText: '이전 달',
        nextText: '다음 달',
        monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
        monthNamesShort: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
        dayNames: ['일', '월', '화', '수', '목', '금', '토'],
        dayNamesShort: ['일', '월', '화', '수', '목', '금', '토'],
        dayNamesMin: ['일', '월', '화', '수', '목', '금', '토'],
        showMonthAfterYear: true,
        yearSuffix: '년'
    });

    $(function() {
        $(".datepickerbutton").datepicker();
    });

	function setDate(kind) {
		var today = '<?php echo date("Y-m-d"); ?>';
		var bday15 = '<?php echo date("Y-m-d", strtotime('-15 days')); ?>';
		var bmonth1 = '<?php echo date("Y-m-d", strtotime('-1 month')); ?>';
		var bmonth3 = '<?php echo date("Y-m-d", strtotime('-3 month')); ?>';

		if(kind==1) {
			$("#sdate").val(today);
			$("#edate").val(today);
		}
		if(kind==2) {
			$("#sdate").val(bday15);
			$("#edate").val(today);
		}
		if(kind==3) {
			$("#sdate").val(bmonth1);
			$("#edate").val(today);
		}
		if(kind==4) {
			$("#sdate").val(bmonth3);
			$("#edate").val(today);
		}

	}
	$( "#today" ).click(function() {
	  setDate(1);
	});
	$( "#bday15" ).click(function() {
	  setDate(2);
	});
	$( "#bmonth1" ).click(function() {
	  setDate(3);
	});
	$( "#bmonth3" ).click(function() {
	  setDate(4);
	});

</script>
<div class="tbl_head03 tbl_wrap">
    <table>
    <thead>
    <tr>
        <th scope="col">주문일자</th>
        <th scope="col">주문번호</th>
		<th scope="col">고객명</th>
        <th scope="col">상품명</th>
        <th scope="col">약국가</th>
        <th scope="col">수량</th>
		<th scope="col">총금액</th>
		<!--<th scope="col">포인트</th>-->
    </tr>
    </thead>
    <tbody>
	<?php
    $sql = " select b.od_total_drug_price,b.od_sum_drug_price, c.od_date1, a.od_id, a.od_name, b.it_id, b.it_name, b.od_option, b.od_qty, b.od_price, b.od_total_sale_price, b.od_total_incen, c.od_status
				  from tbl_shop_order a, tbl_shop_order_detail b, tbl_shop_order_receiver c
				where a.od_id = b.od_id 
				 and a.od_id = c.od_id 
				 and b.od_num = c.od_num 
				 and a.mb_id = '{$member['mb_id']}'
				 and substr(c.od_date1, 1, 10) >= '$sdate'
				 and substr(c.od_date1, 1, 10) <= '$edate'
				 and c.od_seq = 1
				 and c.od_status = '완료'
				  order by c.od_date1 desc
				  limit $from_record, $rows				 
				  ";
    $result = sql_query($sql);
    for ($i=0; $row=sql_fetch_array($result); $i++)
    {

	?>
    
     <tr>
        <td><?php echo substr($row['od_date1'],0,10); ?></td>
		<td><?php echo $row['od_id']; ?></td>
		<td><?php echo $row['od_name']; ?></td>
		<td class="text_left"><?php echo $row['it_name']; ?></td>
		<td><?php echo $row['od_sum_drug_price'] ?></td>
		<td class="td_numbig"><?php echo $row['od_qty']; ?></td>
        <td class="td_numbig"><?php echo display_price($row['od_total_drug_price']); ?></td>
        <!--<td class="td_numbig text_right"><?php echo number_format($row['od_total_incen']); ?>P</td>-->
    </tr>
	<?php } ?>
  
    </tbody>
    </table>
</div>
<!-- } 주문 내역 목록 끝 -->

    <?php
	$qstr = "sdate=".urlencode($sdate)."&amp;edate=$edate";
	echo get_paging($config['cf_write_pages'], $page, $total_page, "{$_SERVER['SCRIPT_NAME']}?$qstr&amp;page="); ?>
</div>
<!-- } 주문 내역 끝 -->

<?php
include_once('./_tail.php');
?>

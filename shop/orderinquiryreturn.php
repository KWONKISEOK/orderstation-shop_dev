<?php
include_once('./_common.php');

// 세션에 저장된 토큰과 폼으로 넘어온 토큰을 비교하여 틀리면 에러
if ($token && get_session("ss_token") == $token) {
    // 맞으면 세션을 지워 다시 입력폼을 통해서 들어오도록 한다.
    set_session("ss_token", "");
} else {
    set_session("ss_token", "");
    alert("토큰 에러", G5_SHOP_URL);
}

/*
if( empty($return_check_yn) || $return_check_yn == "" ){
    alert("배송료 부담 확인여부를 체크해주세요.");
}

if( empty($od_return_method) || $od_return_method == "" ){
    alert("왕복 배송비 결제 방법을 선택해주세요.");
}
*/

if( empty($return_od_num_check) ){
	echo "반품할 상품을 체크해주세요.";
	exit;
}

$return_od_num_check = "'".str_replace("|","','",$return_od_num_check)."'";


$od = sql_fetch(" select * from {$g5['g5_shop_order_table']} where od_id = '$od_id' and mb_id = '{$member['mb_id']}' ");

if (!$od['od_id']) {
    alert("존재하는 주문이 아닙니다.");
}

/*************** 트랜잭션 관련 ****************/
$error_cnt = 0;
mysqli_autocommit($g5['connect_db'], false);
/*************** 트랜잭션 관련 ****************/


if( !empty($client_memo) ){
	$add_sql = ", od_return_client_memo = '".addslashes(strip_tags($client_memo))."' ";
}

// 반품접수
$cancel_memo = addslashes(strip_tags($cancel_memo)) ."  ". $client_memo;
$cancel_price = $od['od_cart_price'];

$sql = " update {$g5['g5_shop_order_table']}
            set od_shop_memo = concat(od_shop_memo,\"\\n주문자 본인 직접 반품 - ".G5_TIME_YMDHIS." (이유 : {$cancel_memo})\")
            where od_id = '$od_id' ";
sql_query($sql);
/*************** 트랜잭션 관련 ****************/
if( mysqli_errno($g5['connect_db']) ){
	$error_cnt += 1;
}
/*************** 트랜잭션 관련 ****************/

//주문상세정보
$sql = " update tbl_shop_order_receiver
            set od_status = '반품접수' , od_return_date_1 = '".G5_TIME_YMDHIS."', od_return_method = '".$od_return_method."'
			". $add_sql ."
            where od_id = '$od_id' and od_num in(".$return_od_num_check.")  ";
sql_query($sql);
/*************** 트랜잭션 관련 ****************/
if( mysqli_errno($g5['connect_db']) ){
	$error_cnt += 1;
}
/*************** 트랜잭션 관련 ****************/


/*************** 트랜잭션 관련 ****************/
if ($error_cnt > 0) {
	mysqli_rollback($g5['connect_db']);
	mysqli_close($g5['connect_db']);
	echo "
	<script>
		alert('데이터베이스의 에러로 인해 롤백되었습니다.');
		history.back();
	</script>
	";
	exit;
} else {
	mysqli_commit($g5['connect_db']);
} 	
/*************** 트랜잭션 관련 ****************/

if ($error_cnt == 0 && $pop_check == 'Y') {
    // 새창으로 해당 페이지 접근하는 경우 처리
    echo "
    <script>
        alert('반품접수가 완료되었습니다.');
        opener.location.reload(true);
        window.close();
    </script>
    ";
} else {
    alert('반품접수가 완료되었습니다.');
    goto_url(G5_SHOP_URL."/orderinquiryview2.php?od_id=$od_id&amp;uid=$uid");
}

?>
<?php
include_once('./_common.php');

if (G5_IS_MOBILE) {
    include_once(G5_MSHOP_PATH.'/item.php');
    return;
}

$it_id = trim($_GET['it_id']);

include_once(G5_LIB_PATH.'/iteminfo.lib.php');

// 분류사용, 상품사용하는 상품의 정보를 얻음
//$sql = " select a.*, b.ca_name, b.ca_use from {$g5['g5_shop_item_table']} a, {$g5['g5_shop_category_table']} b where a.it_id = '$it_id' and a.ca_id = b.ca_id ";
if (! G5_IS_MOBILE) {
	$sql = " select a.*, b.ca_name, b.ca_use, b.ca_staff_use from {$g5['g5_shop_item_table']} a, {$g5['g5_shop_category_table']} b where a.it_only_mobile_display = 0 and a.it_id = '$it_id' and a.ca_id = b.ca_id ";
} else {
	$sql = " select a.*, b.ca_name, b.ca_use, b.ca_staff_use from {$g5['g5_shop_item_table']} a, {$g5['g5_shop_category_table']} b where a.it_id = '$it_id' and a.ca_id = b.ca_id ";
}

$it = sql_fetch($sql);
if (!$it['it_id'])
    alert('자료가 없습니다.');
if (!($it['ca_use'] && $it['it_use'])) {
    if (!$is_admin)
        alert('현재 판매가능한 상품이 아닙니다.');
}

/* url 상에서 상품코드를 직접 치고 들어왔을때 상품노출셋팅에 맞는 회원만 주문가능하게 처리 */
/* 관리자가 아닐때 처리한다. */
if ($member['mb_type'] != "9") {
    if ($member['mb_referee'] != "5") { // 약국,일반회원의 경우
        /* 약국회원인데 상품노출구분에 약국구분이 체크가 되어있지 않다면 경고 띄워주고 주문 못하게 막기*/
        if ($member['mb_type'] == "1" && $it['it_web_view'] == "0") {
            echo "<script>
				alert('해당 상품은 회원별 비노출 상품입니다.');
				history.back();
			  </script>";
            exit;
        }

        /* 일반회원인데 상품노출구분에 일반회원이 체크가 되어있지 않다면 경고 띄워주고 주문 못하게 막기*/
        if ($member['mb_type'] == "0" && $it['it_app_view'] == "0") {
            echo "<script>
				alert('해당 상품은 회원별 비노출 상품입니다.');
				history.back();
			  </script>";
            exit;
        }
        if (get_item_auth_check($it['it_id'], 1) == true && get_item_auth_check($member['mb_id'], 2) == false) {
            echo "<script>
				alert('해당 상품은 회원별 비노출 상품입니다.');
				history.back();
			  </script>";
            exit;
        }
    } else { // 얼라이언스회원의 경우
        if ($member['mb_referee'] == "5" && $member['mb_type'] == "1" && $it['it_new_view'] == "0" && $it['it_web_view'] == "0") {
            echo "<script>
				alert('해당 상품은 회원별 비노출 상품입니다.');
				history.back();
			  </script>";
            exit;
        }
    }
}

// 직원용 프로모션 상품 권한 체크
if ($it['ca_staff_use'] == 1 && !($member['mb_referee'] == 4 || $member['mb_extra'] == 11 || $member['mb_type'] == 9 )) {
    echo "<script>
            alert('해당 상품은 회원별 비노출 상품입니다.');
            history.back();
          </script>";
    exit;
}

// 분류 테이블에서 분류 상단, 하단 코드를 얻음
$sql = " select ca_skin_dir, ca_include_head, ca_include_tail, ca_cert_use, ca_adult_use from {$g5['g5_shop_category_table']} where ca_id = '{$it['ca_id']}' ";
$ca = sql_fetch($sql);

// 본인인증, 성인인증체크
if(!$is_admin) {
    $msg = shop_member_cert_check($it_id, 'item');
    if($msg)
        alert($msg, G5_SHOP_URL);
}

// 오늘 본 상품 저장 시작
// tv 는 today view 약자
$saved = false;
$tv_idx = (int)get_session("ss_tv_idx");
if ($tv_idx > 0) {
    for ($i=1; $i<=$tv_idx; $i++) {
        if (get_session("ss_tv[$i]") == $it_id) {
            $saved = true;
            break;
        }
    }
}

if (!$saved) {
    $tv_idx++;
    set_session("ss_tv_idx", $tv_idx);
    set_session("ss_tv[$tv_idx]", $it_id);
}
// 오늘 본 상품 저장 끝

// 조회수 증가
if (get_cookie('ck_it_id') != $it_id) {
    sql_query(" update {$g5['g5_shop_item_table']} set it_hit = it_hit + 1 where it_id = '$it_id' "); // 1증가
    set_cookie("ck_it_id", $it_id, 3600); // 1시간동안 저장
}

// 스킨경로
$skin_dir = G5_SHOP_SKIN_PATH;
$ca_dir_check = true;

if($it['it_skin']) {
    if(preg_match('#^theme/(.+)$#', $it['it_skin'], $match))
        $skin_dir = G5_THEME_PATH.'/'.G5_SKIN_DIR.'/shop/'.$match[1];
    else
        $skin_dir = G5_PATH.'/'.G5_SKIN_DIR.'/shop/'.$it['it_skin'];

    if(is_dir($skin_dir)) {
        $form_skin_file = $skin_dir.'/item.form.skin.php';

        if(is_file($form_skin_file))
            $ca_dir_check = false;
    }
}

if($ca_dir_check) {
    if($ca['ca_skin_dir']) {
        if(preg_match('#^theme/(.+)$#', $ca['ca_skin_dir'], $match))
            $skin_dir = G5_THEME_PATH.'/'.G5_SKIN_DIR.'/shop/'.$match[1];
        else
            $skin_dir = G5_PATH.'/'.G5_SKIN_DIR.'/shop/'.$ca['ca_skin_dir'];

        if(is_dir($skin_dir)) {
            $form_skin_file = $skin_dir.'/item.form.skin.php';

            if(!is_file($form_skin_file))
                $skin_dir = G5_SHOP_SKIN_PATH;
        } else {
            $skin_dir = G5_SHOP_SKIN_PATH;
        }
    }
}

define('G5_SHOP_CSS_URL', str_replace(G5_PATH, G5_URL, $skin_dir));

$g5['title'] = $it['it_name'];

// 분류 상단 코드가 있으면 출력하고 없으면 기본 상단 코드 출력
if ($ca['ca_include_head'])
    @include_once($ca['ca_include_head']);
else
    include_once(G5_SHOP_PATH.'/_head.php');

if(!G5_IS_MOBILE) {
	echo '<div class="menu-box" style="display:none;">';
	include_once(G5_SHOP_PATH.'/aside.php');
	echo '</div>';
	echo '<div class="site-wrap">';
	echo '<div id="containersub">';
}

?>

<style>

	/*웹_부분*/

	.choice_wrap {
		width: 900px;
		padding: 25px;
		background: #e7e7e9;
		font-family: 'KoPubDotum';
	}

	.choice_wrap .choice_box1 {
		width: 100%;
		background: #FFF;
		height: 300px;
		border: 2px solid #d3d4d6;
		border-radius: 20px;
		overflow: hidden;
	}

	.choice_wrap .box1_left {
		width: 11%;
		height: 140px;
		background: #e42a8d;
		/* fallback for old browsers */
		background: -webkit-linear-gradient(to right, #e42a8d, #bf236a);
		/* Chrome 10-25, Safari 5.1-6 */
		background: linear-gradient(to right, #e42a8d, #bf236a);
		/* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
		float: left;
		border-radius: 0px 0px 20px 0px;
		text-align: center;
		padding-top: 20px;
		color: #FFF;
		font-weight: 600;
	}

	.choice_wrap .box1_right {
		width: 89%;
		padding: 60px 40px 0 40px;
		float: left;
	}

	.choice_wrap .br_left1 {
		float: left;
		width: 60%;
		font-size: 30px;
		word-break:break-word;
	}

	.choice_wrap .br_left1 span {
		display: block;
		font-size: 45px;
		font-weight: 600;
		margin-bottom: 10px;
	}

	.choice_wrap .br_right1 {
		width: 40%;
		float: left;
		padding: 5px 40px;
		margin-top: -50px;
	}

	.choice_wrap .br_right1 img {
		/*width: 100%;*/
		width:250px;
		height:250px;
	}

	.choice_wrap .box1_left span:after {
		content: "";
		display: block;
		width: 50%;
		border-bottom: 1px solid #FFF;

		margin: 5px auto 0 auto;
	}

	.choice_wrap .box1_left span {
		font-size: 20px;
		display: block;
	}

	.choice_wrap .box1_left a {
		display: block;
		font-size: 50px;
		color:#fff;
	}


	.img_wrpa1 {
		margin: 0 auto;
		width: 780px;
		height: auto;
	}

	.img_wrpa1>img {
		width: 100%
	}

	.up_divwrap {
		max-width: 780px;
		height: 85px;
		background: #f78e21;
		padding: 0 15px;
		position: relative;
		bottom: 85px;
		left: 0px;
		margin: 0 auto;
	}

	.up_divwrap img {
		position: absolute;
		bottom: 0px;
		right: 20px;
	}

	.up_divwrap i {
		float: left;
		display: block;
		line-height: 85px;
		font-size: 40px;
		color: #FFF;
	}

	.up_divwrap a {
		float: left;
		display: block;
		color: #FFF;
		width: 460px;
		line-height: 85px;
		font-size: 30px;
		padding: 0 15px;
		font-weight: 600;
		letter-spacing: -1px;
		text-overflow: ellipsis;
		white-space: nowrap;
		overflow: hidden
	}

    /* 관련상품 목록 스킨 10 */
    #sit_rel{border:1px solid #cdcdcd;padding:20px;margin:20px 0 40px;position:relative}
    #sit_rel h2{font-size:1.5em;margin:10px 0 20px;text-align:center}
    #sit_rel .bx-wrapper{margin:0 auto}
    #sit_rel .bx-prev{display:inline-block;position:absolute;top:50%;left:0;width:25px;height:50px;margin-top:-25px;border:1px solid #cdcdcd;border-left:0;text-indent:-999px;overflow:hidden;background:url('./img/btn_prev.png') no-repeat 50% 50%;background-size:50%}
    #sit_rel .bx-next{display:inline-block;position:absolute;top:50%;right:0;width:25px;height:50px;margin-top:-25px;border:1px solid #cdcdcd;border-right:0;text-indent:-999px;overflow:hidden;background:url('./img/btn_next.png') no-repeat 50% 50%;background-size:50%}
    .scr_10 li img{width:100%;height:auto}
    .scr_10 .sct_txt{margin:5px 0}
    .scr_10 .sct_cost{font-weight:bold}
</style>
<!-- 상품 상세보기 시작 { -->
<?php
// 상단 HTML
//echo '<div id="sit_hhtml">'.conv_content($it['it_head_html'], 1).'</div>';

// 보안서버경로
if (G5_HTTPS_DOMAIN)
    $action_url = G5_HTTPS_DOMAIN.'/'.G5_SHOP_DIR.'/cartupdate.php';
else
    $action_url = './cartupdate.php';

// 이전 상품보기
$sql = " select it_id, it_name from {$g5['g5_shop_item_table']} where it_id > '$it_id' and SUBSTRING(ca_id,1,4) = '".substr($it['ca_id'],0,4)."' and it_use = '1' order by it_id asc limit 1 ";
$row = sql_fetch($sql);
if ($row['it_id']) {
    $prev_title = '이전상품<span class="sound_only"> '.$row['it_name'].'</span>';
    $prev_href = '<a href="./item.php?it_id='.$row['it_id'].'" id="siblings_prev">';
    $prev_href2 = '</a>'.PHP_EOL;
} else {
    $prev_title = '';
    $prev_href = '';
    $prev_href2 = '';
}

// 다음 상품보기
$sql = " select it_id, it_name from {$g5['g5_shop_item_table']} where it_id < '$it_id' and SUBSTRING(ca_id,1,4) = '".substr($it['ca_id'],0,4)."' and it_use = '1' order by it_id desc limit 1 ";
$row = sql_fetch($sql);
if ($row['it_id']) {
    $next_title = '다음 상품<span class="sound_only"> '.$row['it_name'].'</span>';
    $next_href = '<a href="./item.php?it_id='.$row['it_id'].'" id="siblings_next">';
    $next_href2 = '</a>'.PHP_EOL;
} else {
    $next_title = '';
    $next_href = '';
    $next_href2 = '';
}

// 고객선호도 별점수
$star_score = get_star_image($it['it_id']);

// 관리자가 확인한 사용후기의 개수를 얻음
$sql = " select count(*) as cnt from `{$g5['g5_shop_item_use_table']}` where it_id = '{$it_id}' /*and is_confirm = '1'*/ ";
if($member['mb_type']=='1' || $is_admin){
	$sql .= " ";
}else{
	$sql .= "and mb_id not in(select mb_id from tbl_member where mb_type ='1') ";
}
$row = sql_fetch($sql);
$item_use_count = $row['cnt'];

// 상품문의의 개수를 얻음
$sql = " select count(*) as cnt from `{$g5['g5_shop_item_qa_table']}` where it_id = '{$it_id}' ";
$row = sql_fetch($sql);
$item_qa_count = $row['cnt'];

// 관련상품의 개수를 얻음
if($default['de_rel_list_use']) {
    $sql = " select count(*) as cnt from {$g5['g5_shop_item_relation_table']} a left join {$g5['g5_shop_item_table']} b on (a.it_id2=b.it_id) where a.it_id = '{$it['it_id']}' and  b.it_use='1' ";
    $row = sql_fetch($sql);
    $item_relation_count = $row['cnt'];
}

// 소셜 관련
$sns_title = get_text($it['it_name']).' | '.get_text($config['cf_title']);
$sns_url  = G5_SHOP_URL.'/item.php?it_id='.$it['it_id'];
//$sns_share_links .= get_sns_share_link('facebook', $sns_url, $sns_title, G5_SHOP_SKIN_URL.'/img/facebook.png').' ';
//$sns_share_links .= get_sns_share_link('twitter', $sns_url, $sns_title, G5_SHOP_SKIN_URL.'/img/twitter.png').' ';
//$sns_share_links .= get_sns_share_link('googleplus', $sns_url, $sns_title, G5_SHOP_SKIN_URL.'/img/gplus.png');

// 상품품절체크
if(G5_SOLDOUT_CHECK)
    $is_soldout = is_soldout($it['it_id']);

// 주문가능체크
$is_orderable = true;
if(!$it['it_use'] || $it['it_tel_inq'] || $is_soldout)
    $is_orderable = false;

if($is_orderable) {
    if(defined('G5_THEME_USE_OPTIONS_TRTD') && G5_THEME_USE_OPTIONS_TRTD){
        $option_item = get_item_options($member['mb_type'],$it['it_id'], $it['it_option_subject'], '');
        $option_item_new = get_item_options_new($member['mb_type'],$it['it_id'], $it['it_option_subject'], '');
        $supply_item = get_item_supply($it['it_id'], $it['it_supply_subject'], '');
    } else {
        // 선택 옵션 ( 기존의 tr td 태그로 가져오려면 'div' 를 '' 로 바꾸거나 또는 지워주세요 )
        $option_item = get_item_options($member['mb_type'],$it['it_id'], $it['it_option_subject'], 'div');
        $option_item_new = get_item_options_new($member['mb_type'],$it['it_id'], $it['it_option_subject'], 'div');
        // 추가 옵션 ( 기존의 tr td 태그로 가져오려면 'div' 를 '' 로 바꾸거나 또는 지워주세요 )
        $supply_item = get_item_supply($it['it_id'], $it['it_supply_subject'], 'div');
    }

    // 상품 선택옵션 수
    $option_count = 0;
    if($it['it_option_subject']) {
        $temp = explode(',', $it['it_option_subject']);
        $option_count = count($temp);
    }

    // 상품 추가옵션 수
    $supply_count = 0;
    if($it['it_supply_subject']) {
        $temp = explode(',', $it['it_supply_subject']);
        $supply_count = count($temp);
    }
}

function pg_anchor($anc_id) {
    global $default;
    global $item_use_count, $item_qa_count, $item_relation_count;
?>
    <ul class="sanchor">
        <li><a href="#sit_inf" <?php if ($anc_id == 'inf') echo 'class="sanchor_on"'; ?>>상품정보</a></li>
        <li><a href="#sit_use" <?php if ($anc_id == 'use') echo 'class="sanchor_on"'; ?>>사용후기 <span class="item_use_count"><?php echo $item_use_count; ?></span></a></li>
        <li><a href="#sit_qa" <?php if ($anc_id == 'qa') echo 'class="sanchor_on"'; ?>>상품문의 <span class="item_qa_count"><?php echo $item_qa_count; ?></span></a></li>
        <?php if ($default['de_baesong_content']) { ?><li><a href="#sit_dvr" <?php if ($anc_id == 'dvr') echo 'class="sanchor_on"'; ?>>배송정보</a></li><?php } ?>
        <?php if ($default['de_change_content']) { ?><li><a href="#sit_ex" <?php if ($anc_id == 'ex') echo 'class="sanchor_on"'; ?>>교환정보</a></li><?php } ?>
    </ul>
<?php
}

if($member['mb_id']=='chaeyoung' or $member['mb_id']=='admin') {
	include_once(G5_SHOP_PATH.'/settle_naverpay.inc.php');
}
?>


	<?php

if ($is_admin) {
    echo '<div class="sit_admin"><a href="'.G5_ADMIN_URL.'/shop_admin/itemform.php?w=u&amp;it_id='.$it_id.'" class="btn_admin btn">상품 관리</a></div>';
}


	?>
	<br/><br/>
	<div class="xans-element- xans-product xans-product-headcategory path ">
		<span>현재 위치</span>
		<?php

// 분류 위치
// HOME > 1단계 > 2단계 ... > 6단계 분류
$ca_id = $it['ca_id'];
if ($ca_id)
{
    $navigation = $bar = "";
    $len = strlen($ca_id) / 2;
    for ($i=1; $i<=$len; $i++)
    {
        $code = substr($ca_id,0,$i*2);

        $sql = " select ca_name from {$g5['g5_shop_category_table']} where ca_id = '$code' ";
        $row = sql_fetch($sql);

        $sct_here = '';
        if ($ca_id == $code) // 현재 분류와 일치하면
            $sct_here = 'sct_here';

        if ($i != $len) // 현재 위치의 마지막 단계가 아니라면
            $sct_bg = 'sct_bg';
        else $sct_bg = '';

        $navigation .= '<li><a href="./list.php?ca_id='.$code.'">'.$row['ca_name'].'</a></li>';
    }
}
else
    $navigation = $g5['title'];




		?>
		<ol>
			<li><a href='<?php echo G5_SHOP_URL; ?>/' >HOME</a></li>
			<?php echo $navigation; ?>
		</ol> 
	</div>
	

	




















<?php if($is_orderable) { ?>

<script src="<?php echo G5_JS_URL; ?>/shop.js?ver=<?php echo G5_JS_VER; ?>"></script>

<?php } ?>


<style>
#sit_rel .bx-wrapper {
	position:static;
}
</style>
<style>
.sct_img {   
	
    border: 1px solid rgba(153,153,153,0.1);
}
.sct_img:hover {   
    border: 1px solid rgba(153,153,153,0.6);
}
/*
.sct_10 .sct_cost .sct_incen {
    display: block;
    color: #999;
    font-size: 0.92em;
}
.scr_10 .sct_cost  {
   
    font-size: 1.2em;
	font-weight: 600;
	letter-spacing: -1px;
    font-family: verdana;
}

*/

</style>
<div id="sit">

    <?php

	//if( $_SERVER['REMOTE_ADDR'] == "121.162.38.65" || $_SERVER['REMOTE_ADDR'] == "59.6.135.109" ){
	if( $it_id == "S00140" || $it_id == "S00141" || $it_id == "S00142" || $it_id == "S00143" || $it_id == "S00144" || $it_id == "S00145" || $it_id == "188019" || $it_id == "S00687" || $it_id == "S00688" ){
		// 상품 구입폼
		include_once($skin_dir.'/item_test.form.skin.php');
	}else{
		// 상품 구입폼
		include_once($skin_dir.'/item.form.skin.php');
	}

	/* SNS URL 공유시 대표이미지 및 설명 보이게 meta 에 추가 */
	if( !empty($img) ){
		preg_match_all("/<img[^>]*src=[\'\"]?([^>\'\"]+)[\'\"]?[^>]*>/", $img, $matchs);
		$match_img = $matchs[1][0];
		$match_title = "[오더스테이션] 이 상품 어때요?";
		$match_description  = stripslashes($it['it_name']);
		$match_url = G5_URL."/shop/item.php?it_id=".$it_id;

		add_meta("<meta property=\"og:title\" content=\"$match_title\"/>",0);
		add_meta("<meta property=\"og:description\" content=\"$match_description\"/>",0);
		add_meta("<meta property=\"og:url\" content=\"$match_url\"/>",0);
		add_meta("<meta property=\"og:image\" content=\"$match_img\"/>",0);
	}

    ?>

	<?php 

// 함께본상품확인
$sql = " select count(*) cnt from tbl_shop_item_same a left join {$g5['g5_shop_item_table']} b on (a.it_id2=b.it_id) where a.it_id = '{$it['it_id']}' and b.it_use='1' ";
$row = sql_fetch($sql);
$item_same_count = $row['cnt'];

// 관련상품
$sql = " select count(*) cnt from {$g5['g5_shop_item_relation_table']} a left join {$g5['g5_shop_item_table']} b on (a.it_id2=b.it_id) where a.it_id = '{$it['it_id']}' and b.it_use='1' ";
$row = sql_fetch($sql);
$item_relation_count = $row['cnt'];





if ($item_same_count > 0) { ?>
<!-- 관련상품 시작 { -->
<h1 class="sit_rel_title" style="display: none;">다른 고객님이 함께 구매한 상품</h1>
<section id="sit_rel" style="display: none;">
    
    <?php
    $rel_skin_file = $skin_dir.'/'.$default['de_rel_list_skin'];
    if(!is_file($rel_skin_file))
        $rel_skin_file = G5_SHOP_SKIN_PATH.'/'.$default['de_rel_list_skin'];

    $sql = " select b.* from tbl_shop_item_same a left join {$g5['g5_shop_item_table']} b on (a.it_id2=b.it_id) where a.it_id = '{$it['it_id']}' and b.it_use='1' ";

    $list = new item_list($rel_skin_file, $default['de_rel_list_mod'], 0, 160, 160);
    $list->set_query($sql);
    echo $list->run();
    ?>
</section>
<!-- } 관련상품 끝 -->
<?php } ?>
<?php if ($item_relation_count > 0) { ?>
<!-- 관련상품 시작 { -->
    <section id="sit_rel">
        <h2>관련상품</h2>
            <?php
            $rel_skin_file = $skin_dir.'/'.$default['de_rel_list_skin'];
            if(!is_file($rel_skin_file))
                $rel_skin_file = G5_SHOP_SKIN_PATH.'/'.$default['de_rel_list_skin'];

            $sql = " select b.* from {$g5['g5_shop_item_relation_table']} a left join {$g5['g5_shop_item_table']} b on (a.it_id2=b.it_id) where a.it_id = '{$it['it_id']}' and b.it_use='1' ";
            $list = new item_list($rel_skin_file, $default['de_rel_list_mod'], 0,160,160);
            $list->set_query($sql);
            echo $list->run();
            ?>
        <div class="bx-controls bx-has-controls-direction">
            <div class="bx-controls-direction">
                <a class="bx-prev" href="">Prev</a>
                <a class="bx-next" href="">Next</a>
            </div>
        </div>
</section>
<!-- } 관련상품 끝 -->
<?php } ?>




    <?php
    // 상품 상세정보
    $info_skin = $skin_dir.'/item.info.skin.php';
    if(!is_file($info_skin))
        $info_skin = G5_SHOP_SKIN_PATH.'/item.info.skin.php';
    include $info_skin;
    ?>

</div>

<?php
// 하단 HTML
//echo conv_content($it['it_tail_html'], 1);
?>







</div>


<div class="clear"></div>





<script>

	$(document).ready(function(){

		$('select').find('option:first').attr('selected', 'selected');

	});

</script>







<?php
if ($ca['ca_include_tail'])
    @include_once($ca['ca_include_tail']);
else
    include_once(G5_SHOP_PATH.'/_tail.php');
?>

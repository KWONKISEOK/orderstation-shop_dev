<?php
include_once('./_common.php');

// print_r2($_POST); exit;

// 보관기간이 지난 상품 삭제
//cart_item_clean();


// cart id 설정
set_cart_id($sw_direct);

if($ct_period_yn != 'Y') {
	$ct_period_cnt = 0;
}

if($sw_direct)
    $tmp_cart_id = get_session('ss_cart_direct');
else
    $tmp_cart_id = get_session('ss_cart_id');

// 브라우저에서 쿠키를 허용하지 않은 경우라고 볼 수 있음.
if (!$tmp_cart_id)
{
    alert('더 이상 작업을 진행할 수 없습니다.\\n\\n브라우저의 쿠키 허용을 사용하지 않음으로 설정한것 같습니다.\\n\\n브라우저의 인터넷 옵션에서 쿠키 허용을 사용으로 설정해 주십시오.\\n\\n그래도 진행이 되지 않는다면 쇼핑몰 운영자에게 문의 바랍니다.');
}


// 레벨(권한)이 상품구입 권한보다 작다면 상품을 구입할 수 없음.
if ($member['mb_level'] < $default['de_level_sell'])
{
    alert('상품을 구입할 수 있는 권한이 없습니다.');
}

/*************** 트랜잭션 관련 ****************/
$error_cnt = 0;
mysqli_autocommit($g5['connect_db'], false);
/*************** 트랜잭션 관련 ****************/

if($act == "buy")
{
    if(!count($_POST['ct_chk']))
        alert("주문하실 상품을 하나이상 선택해 주십시오.");

    // 선택필드 초기화
    $sql = " update {$g5['g5_shop_cart_table']} set ct_select = '0' where od_id = '$tmp_cart_id' ";
    sql_query($sql);
	/*************** 트랜잭션 관련 ****************/
	if( mysqli_errno($g5['connect_db']) ){
		$error_cnt += 1;
	}
	/*************** 트랜잭션 관련 ****************/

    $fldcnt = count($_POST['it_id']);
    for($i=0; $i<$fldcnt; $i++) {
        $ct_chk = $_POST['ct_chk'][$i];
        if($ct_chk) {
            $it_id = $_POST['it_id'][$i];

            // 주문 상품의 재고체크
            $sql = " select ct_qty, it_name, ct_option, io_id, io_type
                        from {$g5['g5_shop_cart_table']}
                        where od_id = '$tmp_cart_id'
                          and it_id = '$it_id' ";
            $result = sql_query($sql);

            for($k=0; $row=sql_fetch_array($result); $k++) {
                $sql = " select SUM(ct_qty) as cnt from {$g5['g5_shop_cart_table']}
                          where od_id <> '$tmp_cart_id'
                            and it_id = '$it_id'
                            and io_id = '{$row['io_id']}'
                            and io_type = '{$row['io_type']}'
                            and ct_stock_use = 0
                            and ct_status = '쇼핑'
                            and ct_select = '1' ";
                $sum = sql_fetch($sql);
                $sum_qty = $sum['cnt'];

                // 재고 구함
                $ct_qty = $row['ct_qty'];
                if(!$row['io_id'])
                    $it_stock_qty = get_it_stock_qty($it_id);
                else
                    $it_stock_qty = get_option_stock_qty($it_id, $row['io_id'], $row['io_type']);

                if ($ct_qty > $it_stock_qty)
                {
                    $item_option = $row['it_name'];
                    if($row['io_id'])
                        $item_option .= '('.$row['ct_option'].')';

                    alert($item_option." 의 재고수량이 부족합니다.\\n\\n현재 재고수량 : " . number_format($it_stock_qty) . " 개");
                }
            }

            $sql = " update {$g5['g5_shop_cart_table']}
                        set ct_select = '1',
                            ct_select_time = '".G5_TIME_YMDHIS."'
                        where od_id = '$tmp_cart_id'
                          and it_id = '$it_id' ";
            sql_query($sql);
			/*************** 트랜잭션 관련 ****************/
			if( mysqli_errno($g5['connect_db']) ){
				$error_cnt += 1;
			}
			/*************** 트랜잭션 관련 ****************/
        }
    }

	/*************** 트랜잭션 관련 ****************/
	if ($error_cnt > 0) {
		mysqli_rollback($g5['connect_db']);
		mysqli_close($g5['connect_db']);
		echo "
		<script>
			alert('데이터베이스의 에러로 인해 롤백되었습니다.');
			history.back();
		</script>
		";
		exit;
	} else {
		mysqli_commit($g5['connect_db']);
	} 	
	/*************** 트랜잭션 관련 ****************/

    if ($is_member) // 회원인 경우
        goto_url(G5_SHOP_URL.'/orderform.php?ct_period_yn='.$ct_period_yn);
		//goto_url(G5_SHOP_URL.'/orderform.php');
    else
        goto_url(G5_BBS_URL.'/login.php?url='.urlencode(G5_SHOP_URL.'/orderform.php'));
}
else if ($act == "auto_mod") // 정기주문수정
{
    $ct_period_sdate = $_POST['ct_period_sdate'];
    $sql = " update  {$g5['g5_shop_cart_table']} set 
					ct_period_sdate = '$ct_period_sdate',
					ct_period_week = '$ct_period_week',
					ct_period_cnt = '$ct_period_cnt',
					ct_period_memo = '$ct_period_memo' 
              where ct_id = '$ct_id' ";
    sql_query($sql);
	/*************** 트랜잭션 관련 ****************/
	if( mysqli_errno($g5['connect_db']) ){
		$error_cnt += 1;
	}
	/*************** 트랜잭션 관련 ****************/

}


else if ($act == "alldelete") // 모두 삭제이면
{
    $sql = " delete from {$g5['g5_shop_cart_table']}
              where od_id = '$tmp_cart_id' ";
    sql_query($sql);
	/*************** 트랜잭션 관련 ****************/
	if( mysqli_errno($g5['connect_db']) ){
		$error_cnt += 1;
	}
	/*************** 트랜잭션 관련 ****************/
}
else if ($act == "seldelete") // 선택삭제
{
    if(!count($_POST['ct_chk'])){
        alert("삭제하실 상품을 하나이상 선택해 주십시오.");
	}

    $fldcnt = count($_POST['it_id']);
    for($i=0; $i<$fldcnt; $i++) {
        $ct_chk = $_POST['ct_chk'][$i];
        if($ct_chk) {
            $it_id = $_POST['it_id'][$i];

			$sql = " delete from {$g5['g5_shop_cart_table']} where it_id = '$it_id' and od_id = '$tmp_cart_id' ";
			sql_query($sql);
			/*************** 트랜잭션 관련 ****************/
			if( mysqli_errno($g5['connect_db']) ){
				$error_cnt += 1;
			}
			/*************** 트랜잭션 관련 ****************/
        }
    }
}
else if ($act == "compdelete") // 공급사제품 선택삭제
{
    if(!count($_POST['ct_chk'])){
        alert("삭제하실 상품을 하나이상 선택해 주십시오.");
	}


	if( empty($_POST['comp_code_del']) ){
        alert("공급사 코드값이 없습니다.");
	}

    $fldcnt = count($_POST['it_id']);
    for($i=0; $i<$fldcnt; $i++) {
        $ct_chk = $_POST['ct_chk'][$i];
        if($ct_chk) {
            $it_id = $_POST['it_id'][$i];
			$comp_code = $_POST['comp_code'][$i];

			//선택 삭제 버튼 눌렀을경우 공급사 값과 리스트에 체크한 공급사 값이 일치할경우에만 삭제한다.
			if( $_POST['comp_code_del'] == $comp_code ){
				$sql = " delete from {$g5['g5_shop_cart_table']} where it_id = '$it_id' and od_id = '$tmp_cart_id' ";
				sql_query($sql);
				/*************** 트랜잭션 관련 ****************/
				if( mysqli_errno($g5['connect_db']) ){
					$error_cnt += 1;
				}
				/*************** 트랜잭션 관련 ****************/
			}
        }
    }
}
else if ($act == "pumdelete") // 품절상품삭제
{

	if( empty($_POST['comp_code_del']) ){
        alert("공급사 코드값이 없습니다.");
	}

	$comp_code = $_POST['comp_code_del'];

	$sql = " 
		DELETE FROM {$g5['g5_shop_cart_table']} WHERE ct_id IN(

			SELECT * FROM(

				SELECT a.ct_id
				from tbl_shop_cart a left join tbl_shop_item b on a.it_id = b.it_id 
											left join tbl_member c on b.comp_code = c.comp_code and c.mb_type = '7'
				where a.od_id = '$tmp_cart_id' AND a.service_type is null and b.it_soldout = '1'
				and b.comp_code = '$comp_code' and c.comp_code = '$comp_code'

			) as X
				
		) AND od_id = '$tmp_cart_id' AND service_type is null
	";
	sql_query($sql);
	/*************** 트랜잭션 관련 ****************/
	if( mysqli_errno($g5['connect_db']) ){
		$error_cnt += 1;
	}
	/*************** 트랜잭션 관련 ****************/

}
else if ($act == "xdel") // 모바일 X버튼 눌러서 삭제
{
    if(!count($_POST['del_it_id'])){
        alert("삭제하실 상품을 하나이상 선택해 주십시오.");
	}


	if( empty($_POST['del_it_id']) ){
        alert("삭제할 상품코드값이 없습니다.");
	}

	$it_id = $_POST['del_it_id'];

	$sql = " delete from {$g5['g5_shop_cart_table']} where it_id = '$it_id' and od_id = '$tmp_cart_id'  ";
	sql_query($sql);
	/*************** 트랜잭션 관련 ****************/
	if( mysqli_errno($g5['connect_db']) ){
		$error_cnt += 1;
	}
	/*************** 트랜잭션 관련 ****************/
}
else // 장바구니에 담기
{

    $count = count($_POST['it_id']);
    if ($count < 1)
        alert('장바구니에 담을 상품을 선택하여 주십시오.');

    $ct_count = 0;
    for($i=0; $i<$count; $i++) {
        // 보관함의 상품을 담을 때 체크되지 않은 상품 건너뜀
        if($act == 'multi' && !$_POST['chk_it_id'][$i])
            continue;

        $it_id = $_POST['it_id'][$i];
        $opt_count = count($_POST['io_id'][$it_id]);

        if($opt_count && $_POST['io_type'][$it_id][0] != 0)
            alert('상품의 선택옵션을 선택해 주십시오.');

        for($k=0; $k<$opt_count; $k++) {
            if ($_POST['ct_qty'][$it_id][$k] < 1)
                alert('수량은 1 이상 입력해 주십시오.');
        }

        // 상품정보
        $sql = " select * from {$g5['g5_shop_item_table']} where it_id = '$it_id' ";
        $it = sql_fetch($sql);
        if(!$it['it_id'])
            alert('상품정보가 존재하지 않습니다.');

        // 바로구매에 있던 장바구니 자료를 지운다.
        if($i == 0 && $sw_direct)
            sql_query(" delete from {$g5['g5_shop_cart_table']} where od_id = '$tmp_cart_id' and ct_direct = 1 ", false);
			/*************** 트랜잭션 관련 ****************/
			if( mysqli_errno($g5['connect_db']) ){
				$error_cnt += 1;
			}
			/*************** 트랜잭션 관련 ****************/

        // 최소, 최대 수량 체크
        if($it['it_buy_min_qty'] || $it['it_buy_max_qty']) {
            $sum_qty = 0;
            for($k=0; $k<$opt_count; $k++) {
                if($_POST['io_type'][$it_id][$k] == 0)
                    $sum_qty += $_POST['ct_qty'][$it_id][$k];
            }

            if($it['it_buy_min_qty'] > 0 && $sum_qty < $it['it_buy_min_qty'])
                alert($it['it_name'].'의 선택옵션 개수 총합 '.number_format($it['it_buy_min_qty']).'개 이상 주문해 주십시오.');

            if($it['it_buy_max_qty'] > 0 && $sum_qty > $it['it_buy_max_qty'])
                alert($it['it_name'].'의 선택옵션 개수 총합 '.number_format($it['it_buy_max_qty']).'개 이하로 주문해 주십시오.');

            // 기존에 장바구니에 담긴 상품이 있는 경우에 최대 구매수량 체크
            if($it['it_buy_max_qty'] > 0) {
                $sql4 = " select sum(ct_qty) as ct_sum
                            from {$g5['g5_shop_cart_table']}
                            where od_id = '$tmp_cart_id'
                              and it_id = '$it_id'
                              and io_type = '0'
                              and ct_status = '쇼핑' ";
                $row4 = sql_fetch($sql4);

                if(($sum_qty + $row4['ct_sum']) > $it['it_buy_max_qty'])
                    alert($it['it_name'].'의 선택옵션 개수 총합 '.number_format($it['it_buy_max_qty']).'개 이하로 주문해 주십시오.', './cart.php');
            }
        }

        // 옵션정보를 얻어서 배열에 저장
        $opt_list = array();
        $sql = " select * from {$g5['g5_shop_item_option_table']} where it_id = '$it_id' and io_use = 1 order by io_no asc ";
        $result = sql_query($sql);
        $lst_count = 0;
        for($k=0; $row=sql_fetch_array($result); $k++) {
            $opt_list[$row['io_type']][$row['io_id']]['id'] = $row['io_id'];
            $opt_list[$row['io_type']][$row['io_id']]['use'] = $row['io_use'];
            $opt_list[$row['io_type']][$row['io_id']]['price'] = $row['io_price'];
            $opt_list[$row['io_type']][$row['io_id']]['stock'] = $row['io_stock_qty'];

            // 선택옵션 개수
            if(!$row['io_type'])
                $lst_count++;
        }

        //--------------------------------------------------------
        //  재고 검사, 바로구매일 때만 체크
        //--------------------------------------------------------
        // 이미 주문폼에 있는 같은 상품의 수량합계를 구한다.
        if($sw_direct) {
            for($k=0; $k<$opt_count; $k++) {
                $io_id = preg_replace(G5_OPTION_ID_FILTER, '', $_POST['io_id'][$it_id][$k]);
                $io_type = preg_replace('#[^01]#', '', $_POST['io_type'][$it_id][$k]);
                $io_value = $_POST['io_value'][$it_id][$k];

                $sql = " select SUM(ct_qty) as cnt from {$g5['g5_shop_cart_table']}
                          where od_id <> '$tmp_cart_id'
                            and it_id = '$it_id'
                            and io_id = '$io_id'
                            and io_type = '$io_type'
                            and ct_stock_use = 0
                            and ct_status = '쇼핑'
                            and ct_select = '1' ";
                $row = sql_fetch($sql);
                $sum_qty = $row['cnt'];

                // 재고 구함
                $ct_qty = $_POST['ct_qty'][$it_id][$k];
                if(!$io_id)
                    $it_stock_qty = get_it_stock_qty($it_id);
                else
                    $it_stock_qty = get_option_stock_qty($it_id, $io_id, $io_type);

                if ($ct_qty > $it_stock_qty)
                {
                    alert($io_value." 의 재고수량이 부족합니다.\\n\\n현재 재고수량 : " . number_format($it_stock_qty) . " 개");
                }
            }
        }
        //--------------------------------------------------------

		// 188019 이즈브레 500ml 10+10 4박스*5회 , 일괄배송 일때 수량은 무조건 1이 되야하므로 장바구니에 누적시키지 않기 위해서...
		if( $it_id == "188019" ){
			$act = "optionmod";
		}


        // 옵션수정일 때 기존 장바구니 자료를 먼저 삭제
        if($act == 'optionmod'){

			// 정기주문상품일경우 선택사항 수정시 아래 delete 문에 의해 배송횟수정보가 날라가는걸 방지하기위한 수정 20190722 수정요청.
			// 회차정보는 그대로이기 때문에 변수에 담아둔다.
			$sql22 = " select ct_period_yn , ct_period_cnt , ct_period_sdate , ct_period_week , ct_period_memo
					   from {$g5['g5_shop_cart_table']} where od_id = '$tmp_cart_id' and it_id = '$it_id' limit 1  ";
			
			$origin_info = sql_fetch($sql22);
			if( $origin_info["ct_period_cnt"] > 0 ){


				$ct_period_yn = "Y";
				$ct_period_sdate = $origin_info["ct_period_sdate"];
				$ct_period_week = $origin_info["ct_period_week"];
				$ct_period_cnt = $origin_info["ct_period_cnt"];
				$ct_period_memo = $origin_info["ct_period_memo"];

				for($k=0; $k<$opt_count; $k++) {

					$io_id = preg_replace(G5_OPTION_ID_FILTER, '', $_POST['io_id'][$it_id][$k]);
			  
					//일괄배송상품 옵션코드값 , 일괄배송일때는 정기주문신청값 N
					$IoIdArray = array("S00140", "S00142", "S00144", "188023","S00687","S00688"); 

					//일괄배송 상품배열에 포함되는지 검사
					if (in_array($io_id, $IoIdArray)){ 

						$ct_period_yn = "N"; //일괄배송일경우 정기주문아님.

						break;

					}

				}

			}

            sql_query(" delete from {$g5['g5_shop_cart_table']} where od_id = '$tmp_cart_id' and it_id = '$it_id' ");
			/*************** 트랜잭션 관련 ****************/
			if( mysqli_errno($g5['connect_db']) ){
				$error_cnt += 1;
			}
			/*************** 트랜잭션 관련 ****************/
		
		}

        //정기주문시 옵션수정이 아닐때..정기주문신청으로 장바구니에 넣고 뒤로가기했다가 정기주문신청안함으로 체크하고 다시 장바구니에 넣을때 
		//정기주문상품이 아닌것으로 되는것을 방지하기 위해 아예 삭제하고 다시 넣어주기.
        if($act != 'optionmod'){

			$sql22 = " select ct_period_yn , ct_period_cnt , ct_period_sdate , ct_period_week , ct_period_memo
					   from {$g5['g5_shop_cart_table']} where od_id = '$tmp_cart_id' and it_id = '$it_id' limit 1  ";
			
			$origin_info = sql_fetch($sql22);

			//장바구니에 있는 정기주문 신청정보와 넘어온 변수값에 신청정보가 다를때 지워준다.
			if( ( $origin_info["ct_period_yn"] != $_POST["ct_period_yn"] ) ){

				$ct_period_yn = $_POST["ct_period_yn"];

				sql_query(" delete from {$g5['g5_shop_cart_table']} where od_id = '$tmp_cart_id' and it_id = '$it_id' ");
				/*************** 트랜잭션 관련 ****************/
				if( mysqli_errno($g5['connect_db']) ){
					$error_cnt += 1;
				}
				/*************** 트랜잭션 관련 ****************/

			}

		}

        // 장바구니에 Insert
        // 바로구매일 경우 장바구니가 체크된것으로 강제 설정
        if($sw_direct) {
            $ct_select = 1;
            $ct_select_time = G5_TIME_YMDHIS;
        } else {
            $ct_select = 0;
            $ct_select_time = '0000-00-00 00:00:00';
        }

        // 장바구니에 Insert
        $comma = '';
        $sql = " INSERT INTO {$g5['g5_shop_cart_table']}
                        ( od_id, mb_id, it_id, it_name, it_sc_type, it_sc_method, it_sc_price, it_sc_minimum, it_sc_qty, ct_status, ct_price, ct_point, ct_point_use, ct_stock_use, ct_option, ct_qty, ct_notax, io_id, io_type, io_price, ct_time, ct_ip, ct_send_cost, ct_direct, ct_select, ct_select_time, ct_period_yn, ct_period_sdate,ct_period_week,ct_period_cnt,ct_period_memo)
                    VALUES ";

        for($k=0; $k<$opt_count; $k++) {
            $io_id = preg_replace(G5_OPTION_ID_FILTER, '', $_POST['io_id'][$it_id][$k]);
            $io_type = preg_replace('#[^01]#', '', $_POST['io_type'][$it_id][$k]);
            $io_value = $_POST['io_value'][$it_id][$k];

            // 선택옵션정보가 존재하는데 선택된 옵션이 없으면 건너뜀
            if($lst_count && $io_id == '')
                continue;

            // 구매할 수 없는 옵션은 건너뜀
            if($io_id && !$opt_list[$io_type][$io_id]['use'])
                continue;

            $io_price = $opt_list[$io_type][$io_id]['price'];
            $ct_qty = $_POST['ct_qty'][$it_id][$k];

            // 구매가격이 음수인지 체크
            if($io_type) {
                if((int)$io_price < 0)
                    alert('구매금액이 음수인 상품은 구매할 수 없습니다.');
            } else {
                if((int)$it['it_price'] + (int)$io_price < 0)
                    alert('구매금액이 음수인 상품은 구매할 수 없습니다.');
            }

            // 동일옵션의 상품이 있으면 수량 더함
            $sql2 = " select ct_id, io_type, ct_qty
                        from {$g5['g5_shop_cart_table']}
                        where od_id = '$tmp_cart_id'
                          and it_id = '$it_id'
                          and io_id = '$io_id'
                          and ct_status = '쇼핑' ";
            $row2 = sql_fetch($sql2);
            if($row2['ct_id']) {
                // 재고체크
                $tmp_ct_qty = $row2['ct_qty'];
                if(!$io_id)
                    $tmp_it_stock_qty = get_it_stock_qty($it_id);
                else
                    $tmp_it_stock_qty = get_option_stock_qty($it_id, $io_id, $row2['io_type']);

                if ($tmp_ct_qty + $ct_qty > $tmp_it_stock_qty)
                {
                    alert($io_value." 의 재고수량이 부족합니다.\\n\\n현재 재고수량 : " . number_format($tmp_it_stock_qty) . " 개");
                }

                $sql3 = " update {$g5['g5_shop_cart_table']}
                            set ct_qty = ct_qty + '$ct_qty'
                            where ct_id = '{$row2['ct_id']}' ";
                sql_query($sql3);
				/*************** 트랜잭션 관련 ****************/
				if( mysqli_errno($g5['connect_db']) ){
					$error_cnt += 1;
				}
				/*************** 트랜잭션 관련 ****************/
                continue;
            }

            // 포인트
            $point = 0;
            if($config['cf_use_point']) {
                if($io_type == 0) {
                    $point = get_item_point($it, $io_id);
                } else {
                    $point = $it['it_supply_point'];
                }

                if($point < 0)
                    $point = 0;
            }

            // 배송비결제
            if($it['it_sc_type'] == 1)
                $ct_send_cost = 2; // 무료
            else if($it['it_sc_type'] > 1 && $it['it_sc_method'] == 1)
                $ct_send_cost = 1; // 착불

            $sql .= $comma."( '$tmp_cart_id', '{$member['mb_id']}', '{$it['it_id']}', '".addslashes($it['it_name'])."', '{$it['it_sc_type']}', '{$it['it_sc_method']}', '{$it['it_sc_price']}', '{$it['it_sc_minimum']}', '{$it['it_sc_qty']}', '쇼핑', '{$it['it_price']}', '$point', '0', '0', '$io_value', '$ct_qty', '{$it['it_notax']}', '$io_id', '$io_type', '$io_price', '".G5_TIME_YMDHIS."', '$REMOTE_ADDR', '$ct_send_cost', '$sw_direct', '$ct_select', '$ct_select_time','$ct_period_yn', '$ct_period_sdate','$ct_period_week','$ct_period_cnt','$ct_period_memo' )";
            $comma = ' , ';
            $ct_count++;
        }

        if ($ct_count > 0)
            sql_query($sql);
        /*************** 트랜잭션 관련 ****************/
        if (mysqli_errno($g5['connect_db'])) {
            $error_cnt += 1;
        }
        /*************** 트랜잭션 관련 ****************/

        $sql = "SELECT DISTINCT(count(ct_id)) AS cnt3 FROM {$g5['g5_shop_cart_table']} WHERE od_id='$tmp_cart_id' AND service_type IS NULL";
        $row = sql_fetch($sql);
        $cnt3 = (int)$row['cnt3'];

    }

}

/*************** 트랜잭션 관련 ****************/
if ($error_cnt > 0) {
	mysqli_rollback($g5['connect_db']);
	mysqli_close($g5['connect_db']);
	echo "
	<script>
		alert('데이터베이스의 에러로 인해 롤백되었습니다.');
		history.back();
	</script>
	";
	exit;
} else {
	mysqli_commit($g5['connect_db']);
} 	
/*************** 트랜잭션 관련 ****************/

// 바로 구매일 경우
if ($sw_direct) {
    if ($is_member) {
        //goto_url(G5_SHOP_URL."/orderform.php?sw_direct=$sw_direct&ct_period_yn=".$ct_period_yn);
        goto_url(G5_SHOP_URL."/orderform.php?sw_direct=$sw_direct");
    } else {
        goto_url(G5_BBS_URL."/login.php?url=".urlencode(G5_SHOP_URL."/orderform.php?sw_direct=$sw_direct").'&noUserOrder=Y&period='.$ct_period_yn);
//        goto_url(G5_SHOP_URL."/orderform.php?sw_direct=$sw_direct");
    }
} else {
    if ($c_status != 1) {//장바구니에서 하는 작업
        if ($ct_period_yn == 'Y') {
            goto_url(G5_SHOP_URL.'/cart.php?period=y');
        } else {
            goto_url(G5_SHOP_URL.'/cart.php');
        }
    } else {//c_status가 1이면, 즉 장바구니담기누르면
        echo $cnt3;
    }
}
?>

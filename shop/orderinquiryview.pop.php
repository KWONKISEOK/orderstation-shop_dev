<?php
include_once('./_common.php');
// 불법접속을 할 수 없도록 세션에 아무값이나 저장하여 hidden 으로 넘겨서 다음 페이지에서 비교함
$token = md5(uniqid(rand(), true));
set_session("ss_token", $token);

if (!$is_member) {
    if (!get_session('ss_orderview_uid'))
        alert("직접 링크로는 주문서 조회가 불가합니다.\\n\\n주문조회 화면을 통하여 조회하시기 바랍니다.", G5_SHOP_URL);
}


$sql = "select * from {$g5['g5_shop_order_table']} where od_id = '$od_id' {$service_type_and_query} ";
if ($is_member && !$is_admin)
    $sql .= " and mb_id = '{$member['mb_id']}' ";
$od = sql_fetch($sql);
if (!$od['od_id'] || (!$is_member && md5($od['od_id'].$od['od_time'].$od['od_ip']) != get_session('ss_orderview_uid'))) {
    alert("조회하실 주문서가 없습니다.", G5_SHOP_URL);
}

$sql = "select sum(od_total_drug_price) as od_total_drug_price from {$g5['g5_shop_order_detail_table']} where od_id = '$od_id' {$service_type_and_query} ";
$od_dt = sql_fetch($sql);

// 결제방법
$settle_case = $od['od_settle_case'];

if (G5_IS_MOBILE) {
    include_once(G5_MSHOP_PATH.'/orderinquiryview.pop.php');
    return;
}

//배송완료인 상태는 반품이 가능하게끔 처리해주기
if ($order_receiver_cnt > 0) {

    $custom_cancel = true;
    $button_name = "반품 신청하기";
    $submit_url = "./orderinquiryreturn.php";
    $txt_1 = "주문반품";
    $txt_2 = "반품사유";
    $txt_3 = "<option value='기타' data-pay_yn='Y'>기타</option>";
    $txt_4 = "";

} else {

    $return_possible = "";
    $button_name = "주문 취소하기";
    $submit_url = "./orderinquirycancel.php";
    $txt_1 = "주문취소";
    $txt_2 = "취소사유";
    $txt_3 = "";
    $txt_4 = " display:none; ";

}

?>

<div id="sod_fin_cancelfrm">
    <form method="post" action="<?=$submit_url?>" onsubmit="return fcancel_check(this);">
        <input type="hidden" name="od_id" value="<?php echo $od['od_id']; ?>">
        <input type="hidden" name="token" value="<?php echo $token; ?>">
        <input type="hidden" name="return_od_num_check" id="return_od_num_check" value="<?php echo $return_od_num_check; ?>"/>
        <input type="hidden" name="pop_check" value="Y">

        <label for="cancel_memo" class="sound_only"><?=$txt_2?></label>
        <!-- <input type="text" name="cancel_memo" id="cancel_memo" required class="frm_input required" size="40" maxlength="100" placeholder="취소사유"> -->
        <select name="cancel_memo" id="cancel_memo" class="frm_input" required>
            <option value="">--<?=$txt_2?>--</option>
            <option value="필요없어짐(단순변심)" data-pay_yn="Y">필요없어짐(단순변심)</option>
            <option value="색상/사이즈가 기대와 다름" data-pay_yn="Y">색상/사이즈가 기대와 다름</option>
            <option value="상품이 파손되어 배송됨" data-pay_yn="N">상품이 파손되어 배송됨</option>
            <option value="상품 결함/기능에 이상이 있음" data-pay_yn="N">상품 결함/기능에 이상이 있음</option>
            <option value="주문 상품과 다른 상품이 배송됨" data-pay_yn="N">주문 상품과 다른 상품이 배송됨</option>
            <option value="상품이 늦게 배송됨" data-pay_yn="Y">상품이 늦게 배송됨</option>
            <?=$txt_3?>
        </select>
        <span id="memo_space" style="display:none;">
            <input type="text" name="client_memo" id="client_memo" class="frm_input" size="40" maxlength="255" placeholder="반품기타사유">
        </span>

        <div id="return_text_info" style="border:1px solid #e7ebf1;padding:10px 20px 10px 10px;margin-bottom:5px;line-height:20px;width:350px;<?=$txt_4?>">
            <table>
                <tr>
                    <td width="30"><input type="checkbox" name="return_check_yn" id="return_check_yn" value="Y"></td>
                    <td align="left">단순 변심이나 개인적인 기호의 차이로 인하여 반품 및 교환 요청시, 배송료(왕복배송비)를 고객님께서 부담하셔야 합니다.</td>
                </tr>
            </table>
        </div>
        <div id="return_method_info" style="border:1px solid #e7ebf1;padding:10px 20px 10px 10px;margin-bottom:5px;line-height:20px;width:350px;<?=$txt_4?>">
            <table>
                <tr>
                    <td></td>
                    <td align="left"><strong>왕복 배송비 결제 방법 선택하기<strong></td>
                </tr>
                <tr height="70">
                    <td width="30"><input type="radio" name="od_return_method" value="1"></td>
                    <td align="left">계좌입금 : 국민은행<br>140101-01-008595 (주)오엔케이</td>
                </tr>
                <tr height="25">
                    <td width="30"><input type="radio" name="od_return_method" value="2"></td>
                    <td align="left">택배 박스에 동봉</td>
                </tr>
            </table>
        </div>
        <input type="submit" value="확인" class="btn_frmline">

    </form>
</div>

<script>
    function fcancel_check(f) {
        <?php if( $order_receiver_cnt > 0 ) { ?>

        var $ct_chk = $("input[name^=return_od_num]");
        var chked_cnt = $ct_chk.filter(":checked").size();
        if (chked_cnt == 0) {
            alert("반품하실 상품을 체크해주세요.");
            return false;
        }
        //체크한 상품 문자열 연결해서 넘기기
        var chk = $("input[name^=return_od_num]:checked").map(function () {
            return this.value;
        }).get().join("|");

        $("#return_od_num_check").val(chk);

        var selected = $("#cancel_memo").find('option:selected');
        var pay_yn = selected.data('pay_yn');

        if (f.return_check_yn.checked == false && pay_yn == "Y") {
            alert("배송료 부담 확인여부를 체크해주세요.");
            return false;
        }
        if (pay_yn == "Y" && !document.getElementsByName("od_return_method")[0].checked && !document.getElementsByName("od_return_method")[1].checked) {
            alert("왕복 배송비 결제 방법을 선택해주세요.");
            return false;
        }
        <?php } else { ?>
        var Msg_1 = "주문을 정말 취소하시겠습니까?";
        var Msg_2 = "취소사유를 입력해 주십시오.";
        <?php } ?>

        if (!confirm(Msg_1))
            return false;

        var memo = f.cancel_memo.value;
        if (memo == "") {
            alert(Msg_2);
            return false;
        }

        return true;
    }
</script>
<?php
if (!defined("_GNUBOARD_")) exit; // 개별 페이지 접근 불가

if (!defined("_ORDERINQUIRY_")) exit; // 개별 페이지 접근 불가

// 테마에 orderinquiry.sub.php 있으면 include
if(defined('G5_THEME_SHOP_PATH')) {
    $theme_inquiry_file = G5_THEME_SHOP_PATH.'/orderinquiry.sub.php';
    if(is_file($theme_inquiry_file)) {
        include_once($theme_inquiry_file);
        return;
        unset($theme_inquiry_file);
    }
}
?>

<!-- 주문 내역 목록 시작 { -->
<?php if (!$limit) { ?>총 <?php echo $cnt; ?> 건<?php } ?>

<link rel="stylesheet" href="//code.jquery.com/ui/1.8.18/themes/base/jquery-ui.css" />
<script src="//code.jquery.com/ui/1.8.18/jquery-ui.min.js"></script>


<form name="frm1" method="get" action="/shop/orderinquiry.php" style="margin:0">	

	<div id="search-box">
		<span class="btndate" id="today">오늘</span>&nbsp;
		<span class="btndate" id="bday15">15일</span>&nbsp;
		<span class="btndate" id="bmonth1">1개월</span>&nbsp;
		<span class="btndate" id="bmonth3">3개월</span>&nbsp;&nbsp;
          <input type="text" name="sdate" id="sdate" style="width:90px; text-align:center" class="datepickerbutton" value="<?php echo $sdate;?>" readonly>
          ~
          <input type="text" name="edate" id="edate" style="width:90px; text-align:center" class="datepickerbutton" value="<?php echo $edate;?>" readonly>
          <button class="btndate">조회</button>
	</div>

</form>
<script>
    $.datepicker.setDefaults({
        dateFormat: 'yy-mm-dd',
        prevText: '이전 달',
        nextText: '다음 달',
        monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
        monthNamesShort: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
        dayNames: ['일', '월', '화', '수', '목', '금', '토'],
        dayNamesShort: ['일', '월', '화', '수', '목', '금', '토'],
        dayNamesMin: ['일', '월', '화', '수', '목', '금', '토'],
        showMonthAfterYear: true,
        yearSuffix: '년'
    });

    $(function() {
        $(".datepickerbutton").datepicker();
    });

	function setDate(kind) {
		var today = '<?php echo date("Y-m-d"); ?>';
		var bday15 = '<?php echo date("Y-m-d", strtotime('-15 days')); ?>';
		var bmonth1 = '<?php echo date("Y-m-d", strtotime('-1 month')); ?>';
		var bmonth3 = '<?php echo date("Y-m-d", strtotime('-3 month')); ?>';

		if(kind==1) {
			$("#sdate").val(today);
			$("#edate").val(today);
		}
		if(kind==2) {
			$("#sdate").val(bday15);
			$("#edate").val(today);
		}
		if(kind==3) {
			$("#sdate").val(bmonth1);
			$("#edate").val(today);
		}
		if(kind==4) {
			$("#sdate").val(bmonth3);
			$("#edate").val(today);
		}

	}
	$( "#today" ).click(function() {
	  setDate(1);
	});
	$( "#bday15" ).click(function() {
	  setDate(2);
	});
	$( "#bmonth1" ).click(function() {
	  setDate(3);
	});
	$( "#bmonth3" ).click(function() {
	  setDate(4);
	});

</script>
<div class="tbl_head03 tbl_wrap">
    <table>
    <thead>
    <tr>
        <th scope="col">주문서번호</th>
        <th scope="col">상품명</th>
        <th scope="col">배송지</th>
		<th scope="col">수량</th>
        <th scope="col">소비자가</th>
		<?php if($member['mb_type']=='1') {?>
        <th scope="col">공급가</th>
        <th scope="col">포인트</th>
		<?php } ?>
        <th scope="col">상태</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $sql = " select od_id, od_time, od_ip, od_misu, od_gubun,
	                (select count(*) cnt from tbl_shop_order_receiver where od_id = a.od_id) od_count
               $sql_common
              order by a.od_id desc $limit";


    $result = sql_query($sql);
	$iRow = 0;
    for ($i=0; $row=sql_fetch_array($result); $i++)
    {
        
		$sql = " select b.it_id, b.it_name, b.od_option, b.od_qty, b.od_total_sale_price, b.od_total_drug_price, b.od_total_incen, c.od_status , c.od_pay_yn,  c.od_b_name, c.od_b_addr1, c.od_b_addr2, c.od_b_addr3
		           from tbl_shop_order_detail b, tbl_shop_order_receiver c
				  where b.od_id = c.od_id and b.od_num = c.od_num and b.od_id = '".$row[od_id]."'";

		$result2 = sql_query($sql);
	
		for ($j=0; $row2=sql_fetch_array($result2); $j++)
		{

			$order[$iRow] = $row;
			$order[$iRow][it_id] = $row2[it_id];
			$order[$iRow][it_name] = $row2[it_name];
			$order[$iRow][od_option] = $row2[od_option];

			$order[$iRow][od_qty] = $row2[od_qty];
			$order[$iRow][od_total_sale_price] = $row2[od_total_sale_price];
            $order[$iRow][od_total_drug_price] = $row2[od_total_drug_price];
			$order[$iRow][od_total_incen] = $row2[od_total_incen];
			$order[$iRow][od_status] = $row2[od_status];
			$order[$iRow][od_pay_yn] = $row2[od_pay_yn];
			$order[$iRow][od_b_name] = $row2[od_b_name];
			$order[$iRow][od_b_addr1] = $row2[od_b_addr1];
			$order[$iRow][od_b_addr2] = $row2[od_b_addr2];
			$order[$iRow][od_b_addr3] = $row2[od_b_addr3];
			$iRow++;
		}
	}

	$i=0;
	foreach ($order as $key => $row) {

		$uid = md5($row['od_id'].$row['od_time'].$row['od_ip']);

		$od_id = $row['od_id'];
		if($member['mb_type']=='1') {
			switch($row['od_gubun']) {
				case '1':
					$od_gubun = '<span class="status_01">고객주문</span>';
					break;
				case '2':
					$od_gubun = '<span class="status_02">약국주문</span>';
					break;
				case '3':
					$od_gubun = '<span class="status_03">추전고객</span>';
					break;
				
			}
		}
		switch($row['od_status']) {
			case '주문':
				$od_status = '<span class="status_01">입금확인중</span>';
				break;
			case '확정':
				$od_status = '<span class="status_01">입금확인중</span>';
				break;
			case '입금':
				$od_status = '<span class="status_02">입금완료</span>';
				break;
			case '준비':
				$od_status = '<span class="status_03">상품준비중</span>';
				break;
			case '배송':
				$od_status = '<span class="status_04">상품배송</span>';
				break;
			case '완료':
				$od_status = '<span class="status_05">배송완료</span>';
				break;
			case '반품접수':
				$od_status = '<span class="status_05">반품접수</span>';
				break;
			case '반품승인':
				$od_status = '<span class="status_05">반품접수</span>';
				break;
			case '반품반려':
				$od_status = '<span class="status_05">반품반려</span>';
				break;
			case '반품완료':
				$od_status = '<span class="status_05">반품완료</span>';
				break;
			default:
				$od_status = '<span class="status_06">주문취소</span>';
				break;
		}

		if(($row['od_status'] == "주문" || $row['od_status'] == "확정") && $row['od_pay_yn'] == "Y" ){
			$od_status = '<span class="status_02">입금완료</span>';
		}

		$od_id_count = $row["od_count"];
		$rowspan = true;
		if ($tmp_od_id == $od_id) {
			$rowspan = false;
		}
		$tmp_od_id = $od_id;
		$i++;

    ?>

    <tr>
        <?php  if ($rowspan) { ?>
		<td rowspan="<?php echo $od_id_count?>" style="vertical-align: inherit;">
           
			<?php echo $od_gubun;?><br/>
            <a href="<?php echo G5_SHOP_URL; ?>/orderinquiryview2.php?od_id=<?php echo $row['od_id']; ?>&amp;uid=<?php echo $uid; ?>"><?php echo $row['od_id']; ?></a><br>
			<?php echo substr($row['od_time'],2,14); ?> (<?php echo get_yoil($row['od_time']); ?>)
        </td>
		<?php  } ?>

        <td class=" text_left">
			<?php echo $row['it_name']; ?>
			<?php if($row['od_option']) echo '<br>'.$row['od_option']; ?>
		</td>
        <td class=" text_left">
            <?php echo $row['od_b_name'];?><br>
            <?php echo $row['od_b_addr1'];?><br>
            <?php echo $row['od_b_addr2'];echo $row['od_b_addr3']; ?>
        </td>
		<td class="td_numbig"><?php echo $row['od_qty']; ?></td>
        <td class="td_numbig"><?php echo display_price($row['od_total_sale_price']); ?></td>
		<?php if($member['mb_type']=='1') {?>
        <td class="td_numbig"><?php echo display_price($row['od_total_drug_price']); ?></td>
        <td class="td_numbig text_right"><?php echo number_format($row['od_total_incen']); ?>P</td>
		<?php } ?>
        <td><?php echo $od_status; ?></td>
    </tr>

    <?php
    }

    if ($i == 0)
        echo '<tr><td colspan="7" class="empty_table">주문 내역이 없습니다.</td></tr>';
    ?>
    </tbody>
    </table>
</div>
<!-- } 주문 내역 목록 끝 -->
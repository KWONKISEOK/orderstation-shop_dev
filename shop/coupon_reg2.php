<?php
include_once('./_common.php');

if($is_guest)
    alert('회원이시라면 로그인 후 이용해 보십시오.', '/bbs/login.php?url='.urlencode(G5_SHOP_URL.'/coupon_reg.php'));

include_once('./_head.php');


if( $member['mb_type'] != 0 && $member['mb_id'] != "admin" ) {
	echo "소비자 회원만 접속 가능합니다.";
	exit;
}

if(!G5_IS_MOBILE) {
echo '<div class="site-wrap">
		<div id="aside">&nbsp;';
			include_once(G5_SHOP_PATH.'/asidemy.php');
echo '	</div>
	  <div id="container">';
}
echo '<div id="wrapper_title">쿠폰 인증하기</div>';

?>

<div id="bo_list">

    <div id="bo_btn_top">
        <div id="bo_list_total">&nbsp;</div>
        <ul class="btn_bo_user">&nbsp;</ul>
    </div>

	<div class="tbl_head01 tbl_wrap">

		<form name="frm" method="post">

		<table>
		<caption> 목록</caption>
		<thead>
		<tr>
			<td scope="col" style="border-top: 2px solid #000;border-bottom: 1px solid #d3d3d3;">
				오더스테이션 회원만 인증할 수 있는 쿠폰입니다. 가지고 계시는 쿠폰을 입력하면 인증이 완료됩니다. 발급용 쿠폰은 쿠폰등록하기 메뉴를 이용해주세요.
			</td>
		</tr>
		</thead>
		<tbody>
		<? if(!G5_IS_MOBILE) { ?>
		<tr>
			<td>
				쿠폰번호&nbsp;&nbsp;
				<input type="text" class="frm_input" name="cp_1" id="cp_1" maxlength="4" style="width:80px;" onkeyup="javascript:MoveFocus(this,'cp_2');"> 
				&nbsp;-&nbsp; 
				<input type="text" class="frm_input" name="cp_2" id="cp_2" maxlength="4" style="width:80px;" onkeyup="javascript:MoveFocus(this,'cp_3');"> 
				&nbsp;-&nbsp; 
				<input type="text" class="frm_input" name="cp_3" id="cp_3" maxlength="4" style="width:80px;" onkeyup="javascript:MoveFocus(this,'cp_4');"> 
				&nbsp;-&nbsp; 
				<input type="text" class="frm_input" name="cp_4" id="cp_4" maxlength="4" style="width:80px;" onkeyup="javascript:MoveFocus(this,'');"> 
				&nbsp;
				<a class="btn_b01 btn" href="javascript:RegCoupon();">쿠폰인증</a>
			</td>
		</tr>
		<? }else{ ?>
		<tr>
			<td>
				<center>
					<input type="text" class="frm_input" name="cp_1" id="cp_1" maxlength="4" style="width:60px;" onkeyup="javascript:MoveFocus(this,'cp_2');"> 
					&nbsp;-&nbsp; 
					<input type="text" class="frm_input" name="cp_2" id="cp_2" maxlength="4" style="width:60px;" onkeyup="javascript:MoveFocus(this,'cp_3');"> 
					&nbsp;-&nbsp;
					<input type="text" class="frm_input" name="cp_3" id="cp_3" maxlength="4" style="width:60px;" onkeyup="javascript:MoveFocus(this,'cp_4');"> 
					&nbsp;-&nbsp; 
					<input type="text" class="frm_input" name="cp_4" id="cp_4" maxlength="4" style="width:60px;" onkeyup="javascript:MoveFocus(this,'');"> 
					&nbsp;
				</center>
				<br>
				<center>
					<a class="btn_b01 btn" href="javascript:RegCoupon();" style="width:100px;height:40px;line-height:35px;">쿠폰인증</a>
				</center>
			</td>
		</tr>
		<? } ?>
		</tbody>
		</table>
		
		</form>

	</div>

</div>

<script>

function RegCoupon(){

	var frm = document.frm;

	if(frm.cp_1.value == ""){
		alert("쿠폰번호를 빠짐없이 입력해주세요.");
		frm.cp_1.focus();
		return;
	}
	if(frm.cp_2.value == ""){
		alert("쿠폰번호를 빠짐없이 입력해주세요.");
		frm.cp_2.focus();
		return;
	}
	if(frm.cp_3.value == ""){
		alert("쿠폰번호를 빠짐없이 입력해주세요.");
		frm.cp_3.focus();
		return;
	}
	if(frm.cp_4.value == ""){
		alert("쿠폰번호를 빠짐없이 입력해주세요.");
		frm.cp_4.focus();
		return;
	}

	frm.target = "iFrm"
	frm.action = "./coupon_reg_proc2.php";
	frm.submit();

}

function MoveFocus( GetText , GetTarget ){
	
	if( GetText.value.length == 4 ){
		if( GetTarget != ""){
			document.getElementById(GetTarget).focus();
		}
	}

}

</script>

<iframe name="iFrm" width="100%" height="0" frameborder="0"></iframe> 

<?php
include_once('./_tail.php');
?>
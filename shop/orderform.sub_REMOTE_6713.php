<?php
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가

/*
$period = "";
if ($_REQUEST['ct_period_yn']) $period = $_REQUEST['ct_period_yn'];
echo "period=" . $period . "<br />";
*/

if($period == 'y') {
    // 정기주문
    $default['de_inicis_mid'] = INICIS_R_MID;
    $default['de_inicis_sign_key'] = INICIS_R_SIGN_KEY;
} else {
    // 일반주문
    $default['de_inicis_mid'] = INICIS_MID;
    $default['de_inicis_sign_key'] = INICIS_SIGN_KEY;

    //$default['de_inicis_mid'] = 'INIpayTest';
    //$default['de_inicis_sign_key'] = 'SU5JTElURV9UUklQTEVERVNfS0VZU1RS';

}

//echo "mid=" . $default['de_inicis_mid'] . "<br>";

require_once(G5_SHOP_PATH.'/settle_'.$default['de_pg_service'].'.inc.php');
require_once(G5_SHOP_PATH.'/settle_kakaopay.inc.php');

if( $default['de_inicis_lpay_use'] ){   //이니시스 Lpay 사용시
    require_once(G5_SHOP_PATH.'/inicis/lpay_common.php');
}

// 결제대행사별 코드 include (스크립트 등)
require_once(G5_SHOP_PATH.'/'.$default['de_pg_service'].'/orderform.1.php');

if( $default['de_inicis_lpay_use'] ){   //이니시스 L.pay 사용시
    require_once(G5_SHOP_PATH.'/inicis/lpay_form.1.php');
}

if($is_kakaopay_use) {
    require_once(G5_SHOP_PATH.'/kakaopay/orderform.1.php');
}
?>

<? if( $member[choice_recommend_pharm_control] == "Y" ){ ?>
    <!-- 출고통제약국일 경우 팝업띄워주고 주문하기 버튼 막기 -->
    <div id="PopupLayer_CONTROL" style="width:500px;height:417px;top:56px;left:0px;position:absolute;z-index:9999;border:0px solid #000;" >
        <div style="padding:5px;height:300px;background:#ee8a1a;">
            <center>
                <br><br>

                <div style="color:#fffaf4;font-weight:bold;font-size:35px;"><?=$member[choice_recommend_pharm_name]?> 폐업 안내</div>

                <br>

                <div style="color:#985000;font-weight:bold;font-size:11px;">
                    회원님의 추천약사이신 <?=$member[choice_recommend_pharm_name]?>&nbsp;<?=$member[choice_recommend_name]?>약사님이 <br>내부사정으로 인해 폐업하였음을 알려드립니다.
                </div>

                <br>

                <div style="color:#ffd4a4;font-weight:bold;font-size:11px;">
                    마이페이지 회원정보수정에서 추천약사를 재지정하거나 변경하실 수 있습니다.
                </div>

                <br>

                <div style="color:#985000;font-weight:bold;font-size:11px;">
                    * 추천약사 변경을 통해서만 주문 및 구매가 가능하오니 이 점 양해 바랍니다.
                </div>

                <div style="color:#6e3a00;font-weight:bold;font-size:15px;">
                    변경방법 :마이페이지->정보수정->약사검색해서 재지정하기->수정하기
                </div>

                <br>
                <div style="color:#ffd4a4;font-weight:bold;font-size:15px;">
                    고객센터 : 1544-5462(헬프라인)
                </div>
            </center>
        </div>
        <div style="background:#000; color:#FFFFFF; text-align:right; padding:0 5px;"><span class="b-close_CONTROL" style="cursor:pointer">Close</span></div>
    </div>
<? } ?>

<form name="forderform" id="forderform" method="post" action="<?php echo $order_action_url; ?>" autocomplete="off">
    <div id="sod_frm">
        <div class="breadcrumb">
            <ul>
                <li><a href="#">장바구니</a></li>
                <li class="active"><a href="#">주문/결제</a></li>
                <li><a href="#">주문완료</a></li>
            </ul>
        </div>
        <h2 class="title-section">주문/결제</h2>
        <div class="order-step-2">
            <section class="left-section">
                <div class="agree-wrap">
                    <p class="title">비회원 정보 수집 동의</p>
                    <div class="form-checkbox form-checkall">
                        <input id="checkbox1-1" name="checkbox" type="checkbox" class="checkbox" checked>
                        <label for="checkbox1-1">전체 동의합니다.</label>
                    </div>
                    <div class="agree-box">
                        <div class="form-checkbox">
                            <input id="checkbox1-1" name="checkbox" type="checkbox" class="checkbox" checked>
                            <label for="checkbox1-1">(필수) 비회원 주문에 대한 개인정보수집 이용 동의</label>
                            <button type="button" class="btn-default btn-detail" onclick="terms();">상세보기</button>
                        </div>
                        <div class="form-checkbox">
                            <input id="checkbox1-1" name="checkbox" type="checkbox" class="checkbox" checked>
                            <label for="checkbox1-1">(필수) 이용약관 동의</label>
                            <button type="button" class="btn-default btn-detail" onclick="terms();">상세보기</button>
                        </div>
                    </div>
                </div>
                <h3 class="title-box">
                    <span class="left-side title-main">주문 상품</span>
                </h3>
                <!-- 주문상품 확인 시작 { -->
                <div class="table-body-border">
                    <table id="sod_list">
                        <colgroup>
                            <col width="*">
                            <col width="120px">
                            <col width="120px">
                            <col width="120px">
                            <col width="120px">
                        </colgroup>
                        <thead>
                        <tr>
                            <th scope="col">상품정보</th>
                            <th scope="col">총수량</th>
                            <th scope="col">소비자가</th>
                            <th scope="col">배송비</th>
                            <th scope="col">소계</th>
                            <!-- <th scope="col">포인트</th> -->
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $tot_point = 0;
                        $tot_sell_price = 0;
                        $tot_left_price = 0; //소비자가총계
                        $tot_right_price = 0; //약국가총계

                        $goods = $goods_it_id = "";
                        $goods_count = -1;
                        $period = 'n';

                        //장바구니 검사
                        $result = c_mysqli_call('SP_ORDER_INFO_NEW_VERSION', " '".$s_cart_id."','ORDER','".$member['mb_type']."','".$period."','','' ");

                        $good_info = '';
                        $it_send_cost = 0;
                        $it_cp_count = 0;

                        $comm_tax_mny = 0; // 과세금액
                        $comm_vat_mny = 0; // 부가세
                        $comm_free_mny = 0; // 면세금액
                        $tot_tax_mny = 0;

                        $one_trans = true;

                        $price_change_yn = ""; //가격변동여부
                        $TimeGoodsYn = "N"; //타임세일 제품이 있는지 여부

                        $i=0;
                        $TmpString="";
                        $LeftSum=0; // 소비자가
                        $RightSum=0; // 약국가
                        $trans_memo = "";
                        foreach($result as $row) {
                            // 합계금액 계산
                            $sql = " select SUM(IF(io_type = 1, (io_price * ct_qty), ((ct_price + io_price) * ct_qty))) as price,
                                    SUM(ct_point * ct_qty) as point,
                                    SUM((select it_incen * a.ct_qty from tbl_shop_item where it_id = a.it_id))  as it_incen, 
                                    SUM((select io_incen * a.ct_qty from tbl_shop_item_option where io_id = a.io_id limit 1) ) as io_incen, SUM(ct_qty) as qty,
                                    SUM(ct_qty) as qty
                                from {$g5['g5_shop_cart_table']} a
                                where it_id = '{$row['it_id']}'
                                and od_id = '$s_cart_id' ";
                            $sum = sql_fetch($sql);
                            if (!$goods)
                            {
                                //$goods = addslashes($row[it_name]);
                                //$goods = get_text($row[it_name]);
                                $goods = preg_replace("/\'|\"|\||\,|\&|\;/", "", $row['it_name']);
                                $goods_it_id = $row['it_id'];
                            }
                            $goods_count++;

                            // 에스크로 상품정보
                            if($default['de_escrow_use']) {
                                if ($i>0)
                                    $good_info .= chr(30);
                                $good_info .= "seq=".($i+1).chr(31);
                                $good_info .= "ordr_numb={$od_id}_".sprintf("%04d", $i).chr(31);
                                $good_info .= "good_name=".addslashes($row['it_name']).chr(31);
                                $good_info .= "good_cntx=".$row['ct_qty'].chr(31);
                                $good_info .= "good_amtx=".$row['ct_price'].chr(31);
                            }

                            $image = get_it_image($row['it_id'], 80, 80);

                            $it_name = '<b>' . stripslashes($row['it_name']) . '</b>';
                            $auto_order='';
                            $period_cnt	=0;
                            $period_cnt		= $row['ct_period_cnt'];
                            if($row['ct_period_yn'] == 'Y') {

                                $period = 'y';//정기주문
                                $period_total = 0;
                                $period_cnt = $row['ct_period_cnt'];

                                //정기주문 기간계산
                                $sdate			= $row['ct_period_sdate'];
                                $edate			= $row['ct_period_sdate'];
                                $period_week	= $row['ct_period_week'];


                                for($i = 2; $i <= $period_cnt; $i++) {
                                    $str_date = strtotime($edate);
                                    if($period_week == '1') {
                                        $str_date = strtotime($edate.'+7 days'); // +1 days로 하루를 더함
                                    }
                                    if($period_week == '2') {
                                        $str_date = strtotime($edate.'+14 days');
                                    }
                                    if($period_week == '3') {
                                        $str_date = strtotime($edate.'+21 days');
                                    }
                                    if($period_week == '4') {
                                        $str_date = strtotime($edate.'+28 days');
                                    }
                                    if($period_week == '5') {
                                        $str_date = strtotime("+".($i-1)." month",strtotime($row['ct_period_sdate']));
                                    }
                                    $edate =  date('Y-m-d',$str_date);
                                    //echo $edate;
                                }
                                if($period_week == '5') {
                                    $offerPeriod = str_replace('-','',$sdate) .'-'. str_replace('-','',$edate);
                                    $auto_order= ' * 정기주문 : '. $offerPeriod.' 주기:지정일마다 횟수:'.$row['ct_period_cnt'];
                                }else{
                                    $offerPeriod = str_replace('-','',$sdate) .'-'. str_replace('-','',$edate);
                                    $auto_order= ' * 정기주문 : '. $offerPeriod.' 주기:'.$row['ct_period_week'].'횟수:'.$row['ct_period_cnt'];
                                }
                            }

                            $it_options = print_item_options_new($row['it_id'], $s_cart_id);
                            if($it_options) {

                                $TmpString=explode("|", $it_options);
                                $LeftSum=$LeftSum+$TmpString[1];
                                $RightSum=$RightSum+$TmpString[2];

                                $it_name .= '<div class="sod_opt">'.$TmpString[0].'</div>';
                            }

                            // 복합과세금액
                            if($default['de_tax_flag_use']) {
                                if($row['it_notax']) {
                                    $comm_free_mny += $sum['price'];
                                } else {
                                    $tot_tax_mny += $sum['price'];
                                }
                            }

                            //정기주문 예외처리 상품
                            $optionprice = get_option_dis_price_new($row['it_id'], $row['io_id'],$row['io_type']);

                            if($optionprice > 0) {
                                $sell_price = $sum['qty'] * ($row['ct_price']+$optionprice);
                            } else {
                                if($period_cnt > 0) {
                                    $sell_price = $sum['price'] * $period_cnt;
                                    $incen_sum = ($sum['it_incen']+$sum['io_incen']) * $period_cnt;
                                } else {
                                    $sell_price = $sum['price'];
                                    $incen_sum = $sum['it_incen']+$sum['io_incen'];
                                }
                            }

                            /* 상품별 배송비 정책 노출 */
                            if($row['it_sc_type'] != 1){

                                if($row['it_sc_type'] =='2') $trans_memo = number_format($row['it_sc_price']).'원 (소비자가 '.number_format($row['it_sc_minimum']).'원 이상 무료 )';
                                if($row['it_sc_type'] =='3') $trans_memo = number_format($row['it_sc_price']).'원';
                                if($row['it_sc_type'] =='4') $trans_memo = number_format($row['it_sc_price']).'원 <br>(본 상품은 X개 단위 묶음포장 상품으로 X개 단위로 배송비 ooo원이 추가 부과됩니다.)';

                            }


                            // 쿠폰
                            if($is_member) {
                                $cp_button = '';
                                $cp_count = 0;

                                $sql = " select cp_id
                                    from {$g5['g5_shop_coupon_table']}
                                    where mb_id IN ( '{$member['mb_id']}', '소비자회원' )
                                    and cp_start <= '".G5_TIME_YMD."'
                                    and cp_end >= '".G5_TIME_YMD."'
                                    and cp_minimum <= '$sell_price'
                                    and (
                                            ( cp_method = '0' and instr( cp_target , concat( concat('|','{$row['it_id']}'),'|' ) ) > 0 )
                                            OR
                                            ( cp_method = '1' and instr( cp_target , concat( concat('|','{$row['ca_id']}'),'|' ) ) > 0 )
                                            OR
                                            ( cp_method = '1' and instr( cp_target , concat( concat('|','{$row['ca_id2']}'),'|' ) ) > 0 )
                                            OR
                                            ( cp_method = '1' and instr( cp_target , concat( concat('|','{$row['ca_id3']}'),'|' ) ) > 0 )
                                        ) and service_type is null ";

                                //echo "<pre>".$sql."</pre>";

                                $res = sql_query($sql);

                                for($k=0; $cp=sql_fetch_array($res); $k++) {
                                    if(is_used_coupon($member['mb_id'], $cp['cp_id']))
                                        continue;

                                    $cp_count++;
                                }

                                if($cp_count) {
                                    $cp_button = '<button type="button" class="cp_btn">쿠폰적용</button>';
                                    $it_cp_count++;
                                }
                            }

                            // 배송비
                            /*
                            switch($row['ct_send_cost'])
                            {
                                case 1:
                                    $ct_send_cost = '착불';
                                    break;
                                case 2:
                                    $ct_send_cost = '무료';
                                    break;
                                default:
                                    $ct_send_cost = '주문시결제';
                                    break;
                            }
                            */
                            if( $row['cal_it_sc_price'] == 0 ){
                                $ct_send_cost = '무료';
                            }else{
                                $ct_send_cost = '주문시결제';
                            }

                            $sendcost = $row['cal_it_sc_price'];

                            //배송비업데이트
                            if($sendcost>=0) {
                                $sql = " update tbl_shop_cart set it_sc_cost = '$sendcost' where ct_id='{$row['ct_id']}' ";
                                $rtncost = sql_fetch($sql);
                            }

                            $total_cost = $total_cost + $sendcost;

                            ?>
                            <tr>

                                <td>
                                    <div class="item-detail">
                                        <div class="sod_img"><?php echo $image; ?></div>
                                        <div class="sod_name">
                                            <input type="hidden" name="it_id[<?php echo $i; ?>]"    value="<?php echo $row['it_id']; ?>">
                                            <input type="hidden" name="it_name[<?php echo $i; ?>]"  value="<?php echo get_text($row['it_name']); ?>">
                                            <input type="hidden" name="it_price[<?php echo $i; ?>]" value="<?php echo $sell_price; ?>">
                                            <input type="hidden" name="cp_id[<?php echo $i; ?>]" value="">
                                            <input type="hidden" name="cp_price[<?php echo $i; ?>]" value="0">
                                            <input type="hidden" name="it_sc_cost[<?php echo $i; ?>]" value="<?php echo $sendcost; ?>">

                                            <?php if($default['de_tax_flag_use']) { ?>
                                                <input type="hidden" name="it_notax[<?php echo $i; ?>]" value="<?php echo $row['it_notax']; ?>">
                                            <?php } ?>
                                            <?php echo $it_name; ?>
                                            <?php echo $auto_order; ?>
                                            <div>
                                                <ul style="list-style:none;margin:0;padding:0;">
                                                    <li style="margin: 0 0 0 0;padding: 0 0 0 0;float: left;">
                                                        <?php echo $cp_button; ?>
                                                    </li>
                                                    <li style="margin: 0 0 0 0;padding: 0 0 0 0;float: left;">
                                                        <div style="padding-top:12px;padding-left:10px;"><?php echo $trans_memo; ?></div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td class="td_num"><?php echo number_format($sum['qty']); ?></td>
                                <td class="td_numbig">
                                    <?if( $row['price_change_yn_sum'] != 0 ){?>
                                    <strike>
                                        <?}?>
                                        <?php
                                        /*
                                        if($optionprice > 0) {
                                            echo number_format($row['ct_price']+$optionprice);
                                        } else {
                                            echo number_format($row['ct_price']+$row['io_price']);
                                        }
                                        */
                                        ?>
                                        <?php echo number_format($LeftSum); ?>
                                        <?if( $row['price_change_yn_sum'] != 0 ){?>
                                    </strike>
                                <br> <font color='red'>가격변동 상품</font>
                                <?}?>
                                    <? if( $row['it_type4'] == "1" && $time_sale_yn == "N" ){ ?>
                                        <br> <font color='blue'>타임세일 상품</font>
                                    <? } ?>
                                </td>


                                <!-- <td class="td_numbig  text_right"><?php echo number_format($point); ?></td> -->
                                <td class="td_dvr"><?php echo $ct_send_cost; ?>
                                    <br/><?php if($sendcost>0) echo number_format($sendcost); ?>
                                </td>
                                <td class="td_numbig">
                                    <span class="total_price"><?php echo number_format($LeftSum+$sendcost); ?></span>
                                </td>
                            </tr>

                            <?php
                            $tot_left_price += $LeftSum;
                            $tot_right_price += $RightSum;
                            $tot_point      += $point;
                            $tot_sell_price += $sell_price;
                            $RightSum = 0;
                            $LeftSum = 0;
                            $trans_memo = "";

                            //가격변동 체크여부
                            if( $row['price_change_yn_sum'] != 0 ){
                                $price_change_yn = "Y";
                            }
                            //타임세일상품 체크여부
                            if( $row['it_type4'] == "1" && $time_sale_yn == "N" ){
                                $TimeGoodsYn = "Y";
                            }

                            $i++;
                        } // for 끝

                        if ($i == 0) {
                            //echo '<tr><td colspan="7" class="empty_table">장바구니에 담긴 상품이 없습니다.</td></tr>';
                            alert('장바구니가 비어 있습니다.', G5_SHOP_URL.'/cart.php');
                        } else {
                            // 배송비 계산
                            //$send_cost = get_sendcost($s_cart_id);
                        }

                        // 복합과세처리
                        if($default['de_tax_flag_use']) {
                            $comm_tax_mny = round(($tot_tax_mny + $total_cost) / 1.1);
                            $comm_vat_mny = ($tot_tax_mny + $total_cost) - $comm_tax_mny;
                        }
                        ?>
                        </tbody>
                    </table>
                </div>

                <?php if ($goods_count) $goods .= ' 외 '.$goods_count.'건'; ?>
                <!-- } 주문상품 확인 끝 -->

                <input type="hidden" name="od_price"    value="<?php echo $tot_sell_price; ?>">
                <input type="hidden" name="org_od_price"    value="<?php echo $tot_sell_price; ?>">
                <input type="hidden" name="od_send_cost" value="<?php echo $total_cost; ?>">
                <input type="hidden" name="od_send_cost2" value="0">
                <input type="hidden" name="item_coupon" value="0">
                <input type="hidden" name="od_coupon" value="0">
                <input type="hidden" name="od_send_coupon" value="0">
                <input type="hidden" name="od_goods_name" value="<?php echo $goods; ?>">

                <?php
                // 결제대행사별 코드 include (결제대행사 정보 필드)
                if($period=='y') {

                    //정기주문
                    if($optionprice > 0) {
                        $tot_price = $tot_sell_price + $total_cost;
                    } else {
                        //$tot_price = ($tot_sell_price*$period_cnt) + $total_cost;
                        $tot_price = ($tot_sell_price) + $total_cost;
                    }


                    //echo $tot_price;
                    require_once(G5_SHOP_PATH.'/'.$default['de_pg_service'].'/orderform.21.php');
                } else {
                    require_once(G5_SHOP_PATH.'/'.$default['de_pg_service'].'/orderform.2.php');

                    if($is_kakaopay_use) {
                        require_once(G5_SHOP_PATH.'/kakaopay/orderform.2.php');
                    }
                }


                ?>

                <!-- 주문하시는 분 입력 시작 { -->
                <section id="sod_frm_orderer">
                    <h3 class="title-box"><span class="left-side title-main">주문자 정보</span></h3>
                    <table>
                        <tbody>
                            <tr>

                                <?php if($member['mb_type']==1) { ?>
                                    <td>
                                        <!-- <a href="<?php echo G5_SHOP_URL;?>/orderuser.php" id="order_user" class="btn_frmline">고객주문</a> -->

                                        <div id="radio" class="ui-buttonset">
                                            <input type="radio" id="od_gubun1" name="od_gubun" onclick="myaddr(1);od_gubun_chg1();" value="1" ><label for="od_gubun1" ><span class="">고객주문</span></label>
                                            <input type="radio" id="od_gubun2" name="od_gubun" value="2" onclick="od_gubun_chg2()" checked ><label for="od_gubun2" ><span class="">약국주문</span></label>

                                        </div>
                                    </td>
                                <?php } else { ?>
                                    <td> <input type="hidden" name="od_gubun" value="3"> </td>
                                <?php }  ?>

                            </tr>
                        </tbody>
                    </table>

                    <div class="table-top-bottom-border">
                        <table>
                            <colgroup>
                                <col width="200px">
                                <col width="*">
                            </colgroup>
                            <tbody>

                                <tr>
                                    <th scope="row"><label for="od_name">주문하시는 분<i class="icon icon-required">필수</i></label></th>
                                    <td><input type="text" name="od_name" value="<?php echo get_text($member['mb_name']); ?>" id="od_name" required class="input-default input-md" maxlength="20"></td>
                                </tr>

                                <?php if (!$is_member) { // 비회원이면 ?>
                                    <tr>
                                        <th scope="row"><label for="od_pwd">주문 비밀번호<i class="icon icon-required">필수</i></label></th>
                                        <td>
                                            <input type="password" name="od_pwd" id="od_pwd" required class="input-default input-md" maxlength="20">
                                            <span class="input-warning-sm"><i class="icon icon-warning"></i>주문 비밀번호 입력은 필수입니다.</span>
                                        </td>
                                    </tr>
                                <?php } ?>
                                <tr>
                                    <th scope="row"><label for="od_pwd">비밀번호 재확인<i class="icon icon-required">필수</i></label></th>
                                    <td>
                                        <!-- <span class="frm_info">영,숫자 3~20자 (주문서 조회시 필요)</span> -->
                                        <input type="password" name="od_pwd" id="od_pwd" required class="input-default input-md" maxlength="20">
                                        <span class="input-warning-sm"><i class="icon icon-warning"></i>비밀번호가 일치하지 않습니다.</span>
                                    </td>
                                </tr>
                                <!--
                                <tr>
                                    <th scope="row"><label for="od_tel">전화번호<strong class="sound_only"> 필수</strong></label></th>
                                    <td><input type="text" name="od_tel" value="<?php echo get_text($member['mb_tel']); ?>" id="od_tel" required class="input-default input-md required" maxlength="20"></td>
                                </tr>
                                -->

                                <tr>
                                    <th scope="row"><label for="od_hp">휴대폰 번호<i class="icon icon-required">필수</i> </label></th>
                                    <td>
                                        <div class="form-set form-md">
                                            <select name="">
                                                <option value="">선택</option>
                                                <option value="">010</option>
                                                <option value="">011</option>
                                                <option value="">016</option>
                                                <option value="">017</option>
                                                <option value="">018</option>
                                                <option value="">019</option>
                                            </select>
                                            <input type="text" name="" id="" required class="input-default" maxlength="20"/>
                                            <input type="text" name="" id="" required class="input-default" maxlength="20"/>
                                        </div>
                                    </td>
                                </tr>
                          
                                <!-- 숨김처리 2019.08.26 -->
                                <tr style="display:none;">
                                    <th scope="row">주소</th>
                                    <td>
                                        <label for="od_zip" class="sound_only">우편번호<strong class="sound_only"> 필수</strong></label>
                                        <input type="text" name="od_zip" value="<?php echo $member['mb_zip1'].$member['mb_zip2']; ?>" id="od_zip"  size="8" maxlength="6" placeholder="우편번호" readonly>
                                        <button type="button" class="btn_address" onclick="win_zip('forderform', 'od_zip', 'od_addr1', 'od_addr2', 'od_addr3', 'od_addr_jibeon');">우편번호 찾기</button><br>
                                        <input type="text" name="od_addr1" value="<?php echo get_text($member['mb_addr1']) ?>" id="od_addr1"  size="60" placeholder="기본주소">
                                        <label for="od_addr1" class="sound_only">기본주소<strong class="sound_only"> 필수</strong></label><br>
                                        <input type="text" name="od_addr2" value="<?php echo get_text($member['mb_addr2']) ?>" id="od_addr2" class="input-default input-md frm_address" size="60" placeholder="상세주소">
                                        <label for="od_addr2" class="sound_only">상세주소</label>
                                        <br>
                                        <input type="text" name="od_addr3" value="<?php echo get_text($member['mb_addr3']) ?>" id="od_addr3" class="input-default input-md frm_address" size="60" readonly="readonly" placeholder="참고항목">
                                        <label for="od_addr3" class="sound_only">참고항목</label><br>
                                        <input type="hidden" name="od_addr_jibeon" value="<?php echo get_text($member['mb_addr_jibeon']); ?>">
                                    </td>
                                </tr>
                                <tr <?php echo ($is_member) ? 'style="display:none;"' : '';?>>
                                    <th scope="row"><label for="od_email">이메일<i class="icon icon-required">필수</i></label></th>
                                    <td>
                                        <div class="form-set form-email">
                                            <input type="text" name="od_email" value="<?php echo $member['mb_email']; ?>" id="od_email" required class="input-default input-md" size="35" maxlength="100">
                                            <select name="">
                                                <option value="">직접입력</option>
                                                <option value="">naver.com</option>
                                                <option value="">daum.net</option>
                                            </select>
                                        </div>
                                    </td>
                                </tr>
                                <!-- 숨김처리 2019.08.26 끝 -->

                                <?php if ($default['de_hope_date_use']) { // 배송희망일 사용 ?>
                                    <tr>
                                        <th scope="row"><label for="od_hope_date">희망배송일</label></th>
                                        <td>
                                            <!-- <select name="od_hope_date" id="od_hope_date">
                                            <option value="">선택하십시오.</option>
                                            <?php
                                            for ($i=0; $i<7; $i++) {
                                                $sdate = date("Y-m-d", time()+86400*($default['de_hope_date_after']+$i));
                                                echo '<option value="'.$sdate.'">'.$sdate.' ('.get_yoil($sdate).')</option>'.PHP_EOL;
                                            }
                                            ?>
                                            </select> -->
                                            <input type="text" name="od_hope_date" value="" id="od_hope_date" required class="input-default input-md required" size="11" maxlength="10" readonly="readonly"> 이후로 배송 바랍니다.
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </section>
                <!-- } 주문하시는 분 입력 끝 -->

                <!-- 받으시는 분 입력 시작 { -->






                <?php
                if($is_member) {
                    // 배송지 이력
                    $addr_list = '';
                    $sep = chr(30);
                    // 신규 배송지
                    $addr_list .= '<div class="form-radio">'.PHP_EOL;
                    $addr_list .= '<input type="radio" name="ad_sel_addr" value="new" id="od_sel_addr_new">'.PHP_EOL;
                    $addr_list .= '<label for="od_sel_addr_new">신규배송지</label>'.PHP_EOL;
                    $addr_list .= '</div>'.PHP_EOL;

                    /*
                    // 주문자와 동일
                    $addr_list .= '<input type="radio" name="ad_sel_addr" value="same" id="ad_sel_addr_same">'.PHP_EOL;
                    $addr_list .= '<label for="ad_sel_addr_same">주문자와 동일</label>'.PHP_EOL; */

                    // 기본배송지
                    $sql = " select *
                                    from {$g5['g5_shop_order_address_table']}
                                    where mb_id = '{$member['mb_id']}'
                                    and ad_default = '1' ";
                    $row = sql_fetch($sql);
                    if($row['ad_id']) {
                        $val1 = $row['ad_name'].$sep.$row['ad_tel'].$sep.$row['ad_hp'].$sep.$row['ad_zip1'].$sep.$row['ad_zip2'].$sep.$row['ad_addr1'].$sep.$row['ad_addr2'].$sep.$row['ad_addr3'].$sep.$row['ad_jibeon'].$sep.$row['ad_subject'];
                        $addr_list .= '<input type="radio" name="ad_sel_addr" value="'.get_text($val1).'" id="ad_sel_addr_def">'.PHP_EOL;
                        $addr_list .= '<label for="ad_sel_addr_def">기본배송지</label>'.PHP_EOL;
                    }

                    // 최근배송지
                    $sql = " select *
                                    from {$g5['g5_shop_order_address_table']}
                                    where mb_id = '{$member['mb_id']}'
                                    and ad_default = '0'
                                    order by ad_id desc
                                    limit 1 ";
                    $result = sql_query($sql);
                    for($i=0; $row=sql_fetch_array($result); $i++) {
                        $val1 = $row['ad_name'].$sep.$row['ad_tel'].$sep.$row['ad_hp'].$sep.$row['ad_zip1'].$sep.$row['ad_zip2'].$sep.$row['ad_addr1'].$sep.$row['ad_addr2'].$sep.$row['ad_addr3'].$sep.$row['ad_jibeon'].$sep.$row['ad_subject'];
                        $val2 = '<label for="ad_sel_addr_'.($i+1).'">최근배송지('.($row['ad_subject'] ? $row['ad_subject'] : $row['ad_name']).')</label>';
                        $addr_list .= '<div class="form-radio">'.PHP_EOL;
                        $addr_list .= '<input type="radio" name="ad_sel_addr" value="'.get_text($val1).'" id="ad_sel_addr_'.($i+1).'"> '.PHP_EOL.$val2.PHP_EOL;
                        $addr_list .= '</div>'.PHP_EOL;
                    }




                    if($member['mb_type']==1) {
                        $addr_list .='<a href="javascript:myaddr(2);" class="btn_frmline">나의고객리스트</a>';
                    } else {
                        $addr_list .='<a href="'.G5_SHOP_URL.'/orderaddress.php" id="order_address" class="btn_frmline">배송지목록</a>';
                    }


                } else {
                    // 주문자와 동일
                    $addr_list .= '<div class="form-checkbox">'.PHP_EOL;
                    $addr_list .= '<input type="checkbox" name="ad_sel_addr" value="same" id="ad_sel_addr_same">'.PHP_EOL;
                    $addr_list .= '<label for="ad_sel_addr_same">주문자 정보와 동일</label>'.PHP_EOL;
                    $addr_list .= '</div>'.PHP_EOL;
                }
                ?>
                <section id="sod_frm_taker">
                    <h3 class="title-box">
                        <span class="left-side title-main">배송 정보</span>
                        <span class="right-side title-sub"><?php echo $addr_list; ?></span>
                    </h3>
                    <div class="table-top-bottom-border">
                        <table>
                            <colgroup>
                                <col width="200px">
                                <col width="*">
                            </colgroup>
                            <tbody>
                            <?php if($is_member) { ?>
                                <tr>
                                    <th scope="row"><label for="ad_subject">배송지명</label></th>
                                    <td>
                                        <input type="text" name="ad_subject" id="ad_subject" class="input-default input-md" maxlength="20">
                                        <div class="form-checkbox ml-10">
                                            <input type="checkbox" name="ad_default" id="ad_default" value="1">
                                            <label for="ad_default">기본배송지로 설정</label>
                                        </div>
                                    </td>
                                </tr>
                                <?php } ?>
                                <tr>
                                    <th scope="row"><label for="od_b_name">받으시는 분<i class="icon icon-required">필수</i></label></th>
                                    <td><input type="text" name="od_b_name" id="od_b_name" required class="input-default input-md" maxlength="20"></td>
                                </tr>
                                <tr>
                                    <th scope="row"><label for="od_b_hp">휴대폰 번호<i class="icon icon-required">필수</i></label></th>
                                    <td>
                                        <div class="form-set form-md">
                                            <select name="">
                                                <option value="">선택</option>
                                                <option value="">010</option>
                                                <option value="">011</option>
                                                <option value="">016</option>
                                                <option value="">017</option>
                                                <option value="">018</option>
                                                <option value="">019</option>
                                            </select>
                                            <input type="text" name="" id="" required class="input-default" maxlength="20"/>
                                            <input type="text" name="" id="" required class="input-default" maxlength="20"/>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">배송 주소<i class="icon icon-required">필수</i></th>
                                    <td id="sod_frm_addr">
                                        <label for="od_b_zip" class="sound_only">우편번호<strong class="sound_only"> 필수</strong></label>
                                        <div class="form-set form-md">
                                            <input type="text" name="od_b_zip" id="od_b_zip" required class="input-default" size="8" maxlength="6" readonly>
                                            <button type="button" class="btn-default btn-address" onclick="win_zip('forderform', 'od_b_zip', 'od_b_addr1', 'od_b_addr2', 'od_b_addr3', 'od_b_addr_jibeon');">우편번호 찾기</button>
                                        </div>
                                        <div class="form-set">
                                            <label for="od_b_addr1" class="sound_only">기본주소<strong>필수</strong></label>
                                            <input type="text" name="od_b_addr1" id="od_b_addr1" required class="input-default input-md" readonly="readonly">
                                            <label for="od_b_addr2" class="sound_only">상세주소</label>
                                            <input type="text" name="od_b_addr2" id="od_b_addr2" class="input-default" placeholder="상세주소">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row"><label for="od_memo">배송메시지</label></th>
                                    <td>
                                        <div class="form-dropdown form-md is-active">
                                            <input class="dropdown-toggle" placeholder="배송 메시지를 선택해 주세요." id="dropdownMenuButton1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" readonly="readonly"/>
                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                                <button class="dropdown-item" type="button">부재 시 경비실에 맡겨주세요.</button>
                                                <button class="dropdown-item" type="button">부재 시 연락 부탁드립니다.</button>
                                                <button class="dropdown-item" type="button">부재 시 문 앞에 놓아주세요.</button>
                                                <button class="dropdown-item" type="button">직접입력</button>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </section>
                <!-- } 받으시는 분 입력 끝 -->

                <!-- [D]할인정보 추가 -->
                <section>
                    <h3 class="title-box">
                        <span class="left-side title-main">할인 정보</span>
                    </h3>
                    <div class="table-th-bg">
                        <table>
                            <colgroup>
                                <col width="150px">
                                <col width="*">
                            </colgroup>
                            <tbody>
                                <tr>
                                    <th>쿠폰 할인</th>
                                    <td>
                                        <ul>
                                            <li class="discount-list">
                                                <span class="discount-label">상품/주문 쿠폰</span>
                                                <div class="form-set form-underline">
                                                    <input type="text" class="input-default" value="14,300">
                                                    <span class="unit">원</span>
                                                </div>
                                                <!-- [D] 쿠폰이 없는 경우 disabled 추가 -->
                                                <button type="button" class="btn-default btn-detail"><span>쿠폰사용</span></button>
                                                <p class="input-guide-sm">*사용 가능 쿠폰 : 1장 / 보유 쿠폰 : 10장</p>
                                            </li>
                                            <li class="discount-list">
                                                <span class="discount-label">배송비 쿠폰</span>
                                                <div class="form-set form-underline">
                                                    <input type="text" class="input-default" value="0">
                                                    <span class="unit">원</span>
                                                </div>
                                                <!-- [D] 쿠폰이 없는 경우 disabled 추가 -->
                                                <button type="button" class="btn-default btn-detail"><span>쿠폰사용</span></button>
                                                <p class="input-guide-sm">*사용 가능 쿠폰 : 0장 / 보유 쿠폰 : 2장</p>
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <th>포인트 사용</th>
                                    <td>
                                        <ul>
                                            <li class="discount-list">
                                                <span class="discount-label">사용 포인트</span>
                                                <div class="form-set form-underline">
                                                    <input type="text" class="input-default" value="500">
                                                    <span class="unit">원</span>
                                                </div>
                                                <!-- [D] 쿠폰이 없는 경우 disabled 추가 -->
                                                <button type="button" class="btn-default btn-detail" disabled><span>전액사용</span></button>
                                                <p class="form-checkbox checkbox-round">
                                                    <input id="checkboxpointuse" type="checkbox" class="checkbox" name="" checked>
                                                    <label for="checkboxpointuse">항상 적립 포인트 전액사용</label>
                                                </p>
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                            </tbody>

                        </table>
                    </div>
                </section>
                                
                <section id="od_pay_sl">
                    <h3 class="title-box">
                        <span class="left-side title-main">결제 수단</span>
                    </h3>
                    <div class="table-top-bottom-border">
                        <?php
                        if($member['mb_type'] != '1' or !$is_member) {
                            if (!$default['de_card_point'])
                                //echo '<p id="sod_frm_pt_alert"><strong>무통장입금</strong> 이외의 결제 수단으로 결제하시는 경우 포인트를 적립해드리지 않습니다.</p>';

                                // echo '<p>&nbsp;</p>';

                            //echo '<p id="sod_frm_pt_alert"><strong>카카오페이</strong> 는 준비중입니다.</p>';

                            $multi_settle == 0;
                            $checked = '';

                            $escrow_title = "";
                            if ($default['de_escrow_use']) {
                                $escrow_title = "에스크로<br>";
                            }

                            if ($is_kakaopay_use || $default['de_bank_use'] || $default['de_vbank_use'] || $default['de_iche_use'] || $default['de_card_use'] || $default['de_hp_use'] || $default['de_easy_pay_use'] || $default['de_inicis_lpay_use']) {
                                echo '<fieldset id="sod_frm_paysel">';
                                echo '<legend>결제방법 선택</legend>';
                                echo '<div class="form-list form-payment">';
                            }

                            if($period != 'y') {

                                // 카카오페이
                                /*
                                if($is_kakaopay_use) {
                                    $multi_settle++;
                                    echo '<input type="radio" id="od_settle_kakaopay" name="od_settle_case" value="KAKAOPAY" '.$checked.'> <label for="od_settle_kakaopay" class="kakaopay_icon lb_icon">KAKAOPAY</label>'.PHP_EOL;
                                    $checked = '';
                                }*/

                                // 가상계좌 사용
                                /*if ($default['de_vbank_use']) {
                                    $multi_settle++;
                                    echo '<input type="radio" id="od_settle_vbank" name="od_settle_case" value="가상계좌" '.$checked.'> <label for="od_settle_vbank" class="lb_icon vbank_icon">'.$escrow_title.'가상계좌</label>'.PHP_EOL;
                                    $checked = '';
                                }*/
                                //포인트결제
                                if (1) {
                                    $multi_settle++;
                                    echo '<span style="display:none;"><input type="radio" id="od_settle_point" name="od_settle_case" value="포인트" '.$checked.'> <label for="od_settle_point" class="lb_icon bank_icon">포인트결제</label></span>'.PHP_EOL;
                                    $checked = '';
                                }

                                // 휴대폰 사용
                                /*
                                if ($default['de_hp_use']) {
                                    $multi_settle++;
                                    echo '<input type="radio" id="od_settle_hp" name="od_settle_case" value="휴대폰" '.$checked.'> <label for="od_settle_hp" class="lb_icon hp_icon">휴대폰</label>'.PHP_EOL;
                                    $checked = '';
                                }*/
                                // [D] 계좌이체 변경
                                if ($default['de_iche_use']) {
                                    $multi_settle++;
                                    echo '<div class="form-radio"><input type="radio" id="od_settle_iche" name="od_settle_case" value="계좌이체" '.$checked.'> <label for="od_settle_iche">'.$escrow_title.'계좌이체</label></div>'.PHP_EOL;
                                    $checked = '';
                                }
                                // [D] 신용카드 변경
                                if ($default['de_card_use']) {
                                    $multi_settle++;
                                    echo '<div class="form-radio"><input type="radio" id="od_settle_card" name="od_settle_case" value="신용카드" '.$checked.'> <label for="od_settle_card">신용카드</label></div>'.PHP_EOL;
                                    $checked = '';
                                }
                                // [D] 무통장입금 변경
                                if ($default['de_bank_use']) {
                                    $multi_settle++;
                                    echo '<div class="form-radio"><input type="radio" id="od_settle_bank" name="od_settle_case" value="무통장" '.$checked.'> <label for="od_settle_bank">무통장입금</label></div>'.PHP_EOL;
                                    $checked = '';
                                }
                                // [D] 휴대폰결제 변경
                                if ($default['de_hp_use']) {
                                    $multi_settle++;
                                    echo '<div class="form-radio"><input type="radio" id="od_settle_hp" name="od_settle_case" value="휴대폰" '.$checked.'> <label for="od_settle_hp">휴대폰</label></div>'.PHP_EOL;
                                    $checked = '';
                                }

                                // PG 간편결제
                                if($default['de_easy_pay_use']) {
                                    switch($default['de_pg_service']) {
                                        case 'lg':
                                            $pg_easy_pay_name = 'PAYNOW';
                                            break;
                                        case 'inicis':
                                            $pg_easy_pay_name = 'KPAY';
                                            break;
                                        default:
                                            $pg_easy_pay_name = 'PAYCO';
                                            break;
                                    }

                                    $multi_settle++;
                                    echo '<input type="radio" id="od_settle_easy_pay" name="od_settle_case" value="간편결제" '.$checked.'> <label for="od_settle_easy_pay" class="'.$pg_easy_pay_name.' lb_icon">'.$pg_easy_pay_name.'</label>'.PHP_EOL;
                                    $checked = '';
                                }

                                //이니시스 Lpay
                                if($default['de_inicis_lpay_use']) {
                                    echo '<input type="radio" id="od_settle_inicislpay" data-case="lpay" name="od_settle_case" value="lpay" '.$checked.'> <label for="od_settle_inicislpay" class="inicis_lpay lb_icon">L.pay</label>'.PHP_EOL;
                                    $checked = '';
                                }

                                $temp_point = 0;
                                // 회원이면서 포인트사용이면
                                if ($is_member && $config['cf_use_point'])
                                {
                                    // 포인트 결제 사용 포인트보다 회원의 포인트가 크다면
                                    if ($member['mb_point'] > 0)
                                    {
                                        /*
                                        $temp_point = (int)$default['de_settle_max_point'];

                                        if($temp_point > (int)$tot_sell_price)
                                            $temp_point = (int)$tot_sell_price;

                                        if($temp_point > (int)$member['mb_point'])
                                            $temp_point = (int)$member['mb_point'];

                                        $point_unit = (int)$default['de_settle_point_unit'];
                                        $temp_point = (int)((int)($temp_point / $point_unit) * $point_unit);
                                        */

                                        $temp_point = $tot_price;
                                        ?>
                                        <div class="sod_frm_point" <?=$point_view?> >
                                            <div>
                                                <label for="od_temp_point">사용 포인트</label>
                                                <input type="hidden" name="max_temp_point" value="<?php echo $tot_price; ?>">
                                                <input type="text" name="od_temp_point" value="0" id="od_temp_point"  size="7"> 점
                                            </div>
                                            <div id="sod_frm_pt">
                                                <span><strong>보유포인트</strong><?php echo display_point($member['mb_point']); ?></span>
                                                <span class="max_point_box"><strong>최대 사용 가능 포인트</strong><em id="use_max_point"><?php echo display_point($tot_price); ?></em></span>
                                            </div>
                                        </div>
                                        <?php
                                        $multi_settle++;
                                    }
                                }

                                if ($default['de_bank_use']) {

                                    // 은행계좌를 배열로 만든후
                                    $str = explode("\n", trim($default['de_bank_account']));
                                    if (count($str) <= 1)
                                    {
                                        $bank_account = '<input type="hidden" name="od_bank_account" value="'.$str[0].'">'.$str[0].PHP_EOL;
                                    }
                                    else
                                    {
                                        $bank_account = '<select name="od_bank_account" id="od_bank_account">'.PHP_EOL;
                                        $bank_account .= '<option value="">선택하십시오.</option>';
                                        for ($i=0; $i<count($str); $i++)
                                        {
                                            //$str[$i] = str_replace("\r", "", $str[$i]);
                                            $str[$i] = trim($str[$i]);
                                            $bank_account .= '<option value="'.$str[$i].'">'.$str[$i].'</option>'.PHP_EOL;
                                        }
                                        $bank_account .= '</select>'.PHP_EOL;
                                    }
                                    echo '<div id="settle_bank" style="display:none">';
                                    echo '<label for="od_bank_account" class="sound_only">입금할 계좌</label>';
                                    echo $bank_account;
                                    echo '<br><label for="od_deposit_name">입금자명</label> ';
                                    echo '<input type="text" name="od_deposit_name" id="od_deposit_name" size="10" maxlength="20">';
                                    echo '</div>';
                                }

                                if ($is_kakaopay_use || $default['de_bank_use'] || $default['de_vbank_use'] || $default['de_iche_use'] || $default['de_card_use'] || $default['de_hp_use'] || $default['de_easy_pay_use'] || $default['de_inicis_lpay_use'] ) {
                                    echo '</fieldset>';
                                }

                                if ($multi_settle == 0)
                                    echo '<p>결제할 방법이 없습니다.<br>운영자에게 알려주시면 감사하겠습니다.</p>';
                            } else {
                                // 신용카드 사용
                                if ($default['de_card_use']) {
                                    //$multi_settle++;
                                    echo '<input type="radio" id="od_settle_card" name="od_settle_case" checked value="신용카드" '.$checked.'> <label for="od_settle_card" class="lb_icon card_icon">신용카드</label>'.PHP_EOL;
                                    $checked = '';
                                }
                            }

                        } else {
                            ?>
                            <input type="hidden" id="od_settle_bank" name="od_settle_case" value="무통장">
                            <input type="hidden" name="od_bank_account" value="약국주문">
                            <input type="hidden" name="od_deposit_name" value="약국주문">
                            <?php
                        }
                        ?>
                    </div>
                </section>
            </section>
            <section class="right-section">
                <div class="float-payinfo-func">
                    <div class="payment-info-box">
                        <h4>결제 정보</h4>
                        <ul class="">
                            <li class="payment-list">
                                <span class="title">총 상품 금액</span>
                                <span class="desc"><span class="amount">7,560</span><span class="unit">원</span></span>
                            </li>
                            <li class="payment-list">
                                <span class="title">총 상품 금액</span>
                                <span class="desc">무료</span>
                            </li>
                        </ul>
                        <p class="payment-list payment-result">
                            <span class="title">결제 예정 금액</span>
                            <span class="desc"><span class="amount amount-total">7,560</span><span class="unit">원</span></span>
                        </p>
                    </div>
                    <div class="payment-agree-box">
                        <div class="agree-box">
                            <div class="form-checkbox">
                                <input id="checkboxagree-all" name="checkbox-agree" type="checkbox" class="checkbox">
                                <label for="checkboxagree-all">전체 동의합니다.</label>
                                <button type="button" class="btn-default btn-detail">상세보기</button>
                            </div>
                            <div class="form-checkbox">
                                <input id="checkboxagree1" name="checkbox-agree" type="checkbox" class="checkbox">
                                <label for="checkboxagree1">(필수) 주문 상품정보에 동의</label>
                                <button type="button" class="btn-default btn-detail mt-5 ml-0">상세보기</button>
                            </div>
                            <div class="form-checkbox">
                                <input id="checkboxagree2" name="checkbox-agree" type="checkbox" class="checkbox">
                                <label for="checkboxagree2">(필수) 결제 대행 서브스 이용을 위한<br/> 개인정보 제3자 제공 및 위탁 동의</label>
                                <button type="button" class="btn-default btn-detail mt-5 ml-0">상세보기</button>
                            </div>
                        </div>
                        <button class="btn-payment"><span>결제하기</span></button>
                    </div>
                </div>

                <!-- 결제정보 입력 시작 { -->
                <?php


                    $oc_cnt = $sc_cnt = 0;
                    if($is_member) {
                        // 주문쿠폰
                        $sql = " select cp_id
                                from {$g5['g5_shop_coupon_table']}
                                where mb_id IN ( '{$member['mb_id']}', '전체회원' )
                                and cp_method = '2'
                                and cp_start <= '".G5_TIME_YMD."'
                                and cp_end >= '".G5_TIME_YMD."'
                                and cp_minimum <= '$tot_sell_price' and service_type is null ";
                        $res = sql_query($sql);

                        for($k=0; $cp=sql_fetch_array($res); $k++) {
                            if(is_used_coupon($member['mb_id'], $cp['cp_id']))
                                continue;

                            $oc_cnt++;
                        }

                        if($total_cost > 0) {
                            // 배송비쿠폰
                            $sql = " select cp_id
                                    from {$g5['g5_shop_coupon_table']}
                                    where mb_id IN ( '{$member['mb_id']}', '전체회원' )
                                    and cp_method = '3'
                                    and cp_start <= '".G5_TIME_YMD."'
                                    and cp_end >= '".G5_TIME_YMD."'
                                    and cp_minimum <= '$tot_sell_price' and service_type is null ";
                            $res = sql_query($sql);

                            for($k=0; $cp=sql_fetch_array($res); $k++) {
                                if(is_used_coupon($member['mb_id'], $cp['cp_id']))
                                    continue;

                                $sc_cnt++;
                            }
                        }
                    }
                ?>

                <?php
                    //가격변동이 없을때 주문할수 있음..
                    if( $price_change_yn != "Y" ){
                        // 출고통제약국일 경우 팝업띄워주고 주문하기 버튼 막기
                        if ($member[choice_recommend_pharm_control] == "N" || $is_member == false) {
                            // 결제대행사별 코드 include (주문버튼)
                            require_once(G5_SHOP_PATH.'/'.$default['de_pg_service'].'/orderform.3.php');
                        }
                    }else{

                        if( $price_change_yn == "Y" ){
                            $ViewMsg_1 = "alert('가격변동이 있는 상품이 있습니다.가격변동상품을 삭제후 다시 주문하십시요.');";
                        }
                        if( $TimeGoodsYn == "Y" ){
                            $ViewMsg_2 = "alert('타임세일 상품은 세일 시간에만 구매 가능합니다.');";
                        }
                        ?>
                        <div id="display_pay_button" class="btn_confirm">
                            <input type="button" value="주문하기" onclick="<?=$ViewMsg_1?><?=$ViewMsg_2?>" class="btn_submit">
                            <a href="javascript:history.go(-1);" class="btn01">취소</a>
                        </div>
                        <div id="display_pay_process" style="display:none">
                            <img src="<?php echo G5_URL; ?>/shop/img/loading.gif" alt="">
                            <span>주문완료 중입니다. 잠시만 기다려 주십시오.</span>
                        </div>
                        <?
                    }


                    if($is_kakaopay_use) {
                        require_once(G5_SHOP_PATH.'/kakaopay/orderform.3.php');
                    }
                ?>

                <?php
                    if ($default['de_escrow_use']) {
                        // 결제대행사별 코드 include (에스크로 안내)
                        require_once(G5_SHOP_PATH.'/'.$default['de_pg_service'].'/orderform.4.php');
                    }
                ?>
            </section>
            <script>
                $(window).resize(function () {
                    if($(window).scrollTop() > $(".right-section").offset().top) {
                        $(".float-payinfo-func").css({"position": "fixed","top": "78px"});
                        if($(window).width() > 1280) {
                            $(".float-payinfo-func").css({"left": "50%", "margin-left": '320px'});
                        } else if($(window).width() < 1280) {
                            $(".float-payinfo-func").css({"left": "960px", "margin-left": '0px'});
                        }
                    }
                    else if(($(window).scrollTop() < $(".right-section").offset().top)) {
                        $(".float-payinfo-func").css({"position": "absolute", "left": "unset"});
                    }
                });

                $(window).scroll(function(event){
                    if($(window).scrollTop() > $(".right-section").offset().top) {
                        $(".float-payinfo-func").css({"position": "fixed","top": "78px"});
                        if($(window).width() > 1280) {
                            $(".float-payinfo-func").css({"left": "50%", "margin-left": '320px'});
                        } else if($(window).width() < 1280) {
                            $(".float-payinfo-func").css({"left": "960px", "margin-left": '0px'});
                        }
                    }
                    else if(($(window).scrollTop() < $(".right-section").offset().top)) {
                        $(".float-payinfo-func").css({"position": "absolute", "left": "unset"});
                    }
                });

            </script>

        </div>
    </div>
</form>

<?php
if( $default['de_inicis_lpay_use'] ){   //이니시스 L.pay 사용시
    require_once(G5_SHOP_PATH.'/inicis/lpay_order.script.php');
}
?>
<script>
    var zipcode = "";
    var form_action_url = "<?php echo $order_action_url; ?>";

    $(function() {
        var $cp_btn_el;
        var $cp_row_el;

        $(".cp_btn").click(function() {
            $cp_btn_el = $(this);
            $cp_row_el = $(this).closest("tr");
            $("#cp_frm").remove();
            var it_id = $cp_btn_el.closest("tr").find("input[name^=it_id]").val();

            $.post(
                "./orderitemcoupon.php",
                { it_id: it_id,  sw_direct: "<?php echo $sw_direct; ?>" },
                function(data) {
                    $cp_btn_el.after(data);
                }
            );
        });

        $(document).on("click", ".cp_apply", function() {
            var $el = $(this).closest("tr");
            var cp_id = $el.find("input[name='f_cp_id[]']").val();
            var price = $el.find("input[name='f_cp_prc[]']").val();
            var subj = $el.find("input[name='f_cp_subj[]']").val();
            var sell_price;
            var sc_cost;

            if(parseInt(price) == 0) {
                if(!confirm(subj+"쿠폰의 할인 금액은 "+price+"원입니다.\n쿠폰을 적용하시겠습니까?")) {
                    return false;
                }
            }

            // 이미 사용한 쿠폰이 있는지
            var cp_dup = false;
            var cp_dup_idx;
            var $cp_dup_el;
            $("input[name^=cp_id]").each(function(index) {
                var id = $(this).val();

                if(id == cp_id) {
                    cp_dup_idx = index;
                    cp_dup = true;
                    $cp_dup_el = $(this).closest("tr");;

                    return false;
                }
            });

            if(cp_dup) {
                var it_name = $("input[name='it_name["+cp_dup_idx+"]']").val();
                if(!confirm(subj+ "쿠폰은 "+it_name+"에 사용되었습니다.\n"+it_name+"의 쿠폰을 취소한 후 적용하시겠습니까?")) {
                    return false;
                } else {
                    coupon_cancel($cp_dup_el);
                    $("#cp_frm").remove();
                    $cp_dup_el.find(".cp_btn").text("적용").focus();
                    $cp_dup_el.find(".cp_cancel").remove();
                }
            }

            var $s_el = $cp_row_el.find(".total_price");
            sc_cost = parseInt($cp_row_el.find("input[name^=it_sc_cost]").val());
            sell_price = parseInt($cp_row_el.find("input[name^=it_price]").val());
            sell_price = sell_price - parseInt(price) + parseInt(sc_cost);

            if(sell_price < 0) {
                alert("쿠폰할인금액이 상품 주문금액보다 크므로 쿠폰을 적용할 수 없습니다.");
                return false;
            }
            $s_el.text(number_format(String(sell_price)));
            $cp_row_el.find("input[name^=cp_id]").val(cp_id);
            $cp_row_el.find("input[name^=cp_price]").val(price);

            calculate_total_price();
            $("#cp_frm").remove();
            $cp_btn_el.text("변경").focus();
            if(!$cp_row_el.find(".cp_cancel").size())
                $cp_btn_el.after("<button type=\"button\" class=\"cp_cancel\">취소</button>");
        });

        $(document).on("click", "#cp_close", function() {
            $("#cp_frm").remove();
            $cp_btn_el.focus();
        });

        $(document).on("click", ".cp_cancel", function() {
            coupon_cancel($(this).closest("tr"));
            calculate_total_price();
            $("#cp_frm").remove();
            $(this).closest("tr").find(".cp_btn").text("적용").focus();
            $(this).remove();
        });

        $("#od_coupon_btn").click(function() {
            $("#od_coupon_frm").remove();
            var $this = $(this);
            var price = parseInt($("input[name=org_od_price]").val()) - parseInt($("input[name=item_coupon]").val());
            if(price <= 0) {
                alert('상품금액이 0원이므로 쿠폰을 사용할 수 없습니다.');
                return false;
            }
            $.post(
                "./ordercoupon1.php",
                { price: price },
                function(data) {
                    $this.after(data);
                }
            );
        });

        $(document).on("click", ".od_cp_apply", function() {
            var $el = $(this).closest("tr");
            var cp_id = $el.find("input[name='o_cp_id[]']").val();
            var price = parseInt($el.find("input[name='o_cp_prc[]']").val());
            var subj = $el.find("input[name='o_cp_subj[]']").val();
            var send_cost = $("input[name=od_send_cost]").val();
            var item_coupon = parseInt($("input[name=item_coupon]").val());
            var od_price = parseInt($("input[name=org_od_price]").val()) - item_coupon;

            if(price == 0) {
                if(!confirm(subj+"쿠폰의 할인 금액은 "+price+"원입니다.\n쿠폰을 적용하시겠습니까?")) {
                    return false;
                }
            }

            if(od_price - price < 0) {
                alert("쿠폰할인금액이 주문금액보다 크므로 쿠폰을 적용할 수 없습니다.1");
                return false;
            }

            $("input[name=sc_cp_id]").val("");
            $("#sc_coupon_btn").text("쿠폰적용");
            $("#sc_coupon_cancel").remove();

            $("input[name=od_price]").val(od_price - price);
            $("input[name=od_cp_id]").val(cp_id);
            $("input[name=od_coupon]").val(price);
            $("input[name=od_send_coupon]").val(0);
            $("#od_cp_price").text(number_format(String(price)));
            $("#sc_cp_price").text(0);
            calculate_order_price();
            $("#od_coupon_frm").remove();
            $("#od_coupon_btn").text("변경").focus();
            if(!$("#od_coupon_cancel").size())
                $("#od_coupon_btn").after("<button type=\"button\" id=\"od_coupon_cancel\" class=\"cp_cancel\">취소</button>");
        });

        $(document).on("click", "#od_coupon_close", function() {
            $("#od_coupon_frm").remove();
            $("#od_coupon_btn").focus();
        });

        $(document).on("click", "#od_coupon_cancel", function() {
            var org_price = $("input[name=org_od_price]").val();
            var item_coupon = parseInt($("input[name=item_coupon]").val());
            $("input[name=od_price]").val(org_price - item_coupon);
            $("input[name=od_cp_id]").val("");
            $("input[name=sc_cp_id]").val("");
            $("input[name=od_coupon]").val(0);
            $("input[name=od_send_coupon]").val(0);
            $("#od_cp_price").text(0);
            $("#sc_cp_price").text(0);
            calculate_order_price();
            $("#od_coupon_frm").remove();
            $("#od_coupon_btn").text("쿠폰적용").focus();
            $(this).remove();
            $("#sc_coupon_btn").text("쿠폰적용");
            $("#sc_coupon_cancel").remove();
        });

        $("#sc_coupon_btn").click(function() {
            $("#sc_coupon_frm").remove();
            var $this = $(this);
            var price = parseInt($("input[name=od_price]").val());
            var send_cost = parseInt($("input[name=od_send_cost]").val());
            $.post(
                "./ordersendcostcoupon.php",
                { price: price, send_cost: send_cost },
                function(data) {
                    $this.after(data);
                }
            );
        });

        $(document).on("click", ".sc_cp_apply", function() {
            var $el = $(this).closest("tr");
            var cp_id = $el.find("input[name='s_cp_id[]']").val();
            var price = parseInt($el.find("input[name='s_cp_prc[]']").val());
            var subj = $el.find("input[name='s_cp_subj[]']").val();
            var send_cost = parseInt($("input[name=od_send_cost]").val());

            if(parseInt(price) == 0) {
                if(!confirm(subj+"쿠폰의 할인 금액은 "+price+"원입니다.\n쿠폰을 적용하시겠습니까?")) {
                    return false;
                }
            }

            $("input[name=sc_cp_id]").val(cp_id);
            $("input[name=od_send_coupon]").val(price);
            $("#sc_cp_price").text(number_format(String(price)));
            calculate_order_price();
            $("#sc_coupon_frm").remove();
            $("#sc_coupon_btn").text("변경").focus();
            if(!$("#sc_coupon_cancel").size())
                $("#sc_coupon_btn").after("<button type=\"button\" id=\"sc_coupon_cancel\" class=\"cp_cancel\">취소</button>");
        });

        $(document).on("click", "#sc_coupon_close", function() {
            $("#sc_coupon_frm").remove();
            $("#sc_coupon_btn").focus();
        });

        $(document).on("click", "#sc_coupon_cancel", function() {
            $("input[name=od_send_coupon]").val(0);
            $("#sc_cp_price").text(0);
            calculate_order_price();
            $("#sc_coupon_frm").remove();
            $("#sc_coupon_btn").text("쿠폰적용").focus();
            $(this).remove();
        });

        $("#od_b_addr2").focus(function() {
            var zip = $("#od_b_zip").val().replace(/[^0-9]/g, "");
            if(zip == "")
                return false;

            var code = String(zip);

            if(zipcode == code)
                return false;

            zipcode = code;
            calculate_sendcost(code);
        });

        $("#od_settle_bank").on("click", function() {
            $("[name=od_deposit_name]").val( $("[name=od_name]").val() );
            $("#settle_bank").show();
        });

        $("#od_settle_iche,#od_settle_card,#od_settle_vbank,#od_settle_hp,#od_settle_easy_pay,#od_settle_kakaopay").bind("click", function() {
            $("#settle_bank").hide();
            $(".sod_frm_point").show();
        });

        $("#od_settle_vbank").on("click", function() {
            $("[name=od_temp_point]").val(0);
            $(".sod_frm_point").hide();
        });

        // 배송지선택
        $("input[name=ad_sel_addr]").on("click", function() {
            var addr = $(this).val().split(String.fromCharCode(30));

            if (addr[0] == "same") {
                gumae2baesong();
            } else {
                if(addr[0] == "new") {
                    for(i=0; i<10; i++) {
                        addr[i] = "";
                    }
                }

                var f = document.forderform;
                f.od_b_name.value        = addr[0];
                //f.od_b_tel.value         = addr[1];
                f.od_b_hp.value          = addr[2];
                f.od_b_zip.value         = addr[3] + addr[4];
                f.od_b_addr1.value       = addr[5];
                f.od_b_addr2.value       = addr[6];
                f.od_b_addr3.value       = addr[7];
                f.od_b_addr_jibeon.value = addr[8];
                f.ad_subject.value       = addr[9];

                var zip1 = addr[3].replace(/[^0-9]/g, "");
                var zip2 = addr[4].replace(/[^0-9]/g, "");

                var code = String(zip1) + String(zip2);

                if(zipcode != code) {
                    calculate_sendcost(code);
                }
            }
        });




        // 고객주문
        /*$("#od_gubun1").on("click", function() {
            var url = this.href;
            window.open(url, "win_address", "left=100,top=100,width=800,height=600,scrollbars=1");
            return false;
        });
        */
        // 배송지목록
        $("#order_address").on("click", function() {
            var url = this.href;
            window.open(url, "win_address", "left=100,top=100,width=800,height=600,scrollbars=1");
            return false;
        });


        $(document).on("click", ".save_bill", function() {
            $("#save_info").toggle(100);
        });

        $(document).on("change", "#select_bill_info", function() {

            var SelectValue = $("#select_bill_info option:selected").val();
            var TmpTxt = "<font color='red'>선택한 결제수단으로 향후 결제 이용에 동의합니다</font>";

            if( SelectValue == "2" ){
                $("#bill_txt").html(TmpTxt);
            }else{
                $("#bill_txt").empty();
            }

        });


    });

    function SaveInfo(GetIdx){

        if(confirm("선택하신 카드정보를 기본결제수단으로 등록하시겠습니까?")) {

            $.ajax({
                type: "POST",
                url: "./save_info.php",
                cache: false,
                async: false,
                data: { m_idx: GetIdx },
                dataType: "html",
                success: function(data) {

                    $("#save_total_info").empty();
                    $("#save_total_info").html(data);

                }
            });

        }

    }

    function DelInfo(GetIdx){

        if(confirm("선택하신 카드정보를 삭제하시겠습니까?")) {

            $.ajax({
                type: "POST",
                url: "./del_info.php",
                cache: false,
                async: false,
                data: { m_idx: GetIdx },
                dataType: "html",
                success: function(data) {
                    console.log(data);
                    $("#save_info").empty();
                    $("#save_info").html(data);

                }
            });

        }

    }

    function od_gubun_chg2(){
        var f = document.forderform;

        /*frm.ord_name.value 		= "인포약국";
        frm.ord_tel1.value 		= "02";
        frm.ord_tel2.value 		= "391";
        frm.ord_tel3.value 		= "0660";
        frm.ord_hp1.value 		= "010";
        frm.ord_hp2.value 		= "3605";
        frm.ord_hp3.value 		= "5879";
        frm.ord_zipcode.value 	= "122-829";
        frm.ord_addr1.value 	= "서울특별시 은평구 녹번동 77~91";
        frm.ord_addr2.value 	= "83-54번지 원기빌딩 202호1212";*/

        f.od_name.value        = "<?php echo get_text($member['mb_name']); ?>";
        //f.od_tel.value         = "<?php echo get_text($member['mb_tel']); ?>";
        f.od_hp.value          = "<?php echo get_text($member['mb_hp']); ?>";
        f.od_zip.value         = "<?php echo $member['mb_zip1'].$member['mb_zip2']; ?>";
        f.od_addr1.value       = "<?php echo get_text($member['mb_addr1']) ?>";
        f.od_addr2.value       = "<?php echo get_text($member['mb_addr2']) ?>";
        f.od_addr3.value       = "<?php echo get_text($member['mb_addr3']) ?>";
        f.od_addr_jibeon.value = "<?php echo get_text($member['mb_addr_jibeon']); ?>";
        f.od_email.value       = "<?php echo $member['mb_email']; ?>";

    }

    function od_gubun_chg1(){
        var f = document.forderform;

        f.od_name.value        = "";
        //f.od_tel.value         = "";
        f.od_hp.value          = "";
        f.od_zip.value         = "";
        f.od_addr1.value       = "";
        f.od_addr2.value       = "";
        f.od_addr3.value       = "";
        f.od_addr_jibeon.value = "";
        f.od_email.value       = "";

    }
    function myaddr(gubun) {
        var url = "<?php echo G5_SHOP_URL;?>/orderuser.php?gubun="+gubun ;
        window.open(url, "win_address", "left=100,top=100,width=800,height=600,scrollbars=1");
        return false;
    }



    function coupon_cancel($el)
    {
        var $dup_sell_el = $el.find(".total_price");
        var $dup_price_el = $el.find("input[name^=cp_price]");
        var org_sell_price = $el.find("input[name^=it_price]").val();

        $dup_sell_el.text(number_format(String(org_sell_price)));
        $dup_price_el.val(0);
        $el.find("input[name^=cp_id]").val("");
    }

    function calculate_total_price()
    {
        var $it_prc = $("input[name^=it_price]");
        var $cp_prc = $("input[name^=cp_price]");
        var tot_sell_price = sell_price = tot_cp_price = 0;
        var it_price, cp_price, it_notax;
        var tot_mny = comm_tax_mny = comm_vat_mny = comm_free_mny = tax_mny = vat_mny = 0;
        var send_cost = parseInt($("input[name=od_send_cost]").val());

        $it_prc.each(function(index) {
            it_price = parseInt($(this).val());
            cp_price = parseInt($cp_prc.eq(index).val());
            sell_price += it_price;
            tot_cp_price += cp_price;
        });

        tot_sell_price = sell_price - tot_cp_price + send_cost;

        $("#ct_tot_coupon").text(number_format(String(tot_cp_price)));
        $("#ct_tot_price").text(number_format(String(tot_sell_price)));

        $("input[name=good_mny]").val(tot_sell_price);
        $("input[name=od_price]").val(sell_price - tot_cp_price);
        $("input[name=item_coupon]").val(tot_cp_price);
        //$("input[name=od_coupon]").val(0);
        //$("input[name=od_send_coupon]").val(0);
        <?//php if($oc_cnt > 0) { ?>
        //$("input[name=od_cp_id]").val("");
        //$("#od_cp_price").text(0);
        /*
        if($("#od_coupon_cancel").size()) {
            $("#od_coupon_btn").text("쿠폰적용");
            $("#od_coupon_cancel").remove();
        }
        */
        <?//php } ?>
        <?//php if($sc_cnt > 0) { ?>
        //$("input[name=sc_cp_id]").val("");
        //$("#sc_cp_price").text(0);
        /*
        if($("#sc_coupon_cancel").size()) {
            $("#sc_coupon_btn").text("쿠폰적용");
            $("#sc_coupon_cancel").remove();
        }
        */
        <?//php } ?>
        $("input[name=od_temp_point]").val(0);
        <?php if($temp_point > 0 && $is_member) { ?>
        calculate_temp_point();
        <?php } ?>
        calculate_order_price();
    }

    function calculate_order_price()
    {
        var sell_price = parseInt($("input[name=org_od_price]").val());
        var send_cost = parseInt($("input[name=od_send_cost]").val());
        var send_cost2 = parseInt($("input[name=od_send_cost2]").val());
        var send_coupon = parseInt($("input[name=od_send_coupon]").val());
        var item_coupon = parseInt($("input[name=item_coupon]").val());
        var od_coupon = parseInt($("input[name=od_coupon]").val());
        var tot_price = sell_price + send_cost + send_cost2 - send_coupon - item_coupon - od_coupon;

        $("input[name=good_mny]").val(tot_price);
        $("#od_tot_price .print_price").text(number_format(String(tot_price)));

        // 쿠폰을 사용하여 결제금액이 0원이 되는 경우
        if (tot_price == 0 && $("input[name=od_cp_id]").val() != '') {
            $('#od_settle_point').closest('span').show();
            $("input:radio[id='od_settle_point']").prop("checked", true);

            $("input:radio[id='od_settle_card']").attr('disabled', true);
            $("input:radio[id='od_settle_iche']").attr('disabled', true);
        } else {
            $('#od_settle_point').closest('span').hide();
            $("input:radio[id='od_settle_point']").prop("checked", false);

            $("input:radio[id='od_settle_card']").attr('disabled', false);
            $("input:radio[id='od_settle_iche']").attr('disabled', false);
        }
        <?php if($temp_point > 0 && $is_member) { ?>
        calculate_temp_point();
        <?php } ?>
    }

    function calculate_temp_point()
    {
        var sell_price = parseInt($("input[name=od_price]").val());
        var mb_point = parseInt(<?php echo $member['mb_point']; ?>);
        var max_point = parseInt(<?php echo $default['de_settle_max_point']; ?>);
        var point_unit = parseInt(<?php echo $default['de_settle_point_unit']; ?>);
        var temp_point = max_point;

        if(temp_point > sell_price)
            temp_point = sell_price;

        if(temp_point > mb_point)
            temp_point = mb_point;

        temp_point = parseInt(temp_point / point_unit) * point_unit;

        //$("#use_max_point").text(number_format(String(temp_point))+"점");
        //$("input[name=max_temp_point]").val(temp_point);
    }

    function calculate_sendcost(code)
    {
        $.post(
            "./ordersendcost.php",
            { zipcode: code },
            function(data) {
                $("input[name=od_send_cost2]").val(data);
                $("#od_send_cost2").text(number_format(String(data)));

                zipcode = code;

                calculate_order_price();
            }
        );
    }

    function calculate_tax()
    {
        var $it_prc = $("input[name^=it_price]");
        var $cp_prc = $("input[name^=cp_price]");
        var sell_price = tot_cp_price = 0;
        var it_price, cp_price, it_notax;
        var tot_mny = comm_free_mny = tax_mny = vat_mny = 0;
        var send_cost = parseInt($("input[name=od_send_cost]").val());
        var send_cost2 = parseInt($("input[name=od_send_cost2]").val());
        var od_coupon = parseInt($("input[name=od_coupon]").val());
        var send_coupon = parseInt($("input[name=od_send_coupon]").val());
        var temp_point = 0;

        $it_prc.each(function(index) {
            it_price = parseInt($(this).val());
            cp_price = parseInt($cp_prc.eq(index).val());
            sell_price += it_price;
            tot_cp_price += cp_price;
            it_notax = $("input[name^=it_notax]").eq(index).val();
            if(it_notax == "1") {
                comm_free_mny += (it_price - cp_price);
            } else {
                tot_mny += (it_price - cp_price);
            }
        });

        if($("input[name=od_temp_point]").size())
            temp_point = parseInt($("input[name=od_temp_point]").val());

        tot_mny += (send_cost + send_cost2 - od_coupon - send_coupon - temp_point);
        if(tot_mny < 0) {
            comm_free_mny = comm_free_mny + tot_mny;
            tot_mny = 0;
        }

        tax_mny = Math.round(tot_mny / 1.1);
        vat_mny = tot_mny - tax_mny;
        $("input[name=comm_tax_mny]").val(tax_mny);
        $("input[name=comm_vat_mny]").val(vat_mny);
        $("input[name=comm_free_mny]").val(comm_free_mny);
    }

    function forderform_check(f)
    {
        // 재고체크
        var stock_msg = order_stock_check();
        if(stock_msg != "") {
            alert(stock_msg);
            return false;
        }

        errmsg = "";
        errfld = "";
        var deffld = "";

        check_field(f.od_name, "주문하시는 분 이름을 입력하십시오.");
        if (typeof(f.od_pwd) != 'undefined')
        {
            clear_field(f.od_pwd);
            if( (f.od_pwd.value.length<3) || (f.od_pwd.value.search(/([^A-Za-z0-9]+)/)!=-1) )
                error_field(f.od_pwd, "회원이 아니신 경우 주문서 조회시 필요한 비밀번호를 3자리 이상 입력해 주십시오.");
        }
        <?php if (!$is_member) // 비회원 주문 핸드폰번호 체크 ?>
        check_field(f.od_hp, "주문하시는 분 핸드폰 번호를 입력하십시오.");
        //check_field(f.od_tel, "주문하시는 분 전화번호를 입력하십시오.");

        /* 주문자 주소 입력 없애기 2019.08.26*/
        // check_field(f.od_addr1, "주소검색을 이용하여 주문하시는 분 주소를 입력하십시오.");
        /* 끝 */

        //check_field(f.od_addr2, " 주문하시는 분의 상세주소를 입력하십시오.");

        /* 주문자 주소 입력 없애기 2019.08.26*/
        check_field(f.od_zip, "");
        /* 끝 */

        clear_field(f.od_email);
        if(f.od_email.value=='' || f.od_email.value.search(/(\S+)@(\S+)\.(\S+)/) == -1)
            error_field(f.od_email, "E-mail을 바르게 입력해 주십시오.");

        if (typeof(f.od_hope_date) != "undefined")
        {
            clear_field(f.od_hope_date);
            if (!f.od_hope_date.value)
                error_field(f.od_hope_date, "희망배송일을 선택하여 주십시오.");
        }

        check_field(f.od_b_name, "받으시는 분 이름을 입력하십시오.");
        check_field(f.od_b_hp, "받으시는 분 핸드폰 번호를 입력하십시오.");
        //check_field(f.od_b_tel, "받으시는 분 전화번호를 입력하십시오.");
        check_field(f.od_b_addr1, "주소검색을 이용하여 받으시는 분 주소를 입력하십시오.");
        //check_field(f.od_b_addr2, "받으시는 분의 상세주소를 입력하십시오.");
        check_field(f.od_b_zip, "");

        var od_settle_hp = document.getElementById("od_settle_hp");
        if (od_settle_hp) {
            if (od_settle_hp.checked) {
                alert('휴대폰 결제는 준비중입니다.');
                return false;
            }
        }

        var od_settle_bank = document.getElementById("od_settle_bank");
        if (od_settle_bank) {
            if (od_settle_bank.checked) {
                check_field(f.od_bank_account, "계좌번호를 선택하세요.");
                check_field(f.od_deposit_name, "입금자명을 입력하세요.");
            }
        }

        // 배송비를 받지 않거나 더 받는 경우 아래식에 + 또는 - 로 대입
        f.od_send_cost.value = parseInt(f.od_send_cost.value);

        var temp_point = 0;
        var od_price = parseInt(f.od_price.value);
        var send_cost = parseInt(f.od_send_cost.value);
        var send_cost2 = parseInt(f.od_send_cost2.value);
        var send_coupon = parseInt(f.od_send_coupon.value);
        var TotalPrice = od_price + send_cost + send_cost2 - send_coupon;

        if (errmsg)
        {
            alert(errmsg);
            errfld.focus();
            return false;
        }
        <?php if($member['mb_type'] != '1' or !$is_member) { ?>

        var settle_case = document.getElementsByName("od_settle_case");
        var settle_check = false;
        var settle_method = "";

        //사용포인트가 총 주문금액과 같은지 비교해서 포인트결제버튼 선택해주기
        if (typeof(f.od_temp_point) != "undefined") {
            if (f.od_temp_point.value){

                if( !$.isNumeric( f.od_temp_point.value ) ){
                    alert("사용 포인트를 숫자형식으로 입력해주세요");
                    return false;
                }

                temp_point = parseInt(f.od_temp_point.value);

                if( temp_point == TotalPrice ){
                    document.getElementById("od_settle_point").checked = true;
                    settle_method = "포인트";
                }


            }
        }

        for (i=0; i<settle_case.length; i++)
        {
            if (settle_case[i].checked)
            {
                settle_check = true;
                settle_method = settle_case[i].value;
                console.log(settle_method);
                break;
            }
        }
        if (!settle_check)
        {
            alert("결제방식을 선택하십시오.");
            return false;
        }

        <?php } else { ?>
        var settle_case = document.getElementsByName("od_settle_case");
        var settle_method = "무통장";
        <?php } ?>



        var max_point = 0;
        if (typeof(f.max_temp_point) != "undefined")
            max_point  = parseInt(f.max_temp_point.value);


        if (typeof(f.od_temp_point) != "undefined") {
            if (f.od_temp_point.value)
            {
                var point_unit = parseInt(<?php echo $default['de_settle_point_unit']; ?>);
                temp_point = parseInt(f.od_temp_point.value);

                if (temp_point < 0) {
                    alert("포인트를 0 이상 입력하세요.");
                    f.od_temp_point.select();
                    return false;
                }

                if (temp_point > TotalPrice) {
                    alert("상품 주문금액 보다 많이 포인트결제할 수 없습니다.");
                    f.od_temp_point.select();
                    return false;
                }


                if (temp_point > <?php echo (int)$member['mb_point']; ?>) {
                    alert("회원님의 포인트보다 많이 결제할 수 없습니다.");
                    f.od_temp_point.select();
                    return false;
                }

                if (temp_point > max_point) {
                    alert(max_point + "점 이상 결제할 수 없습니다.");
                    f.od_temp_point.select();
                    return false;
                }
                /*
                if (parseInt(parseInt(temp_point / point_unit) * point_unit) != temp_point) {
                    alert("포인트를 "+String(point_unit)+"점 단위로 입력하세요.");
                    f.od_temp_point.select();
                    return false;
                }
                */

                // pg 결제 금액에서 포인트 금액 차감
                if(settle_method != "무통장") {
                    f.good_mny.value = od_price + send_cost + send_cost2 - send_coupon - temp_point;
                }
            }
        }

        var tot_price = od_price + send_cost + send_cost2 - send_coupon - temp_point;

        if (document.getElementById("od_settle_iche")) {
            if (document.getElementById("od_settle_iche").checked) {
                /*
                if (tot_price < 150) {
                    alert("계좌이체는 150원 이상 결제가 가능합니다.");
                    return false;
                }
                */
                if (tot_price < 50) {
                    alert("계좌이체는 50원 이상 결제가 가능합니다.");
                    return false;
                }
            }
        }

        if (document.getElementById("od_settle_card")) {
            if (document.getElementById("od_settle_card").checked) {
                if (tot_price < 1000) {
                    alert("신용카드는 1000원 이상 결제가 가능합니다.");
                    return false;
                }
            }
        }

        if (document.getElementById("od_settle_hp")) {
            if (document.getElementById("od_settle_hp").checked) {
                if (tot_price < 350) {
                    alert("휴대폰은 350원 이상 결제가 가능합니다.");
                    return false;
                }
            }
        }

        <?php if($default['de_tax_flag_use']) { ?>
        calculate_tax();
        <?php } ?>

        <?php if($default['de_pg_service'] == 'inicis') { ?>
        if( f.action != form_action_url ){
            f.action = form_action_url;
            f.removeAttribute("target");
            f.removeAttribute("accept-charset");
        }
        <?php } ?>

        // 카카오페이 지불
        if(settle_method == "KAKAOPAY") {
            alert('준비중입니다.');
            return false;
            <?php if($default['de_tax_flag_use']) { ?>
            f.SupplyAmt.value = parseInt(f.comm_tax_mny.value) + parseInt(f.comm_free_mny.value);
            f.GoodsVat.value  = parseInt(f.comm_vat_mny.value);
            <?php } ?>
            getTxnId(f);
            return false;
        }

        var form_order_method = '';

        if( settle_method == "lpay" ){      //이니시스 L.pay 이면 ( 이니시스의 삼성페이는 모바일에서만 단독실행 가능함 )
            form_order_method = 'samsungpay';
        }

        if( jQuery(f).triggerHandler("form_sumbit_order_"+form_order_method) !== false ) {

            // pay_method 설정
            <?php if($default['de_pg_service'] == 'kcp') { ?>
            f.site_cd.value = f.def_site_cd.value;
            f.payco_direct.value = "";
            switch(settle_method)
            {
                case "계좌이체":
                    f.pay_method.value   = "010000000000";
                    break;
                case "가상계좌":
                    f.pay_method.value   = "001000000000";
                    break;
                case "휴대폰":
                    f.pay_method.value   = "000010000000";
                    break;
                case "신용카드":
                    f.pay_method.value   = "100000000000";
                    break;
                case "간편결제":
                <?php if($default['de_card_test']) { ?>
                    f.site_cd.value      = "S6729";
                <?php } ?>
                    f.pay_method.value   = "100000000000";
                    f.payco_direct.value = "Y";
                    break;
                default:
                    f.pay_method.value   = "무통장";
                    break;
            }
            <?php } else if($default['de_pg_service'] == 'lg') { ?>
            f.LGD_EASYPAY_ONLY.value = "";
            if(typeof f.LGD_CUSTOM_USABLEPAY === "undefined") {
                var input = document.createElement("input");
                input.setAttribute("type", "hidden");
                input.setAttribute("name", "LGD_CUSTOM_USABLEPAY");
                input.setAttribute("value", "");
                f.LGD_EASYPAY_ONLY.parentNode.insertBefore(input, f.LGD_EASYPAY_ONLY);
            }

            switch(settle_method)
            {
                case "계좌이체":
                    f.LGD_CUSTOM_FIRSTPAY.value = "SC0030";
                    f.LGD_CUSTOM_USABLEPAY.value = "SC0030";
                    break;
                case "가상계좌":
                    f.LGD_CUSTOM_FIRSTPAY.value = "SC0040";
                    f.LGD_CUSTOM_USABLEPAY.value = "SC0040";
                    break;
                case "휴대폰":
                    f.LGD_CUSTOM_FIRSTPAY.value = "SC0060";
                    f.LGD_CUSTOM_USABLEPAY.value = "SC0060";
                    break;
                case "신용카드":
                    f.LGD_CUSTOM_FIRSTPAY.value = "SC0010";
                    f.LGD_CUSTOM_USABLEPAY.value = "SC0010";
                    break;
                case "간편결제":
                    var elm = f.LGD_CUSTOM_USABLEPAY;
                    if(elm.parentNode)
                        elm.parentNode.removeChild(elm);
                    f.LGD_EASYPAY_ONLY.value = "PAYNOW";
                    break;
                default:
                    f.LGD_CUSTOM_FIRSTPAY.value = "무통장";
                    break;
            }
            <?php }  else if($default['de_pg_service'] == 'inicis') { ?>
            switch(settle_method)
            {
                case "계좌이체":
                    f.gopaymethod.value = "DirectBank";
                    break;
                case "가상계좌":
                    f.gopaymethod.value = "VBank";
                    break;
                case "휴대폰":
                    f.gopaymethod.value = "HPP";
                    break;
                case "신용카드":
                    f.gopaymethod.value = "Card";
                    f.acceptmethod.value = f.acceptmethod.value.replace(":useescrow", "");
                    break;
                case "간편결제":
                    f.gopaymethod.value = "Kpay";
                    break;
                case "lpay":
                    f.gopaymethod.value = "onlylpay";
                    f.acceptmethod.value = f.acceptmethod.value+":cardonly";
                    break;
                case "포인트":
                    f.gopaymethod.value = "포인트";
                    break;
                default:
                    f.gopaymethod.value = "무통장";
                    break;
            }
            <?php } ?>

            // 결제정보설정
            <?php if($default['de_pg_service'] == 'kcp') { ?>
            f.buyr_name.value = f.od_name.value;
            f.buyr_mail.value = f.od_email.value;
            f.buyr_tel1.value = f.od_hp.value;
            f.buyr_tel2.value = f.od_hp.value;
            f.rcvr_name.value = f.od_b_name.value;
            f.rcvr_tel1.value = f.od_b_hp.value;
            f.rcvr_tel2.value = f.od_b_hp.value;
            f.rcvr_mail.value = f.od_email.value;
            f.rcvr_zipx.value = f.od_b_zip.value;
            f.rcvr_add1.value = f.od_b_addr1.value;
            f.rcvr_add2.value = f.od_b_addr2.value;

            if(f.pay_method.value != "무통장") {
                jsf__pay( f );
            } else {
                f.submit();
            }
            <?php } ?>
            <?php if($default['de_pg_service'] == 'lg') { ?>
            f.LGD_BUYER.value = f.od_name.value;
            f.LGD_BUYEREMAIL.value = f.od_email.value;
            f.LGD_BUYERPHONE.value = f.od_hp.value;
            f.LGD_AMOUNT.value = f.good_mny.value;
            f.LGD_RECEIVER.value = f.od_b_name.value;
            f.LGD_RECEIVERPHONE.value = f.od_b_hp.value;
            <?php if($default['de_escrow_use']) { ?>
            f.LGD_ESCROW_ZIPCODE.value = f.od_b_zip.value;
            f.LGD_ESCROW_ADDRESS1.value = f.od_b_addr1.value;
            f.LGD_ESCROW_ADDRESS2.value = f.od_b_addr2.value;
            f.LGD_ESCROW_BUYERPHONE.value = f.od_hp.value;
            <?php } ?>
            <?php if($default['de_tax_flag_use']) { ?>
            f.LGD_TAXFREEAMOUNT.value = f.comm_free_mny.value;
            <?php } ?>

            if(f.LGD_CUSTOM_FIRSTPAY.value != "무통장") {
                launchCrossPlatform(f);
            } else {
                f.submit();
            }
            <?php } ?>
            <?php if($default['de_pg_service'] == 'inicis') { ?>
            <?//php if($period != 'y') {?>
            f.price.value       = f.good_mny.value;
            <?//php } ?>
            <?php if($default['de_tax_flag_use']) { ?>
            f.tax.value         = f.comm_vat_mny.value;
            f.taxfree.value     = f.comm_free_mny.value;
            <?php } ?>
            f.buyername.value   = f.od_name.value;
            f.buyeremail.value  = f.od_email.value;
            f.buyertel.value    = f.od_hp.value ? f.od_hp.value : f.od_hp.value;
            f.recvname.value    = f.od_b_name.value;
            f.recvtel.value     = f.od_b_hp.value ? f.od_b_hp.value : f.od_b_hp.value;
            f.recvpostnum.value = f.od_b_zip.value;
            f.recvaddr.value    = f.od_b_addr1.value + " " +f.od_b_addr2.value;

            if(f.gopaymethod.value != "무통장" && f.gopaymethod.value != "포인트") {
                // 주문정보 임시저장
                var order_data = $(f).serialize();
                var save_result = "";
                $.ajax({
                    type: "POST",
                    data: order_data,
                    url: g5_url+"/shop/ajax.orderdatasave.php",
                    cache: false,
                    async: false,
                    success: function(data) {
                        save_result = data;
                    }
                });

                if(save_result) {
                    alert(save_result);
                    return false;
                }
                <?php if($period == 'y') {?>
                if(!make_signature_bill(f)){
                    return false;
                }
                <?php }else{ ?>
                if(!make_signature(f)){
                    return false;
                }
                <?php } ?>

                //결제수단선택 select box 가 있다면 실행
                if( $("#select_bill_info").length > 0 ){

                    if( $("#select_bill_info").val() == "" ){
                        alert("저장된 결제수단을 선택해주세요.");
                        return false;
                    }
                    if( $("#select_bill_info").val() == "1" ){
                        f.submit();
                    }else{
                        paybtn(f);
                    }

                }else{
                    paybtn(f);
                }

            } else {
                f.submit();
            }
            <?php } ?>
        }

    }

    // 구매자 정보와 동일합니다.
    function gumae2baesong() {
        var f = document.forderform;

        f.od_b_name.value = f.od_name.value;
        //f.od_b_tel.value  = f.od_tel.value;
        f.od_b_hp.value   = f.od_hp.value;
        f.od_b_zip.value  = f.od_zip.value;
        f.od_b_addr1.value = f.od_addr1.value;
        f.od_b_addr2.value = f.od_addr2.value;
        f.od_b_addr3.value = f.od_addr3.value;
        f.od_b_addr_jibeon.value = f.od_addr_jibeon.value;

        calculate_sendcost(String(f.od_b_zip.value));
    }

    <?php if ($default['de_hope_date_use']) { ?>
    $(function(){
        $("#od_hope_date").datepicker({ changeMonth: true, changeYear: true, dateFormat: "yy-mm-dd", showButtonPanel: true, yearRange: "c-99:c+99", minDate: "+<?php echo (int)$default['de_hope_date_after']; ?>d;", maxDate: "+<?php echo (int)$default['de_hope_date_after'] + 6; ?>d;" });
    });
    <?php } ?>
</script>


<script>

    $(".b-close_CONTROL").click(function(){
        $("#PopupLayer_CONTROL").hide();
    });

    //포인트 입력값 비교해서 체크변동시켜주기
    $('#od_temp_point').bind("change keyup input", function(event) {

        var f = document.forderform;
        var od_price = parseInt(f.od_price.value);
        var send_cost = parseInt(f.od_send_cost.value);
        var send_cost2 = parseInt(f.od_send_cost2.value);
        var send_coupon = parseInt(f.od_send_coupon.value);
        var TotalPrice = od_price + send_cost + send_cost2 - send_coupon;

        temp_point = 0;

        //사용포인트가 총 주문금액과 같은지 비교해서 포인트결제버튼 선택해주기
        if (typeof(f.od_temp_point) != "undefined") {
            if (f.od_temp_point.value){

                if( !$.isNumeric( f.od_temp_point.value ) ){
                    alert("사용 포인트를 숫자형식으로 입력해주세요");
                    return false;
                }

                temp_point = parseInt(f.od_temp_point.value);

                if( temp_point == TotalPrice ){
                    document.getElementById("od_settle_point").checked = true;
                    settle_method = "포인트";
                }else{

                    if( document.getElementById("od_settle_point").checked == true ){
                        document.getElementById("od_settle_point").checked = false;
                    }

                }


            }
        }

    });

</script>

<script>

$('.form-dropdown').on('click', function () {
    if($('.dropdown-menu').css('display') ==  'none') {
        $('.dropdown-menu').css('display', 'block');
    } else {
        $('.dropdown-menu').css('display', 'none');
    }
});

$('select').each(function(){
    var $this = $(this), numberOfOptions = $(this).children('option:not([disabled])').length;
 console.log('numberOfOptions',numberOfOptions);
    $this.addClass('select-hidden'); 
    $this.wrap('<div class="form-select"></div>');
    $this.after('<div class="select-styled"></div>');

    var $styledSelect = $this.next('div.select-styled');
    $styledSelect.text($this.children('option').eq(0).text());

    var $list = $('<ul />', {
        'class': 'select-options'
    }).insertAfter($styledSelect);

    for (var i = 0; i < numberOfOptions; i++) {
        $('<li />', {
            text: $this.children('option:not([disabled])').eq(i).text(),
            rel: $this.children('option:not([disabled])').eq(i).val()
        }).appendTo($list);
    }

    var $listItems = $list.children('li');

    $styledSelect.click(function(e) {
        e.stopPropagation();
        $('div.select-styled.active').not(this).each(function(){
            $(this).removeClass('active').next('ul.select-options').hide();
        });
        $(this).toggleClass('active').next('ul.select-options').toggle();
    });

    $listItems.click(function(e) {
        e.stopPropagation();
        $styledSelect.text($(this).text()).removeClass('active');
        $this.val($(this).attr('rel'));
        $list.hide();
        //console.log($this.val());
    });

    $(document).click(function() {
        $styledSelect.removeClass('active');
        $list.hide();
    });

});
</script>
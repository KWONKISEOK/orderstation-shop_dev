<?php
include_once('./_common.php');

if (G5_IS_MOBILE) {

    return;
}

function get_mshop_category($ca_id, $len, $mb_type='')
{
    global $g5;

	if($mb_type=='1') {

		$sql = " select ca_id, ca_name from {$g5['g5_shop_category_table']}
					where ca_use = '1' and ca_web_view=1";
		if($ca_id)
			$sql .= " and ca_id like '$ca_id%' ";
		$sql .= " and length(ca_id) = '$len' order by ca_order, ca_id ";
	} else {
		$sql = " select ca_id, ca_name from {$g5['g5_shop_category_table']}
					where ca_use = '1' and ca_app_view=1";
		if($ca_id)
			$sql .= " and ca_id like '$ca_id%' ";
		$sql .= " and length(ca_id) = '$len' order by ca_order, ca_id ";
	}
	/*
	$sql = " select ca_id, ca_name from {$g5['g5_shop_category_table']}
				where ca_use = '1' ";
	if($ca_id)
		$sql .= " and ca_id like '$ca_id%' ";
	$sql .= " and length(ca_id) = '$len' order by ca_order, ca_id ";

	*/
    return $sql;
}

$mshop_ca_href = G5_SHOP_URL.'/list.php?ca_id=';
$mshop_ca_res1 = sql_query(get_mshop_category('', 2, $member['mb_type']));
for($i=0; $mshop_ca_row1=sql_fetch_array($mshop_ca_res1); $i++) {
	if($i == 0)
		echo '<ul id="main-category" class="xans-element- xans-layout xans-layout-category cate">'.PHP_EOL;
?>
	<li  class="xans-record-">
		<a href="<?php echo $mshop_ca_href.$mshop_ca_row1['ca_id']; ?>" ><?php echo get_text($mshop_ca_row1['ca_name']); ?></a>
		<?php
		$mshop_ca_res2 = sql_query(get_mshop_category($mshop_ca_row1['ca_id'], 4));

		for($j=0; $mshop_ca_row2=sql_fetch_array($mshop_ca_res2); $j++) {
			if($j == 0)
				echo '<div class="sub-category ms"><div class="sub-left"><ul class="sub02 ">'.PHP_EOL;
		?>
			<li class="<?php if($mshop_ca_row1['ca_id'] != '60') echo 'arrow';?>">
				<a href="<?php echo $mshop_ca_href.$mshop_ca_row2['ca_id']; ?>"><?php echo get_text($mshop_ca_row2['ca_name']); ?></a>
					<?php
						$mshop_ca_res3 = sql_query(get_mshop_category($mshop_ca_row2['ca_id'], 6));

						for($k=0; $mshop_ca_row3=sql_fetch_array($mshop_ca_res3); $k++) {
							if($k == 0)
								echo '<ul class="sub03 ">'.PHP_EOL;
						?>
							<li><a href="<?php echo $mshop_ca_href.$mshop_ca_row3['ca_id']; ?>"><?php echo mb_strimwidth($mshop_ca_row3['ca_name'], 0, 30,"...","utf-8") ; ?></a></li>

						<?php
						}
						if($k > 0)
							echo '</ul>'.PHP_EOL;
						?>

			</li>
		<?php
		}

		if($j > 0)
			echo '</ul></div></div>'.PHP_EOL;
		?>
	</li>
<?php
}

if($i > 0)
	echo '</ul>'.PHP_EOL;
else
	echo '<p class="no-cate">등록된 분류가 없습니다.</p>'.PHP_EOL;
?>


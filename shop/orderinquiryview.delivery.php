<?php
include_once ('./_common.php');
// 불법접속을 할 수 없도록 세션에 아무값이나 저장하여 hidden 으로 넘겨서 다음 페이지에서 비교함
$token = md5(uniqid(rand(), true));
set_session("ss_token", $token);

if (!$is_member) {
    if (!get_session('ss_orderview_uid'))
        alert("직접 링크로는 주문서 조회가 불가합니다.\\n\\n주문조회 화면을 통하여 조회하시기 바랍니다.", G5_SHOP_URL);
}


$sql = "select * from {$g5['g5_shop_order_table']} where od_id = '$od_id' {$service_type_and_query} ";
if ($is_member && !$is_admin)
    $sql .= " and mb_id = '{$member['mb_id']}' ";
$od = sql_fetch($sql);
if (!$od['od_id'] || (!$is_member && md5($od['od_id'].$od['od_time'].$od['od_ip']) != get_session('ss_orderview_uid'))) {
    alert("조회하실 주문서가 없습니다.", G5_SHOP_URL);
}

$sql = "select sum(od_total_drug_price) as od_total_drug_price from {$g5['g5_shop_order_detail_table']} where od_id = '$od_id' {$service_type_and_query} ";
$od_dt = sql_fetch($sql);

// 결제방법
$settle_case = $od['od_settle_case'];

if (G5_IS_MOBILE) {
    include_once(G5_MSHOP_PATH.'/orderinquiryview.delivery.php');
    return;
}
?>

<style>
    h3 {
        margin: 5px 0 5px 0;
    }
    table.type09 {
        border-collapse: collapse;
        text-align: left;
        line-height: 1.5;

    }
    table.type09 thead th {
        padding: 10px;
        font-weight: bold;
        vertical-align: top;
        color: #369;
        border-bottom: 3px solid #036;
    }
    table.type09 tbody th {
        width: 150px;
        padding: 10px;
        font-weight: bold;
        vertical-align: top;
        border-bottom: 1px solid #ccc;
        background: #f3f6f7;
    }
    table.type09 td {
        width: 350px;
        padding: 10px;
        vertical-align: top;
        border-bottom: 1px solid #ccc;
    }
    .empty_table {
        padding: 25px 10px 25px 10px !important;
        background: #f3f6f7;
        width: 100% !important;
    }

    #sod_fin_dvr .win_btn {clear:both;margin:10px;text-align:center}
    #sod_fin_dvr .win_btn a {display:inline-block;height:40px;line-height:40px;font-weight:bold}
    #sod_fin_dvr .win_btn .btn_close {display:inline-block;padding:0 10px;border:0;background:#c6c6d2;color:#666;text-decoration:none;font-weight:bold;height:40px;border-radius:3px;margin:10px 0}
    #sod_fin_dvr .win_btn .btn_close_new {display:inline-block;padding:0 10px;background: #fff;border:1px solid #c6c6d2;color:#666;text-decoration:none;font-weight:bold;height:40px;border-radius:3px;margin:10px 0}
    #sod_fin_dvr .win_btn .btn_submit{width:100%;height:40px;line-height:40px;border-radius:3px;margin:0 0 5px;display:inline-block;font-weight:bold}
    #sod_fin_dvr .win_btn .btn_add{width:100%;padding:0 20px;height:40px;border-radius:3px;font-weight:bold;font-size:1.083em;color: #ff8800;background: #fff; border: 1px solid #ff8800;}
    #sod_fin_dvr .fixed {position: fixed;left: 0;bottom: 0;width: 100%;margin: 0;padding: 5px;background: #e6e6e6;}
    #sod_fin_dvr .btn_close_top {position: absolute;top: 0;right: 0;height: 50px;width: 50px;background: url(../img/close_btn.png) no-repeat 50% 50%;border: 0;text-indent: -999px;overflow: hidden;}
</style>

<?php if ($od['od_multi_yn'] != "Y") { ?>
    <section id="sod_fin_dvr">
        <h3>배송정보</h3>

        <div class="tbl_head01 tbl_wrap">
            <table class="type09">

                <tbody>
                <?php
                $sql = " select distinct a.comp_code, b.od_invoice, b.od_delivery_company,b.od_date3, a.od_send_yn , b.od_status
                                        from tbl_shop_order_detail a, tbl_shop_order_receiver b
                                        where a.od_id = b.od_id and a.od_num= b.od_num and a.od_id = '$od_id' {$service_type_and_query_a} ";

                $res = sql_query($sql);

                for($k=0; $oddeli=sql_fetch_array($res); $k++) {

                    if ($oddeli['od_invoice'] && $oddeli['od_delivery_company'])
                    {
                        ?>
                        <tr>
                            <th scope="row">배송회사</th>
                            <td><?php echo $oddeli['od_delivery_company'];
                                if($oddeli['od_delivery_company'] != "한진택배" ){
                                    echo get_delivery_inquiry($oddeli['od_delivery_company'], $oddeli['od_invoice'], 'dvr_link');
                                }else{
                                    echo get_delivery_inquiry_han($oddeli['od_delivery_company'], $oddeli['od_invoice'], 'dvr_link');
                                } ?>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">운송장번호</th>
                            <td><?php echo $oddeli['od_invoice']; ?></td>
                        </tr>
                        <tr>
                            <th scope="row">배송일시</th>
                            <td><?php echo $oddeli['od_date3']; ?></td>
                        </tr>
                        <?php
                    }
                    else
                    {
                        ?>
                        <tr>
                            <td class="empty_table" colspan="2">아직 배송하지 않았거나 배송정보를 입력하지 못하였습니다.</td>
                        </tr>
                        <?php
                    }
                }
                ?>
                </tbody>
            </table>
        </div>
    </section>
<?php } else if ($od['od_multi_yn'] == "Y") { ?>
    <section id="sod_fin_dvr">
        <h3>배송정보</h3>

        <div class="tbl_head01 tbl_wrap">
            <table class="type09">

                <tbody>
                <?php
                $sql = " select a.comp_code, b.od_date3, a.od_send_yn , b.od_status , c.*
                            from tbl_shop_order_detail a, tbl_shop_order_receiver b, tbl_shop_order_multi_receiver c
                            where a.od_id = b.od_id and a.od_num= b.od_num and a.od_id = c.od_id and a.od_num= c.od_num and a.od_id = '$od_id' ";

                $res = sql_query($sql);

                for($k=0; $oddeli=sql_fetch_array($res); $k++) {

                    if ($oddeli['od_invoice'] && $oddeli['od_delivery_company'])
                    {
                        ?>
                        <tr>
                            <th scope="row">받으시는분</th>
                            <td>
                                <?=$oddeli["od_b_name"]?> / <?=$oddeli["od_b_hp"]?>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">주소</th>
                            <td>
                                <?=$oddeli["od_b_addr1"]?> <?=$oddeli["od_b_addr2"]?> <?=$oddeli["od_b_addr3"]?>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">배송회사</th>
                            <td><?php echo $oddeli['od_delivery_company'];
                                if($oddeli['od_delivery_company'] != "한진택배" ){
                                    echo get_delivery_inquiry($oddeli['od_delivery_company'], $oddeli['od_invoice'], 'dvr_link');
                                }else{
                                     echo get_delivery_inquiry_han($oddeli['od_delivery_company'], $oddeli['od_invoice'], 'dvr_link');
                                } ?>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">운송장번호</th>
                            <td><?php echo $oddeli['od_invoice']; ?></td>
                        </tr>
                        <tr>
                            <th scope="row">배송일시</th>
                            <td><?php echo $oddeli['od_date3']; ?></td>
                        </tr>
                        <tr>
                            <td colspan="2"></td>
                        </tr>
                        <?php
                    }
                    else
                    {
                        ?>
                        <tr>
                            <td class="empty_table" colspan="2">아직 배송하지 않았거나 배송정보를 입력하지 못하였습니다.</td>
                        </tr>
                        <?php
                    }
                }
                ?>
                </tbody>
            </table>
        </div>
    </section>
<?php } ?>

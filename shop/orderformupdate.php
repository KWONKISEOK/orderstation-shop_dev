<?php
include_once('./_common.php');
include_once(G5_LIB_PATH . '/mailer.lib.php');

//이니시스 lpay 요청으로 왔다면 $default['de_pg_service'] 값을 이니시스로 변경합니다.
if ($od_settle_case == 'lpay') {
    $default['de_pg_service'] = 'inicis';
}

if (($od_settle_case != '무통장' && $od_settle_case != 'KAKAOPAY') && $default['de_pg_service'] == 'lg' && !$_POST['LGD_PAYKEY'])
    alert('결제등록 요청 후 주문해 주십시오.');

// 장바구니가 비어있는가?
if (get_session("ss_direct"))
    $tmp_cart_id = get_session('ss_cart_direct');
else
    $tmp_cart_id = get_session('ss_cart_id');

if (get_cart_count($tmp_cart_id) == 0)// 장바구니에 담기
    alert('장바구니가 비어 있습니다.\\n\\n이미 주문하셨거나 장바구니에 담긴 상품이 없는 경우입니다.', G5_SHOP_URL . '/cart.php');

$error = "";
// 장바구니 상품 재고 검사
$sql = " select it_id,
                ct_qty,
                it_name,
                io_id,
                io_type,
                ct_option
           from {$g5['g5_shop_cart_table']}
          where od_id = '$tmp_cart_id'
            and ct_select = '1' ";
$result = sql_query($sql);
for ($i = 0; $row = sql_fetch_array($result); $i++) {
    // 상품에 대한 현재고수량
    if ($row['io_id']) { // 옵션 코드
        $it_stock_qty = (int)get_option_stock_qty($row['it_id'], $row['io_id'], $row['io_type']);
    } else {
        $it_stock_qty = (int)get_it_stock_qty($row['it_id']);
    }
    // 장바구니 수량이 재고수량보다 많다면 오류
    if ($row['ct_qty'] > $it_stock_qty)
        $error .= "{$row['ct_option']} 의 재고수량이 부족합니다. 현재고수량 : $it_stock_qty 개\\n\\n";
}

if ($i == 0)
    alert('장바구니가 비어 있습니다.\\n\\n이미 주문하셨거나 장바구니에 담긴 상품이 없는 경우입니다.', G5_SHOP_URL . '/cart.php');

if ($error != "") {
    $error .= "다른 고객님께서 {$od_name}님 보다 먼저 주문하신 경우입니다. 불편을 끼쳐 죄송합니다.";
    alert($error);
}

$i_price = (int)$_POST['od_price'];
$i_send_cost = (int)$_POST['od_send_cost'];
$i_send_cost2 = (int)$_POST['od_send_cost2'];
$i_send_coupon = (int)$_POST['od_send_coupon'];
$i_temp_point = (int)$_POST['od_temp_point'];
$i_company_sendcost = (int)$_POST['company_sendcost'];

if($_POST['ad_sel_addr'] == 'Y'){
    $multiYN = 'Y';
}else if($_POST['ad_sel_addr'] == 'C'){
    $companyYN = 'Y';
}

if($companyYN == 'Y'){
    $i_price -= $i_company_sendcost;
}

$mb_referee = $member['mb_referee'];
$mb_extra = $member['mb_extra'];

$point_type = $_POST['point_type'];

if($point_type != 'c'){
    $point_type = 'p';
}

// 주문금액이 상이함
//정기주문
//할인율 들어간 상품
/* 
할인율은 테이블의 구조적으로 맞지 않다 .
이유1. 고객이 주문할 당시 장바구니 테이블에 할인율 컬럼이 없기때문에 관리자가 확정처리하게 되면 주문당시의 금액과 바뀌게 된다.
이유2. 1번문제를 수정해서 관리자의 확정처리시 tbl_shop_item_option 컬럼과 조인하게 수정을한다고 해도
      타임세일같이 옵션의 가격이 변동되는 경우 또 가격이 변동될수 있다.
최종해결방안 : 장바구니테이블에 할인율 컬럼을 만들어서 주문할당시에 할인율을 넣고 , 관리자에서 확정처리할때도 추가한 컬럼으로 계산이 되게 해야한다.
*/

/*
$sql = " select SUM((((ct_price + b.io_price) * (b.io_period_rate1*0.01) * b.io_period_cnt1)) * ct_qty) as od_price,
              COUNT(distinct a.it_id) as cart_count
            from tbl_shop_cart a, tbl_shop_item_option b 
				where a.io_id = b.io_id and od_id = '$tmp_cart_id' and ct_select = '1' and ct_period_cnt > 0";
$row = sql_fetch($sql);

$tot_ct_price1 = $row['od_price'];
$cart_count1 = $row['cart_count'];
*/

$tot_ct_price1 = 0;
$cart_count1 = 0;

//정기주문
//할인율 안들어간 상품
$sql = " select SUM(IF(io_type = 1, (io_price * ct_qty*ct_period_cnt), ((ct_price + io_price) * ct_qty)*ct_period_cnt)) as od_price,
              COUNT(distinct it_id) as cart_count
            from {$g5['g5_shop_cart_table']} where od_id = '$tmp_cart_id' and ct_select = '1' and ct_period_cnt > 0 AND ct_period_yn = 'Y' ";
$row = sql_fetch($sql);

$tot_ct_price2 = $row['od_price'];
$cart_count2 = $row['cart_count'];

//일반주문
$sql = " select SUM(IF(io_type = 1, (io_price * ct_qty), ((ct_price + io_price) * ct_qty))) as od_price,
              COUNT(distinct it_id) as cart_count
            from {$g5['g5_shop_cart_table']} where od_id = '$tmp_cart_id' and ct_select = '1' and ct_period_cnt = 0 AND ct_period_yn != 'Y' ";
$row = sql_fetch($sql);

$tot_ct_price3 = $row['od_price'];
$cart_count3 = $row['cart_count'];


$cart_count = $cart_count1 + $cart_count2 + $cart_count3;
$tot_ct_price = $tot_ct_price1 + $tot_ct_price2 + $tot_ct_price3;

$tot_od_price = $tot_ct_price;


// 쿠폰금액계산
$tot_cp_price = 0;
if ($is_member) {
    // 상품쿠폰
    $tot_it_cp_price = $tot_od_cp_price = 0;
    $it_cp_cnt = count($_POST['cp_id']);
    $arr_it_cp_prc = array();
    for ($i = 0; $i < $it_cp_cnt; $i++) {
        $cid = $_POST['cp_id'][$i];
        $it_id = $_POST['it_id'][$i];


        $sql = " select cp_id, cp_method, cp_target, cp_type, cp_price, cp_trunc, cp_minimum, cp_maximum
                    from {$g5['g5_shop_coupon_table']}
                    where cp_id = '$cid'
                      and mb_id IN ( '{$member['mb_id']}', '소비자회원', '전체회원' )
                      and cp_start <= '" . G5_TIME_YMD . "'
                      and cp_end >= '" . G5_TIME_YMD . "'
                      and cp_method IN ( 0, 1 ) and service_type is null  ";

        $cp = sql_fetch($sql);
        if (!$cp['cp_id'])
            continue;

        // 사용한 쿠폰인지
        if (is_used_coupon($member['mb_id'], $cp['cp_id']))
            continue;

        // 분류할인인지
        if ($cp['cp_method']) {
            $sql2 = " select it_id, ca_id, ca_id2, ca_id3
                        from {$g5['g5_shop_item_table']}
                        where it_id = '$it_id' ";
            $row2 = sql_fetch($sql2);

            if (!$row2['it_id'])
                continue;

            /*
            if($row2['ca_id'] != $cp['cp_target'] && $row2['ca_id2'] != $cp['cp_target'] && $row2['ca_id3'] != $cp['cp_target'])
                continue;
            */
            if (strpos($cp['cp_target'], "|" . $row2['ca_id'] . "|") === false && strpos($cp['cp_target'], "|" . $row2['ca_id2'] . "|") === false && strpos($cp['cp_target'], "|" . $row2['ca_id3'] . "|") === false) {
                continue;
            }
        } else {

            /*
            if($cp['cp_target'] != $it_id)
                continue;
            */

            if (strpos($cp['cp_target'], "|" . $it_id . "|") === false) {
                continue;
            }

        }

        // 상품금액 달콩 이벤트 약국주문가로 인해 it_incen , io_incen 추가  a 추가
        $sql = " select SUM( IF(io_type = '1', io_price * ct_qty, (ct_price + io_price) * ct_qty)) as sum_price , 
						SUM((select it_incen * a.ct_qty from tbl_shop_item where it_id = a.it_id))  as it_incen, 
						SUM((select io_incen * a.ct_qty from tbl_shop_item_option where io_id = a.io_id limit 1) ) as io_incen
                    from {$g5['g5_shop_cart_table']} a
                    where od_id = '$tmp_cart_id'
                      and it_id = '$it_id'
                      and ct_select = '1' ";
        $ct = sql_fetch($sql);
        $item_price = $ct['sum_price'];
        $incen_sum = $ct['it_incen'] + $ct['io_incen'];

        if ($cp['cp_minimum'] > $item_price)
            continue;

        $dc = 0;


        if ($cp['cp_type']) {
            $dc = floor(($item_price * ($cp['cp_price'] / 100)) / $cp['cp_trunc']) * $cp['cp_trunc'];
        } else {
            $dc = $cp['cp_price'];
        }

        if ($cp['cp_maximum'] && $dc > $cp['cp_maximum'])
            $dc = $cp['cp_maximum'];

        if ($item_price < $dc)
            continue;

        $tot_it_cp_price += $dc;
        $arr_it_cp_prc[$it_id] = $dc;
    }

    $tot_od_price -= $tot_it_cp_price;

    // 주문쿠폰
    $tot_cpprice = 0;
    if ($_POST['od_cp_id']) {

        $str_cp_id = explode(':', $_POST['od_cp_id']);
        $cp_cnt = count($str_cp_id);
        for ($j = 0; $j < $cp_cnt; $j++) {
            //echo($str_cp_id[$i] . "<br/>");
            $sql = " select cp_id, cp_type, cp_price, cp_trunc, cp_minimum, cp_maximum
						from {$g5['g5_shop_coupon_table']}
						where cp_id = '{$str_cp_id[$j]}'
						  and mb_id IN ( '{$member['mb_id']}', '소비자회원', '전체회원' )
						  and cp_start <= '" . G5_TIME_YMD . "'
						  and cp_end >= '" . G5_TIME_YMD . "'
						  and cp_method != '3' and service_type is null ";
            $cp = sql_fetch($sql);

            // 사용한 쿠폰인지
            $cp_used = is_used_coupon($member['mb_id'], $cp['cp_id']);

            $dc = 0;
            if (!$cp_used && $cp['cp_id'] && ($cp['cp_minimum'] <= $tot_od_price)) {
                if ($cp['cp_type']) {
                    $dc = floor(($tot_od_price * ($cp['cp_price'] / 100)) / $cp['cp_trunc']) * $cp['cp_trunc'];
                } else {
                    $dc = $cp['cp_price'];
                }

                if ($cp['cp_maximum'] && $dc > $cp['cp_maximum'])
                    $dc = $cp['cp_maximum'];

                if ($tot_od_price < $dc)
                    die('Order coupon error.');

                $tot_od_cp_price = $dc;
                $tot_od_price -= $tot_od_cp_price;
                $tot_cpprice += $tot_od_cp_price;
            }
        }
    }
    $tot_cp_price = $tot_it_cp_price + $tot_cpprice;
}

//if ((int)($row['od_price'] - $tot_cp_price) !== $i_price) {
//    die("Error.");
//}

// 배송비가 상이함
$send_cost = get_sendcost($tmp_cart_id);

$tot_sc_cp_price = 0;
if ($is_member && $send_cost > 0) {
    // 배송쿠폰
    if ($_POST['sc_cp_id']) {
        $sql = " select cp_id, cp_type, cp_price, cp_trunc, cp_minimum, cp_maximum
                    from {$g5['g5_shop_coupon_table']}
                    where cp_id = '{$_POST['sc_cp_id']}'
                      and mb_id IN ( '{$member['mb_id']}', '소비자회원', '전체회원' )
                      and cp_start <= '" . G5_TIME_YMD . "'
                      and cp_end >= '" . G5_TIME_YMD . "'
                      and cp_method = '3' and service_type is null ";
        $cp = sql_fetch($sql);

        // 사용한 쿠폰인지
        $cp_used = is_used_coupon($member['mb_id'], $cp['cp_id']);

        $dc = 0;
        if (!$cp_used && $cp['cp_id'] && ($cp['cp_minimum'] <= $tot_od_price)) {
            if ($cp['cp_type']) {
                $dc = floor(($send_cost * ($cp['cp_price'] / 100)) / $cp['cp_trunc']) * $cp['cp_trunc'];
            } else {
                $dc = $cp['cp_price'];
            }

            if ($cp['cp_maximum'] && $dc > $cp['cp_maximum'])
                $dc = $cp['cp_maximum'];

            if ($dc > $send_cost)
                $dc = $send_cost;

            $tot_sc_cp_price = $dc;
        }
    }
}

//if ((int)($send_cost - $tot_sc_cp_price) !== (int)($i_send_cost - $i_send_coupon)) {
//    die("Error..");
//}

// 추가배송비가 상이함
$od_b_zip = preg_replace('/[^0-9]/', '', $od_b_zip);
$od_b_zip1 = substr($od_b_zip, 0, 3);
$od_b_zip2 = substr($od_b_zip, 3);
$zipcode = $od_b_zip;
$sql = " select sc_id, sc_price from {$g5['g5_shop_sendcost_table']} where sc_zip1 <= '$zipcode' and sc_zip2 >= '$zipcode' ";
$tmp = sql_fetch($sql);
if (!$tmp['sc_id'])
    $send_cost2 = 0;
else
    $send_cost2 = (int)$tmp['sc_price'];
if ($send_cost2 !== $i_send_cost2)
    die("Error...");

// 결제포인트가 상이함
// 회원이면서 포인트사용이면
$temp_point = 0;
if ($is_member && $config['cf_use_point'] && $i_temp_point > 0) {

    //총 주문금액
    $calc_price = $i_price + $i_send_cost + $i_send_cost2 - $i_send_coupon;
    if ($i_temp_point > $calc_price) {

        echo "
		<script>
			alert('총주문금액보다 사용포인트가 큽니다.다시확인해주십시요.');
			history.back();
		</script>
		";
        exit;

    }
    /*
    if($member['mb_point'] >= $default['de_settle_min_point']) {
        $temp_point = (int)$default['de_settle_max_point'];

        if($temp_point > (int)$tot_od_price)
            $temp_point = (int)$tot_od_price;

        if($temp_point > (int)$member['mb_point'])
            $temp_point = (int)$member['mb_point'];

        $point_unit = (int)$default['de_settle_point_unit'];
        $temp_point = (int)((int)($temp_point / $point_unit) * $point_unit);
    }
    */

}
/*
if (($i_temp_point > (int)$temp_point || $i_temp_point < 0) && $config['cf_use_point'])
    die("Error....");
*/


if($mb_referee == 4 || $mb_extra == 11){
    if ($od_temp_point) {
        if ($member['mb_cash'] < $od_temp_point)
            alert('회원님의 적립금이 부족하여 적립금으로 결제 할 수 없습니다.');
    }
}else{
    if ($od_temp_point) {
        if ($member['mb_point'] < $od_temp_point)
            alert('회원님의 포인트가 부족하여 포인트로 결제 할 수 없습니다.');
    }
}

$i_price = $i_price + $i_send_cost + $i_send_cost2 - $i_temp_point - $i_send_coupon;
//$tot_sc_cp_price->배송비 쿠폰 , $tot_cp_price -> 상품쿠폰 + 주문쿠폰
$order_price = $tot_od_price + $send_cost + $send_cost2 - $tot_sc_cp_price - $od_temp_point;

$od_status = '주문';
$od_tno = '';
if ($od_settle_case == "무통장" || $od_settle_case == "포인트") {
    $od_receipt_point = $i_temp_point;
    $od_receipt_price = 0;
    $od_misu = $i_price - $od_receipt_price;
    if ($od_misu == 0) {
        $od_status = '입금';
        $od_receipt_time = G5_TIME_YMDHIS;
    }
    $od_bank_date = holiday_chk(G5_TIME_YMDHIS);
} else if ($od_settle_case == "계좌이체") {
    switch ($default['de_pg_service']) {
        case 'lg':
            include G5_SHOP_PATH . '/lg/xpay_result.php';
            break;
        case 'inicis':
            include G5_SHOP_PATH . '/inicis/inistdpay_result.php';
            break;
        default:
            include G5_SHOP_PATH . '/kcp/pp_ax_hub.php';
            $bank_name = iconv("cp949", "utf-8", $bank_name);
            break;
    }

    $od_tno = $tno;
    $od_receipt_price = $amount;
    $od_receipt_point = $i_temp_point;
    $od_receipt_time = preg_replace("/([0-9]{4})([0-9]{2})([0-9]{2})([0-9]{2})([0-9]{2})([0-9]{2})/", "\\1-\\2-\\3 \\4:\\5:\\6", $app_time);
    $od_bank_account = $od_settle_case;
    $od_deposit_name = $od_name;
    $od_bank_account = $bank_name;
    $pg_price = $amount;
    $od_misu = $i_price - $od_receipt_price;
    if ($od_misu == 0)
        $od_status = '입금';
} else if ($od_settle_case == "가상계좌") {
    switch ($default['de_pg_service']) {
        case 'lg':
            include G5_SHOP_PATH . '/lg/xpay_result.php';
            break;
        case 'inicis':
            include G5_SHOP_PATH . '/inicis/inistdpay_result.php';
            $od_app_no = $app_no;
            break;
        default:
            include G5_SHOP_PATH . '/kcp/pp_ax_hub.php';
            $bankname = iconv("cp949", "utf-8", $bankname);
            $depositor = iconv("cp949", "utf-8", $depositor);
            break;
    }

    $od_receipt_point = $i_temp_point;
    $od_tno = $tno;
    $od_receipt_price = 0;
    $od_bank_account = $bankname . ' ' . $account;
    $od_deposit_name = $depositor;
    $pg_price = $amount;
    $od_misu = $i_price - $od_receipt_price;
} else if ($od_settle_case == "휴대폰") {
    switch ($default['de_pg_service']) {
        case 'lg':
            include G5_SHOP_PATH . '/lg/xpay_result.php';
            break;
        case 'inicis':
            include G5_SHOP_PATH . '/inicis/inistdpay_result.php';
            break;
        default:
            include G5_SHOP_PATH . '/kcp/pp_ax_hub.php';
            break;
    }

    $od_tno = $tno;
    $od_receipt_price = $amount;
    $od_receipt_point = $i_temp_point;
    $od_receipt_time = preg_replace("/([0-9]{4})([0-9]{2})([0-9]{2})([0-9]{2})([0-9]{2})([0-9]{2})/", "\\1-\\2-\\3 \\4:\\5:\\6", $app_time);
    $od_bank_account = $commid . ($commid ? ' ' : '') . $mobile_no;
    $pg_price = $amount;
    $od_misu = $i_price - $od_receipt_price;
    if ($od_misu == 0)
        $od_status = '입금';
} else if ($od_settle_case == "신용카드") {

    //결제수단 저장을 했을경우 저장된 빌키값을 사용.
    if (!empty($select_bill_info) && $select_bill_info == "1" && $member['od_billkey'] != "") {
        $billkey = $member['od_billkey'];
    } else {

        switch ($default['de_pg_service']) {
            case 'lg':
                include G5_SHOP_PATH . '/lg/xpay_result.php';
                break;
            case 'inicis':
                include G5_SHOP_PATH . '/inicis/inistdpay_result.php';
                break;
            default:
                include G5_SHOP_PATH . '/kcp/pp_ax_hub.php';
                $card_name = iconv("cp949", "utf-8", $card_name);
                break;
        }

    }

    $od_tno = $tno;
    $od_billkey = $billkey;
    $od_app_no = $app_no;
    $od_receipt_price = $amount;
    $od_receipt_point = $i_temp_point;
    $od_receipt_time = preg_replace("/([0-9]{4})([0-9]{2})([0-9]{2})([0-9]{2})([0-9]{2})([0-9]{2})/", "\\1-\\2-\\3 \\4:\\5:\\6", $app_time);
    $od_bank_account = $card_name;
    $pg_price = $amount;
    $od_misu = $i_price - $od_receipt_price;

    // 정기주문이 아닐때 실행
    if (empty($billkey)) {
        if ($od_misu == 0) {
            $od_status = '입금';
        }
    }


} else if ($od_settle_case == "간편결제" || ($od_settle_case == "lpay" && $default['de_pg_service'] === 'inicis')) {
    switch ($default['de_pg_service']) {
        case 'lg':
            include G5_SHOP_PATH . '/lg/xpay_result.php';
            break;
        case 'inicis':
            include G5_SHOP_PATH . '/inicis/inistdpay_result.php';
            break;
        default:
            include G5_SHOP_PATH . '/kcp/pp_ax_hub.php';
            $card_name = iconv("cp949", "utf-8", $card_name);
            break;
    }

    $od_tno = $tno;
    $od_app_no = $app_no;
    $od_receipt_price = $amount;
    $od_receipt_point = $i_temp_point;
    $od_receipt_time = preg_replace("/([0-9]{4})([0-9]{2})([0-9]{2})([0-9]{2})([0-9]{2})([0-9]{2})/", "\\1-\\2-\\3 \\4:\\5:\\6", $app_time);
    $od_bank_account = $card_name;
    $pg_price = $amount;
    $od_misu = $i_price - $od_receipt_price;
    if ($od_misu == 0)
        $od_status = '입금';
} else if ($od_settle_case == "KAKAOPAY") {
    include G5_SHOP_PATH . '/kakaopay/kakaopay_result.php';

    $od_tno = $tno;
    $od_app_no = $app_no;
    $od_receipt_price = $amount;
    $od_receipt_point = $i_temp_point;
    $od_receipt_time = preg_replace("/([0-9]{4})([0-9]{2})([0-9]{2})([0-9]{2})([0-9]{2})([0-9]{2})/", "\\1-\\2-\\3 \\4:\\5:\\6", $app_time);
    $od_bank_account = $card_name;
    $pg_price = $amount;
    $od_misu = $i_price - $od_receipt_price;
    if ($od_misu == 0)
        $od_status = '입금';
} else {
    die("od_settle_case Error!!!");
}

$od_pg = $default['de_pg_service'];
if ($od_settle_case == 'KAKAOPAY')
    $od_pg = 'KAKAOPAY';

// 주문금액과 결제금액이 일치하는지 체크(정기결재 제외
if ($tno and !$billkey) {
    /*if((int)$order_price !== (int)$pg_price) {
        $cancel_msg = '결제금액 불일치';
        switch($od_pg) {
            case 'lg':
                include G5_SHOP_PATH.'/lg/xpay_cancel.php';
                break;
            case 'inicis':
                include G5_SHOP_PATH.'/inicis/inipay_cancel.php';
                break;
            case 'KAKAOPAY':
                $_REQUEST['TID']               = $tno;
                $_REQUEST['Amt']               = $amount;
                $_REQUEST['CancelMsg']         = $cancel_msg;
                $_REQUEST['PartialCancelCode'] = 0;
                include G5_SHOP_PATH.'/kakaopay/kakaopay_cancel.php';
                break;
            default:
                include G5_SHOP_PATH.'/kcp/pp_ax_hub_cancel.php';
                break;
        }

        die("Receipt Amount Error");
    }*/
}

if ($is_member)
    $od_pwd = $member['mb_password'];
else
    $od_pwd = get_encrypt_string($_POST['od_pwd']);

// 주문번호를 얻는다.
$od_id = get_session('ss_order_id');

$od_escrow = 0;
if ($escw_yn == 'Y')
    $od_escrow = 1;

// 복합과세 금액
$od_tax_mny = round($i_price / 1.1);
$od_vat_mny = $i_price - $od_tax_mny;
$od_free_mny = 0;
if ($default['de_tax_flag_use']) {
    $od_tax_mny = (int)$_POST['comm_tax_mny'];
    $od_vat_mny = (int)$_POST['comm_vat_mny'];
    $od_free_mny = (int)$_POST['comm_free_mny'];
}

$od_email = get_email_address($od_email);
$od_name = clean_xss_tags($od_name);
$od_tel = clean_xss_tags($od_tel);
$od_hp = clean_xss_tags($od_hp);
$od_zip = preg_replace('/[^0-9]/', '', $od_zip);
$od_zip1 = substr($od_zip, 0, 3);
$od_zip2 = substr($od_zip, 3);
$od_addr1 = clean_xss_tags($od_addr1);
$od_addr2 = clean_xss_tags($od_addr2);
$od_addr3 = clean_xss_tags($od_addr3);
$od_addr_jibeon = preg_match("/^(N|R)$/", $od_addr_jibeon) ? $od_addr_jibeon : '';
$od_b_subject = clean_xss_tags($ad_subject);
$od_b_name = clean_xss_tags($od_b_name);
$od_b_tel = clean_xss_tags($od_b_tel);
$od_b_hp = clean_xss_tags($od_b_hp);
$od_b_addr1 = clean_xss_tags($od_b_addr1);
$od_b_addr2 = clean_xss_tags($od_b_addr2);
$od_b_addr3 = clean_xss_tags($od_b_addr3);
$od_b_addr_jibeon = preg_match("/^(N|R)$/", $od_b_addr_jibeon) ? $od_b_addr_jibeon : '';
$od_memo = clean_xss_tags($od_memo);
$od_trans_memo = clean_xss_tags($_POST['od_trans_memo']);
$od_deposit_name = clean_xss_tags($od_deposit_name);
$od_tax_flag = $default['de_tax_flag_use'];

$od_gubun = $_POST['od_gubun'];


//약국코드
if ($member['mb_type'] == 1) {
    $pharm_custno = $member['pharm_custno'];
    $pharm_name = $member['pharm_name'];
    $pharm_manager = $member['pharm_manager'];
} else {
    // 추천인 ID검색 =>약국코드 검색
    $sql = " select pharm_custno, pharm_name, pharm_manager
				from {$g5['member_table']} where mb_id = '{$member['mb_recommend']}' ";
    $row = sql_fetch($sql);
    $pharm_custno = $row['pharm_custno'];
    $pharm_name = $row['pharm_name'];
    $pharm_manager = $row['pharm_manager'];
}
//영업자 부서코드
$sql = " select pharm_manager_dept from {$g5['member_table']} where mb_id = '$pharm_manager' ";
$dept = sql_fetch($sql);
$pharm_manager_dept = $dept['pharm_manager_dept'];

/*************** 트랜잭션 관련 ****************/
$error_cnt = 0;
mysqli_autocommit($g5['connect_db'], false);
/*************** 트랜잭션 관련 ****************/

if(($mb_referee == 4 || $mb_extra == 11) && $od_settle_case == "포인트"){
    $od_settle_case = "적립금";
}

// 주문서에 입력
$sql = " insert {$g5['g5_shop_order_table']}
            set od_id             = '$od_id',
                mb_id             = '{$member['mb_id']}',
                od_pwd            = '$od_pwd',
				od_gubun          = '$od_gubun',
                pharm_name        = '$pharm_name',
				pharm_manager     = '$pharm_manager',
				pharm_manager_dept= '$pharm_manager_dept',
				od_name           = '$od_name',
                od_email          = '$od_email',
                od_tel            = '$od_tel',
                od_hp             = '$od_hp',
                od_zip1           = '$od_zip1',
                od_zip2           = '$od_zip2',
                od_addr1          = '$od_addr1',
                od_addr2          = '$od_addr2',
                od_addr3          = '$od_addr3',
                od_addr_jibeon    = '$od_addr_jibeon',
                od_b_subject      = '$ad_subject',
                od_b_name         = '$od_b_name',
                od_b_tel          = '$od_b_tel',
                od_b_hp           = '$od_b_hp',
                od_b_zip1         = '$od_b_zip1',
                od_b_zip2         = '$od_b_zip2',
                od_b_addr1        = '$od_b_addr1',
                od_b_addr2        = '$od_b_addr2',
                od_b_addr3        = '$od_b_addr3',
                od_b_addr_jibeon  = '$od_b_addr_jibeon',
                od_deposit_name   = '$od_deposit_name',
                od_memo           = '$od_memo',
                od_cart_count     = '$cart_count',
                od_cart_price     = '$tot_ct_price',
                od_cart_coupon    = '$tot_it_cp_price',
                od_send_cost      = '$od_send_cost',
                od_send_coupon    = '$tot_sc_cp_price',
                od_send_cost2     = '$od_send_cost2',
                od_coupon         = '$tot_cpprice',
                od_receipt_price  = '$od_receipt_price',
                od_receipt_point  = '$od_receipt_point',
                od_receipt_point_type  = '$point_type',
                od_bank_account   = '$od_bank_account',
                od_bank_date      = '$od_bank_date',
                od_receipt_time   = '$od_receipt_time',
                od_misu           = '$od_misu',
                od_pg             = '$od_pg',
                od_tno            = '$od_tno',
				od_billkey        = '$od_billkey',
                od_app_no         = '$od_app_no',
                od_escrow         = '$od_escrow',
                od_tax_flag       = '$od_tax_flag',
                od_tax_mny        = '$od_tax_mny',
                od_vat_mny        = '$od_vat_mny',
                od_free_mny       = '$od_free_mny',
                od_status         = '$od_status',
                od_shop_memo      = '',
                od_hope_date      = '$od_hope_date',
                od_time           = '" . G5_TIME_YMDHIS . "',
                od_ip             = '$REMOTE_ADDR',
				pharm_custno      = '$pharm_custno',
                od_settle_case    = '$od_settle_case',
                od_test           = '{$default['de_card_test']}'
                ";
$result = sql_query($sql, false);
/*************** 트랜잭션 관련 ****************/
if (mysqli_errno($g5['connect_db'])) {
    $error_cnt += 1;
}
/*************** 트랜잭션 관련 ****************/

/*
//추천약사ID 업데이트
if($_POST['mb_recommend']) {
	$sql = "update tbl_member set mb_recommend ='".$_POST['mb_recommend']."' where mb_id = '{$member['mb_id']}'" ;
	$result = sql_query($sql, false);
}
*/
// 주문 상세정보 생성 2018-07-04 ccy
/*
$sql = "insert into tbl_shop_order_detail (od_id, it_id, it_name, od_option, od_status, od_price, od_qty, comp_code)
		select $od_id od_id, a.it_id, a.it_name,a.io_id, '$od_status' od_status, a.ct_price, a.ct_qty, b.comp_code
		  from tbl_shop_cart a, tbl_shop_item b
		where a.it_id = b.it_id
		   and a.od_id = '$tmp_cart_id'
           and ct_select = '1' ";
$result = sql_query($sql, false);
*/

//주문 상세정보 생성
$sql = "select a.io_id, a.ct_option, a.ct_price, a.ct_qty, a.it_sc_cost, a.ct_period_yn, a.ct_period_sdate, a.ct_period_week, a.ct_period_cnt, a.ct_period_memo,b.*
		  from tbl_shop_cart a, tbl_shop_item b
		where a.it_id = b.it_id
		   and a.od_id = '$tmp_cart_id'
           and ct_select = '1' ";
$result = sql_query($sql);
for ($i = 0; $row = sql_fetch_array($result); $i++) {
    $od_num = $i + 1;
    //1. 주문상세정보
    //단가정보
    $io_sale_price = 0;
    $io_drug_price = 0;
    $io_incen = 0;
    $io_supply_price = 0;
    $io_dodome_price = 0;
    $io_period_rate1 = 0;
    $io_period_cnt1 = 0;
    $od_sum_qty = 0;
    $ct_period_yn = $row['ct_period_yn'];
    $ct_period_cnt = $row['ct_period_cnt'];

    $od_price = $row['it_price'] * $row['ct_qty'];
    $od_qty = $row['ct_qty'];

    $it_supply_price = $row['it_supply_price'];
    $it_dodome_price = $row['it_dodome_price'];
    if ($od_gubun == '3') {//일반회원 주문
        $it_sale_price = $row['it_app_sale_price'];
        $it_drug_price = $row['it_app_drug_price'];
        $it_drug_price_2 = $row['it_drug_price']; //약국판매가로 총무매출잡기위해
        $it_incen = $row['it_app_incen'];
    } else {
        $it_sale_price = $row['it_price'];
        $it_drug_price = $row['it_drug_price'];
        $it_drug_price_2 = $row['it_drug_price'];
        $it_incen = $row['it_incen'];
    }

    if ($row['io_id'] != '') {
        //옵션단가 재계산
        $sql = " select *
					from {$g5['g5_shop_item_option_table']} 
					where it_id = '{$row['it_id']}'
					  and io_id = '{$row['io_id']}'";

        $io = sql_fetch($sql);
        $io_supply_price = $io['io_supply_price'];
        $io_dodome_price = $io['io_dodome_price'];
        $io_period_rate1 = $io['io_period_rate1'];
        $io_period_cnt1 = $io['io_period_cnt1'];

        if ($od_gubun == '3') {//일반회원 주문
            $io_sale_price = $io['io_app_sale_price'];
            $io_drug_price = $io['io_app_drug_price'];
            $io_drug_price_2 = $io['io_drug_price'];
            $io_incen = $io['io_app_incen'];

        } else {
            $io_sale_price = $io['io_price'];
            $io_drug_price = $io['io_drug_price'];
            $io_drug_price_2 = $io['io_drug_price'];
            $io_incen = $io['io_incen'];

        }
        $od_sum_supply_price = $it_supply_price + $io_supply_price;
        $od_sum_dodome_price = $it_dodome_price + $io_dodome_price;
        $od_sum_sale_price = $it_sale_price + $io_sale_price;
        $od_sum_drug_price = $it_drug_price + $io_drug_price;
        $od_sum_incen = $it_incen + $io_incen;
        $od_sum_drug_price_2 = $it_drug_price_2 + $io_drug_price_2;

    } else {
        $od_sum_supply_price = $it_supply_price;
        $od_sum_dodome_price = $it_dodome_price;
        $od_sum_sale_price = $it_sale_price;
        $od_sum_drug_price = $it_drug_price;
        $od_sum_incen = $it_incen;
        $od_sum_drug_price_2 = $it_drug_price_2;
    }


    //정기주문 토탈금액처리
    //1. 횟수만큼 모두정산
    //2. 할인율적용
    if ($ct_period_yn == 'Y') {
        $od_total_supply_price = ($od_sum_supply_price * $od_qty) * $ct_period_cnt;
        $od_total_dodome_price = ($od_sum_dodome_price * $od_qty) * $ct_period_cnt;

        if ($io_period_rate1 > 0) {
            $od_total_sale_price = (($od_sum_sale_price * $io_period_cnt1) * ($io_period_rate1 * 0.01)) * $od_qty;
        } else {
            $od_total_sale_price = ($od_sum_sale_price * $od_qty) * $ct_period_cnt;
        }
        $od_total_drug_price = ($od_sum_drug_price * $od_qty) * $ct_period_cnt;
        $od_total_incen = ($od_sum_incen * $od_qty) * $ct_period_cnt;
        $od_sum_qty = $od_qty * $ct_period_cnt;
        $od_total_drug_price_2 = ($od_sum_drug_price_2 * $od_qty) * $ct_period_cnt;
    } else {
        $od_total_supply_price = $od_sum_supply_price * $od_qty;
        $od_total_dodome_price = $od_sum_dodome_price * $od_qty;
        $od_total_sale_price = $od_sum_sale_price * $od_qty;
        $od_total_drug_price = $od_sum_drug_price * $od_qty;
        $od_total_incen = $od_sum_incen * $od_qty;
        $od_sum_qty = $od_qty;
        $od_total_drug_price_2 = $od_sum_drug_price_2 * $od_qty;
    }

    $p_point = 0;
    if($mb_referee == 4 || $mb_extra == 11){
        $p_point = get_price_cash($row['it_point_type'],$row['it_price']*$od_sum_qty, $member['mb_grade']);
    }

    //상품별
    $sql = " insert tbl_shop_order_detail
            set od_id         = '$od_id',
                it_id         = '{$row['it_id']}',
				od_num        = $od_num,
                it_name       = '{$row['it_name']}',
				io_id         = '{$row['io_id']}',
				od_option     = '{$row['ct_option']}',
				od_price      = '$od_price',
				od_qty        = '$od_sum_qty',
				comp_code     = '{$row['comp_code']}',
				od_supply_price	= '$it_supply_price',
				od_dodome_price	= '$it_dodome_price',
				od_sale_price	= '$it_sale_price',
				od_drug_price	= '$it_drug_price',
				od_incen		= '$it_incen',
				od_itm_supply_price	= '$io_supply_price',
				od_itm_dodome_price	= '$io_dodome_price',
				od_itm_sale_price	= '$io_sale_price',
				od_itm_drug_price	= '$io_drug_price',
				od_itm_incen		= '$io_incen',
				od_sum_supply_price	= '$od_sum_supply_price',
				od_sum_dodome_price	= '$od_sum_dodome_price',
				od_sum_sale_price	= '$od_sum_sale_price',
				od_sum_drug_price	= '$od_sum_drug_price',
				od_sum_incen		= '$od_sum_incen',
				od_send_cost    = '{$row['it_sc_cost']}',
				od_total_supply_price	= '$od_total_supply_price',
				od_total_dodome_price	= '$od_total_dodome_price',
				od_total_sale_price		= '$od_total_sale_price',
				od_total_drug_price		= '$od_total_drug_price',
				od_total_incen		= '$od_total_incen',
				od_period_yn =  '{$row['ct_period_yn']}',
				od_period_week =  '{$row['ct_period_week']}',
				od_period_cnt =  '{$row['ct_period_cnt']}',
				od_cash = '$p_point'
				";
    sql_query($sql, false);
    /*************** 트랜잭션 관련 ****************/
    if (mysqli_errno($g5['connect_db'])) {
        $error_cnt += 1;
    }
    /*************** 트랜잭션 관련 ****************/
    $sql = " insert tbl_shop_order_drug
            set od_id         = '$od_id',
                it_id         = '{$row['it_id']}',
				od_num        = $od_num,
				od_total_drug_price		= '$od_total_drug_price_2'";
    sql_query($sql, false);
    /*************** 트랜잭션 관련 ****************/
    if (mysqli_errno($g5['connect_db'])) {
        $error_cnt += 1;
    }
    /*************** 트랜잭션 관련 ****************/

    if ($od_status == '입금') {
        $od_pay_yn = 'Y';
    } else {
        $od_pay_yn = 'N';
    }

    //2. 정기주문
    if ($ct_period_yn == 'Y') {

        //2.2 DETAIL
        $sdate = $row['ct_period_sdate'];
        $period_week = $row['ct_period_week'];
        $period_cnt = $row['ct_period_cnt'];

        //1회차
        $sql = " insert into tbl_shop_order_receiver 
					set od_id         = '$od_id',
				    od_num			  = '$od_num',
					od_seq			  = '1',
					od_b_name         = '$od_b_name',
					od_b_tel          = '$od_b_tel',
					od_b_hp           = '$od_b_hp',
					od_b_zip1         = '$od_b_zip1',
					od_b_zip2         = '$od_b_zip2',
					od_b_addr1        = '$od_b_addr1',
					od_b_addr2        = '$od_b_addr2',
					od_b_addr3        = '$od_b_addr3',
					od_b_addr_jibeon  = '$od_b_addr_jibeon',
					od_period_date	  = '$sdate',
					od_qty            = '$od_qty',
					od_date1          = '" . G5_TIME_YMDHIS . "',
                    od_pay_yn         = '$od_pay_yn',
                    od_trans_memo     = '$od_trans_memo',
					od_status         = '주문'";
        sql_query($sql, false);
        /*************** 트랜잭션 관련 ****************/
        if (mysqli_errno($g5['connect_db'])) {
            $error_cnt += 1;
        }
        /*************** 트랜잭션 관련 ****************/

        //$sdate = '2018-07-24'; //기준이 되는 날짜
        for ($k = 2; $k <= $period_cnt; $k++) {
            $str_date = strtotime($sdate); //컴퓨터가 인식할 수 있게 변환
            if ($period_week == '1') {
                $str_date = strtotime($sdate . '+7 days'); // +1 days로 하루를 더함
            }
            if ($period_week == '2') {
                $str_date = strtotime($sdate . '+14 days');
            }
            if ($period_week == '3') {
                $str_date = strtotime($sdate . '+21 days');
            }
            if ($period_week == '4') {
                $str_date = strtotime($sdate . '+28 days');
            }
            if ($period_week == '5') {
                $str_date = strtotime("+" . ($k - 1) . " month", strtotime($row['ct_period_sdate']));
            }

            $sdate = date('Y-m-d', $str_date);
            //echo $date;
            //$sql = "insert into tbl_shop_order_auto_detail (od_id, od_num, it_id, auto_seq, auto_date, auto_od_id, auto_status)
            //    values ($od_id, $od_num, '{$row['it_id']}', '$k', '$sdate', '', '')";
            $sql = " insert into tbl_shop_order_receiver 
					set od_id         = '$od_id',
				    od_num			  = '$od_num',
					od_seq			  = '$k',
					od_b_name         = '$od_b_name',
					od_b_tel          = '$od_b_tel',
					od_b_hp           = '$od_b_hp',
					od_b_zip1         = '$od_b_zip1',
					od_b_zip2         = '$od_b_zip2',
					od_b_addr1        = '$od_b_addr1',
					od_b_addr2        = '$od_b_addr2',
					od_b_addr3        = '$od_b_addr3',
					od_b_addr_jibeon  = '$od_b_addr_jibeon',
					od_period_date	  = '$sdate',
					od_qty            = '$od_qty',					
					od_date1          = '" . G5_TIME_YMDHIS . "',
                    od_pay_yn         = '$od_pay_yn',
                    od_trans_memo     = '$od_trans_memo',
					od_status         = '주문'";
            sql_query($sql, false);
            /*************** 트랜잭션 관련 ****************/
            if (mysqli_errno($g5['connect_db'])) {
                $error_cnt += 1;
            }
            /*************** 트랜잭션 관련 ****************/

        }

    } else {
        //일반주문
        $sql = " insert into tbl_shop_order_receiver 
					set od_id         = '$od_id',
				    od_num			  = $od_num,
					od_seq			  = 1,
					od_b_name         = '$od_b_name',
					od_b_tel          = '$od_b_tel',
					od_b_hp           = '$od_b_hp',
					od_b_zip1         = '$od_b_zip1',
					od_b_zip2         = '$od_b_zip2',
					od_b_addr1        = '$od_b_addr1',
					od_b_addr2        = '$od_b_addr2',
					od_b_addr3        = '$od_b_addr3',
					od_b_addr_jibeon  = '$od_b_addr_jibeon',
					od_period_date	  = '',
					od_qty            = '$od_qty',					
					od_date1          = '" . G5_TIME_YMDHIS . "',
                    od_pay_yn         = '$od_pay_yn',
                    od_trans_memo     = '$od_trans_memo',
					od_status         = '주문'";
        sql_query($sql, false);
        /*************** 트랜잭션 관련 ****************/
        if (mysqli_errno($g5['connect_db'])) {
            $error_cnt += 1;
        }
        /*************** 트랜잭션 관련 ****************/
    }
}

/* 위에 로직은 건드리지 않고 다중배송일경우 다중배송테이블에 정보를 따로 넣어준다. */
if ($multiYN == "Y") {

    $multi_sum = 0;
    $cnt = $_POST['multi_addr_cnt'];

    for ($i = 0; $i < $cnt; $i++) {

        $od_seq = $i + 1;
        $multi_cnt = clean_xss_tags($_POST['multi_cnt'][$i]);
        $multi_b_name = clean_xss_tags($_POST['multi_b_name'][$i]);
        $multi_b_hp = clean_xss_tags($_POST['multi_b_hp'][$i]);
        $multi_b_zip = clean_xss_tags($_POST['multi_b_zip'][$i]);
        $multi_b_addr1 = clean_xss_tags($_POST['multi_b_addr1'][$i]);
        $multi_b_addr2 = clean_xss_tags($_POST['multi_b_addr2'][$i]);
        $multi_b_addr3 = clean_xss_tags($_POST['multi_b_addr3'][$i]);
        $multi_b_addr_jibeon = clean_xss_tags($_POST['multi_b_addr_jibeon'][$i]);

        $multi_b_zip = preg_replace('/[^0-9]/', '', $multi_b_zip);
        $multi_b_zip1 = substr($multi_b_zip, 0, 3);
        $multi_b_zip2 = substr($multi_b_zip, 3);

        //다중배송 상품일경우 받는사람정보 넣어주기
        $sql = " insert into tbl_shop_order_multi_receiver 
					set od_id         = '$od_id',
				    od_num			  = 1,
					od_seq			  = $od_seq,
					od_b_name         = '$multi_b_name',
					od_b_hp           = '$multi_b_hp',
					od_b_zip1         = '$multi_b_zip1',
					od_b_zip2         = '$multi_b_zip2',
					od_b_addr1        = '$multi_b_addr1',
					od_b_addr2        = '$multi_b_addr2',
					od_b_addr3        = '$multi_b_addr3',
					od_b_addr_jibeon  = '$multi_b_addr_jibeon',
					od_qty            = '$multi_cnt' ";

        sql_query($sql, false);
        /*************** 트랜잭션 관련 ****************/
        if (mysqli_errno($g5['connect_db'])) {
            $error_cnt += 1;
        }
        /*************** 트랜잭션 관련 ****************/

        $multi_sum = $multi_sum + $multi_cnt;
    }

    if ($multi_sum == 0 || ($od_qty != $multi_sum)) {
        /*************** 트랜잭션 관련 ****************/
        $error_cnt += 1;
        /*************** 트랜잭션 관련 ****************/
    }

    //다중배송주문인지 구분값 업데이트
    $sql = " update {$g5['g5_shop_order_table']} set od_multi_yn = 'Y' where od_id = '$od_id' ";
    sql_query($sql, false);
    /*************** 트랜잭션 관련 ****************/
    if (mysqli_errno($g5['connect_db'])) {
        $error_cnt += 1;
    }
    /*************** 트랜잭션 관련 ****************/

}

// 사내수령
if($companyYN == 'Y'){
    $sql = " update {$g5['g5_shop_order_table']} set od_company_yn = 'Y', od_company_send = '$i_company_sendcost' ,od_misu ='$od_misu', od_send_cost = 0 where od_id = '$od_id' ";
    sql_query($sql, false);
    /*************** 트랜잭션 관련 ****************/
    if (mysqli_errno($g5['connect_db'])) {
        $error_cnt += 1;
    }
    /*************** 트랜잭션 관련 ****************/
}


// 주문정보 입력 오류시 결제 취소
if (!$result) {
    if ($tno) {
        $cancel_msg = '주문정보 입력 오류';
        switch ($od_pg) {
            case 'lg':
                include G5_SHOP_PATH . '/lg/xpay_cancel.php';
                break;
            case 'inicis':
                include G5_SHOP_PATH . '/inicis/inipay_cancel.php';
                break;
            case 'KAKAOPAY':
                $_REQUEST['TID'] = $tno;
                $_REQUEST['Amt'] = $amount;
                $_REQUEST['CancelMsg'] = $cancel_msg;
                $_REQUEST['PartialCancelCode'] = 0;
                include G5_SHOP_PATH . '/kakaopay/kakaopay_cancel.php';
                break;
            default:
                include G5_SHOP_PATH . '/kcp/pp_ax_hub_cancel.php';
                break;
        }
    }

    // 관리자에게 오류 알림 메일발송
    $error = 'order';
    include G5_SHOP_PATH . '/ordererrormail.php';

    die('<p>고객님의 주문 정보를 처리하는 중 오류가 발생해서 주문이 완료되지 않았습니다.</p><p>' . strtoupper($od_pg) . '를 이용한 전자결제(신용카드, 계좌이체, 가상계좌 등)은 자동 취소되었습니다.');
}


// 장바구니 상태변경
// 신용카드로 주문하면서 신용카드 포인트 사용하지 않는다면 포인트 부여하지 않음
$cart_status = $od_status;
$sql_card_point = "";
if ($od_receipt_price > 0 && !$default['de_card_point']) {
    $sql_card_point = " , ct_point = '0' ";
}
$sql = "update {$g5['g5_shop_cart_table']}
           set od_id = '$od_id',
               ct_status = '$cart_status'
               $sql_card_point
         where od_id = '$tmp_cart_id'
           and ct_select = '1' ";
$result = sql_query($sql, false);
/*************** 트랜잭션 관련 ****************/
if (mysqli_errno($g5['connect_db'])) {
    $error_cnt += 1;
}
/*************** 트랜잭션 관련 ****************/


// 주문정보 입력 오류시 결제 취소
if (!$result) {
    if ($tno) {
        $cancel_msg = '주문상태 변경 오류';
        switch ($od_pg) {
            case 'lg':
                include G5_SHOP_PATH . '/lg/xpay_cancel.php';
                break;
            case 'inicis':
                include G5_SHOP_PATH . '/inicis/inipay_cancel.php';
                break;
            case 'KAKAOPAY':
                $_REQUEST['TID'] = $tno;
                $_REQUEST['Amt'] = $amount;
                $_REQUEST['CancelMsg'] = $cancel_msg;
                $_REQUEST['PartialCancelCode'] = 0;
                include G5_SHOP_PATH . '/kakaopay/kakaopay_cancel.php';
                break;
            default:
                include G5_SHOP_PATH . '/kcp/pp_ax_hub_cancel.php';
                break;
        }
    }

    // 관리자에게 오류 알림 메일발송
    $error = 'status';
    include G5_SHOP_PATH . '/ordererrormail.php';

    // 주문삭제
    sql_query(" delete from {$g5['g5_shop_order_table']} where od_id = '$od_id' ");

    die('<p>고객님의 주문 정보를 처리하는 중 오류가 발생해서 주문이 완료되지 않았습니다.</p><p>' . strtoupper($od_pg) . '를 이용한 전자결제(신용카드, 계좌이체, 가상계좌 등)은 자동 취소되었습니다.');
}


// 적립금 | 포인트 사용`
if(($mb_referee == 4 || $mb_extra == 11) && $od_receipt_point){
    insert_cash($member['mb_id'], (-1) * $od_receipt_point, "주문번호 $od_id 결제", '', '', '', '', $od_id);
} else if ($is_member && $od_receipt_point){
    insert_point($member['mb_id'], (-1) * $od_receipt_point, "주문번호 $od_id 결제", '', '', '', '', $od_id);
}

$od_memo = nl2br(htmlspecialchars2(stripslashes($od_memo))) . "&nbsp;";


// 쿠폰사용내역기록
if ($is_member) {
    $it_cp_cnt = count($_POST['cp_id']);
    for ($i = 0; $i < $it_cp_cnt; $i++) {
        $cid = $_POST['cp_id'][$i];
        $cp_it_id = $_POST['it_id'][$i];
        $cp_prc = (int)$arr_it_cp_prc[$cp_it_id];

        if (trim($cid)) {
            $sql = " insert into {$g5['g5_shop_coupon_log_table']}
                        set cp_id       = '$cid',
                            mb_id       = '{$member['mb_id']}',
                            od_id       = '$od_id',
                            cp_price    = '$cp_prc',
                            cl_datetime = '" . G5_TIME_YMDHIS . "' ";
            sql_query($sql);
            /*************** 트랜잭션 관련 ****************/
            if (mysqli_errno($g5['connect_db'])) {
                $error_cnt += 1;
            }
            /*************** 트랜잭션 관련 ****************/
        }

        // 쿠폰사용금액 cart에 기록
        $cp_prc = (int)$arr_it_cp_prc[$cp_it_id];
        $sql = " update {$g5['g5_shop_cart_table']}
                    set cp_price = '$cp_prc'
                    where od_id = '$od_id'
                      and it_id = '$cp_it_id'
                      and ct_select = '1'
                    order by ct_id asc
                    limit 1 ";
        sql_query($sql);
        /*************** 트랜잭션 관련 ****************/
        if (mysqli_errno($g5['connect_db'])) {
            $error_cnt += 1;
        }
        /*************** 트랜잭션 관련 ****************/
    }

    if ($_POST['od_cp_id']) {
        $str_cp_id = explode(':', $_POST['od_cp_id']);
        $cp_cnt = count($str_cp_id);
        for ($j = 0; $j < $cp_cnt; $j++) {
            $sql = " select cp_price from {$g5['g5_shop_coupon_table']}
                        where cp_id       = '{$str_cp_id[$j]}' ";
            $cp = sql_fetch($sql);
            $cp_p = $cp[cp_price];

            $sql = " insert into {$g5['g5_shop_coupon_log_table']}
						set cp_id       = '{$str_cp_id[$j]}',
							mb_id       = '{$member['mb_id']}',
							od_id       = '$od_id',
							cp_price    = '$cp_p',
							cl_datetime = '" . G5_TIME_YMDHIS . "' ";
            sql_query($sql);
            /*************** 트랜잭션 관련 ****************/
            if (mysqli_errno($g5['connect_db'])) {
                $error_cnt += 1;
            }
            /*************** 트랜잭션 관련 ****************/
        }
    }

    if ($_POST['sc_cp_id']) {
        $sql = " insert into {$g5['g5_shop_coupon_log_table']}
                    set cp_id       = '{$_POST['sc_cp_id']}',
                        mb_id       = '{$member['mb_id']}',
                        od_id       = '$od_id',
                        cp_price    = '$tot_sc_cp_price',
                        cl_datetime = '" . G5_TIME_YMDHIS . "' ";
        sql_query($sql);
        /*************** 트랜잭션 관련 ****************/
        if (mysqli_errno($g5['connect_db'])) {
            $error_cnt += 1;
        }
        /*************** 트랜잭션 관련 ****************/
    }
}

//include_once(G5_SHOP_PATH.'/ordermail1.inc.php');
//include_once(G5_SHOP_PATH.'/ordermail2.inc.php');

// ======================= 상품 재고 수량 업데이트 ================================
$sql = "SELECT it_id, io_id, od_qty  FROM tbl_shop_order_detail WHERE od_id = '$od_id' ";
$re_sql = sql_query($sql);

for($i=0; $re=sql_fetch_array($re_sql) ;$i++) {

    $it_id = $re['it_id'];
    $io_id = $re['io_id'];
    $od_qty = $re['od_qty'];

    if ($io_id) {
        // 옵션
        $sql1 = "SELECT io_stock_qty from {$g5['g5_shop_item_option_table']} where it_id = '$it_id'";
        $re2 = sql_fetch($sql1);

        $jaego = $re2['io_stock_qty']; //현재 재고

        $qty = ($jaego - $od_qty);
        $sql2 = "update {$g5['g5_shop_item_option_table']} set io_stock_qty = '$qty' where it_id = '$it_id'";
        sql_fetch($sql2);

        /*************** 트랜잭션 관련 ****************/
        if (mysqli_errno($g5['connect_db'])) {
            $error_cnt += 1;
        }
        /*************** 트랜잭션 관련 ****************/
        
    } else {
        $sql1 = "SELECT it_stock_qty from {$g5['g5_shop_item_table']} where it_id = '$it_id'";
        $re2 = sql_fetch($sql1);

        $jaego = $re2['it_stock_qty']; //현재 재고
        $qty = ($jaego - $od_qty); // 현재 재고 - 주문수량

        $sql2 = "update {$g5['g5_shop_item_table']} set it_stock_qty = '$qty' where it_id = '$it_id'";
        sql_fetch($sql2);

        /*************** 트랜잭션 관련 ****************/
        if (mysqli_errno($g5['connect_db'])) {
            $error_cnt += 1;
        }
        /*************** 트랜잭션 관련 ****************/
    }
}
// ======================= 상품 재고 수량 업데이트 끝 ================================


// SMS BEGIN --------------------------------------------------------
// 주문고객과 쇼핑몰관리자에게 SMS 전송
if ($config['cf_sms_use'] && ($default['de_sms_use2'] || $default['de_sms_use3']) && $error_cnt == 0) {
    include_once(G5_LIB_PATH . '/kakao.alimtalk.lib.php');
    // 주문일시, 주문상품, 입금요청액
    $sql = " SELECT a.od_time, a.od_misu, a.od_b_name, b.it_name from tbl_shop_order a, tbl_shop_order_detail b WHERE a.od_id = b.od_id AND a.od_id = '$od_id'";
    $count = " SELECT count(*) as count from tbl_shop_order a, tbl_shop_order_detail b WHERE a.od_id = b.od_id AND a.od_id = '$od_id'";
    $m_row = sql_fetch($sql);
    $c_row = sql_fetch($count);

    // 다중배송
    if ($multiYN == "Y") {
        $multi_sql = " select * from tbl_shop_order_multi_receiver where od_id = '$od_id' limit 1";
        $multi_re = sql_fetch($multi_sql);
        $multi_od_b_name = $multi_re['od_b_name'];
    }

    $mb_name = $od_name;
    //주문자 알림톡
    $mb_hp = $od_hp;
    $recv_number = preg_replace('/[^0-9]/', '', $mb_hp);

    //if($recv_number) {
    // AlimTalk BEGIN --------------------------------------------------------
    $alimtalk = new AlimTalk();
    $to = $alimtalk->set_phone_number($recv_number);

    if ($od_gubun == '3' && $od_settle_case == '무통장') { // 1:고객주문,2:약국주문,3:일반주문
        // 무통장입금 은행계좌번호
        $de_bank_account = $default['de_bank_account'];

        // 무통장입금_주문(고객)
        $template_code = 'os_order_006';
        $text = $alimtalk->get_template($template_code);
        $text = str_replace('#{이름}', $mb_name, $text);
        $text = str_replace('#{주문일시}', $m_row['od_time'], $text);
        $text = str_replace('#{주문번호}', $od_id, $text);
        if($c_row['count']==1){
            $text = str_replace('#{주문상품}', $m_row['it_name'], $text);
        } else{
            $text = str_replace('#{주문상품}', $m_row['it_name'].' 외 '.($c_row['count'] -1).'건', $text);
        }
        $text = str_replace('#{계좌번호}', $de_bank_account, $text);
        $text = str_replace('#{입금기한}', $od_bank_date, $text);
        $text = str_replace('#{입금요청금액}', number_format($m_row['od_misu']).' 원', $text);
    } else{

        // 주문 완료 (고객, 약국)
        $template_code = 'os_order_001';
        $text = $alimtalk->get_template($template_code);
        $text = str_replace('#{이름}', $mb_name, $text);
        $text = str_replace('#{주문일시}', $m_row['od_time'], $text);
        $text = str_replace('#{주문번호}', $od_id, $text);
        if($c_row['count']==1){
            $text = str_replace('#{주문상품}', $m_row['it_name'], $text);
        } else{
            $text = str_replace('#{주문상품}', $m_row['it_name'].' 외 '.($c_row['count'] -1).'건', $text);
        }
        if ($multiYN != "Y") {
            $text = str_replace('#{수령인}', $od_b_name, $text);
        } else {
            $text = str_replace('#{수령인}', $multi_od_b_name.' 외'.($cnt -1).'명', $text);
        }
    }

    $alimtalk->set_message($template_code, $to, $text);
    $alimtalk->send();
    // AlimTalk END   --------------------------------------------------------
    //}
    //첫구매시 메시지
    //첫구매인지 확인
    if (is_first_order($member['mb_id'])) {
        //쿠폰발행
        $cp_id = get_coupon_id();
        $mb_id = $member['mb_id'];
        $cp_subject = '첫구매 감사쿠폰';
        $cp_method = 2;
        $cp_target = '';
        $cp_start = G5_TIME_YMD;
        $cp_end = date("Y-m-d", (G5_SERVER_TIME + (86400 * 30)));
        $cp_type = 0;
        $cp_price = 2000;//$default['de_member_reg_coupon_price'];
        $cp_trunc = 1;
        $cp_minimum = 0;//$default['de_member_reg_coupon_minimum'];
        $cp_maximum = 0;

        $sql = " INSERT INTO {$g5['g5_shop_coupon_table']}
					( cp_id, cp_subject, cp_method, cp_target, mb_id, cp_start, cp_end, cp_type, cp_price, cp_trunc, cp_minimum, cp_maximum, cp_datetime )
				VALUES
					( '$cp_id', '$cp_subject', '$cp_method', '$cp_target', '$mb_id', '$cp_start', '$cp_end', '$cp_type', '$cp_price', '$cp_trunc', '$cp_minimum', '$cp_maximum', '" . G5_TIME_YMDHIS . "' ) ";

        $res = sql_query($sql, false);
        /*************** 트랜잭션 관련 ****************/
        if (mysqli_errno($g5['connect_db'])) {
            $error_cnt += 1;
        }
        /*************** 트랜잭션 관련 ****************/
    }


    //약국회원일경우 담당 영업사원에게 알림톡보내기
    if ($member['mb_type'] == '1') { // 회원구분(0:일반,1:약국,5:영업자,7:공급사,9:관리자)
        $sql = " select b.mb_id , b.mb_name , b.mb_hp , a.pharm_name, a.od_gubun , a.od_name, c.it_name
				 from tbl_shop_order a, tbl_member b, tbl_shop_order_detail c 
				 where a.pharm_manager = b.mb_id and a.od_id = '$od_id' and a.od_id = c.od_id
				 and b.mb_type=5 limit 1 ";
        $row = sql_fetch($sql);
        $mb_hp = $row['mb_hp'];
        $recv_number = preg_replace('/[^0-9]/', '', $mb_hp);
        if ($recv_number) {
            // AlimTalk BEGIN --------------------------------------------------------
            $alimtalk = new AlimTalk();

            if ($row['od_gubun'] =="1" || $row['od_gubun'] == "2") { // 1:고객주문,2:약국주문,3:일반주문

                $to2 = $alimtalk->set_phone_number($recv_number);

                $template_code2 = 'os_order_005';
                $text2 = $alimtalk->get_template($template_code2);
                $text2 = str_replace('#{이름}', $row['mb_name'], $text2);
                $text2 = str_replace('#{약국명}', $row['pharm_name'], $text2);
                $text2 = str_replace('#{고객명}', $row['od_name'], $text2);
                $text2 = str_replace('#{주문번호}', $od_id, $text2);
                if($c_row['count']==1){
                    $text2 = str_replace('#{주문상품}', $m_row['it_name'], $text2);
                } else{
                    $text2 = str_replace('#{주문상품}', $m_row['it_name'].' 외 '.($c_row['count'] -1).'건', $text2);
                }

                $alimtalk->set_message($template_code2, $to2, $text2);
                $alimtalk->send();
            }

            // AlimTalk END   --------------------------------------------------------
        }
    }
    //공급사

    if ($od_settle_case != "무통장" || $member['mb_type'] == '1' ||  $member['mb_type'] == '9') {

        $sql = "select a.comp_code, a.it_name, b.comp_md_tel, c.od_time
			    from tbl_shop_order_detail a, tbl_member b, tbl_shop_order c
		     	where a.comp_code = b.comp_code
			    and a.od_id = '$od_id'
		     	and a.od_id = c.od_id
			    and b.mb_type=7
			    group by a.comp_code, b.comp_md_tel";


        $result = sql_query($sql);
        for ($i = 0; $row = sql_fetch_array($result); $i++) {
            $mb_hp = $row['comp_md_tel'];
            $it_name = $row['it_name'];
            $od_time = $row['od_time'];
            $recv_number = preg_replace('/[^0-9]/', '', $mb_hp);
            if ($recv_number) {
                // AlimTalk BEGIN --------------------------------------------------------
                $alimtalk = new AlimTalk();
                $to = $alimtalk->set_phone_number($recv_number);

                // 주문완료_공급사
                $template_code = 'os_order_002';
                $text = $alimtalk->get_template($template_code);
                $text = str_replace('#{이름}', $mb_name, $text);
                $text = str_replace('#{주문일시}', $od_time, $text);
                $text = str_replace('#{주문번호}', $od_id, $text);
                if($c_row['count']==1){
                    $text = str_replace('#{주문상품}', $m_row['it_name'], $text);
                } else{
                    $text = str_replace('#{주문상품}', $m_row['it_name'].' 외 '.($c_row['count'] -1).'건', $text);
                }
                if ($multiYN != "Y") {
                    $text = str_replace('#{수령인}', $od_b_name, $text);
                } else{
                    $text = str_replace('#{수령인}', $od_b_name.' 외 '.($cnt -1).'명', $text);
                }
                $alimtalk->set_message($template_code, $to, $text);
                $alimtalk->send();
                // AlimTalk END   --------------------------------------------------------
            }
        }
    }
}
// SMS END   --------------------------------------------------------





// orderview 에서 사용하기 위해 session에 넣고
$uid = md5($od_id . G5_TIME_YMDHIS . $REMOTE_ADDR);
set_session('ss_orderview_uid', $uid);

// 주문 정보 임시 데이터 삭제
if ($od_pg == 'inicis') {
    $sql = " delete from {$g5['g5_shop_order_data_table']} where od_id = '$od_id' and dt_pg = '$od_pg' ";
    sql_query($sql);
    /*************** 트랜잭션 관련 ****************/
    if (mysqli_errno($g5['connect_db'])) {
        $error_cnt += 1;
    }
    /*************** 트랜잭션 관련 ****************/
}

// 주문번호제거
set_session('ss_order_id', '');

// 기존자료 세션에서 제거
if (get_session('ss_direct'))
    set_session('ss_cart_direct', '');

// 배송지처리
if ($is_member) {

    //약국에서 고객주문은 제외
    if ($od_gubun != '1') {
        $sql = " select * from {$g5['g5_shop_order_address_table']}
					where mb_id = '{$member['mb_id']}'
					  and ad_name = '$od_b_name'
					  and ad_tel = '$od_b_tel'
					  and ad_hp = '$od_b_hp'
					  and ad_zip1 = '$od_b_zip1'
					  and ad_zip2 = '$od_b_zip2'
					  and ad_addr1 = '$od_b_addr1'
					  and ad_addr2 = '$od_b_addr2'
					  and ad_addr3 = '$od_b_addr3' ";
        $row = sql_fetch($sql);

        // 기본배송지 체크
        if ($ad_default) {
            $sql = " update {$g5['g5_shop_order_address_table']}
						set ad_default = '0'
						where mb_id = '{$member['mb_id']}' ";
            sql_query($sql);
            /*************** 트랜잭션 관련 ****************/
            if (mysqli_errno($g5['connect_db'])) {
                $error_cnt += 1;
            }
            /*************** 트랜잭션 관련 ****************/
        }

        $ad_subject = clean_xss_tags($ad_subject);

        if ($multiYN != "Y") { // 다중배송이 아닌 경우에만
            if ($row['ad_id']) {

                // 체크 했을때만 업데이트 하도록 조건 추가
                $ad_default_sql = "";
                if ($ad_default) {
                    $ad_default_sql = "ad_default = '$ad_default',";
                }

                $sql = " update {$g5['g5_shop_order_address_table']}
						  set {$ad_default_sql}
							  ad_subject = '$ad_subject',
							  ad_jibeon  = '$od_b_addr_jibeon'
						where mb_id = '{$member['mb_id']}'
						  and ad_id = '{$row['ad_id']}' ";
            } else {
                $sql = " insert into {$g5['g5_shop_order_address_table']}
						set mb_id       = '{$member['mb_id']}',
							ad_subject  = '$ad_subject',
							ad_default  = '$ad_default',
							ad_name     = '$od_b_name',
							ad_tel      = '$od_b_tel',
							ad_hp       = '$od_b_hp',
							ad_zip1     = '$od_b_zip1',
							ad_zip2     = '$od_b_zip2',
							ad_addr1    = '$od_b_addr1',
							ad_addr2    = '$od_b_addr2',
							ad_addr3    = '$od_b_addr3',
							ad_jibeon   = '$od_b_addr_jibeon' ";
            }

            sql_query($sql);
            /*************** 트랜잭션 관련 ****************/
            if (mysqli_errno($g5['connect_db'])) {
                $error_cnt += 1;
            }
            /*************** 트랜잭션 관련 ****************/
        }
    }

    //일반회원 첫주문 주소 업데이트
    if ($od_gubun == '3') {
        //주소정보 업데이트
        if ($member['mb_zip1'] == '') {

            $sql = " update {$g5['member_table']}
						  set mb_zip1	= '$od_zip1',
							  mb_zip2	= '$od_zip2',
							  mb_addr1  = '$od_addr1',
							  mb_addr2  = '$od_addr2',
							  mb_addr3  = '$od_addr3',
							  mb_addr_jibeon  = '$od_addr_jibeon'
						where mb_id = '{$member['mb_id']}'";
            sql_query($sql);
            /*************** 트랜잭션 관련 ****************/
            if (mysqli_errno($g5['connect_db'])) {
                $error_cnt += 1;
            }
            /*************** 트랜잭션 관련 ****************/
        }
    }


}

/* 밀리의서재 프로모션 5만원이상구매자 쿠폰발송 */
/*
if( $tot_ct_price >= 50000 ){
	$event_return = EventCouponSend($member["mb_id"],$member["mb_name"],$member["mb_hp"],"2");
}
*/

/*************** 트랜잭션 관련 ****************/
if ($error_cnt > 0) {
    mysqli_rollback($g5['connect_db']);
    mysqli_close($g5['connect_db']);
    echo "
	<script>
		alert('데이터베이스의 에러로 인해 롤백되었습니다.');
		history.back();
	</script>
	";
    exit;
} else {
    mysqli_commit($g5['connect_db']);
}
/*************** 트랜잭션 관련 ****************/

goto_url(G5_SHOP_URL . '/orderinquiryview.php?od_id=' . $od_id . '&amp;uid=' . $uid);
?>

<html>
<head>
    <title>주문정보 기록</title>
    <script>
        // 결제 중 새로고침 방지 샘플 스크립트 (중복결제 방지)
        function noRefresh() {
            /* CTRL + N키 막음. */
            if ((event.keyCode == 78) && (event.ctrlKey == true)) {
                event.keyCode = 0;
                return false;
            }
            /* F5 번키 막음. */
            if (event.keyCode == 116) {
                event.keyCode = 0;
                return false;
            }
        }

        document.onkeydown = noRefresh;
    </script>
</head>
</html>
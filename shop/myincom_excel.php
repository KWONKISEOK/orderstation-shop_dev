<?php
include_once('./_common.php');

define("_MYINCOM_", true);


//$ExcelTitle = urlencode( "개인소득지급관리" . date( "YmdHis" ) );

$ExcelTitle = iconv('utf-8', 'euc-kr', "개인소득지급관리_" . date( "YmdHis" ));

header( "Content-type: application/vnd.ms-excel" );
header( "Content-type: application/vnd.ms-excel; charset=utf-8" );
header( "Content-Disposition: attachment; filename = " . $ExcelTitle . ".xls" );
header( "Content-Description: PHP4 Generated Data" );
print( "<meta http-equiv=\"Content-Type\" content=\"application/vnd.ms-excel; charset=utf-8\">" ); 	


?>
	<table>
	<tr>
		<td><strong>실 지급액 : <?=$sum_total_incen?></strong></td>
	</tr>
	<table>
    <table border="1">
    <tr align="center">
        <td bgcolor="F7F7F7">주문일자</td>
        <td bgcolor="F7F7F7">주문번호</td>
		<td bgcolor="F7F7F7">고객명</td>
        <td bgcolor="F7F7F7">상품명</td>
        <td bgcolor="F7F7F7">출하가</td>
        <td bgcolor="F7F7F7">수량</td>
		<td bgcolor="F7F7F7">총금액</td>
		<td bgcolor="F7F7F7">포인트</td>
    </tr>
    <?php
    $sql = " select c.od_date1, a.od_id, a.od_name, b.it_id, b.it_name, b.od_option, b.od_qty, b.od_price, b.od_total_sale_price, b.od_total_incen, c.od_status
				  from tbl_shop_order a, tbl_shop_order_detail b, tbl_shop_order_receiver c
				where a.od_id = b.od_id 
				 and a.od_id = c.od_id 
				 and b.od_num = c.od_num 
				 and a.od_gubun = '3'
				 and a.pharm_custno = '{$member['pharm_custno']}'
				 and substr(c.od_date1, 1, 10) >= '$sdate'
				 and substr(c.od_date1, 1, 10) <= '$edate'
				 and c.od_seq = 1
				 and c.od_status = '완료'
				 and c.od_pay_yn = 'Y'  and b.it_id not in('S00603','S00604','S01756')
				  order by c.od_date1 desc
				 
				  ";

    $result = sql_query($sql);
    for ($i=0; $row=sql_fetch_array($result); $i++)
    {
	?>
    
    <tr align="left">
        <td><?php echo substr($row['od_date1'],0,10); ?></td>
		<td style="mso-number-format:'\@';"><?php echo $row['od_id']; ?></td>
		<td><?php echo $row['od_name']; ?></td>
		<td><?php echo $row['it_name']; ?></td>
		<td><?php echo $row['od_price']; ?></td>
		<td><?php echo $row['od_qty']; ?></td>
        <td><?php echo display_price($row['od_total_sale_price']); ?></td>
        <td><?php echo number_format($row['od_total_incen']); ?>P</td>
    </tr>
	<?php } ?>
    </table>
<?php
include_once('./_common.php');

if (G5_IS_MOBILE) {

    return;
}

?>
<style>

</style>
<div id="snb" class="snb_cc">
	<h2 class="tit_snb">마이페이지</h2>
	<div class="inner_snb">
		<ul class="list_menu">
			<li>
			<a href="/shop/orderinquiry.php">주문배송조회</a>
			</li>
			<li><a href="/shop/myauto.php">정기주문내역</a></li>
			<?php if ($member['mb_type'] == 1) { ?>
			
			<li >
				<a href="/shop/myuser.php">고객관리</a>
			</li>
			<li >
				<a href="/shop/myrecord.php">정산조회</a>
			</li>
			<li >
				<a href="/shop/mybank.php">개인소득정보관리</a>
			</li>
			<li >
				<a href="/shop/myincom.php">개인소득지급관리</a>
			</li>

			<?php } ?>
			<li >
				<a href="/shop/qalist.php">1:1 건강상담</a>
			</li>
			<li >
				<a href="/shop/wishlist.php">관심상품</a>
			</li>
			<li >
				<a href="<?php echo G5_BBS_URL; ?>/member_confirm.php?url=register_form.php">정보수정</a>
			</li>
			<? if( $member['mb_id'] == "admin" || $member['mb_id'] == "mynameisbryan" || $member['mb_id'] == "bdadmin" ){ ?>
			<li >
				<a href="https://www.geno-p.com/">검사 결과지 확인</a>
			</li>
			<? } ?>
			<?php if( ( $member['mb_type'] == 0 && !empty($member['mb_id']) ) || $member['mb_id'] == "admin" ) { ?>
			<li >
				<a href="/shop/coupon_reg.php">쿠폰 등록하기</a>
			</li>
			<!--
			<li >
				<a href="/shop/coupon_reg2.php">쿠폰 인증하기</a>
			</li>
			-->
			<?}?>
		</ul>
	</div>
</div>

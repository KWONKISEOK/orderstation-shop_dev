<?php
include_once('./_common.php');
include_once(G5_SHOP_PATH.'/settle_naverpay.inc.php');

$period = $_REQUEST[period];

// 보관기간이 지난 상품 삭제
//cart_item_clean();

// cart id 설정
set_cart_id($sw_direct);

$s_cart_id = get_session('ss_cart_id');
// 선택필드 초기화
$sql = " update {$g5['g5_shop_cart_table']} set ct_select = '0' where od_id = '$s_cart_id' ";
sql_query($sql);

$cart_action_url = G5_SHOP_URL.'/cartupdate.php';

if (G5_IS_MOBILE) {
    include_once(G5_MSHOP_PATH.'/cart.php');
    return;
}

// 테마에 cart.php 있으면 include
if(defined('G5_THEME_SHOP_PATH')) {
    $theme_cart_file = G5_THEME_SHOP_PATH.'/cart.php';
    if(is_file($theme_cart_file)) {
        include_once($theme_cart_file);
        return;
        unset($theme_cart_file);
    }
}

$g5['title'] = '장바구니';
include_once('./_head.php');


if(!G5_IS_MOBILE) {
	echo '<div class="menu-box" style="display:none;">';
	include_once(G5_SHOP_PATH.'/aside.php');
	echo '</div>';
	echo '<div class="site-wrap">';
	echo '<div id="containersub">';
}

$foot_qty		= 0; //총수량 소계
$foot_sendcost	= 0; //배송비 소계
$foot_LeftSum	= 0; //소비자가 소계
$foot_RightSum	= 0; //약국가 소계

if($member[mb_type] != 1) {
?>

<br />
<div class="xans-element- xans-order xans-order-tabinfo ec-base-tab  ">
<ul class="menu">
	<li class="<?php if($period != 'y') echo 'selected';?> "><a href="/shop/cart.php">일반주문 </a></li>
    <?php if ($is_member) { ?>
	<li class="<?php if($period == 'y') echo 'selected';?>"><a href="/shop/cart.php?period=y">정기주문</a></li>
    <?php } ?>
</ul>
</div>

<?php } ?>


<!-- 장바구니 시작 { -->
<script src="<?php echo G5_JS_URL; ?>/shop.js?ver=<?php echo G5_JS_VER; ?>"></script>
<script src="<?php echo G5_JS_URL; ?>/shop.override.js?ver=<?php echo G5_JS_VER; ?>"></script>
<style>
.mod_options_auto {
    padding: 0 7px;
    border: 1px solid #38b2b9;
    color: #38b2b9;
    background: #fff;
    height: 23px;
    margin: 4px 0 0;
}

</style>
<div id="sod_bsk" class="od_prd_list">

	<div class="xans-product-menupackage "><h2><span><?php echo $g5['title'] ?></span>&nbsp;&nbsp;
	<?php if($period == 'y') {?>
		<small >정기주문은 1개의 상품씩 주문 가능 합니다.</small>
	<?php } ?></h2>
	
	</div>
    <form name="frmcartlist" id="sod_bsk_list" class="2017_renewal_itemform" method="post" action="<?php echo $cart_action_url; ?>">

	<input type="hidden" name="comp_code_del"/>

	<div id="CartInfo">

	<?
		//웹 장바구니 공통 include 페이지
		include_once('./cart_common.php');	
	?>

	</div>

    <div id="sod_bsk_act">
        <?php if ($i == 0) { ?>
        <a href="<?php echo G5_SHOP_URL; ?>/" class="btn01">쇼핑 계속하기</a>
        <?php } else { ?>
        <input type="hidden" name="url" value="./orderform.php">
        <input type="hidden" name="records" value="<?php echo $i; ?>">
        <input type="hidden" name="act" value="">
		<input type="hidden" name="ct_period_yn" value="<?php echo $period;?>">
        <a href="<?php echo G5_SHOP_URL; ?>/list.php?ca_id=<?php echo $continue_ca_id; ?>" class="btn01">쇼핑 계속하기</a>
        <button type="button" onclick="return form_check('buy','');" class="btn_submit"><i class="fa fa-credit-card" aria-hidden="true"></i> 주문하기</button>

        <?php if ($naverpay_button_js) { ?>
        <div class="cart-naverpay"><?php echo $naverpay_request_js.$naverpay_button_js; ?></div>
        <?php } ?>
        <?php } ?>
    </div>

    </form>

</div>

<script>

var ClickComp = "";

$(function() {

	var close_btn_idx;

	// 선택사항수정
	$(document).on('click', '.mod_options',function(e) {
		var it_id = $(this).closest("tr").find("input[name^=it_id]").val();
		var $this = $(this);
		close_btn_idx = $(".mod_options").index($(this));

		$.post(
			"./cartoption.php",
			{ it_id: it_id },
			function(data) {
				$("#mod_option_frm").remove();
				$this.after("<div id=\"mod_option_frm\"></div>");
				$("#mod_option_frm").html(data);
				price_calculate();
				e.stopImmediatePropagation();
			}
		);
	});
	// 정기주문
	$(document).on('click', '.mod_options_auto',function(e) {
		var it_id = $(this).closest("tr").find("input[name^=it_id]").val();
		var $this = $(this);
		close_btn_idx = $(".mod_options_auto").index($(this));

		$.post(
			"./cartoption_auto.php",
			{ it_id: it_id },
			function(data) {
				$("#mod_option_frm").remove();
				$this.after("<div id=\"mod_option_frm\"></div>");
				$("#mod_option_frm").html(data);
				e.stopImmediatePropagation();
				//price_calculate();
			}
		);
	});

	// 옵션수정 닫기
	$(document).on("click", "#mod_option_close", function(e) {
		$("#mod_option_frm").remove();
		$(".mod_options").eq(close_btn_idx).focus();
		e.stopImmediatePropagation();
	});
	$(document).on("click", "#win_mask", function(e) {
		$("#mod_option_frm").remove();
		$(".mod_options").eq(close_btn_idx).focus();
		e.stopImmediatePropagation();
	});

});


function fsubmit_check(f) {
	if($("input[name^=ct_chk]:checked").length < 1) {
		alert("구매하실 상품을 하나이상 선택해 주십시오.");
		return false;
	}

	return true;
}

function form_check(act,comp_code) {
	var f = document.frmcartlist;
	var cnt = f.records.value;

	<?//if( $period == "y" && $member['mb_type'] == "0" ){?>
		//alert("정기주문은 모바일에서만 가능합니다.");
		//return false;
	<? //} ?>

	if (act == "buy")
	{
		if($("input[name^=ct_chk]:checked").length < 1) {
			alert("주문하실 상품을 하나이상 선택해 주십시오.");
			return false;
		}
		<?php if($member['mb_type'] != 1 and $period=='y') { ?>
		if($("input[name^=ct_chk]:checked").length > 1) {
			alert("정기주문은 한상품씩 주문가능합니다.");
			return false;
		}
		<?php } ?>

		var i=0;
		var send_yn = "Y";
		$("input[name^=ct_chk]:checked").each(function() {

			if( parseInt( $("#ct_chk_"+i).data( "change_yn" ) ) > 0 ){
				send_yn = "N";
			}
			i++;

		});

		if( send_yn == "N" ){
			alert("가격변동이 있는 상품이 있습니다.가격변동상품을 삭제후 다시 주문하십시요.");
			return false;
		}

		<? if( $TimeGoodsYn == "Y" ){ ?>

			alert("타임세일 상품은 세일 시간에만 구매 가능합니다.\n타임세일 상품을 삭제후 다시 주문하십시요.");
			return false;
		<? } ?>

		<? if( $item_Soldout == "Y" ){ ?>
			alert("품절된 상품이 있습니다. 삭제해주세요.");
			return false;		
		<? } ?>

		f.act.value = act;
		f.submit();
	}
	else if (act == "alldelete")
	{
		f.act.value = act;
		f.submit();
	}
	else if (act == "seldelete")
	{
		if($("input[name^=ct_chk]:checked").length < 1) {
			alert("삭제하실 상품을 하나이상 선택해 주십시오.");
			return false;
		}

		f.act.value = act;
		f.submit();
	}
	else if (act == "compdelete")
	{
		if($("input[name^=ct_chk]:checked").length < 1) {
			alert("삭제하실 상품을 하나이상 선택해 주십시오.");
			return false;
		}

		f.comp_code_del.value = comp_code;
		f.act.value = act;
		f.submit();
		f.comp_code_del.value = "";
	}
	else if (act == "pumdelete")
	{
		f.comp_code_del.value = comp_code;
		f.act.value = act;
		f.submit();
		f.comp_code_del.value = "";
	}

	return true;
}

function fnc_suselect2(str_gbn,obj) {
	if (str_gbn==0) {
		if (parseInt(document.getElementById(obj).value) > 1) {
			document.getElementById(obj).value = parseInt(document.getElementById(obj).value) -1;
		}
	} else {
		if (parseInt(document.getElementById(obj).value) < 5) {
			document.getElementById(obj).value = parseInt(document.getElementById(obj).value) +1;
		}
	}
}
	

var loopcnt=0;

//공급사 전체선택 트리거
$(".ct_all").trigger('click');

//공급사 상품선택 트리거
$("input[name^=ct_chk]").each(function() {

	$("#ct_chk_"+loopcnt).trigger('click');

	loopcnt++;

});

$(document).on('click', 'input[class^=ct_all]',function(e) {

	var i=0;
	ClickComp = $(this).data("comp");

	if( $(this).is(":checked") == true ){
	
		$("input[name^=ct_chk]").each(function() {

			if( $("#ct_chk_"+i).data("comp") == ClickComp ){
				$("#ct_chk_"+i).attr("checked", true);
			}
	
			i++;

		});

	}

	if( $(this).is(":checked") == false ){
	
		$("input[name^=ct_chk]").each(function() {

			if( $("#ct_chk_"+i).data("comp") == ClickComp ){
				$("#ct_chk_"+i).attr("checked", false);
			}
	
			i++;

		});

	}

});

$(document).on('click', 'input[class^=ct_all] , input[name^=ct_chk]',function(e) {

	var i=0;
	var it_id = "";
	var comp_code = "";

	//공급사선택값
	$("input[class^=ct_all]").each(function() {
		
		var ThisCompCode = $(this).data("comp");

		if( $(this).is(":checked") ){

			if( comp_code != "" ){
				comp_code += "|";
			}

			comp_code += ThisCompCode;

		}
	
	});

	//상품선택값
	$("input[name^=ct_chk]").each(function() {

		if( $("#ct_chk_"+i).is(":checked") ){

			if( it_id != "" ){
				it_id += "|";
			}

			it_id += $("#ct_chk_"+i).data( "it_id" );

		}

		i++;
	
	});
	
	$.ajax({
		url: "./cart_choice.php",
		type: 'POST',
		data: {
			'it_id': it_id,
			'comp_code': comp_code,
			'period': '<?=$period?>'
		},
		dataType: 'html',
		async: false,
		success: function(data, textStatus) {
			if (data.error) {
				alert(data.error);
				return false;
			} else {

				if( data == "" ){
					alert("다시 시도해주십시요.");
					location.reload();
				}else{
					
					$("#CartInfo").empty();
					$("#CartInfo").append(data);


					//이벤트의 기본 동작을 중단한다.
					e.stopImmediatePropagation();

				}

			}
		}
	});
	

});

//공급사 전체선택 트리거
$(".ct_all").trigger('click');

</script>
<!-- } 장바구니 끝 -->

<?php
echo '</div>';
include_once('./_tail.php');
?>
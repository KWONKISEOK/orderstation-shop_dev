
<?
add_stylesheet('<link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Noto+Sans+KR:wght@100;300;400;500;700;900&display=swap">', 0);
add_stylesheet('<link rel="stylesheet" href="'.G5_THEME_CSS_URL.'/cart_web.css">', 0);
?>

<div class="tbl_head03 tbl_wrap tbl_head03_test1 notof">

	<?php

	//배송비계산
	$tot_point = 0;
	$tot_sell_price = 0;
	$tot_sendcost_price = 0; //배송비총계
	$tot_left_price = 0; //소비자가총계
	$tot_right_price = 0; //약국가총계
	$tot_point_price = 0; //포인트 할인금액 총계

	//장바구니 검사
	$result = c_mysqli_call('SP_ORDER_INFO_NEW_VERSION', " '".$s_cart_id."','CART','".$member['mb_type']."','".$period."','','".$get_it_id."' ");

	$it_send_cost = 0;
	$one_trans = true;

	$i=0;
	$TmpString="";
	$LeftSum=0; // 소비자가 
	$RightSum=0; // 약국가
	$incen_sum=0;
	$trans_memo = "";
	$TimeGoodsYn = "N"; //타임세일 제품이 있는지 여부
	$item_Soldout ="N"; //품절인지 체크
	$soldout_cnt = 0; //품절상품 갯수 체크 

	foreach($result as $row) {
		// 합계금액 계산
		$sql = " select SUM(IF(io_type = 1, (io_price * ct_qty), ((ct_price + io_price) * ct_qty))) as price,
						SUM(ct_point * ct_qty) as point,
						SUM((select it_incen * a.ct_qty from tbl_shop_item where it_id = a.it_id))  as it_incen, 
						SUM((select io_incen * a.ct_qty from tbl_shop_item_option where io_id = a.io_id limit 1) ) as io_incen, SUM(ct_qty) as qty,
						SUM(ct_qty) as qty
					from {$g5['g5_shop_cart_table']} a
					where it_id = '{$row['it_id']}'
					  and od_id = '$s_cart_id' ";

					  
		$sum = sql_fetch($sql);

		if ($i==0) { // 계속쇼핑
			$continue_ca_id = $row['ca_id'];
		}

		$a1 = '<a href="./item.php?it_id='.$row['it_id'].'" class="prd_name"><b>';
		$a2 = '</b></a>';
		$image = get_it_image($row['it_id'], 80, 80);

		$it_name = $a1 . stripslashes($row['it_name']) . $a2;
		//구분자 | 기준으로 앞에는 옵션내용연결문자열 , 뒤에는 최종합계 약국가의 소계를 보여줄때 사용한다.
		$it_options = print_item_options_new($row['it_id'], $s_cart_id);
		$ct_period_yn = $row['ct_period_yn'];
		$period_cnt	=0;
		$period_cnt	= $row['ct_period_cnt'];
		if($it_options) {

			$TmpString=explode("|", $it_options);
			$LeftSum=$LeftSum+$TmpString[1];
			$RightSum=$RightSum+$TmpString[2];

			if($ct_period_yn == 'Y') {
				$mod_options = '<div class="sod_option_btn"><button type="button" class="mod_options bt_coltst">선택사항변경</button> <div class="sod_option_btn"><button type="button" class="mod_options_auto bt_coltst">정기주문수정</button></div> </div>';
			} else {
				$mod_options = '<div class="sod_option_btn"><button type="button" class="mod_options bt_coltst">선택사항변경</button></div>';
			}
			
			$it_name .= '<div class="sod_opt">'.$TmpString[0].'</div>';
		}

        // 소비자가
        $sql_price = "select it_id, it_cust_price, it_price, it_drug_price from tbl_shop_item where it_id = '$row[it_id]'";
        $re_price = sql_fetch($sql_price);

        $cust = '';
        $sale_yn = '';
        if($re_price['it_cust_price']>0){ // 상품할인이 있는 경우
            $sale_yn = 'y';
            $cust = $re_price['it_cust_price'] * $sum['qty'];
            $sale = ($cust - $LeftSum);
        } else{
            $sale = "0";
        }

        //$ct_period_yn = '<div class="sod_option_btn"><button type="button" class="mod_options_auto">정기주문</button></div>';

		// 배송비
		/*
		switch($row['ct_send_cost'])
		{
			case 1:
				$ct_send_cost = '착불';
				break;
			case 2:
				$ct_send_cost = '무료';
				break;
			default:
				$ct_send_cost = '주문시결제';
				break;
		}
		*/
		if( $row['cal_it_sc_price'] == 0 ){
			 $ct_send_cost = '무료';
		}else{
			 $ct_send_cost = '주문시결제';			
		}

		$sendcost = $row['cal_it_sc_price'];

		//배송비업데이트
		if($sendcost>=0) {
			$sql = " update tbl_shop_cart set it_sc_cost = '$sendcost' where ct_id='{$row['ct_id']}' ";
			$rtncost = sql_fetch($sql);
		}
		
		$total_cost = $total_cost + $sendcost;

		//정기주문 예외처리 상품
		$optionprice = get_option_dis_price_new($row['it_id'], $row['io_id'],$row['io_type']);	
		
		if($optionprice > 0) {
			$sell_price = $sum['qty'] * ($row['ct_price']+$optionprice);
		} else {
			if($period_cnt > 0) {
				$sell_price = $sum['price'] * $period_cnt;
				$incen_sum = ($sum['it_incen']+$sum['io_incen']) * $period_cnt;
			} else {
				$sell_price = $sum['price'];
				$incen_sum = $sum['it_incen']+$sum['io_incen'];
			}
		}

		/* 상품별 배송비 정책 노출 */
		if($row['it_sc_type'] != 1){

			if($row['it_sc_type'] =='2') $trans_memo = number_format($row['it_sc_price']).'원 (동일 판매자 상품 '.number_format($row['it_sc_minimum']).'원 이상 구매 시 무료)';
			if($row['it_sc_type'] =='3') $trans_memo = number_format($row['it_sc_price']).'원 (동일 판매자 상품 묶음배송 가능)';
			if($row['it_sc_type'] =='4') $trans_memo = number_format($row['it_sc_price']).'원 <br>(X개 단위로 배송비 ooo원이 추가 부과)';

		}

		if( $row['it_trans_bundle'] == "1" ){
			$trans_memo .= "&nbsp;[묶음배송상품]";
		}

	?>
	<div <? if( $row['total_comp_it_cnt'] == $row['row_num'] ){ ?>style="margin-bottom:30px;"<?}?> >


		<table style="text-align:center;">
			<? if( $row['row_num'] == "0" ){ ?>
			<thead>
				<tr>
					<th colspan="7" style="background-color:#f8f8f8;font-size:18px;font-weight:600;padding-left:10px;border-top:2px solid #a5a5a5; text-align:left;letter-spacing: 1px;">
						<a href="/shop/listcomp.php?comp=<?=$row['comp_code']?>"><?=$row['comp_name']?></a>
                        <!--
						<? if( $row['comp_trans_use'] == "Y" && $row['comp_trans_compare'] > 0  ){ ?>
						<span style="font-size:12px;font-weight:bold;padding-left:10px;">
							주문하시는 총 [묶음배송상품]의 합이 <?=number_format( $row['comp_trans_compare'] )?>원 이상시 묶음배송비 <?=number_format( $row['comp_trans_price'] )?>원 입니다.
						</span>
						<? } ?>
						-->
					</th>
				</tr>
			</thead>
			<? } ?>
			<tbody class="test_1">
			<? if( $row['row_num'] == "0" ){ ?>
				<tr>
					<th style="padding-left:5px;">
						<label class="container_w"> 
							<input type="checkbox" class="ct_all" id="ct_all_<?=$row['comp_code']?>" value="1" data-comp="<?=$row['comp_code']?>" <? if( strpos($get_comp_code,$row['comp_code']) !== false  ){ ?> checked="checked" <?}?>  >
							<span class="checkmark_w"></span>
						</label>
					</th>
					<th>상품 정보</th>
					<th>수량</th>
					<th>소비자가</th>
                    <th>할인</th>
                    <? if($member['mb_type']==1){ ?>
                    <th>약국 공급가</th>
                    <? } else { ?>
                    <th>상품금액</th>
                    <? } ?>
					<th>배송비</th>
				</tr>
			<? }
			?>
				<tr>
					<td style="text-align:left; width: 60px">
						<label class="container_w"> 
							<input type="checkbox" name="ct_chk[<?php echo $i; ?>]" value="1" id="ct_chk_<?php echo $i; ?>" <? if( strpos($get_it_id,$row['it_id']) !== false  ){ ?> checked="checked" <?}?> data-change_yn="<?=$row['price_change_yn_sum'];?>" data-it_id="<?=$row['it_id'];?>" data-comp="<?=$row['comp_code']?>">
							<span class="checkmark_w"></span>
						</label>
					</td>
					<td class="td_prd" style="text-align:left; width:550px;">
						<div class="item-detail" style="width: 550px;">
							<div class="sod_img"><a href="./item.php?it_id=<?php echo $row['it_id']; ?>"><?php echo $image; ?></a></div>
							<div class="sod_name" style="position: relative;width: auto;">
								<input type="hidden" name="it_id[<?php echo $i; ?>]" value="<?php echo $row['it_id']; ?>">
								<input type="hidden" name="it_name[<?php echo $i; ?>]" value="<?php echo get_text($row['it_name']); ?>">
								<input type="hidden" name="comp_code[<?php echo $i; ?>]" value="<?php echo $row['comp_code']; ?>">
								<?php echo $it_name; ?>
								<div class="desc">
									<?php echo $mod_options; ?>
									<span class="extra-cost"><?php echo $trans_memo; ?></span>
								</div>
							</div>
						</div>
					</td>
                    <!-- 수량 -->
					<td style="width: 40px;"><strong><?php echo number_format($sum['qty']); ?></strong></td>
                    <!-- 소비자가 -->
					<td style="width: 130px;">
						<strong>
						<?if( $row['price_change_yn_sum'] != 0 ){?>
							<strike>
						<?}?>	
							<?php echo number_format($LeftSum+$sale); ?>원
						<?if( $row['price_change_yn_sum'] != 0 ){?>
							</strike>
							<br> <font color='red'>가격변동 상품</font>
						<?}?>
						<? if( $row['it_type4'] == "1" && $time_sale_yn == "N" ){ ?>
							<br> <font color='blue'>타임세일 상품</font>
						<? } ?>
						<? if( $row['it_soldout'] == "1" || $row['it_use'] =="0"){ ?>
							<br> <font color='blue'>품절 상품</font>
							<? $soldout_cnt++; ?>
						<? } ?>
						</strong>
                    </td>
                    <? if($member['mb_type']==1){ ?>
                        <td style="width: 130px;">
                            <span>상품할인</br><?php if($re_price['it_cust_price']>0) {
                                echo "-"; echo number_format($sale);
                            } else{
                                echo "0";
                                }?>원</span></br></br>
                            <span>포인트</br>-<?php echo number_format($LeftSum-$RightSum); ?>원</span></td>
                        <td style="width: 130px;"><span class="tct_test1"><?php echo number_format($RightSum); ?>원</span></td>
                    <? } else{ ?>

                    <td style="width: 130px;">
                        <span>상품할인</br><?php if($re_price['it_cust_price']>0) {
                             echo "-"; echo number_format($sale);
                            } else{
                            echo "0";
                            }?>원</span>
                    </td>
                    <td style="width: 130px;"><span><?php echo number_format($LeftSum);?>원</span></td>
                    <? } ?>
                    <!-- 배송비 -->
					<td> 
						<strong>
						<? if( strpos($get_it_id,$row['it_id']) !== false ){ ?>		
							<?php echo $ct_send_cost; ?>
							<br/><?php if($sendcost>0) echo number_format($sendcost); ?>		
						<? }else{ ?>
							-
						<? } ?>							
						</strong>
					</td>
				</tr>
				<? 

				if( $row['row_num'] == "0" ){
					$foot_qty		= 0; //총수량
					$foot_sendcost	= 0; //배송비
					$foot_LeftSum	= 0; //소비자가
					$foot_RightSum	= 0; //약국가
					$foot_PointSum	= 0; //포인트 할인금액
                    $foot_Sale = 0; // 상품할인가
				}

				if( strpos($get_it_id,$row['it_id']) !== false ){
					$foot_qty += $sum['qty'];
					$foot_sendcost += $sendcost;
					$foot_LeftSum += $LeftSum;
					$foot_RightSum += $RightSum;
					$foot_PointSum += ( $LeftSum-$RightSum );
                    $foot_Sale += $sale;

					$tot_sendcost_price += $sendcost; 
					$tot_left_price += $LeftSum; 
					$tot_right_price += $RightSum;
					$tot_point_price += ( $LeftSum-$RightSum );
					$tot_sell_price += $sell_price;
					$tot_sale += $sale;
					$tot_cust_price += $LeftSum + $sale;
				}

				//총 리스트의 마지막 일때 처리
				if( $row['total_comp_it_cnt'] == $row['row_num'] ){  
				?>
				<tr style="background-color:#f8f8f8;" class="test_1_2">
					<td></td>
					<td></td>
					<td>
						<div><strong><?=$foot_qty?></strong></div> <!--수량-->
					</td>
					<td>
						<div><strong><?=number_format($foot_LeftSum+$foot_Sale)?>원</strong></div> <!--소비자가-->
					</td>

					<? if($member['mb_type']==1){ ?>
					<td>
						<div><strong>-<?=number_format($foot_PointSum+$foot_Sale)?>원</strong></div>
					</td>
					<td>
						<div><span class="tct_test1"><?=number_format($foot_RightSum)?>원</span></div>
					</td>
					<? } else { ?>
                        <td>
                            <div><strong>- <?=number_format($foot_Sale)?>원</strong></div><!--할인-->
                        </td>
                        <td>
                            <div><strong><?=number_format($foot_LeftSum)?>원</strong></div>
                        </td>
                    <? } ?>

					<td>
						<div><strong><?=number_format($foot_sendcost)?>원</strong></div>
					</td>
				</tr>
				<tr>
					<td colspan="7">
						<div class="btn_cart_del" style=" margin-bottom:30px;">
							<button type="button" onclick="return form_check('compdelete','<?=$row['comp_code']?>');" style="float:left;">선택 삭제</button>
							<? if( $soldout_cnt > 0 ){ ?>
							<button type="button" onclick="return form_check('pumdelete','<?=$row['comp_code']?>');" style="float:left;margin-left:3px;">품절 상품 삭제</button>
							<? $soldout_cnt = 0; ?>
							<? } ?>
							<a class="a_test1s">장바구니에 담긴 상품은 최대 15일간 보관됩니다.<span>!</span></a>
						</div>						
					</td>
				</tr>
				<? } ?>

			</tbody>
		</table>


	</div>

	<?php 

		$RightSum = 0;
		$LeftSum = 0;
		$incen_sum = 0;
		$trans_memo = "";
		$sale_yn = "";

		if( $row['it_type4'] == "1" && $time_sale_yn == "N" ){ 
			$TimeGoodsYn = "Y";
		}
		if( $row['it_soldout'] == "1" || $row['it_use'] =="0"){ 
			$item_Soldout = "Y";
		}

		$i++;
	} // for 끝

	if ($i == 0) {
		echo '<table style="text-align:center;"><tr><td colspan="8" class="empty_table">장바구니에 담긴 상품이 없습니다.</td></tr></table>';
	} else {
		// 배송비 계산
		///$send_cost = get_sendcost($s_cart_id, 0);
	}	

	?>

    <?php
    if ($tot_left_price > 0 || $tot_sendcost_price > 0) {
    ?>

	<div class="crt_ts_nb" style="text-align:center;">
		<table style="width:auto;text-align:center;margin:0 auto;border:0;">
			<tr>
				<td style="text-align:center;border:0;line-height:normal;">
					<div>
						<ul>
							<li>
								<div>
									<a><span>소비자가</span><br><strong><?=number_format($tot_cust_price);?>원</strong></a>
									<? if($member['mb_type']==1){ ?>
									<p> - </p>
									<a><span>할인</span><br><strong><?=number_format($tot_point_price+$tot_sale);?>원</strong></a>
									<p> = </p>
									<a><span>약국공급가</span><br> <strong><?=number_format($tot_right_price);?>원</strong></a>
									<? } else {?>
                                    <p> - </p>
                                    <a><span>할인</span><br><strong><?=number_format($tot_sale);?>원</strong></a>
                                    <p> = </p>
                                    <a><span>상품금액</span><br><strong><?=number_format($tot_left_price);?>원</strong></a>
                                    <? }?>
									<p class="col">+</p>
									<a><span>배송비</span><br><strong><?=number_format($tot_sendcost_price);?>원</strong></a>
								</div>
							</li>
							<li>
								<a>결제 예정 금액</a>
								<? if($member['mb_type']==1) { //약국회원일때 ?>
								<span><?=number_format($tot_right_price+$tot_sendcost_price);?>원</span>
								<?}else{?>
								<span><?=number_format($tot_left_price+$tot_sendcost_price);?>원</span>
								<?}?>
							</li>
						</ul>
					</div>
				</td>
			</tr>
		</table>
	</div>

	<?php } ?>


</div>

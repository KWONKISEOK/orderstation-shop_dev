<?php
include_once('./_common.php');

//달콩 이벤트세트 구매한 회원 쿠폰 발행해주기;
//EventDalcongCoupon($member['mb_name'],$member['mb_id'],$member['mb_type']);

if (!$is_member)
    goto_url(G5_BBS_URL."/login.php?url=".urlencode(G5_SHOP_URL."/mypage.php"));

if (G5_IS_MOBILE) {
    include_once(G5_MSHOP_PATH.'/mypage.php');
    return;
}

// 테마에 mypage.php 있으면 include
if(defined('G5_THEME_SHOP_PATH')) {
    $theme_mypage_file = G5_THEME_SHOP_PATH.'/mypage.php';
    if(is_file($theme_mypage_file)) {
        include_once($theme_mypage_file);
        return;
        unset($theme_mypage_file);
    }
}

$g5['title'] = '마이페이지';
include_once('./_head.php');


if(!G5_IS_MOBILE) {
	echo '<div class="menu-box" style="display:none;">';
	include_once(G5_SHOP_PATH.'/aside.php');
	echo '</div>';
	echo '<div class="site-wrap">
	<div id="aside">&nbsp;';
	include_once(G5_SHOP_PATH.'/asidemy.php');
	echo '</div><div id="container">';
}

if ((!$bo_table || $w == 's' ) && !defined('_INDEX_')) { ?><div id="wrapper_title">&nbsp;</div><?php } 

// 쿠폰
$cp_count = 0;
if($member['mb_type'] == 0) {
    $sql = " select cp_id
            from {$g5['g5_shop_coupon_table']}
            where mb_id IN ( '{$member['mb_id']}', '소비자회원', '전체회원' )
              and cp_start <= '".G5_TIME_YMD."'
              and cp_end >= '".G5_TIME_YMD."' and service_type is null ";
} else {
    $sql = " select cp_id
            from {$g5['g5_shop_coupon_table']}
            where mb_id IN ( '{$member['mb_id']}')
              and cp_start <= '".G5_TIME_YMD."'
              and cp_end >= '".G5_TIME_YMD."' and service_type is null ";
}
$res = sql_query($sql);

for($k=0; $cp=sql_fetch_array($res); $k++) {
    if(!is_used_coupon($member['mb_id'], $cp['cp_id']))
        $cp_count++;
}
$sql = " select pharm_name, mb_name
            from {$g5['member_table']}
            where mb_id = '{$member['mb_recommend']}' ";
$pharm = sql_fetch($sql);

?>
<h1 style="height:32px;">&nbsp;</h1>
<!-- 마이페이지 시작 { -->
<div id="smb_my">

    <!-- 회원정보 개요 시작 { -->
    <section id="smb_my_ov">
        <h2>회원정보 개요</h2>
        <strong class="my_ov_name"><img src="<?php echo G5_THEME_IMG_URL ;?>/no_profile.gif" alt="프로필이미지"> <?php echo $member['mb_name']; ?>
		<?php if($member['mb_type']==1) echo '('. $member['pharm_name'].')';?>
		</strong>
        <dl class="cou_pt">
        <?php
        if (in_array($member['mb_id'], $TempIdArray)) { ?>
			<dt>보유포인트</dt>
            <dd><a href="<?php echo G5_BBS_URL; ?>/point.php" target="_blank" class="win_point"><?php echo number_format(point_expire_check($member['mb_id'])); ?></a> 점</dd>
		<?php }?>

        <?php
        if ($member['mb_referee']==4 || $member['mb_extra']==11) { ?>
            <dt>보유적립금</dt>
            <dd><a href="<?php echo G5_BBS_URL; ?>/cash.php" target="_blank" class="win_cash"><?php echo number_format(cash_expire_check($member['mb_id'])); ?></a> 원</dd>
        <?php }?>
            <dt>보유쿠폰</dt>
            <dd><a href="<?php echo G5_SHOP_URL; ?>/coupon.php" target="_blank" class="win_coupon">쿠폰보기 [ <?php echo number_format($cp_count); ?> ] </a></dd>
        </dl>
        <div id="smb_my_act">
            <ul>
                <?php if ($is_admin == 'super') { ?><li><a href="<?php echo G5_ADMIN_URL; ?>/" class="btn_admin">관리자</a></li><?php } ?>
                <li><a href="<?php echo G5_BBS_URL; ?>/memo.php" target="_blank" class="win_memo btn01">쪽지함</a></li>
                <li><a href="<?php echo G5_BBS_URL; ?>/member_confirm.php?url=register_form.php" class="btn01">회원정보수정</a></li>
                <li><a href="<?php echo G5_BBS_URL; ?>/member_confirm.php?url=member_leave.php" onclick="return member_leave();" class="btn01">회원탈퇴</a></li>
            </ul>
        </div>

        <dl class="op_area">
			<?php if($member['mb_type']==0) { ?>
			<dt>나의추천약사</dt>
            <dd><span><?php echo $pharm['pharm_name'];?></span>  &nbsp; <span><?php echo $pharm['mb_name'];?></span> 약사님</dd>
			<?php } ?>

            <dt>연락처</dt>
            <dd><?php echo ($member['mb_hp'] ? $member['mb_hp'] : '미등록'); ?></dd>
            <dt>E-Mail</dt>
            <dd><?php echo ($member['mb_email'] ? $member['mb_email'] : '미등록'); ?></dd>
            <dt>최종접속일시</dt>
            <dd><?php echo $member['mb_today_login']; ?></dd>
            <dt>회원가입일시</dt>
            <dd><?php echo $member['mb_datetime']; ?></dd>
            <dt id="smb_my_ovaddt">주소</dt>
            <dd id="smb_my_ovaddd">
			<?php echo sprintf("%s%s ", $member['mb_zip1'], $member['mb_zip2']).' '.print_address($member['mb_addr1'], $member['mb_addr2'], $member['mb_addr3'], $member['mb_addr_jibeon']); ?></dd>
        </dl>
        <div class="my_ov_btn"><button type="button" class="btn_op_area"><i class="fa fa-caret-up" aria-hidden="true"></i><span class="sound_only">상세정보 보기</span></button></div>

    </section>
    <script>
    
        $(".btn_op_area").on("click", function() {
            $(".op_area").toggle();
            $(".fa-caret-up").toggleClass("fa-caret-down")
        });

    </script>
    <!-- } 회원정보 개요 끝 -->

    <!-- 최근 주문내역 시작 { -->
    <section id="smb_my_od">
        <h2>최근 주문내역</h2>
        <?php
        // 최근 주문내역
        define("_MYPAGE_", true);

        //$limit = " limit 0, 5 ";
        
        ?>

		<!-- 주문 내역 목록 시작 { -->
		<!--<?php// if (!$limit) { ?>총 <?php// echo $cnt; ?> 건<?php// } ?>-->

		<div class="tbl_head03 tbl_wrap">
			<table>
			<thead>
			<tr>
				<th scope="col">주문서번호</th>
				<th scope="col" >상품명</th>
				<th scope="col">배송지</th>
				<th scope="col">수량</th>
				<th scope="col">소비자가</th>
				<?php if($member['mb_type']=='1') {?>
                <th scope="col">공급가</th>
				<th scope="col">포인트</th>
				<?php } ?>
				<th scope="col">상태</th>
			</tr>
			</thead>
			<tbody>
			<?php

			$sql2 = " SELECT SUM(od_seq) AS od_seq FROM(			  		  
						SELECT c.od_id , max(b.od_period_yn) AS od_period_yn , 
						case when max(b.od_period_yn = 'Y') then MAX(c.od_seq) ELSE SUM(c.od_seq) END AS od_seq ,
						( 
							SELECT 
							case when count(od_id) > 0 then 1 else 0 end FROM tbl_schedule_history 
							WHERE mb_id = 'infoeyeplus' AND od_id = a.od_id and res_cd != '00' 
						) AS order_num
						from tbl_shop_order a 
						inner join tbl_shop_order_detail as b ON a.od_id = b.od_id 
						inner join tbl_shop_order_receiver AS c ON b.od_id = c.od_id AND b.od_num = c.od_num where a.mb_id = '{$member['mb_id']}' 
						GROUP BY c.od_id  order by order_num DESC ,  a.od_id desc limit 0, 5
					) AS X ";
			
			$sum_result = sql_fetch($sql2);


			$limit = "limit 0 , ".$sum_result[od_seq];

			/*
			$sql = " select a.od_id, a.od_time, a.od_ip, a.od_misu, a.od_gubun, a.od_cart_count,
							b.it_id, b.it_name, b.od_option, c.od_qty, b.od_price, b.od_total_incen, c.od_status , c.od_pay_yn , 
							(select count(*) cnt from tbl_shop_order_receiver where od_id = a.od_id) od_count , 
							b.od_period_cnt , b.od_total_sale_price
					    from {$g5['g5_shop_order_table']} a inner join tbl_shop_order_detail as b ON a.od_id = b.od_id 
					   									   inner join tbl_shop_order_receiver AS c ON b.od_id = c.od_id AND b.od_num = c.od_num
					    where a.mb_id = '{$member['mb_id']}' {$service_type_and_query_a}
					    order by a.od_id desc $limit";
			*/

			//미결제건이 상단에 뜨도록
			$sql = "
			
					SELECT 
							X.* , 
							case when error_cnt > 0 then 1 ELSE 0 END AS order_num
					 FROM(
						 select a.od_id, a.od_time, a.od_ip, a.od_misu, a.od_gubun, a.od_cart_count, a.od_b_addr1, a.od_b_addr2, a.od_b_addr3, a.od_b_name,
							b.it_id, b.it_name, b.od_option, c.od_qty, b.od_price, b.od_total_incen, c.od_status , c.od_pay_yn , 
							(select count(*) cnt from tbl_shop_order_receiver where od_id = a.od_id) od_count , 
							b.od_period_cnt , b.od_total_sale_price , b.od_total_drug_price, 
							( SELECT count(od_id) FROM tbl_schedule_history WHERE mb_id = '{$member['mb_id']}' AND od_id = a.od_id and res_cd != '00' ) AS error_cnt , 
							ifnull( d.res_cd , '' ) as res_cd , ifnull( d.res_msg , '' ) as res_msg
					    from tbl_shop_order a inner join tbl_shop_order_detail as b ON a.od_id = b.od_id 
					   									   inner join tbl_shop_order_receiver AS c ON b.od_id = c.od_id AND b.od_num = c.od_num
					   									   left join tbl_schedule_history as d ON c.od_id = d.od_id AND c.od_num = d.od_num and c.od_seq = d.od_seq
					    where a.mb_id = '{$member['mb_id']}'
					) AS X order BY order_num DESC , x.od_id desc $limit	
					
			";

			$result = sql_query($sql);
	
			for ($i=0; $row=sql_fetch_array($result); $i++)
			{
				$uid = md5($row['od_id'].$row['od_time'].$row['od_ip']);

				$od_id = $row['od_id'];

				//미결제건
				if( $row['order_num'] == 1 ){
					$not_pay = '<span class="status_01">미결제건</span>';
				}
				
				if($member['mb_type'] == 1) {
					switch($row['od_gubun']) {
						case '1':
							$od_gubun = '<span class="status_01">고객주문</span>';
							break;
						case '2':
							$od_gubun = '<span class="status_02">약국주문</span>';
							break;
						case '3':
							$od_gubun = '<span class="status_03">추천고객</span>';
							break;
						
					}
				}
				switch($row['od_status']) {
					case '주문':
						$od_status = '<span class="status_01">입금확인중</span>';
						break;
					case '확정':
						$od_status = '<span class="status_01">입금확인중</span>';
						break;
					case '입금':
						$od_status = '<span class="status_02">입금완료</span>';
						break;
					case '준비':
						$od_status = '<span class="status_03">상품준비중</span>';
						break;
					case '배송':
						$od_status = '<span class="status_04">상품배송</span>';
						break;
					case '완료':
						$od_status = '<span class="status_05">배송완료</span>';
						break;
					case '반품접수':
						$od_status = '<span class="status_05">반품접수</span>';
						break;
					case '반품승인':
						$od_status = '<span class="status_05">반품접수</span>';
						break;
					case '반품반려':
						$od_status = '<span class="status_05">반품반려</span>';
						break;
					case '반품완료':
						$od_status = '<span class="status_05">반품완료</span>';
						break;
					default:
						$od_status = '<span class="status_06">주문취소</span>';
						break;
				}

				if( $row['od_status'] == "주문" && $row['od_pay_yn'] == "Y" ){
					$od_status = '<span class="status_02">입금완료</span>';
				}
			
				$od_id_count = $row["od_count"];

				$rowspan = true;
				if ($tmp_od_id == $od_id) {
					$rowspan = false;
				}
				$tmp_od_id = $od_id;

				/*5개 일때 rowspan 하나로 만들기*/
				/*
				if( $i == 4 ){
					$od_id_count = "1";
				}
				*/

			?>

			<tr>
				<?php  if ($rowspan) { ?>
				<td rowspan="<?php echo $od_id_count?>" style="vertical-align: inherit;">
					<input type="hidden" name="ct_id[<?php echo $i; ?>]" value="<?php echo $row['ct_id']; ?>">
					<? if( $row['res_cd'] != '00' && !empty($row['res_cd']) ){ ?>
					<?=$not_pay?> <br>
					<? } ?>
					<?php echo $od_gubun;?><br/>
					<a href="<?php echo G5_SHOP_URL; ?>/orderinquiryview2.php?od_id=<?php echo $row['od_id']; ?>&amp;uid=<?php echo $uid; ?>"><?php echo $row['od_id']; ?></a><br>
					<?php echo substr($row['od_time'],2,14); ?> (<?php echo get_yoil($row['od_time']); ?>)
				</td>
				<?php  } ?>

				<td class=" text_left">
					<?php echo $row['it_name']; ?>
					<? if( $row['res_cd'] != '00' && !empty($row['res_cd']) ){ ?>
						<span class="status_01">결제오류</span>
					<? } ?>
					<?php if($row['od_option']) echo '<br>'.$row['od_option']; ?>
				</td>
                <!--배송지-->
				<td class="text_left"><?php
                    echo $row['od_b_name']; ?><br>
                    <?php echo $row['od_b_addr1'];?><br>
                    <?php echo $row['od_b_addr2'];echo $row['od_b_addr3']; ?>
                </td>
				<td class="td_numbig"><?php echo $row['od_qty']; ?></td>
				<td class="td_numbig">
                    <?//php echo display_price($row['od_price']); ?>
                    <?php
                    if($row[od_period_cnt] > 0) {
                        $subprice= $row['od_total_sale_price'] / $row[od_period_cnt];
                        $subincen= $row['od_total_incen'] / $row[od_period_cnt];
                    } else {
                        $subprice=$row['od_total_sale_price'];
                        $subincen= $row['od_total_incen'] ;
                    }
                        echo number_format($subprice);
                    ?>
				</td>
				<?php if($member['mb_type']=='1') {?>
                <td class="td_numbig">
                    <?php
                    if($row[od_period_cnt] > 0) {
                        $subprice= $row['od_total_drug_price'] / $row[od_period_cnt];
                        $subincen= $row['od_total_incen'] / $row[od_period_cnt];
                    } else {
                        $subprice=$row['od_total_drug_price'];
                        $subincen= $row['od_total_incen'];
                    }
                    echo number_format($subprice);
                    ?>
                </td>
				<td class="td_numbig text_right"><?php echo number_format($subincen); ?>P</td>
				<?php } ?>
				<td><?php echo $od_status; ?></td>
			</tr>

			<?php
			}

			if ($i == 0)
				echo '<tr><td colspan="7" class="empty_table">주문 내역이 없습니다.</td></tr>';
			?>
			</tbody>
			</table>
		</div>
		<!-- } 주문 내역 목록 끝 -->



        <div class="smb_my_more">
            <a href="./orderinquiry.php">더보기</a>
        </div>
    </section>
    <!-- } 최근 주문내역 끝 -->

    <!-- 최근 위시리스트 시작 { -->
    <section id="smb_my_wish">
        <h2>최근 위시리스트</h2>

        <div class="list_02">
            <ul>

            <?php
            $sql = " select *
                       from {$g5['g5_shop_wish_table']} a,
                            {$g5['g5_shop_item_table']} b
                      where a.mb_id = '{$member['mb_id']}'
                        and a.it_id  = b.it_id
                      order by a.wi_id desc
                      limit 0, 8 ";
            $result = sql_query($sql);
            for ($i=0; $row = sql_fetch_array($result); $i++)
            {
                $image = get_it_image($row['it_id'], 230, 230, true);
            ?>

            <li>
                <div class="smb_my_img"><?php echo $image; ?></div>
                <div class="smb_my_tit"><a href="./item.php?it_id=<?php echo $row['it_id']; ?>"><?php echo stripslashes($row['it_name']); ?></a></div>
                <div class="smb_my_date"><?php echo $row['wi_time']; ?></div>
            </li>

            <?php
            }

            if ($i == 0)
                echo '<li class="empty_li">보관 내역이 없습니다.</li>';
            ?>
            </ul>
        </div>

        <div class="smb_my_more">
            <a href="./wishlist.php">더보기</a>
        </div>
    </section>
    <!-- } 최근 위시리스트 끝 -->

</div>

<script>
$(function() {
    $(".win_coupon").click(function() {
        var new_win = window.open($(this).attr("href"), "win_coupon", "left=100,top=100,width=700, height=600, scrollbars=1");
        new_win.focus();
        return false;
    });
});

function member_leave()
{
    return confirm('정말 회원에서 탈퇴 하시겠습니까?')
}
</script>
<!-- } 마이페이지 끝 -->

<?php
include_once("./_tail.php");
?>
<?php
include_once('./_common.php');


//$g5['title'] = '고객리스트';
include_once(G5_PATH.'/head.sub.php');


$sido	= $_POST['sido'];
$gugun	= $_POST['gugun'];

$dong	= $_POST['dong'];
$txt_word	= $_POST['txt_word'];




?>
<h2>추천약사선택</h2>
<!-- 장바구니 옵션 시작 { -->
<form name="foption" method="post">
<input type="hidden" name="act" value="auto_mod">
<input type="hidden" name="ct_id" value="<?php echo $row['ct_id']; ?>">

<input type="hidden" name="sw_direct">

<section style="padding:10px;">


<div class="pop_contents">
			<div class="pm_search_w">
			
				<p class="pm_search01">
					<select name="sido" id="sido" onChange="onChangeAddr(1)" class="pm_slect01">
						<option value="">시/도</option>
							<option value="강원도" <?php if($sido=='강원도') echo 'selected';?> >강원도</option>
							<option value="경기도" <?php if($sido=='경기도') echo 'selected';?> >경기도</option>						
							<option value="경상남도" <?php if($sido=='경상남도') echo 'selected';?> >경상남도</option>						
							<option value="경상북도" <?php if($sido=='경상북도') echo 'selected';?> >경상북도</option>
							<option value="광주광역시" <?php if($sido=='광주광역시') echo 'selected';?> >광주광역시</option>
							<option value="대구광역시" <?php if($sido=='대구광역시') echo 'selected';?> >대구광역시</option>
							<option value="대전광역시" <?php if($sido=='대전광역시') echo 'selected';?> >대전광역시</option>
							<option value="부산광역시" <?php if($sido=='부산광역시') echo 'selected';?> >부산광역시</option>
							<option value="서울특별시" <?php if($sido=='서울특별시') echo 'selected';?> >서울특별시</option>
							<option value="세종특별자치시" <?php if($sido=='세종특별자치시') echo 'selected';?> >세종특별자치시</option>
							<option value="울산광역시" <?php if($sido=='울산광역시') echo 'selected';?> >울산광역시</option>
							<option value="인천광역시" <?php if($sido=='인천광역시') echo 'selected';?> >인천광역시</option>
							<option value="전라남도" <?php if($sido=='전라남도') echo 'selected';?> >전라남도</option>
							<option value="전라북도" <?php if($sido=='전라북도') echo 'selected';?> >전라북도</option>
							<option value="제주특별자치도" <?php if($sido=='제주특별자치도') echo 'selected';?> >제주특별자치도</option>
							<option value="충청남도" <?php if($sido=='충청남도') echo 'selected';?> >충청남도</option>
							<option value="충청북도" <?php if($sido=='충청북도') echo 'selected';?> >충청북도</option>
					</select>
					
		            <select name="gugun" id="gugun" onChange="onChangeAddr(2)" class="pm_slect02">
		              <option value="">시/군/구</option>
		            </select>
		            
		            <!-- <select name="dong" id="dong" class="pm_slect03">
		              <option value="">동/면/읍</option>
		            </select> -->
                    <input type="text" name="txt_word" value="<?php echo $txt_word;?>" style="width:70px;" />
					<a href="Javascript:fnc_search();" class="recommend">검색하기</a>
				</p>

			</div>

			
			<div  class="list_scroll mt20" style="height:200px;">
				<table class="pharmacy_list ">
					<colgroup>
						<col/>
						<col width="" />
					</colgroup>
					<thead>
						<tr>
							<th>약사명</th>
							<th>주소</th>
						</tr>
					</thead>
					<tbody>
						<?php
							if($sido=='' and $txt_word=='') $sido='NONE';
							$sql = " select * from {$g5['member_table']} where mb_type=1";
							//$sql .= " 			  and pharm_sido = '$sido'";
							if($sido != '') {
								$sql .= " 			  and pharm_sido = '$sido'";
							}
							if($gugun != '') {
								$sql .= " 			  and pharm_gugun = '$gugun'";
							}
							if($dong != '') {
								$sql .= " 			  and pharm_dong = '$dong'";
							}
							if($txt_word != '') {
								$sql .= " 			  and (pharm_name like '%$txt_word%' or mb_name like '%$txt_word%')";
							}
//echo $sql;

							$result = sql_query($sql);

							for($i=0; $row=sql_fetch_array($result); $i++) {
						?>
						 
						<tr>
							<td><?php echo $row['pharm_name'].' '. $row['mb_name'];?></td>
							<td>
								
									<?php echo $row['mb_addr1'].' '. $row['mb_addr2'];?><br/>
									<a href="javascript:RowDClick('<?php echo $row['mb_id'];?>','<?php echo $row['pharm_name'];?>');" class="recommend">선택</a>
							
							</td>
						</tr>
						<?php } ?>
						
						
					</tbody>
				</table>

			</div>
		
			<!-- <p class="btn_w mt10"><a href="#" onClick="parent.hide_box();" class="btn btn_bk ">닫기</a></p> -->

		</div>





</section>


<div class="btn_confirm">
    <!-- <input type="submit" value="정기주문신청" class="btn_submit"> -->
    <button type="button" id="mod_option_close" class="btn_close"><i class="fa fa-times" aria-hidden="true"></i><span class="sound_only">닫기</span></button>
</div>
</form>
<script>
onChangeAddr(1,'');
		 setTimeout(function(){ onChangeAddr(2,'');}, 100); 
function onChangeAddr(step, val){
 		var sido = $("#sido");
 		var gugun = $("#gugun");
 		var dong = $("#dong");
 		var obj = "";
 
 		if(step == 1){
  			obj = gugun;
  			gugun.empty().data('option');
  			gugun.append("<option value=''>시/군/구</option>");
 		}
 		if(step == 2){
  			obj = dong; 
 		}
 		dong.empty().data('option');
 		dong.append("<option value=''>동/면/읍</option>");
 		$.ajax({
  			type:"post",
		  	dataType:"xml",
		  	url: "/addr.php",
		  	data:{"mem_sido":sido.val(),"mem_gugun":gugun.val(), "step":step},
		  	success:function(data){
   				var row= $(data).find("ROW").find("CELL");    
   				var size=row.length;
   				for(var i=0;i<size;i++){
    				var cell =row.eq(i);
    				$("<option></option>").text(cell.find("NAME").text()).attr("value",cell.find("NAME").text()).appendTo(obj);
   				}
   
			   	if(step==1){
			    	gugun.val(val);
			   	}
			   	if(step==2){
			    	dong.val(val);
			   	}
  			},
  			error:function(request, status, errorThrown){
  				//alert(errorThrown);
  			}
 		});
	}
</script>
<?php
include_once('./_common.php');

if (!$is_member) {
    alert_close("사용후기는 회원만 작성이 가능합니다.");
}

$it_id       = trim($_REQUEST['it_id']);
$od_id       = trim($_REQUEST['od_id']);
$is_subject  = trim($_POST['is_subject']);
$is_content  = trim($_POST['is_content']);
$is_content = preg_replace('#<script(.*?)>(.*?)</script>#is', '', $is_content);
$is_name     = trim($_POST['is_name']);
$is_password = trim($_POST['is_password']);
$is_score    = (int)$_POST['is_score'] > 5 ? 0 : (int)$_POST['is_score'];
$get_editor_img_mode = $config['cf_editor'] ? false : true;
$is_id       = (int) trim($_REQUEST['is_id']);

// 사용후기 작성 설정에 따른 체크
check_itemuse_write($it_id, $member['mb_id']);

if ($w == "" || $w == "u") {
    $is_name     = addslashes(strip_tags($member['mb_name']));
    $is_password = $member['mb_password'];

    if (!$is_subject) alert("제목을 입력하여 주십시오.");
    if (!$is_content) alert("내용을 입력하여 주십시오.");
}

if($is_mobile_shop)
    $url = './item.php?it_id='.$it_id.'&info=use';
else
    $url = "./item.php?it_id=$it_id&_=".get_token()."#sit_use";


/*************** 트랜잭션 관련 ****************/
$error_cnt = 0;
mysqli_autocommit($g5['connect_db'], false);
/*************** 트랜잭션 관련 ****************/
$it_img_dir = G5_DATA_PATH.'/itemuse';

$sql = " select is_image
                from {$g5['g5_shop_item_use']}
                where it_id = '$it_id' and od_id = '$od_id' ";
$use = sql_fetch($sql);

$is_image = $use['is_image'];

if ($_FILES['is_image']['name']) {
    if($w == 'u' && $is_image) {
        $is_image = $it_img_dir.'/'.$is_image;
        @unlink($is_image);
        delete_item_thumbnail(dirname($is_image), basename($is_image));
    }
    $is_image = it_use_upload($_FILES['is_image']['tmp_name'], $_FILES['is_image']['name'], $it_img_dir.'/'.$it_id);
}

if ($w == "")
{
    /*
    $sql = " select max(is_id) as max_is_id from {$g5['g5_shop_item_use_table']} ";
    $row = sql_fetch($sql);
    $max_is_id = $row['max_is_id'];

    $sql = " select max(is_id) as max_is_id from {$g5['g5_shop_item_use_table']} where it_id = '$it_id' and mb_id = '{$member['mb_id']}' ";
    $row = sql_fetch($sql);
    if ($row['max_is_id'] && $row['max_is_id'] == $max_is_id)
        alert("같은 상품에 대하여 계속해서 평가하실 수 없습니다.");
    */

    $sql = "insert {$g5['g5_shop_item_use_table']}
               set od_id = '$od_id',
				   it_id = '$it_id',
                   mb_id = '{$member['mb_id']}',
                   is_score = '$is_score',
                   is_name = '$is_name',
                   is_password = '$is_password',
                   is_subject = '$is_subject',
                   is_content = '$is_content',
                   is_image = '$is_image',
                   is_time = '".G5_TIME_YMDHIS."',
                   is_ip = '{$_SERVER['REMOTE_ADDR']}' ";
    if (!$default['de_item_use_use'])
        $sql .= ", is_confirm = '1' ";
    sql_query($sql);
	/*************** 트랜잭션 관련 ****************/
	if( mysqli_errno($g5['connect_db']) ){
		$error_cnt += 1;
	}
	/*************** 트랜잭션 관련 ****************/

    if ($default['de_item_use_use']) {
        $alert_msg = "평가하신 글은 관리자가 확인한 후에 출력됩니다.";
    }  else {
        $alert_msg = "사용후기가 등록 되었습니다.";
    }
	
	include_once(G5_LIB_PATH.'/kakao.alimtalk.lib.php');
	$mb_name = $member['mb_name'];
	$mb_hp = $member['mb_hp'];
	$recv_number = preg_replace('/[^0-9]/', '', $mb_hp);

	/*************** 트랜잭션 관련 insert의 오류가 없다면 쿠폰,알림톡 실행 ****************/
	if( $error_cnt == 0 && $member['mb_type'] != "1"){
		//해당 주문번호와 상품번호로 이미 쿠폰 받은 내역이 있는지 확인 후 발급진행 및 알림톡 실행
				$sql4 = " select count(*) as cnt from {$g5['g5_shop_coupon_table']} where od_id = '$od_id' and it_id='$it_id' and service_type is null ";
				$row4 = sql_fetch($sql4);
		if($row4[cnt] == 0){	
			// 사용후구기 1000원 쿠폰발생 7일내 사용가능
			if($mb_name) {
				$j = 0;
				$create_coupon = false;

				do {
					$cp_id = get_coupon_id();

					$sql3 = " select count(*) as cnt from {$g5['g5_shop_coupon_table']} where cp_id = '$cp_id' ";
					$row3 = sql_fetch($sql3);

					if(!$row3['cnt']) {
						$create_coupon = true;
						break;
					} else {
						if($j > 20)
							break;
					}
				} while(1);

				if($create_coupon) {
					$mb_id = $member['mb_id'];
					$cp_subject = '사용후기등록 쿠폰';
					$cp_method = 2;
					$cp_target = '';
					$cp_start = G5_TIME_YMD;
					if( date("Ymd") >= "20191101" ){
						$cp_end = date("Y-m-d", (G5_SERVER_TIME + (86400 * 30)));
					}else{
						$cp_end = date("Y-m-d", (G5_SERVER_TIME + (86400 * 7)));				
					}
					$cp_type = 0;
					$cp_price = 1000;//$default['de_member_reg_coupon_price'];
					$cp_trunc = 1;
					$cp_minimum = 0;//$default['de_member_reg_coupon_minimum'];
					$cp_maximum = 0;

					$sql = " INSERT INTO {$g5['g5_shop_coupon_table']}
								( cp_id, cp_subject, cp_method, cp_target, mb_id, cp_start, cp_end, cp_type, cp_price, cp_trunc, cp_minimum, cp_maximum, cp_datetime, od_id, it_id)
							VALUES
								( '$cp_id', '$cp_subject', '$cp_method', '$cp_target', '$mb_id', '$cp_start', '$cp_end', '$cp_type', '$cp_price', '$cp_trunc', '$cp_minimum', '$cp_maximum', '".G5_TIME_YMDHIS."', '$od_id' ,'$it_id' ) ";

					$res = sql_query($sql, false);

					//if($res)
					//	set_session('ss_member_reg_coupon', 1);
				}
			}
		}
		/* 사용후기 알림톡 */
		if($recv_number) {
			// AlimTalk BEGIN --------------------------------------------------------
			$alimtalk = new AlimTalk();
			$to = $alimtalk->set_phone_number($recv_number);
			$template_code = 'os_coupon_003';
			$text = $alimtalk->get_template($template_code);
			$text = str_replace('#{이름}', $mb_name, $text);
			$text = str_replace('#{금액}', 1000, $text);

			$alimtalk->set_message($template_code, $to, $text);
			$alimtalk->send();
			// AlimTalk END   --------------------------------------------------------
		}

	}

}
else if ($w == "u")
{
    $sql = " select is_password from {$g5['g5_shop_item_use_table']} where is_id = '$is_id' ";

    $row = sql_fetch($sql);
    if ($row['is_password'] != $is_password)
        alert("비밀번호가 틀리므로 수정하실 수 없습니다.");

    $sql = " update {$g5['g5_shop_item_use_table']}
                set is_subject = '$is_subject',
                    is_content = '$is_content',
                    is_image = '$is_image',
                    is_score = '$is_score'
              where is_id = '$is_id' ";
    sql_query($sql);
	/*************** 트랜잭션 관련 ****************/
	if( mysqli_errno($g5['connect_db']) ){
		$error_cnt += 1;
	}
	/*************** 트랜잭션 관련 ****************/

    $alert_msg = "사용후기가 수정 되었습니다.";
}
else if ($w == "d")
{
    if (!$is_admin)
    {
        $sql = " select count(*) as cnt from {$g5['g5_shop_item_use_table']} where mb_id = '{$member['mb_id']}' and is_id = '$is_id' ";
        $row = sql_fetch($sql);
        if (!$row['cnt'])
            alert("자신의 사용후기만 삭제하실 수 있습니다.");
    }

    $sql = " delete from {$g5['g5_shop_item_use_table']} where is_id = '$is_id' and md5(concat(is_id,is_time,is_ip)) = '{$hash}' ";
    sql_query($sql);
	/*************** 트랜잭션 관련 ****************/
	if( mysqli_errno($g5['connect_db']) ){
		$error_cnt += 1;
	}
	/*************** 트랜잭션 관련 ****************/

	/*************** 트랜잭션 관련 delete의 오류가 없다면 파일삭제 ****************/
	if( $error_cnt == 0 ){

		// 에디터로 첨부된 이미지 삭제
		$sql = " select is_content from {$g5['g5_shop_item_use_table']} where is_id = '$is_id' and md5(concat(is_id,is_time,is_ip)) = '{$hash}' ";
		$row = sql_fetch($sql);

		$imgs = get_editor_image($row['is_content'], $get_editor_img_mode);

		for($i=0;$i<count($imgs[1]);$i++) {
			$p = parse_url($imgs[1][$i]);
			if(strpos($p['path'], "/data/") != 0)
				$data_path = preg_replace("/^\/.*\/data/", "/data", $p['path']);
			else
				$data_path = $p['path'];


			if( preg_match('/(gif|jpe?g|bmp|png)$/i', strtolower(end(explode('.', $data_path))) ) ){

				$destfile = ( ! preg_match('/\w+\/\.\.\//', $data_path) ) ? G5_PATH.$data_path : '';

				if($destfile && preg_match('/\/data\/editor\/[A-Za-z0-9_]{1,20}\//', $destfile) && is_file($destfile))
					@unlink($destfile);
			}
		}

	}

    $alert_msg = "사용후기를 삭제 하였습니다.";
}

/*************** 트랜잭션 관련 오류가 없다면 함수실행 ****************/
if( $error_cnt == 0 ){
	update_use_avg($it_id);

	//쇼핑몰 설정에서 사용후기가 즉시 출력일 경우
	if( ! $default['de_item_use_use'] ){
		update_use_cnt($it_id);
	}
}

/*************** 트랜잭션 관련 ****************/
if ($error_cnt > 0) {
	mysqli_rollback($g5['connect_db']);
	mysqli_close($g5['connect_db']);
	echo "
	<script>
		alert('데이터베이스의 에러로 인해 롤백되었습니다.');
		history.back();
	</script>
	";
	exit;
} else {
	mysqli_commit($g5['connect_db']);
} 
/*************** 트랜잭션 관련 ****************/

if($w == 'd')
    alert($alert_msg, $url);
else
    alert_opener($alert_msg, $url);
?>
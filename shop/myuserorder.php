<?php
include_once('./_common.php');

if (G5_IS_MOBILE) {
    include_once(G5_MSHOP_PATH.'/myuserorder.php');
    return;
}

define("_ORDERINQUIRY_", true);

$od_pwd = get_encrypt_string($od_pwd);



$mb_id = $member['mb_id'];
// 회원인 경우
if ($is_member)
{
    $sql_common = " from {$g5['member_table']} where mb_recommend = '$mb_id ' ";
}
else // 그렇지 않다면 로그인으로 가기
{
    goto_url(G5_BBS_URL.'/login.php?url='.urlencode(G5_SHOP_URL.'/myuserorder.php'));
}

// 테이블의 전체 레코드수만 얻음
$sql = " select count(*) as cnt " . $sql_common;
$row = sql_fetch($sql);
$total_count = $row['cnt'];

$rows = 10;//$config['cf_page_rows'];
$total_page  = ceil($total_count / $rows);  // 전체 페이지 계산
if ($page < 1) { $page = 1; } // 페이지가 없으면 첫 페이지 (1 페이지)
$from_record = ($page - 1) * $rows; // 시작 열을 구함


$g5['title'] = '고객주문조회';
include_once('./_head.php');

if(!G5_IS_MOBILE) {
	echo '<div class="site-wrap">
	<div id="aside">&nbsp;';
	include_once(G5_SHOP_PATH.'/asidemy.php');
	echo '</div><div id="container">';
}
if ((!$bo_table || $w == 's' ) && !defined('_INDEX_')) { ?><div id="wrapper_title"><?php echo $g5['title'] ?></div><?php } 

if($sdate == '') {
	$sdate = date( "Y-m" ).'-01';
	$edate = date( "Y-m-d" );
}


?>

<!-- 주문 내역 시작 { -->
<div id="sod_v">
    <p id="sod_v_info">고객정보 및 구매 실적조회를 하실 수 있습니다.</p>

    <?php
    $limit = " limit $from_record, $rows ";
?>
<link rel="stylesheet" href="//code.jquery.com/ui/1.8.18/themes/base/jquery-ui.css" />
<script src="//code.jquery.com/ui/1.8.18/jquery-ui.min.js"></script>



<form name="frm1" method="get" action="/shop/myuserorder.php" style="margin:0">	

	<div id="search-box">
		<span class="btndate" id="today">오늘</span>&nbsp;
		<span class="btndate" id="bday15">15일</span>&nbsp;
		<span class="btndate" id="bmonth1">1개월</span>&nbsp;
		<span class="btndate" id="bmonth3">3개월</span>&nbsp;&nbsp;
          <input type="text" name="sdate" id="sdate" style="width:90px; text-align:center" class="datepickerbutton" value="<?php echo $sdate;?>" readonly>
          ~
          <input type="text" name="edate" id="edate" style="width:90px; text-align:center" class="datepickerbutton" value="<?php echo $edate;?>" readonly>
          <button class="btndate">조회</button>

		  <div style="float:right;"><a href="/shop/myuser.php" class=" btn01">고객조회</a> <a href="/shop/myuserorder.php" class=" btn01">고객주문조회</a></div>
	</div>

</form>
<script>
    $.datepicker.setDefaults({
        dateFormat: 'yy-mm-dd',
        prevText: '이전 달',
        nextText: '다음 달',
        monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
        monthNamesShort: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
        dayNames: ['일', '월', '화', '수', '목', '금', '토'],
        dayNamesShort: ['일', '월', '화', '수', '목', '금', '토'],
        dayNamesMin: ['일', '월', '화', '수', '목', '금', '토'],
        showMonthAfterYear: true,
        yearSuffix: '년'
    });

    $(function() {
        $(".datepickerbutton").datepicker();
    });

	function setDate(kind) {
		var today = '<?php echo date("Y-m-d"); ?>';
		var bday15 = '<?php echo date("Y-m-d", strtotime('-15 days')); ?>';
		var bmonth1 = '<?php echo date("Y-m-d", strtotime('-1 month')); ?>';
		var bmonth3 = '<?php echo date("Y-m-d", strtotime('-3 month')); ?>';

		if(kind==1) {
			$("#sdate").val(today);
			$("#edate").val(today);
		}
		if(kind==2) {
			$("#sdate").val(bday15);
			$("#edate").val(today);
		}
		if(kind==3) {
			$("#sdate").val(bmonth1);
			$("#edate").val(today);
		}
		if(kind==4) {
			$("#sdate").val(bmonth3);
			$("#edate").val(today);
		}

	}
	$( "#today" ).click(function() {
	  setDate(1);
	});
	$( "#bday15" ).click(function() {
	  setDate(2);
	});
	$( "#bmonth1" ).click(function() {
	  setDate(3);
	});
	$( "#bmonth3" ).click(function() {
	  setDate(4);
	});

</script>


<div class="tbl_head03 tbl_wrap">
    <table>
    <thead>
    <tr>
        <th scope="col">고객이름</th>
        <th scope="col">총주문건수</th>
		<th scope="col">총주문금액</th>
		<th scope="col">입금확인중</th>
		<th scope="col">결제완료</th>
		<th scope="col">주문확정</th>
		<th scope="col">상품배송중</th>
		<th scope="col">배송완료</th>
        
    </tr>
    </thead>
    <tbody>
	<?php
    //$sql = " select * from tbl_member where mb_recommend='".$member['mb_id']."'";

	$sql = " select *
               from {$g5['member_table']}
              where mb_recommend = '$mb_id '
              $limit ";

    $result = sql_query($sql);
    for ($i=0; $row=sql_fetch_array($result); $i++)
    {
        
		$sql = " select sum(case  when substr(c.od_date1, 1, 10) >= '$sdate' and substr(c.od_date1, 1, 10) <= '$edate' then 1 else 0 end) cnt1 
		               ,sum(case  when substr(c.od_date1, 1, 10) >= '$sdate' and substr(c.od_date1, 1, 10) <= '$edate' then b.od_total_sale_price else 0 end) cnt1amt
					   ,sum(case  when substr(c.od_date1, 1, 10) >= '$sdate' and substr(c.od_date1, 1, 10) <= '$edate' and c.od_pay_yn='N' then 1 else 0 end) cnt2
					   ,sum(case  when substr(c.od_date1, 1, 10) >= '$sdate' and substr(c.od_date1, 1, 10) <= '$edate' and c.od_pay_yn='Y' then 1 else 0 end) cnt3
					   ,sum(case  when substr(c.od_date2, 1, 10) >= '$sdate' and substr(c.od_date2, 1, 10) <= '$edate' then 1 else 0 end) cnt4
					   ,sum(case  when substr(c.od_date3, 1, 10) >= '$sdate' and substr(c.od_date3, 1, 10) <= '$edate' then 1 else 0 end) cnt5
					   ,sum(case  when substr(c.od_date4, 1, 10) >= '$sdate' and substr(c.od_date4, 1, 10) <= '$edate' then 1 else 0 end) cnt6

               from tbl_shop_order a, tbl_shop_order_detail b, tbl_shop_order_receiver c
              where a.od_id = b.od_id
			    and a.od_id = c.od_id
				and b.od_num = c.od_num
			    and a.mb_id = '{$row['mb_id']}'";

			  $row2 = sql_fetch($sql);
	?>
    
    <tr>
       
		<td class=""><?php echo $row['mb_name']; ?></td>
		<td class="td_numbig"><?php echo $row2['cnt1']; ?></td>
        <td class="td_numbig"><?php echo number_format($row2['cnt1amt']); ?></td>
		<td class="td_numbig"><?php echo $row2['cnt2']; ?></td>
		<td class="td_numbig"><?php echo $row2['cnt3']; ?></td>
		<td class="td_numbig"><?php echo $row2['cnt4']; ?></td>
		<td class="td_numbig"><?php echo $row2['cnt5']; ?></td>
		<td class="td_numbig"><?php echo $row2['cnt6']; ?></td>
       
    </tr>
	<?php } ?>
  
    </tbody>
    </table>
</div>
<!-- } 주문 내역 목록 끝 -->

    <?php echo get_paging($config['cf_write_pages'], $page, $total_page, "{$_SERVER['SCRIPT_NAME']}?$qstr&amp;page="); ?>
</div>
<!-- } 주문 내역 끝 -->

<?php
include_once('./_tail.php');
?>

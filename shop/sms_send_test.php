<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);

include_once('./_common.php');
include_once(G5_LIB_PATH.'/onk.sms.lib.php');

$onksms = new OnkSms();

$to = '01041508813';

$text = '오더스테이션 문자메시지 발송 테스트입니다.';
//$text = '오더스테이션 문자메시지 발송 테스트입니다. 서비스가 오픈되었습니다. 많은 이용 바랍니다!!';

$message_type = $onksms->get_msg_type($text);
echo 'msg_type=' . $message_type . '<br>';

if ($message_type == 'S') {	// sms 메시지
	
	$onksms->set_sms_message($to, $text);

} else if ($message_type == 'L') {	// lms 메시지
	
	$subject = '오더스테이션 메시지';
	
	$onksms->set_lms_message($to, $subject, $text);

}	

$onksms->send();

<?php
include_once('./_common.php');

if($is_guest)
    die('회원 로그인 후 이용해 주십시오.');

/***************************************
* 1. 라이브러리 인클루드              *
***************************************/
require("./inicis/libs/INIACTLib.php");

/***************************************
* 2. INIact50 클래스의 인스턴스 생성   *
***************************************/
$iniact = new INIact50;

$act_recv_name = $_POST['pharm_bank_user'];
$act_recv_code = $_POST['pharm_bank_code'];
$act_recv_num = $_POST['pharm_bank_num'];

if( empty($act_recv_name) && empty($act_recv_code) && empty($act_recv_num) ){
	
	echo "
		<script>
			alert('모든정보를 빠짐없이 입력해주세요');
		</script>
	";
	exit;

}

$act_recv_name = iconv("UTF-8", "EUC-KR", $act_recv_name);
$act_recv_code = iconv("UTF-8", "EUC-KR", $act_recv_code); 
$act_recv_num  = iconv("UTF-8", "EUC-KR", $act_recv_num);  

  /***************************************
   * 3. 지불 정보 설정                   *
   ***************************************/
$iniact->SetField("inipayhome","/var/www/html/orderstation-shop/shop/inicis/"); //상점 수정 필요

$iniact->SetField("MID", "openonk001");                   //상점마다 부여받은 상점ID 입력 필요!!(중요)
$iniact->SetField("actkey", "aFMwK3VBQ2hMNTdpZ0l2VHlBUjBxQT09");  //상점마다 부여받은 키를 입력 필요!!(중요)

$iniact->SetField("type", "ACTQ");                        //고정
$iniact->SetField("debug",true);                       //true면 로그를 상세히 남김
$iniact->SetField("act_recv_name",    $act_recv_name  );	   //한글기준  최대 7자, 필수 
$iniact->SetField("act_recv_code",    $act_recv_code  );	   //최대2자, 필수  
$iniact->SetField("act_recv_num",     $act_recv_num   );	   //최대15자, '-'없이, 필수 
								  
/***************************************
* 4. 요청                        *
***************************************/
$iniact->startAction();

/***************************************
* 5. 요청결과                         *
***************************************/
if( $iniact->GetResult("ResultCode") != "0000" ){

	$ReturnMsg = $iniact->GetResult("ResultMsg");
	$ReturnMsg = iconv("EUC-KR", "UTF-8", $iniact->GetResult("ResultCode")."/".$ReturnMsg);

	echo "
		<script>
			alert('".$ReturnMsg."');
		</script>
	";
	exit;
}

	$sql = " update {$g5['member_table']}
				set pharm_bank_code = '{$_POST['pharm_bank_code']}',
					pharm_bank_num = '{$_POST['pharm_bank_num']}',
					pharm_bank_user = '{$_POST['pharm_bank_user']}',
					pharm_bank_agree = 'Y',
					pharm_bank_agree_date = now()
				where mb_id ='".$member['mb_id']."'";
		sql_query($sql);

//echo $sql;

//goto_url(G5_SHOP_URL.'/orderuser.php');
?>
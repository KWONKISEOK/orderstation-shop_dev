<?php

?>

<style>
    .pop-terms {position: absolute;top: 25%; left:50%; width:500px; transform:translate(-50%, -50%); z-index: 101; background: #fff; }
    .pop-layer {display: block;position: absolute;top: 0;left: 0;width: 100%;height: 100%;z-index: 70; }
    /*.pop-layer .dimBg {position: absolute;top: 0;left: 0;width: 100%;height: 100%;background: #000;opacity: .3;filter: alpha(opacity=50);z-index: 80;}*/
    .pop-layer .pop-layer {display: block;}

    .dim-layer {position: fixed;top: 0;left: 0;width: 100%;height: 100%;z-index: 69;}
    .dim-layer .dimBg {position: absolute;top: 0;left: 0;width: 100%;height: 100%;background: #252525; opacity: .5;filter: alpha(opacity=50);}

    .pop-terms h2 {}
    .pop-terms .content {border:1px solid #ccc;text-align:left;line-height:1.6em;color:#666;background:#fafafa;padding:10px;height:150px;margin:10px 0;overflow-y:auto}

    #popupWrap {}
    #popupWrap .layer .layer_inner {position: fixed; left: 40%; top: 20%; z-index: 101; background: #fff; border-radius: 2px; width: 650px; height: 460px; padding: 10px 10px;}
    /*#popupWrap .layer::after {content: '';display: block;width: 100%;height: 100%;position: fixed;left: 0;top: 0;background: rgba(0,0,0,0.7);z-index: 10;}*/
    #popupWrap .pop-info {padding: 15px; width: 615px; height: 300px; border-radius: 2px; border: 1px solid #dbdbdb;overflow-y:auto;}
    #popupWrap .pop-info::-webkit-scrollbar {
        width: 6px;
    }
    #popupWrap .pop-info::-webkit-scrollbar-track {
        background-color: transparent;
    }
    #popupWrap .pop-info::-webkit-scrollbar-thumb {
        border-radius: 3px;
        background-color: #999;
    }
    #popupWrap .pop-info::-webkit-scrollbar-button {
        width: 0;
        height: 0;
    }
    #popupWrap .pop-title {text-align: center;}
    #popupWrap .pop-title h1 {font-size: 23px; letter-spacing: -1px; line-height: 68px; color: #252525; font-weight: bold;}
    #popupWrap .pop-btn-area {text-align: center; padding-top: 15px;}
    #popupWrap .pop-btn-area button {width: 100px; height: 40px; border-radius: 2px; background-color: #ffffff; border: 1px solid #dbdbdb; font-size: 16px; letter-spacing: -1px; color: #252525;}
</style>
<div id="popupWrap">
    <div class="layer complete" id="terms-layer" style="display: none;">
        <div class="layer_inner" style="margin-left: -200px;">
            <div class="pop-title">
                <h1>개인정보 수집 및 이용에 대한 안내</h1>
            </div>
            <div class="pop-info">
                <?php echo $default['de_guest_privacy']; ?>
            </div>
            <div class="pop-btn-area">
                <button type="button" onclick="javascript:hideLayer();" class="btn-red-arrow">확인</button>
            </div>
        </div>
    </div>
</div>
<div class="dim-layer" id="dim-layer" style="display: none;">
    <div class="dimBg"></div>
</div>
<div class="pop-layer" id="pop-layer" style="display:none;">
    <div class="pop-terms">
        <h2>약관 제목</h2>
        <div class="pop-terms content">
            <?php echo $default['de_guest_privacy']; ?>
        </div>
    </div>
</div>

<script>
    function terms () {
        $('#terms-layer').show();
        $('#dim-layer').show();
    }
    function hideLayer() {
        $('#terms-layer').hide();
        $('#dim-layer').hide();
    }
</script>

<?php
include_once('./_common.php');

// 상품이 많을 경우 대비 설정변경
ini_set('memory_limit', '100M');

$file_idx = trim($_POST['file_idx']);

if( empty($file_idx) ){
	echo "잘못된접근입니다.";
	exit;
}

$sql = " select * from tbl_regfile where file_idx = '$file_idx' ";
$wr_row = sql_fetch($sql);

$filename = $wr_row['file_name']; 

$fileroot = $wr_row['file_root']; 


$file = $fileroot;
 

if (is_file($file)) {
 
    if (preg_match("MSIE", $_SERVER['HTTP_USER_AGENT'])) { 
        header("Content-type: application/octet-stream"); 
        header("Content-Length: ".filesize("$file"));
        header("Content-Disposition: attachment; filename=$filename"); // 다운로드되는 파일명 (실제 파일명과 별개로 지정 가능)
        header("Content-Transfer-Encoding: binary"); 
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Pragma: public"); 
        header("Expires: 0"); 
    }
    else { 
        header("Content-type: file/unknown"); 
        header("Content-Length: ".filesize("$file")); 
        header("Content-Disposition: attachment; filename=$filename"); // 다운로드되는 파일명 (실제 파일명과 별개로 지정 가능)
        header("Content-Description: PHP3 Generated Data"); 
        header("Pragma: no-cache"); 
        header("Expires: 0"); 
    }
 
    $fp = fopen($file, "rb"); 
    fpassthru($fp);
    fclose($fp);
}
else {
    echo "해당 파일이 없습니다.";
}


?>
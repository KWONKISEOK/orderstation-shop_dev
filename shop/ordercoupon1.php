<?php
include_once('./_common.php');

if($is_guest)
    exit;

$price = (int)preg_replace('#[^0-9]#', '', $_POST['price']);
$it_id_arr = $_POST['it_id'];

if($price <= 0)
    die('상품금액이 0원이므로 쿠폰을 사용할 수 없습니다.');

/**
 * 주문할인금액 쿠폰 조회
 */
if($member['mb_type'] == 0) { 
	// 쿠폰정보
	$sql = " select *
            from {$g5['g5_shop_coupon_table']}
            where mb_id IN ( '{$member['mb_id']}', '소비자회원', '전체회원' ) 
              #and cp_method = '2'
              and cp_start <= '".G5_TIME_YMD."'
              and cp_end >= '".G5_TIME_YMD."'
              and cp_minimum <= '$price' and service_type is null";

} else if($member['mb_type'] == 1) {
	// 쿠폰정보
	$sql = " select *
            from {$g5['g5_shop_coupon_table']}
            where mb_id IN ( '{$member['mb_id']}', '약국회원', '전체회원' )
              #and cp_method = '2'
              and cp_start <= '".G5_TIME_YMD."'
              and cp_end >= '".G5_TIME_YMD."'
              and cp_minimum <= '$price' and service_type is null ";

} else {
	// 쿠폰정보
	$sql = " select *
            from {$g5['g5_shop_coupon_table']}
            where mb_id IN ( '{$member['mb_id']}', '전체회원' )
              #and cp_method = '2'
              and cp_start <= '".G5_TIME_YMD."'
              and cp_end >= '".G5_TIME_YMD."'
              and cp_minimum <= '$price' and service_type is null ";
}

$result = sql_query($sql);
$count = sql_num_rows($result);

/**
 * 1차 조회된 쿠폰에서 2차로 개별상품, 카테고리 할인 쿠폰에 대해 사용 가능한지 조회
 */
$lists = array();
for($i=0; $row=sql_fetch_array($result); $i++) {
    // 사용한 쿠폰인지 체크
    if (is_used_coupon($member['mb_id'], $row['cp_id']))
        continue;

    $item_coupon = false;
    foreach ($it_id_arr as $it_id) {
        // 사용가능한 쿠폰이면 foreach 탈출
        if ($item_coupon == true)
            continue;

        $sql = " select it_id, ca_id, ca_id2, ca_id3 from {$g5['g5_shop_item_table']} where it_id = '$it_id' ";
        $it = sql_fetch($sql);

        $sql = " select *
                        from {$g5['g5_shop_coupon_table']}
                        where cp_id = '{$row['cp_id']}'
                          and (
                                ( cp_method = '0' and instr( cp_target , concat( concat('|','{$it['it_id']}'),'|' ) ) > 0 )
                                OR
                                ( cp_method = '1' and instr( cp_target , concat( concat('|','{$it['ca_id']}'),'|' ) ) > 0 )
                                OR
                                ( cp_method = '1' and instr( cp_target , concat( concat('|','{$it['ca_id2']}'),'|' ) ) > 0 )
                                OR
                                ( cp_method = '1' and instr( cp_target , concat( concat('|','{$it['ca_id3']}'),'|' ) ) > 0 )
                              ) and service_type is null ";

        $res = sql_query($sql);
        $count = sql_num_rows($res);
        if ($count > 0)
            $item_coupon = true;
    }

    // 개별,카테고리 쿠폰인데 사용불가능한 쿠폰은 제외
    if ($row['cp_method'] != 2 && $item_coupon == false)
        continue;

    $dc = 0;
    if ($row['cp_type']) {
        $dc = floor(($price * ($row['cp_price'] / 100)) / $row['cp_trunc']) * $row['cp_trunc'];
    } else {
        $dc = $row['cp_price'];
    }

    if ($row['cp_maximum'] && $dc > $row['cp_maximum'])
        $dc = $row['cp_maximum'];

    $lists[] = array(
        'cp_id' => $row['cp_id'],
        'cp_subject' => $row['cp_subject'],
        'dc' => $dc,
        'cp_duplicate' => $row['cp_duplicate']
    );
}


?>

<?php if (!G5_IS_MOBILE) { ?>
    <style>
        .od_coupon {
            overflow-y: scroll;
            height:400px;
        }
    </style>
<?php } else { ?>
    <style>
        #od_coupon_frm {
            left: 0;
            margin: -60px 0 0 15px;
        }
        #od_coupon_cancel {
            height: 37px;
            line-height: 37px;
            padding: 0 10px;
            font-size: 11.5px;
            letter-spacing: -0.25px;
            margin:0;
            width: 100%;
        }
    </style>
<?php } ?>


<!-- 쿠폰 선택 시작 { -->
<div id="od_coupon_frm" class="od_coupon">
    <h3>쿠폰 선택</h3>
    <div class="tbl_head02 tbl_wrap">
        <table>
        <caption>쿠폰 선택</caption>
        <thead>
        <tr>
            <th scope="col">쿠폰명</th>
            <th scope="col">할인금액</th>
            <th scope="col">선택</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach($lists as $k => $v) { ?>
            <tr>
                <td>
                    <input type="hidden" name="acp_id[<?php echo $k ?>]" value="<?php echo $v['cp_id']; ?>">
                    <input type="hidden" name="acp_prc[<?php echo $k ?>]" value="<?php echo $v['dc']; ?>">
                    <input type="hidden" name="acp_subj[<?php echo $k ?>]" value="<?php echo $v['cp_subject']; ?>">
                    <input type="hidden" name="acp_due[<?php echo $k ?>]" value="<?php echo $v['cp_duplicate']; ?>">
                    <?php echo get_text($v['cp_subject']); ?>
                </td>
                <td class="td_numbig"><?php echo number_format($v['dc']); ?></td>
                <?php if($v['cp_duplicate'] == '1') { ?>
                    <td class="td_mngsmall"><input type="checkbox" name="chk2[]"  onchange="fnCheck();" value="<?php echo $k;?>"></td>
                <?php } else {?>
                <td class="td_mngsmall"><input type="checkbox" name="chk[]" onchange="fnCheck();" value="<?php echo $k;?>"  onclick="oneCheck(this)"></td>
                <?php } ?>
            </tr>
        <?php } ?>
        <?php if (count($lists) > 0) { ?>
        <tr>
            <th scope="col" style="font-size: 12px" >※ 중복사용가능 표시 쿠폰만 중복사용이 가능합니다.</th>
            <th scope="col"></th>
            <th scope="col" style="text-align: center;">
                <button type="button" class="od_cp_apply btn_frmline" style="width: 50px;">적용</button>
                <input type="hidden" name="o_cp_id[]" value="">
                <input type="hidden" name="o_cp_prc[]" value="0">
                <input type="hidden" name="o_cp_subj[]" value="">
            </th>
        </tr>
        <?php } ?>

        <?php if (count($lists) == 0) { ?>
            <tr>
                <td colspan="3" style="text-align: center; padding: 20px;"><p>사용할 수 있는 쿠폰이 없습니다.</p></td>
            </tr>
        <?php } ?>
        </tbody>
        </table>
    </div>
    <div class="btn_confirm">
        <button type="button" id="od_coupon_close" class="btn_close"><i class="fa fa-times" aria-hidden="true"></i><span class="sound_only">닫기</span></button>
    </div>
</div>
<script>
function fnCheck() {
	var f = document.forderform;
	var chk = document.getElementsByName("chk[]");
	var chk2 = document.getElementsByName("chk2[]");
	var price = 0;
	var cp_id_str = '';
	var cp_subj = '';
	for (var i=0; i<chk.length; i++)
	{
		if (chk[i].checked)
		{
			var k = chk[i].value;
			var cp_prc      = f.elements['acp_prc['+k+']'].value;
			var cp_id      = f.elements['acp_id['+k+']'].value;
			 cp_subj      = f.elements['acp_subj['+k+']'].value;

			price = Number(price) + Number(cp_prc);
			cp_id_str = cp_id_str + cp_id + ':';
		}
	}
	// 중복 쿠폰 사용
    for (var i=0; i<chk2.length; i++)
    {
        if (chk2[i].checked)
        {
            var k = chk2[i].value;
            var cp_prc      = f.elements['acp_prc['+k+']'].value;
            var cp_id      = f.elements['acp_id['+k+']'].value;
            cp_subj      = f.elements['acp_subj['+k+']'].value;

            price = Number(price) + Number(cp_prc);
            cp_id_str = cp_id_str + cp_id + ':';
        }
    }

	f.elements['o_cp_prc[]'].value = price;
	f.elements['o_cp_id[]'].value = cp_id_str.substring( 0, cp_id_str.length -1  );
	f.elements['o_cp_subj[]'].value = cp_subj;
}

function oneCheck(a){
    var obj = document.getElementsByName("chk[]");
    for(var i=0; i<obj.length; i++){
        if(obj[i] != a){
            obj[i].checked = false;
        }
    }
}

</script>
<!-- } 쿠폰 선택 끝 -->

<?php
include_once('./_common.php');

// 테마에 orderaddress.php 있으면 include
if(G5_IS_MOBILE && defined('G5_MSHOP_PATH')) {
    $theme_orderaddress_file = G5_MSHOP_PATH.'/orderaddresswrite.php';
    if(is_file($theme_orderaddress_file)) {
        include_once($theme_orderaddress_file);
        return;
    }
}

if(!$is_member)
    alert_close('회원이시라면 회원로그인 후 이용해 주십시오.');

$ad_id=$_GET['ad_id'];
$sql_common = " from {$g5['g5_shop_order_address_table']} where ad_id = '{$ad_id}' ";

$sql = " select * $sql_common and ad_id = $ad_id";

$result = sql_query($sql);
$row=sql_fetch_array($result);

$g5['title'] = '배송지 리스트';
include_once(G5_PATH.'/head.sub.php');
?>

    <form name="forderuser" method="post" action="./orderaddressupdate.php" autocomplete="off" onsubmit="return forderaddress_submit(this);">
        <input type="hidden" name="mode" value="write">
        <input type="hidden" name="ad_id"  value="<?php echo $ad_id; ?>">
        <input type="hidden" name="multi_gubun" value="<?php echo $multi_gubun; ?>"/>
        <div id="sod_addr" class="new_win">

            <h1 id="win_title"><i class="fa fa-address-book-o" aria-hidden="true"></i> 나의 배송지 리스트</h1>
            <div class="new_win_con">
                <!--<span style="position: absolute;float: right;margin: 90px 20px 0 0;top: 0;right: 0;"><a href="orderaddress.php?multi_gubun=<?php /*echo $multi_gubun*/?>" class="btn_frmline">배송지 리스트</a></span>-->
                <div class="" style="border-bottom: 1px solid #f3f3f3;">
                    <table>
                        <tr>
                            <th width="200">배송지명</th>
                            <td>
                                <input type="text" name="ad_subject" class="frm_input" value="<?php echo get_text($row['ad_subject']); ?>">
                                <input type="checkbox" name="ad_default" value="Y" id="ad_default" <?php if($row['ad_default']) echo 'checked="checked"';?>>
                                <label for="ad_default" class="">기본배송지</label>
                            </td>
                        </tr>
                        <tr>
                            <th width="200">이름</th>
                            <td><input type="text" name="ad_name" required class="required frm_input" value="<?php echo get_text($row['ad_name']); ?>"></td>
                        </tr>
                        <tr>
                            <th scope="col">핸드폰</th>
                            <td><input type="text" name="ad_hp" required class="required frm_input" value="<?php echo get_text($row['ad_hp']); ?>"></td>
                        </tr>
                        <tr>
                            <th align="center" class="bottom last jang_bold bgcolor1">주소</th>
                            <td align="left" class="bottom bgcolor2">
                                <input type="hidden" name="ad_addr3" id="ad_addr3" value="<?php echo $row['ad_addr3']; ?>">
                                <input type="hidden" name="ad_jibeon" id="ad_jibeon" value="<?php echo $row['ad_jibeon']; ?>">

                                <div class="member_ta_01" style="margin-bottom: 4px;">
                                    <input name="ad_zip" id="ad_zip" class="frm_input" type="text" style="width:60px" value="<?php echo get_text($row['ad_zip1'].$row['ad_zip2']); ?>" maxlength="7" readonly>
                                    <button type="button" class="btn_address" onclick="win_zip('forderuser', 'ad_zip', 'ad_addr1', 'ad_addr2', 'ad_addr3', 'ad_jibeon');">
                                        주소 검색
                                    </button>
                                    <br>
                                </div>
                                <?php if(G5_IS_MOBILE) { ?>
                                    <div class="member_ta_01">
                                        <input name="ad_addr1" id="ad_addr1" type="text" class="frm_input" style="width:200px" value="<?php echo get_text($row['ad_addr1']); ?>" maxlength="150" readonly>
                                    </div>
                                    <div class="member_ta_01">
                                        <input name="ad_addr2" id="ad_addr2" type="text" class="frm_input" style="width:200px" value="<?php echo get_text($row['ad_addr2']); ?>" maxlength="150">
                                    </div>
                                <?php } else { ?>
                                    <div class="member_ta_01" style="margin-bottom: 4px;">
                                        <input name="ad_addr1" id="ad_addr1" type="text" class="frm_input" style="width:500px" value="<?php echo get_text($row['ad_addr1']); ?>" maxlength="150" readonly>
                                    </div>
                                    <div class="member_ta_01" style="margin-bottom: 4px;">
                                        <input name="ad_addr2" id="ad_addr2" type="text" class="frm_input" style="width:500px" value="<?php echo get_text($row['ad_addr2']); ?>" maxlength="150">
                                    </div>
                                <?php } ?>
                            </td>
                        </tr>
                    </table>
                </div>

                <div class="win_btn">
                    <a href="orderaddress.php?multi_gubun=<?php echo $multi_gubun?>" class="btn_cancel">취소</a>
                    <input type="submit" name="act_button" value="저장" class="btn_submit">
                    <?php if(G5_IS_MOBILE) { ?>
                        <button type="button" onclick="self.close();" class="btn_close" style="width: 100%;height: 40px;line-height: 40px;border-radius: 3px;margin: -10px 0 20px;display: inline-block;font-weight: bold;">닫기</button>
                    <?php } else { ?>
                        <button type="button" onclick="self.close();" class="btn_close">닫기</button>
                    <?php } ?>

                </div>
            </div>
        </div>
    </form>
    <script src="https://spi.maps.daum.net/imap/map_js_init/postcode.v2.js"></script>
    <script>
        function forderaddress_submit(f) {
            var ad_name = $('input[name=ad_name]');
            if (!ad_name.val()) {
                ad_name.focus();
                return false;
            }

            return true;
        }
    </script>

<?php
include_once(G5_PATH.'/tail.sub.php');
?>
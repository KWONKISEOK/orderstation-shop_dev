<?php
include_once('./_common.php');

if (G5_IS_MOBILE) {
    include_once(G5_MSHOP_PATH.'/event.php');
    return;
}

$sql = " select * from {$g5['g5_shop_event_table']}
          where ev_id = '$ev_id'
            and ev_use = 1 ";
$ev = sql_fetch($sql);
if (!$ev['ev_id'])
    alert('등록된 이벤트가 없습니다.');

$g5['title'] = $ev['ev_subject'];
include_once('./_head.php');

if(!G5_IS_MOBILE) {
	
	echo '<div class="site-wrap">';
	echo '<div id="containersub">';
?>


	<!-- ********************* 소셜 공유하기 ********************* -->
	<div style="padding:10px 0px 10px 0;float:right;">

		<div class="social">
		
		<?php 
			$sns_url   = G5_SHOP_URL.'/event.php?ev_id='.$ev_id;
			$sns_title = $ev['ev_subject'];
			$sns_view_img = '';

			$str = get_sns_share_link('facebook', $sns_url, $sns_title, '/theme/onk/img/icon_facebook.gif');
			$str .= '&nbsp;';
			$str .= get_sns_share_link('twitter', $sns_url, $sns_title, '/theme/onk/img/icon_twitter.gif');
			echo $str;
		?>
		<?php echo get_sns_share_link('kakaotalk', $sns_url, $sns_title, G5_THEME_URL.'/img/kakaolink_btn.png'); ?>			
		
		</div>

	</div>
	<!-- ************************************************** -->

<?
}

/* SNS URL 공유시 대표이미지 및 설명 보이게 meta 에 추가 */
$event_content = $ev['ev_head_html'];
if( !empty($event_content) ){
    preg_match_all("/<img[^>]*src=[\'\"]?([^>\'\"]+)[\'\"]?[^>]*>/", $event_content, $matchs);
    $match_img = $matchs[1][0];
    $match_title = $ev['ev_subject'];
    $match_description  = $ev['ev_content'];
    $match_url = G5_URL."/shop/event.php?ev_id=".$ev_id;

    add_meta("<meta property=\"og:title\" content=\"$match_title\"/>",0);
    add_meta("<meta property=\"og:description\" content=\"$match_description\"/>",0);
    add_meta("<meta property=\"og:url\" content=\"$match_url\"/>",0);
    add_meta("<meta property=\"og:image\" content=\"$match_img\"/>",0);
}

if($ev_id=='1530591545') {
?>
<img src="<?php echo G5_THEME_URL;?>/img/brand/event.jpg" title="" alt="">
<br/><br/>
<?php } ?>

<script>
var itemlist_ca_id = "<?php echo $ev_id; ?>";
</script>
<script src="<?php echo G5_JS_URL; ?>/shop.list.js"></script>

<!-- 이벤트 시작 { -->
<?php
$himg = G5_DATA_PATH.'/event/'.$ev_id.'_h';
if (file_exists($himg))
    echo '<div id="sev_himg" class="sev_img"><img src="'.G5_DATA_URL.'/event/'.$ev_id.'_h" alt=""></div>';

if($ev['ev_head_html'] =='') { ?>

<br/><br/>
<?php }
//미세먼지&황사 기획전
if($ev_id=='1555650939') {
?>
<p><img src="/data/editor/1904/ad74a75fc936ee5021b87572d831e15c_1555650930_2585.jpg" usemap="#image-maps"><br style="clear:both;">&nbsp;</p>
<map name="image-maps">
<area  alt="" title="" href="https://forms.gle/53UpEgD37QFBAAjCA " shape="rect" coords="330,2102,871,2217" target="_blank"/>
</map>
<?
//5월 가정의 달 기획전
}else if($ev_id=='1556499814') {
?>
<p><img src="/data/editor/1904/d5ba22516a0acad82ec51248e16fa930_1556511727_1475.jpg" usemap="#image-maps"><br style="clear:both;">&nbsp;</p>
<map name="image-maps">
<area  alt="" title="" href="/shop/event.php?ev_id=1556500267" shape="rect" coords="260,1933,415,2096"		/>
<area  alt="" title="" href="/shop/event.php?ev_id=1556500374" shape="rect" coords="433,1934,588,2097"      />
<area  alt="" title="" href="/shop/event.php?ev_id=1556500434" shape="rect" coords="613,1935,768,2098"      />
<area  alt="" title="" href="/shop/event.php?ev_id=1556500582" shape="rect" coords="788,1934,943,2097"      />
<area  alt="" title="" href="/shop/event.php?ev_id=1556500685" shape="rect" coords="259,2117,414,2280"      />
<area  alt="" title="" href="/shop/event.php?ev_id=1556500706" shape="rect" coords="435,2118,590,2281"      />
<area  alt="" title="" href="/shop/event.php?ev_id=1556500776" shape="rect" coords="612,2116,767,2279"      />
<area  alt="" title="" href="/shop/event.php?ev_id=1556501025" shape="rect" coords="785,2116,940,2302"      />
<area  alt="" title="" href="/shop/event.php?ev_id=1556842047" shape="rect" coords="448,2326,752,2389"		/>
</map>
<?
//미세먼지&황사 기획전
}else if($ev_id=='1551673828') {
?>
<p><img src="/data/editor/1903/5546fc5a6bc75c9d3d3c25aa1237f9be_1551675994_1549.jpg" usemap="#image-maps"><br style="clear:both;">&nbsp;</p>
<map name="image-maps">
<area  alt="" title="" href="/shop/event.php?ev_id=1551676085" shape="rect" coords="90,1437,417,1936"/>
<area  alt="" title="" href="/shop/event.php?ev_id=1551676863" shape="rect" coords="428,1440,773,1939"/>
<area  alt="" title="" href="/shop/event.php?ev_id=1551676911" shape="rect" coords="784,1439,1136,1938"/>
<area  alt="" title="" href="/shop/event.php?ev_id=1551676956" shape="rect" coords="66,2205,418,2704"/>
<area  alt="" title="" href="/shop/event.php?ev_id=1551677010" shape="rect" coords="423,2206,775,2705"/>
<area  alt="" title="" href="/shop/event.php?ev_id=1551677055" shape="rect" coords="783,2205,1135,2704"/>
<area  alt="" title="" href="/shop/event.php?ev_id=1551677107" shape="rect" coords="65,2983,417,3482"/>
<area  alt="" title="" href="/shop/event.php?ev_id=1551677218" shape="rect" coords="423,2984,775,3483"/>
<area  alt="" title="" href="/shop/event.php?ev_id=1551677263" shape="rect" coords="784,2983,1136,3482"/>
</map>
<?
}else if($ev_id=='1551743206'){ //오에스데이 
?>
<p><img src="/data/editor/1903/e0823dd6771d401c20b21a58352d57b8_1551743181_0795.jpg" usemap="#image-maps"><br style="clear:both;">&nbsp;</p>
<map name="image-maps">
<area  alt="" title="" href="/shop/event.php?ev_id=1551743408" shape="rect" coords="38,1119,585,1952"/>
<area  alt="" title="" href="/shop/event.php?ev_id=1551743537" shape="rect" coords="619,1117,1166,1950"/>
</map>
<?
}else if($ev_id=='1554422517'){ //오에스데이 건강한 봄나들이
?>
<p><img src="/data/editor/1904/50419d95e41fdd7a3bc606183fd5d45a_1554422542_5549.jpg" usemap="#image-maps"><br style="clear:both;">&nbsp;</p>
<map name="image-maps">
<area  alt="" title="" href="/shop/event.php?ev_id=1554422658" shape="rect" coords="25,1081,1175,1676"/>
</map>
<?
}else{

// 상단 HTML
echo '<div id="sev_hhtml" >'.conv_content($ev['ev_head_html'], 1).'</div>';

}

// 상품 출력순서가 있다면
if ($sort != "")
    $order_by = $sort.' '.$sortodr.' , b.it_order, b.it_id desc';
else
    $order_by = 'b.it_order, b.it_id desc';

if ($skin) {
    $skin = preg_replace('#\.+/#', '', $skin);
    $ev['ev_skin'] = $skin;
}

if ($is_admin)
    echo '<div class="sev_admin" style="margin-top: 20px"><a href="'.G5_ADMIN_URL.'/shop_admin/itemeventform.php?w=u&amp;ev_id='.$ev['ev_id'].'" class="btn_admin">이벤트 관리</a></div>';
?>

<style>
    .function {
        overflow: hidden;
        text-align: right;
        line-height: 38px;
        margin-bottom: 20px;
    }
    .prdCount {
        float: left;
        color: #888888;
        font-size: 12px;
        letter-spacing: 1px;
    }
    .xans-product-normalmenu ul#type li {
        display: inline;
        border: 1px solid #e9e9e9;
        padding: 3px 10px 4px;
        font-size: 11px;
    }
    .xans-product-normalmenu ul#type li a{
        color: #9d9d9d;
    }
    .xans-product-normalmenu ul#type li .active {
        color: #ff8800;
    }

</style>
<div class="xans-product-menupackage "><h2><span><?php echo $g5['title']?></span></h2></div>

<?php
define('G5_SHOP_CSS_URL', G5_SHOP_SKIN_URL);

if($ev_id != 1530591545) {
	// 리스트 유형별로 출력
	$list_file = G5_SHOP_SKIN_PATH."/{$ev['ev_skin']}";

	if (file_exists($list_file))
	{
		
		//echo '<div id="sct_sortlst">';
		//include G5_SHOP_SKIN_PATH.'/list.sort.skin.php';

		// 상품 보기 타입 변경 버튼
		//include G5_SHOP_SKIN_PATH.'/list.sub.skin.php';

		//echo '</div>';

		// 총몇개 = 한줄에 몇개 * 몇줄
		$items = $ev['ev_list_mod'] * $ev['ev_list_row'];
		// 페이지가 없으면 첫 페이지 (1 페이지)
		if ($page < 1) $page = 1;
		// 시작 레코드 구함
		$from_record = ($page - 1) * $items;

		$list = new item_list(G5_SHOP_SKIN_PATH.'/'.$ev['ev_skin'], $ev['ev_list_mod'], $ev['ev_list_row'], $ev['ev_img_width'], $ev['ev_img_height']);
		$list->set_mb_type($member['mb_type']);
		$list->set_event($ev['ev_id']);
		$list->set_is_page(true);
		$list->set_order_by($order_by);
		$list->set_from_record($from_record);
		$list->set_view('it_img', true);
		$list->set_view('it_id', false);
		$list->set_view('it_name', true);
		$list->set_view('it_cust_price', false);
		$list->set_view('it_price', true);
		$list->set_view('it_icon', true);
		$list->set_view('sns', true);
        $list->set_price_list($maxPrice, $minPrice);

        $display = $list->run();


        //실행되는 쿼리를 얻어와서 맨 첫번째 이미지를 가지고 온다 og 태그대표이미지를 뿌려주기 위해서이다.
        $getquery = $list->return_query();

        $first_img = "";
		if( !empty($getquery) ){
			$get_event_row = sql_query($getquery);
			for ($i=0; $event_row=sql_fetch_array($get_event_row); $i++) {
				if( $i == 0 ){
					$first_img = G5_DATA_URL."/item/".$event_row['it_img1'];
				}else{
					break;
				}
			}
		}

		// where 된 전체 상품수
		$total_count = $list->total_count;
		// 전체 페이지 계산
		$total_page  = ceil($total_count / $items);
	}
	else
	{
		echo '<div align="center">'.$ev['ev_skin'].' 파일을 찾을 수 없습니다.<br>관리자에게 알려주시면 감사하겠습니다.</div>';
	}


    $qstr .= 'skin='.$skin.'&amp;ev_id='.$ev_id.'&amp;sort='.$sort.'&amp;sortodr='.$sortodr;
    echo get_paging($config['cf_write_pages'], $page, $total_page, "{$_SERVER['SCRIPT_NAME']}?$qstr&amp;page=");
}

// 상품 리스트에서 소비자가 최댓값
$max_sql = "SELECT MAX(it_price) as max_price FROM {$g5['g5_shop_event_item_table']} a, {$g5['g5_shop_item_table']} b WHERE a.it_id = b.it_id and a.ev_id = '$ev_id'";
$max_result = sql_fetch($max_sql);
?>

    <div class="xans-product-normalmenu function">
            <p class="prdCount"><strong><?php echo $total_count;?></strong>개의 상품이 등록되어 있습니다.</p>
        <?php
        if($ev['ev_sort'] == 0 || $ev['ev_sort'] == 2) {
        ?>
            <ul id="type" class="xans-element- xans-product xans-product-orderby">
                <li class="xans-record-"><a href="/shop/event.php?ev_id=<?php echo $ev_id;?>&amp;maxPrice=<?php echo $_GET['maxPrice']?>&amp;minPrice=<?php echo $_GET['minPrice']?>&amp;sort=it_sum_qty&amp;sortodr=desc"><span class="<?php if($sort=='it_sum_qty' && $sortodr=='desc') echo 'active';?>">판매많은순</span></a></li>
                <li class="xans-record-"><a href="/shop/event.php?ev_id=<?php echo $ev_id;?>&amp;maxPrice=<?php echo $_GET['maxPrice']?>&amp;minPrice=<?php echo $_GET['minPrice']?>&amp;sort=it_price&amp;sortodr=asc"><span class="<?php if($sort=='it_price' && $sortodr=='asc') echo 'active';?>">낮은가격순</span></a></li>
                <li class="xans-record-"><a href="/shop/event.php?ev_id=<?php echo $ev_id;?>&amp;maxPrice=<?php echo $_GET['maxPrice']?>&amp;minPrice=<?php echo $_GET['minPrice']?>&amp;sort=it_price&amp;sortodr=desc"><span class="<?php if($sort=='it_price' && $sortodr=='desc') echo 'active';?>">높은가격순</span></a></li>
                <li class="xans-record-"><a href="/shop/event.php?ev_id=<?php echo $ev_id;?>&amp;maxPrice=<?php echo $_GET['maxPrice']?>&amp;minPrice=<?php echo $_GET['minPrice']?>&amp;sort=it_use_avg&amp;sortodr=desc"><span class="<?php if($sort=='it_use_avg' && $sortodr=='desc') echo 'active';?>">평점높은순</span></a></li>
                <li class="xans-record-"><a href="/shop/event.php?ev_id=<?php echo $ev_id;?>&amp;maxPrice=<?php echo $_GET['maxPrice']?>&amp;minPrice=<?php echo $_GET['minPrice']?>&amp;sort=it_use_cnt&amp;sortodr=desc"><span class="<?php if($sort=='it_use_cnt' && $sortodr=='desc') echo 'active';?>">후기많은순</span></a></li>
                <li class="xans-record-"><a href="/shop/event.php?ev_id=<?php echo $ev_id;?>&amp;maxPrice=<?php echo $_GET['maxPrice']?>&amp;minPrice=<?php echo $_GET['minPrice']?>&amp;sort=it_update_time&amp;sortodr=desc"><span class="<?php if($sort=='it_update_time' && $sortodr=='desc') echo 'active';?>">최근등록순</span></a></li>
            </ul>
        <?php
        }
        if($ev['ev_sort'] == 1 || $ev['ev_sort'] == 2) {
        ?>
            <ul id="type" class="xans-element- xans-product xans-product-orderby">
                <li class="xans-record-"><a href="/shop/event.php?ev_id=<?php echo $ev_id;?>"><span class="<?php if($maxPrice=='') echo 'active';?>">전체</span></a></li>
                <li class="xans-record-"><a href="/shop/event.php?ev_id=<?php echo $ev_id;?>&amp;maxPrice=10000&amp;minPrice=1&amp;sort=<?php echo $_GET['sort']?>&amp;sortodr=<?php echo $_GET['sortodr']?>"><span class="<? if($maxPrice=='10000') echo 'active';?>">1만원 이하</span></a></li>
                <li class="xans-record-"><a href="/shop/event.php?ev_id=<?php echo $ev_id;?>&amp;maxPrice=30000&amp;minPrice=10000&amp;sort=<?php echo $_GET['sort']?>&amp;sortodr=<?php echo $_GET['sortodr']?>"><span class="<?php if($maxPrice=='30000') echo 'active';?>">1만원~3만원</span></a></li>
                <li class="xans-record-"><a href="/shop/event.php?ev_id=<?php echo $ev_id;?>&amp;maxPrice=50000&amp;minPrice=30000&amp;sort=<?php echo $_GET['sort']?>&amp;sortodr=<?php echo $_GET['sortodr']?>"><span class="<?php if($maxPrice=='50000') echo 'active';?>">3만원~5만원</span></a></li>
                <li class="xans-record-"><a href="/shop/event.php?ev_id=<?php echo $ev_id;?>&amp;maxPrice=<?php echo $max_result['max_price']?>&amp;minPrice=50000&amp;sort=<?php echo $_GET['sort']?>&amp;sortodr=<?php echo $_GET['sortodr']?>"><span class="<?php if($maxPrice==$max_result['max_price']) echo 'active';?>">5만원이상</span></a></li>
            </ul>
    <?php } ?>
        </div>
<?php
        echo $display;



// 하단 HTML
echo '<div id="sev_thtml">'.conv_content($ev['ev_tail_html'], 1).'</div>';

$timg = G5_DATA_PATH.'/event/'.$ev_id.'_t';
if (file_exists($timg))
    echo '<div id="sev_timg" class="sev_img"><img src="'.G5_DATA_URL.'/event/'.$ev_id.'_t" alt=""></div>';
?>
<!-- } 이벤트 끝 -->

<?
if($config['cf_kakao_js_apikey']) {
?>
<!-- <script src="https://developers.kakao.com/sdk/js/kakao.min.js"></script> -->
<script src="//developers.kakao.com/sdk/js/kakao.min.js"></script>
<!-- <script src="<?php echo G5_JS_URL; ?>/kakaolink.js"></script> -->
<?
}
?>

	<script type='text/javascript'>
	  //<![CDATA[
		// // 사용할 앱의 JavaScript 키를 설정해 주세요.
		Kakao.init("<?php echo $config['cf_kakao_js_apikey']; ?>");
		// // 카카오링크 버튼을 생성합니다. 처음 한번만 호출하면 됩니다.
		Kakao.Link.createDefaultButton({
		  container: '#kakao-link-btn',
		  objectType: 'feed',
		  content: {
			title: '<?php echo $sns_title;?>',
			description: '',
			imageUrl: '<?php echo $first_img;?>',
			link: {
			  mobileWebUrl: '<?php echo $sns_url;?>',
			  webUrl: '<?php echo $sns_url;?>'
			}
		  },
		  /*social: {
			likeCount: 286,
			commentCount: 45,
			sharedCount: 845
		  },*/
		  buttons: [{
			title: '이벤트 보기',
			link: {
			  mobileWebUrl: '<?php echo $sns_url;?>',
			  webUrl: '<?php echo $sns_url;?>'
			}
		  }]
		});
	  //]]>
	</script>	

<?php
include_once('./_tail.php');
?>

<?php
include_once('./_common.php');

if($is_guest)
    die('회원 로그인 후 이용해 주십시오.');


$sql = " SELECT * from tbl_member WHERE mb_id = '".$member['mb_id']."' and mb_type = '1' limit 1 ";
$row = sql_fetch($sql);

if( $row["mb_no"] ){
	
	$no_comp	= $row["pharm_number"]; //사업자번호
	$nm_comp	= $row["pharm_name"]; //상호명
	$nm_boss	= $row["mb_name"]; //회원명
	$nm_regist	= $_POST['pharm_bank_user']; //예금주
	$cd_bank	= $_POST['pharm_bank_code']; //은행코드
	$no_acct	= $_POST['pharm_bank_num']; //계좌번호
	$no_tel		= $row["mb_tel"]; //연락처

}

if( empty($nm_regist) && empty($cd_bank) && empty($no_acct) ){
	
	echo "
		<script>
			alert('모든정보를 빠짐없이 입력해주세요');
		</script>
	";
	exit;

}

require("INIomr.php");

$iniomr = new INIomr;

$iniomr->reqtype 		= "REGIST";
$iniomr->inipayhome 	= "/var/www/html/orderstation-shop/shop/inicis";
$iniomr->id_merchant 	= "openonk001";
$iniomr->id_mall		= preg_replace("/\s+/", "", $member['mb_id']);
$iniomr->cl_id			= "2";
$iniomr->no_comp		= preg_replace("/\s+/", "", $no_comp);
$iniomr->nm_comp		= iconv("UTF-8", "EUC-KR", preg_replace("/\s+/", "", $nm_comp) );
$iniomr->nm_boss		= iconv("UTF-8", "EUC-KR", preg_replace("/\s+/", "", $nm_boss) );
$iniomr->nm_regist		= iconv("UTF-8", "EUC-KR", preg_replace("/\s+/", "", $nm_regist) );
$iniomr->cd_bank		= preg_replace("/\s+/", "", $cd_bank);
$iniomr->no_acct 		= preg_replace("/\s+/", "", $no_acct);
$iniomr->no_tel			= preg_replace("/\s+/", "", $no_tel);
$iniomr->passwd			= "onk2008*****";
$iniomr->cl_gubun		= "2";
$iniomr->debug			= "true";

$iniomr->startAction();

$res_cd  = $iniomr->getResultCode();
$res_msg = $iniomr->getResultMsg();

//갱신을 먼저합니다.
if($res_cd == '00') {
	
	$sql = " update {$g5['member_table']}
				set pharm_bank_code = '{$_POST['pharm_bank_code']}',
					pharm_bank_num = '{$_POST['pharm_bank_num']}',
					pharm_bank_user = '{$_POST['pharm_bank_user']}',
					pharm_bank_agree = 'Y',
					pharm_bank_agree_date = now()
				where mb_id ='".$member['mb_id']."'";
	sql_query($sql);

	echo "
		<script>
			alert('저장되었습니다.');
		</script>
	";
	exit;

}else{

	//갱신데이터에 없으면 신규 등록
	$iniomr->reqtype 		= "REGIST";
	$iniomr->inipayhome 	= "/var/www/html/orderstation-shop/shop/inicis";
	$iniomr->id_merchant 	= "openonk001";
	$iniomr->id_mall		= preg_replace("/\s+/", "", $member['mb_id']);
	$iniomr->cl_id			= "2";
	$iniomr->no_comp		= preg_replace("/\s+/", "", $no_comp);
	$iniomr->nm_comp		= iconv("UTF-8", "EUC-KR", preg_replace("/\s+/", "", $nm_comp) );
	$iniomr->nm_boss		= iconv("UTF-8", "EUC-KR", preg_replace("/\s+/", "", $nm_boss) );
	$iniomr->nm_regist		= iconv("UTF-8", "EUC-KR", preg_replace("/\s+/", "", $nm_regist) );
	$iniomr->cd_bank		= preg_replace("/\s+/", "", $cd_bank);
	$iniomr->no_acct 		= preg_replace("/\s+/", "", $no_acct);
	$iniomr->no_tel			= preg_replace("/\s+/", "", $no_tel);
	$iniomr->passwd			= "onk2008*****";
	$iniomr->cl_gubun		= "1";
	$iniomr->debug			= "true";

	$iniomr->startAction();

	$res_cd  = $iniomr->getResultCode();
	$res_msg = $iniomr->getResultMsg();

	if($res_cd == '00') {

		$sql = " update {$g5['member_table']}
					set pharm_bank_code = '{$_POST['pharm_bank_code']}',
						pharm_bank_num = '{$_POST['pharm_bank_num']}',
						pharm_bank_user = '{$_POST['pharm_bank_user']}',
						pharm_bank_agree = 'Y',
						pharm_bank_agree_date = now()
					where mb_id ='".$member['mb_id']."'";
		sql_query($sql);
	
		echo "
			<script>
				alert('저장되었습니다.');
			</script>
		";

		exit;

	}else{

		if( $res_cd == "01" ){
			$TempMsg = "접근 인증 실패";
		}else if( $res_cd == "02" ){
			$TempMsg = "요청 데이터 검증 오류";		
		}else if( $res_cd == "03" ){
			$TempMsg = "계좌 성명조회 오류";		
		}else if( $res_cd == "04" ){
			$TempMsg = "데이터 입력 오류";		
		}else if( $res_cd == "05" ){
			$TempMsg = "기타 장애 오류[계좌번호 오류]";		
		}else if( $res_cd == "98" ){
			$TempMsg = "데이터 수신 관련 오류";		
		}else if( $res_cd == "99" ){
			$TempMsg = "요청 정보 구성 실패";		
		}

		echo "
			<script>
				alert('".$TempMsg."');
			</script>
		";

		exit;

	}

}

//echo $sql;

//goto_url(G5_SHOP_URL.'/orderuser.php');
?>
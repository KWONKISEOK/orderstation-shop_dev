<?php
include_once('./_common.php');

if($is_guest)
    die('회원 로그인 후 이용해 주십시오.');

$ad_id = $_POST['ad_id'];
$ad_zip1 = substr($_POST['ad_zip'],0, 3);
$ad_zip2 = substr($_POST['ad_zip'], 3);
$ad_flag = $_POST['ad_flag'];
$ad_default = ($_POST['ad_default'] == 'Y') ? 1 : 0;

if($ad_id == '') {
	
	
    if ($ad_default)
        sql_query(" update {$g5['member_table_user']} set ad_default = '0' where mb_id = '{$member['mb_id']}' ");

	$sql = " insert into {$g5['member_table_user']}
				set mb_id = '{$member['mb_id']}' ,
				    ad_subject = '{$_POST['ad_subject']}',
				    ad_default = '{$ad_default}',
					ad_name = '{$_POST['ad_name']}',
					ad_tel = '{$_POST['ad_tel']}',
					ad_hp = '{$_POST['ad_hp']}',
					ad_zip1 = '$ad_zip1',
					ad_zip2 = '$ad_zip2',
					ad_addr1 = '{$_POST['ad_addr1']}',
					ad_addr2 = '{$_POST['ad_addr2']}',
					ad_addr3 = '{$_POST['ad_addr3']}',
					ad_jibeon = '{$_POST['ad_jibeon']}'";
		sql_query($sql);

} else {
    if ($ad_flag == "N") {
        $flag_chk = sql_fetch("select CONCAT(ad_zip1, ad_zip2) AS zipcode from {$g5['member_table_user']} where ad_id=$ad_id");

        if ($flag_chk['zipcode'] != $_POST['ad_zip']) {
            $ad_flag = "Y";
        }
    }

    if ($ad_default)
        sql_query(" update {$g5['member_table_user']} set ad_default = '0' where mb_id = '{$member['mb_id']}' ");

	$sql = " update {$g5['member_table_user']}
				set mb_id = '{$member['mb_id']}' ,
				    ad_subject = '{$_POST['ad_subject']}',
				    ad_default = '{$ad_default}',
					ad_name = '{$_POST['ad_name']}',
					ad_tel = '{$_POST['ad_tel']}',
					ad_hp = '{$_POST['ad_hp']}',
					ad_zip1 = '$ad_zip1',
					ad_zip2 = '$ad_zip2',
					ad_addr1 = '{$_POST['ad_addr1']}',
					ad_addr2 = '{$_POST['ad_addr2']}',
					ad_addr3 = '{$_POST['ad_addr3']}',
					ad_jibeon = '{$_POST['ad_jibeon']}',
					ad_flag = '$ad_flag'
				where ad_id =".$ad_id;
		sql_query($sql);
}


goto_url(G5_SHOP_URL.'/orderuser.php?multi_gubun='.$multi_gubun);
?>
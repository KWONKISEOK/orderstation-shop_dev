<?php
include_once('./_common.php');

// 세션에 저장된 토큰과 폼으로 넘어온 토큰을 비교하여 틀리면 에러
if ($token && get_session("ss_token") == $token) {
    // 맞으면 세션을 지워 다시 입력폼을 통해서 들어오도록 한다.
    set_session("ss_token", "");
} else {
    set_session("ss_token", "");
    alert("토큰 에러", G5_SHOP_URL);
}

$od = sql_fetch(" select * from {$g5['g5_shop_order_table']} where od_id = '$od_id'");

if (!$od['od_id']) {
    alert("존재하는 주문이 아닙니다.");
}

$uid = md5($od['od_id'].$od['od_time'].$od['od_ip']);

if(!$od['od_billkey']) {
	if($od['od_cancel_price'] > 0 ) {
		alert("취소할 수 있는 주문이 아닙니다.", G5_SHOP_URL."/orderinquiryview.php?od_id=$od_id&amp;uid=$uid");
	}
}

$cancel_possible = "true";

if($od['od_billkey'] != "") {

    $sql = " select count(*) as od_count1,
                        SUM(IF(ct_status = '입금' OR ct_status = '취소' OR ct_status = '반품' OR ct_status = '품절', 1, 0)) as od_count2
                    from {$g5['g5_shop_cart_table']}
                    where od_id = '$od_id' ";
    $row = sql_fetch($sql);

    //결제건이 없으면 이니시스에 전송 안되게 하기
    if($row['od_count2'] == 0) {
        $cancel_possible = "false";
    }
    if($row['od_count2'] > 0) {
        $cancel_possible = "true";
        echo "
            <script>
               alert('관리자에게 문의해주세요.');
               history.back();
            </script>
        ";
        exit;
    }

}

// PG 결제 취소
if($od['od_tno'] && $cancel_possible == "true") {
    switch($od['od_pg']) {
        case 'lg':
            require_once('./settle_lg.inc.php');
            $LGD_TID    = $od['od_tno'];        //LG유플러스으로 부터 내려받은 거래번호(LGD_TID)

            $xpay = new XPay($configPath, $CST_PLATFORM);

            // Mert Key 설정
            $xpay->set_config_value('t'.$LGD_MID, $config['cf_lg_mert_key']);
            $xpay->set_config_value($LGD_MID, $config['cf_lg_mert_key']);
            $xpay->Init_TX($LGD_MID);

            $xpay->Set("LGD_TXNAME", "Cancel");
            $xpay->Set("LGD_TID", $LGD_TID);

            if ($xpay->TX()) {
                //1)결제취소결과 화면처리(성공,실패 결과 처리를 하시기 바랍니다.)
                /*
                echo "결제 취소요청이 완료되었습니다.  <br>";
                echo "TX Response_code = " . $xpay->Response_Code() . "<br>";
                echo "TX Response_msg = " . $xpay->Response_Msg() . "<p>";
                */
            } else {
                //2)API 요청 실패 화면처리
                $msg = "결제 취소요청이 실패하였습니다.\\n";
                $msg .= "TX Response_code = " . $xpay->Response_Code() . "\\n";
                $msg .= "TX Response_msg = " . $xpay->Response_Msg();

                alert($msg);
            }
            break;
        case 'inicis':

			if($od['od_billkey'] != '') {
                $default['de_inicis_mid'] = INICIS_R_MID;
                $default['de_inicis_sign_key'] = INICIS_R_SIGN_KEY;
				//$default['de_inicis_mid'] = 'INIBillTst';
				//$default['de_inicis_sign_key'] = 'b09LVzhuTGZVaEY1WmJoQnZzdXpRdz09';
			} else {
                $default['de_inicis_mid'] = INICIS_MID;
                $default['de_inicis_sign_key'] = INICIS_SIGN_KEY;

			}
            include_once(G5_SHOP_PATH.'/settle_inicis.inc.php');
            $cancel_msg = iconv_euckr('주문자 본인 취소-'.$cancel_memo);

            /*********************
             * 3. 취소 정보 설정 *
			 $mid 			= "INIBillTst";
             *********************/
            $inipay->SetField("type",      "cancel");                        // 고정 (절대 수정 불가)
            $inipay->SetField("mid",       $default['de_inicis_mid']);       // 상점아이디
            /**************************************************************************************************
             * admin 은 키패스워드 변수명입니다. 수정하시면 안됩니다. 1111의 부분만 수정해서 사용하시기 바랍니다.
             * 키패스워드는 상점관리자 페이지(https://iniweb.inicis.com)의 비밀번호가 아닙니다. 주의해 주시기 바랍니다.
             * 키패스워드는 숫자 4자리로만 구성됩니다. 이 값은 키파일 발급시 결정됩니다.
             * 키패스워드 값을 확인하시려면 상점측에 발급된 키파일 안의 readme.txt 파일을 참조해 주십시오.
             **************************************************************************************************/
            $inipay->SetField("admin",     $default['de_inicis_admin_key']); //비대칭 사용키 키패스워드
            $inipay->SetField("tid",       $od['od_tno']);                   // 취소할 거래의 거래아이디
            $inipay->SetField("cancelmsg", $cancel_msg);                     // 취소사유

            /****************
             * 4. 취소 요청 *
             ****************/
            $inipay->startAction();

            /****************************************************************
             * 5. 취소 결과                                           	*
             *                                                        	*
             * 결과코드 : $inipay->getResult('ResultCode') ("00"이면 취소 성공)  	*
             * 결과내용 : $inipay->getResult('ResultMsg') (취소결과에 대한 설명) 	*
             * 취소날짜 : $inipay->getResult('CancelDate') (YYYYMMDD)          	*
             * 취소시각 : $inipay->getResult('CancelTime') (HHMMSS)            	*
             * 현금영수증 취소 승인번호 : $inipay->getResult('CSHR_CancelNum')    *
             * (현금영수증 발급 취소시에만 리턴됨)                          *
             ****************************************************************/

            $res_cd  = $inipay->getResult('ResultCode');
            $res_msg = $inipay->getResult('ResultMsg');

            if($res_cd != '00') {
                alert(iconv_utf8($res_msg).' 코드 : '.$res_cd);
            }
            break;
        default:
            require_once('./settle_kcp.inc.php');

            $_POST['tno'] = $od['od_tno'];
            $_POST['req_tx'] = 'mod';
            $_POST['mod_type'] = 'STSC';
            if($od['od_escrow']) {
                $_POST['req_tx'] = 'mod_escrow';
                $_POST['mod_type'] = 'STE2';
                if($od['od_settle_case'] == '가상계좌')
                    $_POST['mod_type'] = 'STE5';
            }
            $_POST['mod_desc'] = iconv("utf-8", "euc-kr", '주문자 본인 취소-'.$cancel_memo);
            $_POST['site_cd'] = $default['de_kcp_mid'];

            // 취소내역 한글깨짐방지
            setlocale(LC_CTYPE, 'ko_KR.euc-kr');

            include G5_SHOP_PATH.'/kcp/pp_ax_hub.php';

            // locale 설정 초기화
            setlocale(LC_CTYPE, '');
    }
}

/*************** 트랜잭션 관련 ****************/
$error_cnt = 0;
mysqli_autocommit($g5['connect_db'], false);
/*************** 트랜잭션 관련 ****************/

// 장바구니 자료 취소
sql_query(" update {$g5['g5_shop_cart_table']} set ct_status = '취소' where od_id = '$od_id' ");
/*************** 트랜잭션 관련 ****************/
if( mysqli_errno($g5['connect_db']) ){
	$error_cnt += 1;
}
/*************** 트랜잭션 관련 ****************/

// 주문 취소
$cancel_memo = addslashes(strip_tags($cancel_memo));
$cancel_price = $od['od_receipt_price'];

// 취소 데이터 저장
$sql = " insert into {$g5['g5_shop_order_cancel_table']}
        set od_id = '{$od_id}' ,
            od_receipt_price = '{$od['od_receipt_price']}',
            od_send_cost = '{$od['od_send_cost']}',
            od_send_cost2 = '{$od['od_send_cost2']}',
            od_send_coupon = '{$od['od_send_coupon']}',
            od_receipt_point = '{$od['od_receipt_point']}',
            od_coupon = '{$od['od_coupon']}'";
sql_query($sql);
/*************** 트랜잭션 관련 ****************/
if( mysqli_errno($g5['connect_db']) ){
    $error_cnt += 1;
}
/*************** 트랜잭션 관련 ****************/

$sql = " update {$g5['g5_shop_order_table']}
            set od_receipt_price = '0',
                od_receipt_point = '0',
                od_misu = '0',
                od_cancel_price = '$cancel_price',
                od_cart_coupon = '0',
                od_coupon = '0',
                od_send_coupon = '0',
                od_status = '취소',
                od_shop_memo = concat(od_shop_memo,\"\\n주문자 본인 직접 취소 - ".G5_TIME_YMDHIS." (취소이유 : {$cancel_memo})\")
            where od_id = '$od_id' ";
sql_query($sql);
/*************** 트랜잭션 관련 ****************/
if( mysqli_errno($g5['connect_db']) ){
	$error_cnt += 1;
}
/*************** 트랜잭션 관련 ****************/

//주문상세정보
$sql = " update tbl_shop_order_receiver
            set od_status = '취소',
                od_cancel_date = '".G5_TIME_YMDHIS."'
            where od_id = '$od_id' ";
sql_query($sql);
/*************** 트랜잭션 관련 ****************/
if( mysqli_errno($g5['connect_db']) ){
	$error_cnt += 1;
}
/*************** 트랜잭션 관련 ****************/

$point_type = $od['od_receipt_point_type'];

// 주문취소 회원의 포인트를 되돌려 줌
if($od['od_receipt_point'] > 0 && $error_cnt == 0 && $point_type == 'c') {
    insert_cash($member['mb_id'], $od['od_receipt_point'], "주문번호 $od_id 본인 취소", '', '', '', '', $od_id);
} else if ($od['od_receipt_point'] > 0 && $error_cnt == 0){
    insert_point($member['mb_id'], $od['od_receipt_point'], "주문번호 $od_id 본인 취소", '', '', '', '', $od_id);
}

// 주문취소 재고 원복
$sql = "SELECT od_id, it_id, io_id, od_qty from tbl_shop_order_detail WHERE od_id='$od_id'";
$result = sql_fetch($sql);
$io_id = $result['io_id'];
$it_id = $result['it_id'];
$ct_qty = $result['od_qty'];

if($io_id){
    // 옵션
    $sql1 = "SELECT io_stock_qty from {$g5['g5_shop_item_option_table']} where it_id = '$it_id'";
    $re = sql_fetch($sql1);

    $jaego = $re['io_stock_qty']; //현재 재고
    $qty = ($jaego + $ct_qty);

    $sql2 =  "update {$g5['g5_shop_item_option_table']} set io_stock_qty = '$qty' where it_id = '$it_id'";
    sql_fetch($sql2);
}else{
    $sql1 = "SELECT it_stock_qty from {$g5['g5_shop_item_table']} where it_id = '$it_id'";
    $re = sql_fetch($sql1);

    $jaego = $re['it_stock_qty']; //현재 재고
    $qty = ($jaego + $ct_qty); // 현재 재고 + 취소한 주문수량

    $sql2 =  "update {$g5['g5_shop_item_table']} set it_stock_qty = '$qty' where it_id = '$it_id'";
    sql_fetch($sql2);
}

//쿠폰사용내역
sql_query(" delete from  {$g5['g5_shop_coupon_log_table']} where od_id = '$od_id' ");
/*************** 트랜잭션 관련 ****************/
if( mysqli_errno($g5['connect_db']) ){
	$error_cnt += 1;
}
/*************** 트랜잭션 관련 ****************/

//주문취소 메시지발송
include_once(G5_LIB_PATH.'/kakao.alimtalk.lib.php');
if ($is_member) {
    $mb_name = $member['mb_name'];
    $mb_hp = $member['mb_hp'];
} else { // 비회원은 주문자 이름, 연락처로 설정
    $mb_name = $od['od_name'];
    $mb_hp = $od['od_hp'];
}
        $sql = " SELECT a.od_time, a.od_hp, a.od_misu, b.it_name from tbl_shop_order a, tbl_shop_order_detail b WHERE a.od_id = b.od_id AND a.od_id = '$od_id'";
        $count = " SELECT count(*) as count from tbl_shop_order a, tbl_shop_order_detail b WHERE a.od_id = b.od_id AND a.od_id = '$od_id'";
        $cancel = sql_fetch($sql);
        $c_row = sql_fetch($count);

        $recv_number = preg_replace('/[^0-9]/', '', $cancel['od_hp']);
if($recv_number && $error_cnt == 0 ) {
	// AlimTalk BEGIN --------------------------------------------------------
	$alimtalk = new AlimTalk();
	$to = $alimtalk->set_phone_number($recv_number);
	$template_code = 'os_order_004';
	$text = $alimtalk->get_template($template_code);
	$text = str_replace('#{이름}', $mb_name, $text);
	$text = str_replace('#{주문번호}', $od_id, $text);
    if($c_row['count']==1){
        $text = str_replace('#{주문상품}', $cancel['it_name'], $text);
    } else{
        $text = str_replace('#{주문상품}', $cancel['it_name'].' 외 '.($c_row['count'] -1).'건', $text);
    }
	$alimtalk->set_message($template_code, $to, $text);
	$alimtalk->send();
	// AlimTalk END   --------------------------------------------------------
}


/*************** 트랜잭션 관련 ****************/
if ($error_cnt > 0) {
	mysqli_rollback($g5['connect_db']);
	mysqli_close($g5['connect_db']);
	echo "
	<script>
		alert('데이터베이스의 에러로 인해 롤백되었습니다.');
		history.back();
	</script>
	";
	exit;
} else {
	mysqli_commit($g5['connect_db']);
}
/*************** 트랜잭션 관련 ****************/

if ($error_cnt == 0 && $pop_check == 'Y') {
    // 새창으로 해당 페이지 접근하는 경우 처리
    echo "
    <script>
        alert('정상적으로 취소되었습니다.');
        opener.location.reload(true);
        window.close();
    </script>
    ";
} else {
    alert('정상적으로 취소되었습니다.');

    if ($return_url == "orderinquiryview2") {
        goto_url(G5_SHOP_URL."/orderinquiryview2.php?od_id=$od_id&amp;uid=$uid");
    } else if ($return_url == "orderinquiryview") {
        goto_url(G5_SHOP_URL."/orderinquiryview.php?od_id=$od_id&amp;uid=$uid");
    } else {
        goto_url(G5_SHOP_URL."/orderinquiryview.php?od_id=$od_id&amp;uid=$uid");
    }
}

?>

<?php

include_once('./_common.php');

if (G5_IS_MOBILE) {
    include_once(G5_MSHOP_PATH.'/coupon.php');
    return;
}

	$sql  = "select aa.pharm_custno, aa.od_calculate_yn, aa.incen ,  ";
	$sql .= "		bb.mb_id , bb.mb_name , bb.mb_tel , bb.pharm_name , bb.pharm_status , bb.pharm_bank_agree ,  ";
	$sql .= "		bb.pharm_bank_agree_date, bb.pharm_bank_user, bb.pharm_bank_num, bb.pharm_bank_code, bb.mb_nick " ;
	$sql .= "from ( ";
	$sql .= "  select ";
	$sql .= "      pharm_custno, od_calculate_yn, sum(od_total_incen) incen ";
	$sql .= "  from tbl_shop_order a ";
	$sql .= "  inner join tbl_shop_order_detail b ";
	$sql .= "      on a.od_id = b.od_id ";
	$sql .= "  inner join tbl_shop_order_receiver c ";
	$sql .= " 	    on a.od_id = c.od_id and b.od_num = c.od_num ";
	$sql .= "  where ";
	$sql .= "      c.od_pay_yn = 'Y' and a.od_gubun = '3' and date_format(c.od_date1, '%Y%m') = '" . "2019" . addstring( "01" ) . "' and c.od_status != '취소'  and a.pharm_custno != ''";
	$sql .= "  group by pharm_custno, od_calculate_yn ";
	$sql .= ") aa ";
	$sql .= "inner join tbl_member bb ";
	$sql .= "  on aa.pharm_custno = bb.pharm_custno and mb_type = '1' ";
	if($sfl=='pharm_name') {
		$sql .= "  and pharm_name like '%이화약국%' ";
	}
	$sql .= "order by aa.pharm_custno desc ";
	$sql .= " limit {$from_record}, {$rows} ";

	$result = sql_query($sql);

    for ($i=0; $row=sql_fetch_array($result); $i++) {

       
        $mb_id = $row['mb_id'];
       

        //$bg = 'bg'.($i%2);
		
		$mb_id           = $row["mb_id"];
		//$mb_pass         = $row["mb_pass"];
		$mb_name         = $row["mb_name"];
		$mb_tel          = $row["mb_tel"];
		$pharm_name   = $row["pharm_name"];
		$pharm_custno  = $row["pharm_custno"];

		//$sales_name       = $row["sales_name"];
		$mb_status       = $row["pharm_status"];
	
		$pharm_bank_agree         = $row["pharm_bank_agree"];
		$pharm_bank_agree_date         = $row["pharm_bank_agree_date"];
		$pharm_bank_user         = $row["pharm_bank_user"];
		$pharm_bank_num          = $row["pharm_bank_num"];
		$pharm_bank_code          = $row["pharm_bank_code"];
		$od_calculate_yn        = $row["od_calculate_yn"];
		$incen        = $row["incen"];
		$org_incen           = $incen;

		if ( $pharm_bank_agree == '' ) {
			$mem_bank_agree = "N";
		}

		$pharm_bank_name = ViewBank( $pharm_bank_code );
		if (  $incen > 30000 ) {
			$incen2 = ( $incen * 0.003 ) * 10;
			$incen3 = ( $incen2 * 0.01 ) * 10;
			if ( $incen2 >= 1000 ) {
				$incen =  $incen  - ( $incen2  +  $incen3  );
			} else {
				$incen2 = 0;
				$incen3 = 0;
			}
		} else {
			$incen2 = 0;
			$incen3 = 0;
		}
		$incen = floor($incen);

		$check_disabled = "";
		if ( $od_calculate_yn != 'N' or  $pharm_bank_agree !='Y' ) {
			$check_disabled = "disabled";
		}

?>
		
<?php
include_once(G5_PATH.'/tail.sub.php');
?>
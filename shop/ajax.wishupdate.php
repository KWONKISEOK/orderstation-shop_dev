<?php
include_once('./_common.php');

if (empty($_POST))
    die('정보가 넘어오지 않았습니다.');

if (!$is_member)
    die('회원 전용 서비스 입니다.');

if(is_array($it_id))
    $it_id = $_POST['it_id'][0];

if(!$it_id)
    die('상품코드가 올바르지 않습니다.');

// 상품정보 체크
$sql = " select it_id from {$g5['g5_shop_item_table']} where it_id = '$it_id' ";
$row = sql_fetch($sql);

if(!$row['it_id'])
    die('상품정보가 존재하지 않습니다.');

$return = '';

/*************** 트랜잭션 관련 ****************/
$error_cnt = 0;
mysqli_autocommit($g5['connect_db'], false);
/*************** 트랜잭션 관련 ****************/

$mode = $_POST['mode'];
if ($mode == "d") {
    $sql = " select wi_id from {$g5['g5_shop_wish_table']}
              where mb_id = '{$member['mb_id']}' and it_id = '$it_id' ";
    $row = sql_fetch($sql);

    if(!$row['wi_id'] && !$member['mb_id'])
        die('위시리시트 상품을 삭제할 권한이 없습니다.');

    $sql = " delete from {$g5['g5_shop_wish_table']}
              where wi_id = '{$row['wi_id']}'
                and mb_id = '{$member['mb_id']}' ";
    sql_query($sql);
    /*************** 트랜잭션 관련 ****************/
    if( mysqli_errno($g5['connect_db']) ){
        $error_cnt += 1;
    }
    /*************** 트랜잭션 관련 ****************/

    $return = 'd';
} else {
    $sql = " select wi_id from {$g5['g5_shop_wish_table']}
              where mb_id = '{$member['mb_id']}' and it_id = '$it_id' ";
    $row = sql_fetch($sql);

    if (!$row['wi_id']) { // 없다면 등록


        $sql = " insert {$g5['g5_shop_wish_table']}
                    set mb_id = '{$member['mb_id']}',
                        it_id = '$it_id',
                        wi_time = '".G5_TIME_YMDHIS."',
                        wi_ip = '$REMOTE_ADDR' ";
        sql_query($sql);

        /*************** 트랜잭션 관련 ****************/
        if( mysqli_errno($g5['connect_db']) ){
            $error_cnt += 1;
        }
        /*************** 트랜잭션 관련 ****************/
    }

    $return = 'w';
}

/*************** 트랜잭션 관련 ****************/
if ($error_cnt > 0) {
    mysqli_rollback($g5['connect_db']);
    mysqli_close($g5['connect_db']);
    die('데이터베이스의 에러로 인해 롤백되었습니다.');
} else {
    mysqli_commit($g5['connect_db']);
}
/*************** 트랜잭션 관련 ****************/

echo $return;

?>
<?php
include_once('./_common.php');

if($config['cf_kakao_js_apikey']) {
?>
<!-- <script src="https://developers.kakao.com/sdk/js/kakao.min.js"></script> -->
<script src="//developers.kakao.com/sdk/js/kakao.min.js"></script>
<!-- <script src="<?php echo G5_JS_URL; ?>/kakaolink.js"></script> -->
<?
}

$add_sql = "";

//로그아웃 상태일때는 전체로 체크된것들만 보이게 하기 
if( empty($member['mb_type']) ){
	$add_sql = " and bn_view = 'a' ";
}
//약국회원 로그인이 아닐때 노출대상이 소비자인것들만 보이게하기
if( !empty($member['mb_type']) && $member['mb_type'] != "1" ){
	$add_sql = " and bn_view in( 'a' , 'b' ) ";
}
//약국회원 로그인일때 노출대상이 약사인것들만 보이게 하기 
if( !empty($member['mb_type']) && $member['mb_type'] == "1" ){
	$add_sql = " and bn_view in( 'a' , 'c' ) ";
}
//관리자일때는 모두보이게 
if( !empty($member['mb_type']) && $member['mb_type'] == "9" ){
	$add_sql = " and bn_view in( 'a' , 'b' , 'c' ) ";
}

if (G5_IS_MOBILE) {
    include_once(G5_MSHOP_PATH.'/eventbanner.php');
    return;
}

// 이벤트배너 출력
$sql = " select * from tbl_shop_event_banner where '".G5_TIME_YMDHIS."' between bn_begin_time and bn_end_time $add_sql order by bn_order, bn_id desc ";
$result = sql_query($sql);
//$ev = sql_fetch($sql);
//if (!$ev['ev_id'])
//    alert('등록된 이벤트가 없습니다.');

//$g5['title'] = $ev['ev_subject'];
include_once('./_head.php');

if(!G5_IS_MOBILE) {
	
	echo '<div class="site-wrap">';
	echo '<div id="containersub">';
}
?>

<!-- ********************* 소셜 공유하기 ********************* -->
<div style="padding:10px 0 10px 0;float:right;">

	<div class="social">
	
	<?php 
		$sns_url   = G5_SHOP_URL.'/eventbanner.php';
		$sns_title = "오더스테이션이 특별히 준비한 다양한 혜택을 누려보세요!";
		//$sns_view_img = G5_URL.'/theme/onk/img/brand/event.jpg';
		$sns_view_img = G5_SHOP_URL.'/img/kakao_event_view.jpg';

		$str = get_sns_share_link('facebook', $sns_url, $sns_title, '/theme/onk/img/icon_facebook.gif');
		$str .= '&nbsp;';
		$str .= get_sns_share_link('twitter', $sns_url, $sns_title, '/theme/onk/img/icon_twitter.gif');
		echo $str;
	?>
	<?php echo get_sns_share_link('kakaotalk', $sns_url, $sns_title, G5_THEME_URL.'/img/kakaolink_btn.png'); ?>
	<script type='text/javascript'>
	  //<![CDATA[
		// // 사용할 앱의 JavaScript 키를 설정해 주세요.
		Kakao.init("<?php echo $config['cf_kakao_js_apikey']; ?>");
		// // 카카오링크 버튼을 생성합니다. 처음 한번만 호출하면 됩니다.
		Kakao.Link.createDefaultButton({
		  container: '#kakao-link-btn',
		  objectType: 'feed',
		  content: {
			title: '<?php echo $sns_title;?>',
			description: '',
			imageUrl: '<?php echo $sns_view_img;?>',
			link: {
			  mobileWebUrl: '<?php echo $sns_url;?>',
			  webUrl: '<?php echo $sns_url;?>'
			}
		  },
		  /*social: {
			likeCount: 286,
			commentCount: 45,
			sharedCount: 845
		  },*/
		  buttons: [{
			title: '이벤트 보기',
			link: {
			  mobileWebUrl: '<?php echo $sns_url;?>',
			  webUrl: '<?php echo $sns_url;?>'
			}
		  }]
		});
	  //]]>
	</script>				
	
	</div>

</div>
<!-- ************************************************** -->

<img src="<?php echo G5_THEME_URL;?>/img/brand/event.jpg" title="" alt="">
<br/><br/>

<?php
if ($is_admin)
    echo '<div class="sev_admin"><a href="'.G5_ADMIN_URL.'/shop_admin/itemeventbanner.php" class="btn_admin">이벤트 관리</a></div>';

for ($i=0; $row=sql_fetch_array($result); $i++) { ?>
	<p><a href="<?php echo $row['bn_url'];?>" target="_self"><img src="<?php echo G5_DATA_URL.'/eventbanner/'.$row['bn_id'] ;?>" alt=""></a></p>
	<br/>
<?php } ?>

<?php
include_once('./_tail.php');
?>

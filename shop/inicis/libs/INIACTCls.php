<?php
require_once ( "INIACTDFN.php" );

extract($_POST);
extract($_GET);



/*-----------------------------------------------------*/
/* LOG Class                                           */
/*-----------------------------------------------------*/
class INIACTLog 
{
	var $handle;
	var $type;
	var $log;
	var $debug_mode;
	var	$array_key;
	var $debug_msg;
	var $starttime;
	var $homedir;
	var $mid;

  function __construct($request)
  {
		$this->debug_msg = array( "", "CRITICAL", "ERROR", "NOTICE", "4", "INFO", "6", "DEBUG", "8"  );
		$this->debug_mode = $request["debug"];
		$this->type = $request["type"];
		$this->log = $request["log"];
		$this->homedir = $request["inipayhome"];
		$this->mid = $request["MID"];
		$this->starttime=$this->GetMicroTime();
	}
  function StartLog() 
	{
		if( $this->log == "false" ) return true;

		if( $this->type == "formpay" || $this->type == "auth" )
			$type = "formpay";	
		else
			$type = $this->type;
		$logfile = $this->homedir. "/log/".PROGRAM."_".$type."_".$this->mid."_".date("ymd").".log";
		$this->handle = fopen( $logfile, "a+" );
		if( !$this->handle ) return false;
		$this->WriteLog( INFO, "START ".PROGRAM." ".$this->type." (V".VERSION."-".BUILDDATE.")(OS:".php_uname('s').php_uname('r').",PHP:".phpversion().")" );
		return true;
	}
  function WriteLog($debug, $data) 
	{
		if( $this->log == "false" || !$this->handle ) return;

		if( !$this->debug_mode && $debug >= DEBUG ) return;

    $pfx = $this->debug_msg[$debug]."\t[" . $this->SetTimestamp() . "] <" . getmypid() . "> ";
    if( is_array( $data ) )
    {
    	foreach ($data as $key => $val)
    	{
				if( $key == "actkey" )
    			fwrite( $this->handle, $pfx . $key . ":[" . substr_replace($val, '****', 2, 4) . "]\r\n");
				else
    			fwrite( $this->handle, $pfx . $key . ":[" . $val . "]\r\n");
    	}
   	}
    else
    {
       fwrite( $this->handle, $pfx . $data . "\r\n" );
    }
    fflush( $this->handle );
	}
  function CloseLog($msg)
  {
		if( $this->log == "false" ) return;

		$laptime=$this->GetMicroTime()-$this->starttime;
		$this->WriteLog( INFO, "END ".$this->type." ".$msg." Laptime:[".round($laptime,3)."sec]" );
		$this->WriteLog( INFO, "===============================================================" );
    fclose( $this->handle );
  }
	function GetMicroTime()
	{
 	   list($usec, $sec) = explode(" ", microtime(true));
 	   return (float)$usec + (float)$sec;
	}
	function SetTimestamp()
	{
    	$m = explode(' ',microtime());
    	list($totalSeconds, $extraMilliseconds) = array($m[1], (int)round($m[0]*1000,3));
			return date("Y-m-d H:i:s", $totalSeconds) . ":$extraMilliseconds";
	} 
}

/*-----------------------------------------------------*/
/* Data Class			                                     */
/* -TID Generate Function	                             */
/* -Data Encrypt Function	                             */
/* -Check Field Function	                             */
/*-----------------------------------------------------*/
class INIACTData 
{
  //----------------------------
	//Common
  //----------------------------
	var $m_Type;
	var $m_ErrCode;
	var $m_ErrMsg;
	
  //----------------------------
	//HTTP
  //----------------------------
	var $m_ACTServer;
	var $m_ACTSocket;
	var $m_ErrorMsg;
	var $m_ACTReq;
	var $m_ACTRes;

  //----------------------------
	//IFD 요청필드
  //----------------------------
	var $m_sCmd;
	var $m_sPayMethod;
	var $m_sTID;
  var $m_TXVersion;
	var $m_sHead;
	var $m_sBody;
	var $m_sEncBody;
  //----------------------------
  //IFD 응답헤더 필드
  //----------------------------
  var $m_Cmd;
  var $m_Body;
  var $m_EncBody;

	var $m_REQUEST 	= array(); 
	var $m_RESULT 	= array(); 	//Encrypted 필드 hash table

	function __construct($request)
	{
		$this->m_REQUEST	= $request;
		$this->m_Type = $this->m_REQUEST["type"];
		if($this->m_Type == "ACT" )
		{
			$this->m_sCmd 			= CMD_REQ_PAY;
			$this->m_sPayMethod	= "ACT_";
		}
		else if($this->m_Type == "ACTQ" )
		{
			$this->m_sCmd 				= CMD_REQ_QRY;
			$this->m_sPayMethod 	= $this->m_Type;
		}
		else if($this->m_Type == "ACTR" )
		{
			$this->m_sCmd 				= CMD_REQ_RMN;
			$this->m_sPayMethod 	= $this->m_Type;
		}
    $this->m_TXVersion =
      sprintf("%-4.4s"  , VERSION).
      sprintf("%-5.5s"  , $this->m_Type).
      sprintf("%-10.10s", php_uname('s')).
      sprintf("%-3.3s", "PHP").   //modulescript
      sprintf("%-10.10s", "chkfake"). //moduledesc
      sprintf("%-30.30s", php_uname('r')."PHP:".phpversion()). //ETCINFO
      sprintf("%-6.6s", "__".$this->m_REQUEST["act_job_kind"])
      ;
	}

  function CheckField()
	{
		//---------------------------------
		//공통
		//---------------------------------
		if( !trim($this->m_REQUEST["inipayhome"]) )
		{	
			$this->m_ErrCode = NULL_DIR_ERR;
			$this->m_ErrMsg = "inipayhome";
			return false;
		}
		if( !trim($this->m_REQUEST["MID"]) )
		{	
			$this->m_ErrCode = NULL_MID_ERR;
			$this->m_ErrMsg = "MID";
			return false;
		}
		if( !trim($this->m_REQUEST["type"]) )
		{	
			$this->m_ErrCode = NULL_TYPE_ERR;
			$this->m_ErrMsg = "type";
			return false;
		}
		if( !trim($this->m_REQUEST["actkey"]) )
		{	
			$this->m_ErrCode = NULL_TYPE_ERR;
			$this->m_ErrMsg = "actkey";
			return false;
		}
		if( !trim($this->m_sPayMethod) )
		{	
			$this->m_ErrCode = NULL_PAYMETHOD_ERR;
			$this->m_ErrMsg = "paymethod";
			return false;
		}
		return true;
	}
  function MakeTID()
  {
		if( $this->m_REQUEST["tid"] == "" )
		{
  		list($usec, $sec) = explode(" ", microtime());
    	$datestr = date("YmdHis", $sec).substr($usec,2,3); //YYYYMMDDHHMMSSSSS
    	$this->m_sTID = "INIphp". $this->m_sPayMethod . $this->m_REQUEST["MID"] . $datestr . rand(100,999);
    	if( strlen( $this->m_sTID ) != TID_LEN )
			{
				return false;
			}
		}
		else //TEST!!(실제로 TID를 상점에서 올리면 안돼요~~)
		{
			$this->m_sTID = $this->m_REQUEST["tid"];
		}
		$this->m_REQUEST["TID"] = $this->m_sTID;
		$this->m_RESULT["TID"] = $this->m_sTID;
    return true;
  }
  function MakeBody()
	{
		list($usec, $sec) = explode(" ", microtime());
		$datestr1 = date("Ymd", $sec);
		$datestr2 = date("His", $sec);

		if($this->m_Type == "ACT" )
		{
			$this->m_sBody .= $this->m_REQUEST["act_recv_name"]."\x0B";
			$this->m_sBody .= $this->m_REQUEST["act_recv_code"]."\x0B";
			$this->m_sBody .= $this->m_REQUEST["act_recv_num"]."\x0B";
			$this->m_sBody .= $this->m_REQUEST["act_recv_price"]."\x0B";
			$this->m_sBody .= $this->m_REQUEST["act_send_name"]."\x0B";
			$this->m_sBody .= $this->m_REQUEST["act_recv_jumin"]."\x0B";
			$this->m_sBody .= $datestr1."\x0B";
			$this->m_sBody .= $datestr2."\x0B";
			$this->m_sBody .= $this->m_REQUEST["act_send_oid"]."\x0B";
			$this->m_sBody .= getenv("REMOTE_ADDR")."\x0B";
			$this->m_sBody .= $this->m_TXVersion."\x0B";
			$this->m_sBody .= $this->m_REQUEST["act_id_customer"]."\x0B";
			$this->m_sBody .= $this->m_REQUEST["act_job_kind"]."\x0B";
			$this->m_sBody .= $this->m_REQUEST["act_query_name"]."\x0B";
		}
		else if($this->m_Type == "ACTQ" )
		{
			$this->m_sBody .= $this->m_REQUEST["act_recv_name"]."\x0B";
			$this->m_sBody .= $this->m_REQUEST["act_recv_code"]."\x0B";
			$this->m_sBody .= $this->m_REQUEST["act_recv_num"]."\x0B";
			$this->m_sBody .= $datestr1."\x0B";
			$this->m_sBody .= $datestr2."\x0B";
			$this->m_sBody .= getenv("REMOTE_ADDR")."\x0B";
			$this->m_sBody .= $this->m_TXVersion."\x0B";
		}
		else if($this->m_Type == "ACTR" )
		{
			$this->m_sBody .= $datestr1."\x0B";
			$this->m_sBody .= $datestr2."\x0B";
			$this->m_sBody .= getenv("REMOTE_ADDR")."\x0B";
			$this->m_sBody .= $this->m_TXVersion."\x0B";
		}
		//암호화
		$this->MakeEncrypt( $this->m_sBody, $this->m_REQUEST["actkey"], $this->m_sEncBody);
		return OK;
	}
  function MakeHead()
	{
		$this->m_sHead = "R";
		$this->m_sHead .= sprintf("%4.4s",	$this->m_sCmd);
		$this->m_sHead .= "10";
		$this->m_sHead .= sprintf("%10.10s",	$this->m_REQUEST["MID"]);
		$this->m_sHead .= sprintf("%40.40s",	$this->m_sTID);
		$this->m_sHead .= sprintf("%04.04d", strlen($this->m_sEncBody));
		$this->m_sHead .= sprintf("%1.1s", "R"); //RC4
		return OK;
	}
  function MakeMsg()
	{
		return ($this->m_ACTReq = $this->m_sHead . $this->m_sEncBody);
	}
	function GTHR( $err_code, $err_msg )
	{
		//Set
    $data["mid"] = $this->m_REQUEST["MID"];
    $data["paymethod"] = $this->m_sPayMethod;
    $data["user_ip"] = getenv("REMOTE_ADDR");
    $data["tx_version"] = $this->m_TXVersion;
    $data["err_code"] = $err_code;
    $data["err_msg"] = $err_msg;

		//Send
		$qs = "ctype=TX&";
		foreach ($data as $key => $val) 
   		$qs .= $key.'='.urlencode($val).'&';
	
		$fp = fsockopen(G_SERVER, G_PORT, $errno, $errstr, G_TIMEOUT_CONNECT);
		if ($fp) 
		{
    	$out = "GET ".G_CGI."?".$qs." HTTP/1.0\r\n";
    	$out .= "Host: ".G_SERVER."\r\n";
    	$out .= "Connection: Close\r\n\r\n";
    	fwrite($fp, $out);
    	fclose($fp);
		}
	}
  function GetResult ( $node )
  {
		foreach ($this->m_RESULT as $key => $val) 
		{
			if( $node == $key ) return $val;
		}
  }
  function ConnSVR ($gws)
	{
		if( trim($gws) != "" )
			$this->m_ACTServer = $gws;
		else
			$this->m_ACTServer = ACTSVR_HOST_NAME;

		if (!$this->m_ACTSocket = @fsockopen( $this->m_ACTServer, ACTSVR_PORT, $errno, $errstr, TIMEOUT_CONNECT))
		{
			$this->m_ACTServer = ACTSVR_DR_IP;  //DR로 ( GLB를 쓰면 솔직히 어디로 붙는지 모르지만, 일단 DR로)
			if (!$this->m_ACTSocket = @fsockopen( $this->m_ACTServer, ACTSVR_PORT, $errno, $errstr, TIMEOUT_CONNECT))
			{
    		switch($errno)
    		{
	    		case -3:
 		  			$this->m_ErrorMsg = 'Socket creation failed (-3)';
    			case -4:
    				$this->m_ErrorMsg = 'DNS lookup failure (-4)';
    			case -5:
    				$this->m_ErrorMsg = 'Connection refused or timed out (-5)';
    			default:
    				$this->m_ErrorMsg = 'Connection failed ('.$errno.')';
    			$this->m_ErrorMsg .= ' '.$errstr;
    		}
    		return FAIL;
			}
		}
		return OK;
	}
  function SendSVR()
	{
    fwrite($this->m_ACTSocket, $this->m_ACTReq);
		return OK;
	}
  function RecvSVR ()
	{
    while ( !feof($this->m_ACTSocket) )
    {
      $line = fgets($this->m_ACTSocket, 4096);
      $this->m_ACTRes .= $line;
    }
		return OK;
	}
  function ParseRecv ()
	{
		$this->m_RESULT["R_MODE"]     	= substr($this->m_ACTRes, 0, 1);
		$this->m_RESULT["R_CMD"]      	= substr($this->m_ACTRes, 1, 4);
		$this->m_RESULT["R_VERSION"]  	= substr($this->m_ACTRes, 5, 2);
		$this->m_RESULT["R_MID"]  			= substr($this->m_ACTRes, 7, 10);
		$this->m_RESULT["TID"]  			  = substr($this->m_ACTRes, 17, 40);
		$this->m_RESULT["R_LEN"]  			= substr($this->m_ACTRes, 57, 4);
		$this->m_RESULT["R_ENC"]  			= substr($this->m_ACTRes, 61, 1);
		$this->m_RESULT["R_BODY"]  			= substr($this->m_ACTRes, 62);
		$this->m_RESULT[NM_RESULTCODE] 	=	substr($this->m_RESULT["R_BODY"], 0, 4);
		$this->m_RESULT[NM_RESULTMSG]		= substr($this->m_RESULT["R_BODY"], 5);
		return OK;
	}
  function CloseSVR ()
	{
		fclose( $this->m_ACTSocket );
		return OK;
	}
  function MakeEncrypt($pt, $key, &$ct)
  {
		$key = base64_decode( $key );
		//$key = substr( $key, 0, 16); //RC4 키길이가 16byte일때만 서버에서 정상복호화됨. 왜??!!
    if (get_magic_quotes_gpc())
    {
      $key = stripslashes($key);
      $pt = stripslashes($pt);
    }
    $ct = chunk_split(base64_encode( $this->Encrypt ( $key, $pt )), 64, "\n");
		$ct = substr($ct, 0, -1); // Eliminate unnecessary \n
    return true;
  }
  function MakeDecrypt($ct, $key, &$pt)
  {
    $pt = $this->Encrypt ( $key, base64_decode($ct) );
    return true;
  }
  function Encrypt ($pwd, $data)
  {
    $key[] = '';
    $box[] = '';
    $cipher = '';

    $pwd_length = strlen($pwd);
    $data_length = strlen($data);

    for ($i = 0; $i < 256; $i++)
    {
        $key[$i] = ord($pwd[$i % $pwd_length]);
        $box[$i] = $i;
    }
    for ($j = $i = 0; $i < 256; $i++)
    {
        $j = ($j + $box[$i] + $key[$i]) % 256;
        $tmp = $box[$i];
        $box[$i] = $box[$j];
        $box[$j] = $tmp;
    }
    for ($a = $j = $i = 0; $i < $data_length; $i++)
    {
        $a = ($a + 1) % 256;
        $j = ($j + $box[$a]) % 256;
        $tmp = $box[$a];
        $box[$a] = $box[$j];
        $box[$j] = $tmp;
        $k = $box[(($box[$a] + $box[$j]) % 256)];
        $cipher .= chr(ord($data[$i]) ^ $k);
    }
    return $cipher;
  }
}
?>


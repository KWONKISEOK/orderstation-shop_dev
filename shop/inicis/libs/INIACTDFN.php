<?php
/* GLOBAL */
/* 
 * VERSION : 5000
 * BUILDDATE : 20080701
 * Comment : initialize (5000 release된곳 없음)
 *
 * VERSION : 5001
 * BUILDDATE : 20080923
 * Comment : 1)계좌성명조회 추가
 *           2)오류발생시 로그를 닫지 않는 버그 fix. 
 *
 * VERSION : 5002
 * BUILDDATE : 20081029
 * Comment : 1)계좌잔액조회 추가
 */
define("PROGRAM", "INIPHP");
define("LANG", "PHP");
define("VERSION", "5048");
define("BUILDDATE", "20170605");
define("TID_LEN", 40);

/* TIMEOUT */
define("TIMEOUT_CONNECT", 5);
define("TIMEOUT_WRITE", 2);
define("TIMEOUT_READ", 20);
define("G_TIMEOUT_CONNECT", 2);

/* LOG LEVEL */
define("CRITICAL", 1);
define("ERROR", 2);
define("NOTICE", 3);
define("INFO", 5);
define("DEBUG", 7);

/* SERVER INFO */
define("ACTSVR_HOST_NAME",	"remit.inicis.com"); 
define("ACTSVR_TEST_IP", 		"203.238.37.17"); 
define("ACTSVR_MAIN_IP", 		"203.238.37.3"); 
define("ACTSVR_DR_IP", 			"211.219.96.180"); 
define("ACTSVR_PORT",       32000);
define("G_SERVER",              "gthr.inicis.com");
define("G_CGI",                 "/cgi-bin/g.cgi");
define("G_PORT",                80);

define("OK", "0");
define("FAIL", "1");

define("IV",             			"Initiative_Tech");

//------------------------------------------------------
// IFD Header
//------------------------------------------------------
define("MSGHEADER_LEN",       62);
define("CMD_LEN",             4);
define("BODY_LEN",            4);
define("MID_LEN",             10);
define("TOTPRICE_LEN",        20);
define("TID_LEN",             40);


//------------------------------------------------------
// IFD CMD
//------------------------------------------------------
define("CMD_REQ_PAY",         "0100");
define("CMD_RES_PAY",         "0110");
define("CMD_REQ_QRY",         "0200");
define("CMD_RES_QRY",         "0210");
define("CMD_REQ_RMN",         "0300");
define("CMD_RES_RMN",         "0310");


//------------------------------------------------------
//Charset
//------------------------------------------------------
define("EUCKR", "EUC-KR");
define("UTF8",  "UTF-8");

//------------------------------------------------------
//URL Encoding/Decoding Name
//------------------------------------------------------
define("URLENCODE", "urlencode");
define("URLDECODE", "urldecode");

//------------------------------------------------------
//
//응답전문
//
//------------------------------------------------------
define("NM_RESULTCODE",   "ResultCode");
define("NM_RESULTMSG",    "ResultMsg");

//----------------------------------
//ERROR CODE
//----------------------------------
//!!신TX에 추가된 에러!!!
define("NULL_DIR_ERR",                  "TX9001");
define("NULL_TYPE_ERR",                 "TX9002");
define("NULL_NOINTEREST_ERR",           "TX9003");
define("NULL_QUOTABASE_ERR",            "TX9004");
define("DNS_LOOKUP_ERR",                "TX9005");
define("MERCHANT_DB_ERR",               "TX9006");
define("DNS_LOOKUP_TIMEOUT_ERR",        "TX9007");
define("PGPUB_UPDATE_ERR",              "TX9612");

//암복호화 에러
define("B64DECODE_UPDATE_ERR",          "TX9101");
define("B64DECODE_FINAL_ERR",           "TX9102");
define("B64DECODE_LENGTH_ERR",          "TX9103");
define("GET_KEYPW_EVP_B2K_ERR",         "TX9104");
define("GET_KEYPW_FILE_OPEN_ERR",       "TX9105");
define("GET_KEYPW_FILE_READ_ERR",       "TX9106");
define("GET_KEYPW_DECRYPT_INIT_ERR",    "TX9107");
define("GET_KEYPW_DECRYPT_UPDATE_ERR",  "TX9108");
define("GET_KEYPW_DECRYPT_FINAL_ERR",   "TX9109");
define("ENC_RAND_BYTES_ERR",            "TX9110");
define("ENC_INIT_ERR",                  "TX9111");
define("ENC_UPDATE_ERR",                "TX9112");
define("ENC_FINAL_ERR",                 "TX9113");
define("ENC_RSA_ERR",                   "TX9114");
define("DEC_RSA_ERR",                   "TX9115");
define("DEC_CIPHER_ERR",                "TX9116");
define("DEC_INIT_ERR",                  "TX9117");
define("DEC_UPDATE_ERR",                "TX9118");
define("DEC_FINAL_ERR",                 "TX9119");
define("SIGN_FINAL_ERR",                "TX9120");
define("SIGN_CHECK_ERR",                "TX9121");
define("ENC_NULL_F_ERR",                "TX9122");
define("ENC_INIT_RAND_ERR",             "TX9123");
define("ENC_PUTENV_ERR",                "TX9124");
//필드체크
define("NULL_KEYPW_ERR",                "TX9201");
define("NULL_MID_ERR",                  "TX9202");
define("NULL_PGID_ERR",                 "TX9203");
define("NULL_TID_ERR",                  "TX9204");
define("NULL_UIP_ERR",                  "TX9205");
define("NULL_URL_ERR",                  "TX9206");
define("NULL_PRICE_ERR",                "TX9207");
define("NULL_PRICE1_ERR",               "TX9208");
define("NULL_PRICE2_ERR",               "TX9209");
define("NULL_CARDNUMBER_ERR",           "TX9210");
define("NULL_CARDEXPIRE_ERR",           "TX9211");
define("NULL_ENCRYPTED_ERR",            "TX9212");
define("NULL_CARDQUOTA_ERR",            "TX9213");
define("NULL_QUOTAINTEREST_ERR",        "TX9214");
define("NULL_AUTHENTIFICATION_ERR",     "TX9215");
define("NULL_AUTHFIELD1_ERR",           "TX9216");
define("NULL_AUTHFIELD2_ERR",           "TX9217");
define("NULL_BANKCODE_ERR",             "TX9218");
define("NULL_BANKACCOUNT_ERR",          "TX9219");
define("NULL_REGNUMBER_ERR",            "TX9220");
define("NULL_OCBCARDNUM_ERR",           "TX9221");
define("NULL_OCBPASSWD_ERR",            "TX9222");
define("NULL_PASSWD_ERR",               "TX9223");
define("NULL_CURRENCY_ERR",             "TX9224");
define("NULL_PAYMETHOD_ERR",            "TX9225");
define("NULL_GOODNAME_ERR",             "TX9226");
define("NULL_BUYERNAME_ERR",            "TX9227");
define("NULL_BUYERTEL_ERR",             "TX9228");
define("NULL_BUYEREMAIL_ERR",           "TX9229");
define("NULL_SESSIONKEY_ERR",           "TX9230");
//pg공개키 로드 오류
define("NULL_PGCERT_FP_ERR",            "TX9231");
define("NULL_X509_ERR",                 "TX9232");
define("NULL_PGCERT_ERR",               "TX9233");

define("RESULT_MSG_FORMAT_ERR",         "TX9234");

// 가상 계좌 이체 예약
define("NULL_PERNO_ERR",                "TX9235");  // 주민번호 빠짐
define("NULL_OID_ERR",                  "TX9236");  // 주문번호 빠짐
define("NULL_VCDBANK_ERR",              "TX9237");  // 은행코드 빠짐
define("NULL_DTINPUT_ERR",              "TX9238");  // 입금 예정일 빠짐
define("NULL_NMINPUT_ERR",              "TX9239");  // 송금자 성명 빠짐

//실시간 빌링
define("NULL_BILLKEY_ERR",              "TX9240");  // 빌키 빠짐
define("NULL_CARDPASS_ERR",             "TX9241");  // 카드 비번 빠짐
define("NULL_BILLTYPE_ERR",             "TX9242");  // 빌타입 누락
// CMS 계좌이체
define("NULL_PRICE_ORG_ERR",            "TX9250"); // CMS 출금총금액 빠짐
define("NULL_CMSDAY_ERR",               "TX9251"); // CMS 출금일자 빠짐
define("NULL_CMSDATEFROM_ERR",          "TX9252"); // CMS 출금시작월 빠짐
define("NULL_CMSDATETO_ERR",            "TX9253"); // CMS 출금종료월 빠짐

// 부분취소
define("NULL_CONFIRM_PRICE_ERR",        "TX9260"); // 재승인 요청금액 누락 에러

// 현금영수증 발행
define("NULL_CR_PRICE_ERR",             "TX9270"); // 현금결제 금액 빠짐
define("NULL_SUP_PRICE_ERR",            "TX9271"); // 공급가액 빠짐
define("NULL_TAX_ERR",                  "TX9272"); // 부가세 빠짐
define("NULL_SRVC_PRICE_ERR",           "TX9273"); // 봉사료 빠짐
define("NULL_REG_NUM_ERR",              "TX9274");  // 주민번호(사업자번호)
define("NULL_USEOPT_ERR",               "TX9275"); // 현금영수증 용도 구분자 빠짐

define("PRIVKEY_FILE_OPEN_ERR",         "TX9301");
define("INVALID_KEYPASS_ERR",           "TX9302");

define("MAKE_TID_ERR",                  "TX9401");
define("ACK_CHECKSUM_ERR",              "TX9402");
define("NETCANCEL_SOCK_CREATE_ERR",     "TX9403");
define("NETCANCEL_SOCK_SEND_ERR",       "TX9404");
define("NETCANCEL_SOCK_RECV_ERR",       "TX9405");
define("LOG_OPEN_ERR",                  "TX9406");
define("LOG_WRITE_ERR",                 "TX9407");

define("SOCK_MAKE_EP_ERR",              "TX9501");
define("SOCK_CONN_ERR",                 "TX9502");
define("SOCK_SEND1_ERR",                "TX9503");
define("SOCK_SEND2_ERR",                "TX9504");
define("SOCK_CLOSED_BY_PEER_ERR",       "TX9505");
define("SOCK_RECV1_ERR",                "TX9506");
define("SOCK_RECV2_ERR",                "TX9507");
define("SOCK_RECV_LEN_ERR",             "TX9508");
define("SOCK_TIMEO_ERR",                "TX9509");
define("SOCK_ETC1_ERR",                 "TX9510");
define("SOCK_ETC2_ERR",                 "TX9511");
define("NULL_ESCROWTYPE_ERR",           "TX6000");
define("NULL_ESCROWMSG_ERR",            "TX6001");

?>

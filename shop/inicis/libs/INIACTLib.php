<?php

	require_once('INIACTCls.php');

class INIact50
{
	var $m_type; 				// 거래 유형

	var $m_Data;
	var $m_Log;

	var $m_REQUEST 	= array();
	var $m_RESULT 	= array();

	function __construct()
	{
		unset($this->m_REQUEST); 
	}

	/*--------------------------------------------------*/
	/*																									*/
	/* 결제/취소 요청값 Set or Add                      */
	/*																									*/
 	/*--------------------------------------------------*/
	function SetField( $key, $val ) 
	{
		$this->m_REQUEST[$key] = $val;
	}

	/*--------------------------------------------------*/
	/*																									*/
	/* 결제/취소 결과값 fetch                           */
	/*																									*/
 	/*--------------------------------------------------*/
	function GetResult( $name ) 
	{
		$result = $this->m_Data->GetResult( $name );
		if( $result == "" )
			$result = $this->m_REQUEST[$name];
		return $result;
	}

	/*--------------------------------------------------*/
	/*																									*/
	/* 결제/취소 처리 메인                              */
	/*																									*/
 	/*--------------------------------------------------*/
	function startAction()
	{
		
		/*--------------------------------------------------*/
		/* Overhead Operation                               */
  	/*--------------------------------------------------*/
		$this->m_Data = new INIACTData( $this->m_REQUEST );
		
		/*--------------------------------------------------*/
		/* Log Start																				*/
   	/*--------------------------------------------------*/
		$this->m_Log = new INIACTLog( $this->m_REQUEST );
		if(!$this->m_Log->StartLog()) 
		{
			$this->MakeTXErrMsg( LOG_OPEN_ERR, "로그파일을 열수가 없습니다.[".$this->m_REQUEST["inipayhome"]."]"); 
			return;
		}

		/*--------------------------------------------------*/
		/* Logging Request Parameter												*/
   	/*--------------------------------------------------*/
		$this->m_Log->WriteLog( DEBUG, $this->m_REQUEST );

		$this->m_type = $this->m_REQUEST["type"];

		/*--------------------------------------------------*/
		/* Check Field																			*/
   	/*--------------------------------------------------*/
		if( !$this->m_Data->CheckField() )
		{
			$err_msg = "필수항목(".$this->m_Data->m_ErrMsg.")이 누락되었습니다.";
			$this->MakeTXErrMsg( $this->m_Data->m_ErrCode, $err_msg ); 
			$this->m_Log->WriteLog( ERROR, $err_msg );
			$this->m_Log->CloseLog( $this->GetResult(NM_RESULTMSG) );
			return;
		}
		$this->m_Log->WriteLog( INFO, "Check Field OK" );
		/*--------------------------------------------------*/
		// Make TID
		/*--------------------------------------------------*/
		if( !$this->m_Data->MakeTID() )
		{
			$err_msg = "Make TID Err[".$this->m_Data->m_sTID."]";
			$this->m_Log->WriteLog( ERROR, $err_msg );
			$this->MakeTXErrMsg( $rtv, $err_msg ); 
			$this->m_Log->CloseLog( $this->GetResult(NM_RESULTMSG) );
			return;
		}
		$this->m_Log->WriteLog( INFO, "Make TID OK:". $this->m_Data->m_sTID );

		/*--------------------------------------------------*/
		// Make Body
		/*--------------------------------------------------*/
		$this->m_Data->MakeBody();
		$this->m_Log->WriteLog( DEBUG, "Make Body OK:[". $this->m_Data->m_sBody."]" );
		$this->m_Log->WriteLog( INFO, "Make EncBody OK:". $this->m_Data->m_sEncBody."[".strlen($this->m_Data->m_sEncBody)."]" );
		/*--------------------------------------------------*/
		// Make Head
		/*--------------------------------------------------*/
		$this->m_Data->MakeHead();
		$this->m_Log->WriteLog( INFO, "Make Head OK:". $this->m_Data->m_sHead );
		/*--------------------------------------------------*/
		// Make Msg
		/*--------------------------------------------------*/
		$this->m_Data->MakeMsg();
		/*--------------------------------------------------*/
		// Connect
		/*--------------------------------------------------*/
		if( ($rtv = $this->m_Data->ConnSVR($this->m_REQUEST["GWS"])) != OK )
		{
			$err_msg = "Connect Err[".$this->m_Data->m_ACTServer."][".ACTSVR_PORT."]";
			$this->m_Log->WriteLog( ERROR, $err_msg );
			$this->MakeTXErrMsg( $rtv, $err_msg ); 
			$this->m_Log->CloseLog( $this->GetResult(NM_RESULTMSG) );
			return;
		}
		$this->m_Log->WriteLog( INFO, "CONNECT OK::".$this->m_Data->m_ACTServer.":".ACTSVR_PORT );
		/*--------------------------------------------------*/
		// Send
		/*--------------------------------------------------*/
		if( ($rtv = $this->m_Data->SendSVR()) != OK )
		{
			$err_msg = "Send Err";
			$this->m_Log->WriteLog( ERROR, $err_msg );
			$this->MakeTXErrMsg( $rtv, $err_msg ); 
			$this->m_Data->CloseSVR();
			$this->m_Log->CloseLog( $this->GetResult(NM_RESULTMSG) );
			return;
		}
		$this->m_Log->WriteLog( INFO, "SEND OK[".$this->m_Data->m_ACTReq."]" );
		/*--------------------------------------------------*/
		// Recv
		/*--------------------------------------------------*/
		if( ($rtv = $this->m_Data->RecvSVR()) != OK )
		{
			$err_msg = "Recv SVR Err[Header]:".$this->m_Data->m_ACTHeader;
			$this->m_Log->WriteLog( ERROR, $err_msg );
			$this->MakeTXErrMsg( $rtv, $err_msg ); 
			if( $this->m_Data->m_Type == "ACT") //계좌송금일 경우, 수신오류는 무조건 성공처리.
			{
				$this->m_Data->m_RESULT[NM_RESULTCODE]			= "0000";
				$this->m_Data->m_RESULT[NM_RESULTMSG]				= "정상처리";
			}
			$this->m_Data->CloseSVR();
			$this->m_Log->CloseLog( $this->GetResult(NM_RESULTMSG) );
			return;
		}
		$this->m_Log->WriteLog( INFO, "Recv OK[".$this->m_Data->m_ACTRes."]" );
		$this->m_Data->CloseSVR();
		/*--------------------------------------------------*/
		// Parse
		/*--------------------------------------------------*/
		if( ($rtv = $this->m_Data->ParseRecv()) != OK )
		{
			$err_msg = "Parse Err";
			$this->m_Log->WriteLog( ERROR, $err_msg );
			$this->MakeTXErrMsg( $rtv, $err_msg ); 
			if( $this->m_Data->m_Type == "ACT") //계좌송금일 경우, 수신오류는 무조건 성공처리.
			{
				$this->m_Data->m_RESULT[NM_RESULTCODE]			= "0000";
				$this->m_Data->m_RESULT[NM_RESULTMSG]				= "정상처리";
			}
			$this->m_Log->CloseLog( $this->GetResult(NM_RESULTMSG) );
			return;
		}
		$this->m_Log->WriteLog( INFO, "Parse OK" );

		$this->m_Log->CloseLog( $this->GetResult(NM_RESULTMSG) );
		return OK;
	}

	/*--------------------------------------------------*/
	/*																									*/
	/* 에러메세지 Make				                          */
	/*																									*/
 	/*--------------------------------------------------*/
	function MakeTXErrMsg($err_code, $err_msg)
	{
		$this->m_Data->m_RESULT[NM_RESULTCODE]			= $err_code;
		$this->m_Data->m_RESULT[NM_RESULTMSG]				= $err_msg;
		$this->m_Data->GTHR( $err_code, $err_msg );
		return;
	}

}

?>

<?php
//require_once('../stdpay/libs/INIStdPayUtil.php');
$SignatureUtil = new INIStdPayUtil();


//############################################
// 1.전문 필드 값 설정(***가맹점 개발수정***)
//아이디 : order00001
//패스워드 : stati0n1!
//이니라이트 키 생성 비밀번호 : stati0n1!
//이니라이트키 SmdtaGNmTzBwNXJMMEd0OTRwSXBHdz09
//빌링결제키 VkYwYUMwYTJITWE3SWIrK3BMN1FWZz09
//INIBillTst
//SU5JTElURV9UUklQTEVERVNfS0VZU1RS
//order00001
//VkYwYUMwYTJITWE3SWIrK3BMN1FWZz09
//############################################
// 여기에 설정된 값은 Form 필드에 동일한 값으로 설정
$mid 			= "order00001";  								// 가맹점 ID(가맹점 수정후 고정)					
$signKey 		= "VkYwYUMwYTJITWE3SWIrK3BMN1FWZz09"; 			// 가맹점에 제공된 키(이니라이트키) (가맹점 수정후 고정) !!!절대!! 전문 데이터로 설정금지
$timestamp 		= $SignatureUtil->getTimestamp();   			// util에 의해서 자동생성
$orderNumber 	= $od_id; 						// 가맹점 주문번호(가맹점에서 직접 설정)

//###################################
// 2. 가맹점 확인을 위한 signKey를 해시값으로 변경 (SHA-256방식 사용)
//###################################
$mKey 					= $SignatureUtil->makeHash($signKey, "sha256");
/*
 **** 위변조 방지체크를 signature 생성 ***
 * oid, price, timestamp 3개의 키와 값을
 * key=value 형식으로 하여 '&'로 연결한 하여 SHA-256 Hash로 생성 된값
 * ex) oid=INIpayTest_1432813606995&price=819000&timestamp=2012-02-01 09:19:04.004
 * key기준 알파벳 정렬
 * timestamp는 반드시 signature생성에 사용한 timestamp 값을 timestamp input에 그데로 사용하여야함
 */
$params = array(
    "oid" => $orderNumber,
    "price" => $tot_price,
    "timestamp" => $timestamp
);
$sign		= $SignatureUtil->makeSignature($params);

//$http_host 	= $_SERVER['HTTP_HOST'];
// 페이지 URL에서 고정된 부분을 적는다. 
// Ex) returnURL이 http://localhost:8082/demo/INIpayStdSample/INIStdPayReturn.jsp 라면
//                 http://localhost:8082/demo/INIpayStdSample 까지만 기입한다.

?>
<input type="hidden" name="version" value="1.0" >
<input type="hidden" name="mid" value="<?php echo $mid ?>" >
<input type="hidden" name="goodname" value="<?php echo $goods; ?>">
<input type="hidden" name="oid" value="<?php echo $od_id; ?>" >
<input type="hidden" name="price" value="<?php echo $tot_price; ?>">
<input type="hidden" name="currency" value="WON" >
<input type="hidden" name="buyername" value="" >
<input type="hidden" name="buyertel" value="" >
<input type="hidden" name="buyeremail" value="" >
<input type="hidden" name="timestamp" value="<?php echo $timestamp ?>" >
<input type="hidden" name="signature" value="<?php echo $sign ?>" >
<input type="hidden" name="returnUrl" value="<?php echo $returnUrl; ?>"> 

<input type="hidden"  name="mKey" value="<?php echo $mKey ?>" >
<input type="hidden" name="gopaymethod" value="" >
<input type="hidden" name="offerPeriod" value="<?php echo $offerPeriod;?>" >
<input type="hidden" id="acceptmethod" name="acceptmethod" value="billauth(card)" >
<input type="hidden" id="billPrint_msg" name="billPrint_msg" value="고객님은 오더스테이션에서 정기결제를 신청합니다." >

<input type="hidden" name="closeUrl"    value="<?php echo $closeUrl; ?>">
<input type="hidden" name="popupUrl"    value="<?php echo $popupUrl; ?>">

<input type="hidden" name="recvname"    value="">
<input type="hidden" name="recvtel"     value="">
<input type="hidden" name="recvaddr"    value="">
<input type="hidden" name="recvpostnum" value="">
 

<?php /* 주문폼 자바스크립트 에러 방지를 위해 추가함 */ ?>
<input type="hidden" name="good_mny"    value="<?php echo $tot_price; ?>">
<?php
if($default['de_tax_flag_use']) {
?>
<input type="hidden" name="comm_tax_mny"	  value="<?php echo $comm_tax_mny; ?>">         <!-- 과세금액    -->
<input type="hidden" name="comm_vat_mny"      value="<?php echo $comm_vat_mny; ?>">         <!-- 부가세	    -->
<input type="hidden" name="comm_free_mny"     value="<?php echo $comm_free_mny; ?>">        <!-- 비과세 금액 -->
<?php
}
?>

<?php if($default['de_tax_flag_use']) { ?>
<input type="hidden" name="tax"         value="<?php echo $comm_vat_mny; ?>">
<input type="hidden" name="taxfree"     value="<?php echo $comm_free_mny; ?>">
<?php } ?>
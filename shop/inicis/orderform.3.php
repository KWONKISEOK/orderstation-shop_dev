<?php
if (!defined("_GNUBOARD_")) exit; // 개별 페이지 접근 불가
?>

<div id="display_pay_button" class="btn_confirm">
	<? if ($member[choice_recommend_pharm_name] == "N" && $is_member == true) { ?>
		<strong>회원정보 누락으로 인해 주문불가합니다. 회원정보를 빠짐없이 입력해주세요.</strong>
	<?}else{?>
		
		<?
		//정기결제일때만 실행
		if( $period == "y" && $member['mb_type'] != "1" ){ 

			//결제내역 검사
			$sql2 = " select count(m_idx) as m_cnt from tbl_bill_info where mb_id = '{$member['mb_id']}' and delYN = 'N' and service_type is null ";
			$row2 = sql_fetch($sql2);

			if( $row2["m_cnt"] > 0 ){
		?>
			<span id="save_total_info">

				<div style="float:left;text-align:left;font-size:12px;margin-bottom:10px;">

					<? if( !empty( $member['od_billkey'] ) ){ ?>
					<div style="margin-bottom:10px;">
						<select name="select_bill_info" id="select_bill_info" style="width:360px;height:30px;margin-top:10px;">
							<option value="">---저장된 결제수단 선택---</option>
							<option value="1">[<?=GetBankName($member["cardCode"])?>]&nbsp;[<?=$member["cardNumber"]?>]</option>
							<option value="2">새 결제수단</option>
						</select>
						<div id="bill_txt"></div>
					</div>

					<? } ?>

					<? if( empty( $member['od_billkey'] ) ){ ?>
						<input type="checkbox" class="save_bill" value="Y"><strong>결제수단 저장</strong>
					<? }else{ ?>
						<input type="checkbox" class="save_bill" value="Y"><strong>결제수단 목록보기</strong>
					<? } ?>

					<div id="save_info" style="clear:both;margin-top:10px;width:100%;display:none;">
						<?
							if( !empty($member['od_billkey']) ){
								$TmpSql = " and od_billkey != '{$member['od_billkey']}' ";
							}
							$sql2 = " select * from tbl_bill_info where mb_id = '{$member['mb_id']}' and delYN = 'N' and service_type is null ".$TmpSql." order by regDate desc  ";
							$result2 = sql_query($sql2);
							
							$i = 0;
							for($i=0; $row2=sql_fetch_array($result2); $i++) {
						?>
							<div style="float:left;width:100%;text-align:left;font-size:12px;padding-left:8px;border:1px solid #eceff4;background:#fff;">
								[<?=GetBankName($row2["cardCode"])?>]&nbsp; 
								[<?=$row2["cardNumber"]?>]&nbsp;&nbsp;
								<a href="javascript:SaveInfo('<?=$row2["m_idx"]?>');" class="btn_frmline" style="height:25px;">기본결제수단으로</a>
								<a href="javascript:DelInfo('<?=$row2["m_idx"]?>');" class="btn_frmline" style="height:25px;">삭제</a>
							</div>
						<?
							}
						?>
						<? if( $i == 0 ){ ?>
							<div style="float:left;width:100%;height:30px;text-align:left;font-size:12px;padding-left:8px;padding-top:6px;border:1px solid #eceff4;background:#fff;">
								결제수단 데이터가 없습니다.
							</div>
						<? } ?>
					</div>

				</div>


			</span>
		<?
			}

		}
		?>

		<input type="button" value="주문하기" onclick="forderform_check(this.form);" class="btn_submit">
		
		<a href="javascript:history.go(-1);" class="btn01">취소</a>
	<?}?>
</div>
<div id="display_pay_process" style="display:none">
    <img src="<?php echo G5_URL; ?>/shop/img/loading.gif" alt="">
    <span>주문완료 중입니다. 잠시만 기다려 주십시오.</span>
</div>
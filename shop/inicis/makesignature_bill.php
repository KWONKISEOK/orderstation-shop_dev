<?php
include_once('./_common.php');
include_once(G5_LIB_PATH.'/json.lib.php');
include_once(G5_SHOP_PATH.'/settle_inicis.inc.php');


if($default['de_pg_service'] != 'inicis' && ! $default['de_inicis_lpay_use'] )
    die(json_encode(array('error'=>'올바른 방법으로 이용해 주십시오.')));


$orderNumber = get_session('ss_order_inicis_id');
$price = preg_replace('#[^0-9]#', '', $_POST['price']);


if(strlen($price) < 1)
    die(json_encode(array('error'=>'가격이 올바르지 않습니다.')));


//require_once('../stdpay/libs/INIStdPayUtil.php');
$SignatureUtil = new INIStdPayUtil();

//############################################
// 1.전문 필드 값 설정(***가맹점 개발수정***)
//아이디 : order00001
//패스워드 : stati0n1!
//이니라이트 키 생성 비밀번호 : stati0n1!
//이니라이트키 SmdtaGNmTzBwNXJMMEd0OTRwSXBHdz09
//빌링결제키 VkYwYUMwYTJITWE3SWIrK3BMN1FWZz09
//INIBillTst
//SU5JTElURV9UUklQTEVERVNfS0VZU1RS
//order00001
//VkYwYUMwYTJITWE3SWIrK3BMN1FWZz09
//############################################
// 여기에 설정된 값은 Form 필드에 동일한 값으로 설정
$mid 			= INICIS_R_MID;  						// 가맹점 ID(가맹점 수정후 고정)
$signKey 		= INICIS_R_SIGN_KEY; 	// 가맹점에 제공된 키(이니라이트키) (가맹점 수정후 고정) !!!절대!! 전문 데이터로 설정금지
$timestamp 		= $SignatureUtil->getTimestamp();   	// util에 의해서 자동생성

//###################################
// 2. 가맹점 확인을 위한 signKey를 해시값으로 변경 (SHA-256방식 사용)
//###################################
$mKey 			= $SignatureUtil->makeHash($signKey, "sha256");
/*
 **** 위변조 방지체크를 signature 생성 ***
 * oid, price, timestamp 3개의 키와 값을
 * key=value 형식으로 하여 '&'로 연결한 하여 SHA-256 Hash로 생성 된값
 * ex) oid=INIpayTest_1432813606995&price=819000&timestamp=2012-02-01 09:19:04.004
 * key기준 알파벳 정렬
 * timestamp는 반드시 signature생성에 사용한 timestamp 값을 timestamp input에 그데로 사용하여야함
 */
$params = array(
    "oid" => $orderNumber,
    "price" => $_POST['price'],
    "timestamp" => $timestamp
);
$sign		= $SignatureUtil->makeSignature($params);

//$http_host 	= $_SERVER['HTTP_HOST'];
// 페이지 URL에서 고정된 부분을 적는다. 
// Ex) returnURL이 http://localhost:8082/demo/INIpayStdSample/INIStdPayReturn.jsp 라면
//                 http://localhost:8082/demo/INIpayStdSample 까지만 기입한다.

die(json_encode(array('error'=>'', 'mKey'=>$mKey, 'timestamp'=>$timestamp, 'sign'=>$sign)));
?>
<?php
include_once('./_common.php');

if (G5_IS_MOBILE) {
    include_once(G5_MSHOP_PATH.'/orderinquiry.php');
    return;
}

define("_ORDERINQUIRY_", true);

$od_pwd = get_encrypt_string($od_pwd);

// 회원인 경우
if ($is_member)
{
    $sql_common = " from {$g5['g5_shop_order_table']} where mb_id = '{$member['mb_id']}' ";
}
else if ($od_id && $od_pwd) // 비회원인 경우 주문서번호와 비밀번호가 넘어왔다면
{
    $sql_common = " from {$g5['g5_shop_order_table']} where od_id = '$od_id' and od_pwd = '$od_pwd' ";
}
else // 그렇지 않다면 로그인으로 가기
{
    goto_url(G5_BBS_URL.'/login.php?url='.urlencode(G5_SHOP_URL.'/orderinquiry.php'));
}

// 테이블의 전체 레코드수만 얻음
$sql = " select count(*) as cnt " . $sql_common;
$row = sql_fetch($sql);
$total_count = $row['cnt'];

// 비회원 주문확인시 비회원의 모든 주문이 다 출력되는 오류 수정
// 조건에 맞는 주문서가 없다면
if ($total_count == 0)
{
    if ($is_member) // 회원일 경우는 메인으로 이동
        alert('주문이 존재하지 않습니다.', G5_SHOP_URL);
    else // 비회원일 경우는 이전 페이지로 이동
        alert('주문이 존재하지 않습니다.');
}

$rows = $config['cf_page_rows'];
$total_page  = ceil($total_count / $rows);  // 전체 페이지 계산
if ($page < 1) { $page = 1; } // 페이지가 없으면 첫 페이지 (1 페이지)
$from_record = ($page - 1) * $rows; // 시작 열을 구함




$g5['title'] = '정기주문내역';
include_once('./_head.php');

if(!G5_IS_MOBILE) {
	echo '<div class="site-wrap">
	<div id="aside">&nbsp;';
	include_once(G5_SHOP_PATH.'/asidemy.php');
	echo '</div><div id="container">';
}
if ((!$bo_table || $w == 's' ) && !defined('_INDEX_')) { ?><div id="wrapper_title"><?php echo $g5['title'] ?></div><?php } 

?>

<!-- 주문 내역 시작 { -->
<div id="sod_v">
    <p id="sod_v_info">정기주문 신청하신 내역을 조회하실 수 있습니다.</p>

    <?php
    $limit = " limit $from_record, $rows ";
?>


<div class="tbl_head03 tbl_wrap">
    <table>
    <thead>
    <tr>
        <th scope="col">상태</th>
        <th scope="col">결제주기</th>
		<th scope="col">상품명</th>
        <th scope="col">완료결제</th>
        <th scope="col">남은결제</th>
        <th scope="col">다음결제일</th>
		<th scope="col">상세</th>
    </tr>
    </thead>
    <tbody>
    
    <tr>
        <td>진행중</td>
		<td>1달(5회)</td>
		<td>이즈브레(먹는샘물)</td>
		<td>1회</td>
		<td>4회</td>
		<td>2018-07-20</td>
		<td>관리</td>
        
    </tr>

  
    </tbody>
    </table>
</div>
<!-- } 주문 내역 목록 끝 -->

    <?php echo get_paging($config['cf_write_pages'], $page, $total_page, "{$_SERVER['SCRIPT_NAME']}?$qstr&amp;page="); ?>
</div>
<!-- } 주문 내역 끝 -->

<?php
include_once('./_tail.php');
?>

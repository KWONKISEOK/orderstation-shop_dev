<?php
include_once('./_common.php');


$g5['title'] = "검사 결과지 확인";
include_once('./_head.php');

if(!G5_IS_MOBILE) {
	echo '<div class="menu-box" style="display:none;">';
	include_once(G5_SHOP_PATH.'/aside.php');
	echo '</div>';
	echo '<div class="site-wrap">';
	echo '<div id="containersub">';
}

if ($member["mb_id"] != "admin" && $member["mb_id"] != "bdadmin")
{
    alert('접근 가능 페이지가 아닙니다.');
}

// add_stylesheet('css 구문', 출력순서); 숫자가 작을 수록 먼저 출력됨
add_stylesheet('<link rel="stylesheet" href="'.$member_skin_url.'/style.css">', 0);

?>

<div class="xans-product-menupackage " style="margin-bottom:20px important; margin-left:4% !important;"><h2><span><?php echo $g5['title'] ?></span></h2></div>
<!-- 검사지확인 시작 { -->
<div id="sod_ws">

    <form name="frm" method="post" action="./regfile_search.php">
    

		<section id="fregister_private">
		

			<div style="text-align:center !important;">
			
			<!--<img src="./img/jang_app.jpg" alt="code_image" style="max-width: 100%; height: auto;"><br><br><br>-->

				<table style="margin-bottom:5% !important;">
					<thead>
					<tr>
						<th style='background-color:#ebebeb !important; border:none !important; color:black; height:35px; font-size:1.1em;'>기본정보입력</th>
					</tr>
					</thead>
					<tbody>
					<tr>
						<td>
							<input type="text" value="<?=$name?>" name="name" class="frm_input required" placeholder="성함을 입력해주세요." style="width:90%; text-align:center; font-size:1.1em; margin-bottom:20px; margin-top:20px;"/><br>
							<input type="text" value="<?=$birth?>" name="birth" class="frm_input required" placeholder="생년월일 6자리를 입력해주세요. ex ) 900101" style="width:90%; text-align:center; font-size:1.1em; margin-bottom:20px;"/><br>
							
						</td>
					</tr>
					</tbody>
				</table>

				
				<table>
					<thead>
					<tr>
						<th style='background-color:#ebebeb !important; border:none !important;color:black; height:35px; font-size:1.1em;'>바코드 정보 입력</th>
					</tr>
					</thead>
					<tbody>
					<tr>
						<td>
							<img src="./img/code_image.jpg" alt="code_image"><br>
							<input type="text" value="<?=$code?>" name="code" class="frm_input required" placeholder="바코드번호를 입력해주세요." style="width:90%; font-size:1.1em; text-align:center; margin-bottom:10px;margin-top:10px;"/><br><br>
							
							<strong>바코드 번호를 하이픈(-)도 포함하여 정확히 입력해주세요.<br><br></strong>
						</td>
					</tr>
					</tbody>
				</table>
				
				<div style="text-align:center !important;">
						<button type="button" style="background-color: #35B8EA; color: white; height:40px; width:30%;  border-radius:3px; color:white; font-size: 1.083em; font-weight: bold;" onclick="javascript:FileSearch();">결과 확인 ▶</button>
						<button type="button" style="background-color: gray; height:40px; width:30%;  border-radius:3px; color:white; font-size: 1.083em; font-weight: bold;" onclick="javascript:location.href='/shop/regfile_search.php';">모두 지우기</button><br><br>
				</div>
				<? 
					
					$where_sql = "";

					//admin , bdadmin 이 아닐때 파일명 정확하게 일치
					if( ( $member["mb_id"] != "admin" && $member["mb_id"] != "bdadmin" ) && $name != "" ){ 
						$where_sql = " and REPLACE(name , '.pdf' , '') = '".$name."' and REPLACE(code , '.pdf' , '') = '".$code."' and REPLACE(birth , '.pdf' , '') = '".$birth."' ";					
					}
					//admin ,  일때 like
					if( ( $member["mb_id"] == "admin" || $member["mb_id"] == "bdadmin" ) && $name != "" ){ 
						$where_sql = " and instr(file_name , '".$name."') > 0 ";					
					}
					if( ( $member["mb_id"] == "admin" || $member["mb_id"] == "bdadmin" ) ){
						$sql = " select * from tbl_regfile where 1=1 ".$where_sql." ";
						$result = sql_query($sql);		
					}
					if( ( $member["mb_id"] != "admin" && $member["mb_id"] != "bdadmin" ) && $name != "" ){
						$sql = " select * from tbl_regfile where 1=1 ".$where_sql." ";
						$result = sql_query($sql);		
					}
					if( ( $member["mb_id"] != "admin" && $member["mb_id"] != "bdadmin" ) && $name == "" ){
						$sql = " select * from tbl_regfile where 1=2 ";
						$result = sql_query($sql);		
					}

				?>
				<br><br>
				<table>
					<thead>
					<tr>
						<th colspan="2" style='background-color:#35B8EA !important; border:none !important; margin-bottom:10%; color:#ffffff; height:35px; font-size:1.1em;'>조회결과</th>
					</tr>
					</thead>
					<tbody>
					<? if( $result->num_rows > 0 ){ ?>
						<? for ($i=0; $row=sql_fetch_array($result); $i++) { ?>
						<tr>
							<td style="border-bottom:1px solid #fff;padding:4px;">
								<b><?=$row["file_name"]?>
							</td>
							<td rowspan="2" style="padding:4px;">
								<button type="button" style="background-color:gray; width:90%; height:90%;  padding:2%; border-radius:3px; color:white; font-size: 1.083em; font-weight: bold;" onclick="javascript:FileDown(<?=$row["file_idx"]?>);">다운로드</button>				
							</td>
						</tr>
						<tr>
							<td style="padding:4px;">
							업로드일시 : <?=$row["reg_time"]?>
							</td>
						</tr>
						<? } ?>
					<?}else{?>
						<tr><td style="height:100px !important;">검사결과가 없습니다.</td></tr>
					<? } ?>
					</tbody>
				</table>

			</div>

		</section>


    </form>

	<form name="filefrm">
		<input type="hidden" name="file_idx" value=""/>
	</form>

</div>

<script>

function FileSearch(){
	
	var frm = document.frm;
	/*
	if( frm.file_name.value == ""){
		alert("검사코드를 입력해주세요");
		return;
	}*/
	frm.submit();

}

function FileDown( GetIdx ){

	var frm = document.filefrm;

	frm.file_idx.value = GetIdx;
	frm.method = "post";
	frm.action = "./regfile_download.php";
	frm.submit();


}

</script>

<?php
include_once('./_tail.php');
?>
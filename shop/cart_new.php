<?php
include_once('./_common.php');
include_once(G5_SHOP_PATH.'/settle_naverpay.inc.php');

$period = $_REQUEST[period];

// 보관기간이 지난 상품 삭제
cart_item_clean();

// cart id 설정
set_cart_id($sw_direct);

$s_cart_id = get_session('ss_cart_id');
// 선택필드 초기화
$sql = " update {$g5['g5_shop_cart_table']} set ct_select = '0' where od_id = '$s_cart_id' ";
sql_query($sql);

$cart_action_url = G5_SHOP_URL.'/cartupdate.php';

if (G5_IS_MOBILE) {
    include_once(G5_MSHOP_PATH.'/cart.php');
    return;
}

// 테마에 cart.php 있으면 include
if(defined('G5_THEME_SHOP_PATH')) {
    $theme_cart_file = G5_THEME_SHOP_PATH.'/cart.php';
    if(is_file($theme_cart_file)) {
        include_once($theme_cart_file);
        return;
        unset($theme_cart_file);
    }
}

$g5['title'] = '장바구니';
include_once('./_head.php');


if(!G5_IS_MOBILE) {
	echo '<div class="menu-box" style="display:none;">';
	include_once(G5_SHOP_PATH.'/aside.php');
	echo '</div>';
	echo '<div class="site-wrap">';
	echo '<div id="containersub">';
}

if($member[mb_type] != 1) {
?>

<br />
<div class="xans-element- xans-order xans-order-tabinfo ec-base-tab  ">
<ul class="menu">
	<li class="<?php if($period != 'y') echo 'selected';?> "><a href="/shop/cart.php">일반주문 </a></li>
	<li class="<?php if($period == 'y') echo 'selected';?>"><a href="/shop/cart.php?period=y">정기주문</a></li>
</ul>
</div>

<?php } ?>


<!-- 장바구니 시작 { -->
<script src="<?php echo G5_JS_URL; ?>/shop.js?ver=<?php echo G5_JS_VER; ?>"></script>
<script src="<?php echo G5_JS_URL; ?>/shop.override.js?ver=<?php echo G5_JS_VER; ?>"></script>
<style>
.mod_options_auto {
    padding: 0 7px;
    border: 1px solid #38b2b9;
    color: #38b2b9;
    background: #fff;
    height: 23px;
    margin: 4px 0 0;
}

</style>
<div id="sod_bsk" class="od_prd_list">

	<div class="xans-product-menupackage "><h2><span><?php echo $g5['title'] ?></span>&nbsp;&nbsp;
	<?php if($period == 'y') {?>
		<small >정기주문은 1개의 상품씩 주문 가능 합니다.</small>
	<?php } ?></h2>
	
	</div>
    <form name="frmcartlist" id="sod_bsk_list" class="2017_renewal_itemform" method="post" action="<?php echo $cart_action_url; ?>">
    <div class="tbl_head03 tbl_wrap">
        <table>
        <thead>
        <tr>
            <th scope="col">
                <label for="ct_all" class="sound_only">상품 전체</label>
                <input type="checkbox" name="ct_all" value="1" id="ct_all" checked="checked">
            </th>
            <th scope="col">상품명</th>
            <th scope="col">총수량</th>
            <th scope="col">소비자가</th>
            <!-- <th scope="col">포인트</th> -->
            <th scope="col">배송비</th>
            <th scope="col">소계</th>
        </tr>
        </thead>
        <tbody>
        <?php

		//배송비계산
        $tot_point = 0;
        $tot_sell_price = 0;

		//장바구니 검사
		$result = c_mysqli_call('SP_ORDER_INFO', " '".$s_cart_id."','CART','".$member['mb_type']."','".$period."' ");

        $it_send_cost = 0;
		$one_trans = true;

		$i=0;
		foreach($result as $row) {
            // 합계금액 계산
            $sql = " select SUM(IF(io_type = 1, (io_price * ct_qty), ((ct_price + io_price) * ct_qty))) as price,
                            SUM(ct_point * ct_qty) as point,
							SUM((select it_incen * a.ct_qty from tbl_shop_item where it_id = a.it_id))  as it_incen, 
                            SUM((select io_incen * a.ct_qty from tbl_shop_item_option where io_id = a.io_id limit 1) ) as io_incen, SUM(ct_qty) as qty,
                            SUM(ct_qty) as qty
                        from {$g5['g5_shop_cart_table']} a
                        where it_id = '{$row['it_id']}'
                          and od_id = '$s_cart_id' ";
            $sum = sql_fetch($sql);

            if ($i==0) { // 계속쇼핑
                $continue_ca_id = $row['ca_id'];
            }

            $a1 = '<a href="./item.php?it_id='.$row['it_id'].'" class="prd_name"><b>';
            $a2 = '</b></a>';
            $image = get_it_image($row['it_id'], 80, 80);

            $it_name = $a1 . stripslashes($row['it_name']) . $a2;
            $it_options = print_item_options($row['it_id'], $s_cart_id);
			$ct_period_yn = $row['ct_period_yn'];
			$period_cnt	=0;
			$period_cnt		= $row['ct_period_cnt'];
            if($it_options) {
				if($ct_period_yn == 'Y') {
					$mod_options = '<div class="sod_option_btn"><button type="button" class="mod_options">선택사항수정</button> <div class="sod_option_btn"><button type="button" class="mod_options_auto">정기주문수정</button></div> </div>';
				} else {
					$mod_options = '<div class="sod_option_btn"><button type="button" class="mod_options">선택사항수정</button></div>';
				}
                
                $it_name .= '<div class="sod_opt">'.$it_options.'</div>';
            }

			//$ct_period_yn = '<div class="sod_option_btn"><button type="button" class="mod_options_auto">정기주문</button></div>';

            // 배송비
            switch($row['ct_send_cost'])
            {
                case 1:
                    $ct_send_cost = '착불';
                    break;
                case 2:
                    $ct_send_cost = '무료';
                    break;
                default:
                    $ct_send_cost = '주문시결제';
                    break;
            }

			$sendcost = $row['cal_it_sc_price'];
			
			$total_cost = $total_cost + $sendcost;

            //정기주문 예외처리 상품
			$optionprice = get_option_dis_price($row['it_id'], $row['io_id'],$row['io_type']);	
			
			if($optionprice > 0) {
				$sell_price = $sum['qty'] * ($row['ct_price']+$optionprice);
			} else {
				if($period_cnt > 0) {
					$sell_price = $sum['price'] * $period_cnt;
					$incen_sum = ($sum['it_incen']+$sum['io_incen']) * $period_cnt;
				} else {
					$sell_price = $sum['price'];
					$incen_sum = $sum['it_incen']+$sum['io_incen'];
				}
			}
        ?>
        <tr>
            <td class="td_chk">
                <label for="ct_chk_<?php echo $i; ?>" class="sound_only">상품</label>
                <input type="checkbox" name="ct_chk[<?php echo $i; ?>]" value="1" id="ct_chk_<?php echo $i; ?>" checked="checked">
            </td> 
            
            <td  class="td_prd">
                <div class="sod_img"><a href="./item.php?it_id=<?php echo $row['it_id']; ?>"><?php echo $image; ?></a></div>
                <div class="sod_name">
                    <input type="hidden" name="it_id[<?php echo $i; ?>]"    value="<?php echo $row['it_id']; ?>">
                    <input type="hidden" name="it_name[<?php echo $i; ?>]"  value="<?php echo get_text($row['it_name']); ?>">
                    <?php echo $it_name.$mod_options; ?>
					
                </div>
            </td>
            <td class="td_num"><?php echo number_format($sum['qty']); ?></td>
            <td class="td_numbig text_right">
				<?php 
					if($optionprice > 0) {
						echo number_format($row['ct_price']+$optionprice);
					} else {
						echo number_format($row['ct_price']+$row['io_price']);
					}
				?>
				<?php if($member['mb_type']==1) { ?>
					<br/>(<?php echo number_format($sum['it_incen']+$sum['io_incen']); ?>)
				<?php } ?>			
			</td>
            <!-- <td class="td_numbig text_right"><?php echo number_format($point); ?></td> -->
            <td class="td_dvr"><?php echo $ct_send_cost; ?>
				<br/><?php if($sendcost>0) echo number_format($sendcost); ?>
			</td>
            <td class="td_numbig text_right"><span id="sell_price_<?php echo $i; ?>" class="total_prc"><?php echo number_format($sell_price); ?></span>
			<?php if($member['mb_type']==1) { ?>
				<br/>(<?php echo number_format($incen_sum); ?>)
			<?php } ?>			
			</td>

        </tr>

        <?php
            $tot_point      += $point;
            $tot_sell_price += $sell_price;

			$i++;
        } // for 끝

        if ($i == 0) {
            echo '<tr><td colspan="8" class="empty_table">장바구니에 담긴 상품이 없습니다.</td></tr>';
        } else {
            // 배송비 계산
            ///$send_cost = get_sendcost($s_cart_id, 0);
        }
        ?>
        </tbody>
        </table>
        <div class="btn_cart_del">
            <button type="button" onclick="return form_check('seldelete');">선택삭제</button>
            <button type="button" onclick="return form_check('alldelete');">비우기</button>
        </div>
    </div>

    <?php
    $tot_price = $tot_sell_price + $sendcost; // 총계 = 주문상품금액합계 + 배송비
    if ($tot_price > 0 || $sendcost > 0) {
    ?>
    <div id="sod_bsk_tot">
        <ul>
            <li class="sod_bsk_dvr">
                <span>배송비</span>
                <strong><?php echo number_format($total_cost); ?></strong> 원
            </li>

            <li class="sod_bsk_pt">
                <span>&nbsp;</span>
                <strong>&nbsp;</strong>
            </li>

            <li class="sod_bsk_cnt">
                <span>총계 가격</span>
                <strong><?php echo number_format($tot_price); ?></strong> 원 
            </li>
         
        </ul>
    </div>
    <?php } ?>

    <div id="sod_bsk_act">
        <?php if ($i == 0) { ?>
        <a href="<?php echo G5_SHOP_URL; ?>/" class="btn01">쇼핑 계속하기</a>
        <?php } else { ?>
        <input type="hidden" name="url" value="./orderform.php">
        <input type="hidden" name="records" value="<?php echo $i; ?>">
        <input type="hidden" name="act" value="">
		<input type="hidden" name="ct_period_yn" value="<?php echo $period;?>">
        <a href="<?php echo G5_SHOP_URL; ?>/list.php?ca_id=<?php echo $continue_ca_id; ?>" class="btn01">쇼핑 계속하기</a>
        <button type="button" onclick="return form_check('buy');" class="btn_submit"><i class="fa fa-credit-card" aria-hidden="true"></i> 주문하기</button>

        <?php if ($naverpay_button_js) { ?>
        <div class="cart-naverpay"><?php echo $naverpay_request_js.$naverpay_button_js; ?></div>
        <?php } ?>
        <?php } ?>
    </div>

    </form>

</div>

<script>
$(function() {
    var close_btn_idx;

    // 선택사항수정
    $(".mod_options").click(function() {
        var it_id = $(this).closest("tr").find("input[name^=it_id]").val();
        var $this = $(this);
        close_btn_idx = $(".mod_options").index($(this));

        $.post(
            "./cartoption.php",
            { it_id: it_id },
            function(data) {
                $("#mod_option_frm").remove();
                $this.after("<div id=\"mod_option_frm\"></div>");
                $("#mod_option_frm").html(data);
                price_calculate();
            }
        );
    });
	// 정기주문
    $(".mod_options_auto").click(function() {
        var it_id = $(this).closest("tr").find("input[name^=it_id]").val();
        var $this = $(this);
        close_btn_idx = $(".mod_options_auto").index($(this));

        $.post(
            "./cartoption_auto.php",
            { it_id: it_id },
            function(data) {
                $("#mod_option_frm").remove();
                $this.after("<div id=\"mod_option_frm\"></div>");
                $("#mod_option_frm").html(data);
                //price_calculate();
            }
        );
    });

    // 모두선택
    $("input[name=ct_all]").click(function() {
        if($(this).is(":checked"))
            $("input[name^=ct_chk]").attr("checked", true);
        else
            $("input[name^=ct_chk]").attr("checked", false);
    });

    // 옵션수정 닫기
    $(document).on("click", "#mod_option_close", function() {
        $("#mod_option_frm").remove();
        $(".mod_options").eq(close_btn_idx).focus();
    });
    $("#win_mask").click(function () {
        $("#mod_option_frm").remove();
        $(".mod_options").eq(close_btn_idx).focus();
    });

});

function fsubmit_check(f) {
    if($("input[name^=ct_chk]:checked").length < 1) {
        alert("구매하실 상품을 하나이상 선택해 주십시오.");
        return false;
    }

    return true;
}

function form_check(act) {
    var f = document.frmcartlist;
    var cnt = f.records.value;

    if (act == "buy")
    {
        if($("input[name^=ct_chk]:checked").length < 1) {
            alert("주문하실 상품을 하나이상 선택해 주십시오.");
            return false;
        }
		<?php if($member['mb_type'] != 1 and $period=='y') { ?>
		if($("input[name^=ct_chk]:checked").length > 1) {
            alert("정기주문은 한상품씩 주문가능합니다.");
            return false;
        }
		<?php } ?>

        f.act.value = act;
        f.submit();
    }
    else if (act == "alldelete")
    {
        f.act.value = act;
        f.submit();
    }
    else if (act == "seldelete")
    {
        if($("input[name^=ct_chk]:checked").length < 1) {
            alert("삭제하실 상품을 하나이상 선택해 주십시오.");
            return false;
        }

        f.act.value = act;
        f.submit();
    }

    return true;
}
</script>
<!-- } 장바구니 끝 -->

<?php
echo '</div>';
include_once('./_tail.php');
?>
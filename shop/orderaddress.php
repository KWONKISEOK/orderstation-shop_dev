<?php
include_once('./_common.php');

if(!$is_member)
    alert_close('회원이시라면 회원로그인 후 이용해 주십시오.');

$multi_gubun = $_REQUEST[multi_gubun]; //다중배송여부

if($w == 'd') {
    $sql = " delete from {$g5['g5_shop_order_address_table']} where mb_id = '{$member['mb_id']}' and ad_id = '$ad_id' ";
    sql_query($sql);
    goto_url(G5_SHOP_URL.'/orderaddress.php?multi_gubun='.$multi_gubun);
}

$sql_common = " from {$g5['g5_shop_order_address_table']} where mb_id = '{$member['mb_id']}' ";

$sql = " select count(ad_id) as cnt " . $sql_common;
$row = sql_fetch($sql);
$total_count = $row['cnt'];

$rows = $config['cf_page_rows'];
$total_page  = ceil($total_count / $rows);  // 전체 페이지 계산
if ($page < 1) { $page = 1; } // 페이지가 없으면 첫 페이지 (1 페이지)
$from_record = ($page - 1) * $rows; // 시작 열을 구함

$sql = " select *
            $sql_common
            order by ad_default desc, ad_id desc
            limit $from_record, $rows";

$result = sql_query($sql);

if(!sql_num_rows($result))
//    alert_close('배송지 목록 자료가 없습니다.');

$order_action_url = G5_HTTPS_SHOP_URL.'/orderaddressupdate.php';

if (G5_IS_MOBILE) {
    include_once(G5_MSHOP_PATH.'/orderaddress.php');
    return;
}

// 테마에 orderaddress.php 있으면 include
if(defined('G5_THEME_SHOP_PATH')) {
    $theme_orderaddress_file = G5_THEME_SHOP_PATH.'/orderaddress.php';
    if(is_file($theme_orderaddress_file)) {
        include_once($theme_orderaddress_file);
        return;
    }
}

$g5['title'] = '배송지 목록';
include_once(G5_PATH.'/head.sub.php');
?>

<style>
    table td {text-align: center;}
</style>
<form name="forderaddress" method="post" action="<?php echo $order_action_url; ?>" autocomplete="off">
    <input type="hidden" name="mode" value="list">
    <div id="sod_addr" class="new_win">

        <h1 id="win_title"><i class="fa fa-address-book-o" aria-hidden="true"></i> 배송지 목록 <span id="order_qty"></span></h1>
        <div class="new_win_con">
            <div class="tbl_head01 tbl_wrap">
                <table>
                <thead>
                <tr>
                    <? if( $multi_gubun == "Y" ){ ?>
                        <th scope="col" width="30"></th>
                    <? } ?>
                    <th scope="col">배송지명</th>
                    <th scope="col" width="100">이름</th>
                    <th scope="col" width="120">연락처</th>
                    <th scope="col">주소</th>
                    <th scope="col">관리</th>
                </tr>

                </thead>
                <tbody>
                <?php
                $sep = chr(30);
                for($i=0; $row=sql_fetch_array($result); $i++) {
                    $addr = $row['ad_name'].$sep.$row['ad_tel'].$sep.$row['ad_hp'].$sep.$row['ad_zip1'].$sep.$row['ad_zip2'].$sep.$row['ad_addr1'].$sep.$row['ad_addr2'].$sep.$row['ad_addr3'].$sep.$row['ad_jibeon'].$sep.$row['ad_subject'];
                    $addr = get_text($addr);
                ?>
                <tr>
                    <? if( $multi_gubun == "Y" ){ ?>
                        <td scope="col"><input type="checkbox" name="chk" class="chk"></td>
                    <? } ?>
                    <td style="display: none;">
                        <input type="hidden" name="ad_id[<?php echo $i; ?>]" value="<?php echo $row['ad_id'];?>">
                        <label for="chk_<?php echo $i;?>" class="sound_only">배송지선택</label>
                        <input type="checkbox" name="chk[]" value="<?php echo $i;?>" id="chk_<?php echo $i;?>">
                    </td>
                    <td class="td_sbj">
                        <label for="ad_subject<?php echo $i;?>" class="sound_only">배송지명</label>
                        <span id="ad_subject<?php echo $i;?>"><?php echo str_empty_chk(get_text($row['ad_subject'])); ?></span>
                        <br><font color="red"><?php if($row['ad_default']) echo "(기본배송지)";?></font>
                    </td>

                    <td class="td_name"><?php echo get_text($row['ad_name']); ?></td>
                    <td class="td_hp"><?php echo get_text($row['ad_hp']); ?></td>
                    <td class="td_address">
                        <?php echo print_address($row['ad_addr1'], $row['ad_addr2'], $row['ad_addr3'], $row['ad_jibeon']); ?><br>
                    </td>
                    <td class="td_mng">
                        <input type="hidden" value="<?php echo $addr; ?>" class="hidden_addr">
                        <input type="hidden" value="<?php echo $row['ad_id']; ?>" class="hidden_aid">
                        <?php if ($multi_gubun == "Y") { ?>
                            <a href="javascript:;" class="mng_btn default_lb" onclick="RegMultiReceiverConfirm(this);">선택</a>
                        <?php } else { ?>
                            <a href="javascript:;" class="mng_btn sel_address">선택</a>
                        <?php } ?>
                        <a href="./orderaddresswrite.php?ad_id=<?php echo $row['ad_id']; ?>&multi_gubun=<?php echo $multi_gubun?>" class="mng_btn default_lb">수정</a>
                        <a href="<?php echo $_SERVER['SCRIPT_NAME']; ?>?gubun=2&multi_gubun=<?php echo $multi_gubun?>&w=d&amp;ad_id=<?php echo $row['ad_id']; ?>" class="mng_btn del_address">삭제</a>
                    </td>
                </tr>

                <?php
                }
                ?>
                
                <?php if ($total_count == 0) { ?>
                    <tr>
                        <td colspan="7">등록된 배송지가 없습니다</td>
                    </tr>
                <?php } ?>
                </tbody>
                </table>
            </div>

            <div class="win_btn">
                <? if ($multi_gubun == "Y") { ?>
                    <a href="javascript:RegMultiReceiver();" class="btn_sel " style="line-height: 40px; margin-left: 10px;">선택</a>
                <? } ?>
                <a href="./orderaddresswrite.php?multi_gubun=<?=$multi_gubun?>" class="btn_add" style="line-height: 40px;">신규등록</a>
                <button type="button" onclick="self.close();" class="btn_close">닫기</button>
            </div>
        </div>
    </div>
</form>

<?php echo get_paging($config['cf_write_pages'], $page, $total_page, "{$_SERVER['SCRIPT_NAME']}?$qstr&amp;page="); ?>

<script>
    $(function() {
        $(".sel_address").on("click", function() {
            var addr = $(this).siblings("input").val().split(String.fromCharCode(30));

            var f = window.opener.forderform;
            f.od_b_name.value        = addr[0];
            //f.od_b_tel.value         = addr[1];
            f.od_b_hp.value          = addr[2];
            f.od_b_zip.value         = addr[3] + addr[4];
            f.od_b_addr1.value       = addr[5];
            f.od_b_addr2.value       = addr[6];
            f.od_b_addr3.value       = addr[7];
            f.od_b_addr_jibeon.value = addr[8];
            f.ad_subject.value       = addr[9];

            var zip1 = addr[3].replace(/[^0-9]/g, "");
            var zip2 = addr[4].replace(/[^0-9]/g, "");

            if(zip1 != "" && zip2 != "") {
                var code = String(zip1) + String(zip2);

                if(window.opener.zipcode != code) {
                    window.opener.zipcode = code;
                   // window.opener.calculate_sendcost(code);
                }
            }

            window.close();
        });

        $(".del_address").on("click", function() {
            return confirm("배송지 목록을 삭제하시겠습니까?");
        });

        // 전체선택 부분
        $("#chk_all").on("click", function() {
            if($(this).is(":checked")) {
                $("input[name^='chk[']").attr("checked", true);
            } else {
                $("input[name^='chk[']").attr("checked", false);
            }
        });

        $("input[name=act_button]").on("click", function() {
            if($("input[name^='chk[']:checked").length==0 ){
                alert("수정하실 항목을 하나 이상 선택하세요.");
                return false;
            }
        });

    });

    // 다중배송지 개별 선택
    function RegMultiReceiverConfirm(target){

        // 선택한 행의 전체 선택자
        var ts = $(target).closest('tr');

        var TmpHtml = "";
        var MultiSum = 0;
        var order_cnt = parseInt($(opener.document).find("#order_qty").val());
        var aid = "";
        var aidChk = 0;

        var parent_sum = 0;
        var i=1;

         var addr = ts.find(".hidden_addr").val().split(String.fromCharCode(30));

        // 중복 주소지 추가 체크
        aid = ts.find(".hidden_aid").val();
        $(opener.document).find('input[name=multi_aid]').each(function (k, v) {
            if (v.value == aid) {
                aidChk++;
            }
        });


        TmpHtml += "<tr id='row_"+i+"'>";
        TmpHtml += " <td align='center' style='text-align:center;'>";
        TmpHtml += "    <input type='hidden' name='multi_aid' value='"+aid+"'/>";
        TmpHtml += "    <input type='checkbox' name='multi_chk'/>";
        TmpHtml += " </td>";
        TmpHtml += " <td align='center' style='text-align:center;'>";
        TmpHtml += " <input type='text' name='multi_cnt[]' class='multi_cnt' style='width:30px;padding:0px;' onKeyPress=onlyNumber(this) maxlength='4' value='1'/> ";
        TmpHtml += " </td>";
        TmpHtml += " <td align='center'> ";
        TmpHtml += "	<input type='hidden' name='multi_b_name[]' value='"+addr[0]+"'/> "+ addr[0] ;
        TmpHtml += " </td>";
        TmpHtml += " <td align='center'>";
        TmpHtml += "	<input type='hidden' name='multi_b_hp[]' value='"+addr[2]+"'/> "+ addr[2] ;
        TmpHtml += " </td>";
        TmpHtml += " <td align='left'>";
        TmpHtml += "	<input type='hidden' name='multi_b_zip[]' value='"+addr[3]+addr[4]+"'/> "+ addr[3]+addr[4] ;
        TmpHtml += "	<input type='hidden' name='multi_b_addr1[]' value='"+addr[5]+"'/> "+ addr[5] ;
        TmpHtml += "	<input type='hidden' name='multi_b_addr2[]' value='"+addr[6]+"'/> "+ addr[6] ;
        TmpHtml += "	<input type='hidden' name='multi_b_addr3[]' value='"+addr[7]+"'/> "+ addr[7] ;
        TmpHtml += "	<input type='hidden' name='multi_b_addr_jibeon[]' value='"+addr[7]+"'/> ";
        TmpHtml += "	<input type='hidden' name='multi_ad_subject[]' value='"+addr[7]+"'/> ";
        TmpHtml += " </td>";
        TmpHtml += "</tr>";


        if (aidChk > 0) {
            alert("이미 추가된 배송지입니다. 주문서를 확인해주세요.");
            return;
        }
        $(opener.document).find('#multi_table > tbody:last').append(TmpHtml);
        $(opener.document).find("#multAdd").prop("type","button");
        $(opener.document).find(".multi_cnt").prop("type", "text");
        $(opener.document).find(".m_cnt").remove();
        $(opener.document).find("#multiChk").val("0");

        self.close();

    }

    // 다중배송지 리스트 체크 선택
    function RegMultiReceiver(){

        var addr = "";
        var TmpHtml = "";
        var MultiSum = 0;
        var ErrorChk = 0;
        var order_cnt = parseInt($(opener.document).find("#order_qty").val());

        var aid = "";
        var aidChk = 0;

        var parent_sum = 0;
        var i=1;


        $("input[name=chk]:checkbox").each(function(index, item) {

            if( $("input:checkbox[name=chk]").eq(index).prop("checked") ){

                addr = $(document).find(".hidden_addr").eq(index).val().split(String.fromCharCode(30));

                // 중복 주소지 추가 체크
                aid = $(document).find(".hidden_aid").eq(index).val();
                $(opener.document).find('input[name=multi_aid]').each(function(k, v) {
                    if (v.value == aid) {
                        aidChk++;
                    }
                });


                TmpHtml += "<tr id='row_"+i+"'>";
                TmpHtml += " <td align='center' style='text-align:center;'>";
                TmpHtml += "    <input type='hidden' name='multi_aid' value='"+aid+"'/>";
                TmpHtml += "    <input type='checkbox' name='multi_chk'/>";
                TmpHtml += " </td>";
                TmpHtml += " <td align='center' style='text-align:center;'>";
                TmpHtml += " <input type='text' name='multi_cnt[]' class='multi_cnt'  style='width:30px;padding:0px;' onKeyPress=onlyNumber(this) maxlength='4' value='1'/> ";
                TmpHtml += " </td>";
                TmpHtml += " <td align='center'> ";
                TmpHtml += "	<input type='hidden' name='multi_b_name[]' value='"+addr[0]+"'/> "+ addr[0] ;
                TmpHtml += " </td>";
                TmpHtml += " <td align='center'>";
                TmpHtml += "	<input type='hidden' name='multi_b_hp[]' value='"+addr[2]+"'/> "+ addr[2] ;
                TmpHtml += " </td>";
                TmpHtml += " <td align='left'>";
                TmpHtml += "	<input type='hidden' name='multi_b_zip[]' value='"+addr[3]+addr[4]+"'/> "+ addr[3]+addr[4] ;
                TmpHtml += "	<input type='hidden' name='multi_b_addr1[]' value='"+addr[5]+"'/> "+ addr[5] ;
                TmpHtml += "	<input type='hidden' name='multi_b_addr2[]' value='"+addr[6]+"'/> "+ addr[6] ;
                TmpHtml += "	<input type='hidden' name='multi_b_addr3[]' value='"+addr[7]+"'/> "+ addr[7] ;
                TmpHtml += "	<input type='hidden' name='multi_b_addr_jibeon[]' value='"+addr[7]+"'/> ";
                TmpHtml += "	<input type='hidden' name='multi_ad_subject[]' value='"+addr[7]+"'/> ";
                TmpHtml += " </td>";
                TmpHtml += "</tr>";

                i++;

                ErrorChk++;
            }

        });

        if (ErrorChk == 0) {
            alert("선택된 배송지가 없습니다. 배송지를 체크해주세요.");
            return;
        }

        if (aidChk > 0) {
            alert("이미 추가된 배송지가 있습니다. 주문서를 확인해주세요.");
            return;
        }

        if (order_cnt < ( MultiSum + parent_sum )) {
            alert("총 주문수량보다 상품수량의 합이 큽니다.");
            return;
        }

        //$(opener.document).find('#multi_table > tbody').empty();
        $(opener.document).find('#multi_table > tbody:last').append(TmpHtml);
        $(opener.document).find("#multAdd").prop("type","button");
        $(opener.document).find(".multi_cnt").prop("type", "text");
        $(opener.document).find(".m_cnt").remove();
        $(opener.document).find("#multiChk").val("0");

        self.close();

    }

    <? if( $multi_gubun == "Y" ){ ?>
    var order_qty = $(opener.document).find('#order_qty').val();
    $("#order_qty").html("(장바구니 담은갯수 : "+order_qty+")");

    /*var now_qty = 0;
    $(opener.document).find(".multi_cnt").each(function(index, item) {
        now_qty += parseInt( $(opener.document).find(".multi_cnt").eq(index).val() );
    });

    if (now_qty > 0) {
        $("#order_qty").html("(장바구니 담은갯수 : "+now_qty+"/"+order_qty+")");
    } else {
        $("#order_qty").html("(장바구니 담은갯수 : "+order_qty+")");
    }*/

    <? } ?>
</script>

<?php
include_once(G5_PATH.'/tail.sub.php');
?>
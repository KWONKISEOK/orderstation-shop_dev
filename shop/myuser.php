<?php
include_once('./_common.php');

if (G5_IS_MOBILE) {
    include_once(G5_MSHOP_PATH.'/myuser.php');
    return;
}

define("_ORDERINQUIRY_", true);

//$od_pwd = get_encrypt_string($od_pwd);


$mb_name = $_POST['mb_name'];
$mb_id = $member['mb_id'];
// 회원인 경우
if ($is_member)
{
    $sql_common = " from {$g5['member_table']} c where mb_recommend = '$mb_id ' ";
}
else // 그렇지 않다면 로그인으로 가기
{
    goto_url(G5_BBS_URL.'/login.php?url='.urlencode(G5_SHOP_URL.'/orderinquiry.php'));
}

if($mb_name) $sql_common .= " and mb_name like '%".$mb_name."%'";

// 테이블의 전체 레코드수만 얻음
$sql = " select count(*) as cnt " . $sql_common;
$row = sql_fetch($sql);
$total_count = $row['cnt'];

$rows = 10;//$config['cf_page_rows'];
$total_page  = ceil($total_count / $rows);  // 전체 페이지 계산
if ($page < 1) { $page = 1; } // 페이지가 없으면 첫 페이지 (1 페이지)
$from_record = ($page - 1) * $rows; // 시작 열을 구함

$g5['title'] = '고객관리';
include_once('./_head.php');

if(!G5_IS_MOBILE) {
	echo '<div class="site-wrap">
	<div id="aside">&nbsp;';
	include_once(G5_SHOP_PATH.'/asidemy.php');
	echo '</div><div id="container">';
}
if ((!$bo_table || $w == 's' ) && !defined('_INDEX_')) { ?><div id="wrapper_title"><?php echo $g5['title'] ?></div><?php } 

if($sdate == '') {
	$sdate = date( "Y-m" ).'-01';
	$edate = date( "Y-m-d" );
}


?>

<!-- 주문 내역 시작 { -->
<div id="sod_v">
    <p id="sod_v_info">고객정보를 조회 하실 수 있습니다.</p>

    <?php
    $limit = " limit $from_record, $rows ";
?>

<form name="frm1" method="post" action="/shop/myuser.php" style="margin:0">	

	<div id="search-box">
		
          고객명 <input type="text" name="mb_name" style="width:150px; text-align:center"  value="<?php echo $mb_name;?>" >
          <button class="btndate">조회</button>

		  <div style="float:right;"><a href="/shop/myuser.php" class=" btn01 ">고객조회</a> <a href="/shop/myuserorder.php" class=" btn01 ">고객주문조회</a></div>
	</div>

</form>

<form name="frm2" method="post">
<div class="tbl_head03 tbl_wrap">
    <table>
    <thead>
    <tr>
	    <th scope="col" id="mb_list_chk" >
            <label for="chkall" class="sound_only">회원 전체</label>
            <input type="checkbox" name="chkall" value="1" id="chkall" onclick="check_all(this.form)">
        </th>
        <th scope="col">고객이름</th>
		<th scope="col">연락처</th>
		<th scope="col">가입일</th>
        <th scope="col">총주문건수</th>
		<th scope="col">총주문금액</th>        
    </tr>
    </thead>
    <tbody>
	<?php
    //$sql = " select * from tbl_member where mb_recommend='".$member['mb_id']."'";

	$sql = " select c.*
	          ,(select count(*) cnt from tbl_shop_order where od_status !='취소' and mb_id= c.mb_id) cnt
			  ,(select sum(od_total_sale_price) from tbl_shop_order a,  tbl_shop_order_detail b where a.od_id = b.od_id and a.od_status !='취소' and mb_id= c.mb_id ) od_price
	          $sql_common
              $limit ";
		//echo $sql;

    $result = sql_query($sql);
    for ($i=0; $row=sql_fetch_array($result); $i++)
    {
        
	?>
    
    <tr>
        <td headers="mb_list_chk" class="td_chk" >
            <input type="hidden" name="mb_id[<?php echo $i ?>]" value="<?php echo $row['mb_id'] ?>" id="mb_id_<?php echo $i ?>">
			<input type="hidden" name="mb_hp[<?php echo $i ?>]" value="<?php echo $row['mb_name'] ?>:<?php echo $row['mb_hp'] ?>" id="mb_hp_<?php echo $i ?>">
            <input type="checkbox" name="chk[]" value="<?php echo $i ?>" id="chk_<?php echo $i ?>">
        </td>
		<td class=""><?php echo $row['mb_name']; ?> ( <?php echo $row['mb_id']; ?> )</td>
		<td class=""><?php echo $row['mb_hp']; ?></td>
		<td><?php echo substr($row['mb_datetime'], 0, 10); ?></td>
		<td class="td_numbig"><?php echo $row['cnt']; ?></td>
        <td class="td_numbig"><?php echo number_format($row['od_price']); ?></td>
       
    </tr>
	<?php
    }
	?>
	<!--
	<tr>
	  <td colspan="6" style='text-align:left;'>
	  	

		SMS전송 : <a href="javascript:sms_open('NO');" class=" btn">선택발송</a> &nbsp; <a href="javascript:sms_open('ALL');" class="btn">전체발송</a>
	
	  </td>
	</tr>
	-->
	<?php
    if ($i == 0)
        echo "<tr><td colspan=7 class=\"empty_table\">자료가 없습니다.</td></tr>";
    ?>
  
    </tbody>
    </table>
</div>
</form>
<!-- } 주문 내역 목록 끝 -->

    <?php echo get_paging($config['cf_write_pages'], $page, $total_page, "{$_SERVER['SCRIPT_NAME']}?$qstr&amp;page="); ?>
</div>
<!-- } 주문 내역 끝 -->
<script>
function check_all(f)
{
    var chk = document.getElementsByName("chk[]");

    for (i=0; i<chk.length; i++) {
		if (!chk[i].disabled){
			chk[i].checked = f.chkall.checked;
		}        
	}
}

function sms_open(gubun2){
	//fnPop("/adm/sms_admin/send.php?phonenumber="+val, "Pop_sms", "600", "450", "no", "no");	

	var frm = document.frm2;
	popUpWindow("about:blank", "Pop_sms", "600", "450", "no", "no");	
	
	frm.target = "Pop_sms";
	frm.action = "/adm/sms_admin/send_user.php?gubun1=2&gubun2="+gubun2;
	frm.submit();

}
function popUpWindow(url, popName, width, height, scroll, resizable){
	if( width == null || width == "" ) width = 600;
	if( height == null || height == "" ) height = 500;
	if( resizable == null || resizable == "" ) resizable = "yes";
	
	var left, top;
	top  = (window.screen.height - height) / 2;
	left = (window.screen.width  - width ) / 2;
	sFeatures = "width="+width+",height="+height+",left="+left+",top="+top+",resizable="+resizable;
	if(scroll == "yes") sFeatures += ",scrollbars=yes";
	else sFeatures += ",status=no,toolbar=no,menubar=no,location=no";

	var obj = window.open(url,popName,sFeatures);
	obj.focus();
	return obj;
}


function fnPop(url,width,height){
		var sst = window.open(url,'popwin','top='+((screen.availHeight - height)/2 - 40) +', left='+(screen.availWidth - width)/2+', width='+width+', height='+height+', toolbar=0, directories=0, status=0, menubar=0, scrollbars=1, resizable=0');
		if(sst){
			sst.focus();
		}
	}
</script>
<?php
include_once('./_tail.php');
?>

<?php
include_once('./_common.php');
include_once('./_head.php');

$muija = sql_fetch_row(main_banner('무이자_page', 1));

?>

<!-- 신용카드 무이자 할부 안내 -->
<div class="site-wrap">
		<div id="sev_list">

			<!-- ********************* 소셜 공유하기 ********************* -->
			<div style="padding:10px 0px 10px 0;float:right;">

				<div class="social">
				
				<?php 
					$sns_url   = G5_SHOP_URL.'/payinfo.php';
					$sns_title = $ev['ev_subject'];
					$sns_view_img = '';

					$str = get_sns_share_link('facebook', $sns_url, $sns_title, '/theme/onk/img/icon_facebook.gif');
					$str .= '&nbsp;';
					$str .= get_sns_share_link('twitter', $sns_url, $sns_title, '/theme/onk/img/icon_twitter.gif');
					echo $str;
				?>
				<?php echo get_sns_share_link('kakaotalk', $sns_url, $sns_title, G5_THEME_URL.'/img/kakaolink_btn.png'); ?>	
				
				</div>

			</div>
			<!-- ************************************************** -->

		</div>
		<br>

		<div style="margin-top:56px;margin-bottom:56px;text-align:center;"><img src="<?php echo G5_DATA_URL.'/banner/'.$muija['bn_id']; ?>?ver=<?php echo G5_YM_VER; ?>" style="max-width:90%;"/></div>
</div>

<?
if($config['cf_kakao_js_apikey']) {
?>
<!-- <script src="https://developers.kakao.com/sdk/js/kakao.min.js"></script> -->
<script src="//developers.kakao.com/sdk/js/kakao.min.js"></script>
<!-- <script src="<?php echo G5_JS_URL; ?>/kakaolink.js"></script> -->
<?
}
?>

	<script type='text/javascript'>
	  //<![CDATA[
		// // 사용할 앱의 JavaScript 키를 설정해 주세요.
		Kakao.init("<?php echo $config['cf_kakao_js_apikey']; ?>");
		// // 카카오링크 버튼을 생성합니다. 처음 한번만 호출하면 됩니다.
		Kakao.Link.createDefaultButton({
		  container: '#kakao-link-btn',
		  objectType: 'feed',
		  content: {
			title: '<?php echo $sns_title;?>',
			description: '',
			imageUrl: '<?php echo $first_img;?>',
			link: {
			  mobileWebUrl: '<?php echo $sns_url;?>',
			  webUrl: '<?php echo $sns_url;?>'
			}
		  },
		  /*social: {
			likeCount: 286,
			commentCount: 45,
			sharedCount: 845
		  },*/
		  buttons: [{
			title: '이벤트 보기',
			link: {
			  mobileWebUrl: '<?php echo $sns_url;?>',
			  webUrl: '<?php echo $sns_url;?>'
			}
		  }]
		});
	  //]]>
	</script>	

	

<?php
include_once('./_tail.php');
?>

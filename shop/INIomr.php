<?

require("HttpClient.php");

define(HOST_IP, "iniweb.inicis.com");
//define (HOST_IP, "http://203.238.37.26");
//define (HOST_IP, "203.238.37.26");
//define (HOST_IP, "203.238.37.171");
define(HOST_PORT, "443");
define(REGIST_URL, "/DefaultWebApp/mall/cr/open/OpMallUrlConnection.jsp");
define(REQUEST_URL, "/DefaultWebApp/mall/cr/open/OpMallReqUrlConnection.jsp");
define(REFUND_URL, "/DefaultWebApp/mall/cr/open/OpMallRefundUrlConnection.jsp");
define(VERSION, "(V4131B20150922PHP)");

class INIomr {

    //요청 구분 : 공통
    var $reqtype;
    //등록
    var $inipayhome;
    var $id_merchant;
    var $id_mall;
    var $cl_id;
    var $no_comp;
    var $nm_comp;
    var $nm_regist;
    var $cd_bank;
    var $no_acct;
    var $no_tel;
    var $passwd;
    var $cl_gubun;
    //요청,환불
    var $dt_pay;
    var $amt_supply;
    var $cl_service;
    //환불
    var $nm_return;
    var $id_buyer;
    //클래스 내부 용
    var $m_serviceurl;
    var $m_resultcode;
    var $m_resultmsg;

    function startAction() {
        $this->printLog("Start INIomr " . $this->reqtype . VERSION);
        $this->printLog("INIPAYHOME:" . $this->inipayhome);
        $this->printLog("ID_MERCHANT:" . $this->id_merchant);
        $this->printLog("TYPE:" . $this->reqtype);
        $this->printLog("ID_MALL:" . $this->id_mall);
        $this->printLog("CL_ID:" . $this->cl_id);
        $this->printLog("NO_COMP:" . $this->no_comp);
        $this->printLog("NM_COMP:" . $this->nm_comp);
        $this->printLog("NM_BOSS:" . $this->nm_boss);
        $this->printLog("NM_REGIST:" . $this->nm_regist);
        $this->printLog("CD_BANK:" . $this->cd_bank);
        $this->printLog("NO_TEL:" . $this->no_tel);
        $this->printLog("CL_GUBUN:" . $this->cl_gubun);
        $this->printLog("AMT_SUPPLY:" . $this->amt_supply);
        $this->printLog("DT_PAY:" . $this->dt_pay);
        $this->printLog("NM_RETURN:" . $this->nm_return);
        $this->printLog("ID_BUYER:" . $this->id_buyer);

        switch ($this->reqtype) {
            case("REGIST"):
                $msg = "";
                $msg .= "id_merchant=" . urlencode($this->id_merchant) . "&";
                $msg .= "id_mall=" . urlencode($this->id_mall) . "&";
                $msg .= "cl_id=" . urlencode($this->cl_id) . "&";
                $msg .= "no_comp=" . urlencode($this->no_comp) . "&";
                $msg .= "nm_comp=" . urlencode($this->nm_comp) . "&";
                $msg .= "nm_boss=" . urlencode($this->nm_boss) . "&";
                $msg .= "nm_regist=" . urlencode($this->nm_regist) . "&";
                $msg .= "cd_bank=" . urlencode($this->cd_bank) . "&";
                $msg .= "no_acct=" . urlencode($this->no_acct) . "&";
                $msg .= "no_tel=" . urlencode($this->no_tel) . "&";
                $msg .= "passwd=" . urlencode($this->passwd) . "&";
                $msg .= "cl_gubun=" . urlencode($this->cl_gubun);

                $this->m_serviceurl = REGIST_URL;
                $this->connectURL($msg);
                break;
            case("REQUEST"):
                $msg = "";
                $msg .= "id_merchant=" . urlencode($this->id_merchant) . "&";
                $msg .= "id_mall=" . urlencode($this->id_mall) . "&";
                $msg .= "dt_pay=" . urlencode($this->dt_pay) . "&";
                $msg .= "no_comp=" . urlencode($this->no_comp) . "&";
                $msg .= "nm_regist=" . urlencode($this->nm_regist) . "&";
                $msg .= "cd_bank=" . urlencode($this->cd_bank) . "&";
                $msg .= "no_acct=" . urlencode($this->no_acct) . "&";
                $msg .= "amt_supply=" . urlencode($this->amt_supply) . "&";
                if ($this->cl_service != "") {
                    $msg .= "passwd=" . urlencode($this->passwd) . "&";
                    $msg .= "cl_service=" . urlencode($this->cl_service);
                } else {
                    $msg .= "passwd=" . urlencode($this->passwd);
                }
                //$msg .= "passwd=".$this->passwd;

                $this->m_serviceurl = REQUEST_URL;
                $this->connectURL($msg);
                break;
            case("REFUND"):
                $msg = "";
                $msg .= "id_merchant=" . urlencode($this->id_merchant) . "&";
                $msg .= "passwd=" . urlencode($this->passwd) . "&";
                $msg .= "nm_return=" . urlencode($this->nm_return) . "&";
                $msg .= "id_buyer=" . urlencode($this->id_buyer) . "&";
                $msg .= "dt_pay=" . urlencode($this->dt_pay) . "&";
                $msg .= "nm_regist=" . urlencode($this->nm_regist) . "&";
                $msg .= "cd_bank=" . urlencode($this->cd_bank) . "&";
                $msg .= "amt_supply=" . urlencode($this->amt_supply) . "&";
                if ($this->cl_service != "") {
                    $msg .= "no_acct=" . urlencode($this->no_acct) . "&";
                    $msg .= "cl_service=" . urlencode($this->cl_service);
                } else {
                    $msg .= "no_acct=" . urlencode($this->no_acct);
                }

                $this->m_serviceurl = REFUND_URL;
                $this->connectURL($msg);
                break;
        }
        $this->printLog("End INIomr " . $this->reqtype . VERSION);
    }

    function connectURL($msg) {
        $this->printLog($msg);
        $httpclient = new HttpClient(true, HOST_IP, HOST_PORT);

        $this->printLog("Start HTTP Connect:" . HOST_IP . ":" . HOST_PORT . $this->m_serviceurl);
        if ($httpclient->HttpConnect()) {
            $this->printLog("HTTP CONNECTION SUCCESS");
            if ($httpclient->HttpRequest($this->m_serviceurl, $msg)) {
                $this->printLog("RECV REQUEST:" . trim($httpclient->getBody()));
                // 응답 전문 파싱
                $resultToken = explode('&', $httpclient->getBody());

                $temp_code = explode('=', $resultToken[0]);
                $temp_msg = explode('=', $resultToken[1]);
                $this->m_resultcode = $temp_code[1];
                $this->m_resultmsg = $temp_msg[1];

                //$this->m_resultcode = trim(explode('=', $resultToken[0])[1]);
                //$this->m_resultmsg  = trim(explode('=', $resultToken[1])[1]);

                $this->printLog("RESULT CODE:" . $this->m_resultcode);
                $this->printLog("RESULT MSG:" . $this->m_resultmsg);
            } else {
                $this->printLog("HTTP REQUEST FAIL");
                // 전문 응답 요청 오류
                $this->m_resultcode = $httpclient->getErrorCode();
                $this->m_resultmsg = $httpclient->getErrorMsg();
            }
        } else {
            $this->printLog("HTTP CONNECTION FAIL");
            // 서버 접속 오류
            $this->m_resultcode = $httpclient->getErrorCode();
            $this->m_resultmsg = $httpclient->getErrorMsg();
        }
    }

    function getResultCode() {
        return $this->m_resultcode;
    }

    function getResultMsg() {
        return $this->m_resultmsg;
    }

    function printLog($msg) {
        $path = $this->inipayhome . "/log/";
        $file = "INIomr" . $this->reqtype . "_" . $this->id_merchant . ".log." . date("Ymd");
        $msg_head = "[" . date("Y-m-d") . "_" . date("H:i:s") . "][P=" . getmypid() . "]";

        if (!is_dir($path)) {
            mkdir($path, 0755);
        }
        if (!($fp = fopen($path . $file, "a+")))
            return 0;
        fwrite($fp, $msg_head);

        if (!empty($cmt) || !empty($line)) {
            $cmt = basename($cmt);
            fwrite($fp, "[$cmt($line)]");
        }

        ob_start();
        print_r($msg);
        $ob_msg = ob_get_contents();
        ob_clean();

        if (fwrite($fp, " " . $ob_msg . "\n") === FALSE) {
            fclose($fp);
            return 0;
        }
        fclose($fp);
        return 1;
    }

}

?>

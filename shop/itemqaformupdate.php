<?php
include_once('./_common.php');

if (!$is_member) {
    alert_close("상품문의는 회원만 작성이 가능합니다.");
}

$iq_id = (int) trim($_REQUEST['iq_id']);
$iq_subject = trim($_POST['iq_subject']);
$iq_question = trim($_POST['iq_question']);
$iq_question = preg_replace('#<script(.*?)>(.*?)</script>#is', '', $iq_question);
$iq_answer = trim($_POST['iq_answer']);
$hash = trim($_REQUEST['hash']);
$get_editor_img_mode = $config['cf_editor'] ? false : true;

if ($w == "" || $w == "u") {
    $iq_name     = addslashes(strip_tags($member['mb_name']));
    $iq_password = $member['mb_password'];

    if (!$iq_subject) alert("제목을 입력하여 주십시오.");
    if (!$iq_question) alert("질문을 입력하여 주십시오.");
}
$sql = " select it_name from {$g5['g5_shop_item_table']} where it_id = '$it_id' ";
$row=sql_fetch($sql);
if($row['it_name']){
	$it_name=$row['it_name'];
}
if($is_mobile_shop)
    $url = "./item.php?it_id=$it_id&_=".get_token()."#sit_qa";
else
    $url = "./item.php?it_id=$it_id&_=".get_token()."#sit_qa";


/*************** 트랜잭션 관련 ****************/
$error_cnt = 0;
mysqli_autocommit($g5['connect_db'], false);
/*************** 트랜잭션 관련 ****************/

if ($w == "")
{
    $sql = "insert {$g5['g5_shop_item_qa_table']}
               set it_id = '$it_id',
                   mb_id = '{$member['mb_id']}',
                   iq_secret = '$iq_secret',
                   iq_name  = '$iq_name',
                   iq_email = '$iq_email',
                   iq_hp = '$iq_hp',
                   iq_password  = '$iq_password',
                   iq_subject  = '$iq_subject',
                   iq_question = '$iq_question',
                   iq_time = '".G5_TIME_YMDHIS."',
                   iq_ip = '$REMOTE_ADDR' ";
    sql_query($sql);
	/*************** 트랜잭션 관련 ****************/
	if( mysqli_errno($g5['connect_db']) ){
		$error_cnt += 1;
	}
	/*************** 트랜잭션 관련 ****************/
    include_once(G5_LIB_PATH.'/mailer.lib.php');
	$subject = '상품 Q&A 문의 알림 메일';
	$content = '[ '.$it_name.' ] 상품의 문의가 등록되었습니다. 확인해주세요.';
	mailer($config['cf_admin_email_name'], 'onkorderstation@gmail.com' ,'hwijin.park@taejeongroup.com', $subject, $content, 1);
    $alert_msg = '상품문의가 등록 되었습니다.';
}
else if ($w == "u")
{
    if (!$is_admin)
    {
        $sql = " select count(*) as cnt from {$g5['g5_shop_item_qa_table']} where mb_id = '{$member['mb_id']}' and iq_id = '$iq_id' ";
        $row = sql_fetch($sql);
        if (!$row['cnt'])
            alert("자신의 상품문의만 수정하실 수 있습니다.");
    }

    $sql = " update {$g5['g5_shop_item_qa_table']}
                set iq_secret = '$iq_secret',
                    iq_email = '$iq_email',
                    iq_hp = '$iq_hp',
                    iq_subject = '$iq_subject',
                    iq_question = '$iq_question'
              where iq_id = '$iq_id' ";
    sql_query($sql);
	/*************** 트랜잭션 관련 ****************/
	if( mysqli_errno($g5['connect_db']) ){
		$error_cnt += 1;
	}
	/*************** 트랜잭션 관련 ****************/

    $alert_msg = '상품문의가 수정 되었습니다.';
}
else if ($w == "d")
{
    if (!$is_admin)
    {
        $sql = " select iq_answer from {$g5['g5_shop_item_qa_table']} where mb_id = '{$member['mb_id']}' and iq_id = '$iq_id' ";
        $row = sql_fetch($sql);
        if (!$row)
            alert("자신의 상품문의만 삭제하실 수 있습니다.");

        if ($row['iq_answer'])
            alert("답변이 있는 상품문의는 삭제하실 수 없습니다.");
    }

    $sql = " delete from {$g5['g5_shop_item_qa_table']} where iq_id = '$iq_id' and md5(concat(iq_id,iq_time,iq_ip)) = '{$hash}' ";
    sql_query($sql);
	/*************** 트랜잭션 관련 ****************/
	if( mysqli_errno($g5['connect_db']) ){
		$error_cnt += 1;
	}
	/*************** 트랜잭션 관련 ****************/

	/*************** 트랜잭션 관련 delete의 오류가 없다면 파일삭제 ****************/
	if( $error_cnt == 0 ){

		// 에디터로 첨부된 이미지 삭제
		$sql = " select iq_question, iq_answer from {$g5['g5_shop_item_qa_table']} where iq_id = '$iq_id' and md5(concat(iq_id,iq_time,iq_ip)) = '{$hash}' ";
		$row = sql_fetch($sql);

		$imgs = get_editor_image($row['iq_question'], $get_editor_img_mode);

		for($i=0;$i<count($imgs[1]);$i++) {
			$p = parse_url($imgs[1][$i]);
			if(strpos($p['path'], "/data/") != 0)
				$data_path = preg_replace("/^\/.*\/data/", "/data", $p['path']);
			else
				$data_path = $p['path'];

			if( preg_match('/(gif|jpe?g|bmp|png)$/i', strtolower(end(explode('.', $data_path))) ) ){

				$destfile = ( ! preg_match('/\w+\/\.\.\//', $data_path) ) ? G5_PATH.$data_path : '';

				if($destfile && preg_match('/\/data\/editor\/[A-Za-z0-9_]{1,20}\//', $destfile) && is_file($destfile))
					@unlink($destfile);
			}
		}

		$imgs = get_editor_image($row['iq_answer'], $get_editor_img_mode);

		for($i=0;$i<count($imgs[1]);$i++) {
			$p = parse_url($imgs[1][$i]);
			if(strpos($p['path'], "/data/") != 0)
				$data_path = preg_replace("/^\/.*\/data/", "/data", $p['path']);
			else
				$data_path = $p['path'];

			if( preg_match('/(gif|jpe?g|bmp|png)$/i', strtolower(end(explode('.', $data_path))) ) ){

				$destfile = ( ! preg_match('/\w+\/\.\.\//', $data_path) ) ? G5_PATH.$data_path : '';

				if($destfile && preg_match('/\/data\/editor\/[A-Za-z0-9_]{1,20}\//', $destfile) && is_file($destfile))
					@unlink($destfile);
			}
		}

	}

    $alert_msg = '상품문의가 삭제 되었습니다.';
}

/*************** 트랜잭션 관련 ****************/
if ($error_cnt > 0) {
	mysqli_rollback($g5['connect_db']);
	mysqli_close($g5['connect_db']);
	echo "
	<script>
		alert('데이터베이스의 에러로 인해 롤백되었습니다.');
		history.back();
	</script>
	";
	exit;
} else {
	mysqli_commit($g5['connect_db']);
} 
/*************** 트랜잭션 관련 ****************/

if($w == 'd')
    alert($alert_msg);
else
    alert_opener($alert_msg, $url);
?>

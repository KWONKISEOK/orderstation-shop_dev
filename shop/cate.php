<?php
include_once('./_common.php');

if($member['mb_type']=='1' || $member['mb_type']=='9') {
	$sql = "select ca_id , ca_name, 1 parent_cate_no from tbl_shop_category where ca_web_view=1 and length(ca_id) =2 ";
} else {
	$sql = "select ca_id , ca_name, 1 parent_cate_no from tbl_shop_category where ca_app_view=1 and length(ca_id) =2 ";
}
$result = sql_query($sql);
$i = 0;
while($row = sql_fetch_array($result)) {
	$response[$i]['cate_no']		= $row['ca_id'];
	$response[$i]['param']			= '?ca_id='.$row['ca_id'];
	$response[$i]['name']			= $row['ca_name'];
	$response[$i]['parent_cate_no'] = $row['parent_cate_no'];
	$i++;
}


if($member['mb_type']=='1' || $member['mb_type']=='9') {
	$sql = "select ca_id , ca_name, left(ca_id, 2) parent_cate_no from tbl_shop_category where ca_web_view=1 and length(ca_id) =4";
} else {
	$sql = "select ca_id , ca_name, left(ca_id, 2) parent_cate_no from tbl_shop_category where ca_app_view=1 and length(ca_id) =4";
}

$result = sql_query($sql);
while($row = sql_fetch_array($result)) {
	$response[$i]['cate_no']		= $row['ca_id'];
	$response[$i]['param']			= '?ca_id='.$row['ca_id'];
	$response[$i]['name']			= $row['ca_name'];
	$response[$i]['parent_cate_no'] = $row['parent_cate_no'];
	$i++;
}

if($member['mb_type']=='1' || $member['mb_type']=='9') {
	$sql = "select ca_id , ca_name, left(ca_id, 4) parent_cate_no from tbl_shop_category where ca_web_view=1 and length(ca_id) =6";
} else {
	$sql = "select ca_id , ca_name, left(ca_id, 4) parent_cate_no from tbl_shop_category where ca_app_view=1 and length(ca_id) =6";
}


$result = sql_query($sql);
while($row = sql_fetch_array($result)) {
	$response[$i]['cate_no']		= $row['ca_id'];
	$response[$i]['param']			= '?ca_id='.$row['ca_id'];
	$response[$i]['name']			= $row['ca_name'];
	$response[$i]['parent_cate_no'] = $row['parent_cate_no'];
	$i++;
}

echo json_encode($response);

?>

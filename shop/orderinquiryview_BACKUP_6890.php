<?php
include_once('./_common.php');

$od_id = isset($od_id) ? preg_replace('/[^A-Za-z0-9\-_]/', '', strip_tags($od_id)) : 0;

if( isset($_GET['ini_noti']) && !isset($_GET['uid']) ){
    goto_url(G5_SHOP_URL.'/orderinquiry.php');
}

// 불법접속을 할 수 없도록 세션에 아무값이나 저장하여 hidden 으로 넘겨서 다음 페이지에서 비교함
$token = md5(uniqid(rand(), true));
set_session("ss_token", $token);

//달콩 이벤트세트 구매한 회원 쿠폰 발행해주기;
//EventDalcongCoupon($member['mb_name'],$member['mb_id'],$member['mb_type']);

if (!$is_member) {
    if (get_session('ss_orderview_uid') != $_GET['uid'])
        alert("직접 링크로는 주문서 조회가 불가합니다.\\n\\n주문조회 화면을 통하여 조회하시기 바랍니다.", G5_SHOP_URL);
}

$sql = "select * from {$g5['g5_shop_order_table']} where od_id = '$od_id' {$service_type_and_query} ";
if($is_member && !$is_admin)
    $sql .= " and mb_id = '{$member['mb_id']}' ";
$od = sql_fetch($sql);
if (!$od['od_id'] || (!$is_member && md5($od['od_id'].$od['od_time'].$od['od_ip']) != get_session('ss_orderview_uid'))) {
    alert("조회하실 주문서가 없습니다.", G5_SHOP_URL);
}

$sql = "select sum(od_total_drug_price) as od_total_drug_price from {$g5['g5_shop_order_detail_table']} where od_id = '$od_id' {$service_type_and_query} ";
$od_dt = sql_fetch($sql);

// 결제방법
$settle_case = $od['od_settle_case'];

if (G5_IS_MOBILE) {
    include_once(G5_MSHOP_PATH.'/orderinquiryview.php');
    return;
}

// 테마에 orderinquiryview.php 있으면 include
if(defined('G5_THEME_SHOP_PATH')) {
    $theme_inquiryview_file = G5_THEME_SHOP_PATH.'/orderinquiryview.php';
    if(is_file($theme_inquiryview_file)) {
        include_once($theme_inquiryview_file);
        return;
        unset($theme_inquiryview_file);
    }
}

$g5['title'] = '주문상세내역';
include_once('./_head.php');


/*
 * 회원/비회원 노출 구분필요

if(!G5_IS_MOBILE) {
	echo '<div class="site-wrap">
	<div id="aside">';
	include_once(G5_SHOP_PATH.'/asidemy.php');
	echo '</div><div id="container">';
}
*/
if(!G5_IS_MOBILE) {
	echo '<div class="site-wrap">';
	echo '<div id="container">';
}



// LG 현금영수증 JS
if($od['od_pg'] == 'lg') {
    if($default['de_card_test']) {
    echo '<script language="JavaScript" src="http://pgweb.uplus.co.kr:7085/WEB_SERVER/js/receipt_link.js"></script>'.PHP_EOL;
    } else {
        echo '<script language="JavaScript" src="http://pgweb.uplus.co.kr/WEB_SERVER/js/receipt_link.js"></script>'.PHP_EOL;
    }
}
?>

<!-- 주문상세내역 시작 { -->
<div id="sod_fin">
<<<<<<< HEAD
	<br />
    <div id="sod_fin_no">주문번호 <strong><?php echo $od_id; ?></strong></div>

    <section id="sod_fin_list">
        <h2>주문하신 상품</h2>

        <?php
        $st_count1 = $st_count2 = 0;
        $custom_cancel = false;

        $sql = " select it_id, it_name, od_send_cost, 0 it_sc_type
                    from {$g5['g5_shop_order_detail_table']}
                    where od_id = '$od_id' {$service_type_and_query}
                    group by it_id
                    order by od_id "; 
        $result = sql_query($sql);
        ?>
        <div class="tbl_head03 tbl_wrap">
            <table>
            <thead>
            <tr>
                <th scope="col" rowspan="2">이미지</th>
                <th scope="col" colspan="6" id="th_itname">상품명</th>
            </tr>
            <tr class="th_line">
                <th scope="col" id="th_itopt">옵션명</th>
                <th scope="col" id="th_itqty">수량</th>
                <!--<th scope="col" id="th_itprice">소비자가</th>-->
                <th scope="col" id="th_itsum">소비자가</th>
                <?php if($member['mb_type']=='1') {?>
                    <th scope="col">공급가</th>
                <?php } ?>
                <!-- <th scope="col" id="th_itpt">적립포인트</th> -->
                <th scope="col" id="th_itsd">배송비</th>
                <th scope="col" id="th_itst">상태</th>
            </tr>
            </thead>
            <tbody>
            <?php
			$order_receiver_cnt = 0;
			$return_possible = ""; //2차옵션 상품일경우 반품을 막기위한 조치

            for($i=0; $row=sql_fetch_array($result); $i++) {
                $image = get_it_image($row['it_id'], 70, 70);

                /*$sql = " select ct_id, it_name, ct_option, ct_qty, ct_price, ct_point, ct_status, io_type, io_price
                            from {$g5['g5_shop_cart_table']}
                            where od_id = '$od_id'
                              and it_id = '{$row['it_id']}'
                            order by io_type asc, ct_id asc ";*/

				$sql = " select a.od_id, a.it_name, a.od_option, a.od_qty qty, b.od_qty, a.od_price, od_total_sale_price, od_total_drug_price, od_total_incen, b.od_status, b.od_period_date, a.od_period_cnt, a.od_send_cost,b.od_delivery_company,b.od_invoice, a.od_num , b.od_bill_tno , a.io_id, b.od_pay_yn
                            from {$g5['g5_shop_order_detail_table']} a, tbl_shop_order_receiver b
                            where a.od_id = b.od_id
							  and a.od_num = b.od_num
							  and a.od_id = '$od_id'
                              and it_id = '{$row['it_id']}' {$service_type_and_query_a} ";

                $res = sql_query($sql);
                $rowspan = sql_num_rows($res) + 1;

				$ReceiptHtml = "";
			
                for($k=0; $opt=sql_fetch_array($res); $k++) {

					//2차옵션 상품이 끼어있으면 반품신청을 못한다.
					if( strpos( $opt['io_id'] , chr(30) ) !== false ){
						$return_possible = "false";
					}

					if( !empty($opt['od_bill_tno']) ) {
						$receipt_script = 'window.open(\'https://iniweb.inicis.com/DefaultWebApp/mall/cr/cm/mCmReceipt_head.jsp?noTid='.$opt['od_bill_tno'].'&noMethod=1\',\'receipt\',\'width=430,height=700\');';
						$link_script = "&nbsp;<a href=\"javascript:;\" onclick=".$receipt_script.">[영수증 출력]</a>";
						$ReceiptHtml .= "<tr><td>".get_text($opt['od_option'])."-".get_text($opt['od_period_date'])."&nbsp;".$link_script."</td></tr>";
					}

                    if($k == 0) {
            ?>
            <tr>
                <td rowspan="<?php echo $rowspan; ?>" class="td_imgsmall"><?php echo $image; ?></td>
                <td headers="th_itname" colspan="6" class="td_bdno">
					<? 
					if( $opt['od_status'] == "완료" || $opt['od_status'] == "반품반려" ){ 
						$order_receiver_cnt++;
					?>
					<input type="checkbox" name="return_od_num[]" value="<?=$opt['od_num']?>">
					<? } ?>
					<a href="./item.php?it_id=<?php echo $row['it_id']; ?>"><?php echo $row['it_name']; ?></a>
				</td>
            </tr>
            <?php } ?>
            <tr>
                <td headers="th_itopt" class="td_bdno"><?php echo get_text($opt['od_option']); ?> - <?php echo get_text($opt['od_period_date']); ?></td>
                <td headers="th_itqty" class="td_mngsmall"><?php echo number_format($opt['od_qty']); ?></td>
                <!--<td headers="th_itprice" class="td_numbig text_right"><?php echo number_format($opt['od_price']); ?></td>-->
                <td headers="th_itsum" class="td_numbig">
				<?php
				if($opt[od_period_cnt] > 0) {
					$subprice= $opt['od_total_sale_price'] / $opt[od_period_cnt];
					$subincen= $opt['od_total_incen'] / $opt[od_period_cnt];
				} else {
					$subprice=$opt['od_total_sale_price'];
					$subincen= $opt['od_total_incen'] ;
				}
					echo number_format($subprice); 
			
				?></td>
                <?php if($member['mb_type']=='1') {?>
                    <td class="td_numbig">
                        <?php
                        if($opt[od_period_cnt] > 0) {
                            $subprice= $opt['od_total_drug_price'] / $opt[od_period_cnt];
                            $subincen= $opt['od_total_incen'] / $opt[od_period_cnt];
                        } else {
                            $subprice=$opt['od_total_drug_price'];
                            $subincen= $opt['od_total_incen'];
                        }
                        echo number_format($subprice);
                        ?>
                    </td>
                <?php } ?>
                <!-- <td headers="th_itpt" class="td_numbig text_right"><?php echo number_format($point); ?></td> -->
                <td headers="th_itsd" class="td_dvr"><?php echo number_format($opt['od_send_cost']); ?></td>
                <td headers="th_itst" class="td_mngsmall">
                    <?php
                    if ($opt['od_status'] == "주문" && $opt['od_pay_yn'] == "Y") {
                        echo '입금';
                    } else {
                        echo $opt['od_status'];
                    }
                    ?>
                </td>
            </tr>
            <?php
                    $tot_point       += $point;

                    $st_count1++;
                    if($opt['od_status'] == '주문' || (($opt['od_status'] == '입금') && ($od['od_settle_case'] == '신용카드')))
                        $st_count2++;
                }
            }

            // 주문 상품의 상태가 모두 주문이면 고객 취소 가능
            if($st_count1 > 0 && $st_count1 == $st_count2)
                $custom_cancel = true;
            ?>
            </tbody>
            </table>
        </div>
        
        <div id="sod_sts_wrap">
            <span class="sound_only">상품 상태 설명</span>
			<span style="float:left;">
			<button type="button" id="btn_frmline_1" class="btn_frmline_1" onclick="javascript:location.href='/shop/orderinquiry.php';">주문내역확인</button>&nbsp;
			<button type="button" id="btn_frmline_2" class="btn_frmline_2" onclick="javascript:location.href='/';">메인으로가기</button>
			</span>
            <button type="button" id="sod_sts_explan_open" class="btn_frmline">상태설명보기</button>
			<? if( $od['od_status'] == "반품반려" ){ ?>
			<!--<button type="button" id="return_explan_open" class="btn_frmline">반품반려 이유보기</button>-->
			<? } ?>
            <div id="sod_sts_explan">
                <dl id="sod_fin_legend">
                    <dt>주문</dt>
                    <dd>주문이 접수되었습니다.
                    <dt>입금</dt>
                    <dd>입금(결제)이 완료 되었습니다.
                    <dt>준비</dt>
                    <dd>상품 준비 중입니다.
                    <dt>배송</dt>
                    <dd>상품 배송 중입니다.
                    <dt>완료</dt>
                    <dd>상품 배송이 완료 되었습니다.
                    <dt>반품접수</dt>
                    <dd>상품 반품접수가 완료 되었습니다.
                    <dt>반품반려</dt>
                    <dd>상품 반품접수가 반려 되었습니다.
                    <dt>반품완료</dt>
                    <dd>상품 반품이 완료처리 되었습니다.
                </dl>
                <button type="button" id="sod_sts_explan_close" class="btn_frmline">상태설명닫기</button>
            </div>
			<div id="return_explan" style="display:none;">
                <dl id="sod_fin_legend">
                    <dd><?=$od['od_return_admin_memo']?></dd>
                </dl>				
				<button type="button" id="return_explan_close" class="btn_frmline">반품반려 이유닫기</button>
			</div>
        </div>
    
    </section>
    <div class="sod_left">
        <h2>결제/배송 정보</h2>
        <?php
        // 총계 = 주문상품금액합계 + 배송비 - 상품할인 - 결제할인 - 배송비할인
        $tot_price = $od['od_cart_price'] + $od['od_send_cost'] + $od['od_send_cost2']
                        - $od['od_cart_coupon'] - $od['od_coupon'] - $od['od_send_coupon']
                        - $od['od_cancel_price'];
        $tot_drug_price = $od_dt['od_total_drug_price'] + $od['od_send_cost'] + $od['od_send_cost2']
                        - $od['od_cart_coupon'] - $od['od_coupon'] - $od['od_send_coupon']
                        - $od['od_cancel_price'];

        $receipt_price  = $od['od_receipt_price']
                        + $od['od_receipt_point'];
        $cancel_price   = $od['od_cancel_price'];

        $misu = true;
        $misu_price = $tot_price - $receipt_price - $cancel_price;

        if ($misu_price == 0 && ($od['od_cart_price'] > $od['od_cancel_price'])) {
            $wanbul = " (완불)";
            $misu = false; // 미수금 없음
        }
        else
        {
            $wanbul = display_price($receipt_price);
        }

        // 결제정보처리
        if($od['od_receipt_price'] > 0)
            $od_receipt_price = display_price($od['od_receipt_price']);
        else
            $od_receipt_price = '아직 입금되지 않았거나 입금정보를 입력하지 못하였습니다.';

        $app_no_subj = '';
        $disp_bank = true;
        $disp_receipt = false;
        if($od['od_settle_case'] == '신용카드' || $od['od_settle_case'] == 'KAKAOPAY' || is_inicis_order_pay($od['od_settle_case']) ) {
            $app_no_subj = '승인번호';
            $app_no = $od['od_app_no'];
            $disp_bank = false;
            $disp_receipt = true;
        } else if($od['od_settle_case'] == '간편결제') {
            $app_no_subj = '승인번호';
            $app_no = $od['od_app_no'];
            $disp_bank = false;
            switch($od['od_pg']) {
                case 'lg':
                    $easy_pay_name = 'PAYNOW';
                    break;
                case 'inicis':
                    $easy_pay_name = 'KPAY';
                    break;
                case 'kcp':
                    $easy_pay_name = 'PAYCO';
                    break;
                default:
                    break;
            }
        } else if($od['od_settle_case'] == '휴대폰') {
            $app_no_subj = '휴대폰번호';
            $app_no = $od['od_bank_account'];
            $disp_bank = false;
            $disp_receipt = true;
        } else if($od['od_settle_case'] == '가상계좌' || $od['od_settle_case'] == '계좌이체') {
            $app_no_subj = '거래번호';
            $app_no = $od['od_tno'];
        }
        ?>

        <section id="sod_fin_pay">
            <h3>결제정보</h3>

            <div class="tbl_head01 tbl_wrap">
                <table>
                <tbody>
                <tr>
                    <th scope="row">주문번호</th>
                    <td><?php echo $od_id; ?></td>
                </tr>
                <tr>
                    <th scope="row">주문일시</th>
                    <td><?php echo $od['od_time']; ?></td>
                </tr>
				<?php if($member[mb_type] != 1) { ?>
                <tr>
                    <th scope="row">결제방식</th>
                    <td><?php echo ($easy_pay_name ? $easy_pay_name.'('.$od['od_settle_case'].')' : check_pay_name_replace($od['od_settle_case']) ); ?></td>
                </tr>
                <tr>
                    <th scope="row">결제금액</th>
                    <td><?php echo $od_receipt_price; ?></td>
                </tr>
				<?php } ?>
                <?php
                if($od['od_receipt_price'] > 0)
                {
                ?>
                <tr>
                    <th scope="row">결제일시</th>
                    <td><?php echo $od['od_receipt_time']; ?></td>
                </tr>
                <?php
                }

                // 승인번호, 휴대폰번호, 거래번호
                if($app_no_subj)
                {
                ?>
                <tr>
                    <th scope="row"><?php echo $app_no_subj; ?></th>
                    <td><?php echo $app_no; ?></td>
                </tr>
                <?php
                }

                // 계좌정보
                if($disp_bank)
                {
                ?>
                <tr>
                    <th scope="row">입금자명</th>
                    <td><?php echo get_text($od['od_deposit_name']); ?></td>
                </tr>
                <tr>
                    <th scope="row">입금계좌</th>
                    <td><?php echo get_text($od['od_bank_account']); ?></td>
                </tr>
                <tr>
                    <th scope="row">입금기한</th>
                    <td><?php echo get_text($od['od_bank_date']); ?></td>
                </tr>
                <?php
                }

                if($disp_receipt) {
                ?>
                <tr>
                    <th scope="row">영수증</th>
                    <td>
                        <?php
                        if($od['od_settle_case'] == '휴대폰')
                        {
                            if($od['od_pg'] == 'lg') {
                                require_once G5_SHOP_PATH.'/settle_lg.inc.php';
                                $LGD_TID      = $od['od_tno'];
                                $LGD_MERTKEY  = $config['cf_lg_mert_key'];
                                $LGD_HASHDATA = md5($LGD_MID.$LGD_TID.$LGD_MERTKEY);

                                $hp_receipt_script = 'showReceiptByTID(\''.$LGD_MID.'\', \''.$LGD_TID.'\', \''.$LGD_HASHDATA.'\');';
                            } else if($od['od_pg'] == 'inicis') {
                                $hp_receipt_script = 'window.open(\'https://iniweb.inicis.com/DefaultWebApp/mall/cr/cm/mCmReceipt_head.jsp?noTid='.$od['od_tno'].'&noMethod=1\',\'receipt\',\'width=430,height=700\');';
                            } else {
                                $hp_receipt_script = 'window.open(\''.G5_BILL_RECEIPT_URL.'mcash_bill&tno='.$od['od_tno'].'&order_no='.$od['od_id'].'&trade_mony='.$od['od_receipt_price'].'\', \'winreceipt\', \'width=500,height=690,scrollbars=yes,resizable=yes\');';
                            }
                        ?>
                        <a href="javascript:;" onclick="<?php echo $hp_receipt_script; ?>">영수증 출력</a>
                        <?php
                        }

                        if($od['od_settle_case'] == '신용카드' || is_inicis_order_pay($od['od_settle_case']) )
                        {
                            if($od['od_pg'] == 'lg') {
                                require_once G5_SHOP_PATH.'/settle_lg.inc.php';
                                $LGD_TID      = $od['od_tno'];
                                $LGD_MERTKEY  = $config['cf_lg_mert_key'];
                                $LGD_HASHDATA = md5($LGD_MID.$LGD_TID.$LGD_MERTKEY);

                                $card_receipt_script = 'showReceiptByTID(\''.$LGD_MID.'\', \''.$LGD_TID.'\', \''.$LGD_HASHDATA.'\');';
                            } else if($od['od_pg'] == 'inicis') {
                                $card_receipt_script = 'window.open(\'https://iniweb.inicis.com/DefaultWebApp/mall/cr/cm/mCmReceipt_head.jsp?noTid='.$od['od_tno'].'&noMethod=1\',\'receipt\',\'width=430,height=700\');';
                            } else {
                                $card_receipt_script = 'window.open(\''.G5_BILL_RECEIPT_URL.'card_bill&tno='.$od['od_tno'].'&order_no='.$od['od_id'].'&trade_mony='.$od['od_receipt_price'].'\', \'winreceipt\', \'width=470,height=815,scrollbars=yes,resizable=yes\');';
                            }
                        ?>
							<? if( $ReceiptHtml == "" ){ ?>
								<a href="javascript:;" onclick="<?php echo $card_receipt_script; ?>">영수증 출력</a>
							<? }else{ ?>
								<table>
								<?=$ReceiptHtml?>
								</table>
							<? } ?>
                        <?php
                        }

                        if($od['od_settle_case'] == 'KAKAOPAY')
                        {
                            $card_receipt_script = 'window.open(\'https://mms.cnspay.co.kr/trans/retrieveIssueLoader.do?TID='.$od['od_tno'].'&type=0\', \'popupIssue\', \'toolbar=no,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=yes,width=420,height=540\');';
                        ?>
                        <a href="javascript:;" onclick="<?php echo $card_receipt_script; ?>">영수증 출력</a>
                        <?php
                        }
                        ?>
                    </td>
                </tr>
                <?php
                }

                if ($od['od_receipt_point'] > 0)
                {
                ?>
                <tr>
                    <th scope="row">포인트사용</th>
                    <td><?php echo display_point($od['od_receipt_point']); ?></td>
                </tr>

                <?php
                }
=======
    <div class="breadcrumb">
        <ul>
            <li><a href="#">장바구니</a></li>
            <li><a href="#">주문/결제</a></li>
            <li class="active"><a href="#">주문완료</a></li>
        </ul>
    </div>
    <div class="order-step-3">
        <div class="left-section">
            <div class="order-result-top">
                <p class="order-desc">고객님의 주문이 정상적으로 완료되었습니다.</p>
                <p class="order-number"><span class="label">주문번호</span><span class="desc"><?php echo $od_id; ?></span></p>
            </div>
            <section id="sod_fin_list">
                <h3 class="title-box">
                    <span class="left-side title-main">주문 상품</span>
                </h3>
>>>>>>> OS-95_P

                <?php
                $st_count1 = $st_count2 = 0;
                $custom_cancel = false;

                $sql = " select it_id, it_name, od_send_cost, 0 it_sc_type
                            from {$g5['g5_shop_order_detail_table']}
                            where od_id = '$od_id' {$service_type_and_query}
                            group by it_id
                            order by od_id "; 
                $result = sql_query($sql);
                ?>
                <div class="table-body-border">
                    <table>
                        <colgroup>
                            <col width="*">
                            <col width="120px">
                            <col width="120px">
                            <col width="120px">
                            <col width="120px">
                            <col width="120px">
                        </colgroup>
                        <thead>
                            <tr>
                                <th scope="col" id="th_itname">상품정보</th>
                                <th scope="col" id="th_itqty">총수량</th>
                                <th scope="col" id="th_itsum">소비자가</th>
                                <th scope="col" id="th_itsd">배송비</th>
                                <th scope="col">소계</th>
                                <th scope="col">상태</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $order_receiver_cnt = 0;
                            $return_possible = ""; //2차옵션 상품일경우 반품을 막기위한 조치

                            for($i=0; $row=sql_fetch_array($result); $i++) {
                                $image = get_it_image($row['it_id'], 70, 70);

                                /*$sql = " select ct_id, it_name, ct_option, ct_qty, ct_price, ct_point, ct_status, io_type, io_price
                                            from {$g5['g5_shop_cart_table']}
                                            where od_id = '$od_id'
                                            and it_id = '{$row['it_id']}'
                                            order by io_type asc, ct_id asc ";*/

                                $sql = " select a.od_id, a.it_name, a.od_option, a.od_qty qty, b.od_qty, a.od_price, od_total_sale_price, od_total_incen, b.od_status, b.od_period_date, a.od_period_cnt, a.od_send_cost,b.od_delivery_company,b.od_invoice, a.od_num , b.od_bill_tno , a.io_id, b.od_pay_yn
                                            from {$g5['g5_shop_order_detail_table']} a, tbl_shop_order_receiver b
                                            where a.od_id = b.od_id
                                            and a.od_num = b.od_num
                                            and a.od_id = '$od_id'
                                            and it_id = '{$row['it_id']}' {$service_type_and_query_a} ";

                                $res = sql_query($sql);

                                $ReceiptHtml = "";
                            
                                for($k=0; $opt=sql_fetch_array($res); $k++) {

                                    //2차옵션 상품이 끼어있으면 반품신청을 못한다.
                                    if( strpos( $opt['io_id'] , chr(30) ) !== false ){
                                        $return_possible = "false";
                                    }

                                    if( !empty($opt['od_bill_tno']) ) {
                                        $receipt_script = 'window.open(\'https://iniweb.inicis.com/DefaultWebApp/mall/cr/cm/mCmReceipt_head.jsp?noTid='.$opt['od_bill_tno'].'&noMethod=1\',\'receipt\',\'width=430,height=700\');';
                                        $link_script = "&nbsp;<a href=\"javascript:;\" onclick=".$receipt_script.">[영수증 출력]</a>";
                                        $ReceiptHtml .= "<tr><td>".get_text($opt['od_option'])."-".get_text($opt['od_period_date'])."&nbsp;".$link_script."</td></tr>";
                                    }

                                    if($k == 0) {
                            ?>
                            <tr>
                                <td>
                                    <div class="item-detail">
                                        <div class="sod_img"><?php echo $image; ?> </div>
                                    </div>
                                </td>
                                <!-- <td headers="th_itname" class="td_bdno">
                                    <? 
                                    if( $opt['od_status'] == "완료" || $opt['od_status'] == "반품반려" ){ 
                                        $order_receiver_cnt++;
                                    ?>
                                    <input type="checkbox" name="return_od_num[]" value="<?=$opt['od_num']?>">
                                    <? } ?>
                                    <a href="./item.php?it_id=<?php echo $row['it_id']; ?>"><?php echo $row['it_name']; ?></a>
                                </td> -->
                                <td headers="th_itopt" class="td_bdno"><?php echo get_text($opt['od_option']); ?> - <?php echo get_text($opt['od_period_date']); ?></td>
                                <td headers="th_itqty" class="td_mngsmall"><?php echo number_format($opt['od_qty']); ?></td>
                                <!--<td headers="th_itprice" class="td_numbig"><?php echo number_format($opt['od_price']); ?></td>-->
                                <td headers="th_itsum" class="td_numbig">
                                <?php
                                if($opt[od_period_cnt] > 0) {
                                    $subprice= $opt['od_total_sale_price'] / $opt[od_period_cnt];
                                    $subincen= $opt['od_total_incen'] / $opt[od_period_cnt];
                                } else {
                                    $subprice=$opt['od_total_sale_price'];
                                    $subincen= $opt['od_total_incen'] ;
                                }
                                    echo number_format($subprice); 
                            
                                ?></td>
                                <!-- <td headers="th_itpt" class="td_numbig"><?php echo number_format($point); ?></td> -->
                                <td headers="th_itsd" class="td_dvr"><?php echo number_format($opt['od_send_cost']); ?></td>
                                <td headers="th_itst" class="td_mngsmall">
                                    <?php
                                    if ($opt['od_status'] == "주문" && $opt['od_pay_yn'] == "Y") {
                                        echo '입금';
                                    } else {
                                        echo $opt['od_status'];
                                    }
                                    ?>
                                </td>
                            </tr>
                            <?php } ?>
                            <?php
                                    $tot_point       += $point;

                                    $st_count1++;
                                    if($opt['od_status'] == '주문' || (($opt['od_status'] == '입금') && ($od['od_settle_case'] == '신용카드')))
                                        $st_count2++;
                                }
                            }

<<<<<<< HEAD
            <div class="tbl_head01 tbl_wrap">
				<table border="1">
				<tr>
					<td scope="col" style="width:30px;" align="center">성명</td>
					<td scope="col" style="width:40px;" align="center">HP</td>
					<td scope="col" style="width:50px;" align="center">주소</td>
					<td scope="col" style="width:50px;" align="center">배송회사</td>
					<td scope="col" style="width:50px;" align="center">운송장번호</td>
				</tr>
				<tbody>
<?
                $res = sql_query($sql);
                for($k=0; $rs=sql_fetch_array($res); $k++) {				
?>
					<tr align="center">
						<td><?=$rs["od_b_name"]?></td>
						<td><?=$rs["od_b_hp"]?></td>
						<td>
							<?=$rs["od_b_addr1"]?><br>
							<?=$rs["od_b_addr2"]?><br>
							<?=$rs["od_b_addr3"]?>
						</td>
						<td><?=$rs["od_delivery_company"]?></td>
						<td><?=$rs["od_invoice"]?></td>
					</tr>
<?
				}	
?>
                </tbody>
                </table>
            </div>
			<div>
				<center>
				<button type="button" id="btn_frmline_1" class="btn_frmline_1" onclick="javascript:location.href='/shop/orderinquiry.php';">주문내역확인</button>&nbsp;
				<button type="button" id="btn_frmline_3" class="btn_frmline_3" onclick="javascript:location.href='/';">메인으로가기</button>
				</center>
			</div>
        </section>

		<? } ?>

    </div>

    <div class="sod_right">
        <ul id="sod_bsk_tot">
            <li class="sod_bsk_dvr">
                <span>주문총액</span>
                <strong><?php echo number_format($od['od_cart_price']); ?> 원</strong>
            </li>
            <?php if($od['od_cart_coupon'] > 0) { ?>
            <li class="sod_bsk_dvr">
                <span>개별상품 쿠폰할인</span>
                <strong><?php echo number_format($od['od_cart_coupon']); ?> 원</strong>
                
            </li>
            <?php } ?>
            <?php if($od['od_coupon'] > 0) { ?>
            <li class="sod_bsk_dvr">
                <span>주문금액 쿠폰할인</span>
                <strong><?php echo number_format($od['od_coupon']); ?> 원</strong>
                
            </li>
            <?php } ?>
            <?php if ($od['od_send_cost'] > 0) { ?>
            <li class="sod_bsk_dvr">
                <span>배송비</span>
                <strong><?php echo number_format($od['od_send_cost']); ?> 원</strong>
                
            </li>
            <?php } ?>
            <?php if($od['od_send_coupon'] > 0) { ?>
            <li class="sod_bsk_dvr">
                <span>배송비 쿠폰할인</span>
                <strong><?php echo number_format($od['od_send_coupon']); ?> 원</strong>
                
            </li>
            <?php } ?>
            <?php if ($od['od_send_cost2'] > 0) { ?>
            <li class="sod_bsk_dvr">
                <span>추가배송비</span>
                <strong><?php echo number_format($od['od_send_cost2']); ?> 원</strong>
            </li>
            <?php } ?>
            <?php if ($od['od_cancel_price'] > 0) { ?>
            <li class="sod_bsk_dvr">
                <span>취소금액</span>
                <strong><?php echo number_format($od['od_cancel_price']); ?> 원</strong>
                
            </li>
            <?php } ?>
            <li class="sod_bsk_cnt" style="height: auto;">
                <div>
                    <span>총계</span>
                    <strong><?php echo number_format($tot_price); ?> 원</strong>
                    <?php if($member['mb_type']=='1' && $cancel_price == 0) {?>
                    <br>
                    <strong>(약국)</strong>&nbsp;
                    <span style="float: right;font-size: 1.2em;font-weight: bold; color: #000;"><?php echo number_format($tot_drug_price); ?> 원</span>
                    <?php } ?>
                </div>
            </li>
            <li class="sod_bsk_point">
                <span>적립포인트</span>
                <strong><?php echo number_format($tot_point); ?> 점</strong>
            </li>
        </ul>
        
        <section id="sod_fin_tot">
            <h2>결제합계</h2>

            <ul>
                <li>
                    총 구매액
                    <strong><?php echo display_price($tot_price); ?></strong>
                </li>
                <?php
                if ($misu_price > 0) {
                echo '<li>';
                echo '미결제액'.PHP_EOL;
                echo '<strong>'.display_price($misu_price).'</strong>';
                echo '</li>';
                }
                ?>
                <li id="alrdy">
                    결제액
                    <strong><?php echo $wanbul; ?></strong>
                    <?php if( $od['od_receipt_point'] ){    //포인트로 결제한 내용이 있으면 ?>
                    <div class="right">
                        <p><span class="title"><i class="fa fa-angle-right" aria-hidden="true"></i> 포인트 결제</span><?php echo number_format($od['od_receipt_point']); ?> 점</p>
                        <p><span class="title"><i class="fa fa-angle-right" aria-hidden="true"></i> 실결제</span><?php echo number_format($od['od_receipt_price']); ?> 원</p>
=======
                            // 주문 상품의 상태가 모두 주문이면 고객 취소 가능
                            if($st_count1 > 0 && $st_count1 == $st_count2)
                                $custom_cancel = true;
                            ?>
                        </tbody>
                    </table>
                </div>
            
            </section>
            <section class="">
                <h3 class="title-box">
                    <span class="left-side title-main">배송지/결제 정보</span>
                    <span class="right-side title-sub"><button type="button" class="btn-default btn-white-border">주문 취소</span>
                </h3>
                <div class="payment-result-box">
                    <div class="left-side">
                        <ul class="payment-box">
                            <li class="payment-list">
                                <p class="title">받으시는 분</p>
                                <p class="desc">오에스</p>
                            </li>
                            <li class="payment-list">
                                <p class="title">휴대폰 번호</p>
                                <p class="desc">010-1234-5678</p>
                            </li>
                            <li class="payment-list">
                                <p class="title">배송주소</p>
                                <p class="desc">
                                    <span class="br">[03994]</span>
                                    <span class="br">서울특별시 마포구 동교로 210</span>
                                    <span class="br">태전빌딩 2층</span>
                                </p>
                            </li>
                            <li class="payment-list">
                                <p class="title">배송 메시지</p>
                                <p class="desc">부재 시 경비실에 맡겨주세요.<br/>항상 안전하게 배송해주셔서 감사합니다.</p>
                            </li>
                        </ul>
                        <ul class="payment-box">
                            <li class="payment-list">
                                <p class="title">주문하시는 분</p>
                                <p class="desc">오에스</p>
                            </li>
                            <li class="payment-list">
                                <p class="title">휴대폰 번호</p>
                                <p class="desc">010-1234-5678</p>
                            </li>
                            <li class="payment-list">
                                <p class="title">이메일</p>
                                <p class="desc">orderstation@gmail.com</p>
                            </li>
                        </ul>
>>>>>>> OS-95_P
                    </div>
                    <div class="right-side">
                        <ul class="payment-box bg-box">
                            <li class="payment-list">
                                <p class="title">결제 수단</p>
                                <p class="desc">
                                    <span class="br">신용카드</span>
                                    <span class="br">현대카드(1234-****-****-****)</span>
                                    <span class="br">일시불</span>
                                </p>
                            </li>
                            <li class="payment-list">
                                <p class="title">결제 일시</p>
                                <p class="desc">2020-11-01 20:03:21</p>
                            </li>
                            <li class="payment-list">
                                <p class="title">총 상품 금액</p>
                                <p class="desc"><span class="amount">7,560</span><span class="unit">원</span></p>
                            </li>
                            <li class="payment-list">
                                <p class="title">배송비</p>
                                <p class="desc">무료</p>
                            </li>
                            <li class="payment-list">
                                <p class="title">최종 결제 금액</p>
                                <p class="desc"><span class="amount total-amount">7,560</span><span class="unit">원</span></p>
                            </li>
                            <li class="payment-list">
                                <p class="title">쿠폰 할인</p>
                                <p class="desc"><span class="amount">0</span><span class="unit">원</span></p>
                            </li>
                            <li class="payment-list">
                                <p class="title">적립 포인트</p>
                                <p class="desc"><span class="amount">500</span><span class="unit">원</span></p>
                            </li>
                            <li class="payment-list">
                                <p class="title">영수증</p>
                                <p class="desc"><button class="btn-default btn-white-border" type="button"><span>영수증 출력</span></button></p>
                            </li>
                        </ul>
                    </div>
                </div>
            </section>
            <div class="btn-set btn-set-md">
                <button class="btn-default btn-white"><span>주문 내역 조회</span></button>
                <button class="btn-default btn-orange"><span>쇼핑 계속하기</span></button>
            </div>
        </div>
    </div>
</div>
<!-- } 주문상세내역 끝 -->

<script>

history.pushState(null, document.title, location.href);
window.addEventListener('popstate', function (event) {
    //history.pushState(null, document.title, '/shop');
    location.href = '<?php echo G5_SHOP_URL;?>'
});

$(function() {
    $("#sod_sts_explan_open").on("click", function() {
        var $explan = $("#sod_sts_explan");
        if($explan.is(":animated"))
            return false;

        if($explan.is(":visible")) {
            $explan.slideUp(200);
            $("#sod_sts_explan_open").text("상태설명보기");
        } else {
            $explan.slideDown(200);
            $("#sod_sts_explan_open").text("상태설명닫기");
        }
    });

    $("#sod_sts_explan_close").on("click", function() {
        var $explan = $("#sod_sts_explan");
        if($explan.is(":animated"))
            return false;

        $explan.slideUp(200);
        $("#sod_sts_explan_open").text("상태설명보기");
    });
    $("#return_explan_open").on("click", function() {
        var $explan = $("#return_explan");
        if($explan.is(":animated"))
            return false;

        if($explan.is(":visible")) {
            $explan.slideUp(200);
            $("#return_explan_open").text("반품반려 이유보기");
        } else {
            $explan.slideDown(200);
            $("#return_explan_open").text("반품반려 이유닫기");
        }
    });

    $("#return_explan_close").on("click", function() {
        var $explan = $("#return_explan");
        if($explan.is(":animated"))
            return false;

        $explan.slideUp(200);
        $("#return_explan_open").text("반품반려 이유보기");
    });

	<? if( $order_receiver_cnt > 0 ){ ?>

	$("#cancel_memo").change(function(){

		var selected = $(this).find('option:selected');
        var extra = selected.data('pay_yn'); 

		if ( extra == "Y" ){
			$("#return_text_info").css("display","");
			$("#return_method_info").css("display","");
		}else{
			$("#return_text_info").css("display","none");
			$("#return_method_info").css("display","none");		
			$("input:checkbox[id='return_check_yn']").prop("checked", false);
			$("input:radio[name='od_return_method']").prop("checked", false);
		}

		if( $(this).val() == "기타" ){
			$("#memo_space").css("display","");
		}else{
			$("#memo_space").css("display","none");		
			$("#client_memo").val("");
		}
	});

	<? } ?>

});

function fcancel_check(f)
{
	<?if( $order_receiver_cnt > 0 ){?>

        var $ct_chk = $("input[name^=return_od_num]");
        var chked_cnt = $ct_chk.filter(":checked").size();
		if( chked_cnt == 0 ){
			alert("반품하실 상품을 체크해주세요.");
			return false;
		}
		//체크한 상품 문자열 연결해서 넘기기
		var chk = $("input[name^=return_od_num]:checked").map(function() {
			return this.value;
		}).get().join("|");

		$("#return_od_num_check").val(chk);

		var selected = $("#cancel_memo").find('option:selected');
        var pay_yn = selected.data('pay_yn'); 

		var Msg_1 = "주문을 정말 반품하시겠습니까?";
		var Msg_2 = "반품사유를 입력해 주십시오.";

		if( f.return_check_yn.checked == false && pay_yn == "Y" ){
			alert("배송료 부담 확인여부를 체크해주세요.");
			return false;
		}
		if( pay_yn == "Y" && !document.getElementsByName("od_return_method")[0].checked && !document.getElementsByName("od_return_method")[1].checked ){
			alert("왕복 배송비 결제 방법을 선택해주세요.");
			return false;
		}
	<?}else{?>
		var Msg_1 = "주문을 정말 취소하시겠습니까?";
		var Msg_2 = "취소사유를 입력해 주십시오.";
	<?}?>
    if(!confirm(Msg_1))
        return false;

    var memo = f.cancel_memo.value;
    if(memo == "") {
        alert(Msg_2);
        return false;
    }

    return true;
}
</script>

<?php
include_once('./_tail.php');
?>
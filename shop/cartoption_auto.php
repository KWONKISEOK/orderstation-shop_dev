<?php
include_once('./_common.php');

$pattern = '#[/\'\"%=*\#\(\)\|\+\&\!\$~\{\}\[\]`;:\?\^\,]#';
$it_id  = preg_replace($pattern, '', $_POST['it_id']);

if( $it_id == "S00140" || $it_id == "S00141" || $it_id == "S00142" || $it_id == "S00143" || $it_id == "S00144" || $it_id == "S00145" || $it_id == "188019" || $it_id == "S00687" || $it_id == "S00688" ){
	$temp_readonly = " readonly ";
}else{
	$temp_readonly = "";
}

$sql = " select * from {$g5['g5_shop_item_table']} where it_id = '$it_id' and it_use = '1' ";
$it = sql_fetch($sql);
$it_point = get_item_point($it);

if(!$it['it_id'])
    die('no-item');

// 장바구니 자료
$cart_id = get_session('ss_cart_id');
$sql = " select * from {$g5['g5_shop_cart_table']} where od_id = '$cart_id' and it_id = '$it_id' order by io_type asc, ct_id asc ";
$result = sql_query($sql);

$row=sql_fetch_array($result);

// 판매가격
//$sql2 = " select ct_price, it_name, ct_send_cost from {$g5['g5_shop_cart_table']} where od_id = '$cart_id' and it_id = '$it_id' order by ct_id asc limit 1 ";
//$row2 = sql_fetch($sql2);

if(!sql_num_rows($result))
    die('no-cart');
?>
<style>
	/* table정의 */
	.tbl-list_new {
		line-height: 18px; margin:10px auto;
		border-top:2px solid #6e6e6e; 
	}
	.tbl-list_new th{padding:5px 6px 5px 6px;}
	.tbl-list_new td{padding:5px 6px 5px 6px; }
	.tbl-list_new .bgcolor1{ background:#f6f6f6;}
	.tbl-list_new .bgcolor2{ background:#ffffff;}
	.tbl-list_new .bgcolor3{ background:#deefff;}
	.tbl-list_new .first{border-left:1px solid #c7c7c7;}
	.tbl-list_new .last{border-right:1px solid #c7c7c7;}
	.tbl-list_new .top{border-top:1px solid #c7c7c7; border-bottom:1px solid #c7c7c7;}
	.tbl-list_new .bottom{border-bottom:1px solid #c7c7c7;}
	.tbl-list_new .bottom02{border-bottom:2px solid #c7c7c7;}
	.tbl-list_new .left{ text-align:left;}
	.tbl-list_new .center{ text-align:center;}

	</style>
	<link rel="stylesheet" href="//code.jquery.com/ui/1.8.18/themes/base/jquery-ui.css" />
	<script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<h2>정기주문수정</h2>
<!-- 장바구니 옵션 시작 { -->
<form name="foption" method="post" action="<?php echo G5_SHOP_URL; ?>/cartupdate.php" onsubmit="return formcheck(this);">
<input type="hidden" name="act" value="auto_mod">
<input type="hidden" name="ct_id" value="<?php echo $row['ct_id']; ?>">
<input type="hidden" name="ct_period_yn" value="Y" />

<input type="hidden" name="sw_direct">

<section style="padding:20px;">
   

      
	<?php if (G5_IS_MOBILE) { ?>
	<table width="100%" cellpadding="0" cellspacing="0" class="tbl-list_new" id="ct_period_table" >
	  <tr>
		<td width="100" align="center" class="bgcolor1 bottom last font_bold">배송시작일</td>
		<td align="left" bgcolor="#e9f2da" class="bgcolor2 bottom"><input type="text" name="ct_period_sdate" id="ct_period_sdate" class="datepickerbutton"  size="10" value="<?php echo $row['ct_period_sdate']; ?>"></td>
	  </tr>
	  <tr>
		<td align="center" class="bottom last font_bold bgcolor1">배송주기</td>
		<td align="left" class="bottom bgcolor2">
		<? if($it_id == "S00687"||$it_id == "S00688"){ ?>
		  <input name="ct_period_week" id="ct_period_week2" type="radio" value="2" checked>
		  <label for="ct_period_week2">2주마다</label>&nbsp;&nbsp;
		<? }else{ ?>
		  <input name="ct_period_week" id="ct_period_week1" type="radio" value="1" <?php if($row['ct_period_week'] == '1') echo 'checked'; ?> >
		  <label for="ct_period_week1">매주</label><br/>
		  <input name="ct_period_week" id="ct_period_week2" type="radio" value="2" <?php if($row['ct_period_week'] == '2') echo 'checked'; ?>>
		  <label for="ct_period_week2">2주마다</label><br/>
		  <input name="ct_period_week" id="ct_period_week3" type="radio" value="3" <?php if($row['ct_period_week'] == '3') echo 'checked'; ?>>
		  <label for="ct_period_week3">3주마다</label><br/>
		  <input name="ct_period_week" id="ct_period_week4" type="radio" value="4" <?php if($row['ct_period_week'] == '4') echo 'checked'; ?>>
		  <label for="ct_period_week4">4주마다</label><br/>
		  <input name="ct_period_week" id="ct_period_week5" type="radio" value="5" <?php if($row['ct_period_week'] == '5') echo 'checked'; ?>>
		  <label for="ct_period_week5">지정일마다</label>
		<? } ?>
		</td>
	  </tr>
	  <tr>
		<td align="center" class="bottom last font_bold bgcolor1">배송횟수</td>
		<td align="left" class="bottom bgcolor2">
			<? if( $temp_readonly == "" ){ ?>
			<a href="javascript:fnc_suselect2(0,'ct_period_cnt')"><span style="font-size:1.4em;">&nbsp;-&nbsp;</span></a>
			<? } ?>
			<input type="text" name="ct_period_cnt" id="ct_period_cnt" style="ime-mode:disabled;text-align:right;" maxlength="2" size="3" onKeyPress="onlyNumber()" value="<?php echo $row['ct_period_cnt']; ?>" <?=$temp_readonly?> >
			<? if( $temp_readonly == "" ){ ?>
			<a href="javascript:fnc_suselect2(1,'ct_period_cnt')"><span style="font-size:1.4em;">&nbsp;+&nbsp;</span></a>
			<? } ?>
			회 <br/>(<font color="#FF0000">주문수량 * 배송횟수</font>)
		</td>
	  </tr>
	  <tr>
		<td align="center" class="bottom last font_bold bgcolor1">배송요구사항</td>
		<td align="left" class="bottom bgcolor2"><input type="text" name="ct_period_memo" id="ct_period_memo" size="18" maxlength="18" value="<?php echo $row['ct_period_memo']; ?>"></td>
	  </tr>
	</table>
	<?php } else { ?>
	<table width="100%" cellpadding="0" cellspacing="0" class="tbl-list_new" id="ct_period_table" >
	  <tr>
		<td width="142" align="center" class="bgcolor1 bottom last font_bold">배송시작일</td>
		<td align="left" bgcolor="#e9f2da" class="bgcolor2 bottom"><input type="text" name="ct_period_sdate" id="ct_period_sdate" class="datepickerbutton"  size="10" value="<?php echo $row['ct_period_sdate']; ?>"></td>
	  </tr>
	  <tr>
		<td align="center" class="bottom last font_bold bgcolor1">배송주기</td>
		<td align="left" class="bottom bgcolor2">
		<? if($it_id == "S00687"||$it_id == "S00688"){ ?>
		  <input name="ct_period_week" id="ct_period_week2" type="radio" value="2" checked>
		  <label for="ct_period_week2">2주마다</label>&nbsp;&nbsp;
		<? }else{ ?>
		  <input name="ct_period_week" id="ct_period_week1" type="radio" value="1" <?php if($row['ct_period_week'] == '1') echo 'checked'; ?> >
		  <label for="ct_period_week1">매주</label>&nbsp;&nbsp;
		  <input name="ct_period_week" id="ct_period_week2" type="radio" value="2" <?php if($row['ct_period_week'] == '2') echo 'checked'; ?>>
		  <label for="ct_period_week2">2주마다</label>&nbsp;&nbsp;
		  <input name="ct_period_week" id="ct_period_week3" type="radio" value="3" <?php if($row['ct_period_week'] == '3') echo 'checked'; ?>>
		  <label for="ct_period_week3">3주마다</label>&nbsp;&nbsp;
		  <input name="ct_period_week" id="ct_period_week4" type="radio" value="4" <?php if($row['ct_period_week'] == '4') echo 'checked'; ?>>
		  <label for="ct_period_week4">4주마다</label>&nbsp;&nbsp;
		  <input name="ct_period_week" id="ct_period_week5" type="radio" value="5" <?php if($row['ct_period_week'] == '5') echo 'checked'; ?>>
		  <label for="ct_period_week5">지정일마다</label>
		<? } ?>
		</td>
	  </tr>
	  <tr>
		<td align="center" class="bottom last font_bold bgcolor1">배송횟수</td>
		<td align="left" class="bottom bgcolor2">
			<? if( $temp_readonly == "" ){ ?>
			<a href="javascript:fnc_suselect2(0,'ct_period_cnt')"><span style="font-size:1.4em;">&nbsp;-&nbsp;</span></a>
			<? } ?>
			<input type="text" name="ct_period_cnt" id="ct_period_cnt" style="ime-mode:disabled;text-align:right;" maxlength="2" size="3" onKeyPress="onlyNumber()" value="<?php echo $row['ct_period_cnt']; ?>" <?=$temp_readonly?> >
			<? if( $temp_readonly == "" ){ ?>
			<a href="javascript:fnc_suselect2(1,'ct_period_cnt')"><span style="font-size:1.4em;">&nbsp;+&nbsp;</span></a>
			<? } ?>
			회 (<font color="#FF0000">주문수량 * 배송횟수</font>)
		</td>
	  </tr>
	  <tr>
		<td align="center" class="bottom last font_bold bgcolor1">배송요구사항</td>
		<td align="left" class="bottom bgcolor2"><input type="text" name="ct_period_memo" id="ct_period_memo" size="50" maxlength="50" value="<?php echo $row['ct_period_memo']; ?>"></td>
	  </tr>
	</table>
	<?php } ?>

</section>




<div class="btn_confirm">
    <input type="submit" value="정기주문신청" class="btn_submit">
    <button type="button" id="mod_option_close" class="btn_close"><i class="fa fa-times" aria-hidden="true"></i><span class="sound_only">닫기</span></button>
</div>
</form>

<script>
	$.datepicker.setDefaults({
		dateFormat: 'yy-mm-dd',
		prevText: '이전 달',
		nextText: '다음 달',
		monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
		monthNamesShort: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
		dayNames: ['일', '월', '화', '수', '목', '금', '토'],
		dayNamesShort: ['일', '월', '화', '수', '목', '금', '토'],
		dayNamesMin: ['일', '월', '화', '수', '목', '금', '토'],
		showMonthAfterYear: true,
		yearSuffix: '년'
	});

	$(function() {
		$(".datepickerbutton").datepicker();
	});
</script>
<!-- } 장바구니 옵션 끝 -->
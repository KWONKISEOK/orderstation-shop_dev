<?php
include_once('./_common.php');

$od_id = isset($od_id) ? preg_replace('/[^A-Za-z0-9\-_]/', '', strip_tags($od_id)) : 0;

if( isset($_GET['ini_noti']) && !isset($_GET['uid']) ){
    goto_url(G5_SHOP_URL.'/orderinquiry.php');
}

// 불법접속을 할 수 없도록 세션에 아무값이나 저장하여 hidden 으로 넘겨서 다음 페이지에서 비교함
$token = md5(uniqid(rand(), true));
set_session("ss_token", $token);

if (!$is_member) {
    if (get_session('ss_orderview_uid') != $_GET['uid'])
        alert("직접 링크로는 주문서 조회가 불가합니다.\\n\\n주문조회 화면을 통하여 조회하시기 바랍니다.", G5_SHOP_URL);
}

$sql = "select * from {$g5['g5_shop_order_table']} where od_id = '$od_id' {$service_type_and_query} ";
if($is_member && !$is_admin)
    $sql .= " and mb_id = '{$member['mb_id']}' ";
$od = sql_fetch($sql);
if (!$od['od_id'] || (!$is_member && md5($od['od_id'].$od['od_time'].$od['od_ip']) != get_session('ss_orderview_uid'))) {
    alert("조회하실 주문서가 없습니다.", G5_SHOP_URL);
}

$sql = "select sum(od_total_drug_price) as od_total_drug_price from {$g5['g5_shop_order_detail_table']} where od_id = '$od_id' {$service_type_and_query} ";
$od_dt = sql_fetch($sql);

// 주문 취소 데이터 확인
$sql = "select * from {$g5['g5_shop_order_cancel_table']} where od_id = '$od_id'";
$od_cancel = sql_fetch($sql);

// 결제방법
$settle_case = $od['od_settle_case'];

if (G5_IS_MOBILE) {
    include_once(G5_MSHOP_PATH.'/orderinquiryview.php');
    return;
}

// 테마에 orderinquiryview.php 있으면 include
if(defined('G5_THEME_SHOP_PATH')) {
    $theme_inquiryview_file = G5_THEME_SHOP_PATH.'/orderinquiryview.php';
    if(is_file($theme_inquiryview_file)) {
        include_once($theme_inquiryview_file);
        return;
        unset($theme_inquiryview_file);
    }
}

$g5['title'] = '주문상세내역';
include_once('./_head.php');


if(!G5_IS_MOBILE) {
    if ($is_member) {
        echo '<div class="site-wrap">
	    <div id="aside">&nbsp;';
        include_once(G5_SHOP_PATH.'/asidemy.php');
        echo '</div><div id="container">';
    } else {
        echo '<div class="site-wrap">';
        echo '<div id="container">';
    }
}

// LG 현금영수증 JS
if($od['od_pg'] == 'lg') {
    if($default['de_card_test']) {
    echo '<script language="JavaScript" src="http://pgweb.uplus.co.kr:7085/WEB_SERVER/js/receipt_link.js"></script>'.PHP_EOL;
    } else {
        echo '<script language="JavaScript" src="http://pgweb.uplus.co.kr/WEB_SERVER/js/receipt_link.js"></script>'.PHP_EOL;
    }
}
?>

<!-- 주문상세내역 시작 { -->
<div id="sod_fin">
    <div class="breadcrumb">
        <ul>
            <li><a href="#">장바구니</a></li>
            <li><a href="#">주문/결제</a></li>
            <li class="active"><a href="#">주문완료</a></li>
        </ul>
    </div>
    <div class="order-step-3">
        <div class="left-section">
            <div class="order-result-top">
                <p class="order-desc">고객님의 주문이 정상적으로 완료되었습니다.</p>
                <p class="order-number"><span class="label">주문번호</span><span class="desc"><?php echo $od_id; ?></span></p>
            </div>
            <section id="sod_fin_list">
                <h3 class="title-box">
                    <span class="left-side title-main">주문 상품</span>
                </h3>

                <?php
                $st_count1 = $st_count2 = 0;
                $custom_cancel = false;

                $sql = " select it_id, it_name, od_send_cost, 0 it_sc_type
                            from {$g5['g5_shop_order_detail_table']}
                            where od_id = '$od_id' {$service_type_and_query}
                            group by it_id
                            order by od_id ";
                $result = sql_query($sql);
                ?>
                <div class="table-body-border">
                    <table>
                    <colgroup>
                        <col width="*">
                        <col width="120px">
                        <col width="120px">
                        <col width="120px">
                        <col width="120px">
                        <col width="120px">
                    </colgroup>
                    <thead>
                        <tr>
                            <th scope="col" id="th_itname">상품정보</th>
                            <th scope="col" id="th_itqty">총수량</th>
                            <th scope="col" id="th_itsum">상품금액</th>
                            <th scope="col" id="th_itsd">배송비</th>
                            <th scope="col">총금액</th>
                            <th scope="col">상태</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                    $order_receiver_cnt = 0;
                    $return_possible = ""; //2차옵션 상품일경우 반품을 막기위한 조치

                    for($i=0; $row=sql_fetch_array($result); $i++) {
                        $image = get_it_image($row['it_id'], 70, 70);

                        $sql = " select a.od_id, a.it_name, a.od_option, a.od_qty qty, b.od_qty, a.od_price, od_total_sale_price, od_total_drug_price, od_total_incen, b.od_status, b.od_period_date, a.od_period_cnt, a.od_send_cost,b.od_delivery_company,b.od_invoice, a.od_num , b.od_bill_tno , a.io_id, b.od_pay_yn, b.od_return_confirm, b.od_pay_date
                                    from {$g5['g5_shop_order_detail_table']} a, tbl_shop_order_receiver b
                                    where a.od_id = b.od_id
                                      and a.od_num = b.od_num
                                      and a.od_id = '$od_id'
                                      and it_id = '{$row['it_id']}' {$service_type_and_query_a} ";

                        $res = sql_query($sql);
                        $rowspan = sql_num_rows($res);

                        $ReceiptHtml = "";

                        for($k=0; $opt=sql_fetch_array($res); $k++) {

                            //2차옵션 상품이 끼어있으면 반품신청을 못한다.
                            if( strpos( $opt['io_id'] , chr(30) ) !== false ){
                                $return_possible = "false";
                            }

                            if( !empty($opt['od_bill_tno']) ) {
                                $receipt_script = 'window.open(\'https://iniweb.inicis.com/DefaultWebApp/mall/cr/cm/mCmReceipt_head.jsp?noTid='.$opt['od_bill_tno'].'&noMethod=1\',\'receipt\',\'width=430,height=700\');';
                                $link_script = "&nbsp;<a href=\"javascript:;\" onclick=".$receipt_script.">[영수증 출력]</a>";
                                $ReceiptHtml .= "<tr><td>".get_text($opt['od_option'])."-".get_text($opt['od_period_date'])."&nbsp;".$link_script."</td></tr>";
                            }

                            if($opt[od_period_cnt] > 0) {
                                $subprice= $opt['od_total_sale_price'] / $opt[od_period_cnt];
                                $subincen= $opt['od_total_incen'] / $opt[od_period_cnt];
                            } else {
                                $subprice=$opt['od_total_sale_price'];
                                $subincen= $opt['od_total_incen'] ;
                            }

                            // 소비자가
                            $sql_price = "select it_id, it_cust_price, it_price, it_drug_price from tbl_shop_item where it_id = '$row[it_id]'";
                            $re_price = sql_fetch($sql_price);

                            $cust = '';
                            $sale_yn = '';
                            $sale = '';
                            if($re_price['it_cust_price']>0){ // 상품할인이 있는 경우
                                $sale_yn = 'y';
                                $cust = $re_price['it_cust_price'] * $opt['od_qty'];
                                $sale = ($cust - $subprice);
                            }


                            $tot_cust_price += ($sale + $subprice);
                            $tot_sale += $sale;
                            ?>
                        <tr>
                            <?php if($k == 0) { ?>
                            <td rowspan="<?php echo $rowspan; ?>">
                                <div class="item-detail">
                                    <div class="sod_img"><?php echo $image; ?> </div>
                                    <div class="sod_name">
                                        <?php
                                        if( $opt['od_status'] == "완료" || $opt['od_status'] == "반품반려" ) {
                                            $order_receiver_cnt++;
                                            ?>
                                            <input type="checkbox" name="return_od_num[]" value="<?=$opt['od_num']?>">
                                        <?php } ?>
                                        <a href="./item.php?it_id=<?php echo $row['it_id']; ?>"><b><?php echo $row['it_name']; ?></b></a>
                                        <div class="sod_opt">
                                            <ul>
                                                <li>
                                                    <?php echo get_text($opt['od_option']); ?>
                                                    <?php if ($opt['od_period_yn'] == 'Y') { ?>
                                                        - <?php echo get_text($opt['od_period_date']); ?>
                                                    <?php } ?>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </td>
                            <?php } ?>
                            <td headers="th_itqty" class="td_mngsmall"><?php echo number_format($opt['od_qty']); ?></td>
                            <td headers="th_itsum" class="td_numbig">
                                <?php
                                if($cust != ''){ ?>
                                    <span style="text-decoration:line-through; color: #757575; font-size: 13px;"><?php echo number_format($cust).'<span class="unit">원</span>' ; //정가 ?></span></br>
                                <?php } else if($member['mb_type']=='1') { ?>
                                    <span style="text-decoration:line-through; color: #757575; font-size: 13px;"><?php echo number_format($subprice).'<span class="unit">원</span>' ; //정가 ?></span></br>
                                <?php }
                                if($member['mb_type']=='1') {
                                    if ($opt[od_period_cnt] > 0) {
                                        $subprice = $opt['od_total_drug_price'] / $opt[od_period_cnt];
                                        $subincen = $opt['od_total_incen'] / $opt[od_period_cnt];
                                    } else {
                                        $subprice = $opt['od_total_drug_price'];
                                        $subincen = $opt['od_total_incen'];
                                    }
                                }
                                echo number_format($subprice).'<span class="unit">원</span>';
                                ?>
                            </td>

                            <td headers="th_itsd" class="td_dvr">
                                <?php if($od['od_company_yn'] == 'Y') {?>
                                    사내수령<br>배송비할인
                                <?php } else{
                                    echo ($od['od_send_cost'] == 0) ? '무료' : number_format($od['od_send_cost']).'<span class="unit">원</span>';
                                }?>
                            </td>
                            <td headers="th_itpt" class="td_numbig"><?php echo number_format($subprice+$od['od_send_cost']); ?><span class="unit">원</span></td>
                            <?php if($k == 0) { ?>
                            <td headers="th_itst" class="td_mngsmall" rowspan="<?php echo $rowspan; ?>">
                                <?php
                                if ($opt['od_status'] == "주문" && $opt['od_pay_yn'] == "Y") {
                                    echo '입금';
                                } else if( $opt['od_return_confirm'] == "Y" && $opt['od_status'] == "반품접수" ){
                                    echo "<font color='red'>반품승인</font>";
                                } else {
                                    echo $opt['od_status'];
                                }
                                ?>
                            </td>
                            <?php } ?>
                        </tr>
                        <?php
                            $tot_point += $subincen;
                            $st_count1++;
                            if($opt['od_status'] == '주문' || (($opt['od_status'] == '입금') && ($od['od_settle_case'] == '신용카드')))
                                $st_count2++;

                            $od_pay_date = $opt['od_pay_date'];
                        }
                    }

                    // 주문 상품의 상태가 모두 주문이면 고객 취소 가능
                    if($st_count1 > 0 && $st_count1 == $st_count2)
                        $custom_cancel = true;
                    ?>
                    </tbody>
                    </table>
                </div>

            </section>
            <section class="">
                <h3 class="title-box">
                    <span class="left-side title-main">배송지/결제 정보</span>
                    <?php
                    //배송완료인 상태는 반품이 가능하게끔 처리해주기
                    if( $order_receiver_cnt > 0 ){

                        $custom_cancel = true;
                        $button_name = "반품 신청하기";
                        $submit_url = "./orderinquiryreturn.php";
                        $txt_1 = "주문 반품";
                        $txt_2 = "반품 사유";
                        $txt_3 = "<option value='기타' data-pay_yn='Y'>기타</option>";
                        $txt_4 = "";

                    }else{

                        $return_possible = "";
                        $button_name = "주문 취소하기";
                        $submit_url = "./orderinquirycancel.php";
                        $txt_1 = "주문 취소";
                        $txt_2 = "취소 사유";
                        $txt_3 = "";
                        $txt_4 = " display:none; ";

                    }
                    ?>
                    <?php
                    if( $od['od_status'] != "반품접수" && $od['od_status'] != "반품완료" ) {
                        if ($cancel_price == 0 && $custom_cancel) { ?>
                            <span class="right-side title-sub">
                                <button type="button" class="btn-default btn-white-border" onclick="fcancel_pop();"><?php echo $txt_1 ?>
                            </span>
                        <?php }
                    } ?>
                </h3>
                <div class="payment-result-box">
                    <div class="left-side">
                        <?php if ($od['od_multi_yn'] != "Y") { ?>
                        <ul class="payment-box">
                            <li class="payment-list">
                                <p class="title">받으시는 분</p>
                                <p class="desc"><?php echo get_text($od['od_b_name']); ?></p>
                            </li>
                            <li class="payment-list">
                                <p class="title">휴대폰 번호</p>
                                <p class="desc"><?php echo get_text($od['od_b_hp']); ?></p>
                            </li>
                            <li class="payment-list">
                                <p class="title">배송주소</p>
                                <p class="desc">
                                    <span class="br">[<?php echo get_text(sprintf("%s%s", $od['od_b_zip1'], $od['od_b_zip2'])); ?>]</span>
                                    <span class="br"><?php echo print_address($od['od_b_addr1'], '', $od['od_b_addr3'], $od['od_b_addr_jibeon']); ?></span>
                                    <!--<span class="br"><?php /*echo get_text($od['od_b_addr3']); */?></span>-->
                                    <span class="br"><?php echo get_text($od['od_b_addr2']); ?></span>
                                </p>
                            </li>
                            <li class="payment-list">
                                <?php
                                $sql = "select od_trans_memo from tbl_shop_order_receiver where od_id='{$od['od_id']}' limit 1";
                                $fetch = sql_fetch($sql);
                                $od_trans_memo = $fetch['od_trans_memo'];
                                ?>
                                <p class="title">배송 메시지</p>
                                <p class="desc"><?php echo get_text($od_trans_memo); ?></p>
                            </li>
                        </ul>
                        <ul class="payment-box">
                            <li class="payment-list">
                                <p class="title">주문하시는 분</p>
                                <p class="desc"><?php echo get_text($od['od_name']); ?></p>
                            </li>
                            <li class="payment-list">
                                <p class="title">휴대폰 번호</p>
                                <p class="desc"><?php echo get_text($od['od_hp']); ?></p>
                            </li>
                            <?php if ($od['od_email']) { ?>
                            <li class="payment-list">
                                <p class="title">이메일</p>
                                <p class="desc"><?php echo get_text($od['od_email']); ?></p>
                            </li>
                            <?php } ?>
                        </ul>
                        <?php if ($od['od_settle_case'] == '무통장' && $is_drug == false) { ?>
                        <ul class="payment-box">
                            <li class="payment-list">
                                <p class="title">입금자명</p>
                                <p class="desc"><?php echo get_text($od['od_deposit_name']); ?></p>
                            </li>
                            <li class="payment-list">
                                <p class="title">입금계좌</p>
                                <p class="desc"><?php echo get_text($od['od_bank_account']); ?></p>
                            </li>
                            <li class="payment-list">
                                <p class="title">입금기한</p>
                                <p class="desc"><?php echo get_text($od['od_bank_date']); ?></p>
                            </li>
                        </ul>
                        <?php } ?>
                        <?php } ?>

                        <?php
                        //다중배송일때.
                        if ($od['od_multi_yn'] == "Y") {

                        $sql = " select a.comp_code, b.od_date3, a.od_send_yn , b.od_status, b.od_trans_memo, c.*
                            from tbl_shop_order_detail a, tbl_shop_order_receiver b, tbl_shop_order_multi_receiver c
                            where a.od_id = b.od_id and a.od_num= b.od_num and a.od_id = c.od_id and a.od_num= c.od_num and a.od_id = '$od_id' ";

                        $res = sql_query($sql);
                        for($k=0; $rs=sql_fetch_array($res); $k++) {
                            ?>
                            <ul class="payment-box">
                                <li class="payment-list">
                                    <p class="title">받으시는 분</p>
                                    <p class="desc"><?php echo get_text($rs['od_b_name']); ?></p>
                                </li>
                                <li class="payment-list">
                                    <p class="title">휴대폰 번호</p>
                                    <p class="desc"><?php echo get_text($rs['od_b_hp']); ?></p>
                                </li>
                                <li class="payment-list">
                                    <p class="title">배송주소</p>
                                    <p class="desc">
                                        <span class="br">[<?php echo get_text(sprintf("%s%s", $rs['od_b_zip1'], $rs['od_b_zip2'])); ?>]</span>
                                        <span class="br"><?php echo print_address($rs['od_b_addr1'], '', $rs['od_b_addr3'], $rs['od_b_addr_jibeon']); ?></span>
                                        <!--<span class="br"><?php /*echo get_text($od['od_b_addr3']); */?></span>-->
                                        <span class="br"><?php echo get_text($rs['od_b_addr2']); ?></span>
                                    </p>
                                </li>
                                <li class="payment-list">
                                    <p class="title">배송 메시지</p>
                                    <p class="desc"><?php echo get_text($rs["od_trans_memo"]); ?></p>
                                </li>
                                <li class="payment-list">
                                    <p class="title">배송회사</p>
                                    <p class="desc"><?php echo get_text($rs["od_delivery_company"]); ?></p>
                                </li>
                                <li class="payment-list">
                                    <p class="title">운송장번호</p>
                                    <p class="desc"><?php echo get_text($rs["od_invoice"]); ?></p>
                                </li>
                            </ul>
                        <?php } ?>

                        <ul class="payment-box">
                            <li class="payment-list">
                                <p class="title">주문하시는 분</p>
                                <p class="desc"><?php echo get_text($od['od_name']); ?></p>
                            </li>
                            <li class="payment-list">
                                <p class="title">휴대폰 번호</p>
                                <p class="desc"><?php echo get_text($od['od_hp']); ?></p>
                            </li>
                            <?php if ($od['od_email']) { ?>
                            <li class="payment-list">
                                <p class="title">이메일</p>
                                <p class="desc"><?php echo get_text($od['od_email']); ?></p>
                            </li>
                            <?php } ?>
                        </ul>
                        <?php if ($od['od_settle_case'] == '무통장' && $is_drug == false) { ?>
                        <ul class="payment-box">
                            <li class="payment-list">
                                <p class="title">입금자명</p>
                                <p class="desc"><?php echo get_text($od['od_deposit_name']); ?></p>
                            </li>
                            <li class="payment-list">
                                <p class="title">입금계좌</p>
                                <p class="desc"><?php echo get_text($od['od_bank_account']); ?></p>
                            </li>
                            <li class="payment-list">
                                <p class="title">입금기한</p>
                                <p class="desc"><?php echo get_text($od['od_bank_date']); ?></p>
                            </li>
                        </ul>
                        <?php } ?>
                        <?php } ?>
                    </div>
                    <div class="right-side">
                        <ul class="payment-box bg-box">
                            <?php
                            // 총계 = 주문상품금액합계 + 배송비 - 상품할인 - 결제할인 - 배송비할인
                            $tot_price = $od['od_cart_price'] + $od['od_send_cost'] + $od['od_send_cost2']
                                - $od['od_cart_coupon'] - $od['od_coupon'] - $od['od_send_coupon'] - $od['od_receipt_point']
                                - $od['od_cancel_price'];
                            $tot_drug_price = $od_dt['od_total_drug_price'] + $od['od_send_cost'] + $od['od_send_cost2']
                                - $od['od_cart_coupon'] - $od['od_coupon'] - $od['od_send_coupon'] - $od['od_receipt_point']
                                - $od['od_cancel_price'];

                            $receipt_price  = $od['od_receipt_price'] + $od['od_receipt_point'];
                            $cancel_price   = $od['od_cancel_price'];

                            $misu = true;
                            $misu_price = $tot_price - $receipt_price - $cancel_price;

                            if ($misu_price == 0 && ($od['od_cart_price'] > $od['od_cancel_price'])) {
                                $wanbul = " (완불)";
                                $misu = false; // 미수금 없음
                            }
                            else
                            {
                                $wanbul = display_price($receipt_price);
                            }

                            // 결제정보처리
                            if($od['od_receipt_price'] > 0)
                                $od_receipt_price = display_price($od['od_receipt_price']);
                            else
                                $od_receipt_price = '아직 입금되지 않았거나 입금정보를 입력하지 못하였습니다.';

                            $app_no_subj = '';
                            $disp_bank = true;
                            $disp_receipt = false;
                            if($od['od_settle_case'] == '신용카드' || $od['od_settle_case'] == 'KAKAOPAY' || is_inicis_order_pay($od['od_settle_case']) ) {
                                $app_no_subj = '승인번호';
                                $app_no = $od['od_app_no'];
                                $disp_bank = false;
                                $disp_receipt = true;
                            } else if($od['od_settle_case'] == '간편결제') {
                                $app_no_subj = '승인번호';
                                $app_no = $od['od_app_no'];
                                $disp_bank = false;
                                switch($od['od_pg']) {
                                    case 'lg':
                                        $easy_pay_name = 'PAYNOW';
                                        break;
                                    case 'inicis':
                                        $easy_pay_name = 'KPAY';
                                        break;
                                    case 'kcp':
                                        $easy_pay_name = 'PAYCO';
                                        break;
                                    default:
                                        break;
                                }
                            } else if($od['od_settle_case'] == '휴대폰') {
                                $app_no_subj = '휴대폰번호';
                                $app_no = $od['od_bank_account'];
                                $disp_bank = false;
                                $disp_receipt = true;
                            } else if($od['od_settle_case'] == '가상계좌' || $od['od_settle_case'] == '계좌이체') {
                                $app_no_subj = '거래번호';
                                $app_no = $od['od_tno'];
                            }

                            $od_data_check = true;
                            if ($od['od_status'] == '취소' || strpos($od['od_status'], '반품')) {
                                if ($od_cancel['od_id']) {
                                    // 주문상태에 따른 값 처리
                                    $od_send_cost = $od_cancel['od_send_cost'];
                                    $od_send_cost2 = $od_cancel['od_send_cost2'];
                                    $tot_price = $od_cancel['od_receipt_price'];
                                    $od_coupon = $od_cancel['od_coupon'];
                                    $od_receipt_point = $od_cancel['od_receipt_point'];
                                } else {
                                    // 과거 데이터는 취소 시 기존 데이터가 0 처리되어 확인이 불가
                                    $od_send_cost = $od['od_send_cost'];
                                    $od_send_cost2 = $od['od_send_cost2'];
                                    $tot_price = $od['od_cart_price'];
                                    $od_coupon = $od['od_coupon'];
                                    $od_receipt_point = $od['od_receipt_point'];
                                    $od_data_check = false;
                                }
                            } else {
                                $od_send_cost = $od['od_send_cost'];
                                $od_send_cost2 = $od['od_send_cost2'];
                                $od_coupon = $od['od_coupon'];
                                $od_receipt_point = $od['od_receipt_point'];
                            }


                            ?>
                            <li class="payment-list">
                                <p class="title">결제 수단</p>
                                <p class="desc">
                                    <span class="br"><?php echo ($easy_pay_name ? $easy_pay_name.'('.$od['od_settle_case'].')' : check_pay_name_replace($od['od_settle_case']) ); ?></span>
                                    <?php if ($od['od_settle_case'] == '신용카드') { ?>
                                    <!--<span class="br">현대카드(1234-****-****-****)</span>-->
                                    <!--<span class="br">일시불</span>-->
                                    <?php } ?>
                                </p>
                            </li>
                            <li class="payment-list">
                                <p class="title">결제 일시</p>
                                <p class="desc">
                                    <?php
                                    if ($od_pay_date != "" && $od_pay_date != "0000-00-00 00:00:00") {
                                        $od_pay_date_str = $od_pay_date;
                                    } else if ($od['od_receipt_time'] != "0000-00-00 00:00:00") {
                                        $od_pay_date_str = $od['od_receipt_time'];
                                    } else {
                                        $od_pay_date_str = "입금확인시 노출됩니다.";
                                    }

                                    echo $od_pay_date_str;
                                    ?>
                                </p>
                            </li>
                            <?php if ($od_data_check == true) { ?>
                            <li class="payment-list">
                                <p class="title">소비자가</p>
                                <p class="desc"><span class="amount"><?php echo number_format($tot_cust_price); ?></span><span class="unit">원</span></p>
                            </li>
                            <?php if($tot_sale > 0) { ?>
                            <li class="payment-list">
                                <p class="title">상품할인</p>
                                <p class="desc">- <span class="amount"><?php echo number_format($tot_sale); ?></span><span class="unit">원</span></p>
                            </li>
                            <?php } ?>
                            <?php if ($od_coupon > 0) {?>
                                <li class="payment-list">
                                    <p class="title">쿠폰 할인</p>
                                    <p class="desc">- <span class="amount"><?php echo number_format($od_coupon); ?></span><span class="unit">원</span></p>
                                </li>
                            <?php } ?>
                            <?php
                            if($member['mb_referee'] == 4 || $member['mb_extra'] == 11){
                                $p_name = "적립금";
                            }else{
                                $p_name = "포인트";
                            }
                                if ($od['od_receipt_point'] > 0) { ?>
                                <li class="payment-list">
                                    <p class="title">사용 <?php echo $p_name ?> </p>
                                    <p class="desc">- <span class="amount"><?php echo number_format($od['od_receipt_point']); ?></span><span class="unit">원</span></p>
                                </li>
                            <?php } ?>
                            <?php if($member['mb_type']=='1') {?>
                                <li class="payment-list">
                                    <p class="title">포인트 할인</p>
                                    <p class="desc">- <span class="amount"><?php echo number_format($tot_price-$tot_drug_price); ?></span><span class="unit">원</span></p>
                                </li>
                            <?php } ?>
                            <li class="payment-list">
                                <p class="title">배송비</p>
                                <p class="desc">
                                    <?php  if($od['od_company_yn'] == 'Y') {?>
                                        사내수령배송비할인
                                    <?php } else{
                                        echo ($od_send_cost == 0) ? '무료' : number_format($od_send_cost).'<span class="unit">원</span>';
                                    }?></p>
                            </li>
                            <?php } ?>
                            <?php if ($od_send_cost2 > 0) { ?>
                            <li class="payment-list">
                                <p class="title">추가배송비</p>
                                <p class="desc"><?php echo number_format($od_send_cost2).'<span class="unit">원</span>'; ?></p>
                            </li>
                            <?php } ?>
                            <?php if($member['mb_type']=='1' && $cancel_price == 0) {?>
                                <li class="payment-list">
                                    <p class="title" style="color:#ff006c;">총 약국 결제 금액</p>
                                    <p class="desc"><span class="amount total-amount"><?php echo number_format($tot_drug_price); ?></span><span class="unit">원</span></p>
                                </li>
                            <?php } else{ ?>
                                <li class="payment-list">
                                    <p class="title">최종 결제 금액</p>
                                    <p class="desc"><span class="amount total-amount"><?php echo number_format($tot_price); ?></span><span class="unit">원</span></p>
                                </li>
                            <?php } ?>
                            <?php if ($od['od_cancel_price'] > 0) { ?>
                            <!--<li class="payment-list">
                                <p class="title" style="color: #ff6215;">취소 금액</p>
                                <p class="desc" style="color: #ff6215;"><span class="amount total-amount" style="color: #ff6215;"><?php /*echo number_format($od['od_cancel_price']); */?></span><span class="unit">원</span></p>
                            </li>-->
                            <?php }?>
                            <!--<li class="payment-list">
                                <p class="title">적립 포인트</p>
                                <p class="desc"><span class="amount"><?php /*echo number_format($tot_point); */?></span><span class="unit">원</span></p>
                            </li>-->
                            <?php if($disp_receipt) { ?>
                            <li class="payment-list">
                                <p class="title">영수증</p>
                                <p class="desc">
                                    <?php
                                    if ($od['od_settle_case'] == '휴대폰') {
                                        if($od['od_pg'] == 'lg') {
                                            require_once G5_SHOP_PATH.'/settle_lg.inc.php';
                                            $LGD_TID      = $od['od_tno'];
                                            $LGD_MERTKEY  = $config['cf_lg_mert_key'];
                                            $LGD_HASHDATA = md5($LGD_MID.$LGD_TID.$LGD_MERTKEY);

                                            $receipt_script = 'showReceiptByTID(\''.$LGD_MID.'\', \''.$LGD_TID.'\', \''.$LGD_HASHDATA.'\');';
                                        } else if($od['od_pg'] == 'inicis') {
                                            $receipt_script = 'window.open(\'https://iniweb.inicis.com/DefaultWebApp/mall/cr/cm/mCmReceipt_head.jsp?noTid='.$od['od_tno'].'&noMethod=1\',\'receipt\',\'width=430,height=700\');';
                                        } else {
                                            $receipt_script = 'window.open(\''.G5_BILL_RECEIPT_URL.'mcash_bill&tno='.$od['od_tno'].'&order_no='.$od['od_id'].'&trade_mony='.$od['od_receipt_price'].'\', \'winreceipt\', \'width=500,height=690,scrollbars=yes,resizable=yes\');';
                                        }
                                    } else if ($od['od_settle_case'] == '신용카드' || is_inicis_order_pay($od['od_settle_case'])) {
                                        if($od['od_pg'] == 'lg') {
                                            require_once G5_SHOP_PATH.'/settle_lg.inc.php';
                                            $LGD_TID      = $od['od_tno'];
                                            $LGD_MERTKEY  = $config['cf_lg_mert_key'];
                                            $LGD_HASHDATA = md5($LGD_MID.$LGD_TID.$LGD_MERTKEY);

                                            $receipt_script = 'showReceiptByTID(\''.$LGD_MID.'\', \''.$LGD_TID.'\', \''.$LGD_HASHDATA.'\');';
                                        } else if($od['od_pg'] == 'inicis') {
                                            $receipt_script = 'window.open(\'https://iniweb.inicis.com/DefaultWebApp/mall/cr/cm/mCmReceipt_head.jsp?noTid='.$od['od_tno'].'&noMethod=1\',\'receipt\',\'width=430,height=700\');';
                                        } else {
                                            $receipt_script = 'window.open(\''.G5_BILL_RECEIPT_URL.'card_bill&tno='.$od['od_tno'].'&order_no='.$od['od_id'].'&trade_mony='.$od['od_receipt_price'].'\', \'winreceipt\', \'width=470,height=815,scrollbars=yes,resizable=yes\');';
                                        }
                                    } else if ($od['od_settle_case'] == 'KAKAOPAY') {
                                        $receipt_script = 'window.open(\'https://mms.cnspay.co.kr/trans/retrieveIssueLoader.do?TID='.$od['od_tno'].'&type=0\', \'popupIssue\', \'toolbar=no,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=yes,width=420,height=540\');';
                                    }
                                    ?>
                                    <button class="btn-default btn-white-border" type="button" onclick="<?php echo $receipt_script; ?>">
                                        <span>영수증 출력</span>
                                    </button>
                                </p>
                            </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            </section>
            <div class="btn-set btn-set-md">
                <button class="btn-default btn-white" onclick="location.href='/shop/orderinquiry.php';"><span>주문 내역 조회</span></button>
                <a href="<?php echo G5_SHOP_URL; ?>" class="btn-default btn-orange"><span>쇼핑 계속하기</span></a>
            </div>
        </div>
    </div>
</div>
<!-- } 주문상세내역 끝 -->

<script>

    history.pushState(null, document.title, location.href);
    window.addEventListener('popstate', function (event) {
        //history.pushState(null, document.title, '/shop');
        location.href = '<?php echo G5_SHOP_URL;?>'
    });

    $(function() {
        $("#sod_sts_explan_open").on("click", function() {
            var $explan = $("#sod_sts_explan");
            if($explan.is(":animated"))
                return false;

            if($explan.is(":visible")) {
                $explan.slideUp(200);
                $("#sod_sts_explan_open").text("상태설명보기");
            } else {
                $explan.slideDown(200);
                $("#sod_sts_explan_open").text("상태설명닫기");
            }
        });

        $("#sod_sts_explan_close").on("click", function() {
            var $explan = $("#sod_sts_explan");
            if($explan.is(":animated"))
                return false;

            $explan.slideUp(200);
            $("#sod_sts_explan_open").text("상태설명보기");
        });
        $("#return_explan_open").on("click", function() {
            var $explan = $("#return_explan");
            if($explan.is(":animated"))
                return false;

            if($explan.is(":visible")) {
                $explan.slideUp(200);
                $("#return_explan_open").text("반품반려 이유보기");
            } else {
                $explan.slideDown(200);
                $("#return_explan_open").text("반품반려 이유닫기");
            }
        });

        $("#return_explan_close").on("click", function() {
            var $explan = $("#return_explan");
            if($explan.is(":animated"))
                return false;

            $explan.slideUp(200);
            $("#return_explan_open").text("반품반려 이유보기");
        });

        <? if( $order_receiver_cnt > 0 ){ ?>

        $("#cancel_memo").change(function(){

            var selected = $(this).find('option:selected');
            var extra = selected.data('pay_yn');

            if ( extra == "Y" ){
                $("#return_text_info").css("display","");
                $("#return_method_info").css("display","");
            }else{
                $("#return_text_info").css("display","none");
                $("#return_method_info").css("display","none");
                $("input:checkbox[id='return_check_yn']").prop("checked", false);
                $("input:radio[name='od_return_method']").prop("checked", false);
            }

            if( $(this).val() == "기타" ){
                $("#memo_space").css("display","");
            }else{
                $("#memo_space").css("display","none");
                $("#client_memo").val("");
            }
        });

        <? } ?>

    });

    function fcancel_pop() {
        var return_od_num_check = '';
        var order_receiver_cnt = 0;
        order_receiver_cnt = <?php echo ($order_receiver_cnt > 0) ? $order_receiver_cnt: 0; ?>;

        if (order_receiver_cnt > 0) {
            var $ct_chk = $("input[name^=return_od_num]");
            var chked_cnt = $ct_chk.filter(":checked").size();
            if (chked_cnt == 0) {
                alert("반품하실 상품을 체크해주세요.");
                return false;
            }
            //체크한 상품 문자열 연결해서 넘기기
            var chk = $("input[name^=return_od_num]:checked").map(function () {
                return this.value;
            }).get().join("|");

            return_od_num_check = chk;
        }

        var params = "?od_id=<?php echo $od_id; ?>&order_receiver_cnt="+order_receiver_cnt+"&return_od_num_check="+return_od_num_check;
        window.open('<?php echo G5_SHOP_URL; ?>/orderinquiryview.pop.php'+params, '주문취소', 'width=550,height=400,scrollbars=1,menus=0');

    }

</script>

<?php
include_once('./_tail.php');
?>
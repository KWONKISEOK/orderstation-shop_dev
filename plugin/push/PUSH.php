<?php
if (!defined('_GNUBOARD_')) exit;

//include_once('./_common.php');
include_once(G5_PUSH_PATH.'/_common.php');
include_once(G5_PUSH_PATH.'/FCM.php');
include_once(G5_PUSH_PATH.'/APNS.php');

class Push
{
    private static $self;

    private $fcm;
    private $apns;

    public function __construct()
    {
        $this->fcm = Fcm::getInstance();
        $this->apns = Apns::getInstance();
    }

    public static function getInstance()
    {
        if (!self::$self) self::$self = new Push();
        return self::$self;
    }

    public function sendPush($os, $title, $message, $token)
    {
        if ($os == 'A') {
            $result = $this->fcm->send($title, $message, $token);
        } else {
            $result = $this->apns->send($title, $message, $token);
        }

        return $result;
    }
}
?>
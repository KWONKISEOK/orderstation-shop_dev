<?php
if (!defined('_GNUBOARD_')) exit;

//include_once('./_common.php');
include_once(G5_PUSH_PATH.'/_common.php');

class Apns
{
    private static $self;

    private $apnsCert;
    private $apnsHost;
    private $apnsPort;

    public function __construct()
    {
        $this->apnsHost = APNS_HOST;
        $this->apnsPort = APNS_PORT;
        $this->apnsCert = APNS_CERT;
        
        sys_log('APNs Host: ' . $this->apnsHost);
        sys_log('APNs Port: ' . $this->apnsPORT);
        sys_log('APNs Cert: ' . $this->apnsCert);
        
    }

    public static function getInstance()
    {
        if (!self::$self) self::$self = new Apns();
        return self::$self;
    }

    public function send($title, $message, $id)
    {
        sys_log('APNs send');
        
        if (!file_exists($this->apnsCert)) {
            sys_log('APNS cert file not exist');
            return false;
        }

        $payload = array('aps' => array(
            'alert' => array(
                'title' => $title,
                'body' => $message
            ), 
            'badge' => 0, 
            'sound' => 'default'
        ));

        $payload = json_encode($payload);

        $streamContext = stream_context_create();
        stream_context_set_option($streamContext, 'ssl', 'local_cert', $this->apnsCert);

        $apns = stream_socket_client('ssl://' . $this->apnsHost . ':' . $this->apnsPort, $error, $errorString, 2, STREAM_CLIENT_CONNECT, $streamContext);

        if ($apns) {
            $apnsMessage = chr(0) . chr(0) . chr(32) . pack('H*', str_replace(' ', '', $id)) . chr(0) . chr(strlen($payload)) . $payload;
        }

        fwrite($apns, $apnsMessage);

        fclose($apns);
    }
}
?>
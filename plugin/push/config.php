<?php
if (!defined('_GNUBOARD_')) exit;

/*************************************************************************
**
**  push에 관련된 환경설정
**
*************************************************************************/

if ($_SERVER['HTTP_HOST'] == "103.219.124.254" || $_SERVER['HTTP_HOST'] == "orderstation.kr") {
	
	//========================== Google FCM - PRODUCTION START ============================================//
	
	//define('FCM_KEY', 'AAAAwYGJ3A8:APA91bFBlZFdto_bSaQwiSRiTu56TT602nH4ryId7HT35eUViELFonUU22g93Y2RNmKpq6EFQm-oM15yGSbj7QgOQPxm88hb58eyT681ZLnRhw7OyAXejBnP5T7wNq0wts8-f20w4Wyq');// Old 서버 키 : New 서버키가 정상적으로 동작하지 않는 경우에 사용
	//define('FCM_KEY', 'AIzaSyAYTz0PWqAXvOJwZGlUnVTfgP3VWPlRaDc');
	
	// New 서버 키 
	define('FCM_KEY', 'AAAAsqw45Xo:APA91bHWo6nUbL02ttodbrsNNxjVt6Vly5ovFxLi48WIBWZOC0k9h8N15FVAYfGUsZvavAd2nUHAkx2HjmfFMREs3svlXlVZfCl1ktI4vEUuMjDssKjF0hnmwjfVVwDUNMPL8_uL3hzi');
	// Old 서버 키 : New 서버키가 정상적으로 동작하지 않는 경우에 사용
	//define('FCM_KEY', 'AIzaSyAY5Jjruxmzkgd1V_MrFpF_VNVOXIkAPqI');
	
	define('FCM_URL', 'https://fcm.googleapis.com/fcm/send');

	//========================== Google FCM - PRODUCTION END ============================================//
	
	// Apple APNS Info - PRODUCTION
	/*
	define('APNS_HOST', 'gateway.push.apple.com');
	define('APNS_CERT', '/var/www/html/orderstation-shop/plugin/push/cert/apns.pem');
	define('APNS_PORT.apns.port', '2195');
	*/
	// Apple APNS Info - DEVELOPMENT
	define('APNS_HOST', 'gateway.sandbox.push.apple.com');
	define('APNS_CERT', '/var/www/html/orderstation-shop/plugin/push/cert/apns_development.pem');
	define('APNS_PORT', '2195');
	
} else {
	
	//========================== Google FCM - DEVELOPMENT START ============================================//
	
	//define('FCM_KEY', 'AAAAwYGJ3A8:APA91bFBlZFdto_bSaQwiSRiTu56TT602nH4ryId7HT35eUViELFonUU22g93Y2RNmKpq6EFQm-oM15yGSbj7QgOQPxm88hb58eyT681ZLnRhw7OyAXejBnP5T7wNq0wts8-f20w4Wyq');// Old 서버 키 : New 서버키가 정상적으로 동작하지 않는 경우에 사용
	//define('FCM_KEY', 'AIzaSyAYTz0PWqAXvOJwZGlUnVTfgP3VWPlRaDc');
	
	// New 서버 키 
	define('FCM_KEY', 'AAAAsqw45Xo:APA91bHWo6nUbL02ttodbrsNNxjVt6Vly5ovFxLi48WIBWZOC0k9h8N15FVAYfGUsZvavAd2nUHAkx2HjmfFMREs3svlXlVZfCl1ktI4vEUuMjDssKjF0hnmwjfVVwDUNMPL8_uL3hzi');
	// Old 서버 키 : New 서버키가 정상적으로 동작하지 않는 경우에 사용
	//define('FCM_KEY', 'AIzaSyAY5Jjruxmzkgd1V_MrFpF_VNVOXIkAPqI');
	
	define('FCM_URL', 'https://fcm.googleapis.com/fcm/send');

	//========================== Google FCM - DEVELOPMENT END ============================================//

	// Apple APNS Info - DEVELOPMENT
	define('APNS_HOST', 'gateway.sandbox.push.apple.com');
	define('APNS_CERT', '/var/www/html/orderstation-shop/plugin/push/cert/apns_development.pem');
	define('APNS_PORT', '2195');
}

// Log Path
define('PUSH_LOG_PATH', '/logs/');
?>
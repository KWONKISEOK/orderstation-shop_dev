<?php
if (!defined('_GNUBOARD_')) exit;

include_once(G5_PUSH_PATH.'/PUSH.php');

/*************************************************************************
**
**  push에 사용할 함수 모음
**
*************************************************************************/

// $_POST value 체크
function post_if_set($val, $default="")
{
	$var = isset($_POST[$val]) && !empty($_POST[$val]) ? trim($_POST[$val]) : $default;
	return $var;
}

// $_GET value 체크
function get_if_set($val, $default="")
{
	$var = isset($_GET[$val]) && !empty($_GET[$val]) ? trim($_GET[$val]) : $default;
	return $var;
}

// $_REQUEST value 체크
function req_if_set($val, $default="")
{
	$var = isset($_REQUEST[$val]) && !empty($_REQUEST[$val]) ? trim($_REQUEST[$val]) : $default;
	return $var;
}

// 요청에 대한 json 리턴 형식
function res_json($code='100', $data=null)
{
	switch ($code) {
		case '100': $status = 'SUCCESS'; break;
		case '110': $status = 'PARAMETER_NOT_FOUND'; break;
		case '120': $status = 'DATA_NOT_FOUND'; break;
		case '300': $status = 'DATA_DUPLICATE'; break;
		case '400': $status = 'NOT_AUTH_LOGIN'; break;
		case '900': $status = 'ERROR'; break;
		case '910': $status = 'ERROR_DB_INSERT'; break;
		case '920': $status = 'ERROR_DB_UPDATE'; break;			
	}
	
	$response = array('code' => $code, 'status' => $status);
	if ($data) $response = array_merge($response, array('data' => $data));
	
	die( json_encode($response) );
}

// 앱 버전 정보를 조회한다
function get_app_version($os)
{
	global $g5;
	
    if ($os == "") return "";

    $sql = " SELECT version FROM tbl_app_info 
	         WHERE platform = '$os' ";
	//echo $sql . "<br>";
    $row = sql_fetch($sql);
    if (! $row['version']) return false;
    
    return $row['version'];
}

// 회원의 os별 디바이스 토큰의 존재여부를 조회한다
function exist_mb_id_token($mb_id, $os)
{
	global $g5;
	
    if ($mb_id == "" || $os == "") return "";

    $sql = " SELECT COUNT(*) AS cnt FROM {$g5['member_token_table']} 
	         WHERE mb_id = '$mb_id' AND os = '$os' ";
	//echo $sql . "<br>";
    $row = sql_fetch($sql);
    if ($row['cnt'])
        return true;
    else
        return false;
}

// 회원의 디바이스 토큰 정보를 조회한다
function get_mb_id_token($mb_id)
{
	global $g5;
	
    //if ($mb_id == "") return "";
	if ($mb_id == "") {
		$sql = " SELECT * FROM {$g5['member_token_table']} ";
	} else {
		$sql = " SELECT * FROM {$g5['member_token_table']} 
				 WHERE mb_id = '$mb_id' ";
	}
	//echo $sql . "<br>";
    $result = sql_query($sql);

	$i = 0;
	while ($row = sql_fetch_array($result)) {
        $list[$i] = $row;
		$i++;
    }

	return $list;
}

// 회원의 디바이스 토큰 정보를 신규 등록한다
function register_mb_token($token, $mb_id, $os) 
{
	global $g5;
	
	if ($token == "" || $mb_id == "" || $os == "") return "";
	
	$sql = " insert into {$g5['member_token_table']}
                set token = '{$token}',
					mb_id = '{$mb_id}',
					os    = '{$os}' ";
	//echo $sql . "<br>";					
    $result = sql_query($sql);
	
	return $result;
}

// 회원 디바이스 토큰 정보를 갱신한다
function update_mb_token($token, $mb_id, $os) 
{
	global $g5;
	
	if ($token == "" || $mb_id == "" || $os == "") return "";
	
	$sql = " update {$g5['member_token_table']} 
	         set token = '$token', updatetime = now() where mb_id = '$mb_id' AND os = '$os' ";
	//echo $sql . "<br>";
	$result = sql_query($sql);
	
	return $result;	
}

// 회원에게 push를 발송한다
function send_push($os, $title, $message, $token) 
{
	$push = new Push();
	$push->sendPush($os, $title, $message, $token);
}


?>
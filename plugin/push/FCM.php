<?php
if (!defined('_GNUBOARD_')) exit;

//include_once('./_common.php');
include_once(G5_PUSH_PATH.'/_common.php');

class Fcm
{
    private static $self;

	private $fcmKey;
    private $fcmUrl;
	
    public function __construct()
    {
        $this->fcmUrl = FCM_URL;
        $this->fcmKey = FCM_KEY;
    }

    public static function getInstance()
    {
        if (!self::$self) self::$self = new Fcm();
        return self::$self;
    }

    public function send($title, $message, $id)
    {
        $headers = array(
            'Authorization: key=' . $this->fcmKey,
            'Content-Type: application/json'
        );
        $fields = array(
            'data' => array("message" => $message,'title'	=> $title),
            'notification' => array("body" => $message,'title'	=> $title)
        );

        if (is_array($id)) {
            $fields['registration_ids'] = $id;
        } else {
            $fields['to'] = $id;
        }

        $fields['priority'] = "high";

        $fields = json_encode($fields);

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $this->fcmUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);

        $result = curl_exec($ch);

        if ($result === false) {
            sys_log('FCM send error : ', curl_error($ch));
        }

        curl_close($ch);

        sys_log('FCM send result : ', $result);

        return $result;
    }
}
?>
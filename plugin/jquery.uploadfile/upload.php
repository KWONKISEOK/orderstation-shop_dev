<?php
include_once('../../common.php');

$path = G5_DATA_PATH."/editor/playfile";
$output_dir = "uploads/";

@chmod($path, 0777);

if (isset($_FILES["myfile"])) {
    $ret = array();

    //	This is for custom errors;
    $custom_error = array();
    if (file_exists($path.'/'.$_FILES["myfile"]["name"])) {
        $custom_error['jquery-upload-file-error'] = '동일한 파일명으로 등록된 파일이 있습니다.';
        echo json_encode($custom_error);
        die();
    }

    $error = $_FILES["myfile"]["error"];
    //You need to handle  both cases
    //If Any browser does not support serializing of multiple files using FormData()
    if (!is_array($_FILES["myfile"]["name"])) //single file
    {
        $fileName = $_FILES["myfile"]["name"];
        move_uploaded_file($_FILES["myfile"]["tmp_name"], $path.'/'.$fileName);
        $ret[] = $fileName;
    } else  //Multiple files, file[]
    {
        $fileCount = count($_FILES["myfile"]["name"]);
        for ($i = 0; $i < $fileCount; $i++) {
            $fileName = $_FILES["myfile"]["name"][$i];
            move_uploaded_file($_FILES["myfile"]["tmp_name"][$i], $path.'/'.$fileName);
            $ret[] = $fileName;
        }

    }

    /************* 관리자 로그 처리 START *************/
    $al_data = $_FILES["myfile"];
    insert_admin_log(600,600690, '동영상 등록', '', '', '', $_SERVER['REQUEST_URI'], $al_data);
    /************* 관리자 로그 처리 END *************/

    echo json_encode($ret);
}
?>
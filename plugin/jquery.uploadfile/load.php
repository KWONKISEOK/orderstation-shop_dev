<?php
include_once('../../common.php');

$dir = G5_DATA_PATH."/editor/playfile";
$src = G5_DATA_URL."/editor/playfile";
$files = scandir($dir);

$ret = array();
foreach ($files as $file) {
    if ($file == "." || $file == "..")
        continue;
    $filePath = $dir."/".$file;
    $details = array();
    $details['name'] = $file;
    $details['path'] = $src.'/'.$file;
    $details['size'] = filesize($filePath);
    $details['time'] = filemtime($filePath);
    $ret[] = $details;

}
foreach ((array) $ret as $key => $value) {
    $sort[$key] = $value['time'];
}

array_multisort($sort, SORT_ASC, $ret);
//$ret = array_keys($ret);

//debug2($ret);
echo json_encode($ret);


?>



<?php
include_once('../../common.php');

$output_dir = G5_DATA_PATH."/editor/playfile/";
if(isset($_POST["op"]) && $_POST["op"] == "delete" && isset($_POST['name']))
{
    $fileName =$_POST['name'];
    $fileName=str_replace("..",".",$fileName); //required. if somebody is trying parent folder files
    $filePath = $output_dir. $fileName;
    if (file_exists($filePath))
    {
        unlink($filePath);
    }

    // 관리자 로그

    /************* 관리자 로그 처리 START *************/
    $al_data = array(
        'filePath' => $filePath
    );
    insert_admin_log(600,600690, '동영상 삭제', '', '', '', $_SERVER['REQUEST_URI'], $al_data);
    /************* 관리자 로그 처리 END *************/
    
    echo "Deleted File ".$fileName."<br>";
}

?>
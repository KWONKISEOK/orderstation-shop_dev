<?php
include_once('./_common.php');
include_once(G5_CAPTCHA_PATH.'/captcha.lib.php');
include_once(G5_LIB_PATH.'/register.lib.php');

// 불법접근을 막도록 토큰생성
$token = md5(uniqid(rand(), true));
set_session("ss_token", $token);
set_session("ss_cert_no",   "");
set_session("ss_cert_hash", "");
set_session("ss_cert_type", "");
$social_regist_id = "";

if( $provider && function_exists('social_nonce_is_valid') ){   //모바일로 소셜 연결을 했다면
    if( social_nonce_is_valid(get_session("social_link_token"), $provider) ){  //토큰값이 유효한지 체크
        $w = 'u';   //회원 수정으로 처리
        $_POST['mb_id'] = $member['mb_id'];
    }
}


//소셜 로그인 정보
//if( $_SERVER['REMOTE_ADDR'] == "121.162.38.65" || $_SERVER['REMOTE_ADDR'] == "223.131.133.239" || $_SERVER['REMOTE_ADDR'] == "59.6.135.109" ){

	$provider_name = social_get_request_provider();
	$user_profile = social_session_exists_check();


	// 소셜 가입된 내역이 있는지 확인 상수 G5_SOCIAL_DELETE_DAY 관련
	$is_exists_social_account = social_before_join_check($url);

	$user_nick = social_relace_nick($user_profile->displayName);
	$user_email = isset($user_profile->emailVerified) ? $user_profile->emailVerified : $user_profile->email;
	$user_id = $user_profile->sid ? preg_replace("/[^0-9a-z_]+/i", "", $user_profile->sid) : get_social_convert_id($user_profile->identifier, $provider_name);
	$user_identifier = $user_profile->identifier;
	$user_gender =  $user_profile->gender;

	//$is_exists_id = exist_mb_id($user_id);
	//$is_exists_name = exist_mb_nick($user_nick, '');
	$user_id = exist_mb_id_recursive($user_id);
	$user_nick = exist_mb_nick_recursive($user_nick, '');
	$is_exists_email = $user_email ? exist_mb_email($user_email, '') : false;

	//echo $url . "/" . $user_email . "/" . $user_profile->identifier;

//}

if ($w == "") {

    // 회원 로그인을 한 경우 회원가입 할 수 없다
    // 경고창이 뜨는것을 막기위해 아래의 코드로 대체
    // alert("이미 로그인중이므로 회원 가입 하실 수 없습니다.", "./");
    if ($is_member) {
        goto_url(G5_URL);
    }

    // 리퍼러 체크
    referer_check();

    if (!isset($_POST['agree']) || !$_POST['agree']) {
        alert('회원가입약관의 내용에 동의하셔야 회원가입 하실 수 있습니다.', G5_BBS_URL.'/register.php');
    }

    if (!isset($_POST['agree2']) || !$_POST['agree2']) {
        alert('개인정보처리방침안내의 내용에 동의하셔야 회원가입 하실 수 있습니다.', G5_BBS_URL.'/register.php');
    }

    $agree  = preg_replace('#[^0-9]#', '', $_POST['agree']);
    $agree2 = preg_replace('#[^0-9]#', '', $_POST['agree2']);

    $member['mb_birth'] = '';
    $member['mb_sex']   = '';
    $member['mb_name']  = '';
    if (isset($_POST['birth'])) {
        $member['mb_birth'] = $_POST['birth'];
    }
    if (isset($_POST['sex'])) {
        $member['mb_sex']   = $_POST['sex'];
    }
    if (isset($_POST['mb_name'])) {
        $member['mb_name']  = $_POST['mb_name'];
    }

    $g5['title'] = '회원 가입';

} else if ($w == 'u') {

    if ($is_admin == 'super')
        alert('관리자의 회원정보는 관리자 화면에서 수정해 주십시오.', G5_URL);

    if (!$is_member)
        alert('로그인 후 이용하여 주십시오.', G5_URL);

    if ($member['mb_id'] != $_POST['mb_id'])
        alert('로그인된 회원과 넘어온 정보가 서로 다릅니다.');

    /*
    if (!($member[mb_password] == sql_password($_POST[mb_password]) && $_POST[mb_password]))
        alert("비밀번호가 틀립니다.");

    // 수정 후 다시 이 폼으로 돌아오기 위해 임시로 저장해 놓음
    set_session("ss_tmp_password", $_POST[mb_password]);
    */

    if ($_POST['mb_password']) {
        // 수정된 정보를 업데이트후 되돌아 온것이라면 비밀번호가 암호화 된채로 넘어온것임
        if ($_POST['is_update'])
            $tmp_password = $_POST['mb_password'];
        else
            $tmp_password = get_encrypt_string($_POST['mb_password']);

        if ($member['mb_password'] != $tmp_password)
            alert('비밀번호가 틀립니다.');
    }

    $g5['title'] = '회원 정보 수정';

    set_session("ss_reg_mb_name", $member['mb_name']);
    set_session("ss_reg_mb_hp", $member['mb_hp']);

    $member['mb_email']       = get_text($member['mb_email']);
    $member['mb_homepage']    = get_text($member['mb_homepage']);
    $member['mb_birth']       = get_text($member['mb_birth']);
    $member['mb_tel']         = get_text($member['mb_tel']);
    $member['mb_hp']          = get_text($member['mb_hp']);
    $member['mb_addr1']       = get_text($member['mb_addr1']);
    $member['mb_addr2']       = get_text($member['mb_addr2']);
    $member['mb_signature']   = get_text($member['mb_signature']);
    $member['mb_recommend']   = get_text($member['mb_recommend']);
    $member['mb_profile']     = get_text($member['mb_profile']);


	//if( $_SERVER['REMOTE_ADDR'] == "121.162.38.65" || $_SERVER['REMOTE_ADDR'] == "223.131.133.239" || $_SERVER['REMOTE_ADDR'] == "59.6.135.109" ){

		//소셜 가입된 아이디인지 체크
		$sql_social = " 
			SELECT 
			count(mb_id) as m_cnt ,
			(	
				SELECT COUNT(mb_id) AS cnt FROM tbl_member_social_profiles WHERE mb_id = '".$member['mb_id']."' 
			) AS s_cnt
			from tbl_member 
			where mb_id = '".$member['mb_id']."'
		";	
		$row_social = sql_fetch($sql_social);
		if ($row_social['m_cnt'] > 0 && $row_social['s_cnt'] > 0 ){
			$social_regist_id = "true";
		}

	//}

} else {
    alert('w 값이 제대로 넘어오지 않았습니다.');
}

include_once('./_head.php');


if(!G5_IS_MOBILE) {
echo '<div class="site-wrap">';
echo '<div id="container" >';
}
echo '<div id="wrapper_title"></div>';





// 회원아이콘 경로
$mb_icon_path = G5_DATA_PATH.'/member/'.substr($member['mb_id'],0,2).'/'.$member['mb_id'].'.gif';
$mb_icon_url  = G5_DATA_URL.'/member/'.substr($member['mb_id'],0,2).'/'.$member['mb_id'].'.gif';

// 회원이미지 경로
$mb_img_path = G5_DATA_PATH.'/member_image/'.substr($member['mb_id'],0,2).'/'.$member['mb_id'].'.gif';
$mb_img_url  = G5_DATA_URL.'/member_image/'.substr($member['mb_id'],0,2).'/'.$member['mb_id'].'.gif';

$register_action_url = G5_HTTPS_BBS_URL.'/register_form_update.php';
$req_nick = !isset($member['mb_nick_date']) || (isset($member['mb_nick_date']) && $member['mb_nick_date'] <= date("Y-m-d", G5_SERVER_TIME - ($config['cf_nick_modify'] * 86400)));
$required = ($w=='') ? 'required' : '';
$readonly = ($w=='u') ? 'readonly' : '';

$agree  = preg_replace('#[^0-9]#', '', $agree);
$agree2 = preg_replace('#[^0-9]#', '', $agree2);

// add_javascript('js 구문', 출력순서); 숫자가 작을 수록 먼저 출력됨
if ($config['cf_use_addr'])
    add_javascript(G5_POSTCODE_JS, 0);    //다음 주소 js

//소셜로그인 일때 스킨
if( ( !empty($user_identifier) || $social_regist_id == "true" ) ){
	include_once($member_skin_path.'/social_register_form.skin.php');
}else{
	include_once($member_skin_path.'/register_form.skin.php');
}
include_once('./_tail.php');
?>

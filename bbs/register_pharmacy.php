<?php
include_once('./_common.php');


//$g5['title'] = '고객리스트';
//include_once(G5_PATH.'/head.sub.php');


$sido	= $_POST['sido'];
$gugun	= $_POST['gugun'];

$dong	= $_POST['dong'];
$txt_word	= $_POST['txt_word'];




?>
<style>
.pharmacy_list {
	font-size:12px;
	}

/* 정보입력 */
.boardList { color:#1b1b1b; margin:0 0 7px 0; }
.boardList table { font-size:13px; word-wrap:break-word; border:1px solid #ccc; }
.boardList table tr:first-child th,
.boardList table tr:first-child td { border-top:none; }
.boardList table th { width:90px; padding:7px 0 7px 10px; border-top:1px solid #ececec; color:#757575; font-size:13px; font-weight:normal; text-align:left; vertical-align:middle; background:#fff; }
.boardList table th > img { vertical-align:top; padding:4px 0 0; }
.boardList table td { padding:7px 10px; border-top:1px solid #ececec; vertical-align:middle; background:#fff; }
.boardList table .idCheck { font-size:11px; }
.boardList table .idCheck th { vertical-align:top; padding-top:13px; }
.boardList table .idCheck .error { color:#f00; }
.boardList table td.thead { border-left:1px solid #ccc; }
.boardList table td.thead label:last-child { margin-right:0; }
.boardList table td #foreignerAuth label:last-child { margin-right:0; }
.boardList table td.thead label input { vertical-align:middle; }
.boardList table td.thead p { height:30px; padding:6px 0 0; color:#757575; font-size:13px; }
.boardList table td label { margin:0 14px 0 0; }
.boardList .interest label { display:inline-block; min-width:90px; margin:0 7px 14px 0; }
.boardList .interest { font-size:11px; line-height:1.2em; }
.boardList .interest input { margin:0 3px 0 0; }
.boardList .interest span { display:inline-block; }
.boardList select { margin-right:4px; }

select {
    max-width: 100%;
    height: 29px;
    margin: 0;
    padding: 0 35px 0 3px;
    color: #353535;
    word-break: break-all;
    font-weight: inherit;
    border: 1px solid #ccc;
    background: #fff url(http://img.echosting.cafe24.com/skin/mobile_ko_KR/layout/bg_selectbox.gif) no-repeat right center;
    background-size: 32px 27px;
    -webkit-border-radius: 3px;
    -webkit-appearance: none;
}
input[type=text], input[type=password] {
    height: 28px;
    line-height: 28px;
    padding: 2px 4px;
    border: 1px solid #d5d5d5;
    color: #353535;
    font-size: 12px;
}
/*
input[type='text'] input[type='password'] {
    margin: 3px 0px;
    padding: 2px;
    outline: 0px;
    border: 1px solid rgb(147, 149, 152);
    border-image: none;
    height: 24px;
   
    line-height: 15px;
    font-size: 13px;
  
}
*/

.btn_wt_s {
    vertical-align: middle;
    padding: 0 7px;
    border: 1px solid #787878;
    color: #333;
    line-height: 28px;
    background: #ffffff;
    background: -moz-linear-gradient(top, #ffffff 0%, #f3f3f3 100%);
    background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#ffffff), color-stop(100%,#f3f3f3));
    background: -webkit-linear-gradient(top, #ffffff 0%,#f3f3f3 100%);
    background: -o-linear-gradient(top, #ffffff 0%,#f3f3f3 100%);
    background: -ms-linear-gradient(top, #ffffff 0%,#f3f3f3 100%);
    background: linear-gradient(to bottom, #ffffff 0%,#f3f3f3 100%);
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#f3f3f3',GradientType=0 );
}
.btn-base {
    display: inline-block;
    border-radius: 3px;
    /*line-height: 300%;*/
    font-size: 12px;
}

</style>
<script src="/js/jquery-1.8.3.min.js"></script>
<form id="frmSearch" name="frmSearch" method="post" onsubmit="fnSubmit(); return false;">
<input type="hidden" name="act" value="auto_mod">
<input type="hidden" name="ct_id" value="<?php echo $row['ct_id']; ?>">

<input type="hidden" name="sw_direct">

<section style="padding:10px;">


<div class="boardList">
			
		<select name="sido" id="sido" onChange="onChangeAddr(1)"  >
			<option value="">시/도</option>
				<option value="강원도" <?php if($sido=='강원도') echo 'selected';?> >강원도</option>
				<option value="경기도" <?php if($sido=='경기도') echo 'selected';?> >경기도</option>						
				<option value="경상남도" <?php if($sido=='경상남도') echo 'selected';?> >경상남도</option>						
				<option value="경상북도" <?php if($sido=='경상북도') echo 'selected';?> >경상북도</option>
				<option value="광주광역시" <?php if($sido=='광주광역시') echo 'selected';?> >광주광역시</option>
				<option value="대구광역시" <?php if($sido=='대구광역시') echo 'selected';?> >대구광역시</option>
				<option value="대전광역시" <?php if($sido=='대전광역시') echo 'selected';?> >대전광역시</option>
				<option value="부산광역시" <?php if($sido=='부산광역시') echo 'selected';?> >부산광역시</option>
				<option value="서울특별시" <?php if($sido=='서울특별시') echo 'selected';?> >서울특별시</option>
				<option value="세종특별자치시" <?php if($sido=='세종특별자치시') echo 'selected';?> >세종특별자치시</option>
				<option value="울산광역시" <?php if($sido=='울산광역시') echo 'selected';?> >울산광역시</option>
				<option value="인천광역시" <?php if($sido=='인천광역시') echo 'selected';?> >인천광역시</option>
				<option value="전라남도" <?php if($sido=='전라남도') echo 'selected';?> >전라남도</option>
				<option value="전라북도" <?php if($sido=='전라북도') echo 'selected';?> >전라북도</option>
				<option value="제주특별자치도" <?php if($sido=='제주특별자치도') echo 'selected';?> >제주특별자치도</option>
				<option value="충청남도" <?php if($sido=='충청남도') echo 'selected';?> >충청남도</option>
				<option value="충청북도" <?php if($sido=='충청북도') echo 'selected';?> >충청북도</option>
		</select>
		
		<select name="gugun" id="gugun" onChange="onChangeAddr(2)" >
		  <option value="">시/군/구</option>
		</select>
		
		<!--
	  <select type="hidden" name="dong" id="dong"  >
		  <option value="">동/면/읍</option>
		</select> -->
		
		<input type="text" name="txt_word" class="inputTypeText" value="<?php echo $txt_word;?>" style="width:70px;" />
	 <button type="submit" class="btn-base btn_wt_s"  id="basic-addon21" >검색</button> 
				
			
		<div id="wrap_list" style="height:150px;">
										
		</div>

		
</div>





</section>


<div class="btn_confirm">
    <!-- <input type="submit" value="정기주문신청" class="btn_submit"> -->
    <!-- <button type="button" id="mod_option_close" class="btn_close"><i class="fa fa-times" aria-hidden="true"></i><span class="sound_only">닫기</span></button> -->
</div>
</form>
<script>
	function rowClick(mb_id, mb_name) {
		
		$("#mb_recommend" , parent.fregisterform).val(mb_id);


		//parent.fregisterform.mb_recommend.value=mb_id;
		//parent.forderform.mb_recommend_name.value=mb_name;		
		$("#mb_recommend_name",parent.document).text(mb_name);

			
		//parent.closeLayer();
	}

	onChangeAddr(1,'');
	setTimeout(function(){ onChangeAddr(2,'');}, 100); 
	function onChangeAddr(step, val){
		var sido = $("#sido");
		var gugun = $("#gugun");
		var dong = $("#dong");
		var obj = "";

		if(step == 1){
			obj = gugun;
			gugun.empty().data('option');
			gugun.append("<option value=''>시/군/구</option>");
		}
		if(step == 2){
			obj = dong; 
		}
		dong.empty().data('option');
		dong.append("<option value=''>동/면/읍</option>");
		$.ajax({
			type:"post",
			dataType:"xml",
			url: "/addr.php",
			data:{"mem_sido":sido.val(),"mem_gugun":gugun.val(), "step":step},
			success:function(data){
				var row= $(data).find("ROW").find("CELL");    
				var size=row.length;
				for(var i=0;i<size;i++){
					var cell =row.eq(i);
					$("<option></option>").text(cell.find("NAME").text()).attr("value",cell.find("NAME").text()).appendTo(obj);
				}

				if(step==1){
					gugun.val(val);
				}
				if(step==2){
					dong.val(val);
				}
			},
			error:function(request, status, errorThrown){
				//alert(errorThrown);
			}
		});
	}



	function fnSearch(type) {
		
		var $data_list;
		$data_list = $("#wrap_list");

		//$("#preloader").show();
		$data_list.html("") ;
		$.ajax({
			type: "post",
			url: "register_pharmacy_01.php",
			data:$('#frmSearch').serialize(),
			dataType: "text",
			cache: false,
			success: function(resultText) {
				$data_list.html(resultText);
			
				//$("#preloader").hide();
			}
		});
	}
	function fnSubmit() {
		fnSearch(0);
	}

	fnSearch(0);
</script>
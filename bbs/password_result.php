<?php
include_once('./_common.php');
//include_once(G5_CAPTCHA_PATH.'/captcha.lib.php');

if ($is_member) {
    alert("이미 로그인중입니다.");
}

$g5['title'] = '회원정보 찾기 결과';
include_once(G5_PATH.'/head.sub.php');

include_once($member_skin_path.'/password_result.skin.php');

include_once(G5_PATH.'/tail.sub.php');
?>
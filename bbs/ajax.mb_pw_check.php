<?php
include_once('./_common.php');

$mode = $_REQUEST['w'];
$mb_id = $_REQUEST['mb_id'];
$mb_password = $_REQUEST['mb_password'];

if ($mode=='u') {

    if ($mb_password) {
        // 수정된 정보를 업데이트후 되돌아 온것이라면 비밀번호가 암호화 된채로 넘어온것임
        $tmp_password = get_encrypt_string($mb_password);

        if ($member['mb_password'] != $tmp_password) {
            $msg = '비밀번호가 틀립니다.';
            $response=array('type' => 'error','msg' => $msg);
        } else {
            $response=array('type' => 'ok');
        }

    } else {
        $msg = '비밀번호가 입력되지 않았습니다.';
        $response=array('type' => 'error','msg' => $msg);
    }
} else {
    $msg = '잘못된 접근입니다.';
    $response=array('type' => 'error','msg' => $msg);
}

echo json_encode($response);

?>
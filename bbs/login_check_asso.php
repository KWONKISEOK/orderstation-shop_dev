<?php
include_once('./_common.php');

$g5['title'] = "로그인 검사";

$mb_id       = trim($_REQUEST['mb_id']);
//$mb_password = trim($_POST['mb_password']);

$mb = get_member($mb_id);

if (!$mb['mb_id'] ) {
    alert('가입된 회원아이디가 아닙니다.');
}


// 회원아이디 세션 생성
set_session('ss_mb_id', $mb['mb_id']);
// FLASH XSS 공격에 대응하기 위하여 회원의 고유키를 생성해 놓는다. 관리자에서 검사함 - 110106
set_session('ss_mb_key', md5($mb['mb_datetime'] . get_real_client_ip() . $_SERVER['HTTP_USER_AGENT']));


goto_url('/shop/?device=mobile');
?>

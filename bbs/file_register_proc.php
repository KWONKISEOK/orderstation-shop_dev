<?
include_once('./_common.php');

@chmod(G5_DATA_PATH."/regfile", G5_DIR_PERMISSION);


/*************** 트랜잭션 관련 ****************/
$error_cnt = 0;
$ErrorMsg = "";
mysqli_autocommit($g5['connect_db'], false);
/*************** 트랜잭션 관련 ****************/

$mb_password = trim($_POST['mb_password']);

$total = count($_FILES['reg_file']['name']);

//파일명 유효성 검사
for( $i=0 ; $i < $total ; $i++ ) {

	  $tmpFilePath = $_FILES['reg_file']['tmp_name'][$i];
	  $filelength = mb_strlen( $_FILES['reg_file']['name'][$i] , 'utf-8' );

	  if ($tmpFilePath != ""){

			$newFilePath = G5_DATA_PATH."/regfile/" . $_FILES['reg_file']['name'][$i];
			$newFilePath_re = $_FILES['reg_file']['name'][$i]; // 이게 main_tab_5.png 이름만
			$f_path=G5_DATA_PATH."/regfile/" . $_FILES['reg_file']['name'][$i];
			
			$strTok = explode('_' , $newFilePath_re);
			
			$cnt = count($strTok);
			
			if($cnt!=4)
			{
				echo "<script>	
						alert('파일 형식이 잘못되었습니다. 다시 확인해주세요.');
						history.back();
					  </script>";
				exit;
			}
/*
			for($i = 0 ; $i < $cnt ; $i++){

				echo($strTok[$i] . "<br/>");

			}
			$i=0;*/

			if( pathinfo( $_FILES['reg_file']['name'][$i], PATHINFO_EXTENSION) != "pdf" ){
				echo "<script>
						alert('pdf 파일만 업로드 가능합니다. ');
						history.back();
					  </script>";
				exit;	
			}

			//확장자 pdf 포함23자리
			//if( $filelength != 23 ){
			//	echo "<script>
			//		    alert('{$filelength}');
			//			alert('파일명의 자리수는 19자리(GS-0000000홍길동YYMMDD)이어야 합니다.');
			//			history.back();
			//		  </script>";
			//	exit;			
			//}

			if(!move_uploaded_file($tmpFilePath,$newFilePath)) {
				echo "<script>
						alert('업로드된 파일을 옮기는 중 에러가 발생했습니다.');
						history.back();
					  </script>";
				exit;
			}

			$sql = " select count(file_idx) as cnt from tbl_regfile where file_name = '{$_FILES['reg_file']['name'][$i]}'; ";
			$row = sql_fetch($sql);
			$total_count = $row['cnt'];
			
			$birth_=explode('.' , $strTok[3]);
			
			$cnt = count($birth_);
			$first_str = substr($strTok[0], 0, 2);
/*
			for($i = 0 ; $i < $cnt ; $i++){
				echo($birth_[$i] . "<br/>");

			}
			
			$i=0;*/
			

			//업로드파일이 없을때 실행
			if( $total_count == 0 ){

				$sql = " insert into tbl_regfile( file_name,code,name,phone,birth,file_root) values('{$_FILES['reg_file']['name'][$i]}','$strTok[0]','$strTok[1]','$strTok[2]','$birth_[0]','$f_path'); ";
				sql_query($sql);
				/*************** 트랜잭션 관련 ****************/
				if( mysqli_errno($g5['connect_db']) ){
					$error_cnt += 1;
				}
				/*************** 트랜잭션 관련 ****************/
				
				/*if($error_cnt==0 && $first_str=='GS'){
					
					include_once(G5_LIB_PATH.'/kakao.alimtalk.lib.php');

					$alimtalk = new AlimTalk();

					// 국가번호 포함한 전화번호 (수신번호)
					$to = $alimtalk->set_phone_number($strTok[2]);

					// 발송할 메시지에 해당하는 템플릿 코드
					$template_code = 'clinomics_01';  // 회원가입축하메시지

					// 발송할 메시지에 해당하는 템플릿 내용
					// 안녕하세요 #{회원이름} 고객님, 오더스테이션 가입을 환영합니다! 오더스테이션만의 건강한 먹거리와, 다양한 건강정보를 약사님과의 1:1 상담과 함께 누려보세요!
					$text = $alimtalk->get_template($template_code);
					$text = str_replace('#{이름}', $strTok[1], $text);
					$text = str_replace('#{결과지확인URL경로}', 'https://www.orderstation.co.kr/shop/regfile_search.php', $text);

					// 알림톡 메시지 설정
					$alimtalk->set_message($template_code, $to, $text);

					// 알림톡 발송
					$alimtalk->send();
					
				}else if($error_cnt==0 && $first_str=='GB'){
					
					include_once(G5_LIB_PATH.'/kakao.alimtalk.lib.php');

					$alimtalk = new AlimTalk();

					// 국가번호 포함한 전화번호 (수신번호)
					$to = $alimtalk->set_phone_number($strTok[2]);

					// 발송할 메시지에 해당하는 템플릿 코드
					$template_code = 'clinomics_02';  // 회원가입축하메시지

					// 발송할 메시지에 해당하는 템플릿 내용
					// 안녕하세요 #{회원이름} 고객님, 오더스테이션 가입을 환영합니다! 오더스테이션만의 건강한 먹거리와, 다양한 건강정보를 약사님과의 1:1 상담과 함께 누려보세요!
					$text = $alimtalk->get_template($template_code);
					$text = str_replace('#{이름}', $strTok[1], $text);
					$text = str_replace('#{결과지확인URL경로}', 'https://www.orderstation.co.kr/shop/regfile_search.php', $text);

					// 알림톡 메시지 설정
					$alimtalk->set_message($template_code, $to, $text);

					// 알림톡 발송
					$alimtalk->send();
					
				}*/

			}

	  }

}

/*************** 트랜잭션 관련 ****************/
if ($error_cnt > 0) {
	mysqli_rollback($g5['connect_db']);
	mysqli_close($g5['connect_db']);
	echo "
	<script>
		alert('데이터베이스의 에러로 인해 롤백되었습니다.');
		location.href='/shop/file_reg.php';
	</script>
	";
	exit;
} else {
	mysqli_commit($g5['connect_db']);
} 	
/*************** 트랜잭션 관련 ****************/

?>
<form name="frm" method="post">
	<input type="hidden" name="mb_password" value="<?=$mb_password?>"/>
</form>
<script>
	alert("검사지가 등록되었습니다.");
	document.frm.action = "/shop/file_reg.php";
	document.frm.submit();
</script>
<?php
include_once('./_common.php');
//include_once(G5_CAPTCHA_PATH.'/captcha.lib.php');

$g5['title'] = '회원정보 찾기';
include_once(G5_PATH.'/head.sub.php');


if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가

// add_stylesheet('css 구문', 출력순서); 숫자가 작을 수록 먼저 출력됨
add_stylesheet('<link rel="stylesheet" href="'.$member_skin_url.'/style.css">', 0);

$mb_kind = $_POST['mb_kind'];
$mb_id = $_POST['mb_id'];

$mb_hp = $_POST['mb_hp1'].$_POST['mb_hp2'].$_POST['mb_hp3'];

?>
<style
<style>
input[type=text], input[type=password] {
    height: 25px;
    line-height: 28px;
    padding: 2px 4px;
	width:100px;
    border: 1px solid #d5d5d5;
    color: #353535;
    font-size: 12px;
}
</style>

<!-- 회원정보 찾기 시작 { -->
<div id="find_info" class="new_win">
    <h1 id="win_title">회원정보 찾기</h1>
    <div class="new_win_con">
        <form name="fpasswordlost" action="password_update.php" onsubmit="return form_submit(this);"  method="post" autocomplete="off">
		<input type="hidden" name="mb_id" value="<?php echo $mb_id;?>" />

			<?php if($mb_kind == 'id') {
			// ID찾기
			$sql =  "select mb_id from tbl_member where mb_name = '$mb_id' and replace(mb_hp,'-','')= '$mb_hp' limit 1";

			$row = sql_fetch($sql);
			$mb_id_find = $row['mb_id'] ;
			
			
			?>
				
			<h3> <?php echo $mb_id;?>님의 ID는 <?php echo $mb_id_find; ?> 입니다.</h3>

			<?php } else { 
			
			
			
			?>

			<h3><?php echo $mb_id;?> 님의 비밀번호를 변경하세요.</h3>

			<table>
			<tr>
				<th scope="row">비밀번호</th>
				<td>
					<input type="password" name="mb_passwd1" id="mb_passwd1" maxlength="20" class="inputTypeText" />
					비밀번호는 6~20자의 최소 1개의 숫자, 영문자를 포함.
				</td>
			</tr>
			<tr>
				<th scope="row">비밀번호 확인</th>
				<td>
					<input type="password" name="mb_passwd2" id="mb_passwd2" maxlength="20" class="inputTypeText" />
				</td>
			</tr>
			</table>
			

		<input type="submit" value="비밀번호변경" class="btn_submit"> 


			<?php }  ?>
		
				

	
    <button type="button" onclick="window.close();" class="btn_close">창닫기</button>

    </form>

</div>

<script>

// submit 최종 폼체크
function form_submit(f)
{
		
	if(chkSpace(f.mb_passwd1.value)){
		alert("\n비밀번호를 입력해 주세요..");
		f.mb_passwd1.focus();
		return false;
	}
	if(chkSpace(f.mb_passwd2.value)){
		alert("\n비밀번호를 입력해 주세요..");
		f.mb_passwd2.focus();
		return false;
	}
	if(mb_passwd_check()==false) {
		f.mb_passwd1.focus();
		return false;
	}
	return true;
}
// 입력값이 NULL 인지 체크
function chkSpace(strValue){
	var flag=true;
	if (strValue!=""){
		for (var i=0; i < strValue.length; i++){
			if (strValue.charAt(i) != " "){
				flag=false;
				break;
			}
		}
	}
	return flag;
}
// 비밀번호
function mb_passwd_check()
{
	//valid check
	if ( document.fpasswordlost.mb_passwd1.value.length <= 0 )
	{
		alert("\n비밀번호를 입력하세요.");
		return false;
	}
	
	var isID = /^(?=.*[a-zA-Z])(?=.*\d).{6,20}$/;
	if( !isID.test(document.fpasswordlost.mb_passwd1.value) ) {
		alert("비밀번호는 6~20자의 최소 1개의 숫자, 영문자 를 포함해야 합니다."); 
		//str.focus();
		return false; 
	}
	
	if ( document.fpasswordlost.mb_passwd2.value.length <= 0 )
	{
		alert("\n비밀번호를 한번 더 입력하세요.");
		document.fpasswordlost.mb_passwd2.value="";
		return false;
	}

		if( document.fpasswordlost.mb_passwd1.value != document.fpasswordlost.mb_passwd2.value)
	{
		alert("\n비밀번호를 정확히 입력하세요.");
		document.fpasswordlost.mb_passwd1.value="";
		document.fpasswordlost.mb_passwd2.value="";
		return false;
	}

	//if ( _valid_length_check(2) ) return false;
	return true;
}

</script>

<?php







include_once(G5_PATH.'/tail.sub.php');
?>
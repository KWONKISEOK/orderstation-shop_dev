<?php 
include_once('./_common.php');

$mode	= $_REQUEST['mode'];

//인증번호 확인모드가 아닌데 핸드폰번호가 없을때 에러처리.
if ($mode !='check' && trim($_REQUEST['mb_hp']) == "" ) {
	$response = array('type' => 'error','msg' => '전화번호를 입력해주세요.');
	echo json_encode($response);
	exit;
}

if ($mode=='send-register') {
	$hp	= $_REQUEST['mb_hp'];

	// 세션에 저장된 토큰과 폼값으로 넘어온 토큰을 비교 
	if ($_REQUEST["token"] && get_session("ss_token") == $_REQUEST["token"]) { 
		set_session("ss_token", ""); // 맞으면 세션을 지워 다시 입력폼을 통해서 들어오도록 한다. 

		$msg = "";
	} else { 
		//alert_close("인증번호 발송시 오류가 발생하였습니다."); 
		//exit; 

		$msg = "인증번호 발송시 오류가 발생하였습니다.";
	} 
	
	//휴대폰번호 중복체크
	//실제 존재하는 체크
	/*
	$sql =  "select count(*) cnt from tbl_member where replace(mb_hp,'-','') = '$hp' limit 1";
	$row = sql_fetch($sql);
	$mb_id_count = $row['cnt'] ;
	if($mb_id_count > 0) {
		$msg = "이미가입된 전화번호 입니다.";
	}
	*/
	

	if($msg=='') {	
		$certify_number = rand(100000, 999999); 

		// 생성된 인증번호를 세션에 저장함 
		// form 에서 넘어온 인증번호와 비교하여 같으면 글쓰기 허용함 
		set_session("ss_hp_certify_number", $certify_number); 


		// AlimTalk BEGIN --------------------------------------------------------
		include_once(G5_LIB_PATH.'/kakao.alimtalk.lib.php');

		$alimtalk = new AlimTalk();

		// 국가번호 포함한 전화번호 (수신번호)
		$to = $alimtalk->set_phone_number($hp);

		// 발송할 메시지에 해당하는 템플릿 코드
		$template_code = 'os_member_002';  // 가입_휴대폰인증번호

		// 발송할 메시지에 해당하는 템플릿 내용
		// [오더스테이션] 휴대폰 인증번호 #{인증번호} 위 번호를 인증창에 입력하세요.
		$text = $alimtalk->get_template($template_code);
		$text = str_replace('#{인증번호}', $certify_number, $text);

		// 알림톡 메시지 설정
		$alimtalk->set_message($template_code, $to, $text);

		// 알림톡 발송
		$is_alimtalk_success = $alimtalk->send();
		// AlimTalk END   --------------------------------------------------------
	}
	if ($msg == '' && $is_alimtalk_success) {
		//$response = array('type' => 'ok','certify' => $certify_number);
		$response = array('type' => 'ok','msg' => '인증번호가 전송되었습니다.');
	} else {
		$response = array('type' => 'error','msg' => $msg);
	}
}
if ($mode=='send') {
	$hp	= $_REQUEST['mb_hp'];

	// 세션에 저장된 토큰과 폼값으로 넘어온 토큰을 비교 
	if ($_REQUEST["token"] && get_session("ss_token") == $_REQUEST["token"]) { 
		set_session("ss_token", ""); // 맞으면 세션을 지워 다시 입력폼을 통해서 들어오도록 한다. 

		$msg = "";
	} else { 
		//alert_close("인증번호 발송시 오류가 발생하였습니다."); 
		//exit; 

		$msg = "인증번호 발송시 오류가 발생하였습니다.";
	} 

	$certify_number = rand(100000, 999999); 

	// 생성된 인증번호를 세션에 저장함 
	// form 에서 넘어온 인증번호와 비교하여 같으면 글쓰기 허용함 
	set_session("ss_hp_certify_number", $certify_number); 


    // AlimTalk BEGIN --------------------------------------------------------
    include_once(G5_LIB_PATH.'/kakao.alimtalk.lib.php');

    $alimtalk = new AlimTalk();

    // 국가번호 포함한 전화번호 (수신번호)
    $to = $alimtalk->set_phone_number($hp);

    // 발송할 메시지에 해당하는 템플릿 코드
    $template_code = 'os_member_002';  // 가입_휴대폰인증번호

    // 발송할 메시지에 해당하는 템플릿 내용
    // [오더스테이션] 휴대폰 인증번호 #{인증번호} 위 번호를 인증창에 입력하세요.
    $text = $alimtalk->get_template($template_code);
    $text = str_replace('#{인증번호}', $certify_number, $text);

    // 알림톡 메시지 설정
    $alimtalk->set_message($template_code, $to, $text);

    // 알림톡 발송
    $is_alimtalk_success = $alimtalk->send();
    // AlimTalk END   --------------------------------------------------------

	if ($msg == '' && $is_alimtalk_success) {
		//$response = array('type' => 'ok','certify' => $certify_number);
		$response = array('type' => 'ok','msg' => '인증번호가 전송되었습니다.');
	} else {
		$response = array('type' => 'error','msg' => $msg);
	}
}
if ($mode=='search-send') {
	$mb_kind = $_POST['mb_kind'];
	$mb_id = $_POST['mb_id'];

	$hp	= $_REQUEST['mb_hp'];
	

	//실제 존재하는 체크
	if($mb_kind =='id') {
		$sql =  "select count(*) cnt from tbl_member where mb_name = '$mb_id' and replace(mb_hp,'-','')= '$hp' limit 1";
	} else {
		$sql =  "select  count(*) cnt from tbl_member where mb_id = '$mb_id' and replace(mb_hp,'-','')= '$hp' limit 1";
	}

	$row = sql_fetch($sql);
	$mb_id_count = $row['cnt'] ;

	if($mb_id_count > 0) {

		/*
		// 세션에 저장된 토큰과 폼값으로 넘어온 토큰을 비교 
		if ($_REQUEST["token"] && get_session("ss_token") == $_REQUEST["token"]) { 
			set_session("ss_token", ""); // 맞으면 세션을 지워 다시 입력폼을 통해서 들어오도록 한다. 

			$msg = "";
		} else { 
			//alert_close("인증번호 발송시 오류가 발생하였습니다."); 
			//exit; 

			$msg = "인증번호 발송시 오류가 발생하였습니다.";
		} 
		*/
		$msg = "";
			
		$certify_number = rand(100000, 999999); 

		// 생성된 인증번호를 세션에 저장함 
		// form 에서 넘어온 인증번호와 비교하여 같으면 글쓰기 허용함 
		set_session("ss_hp_certify_number", $certify_number); 


		// AlimTalk BEGIN --------------------------------------------------------
		include_once(G5_LIB_PATH.'/kakao.alimtalk.lib.php');

		$alimtalk = new AlimTalk();

		// 국가번호 포함한 전화번호 (수신번호)
		$to = $alimtalk->set_phone_number($hp);

		// 발송할 메시지에 해당하는 템플릿 코드
		$template_code = 'os_member_002';  // 가입_휴대폰인증번호

		// 발송할 메시지에 해당하는 템플릿 내용
		// [오더스테이션] 휴대폰 인증번호 #{인증번호} 위 번호를 인증창에 입력하세요.
		$text = $alimtalk->get_template($template_code);
		$text = str_replace('#{인증번호}', $certify_number, $text);

		// 알림톡 메시지 설정
		$alimtalk->set_message($template_code, $to, $text);

		// 알림톡 발송
		$is_alimtalk_success = $alimtalk->send();
		// AlimTalk END   --------------------------------------------------------
	
		if ($msg == '' && $is_alimtalk_success) {
			//$response = array('type' => 'ok','certify' => $certify_number);
			$response = array('type' => 'ok','msg' => '인증번호가 전송되었습니다.');
		} else {
			$response = array('type' => 'error','msg' => '관리자에게 문의하세요.');
		}

	} else {
		if($mb_kind =='id') {
			$response = array('type' => 'error','msg' => '성명 또는 전화번호가 없습니다.');
		} else {
			$response = array('type' => 'error','msg' => '아이디 또는 전화번호가 없습니다.');
		}
		
	}
}

if ($mode=='check') {

	$mb_hp_certify = $_REQUEST['mb_hp_certify'];
	
	// 인증번호가 같다면 
	if (get_session("ss_hp_certify_number") == $mb_hp_certify) { 
		$response = array('type' => 'ok','msg' => '인증되었습니다.');
	} else {
		//$response = array('type' => 'error','msg' => get_session("ss_hp_certify_number"));
		$response = array('type' => 'error','msg' => '인증번호가 틀립니다. 다시입력해주세요.');
	}

}

echo json_encode($response);

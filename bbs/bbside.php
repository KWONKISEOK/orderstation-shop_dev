<?php
include_once('./_common.php');

if (G5_IS_MOBILE) {

    return;
}

?>

<style>

</style>

<div class="cstop">
<?php
if($bo_table == 'health' or $bo_table == 'healthrecipe' or $bo_table == 'gallery' or $bo_table == 'allsee' or $bo_table == 'gallery_bak') {?>
<dl class="xans-element- xans-layout xans-layout-boardinfo board"><dt>HEALTH CARE</dt>
    <dd class="xans-record-"><a class="btn" href="/bbs/board.php?bo_table=allsee">전체보기</a></dd>
	<dd class="xans-record-"><a class="btn" href="/bbs/board.php?bo_table=health">365 건강지킴이</a></dd>
	<dd class="xans-record-"><a class="btn" href="/bbs/board.php?bo_table=healthrecipe">건강 레시피</a></dd>
	<dd class="xans-record-"><a class="btn" href="/bbs/board.php?bo_table=gallery">OS뉴스레터</a></dd>
	<? if ($is_admin) { ?>
	<dd class="xans-record-"><a class="btn" href="/bbs/board.php?bo_table=gallery_bak">OS뉴스레터_백업!</a></dd>
	<?
	}?>
</dl>
	
<?php } else { ?>
<dl class="xans-element- xans-layout xans-layout-boardinfo board"><dt>CUSTOMER CENTER</dt>
	<dd class="xans-record-"><a href="/bbs/faq.php" class="btn">이용안내 FAQ</a></dd>
	<?php if ($member['mb_type'] == 1) { ?>
	<dd class="xans-record-"><a href="/bbs/board.php?bo_table=noticedrug" class="btn">약사님 공지사항</a></dd>
	<?php } ?>
	<?php if ($is_admin == 'super') { ?>
	<dd class="xans-record-"><a href="/bbs/board.php?bo_table=noticedrug" class="btn">약사님 공지사항</a></dd>
	<?php } ?>

	<dd class="xans-record-"><a href="/bbs/board.php?bo_table=notice" class="btn">공지사항</a></dd>

	<dd class="xans-record-"><a href="/shop/itemqalist.php" class="btn">상품 Q&A</a></dd>
	<dd class="xans-record-"><a href="/shop/itemuselist.php" class="btn">상품 사용후기</a></dd>

	<dd class="xans-record-"><a href="/bbs/board.php?bo_table=free" class="btn">게시판</a></dd>
	<!-- <dd class="xans-record-"><a href="/bbs/board.php?bo_table=qa" class="btn">질문/답변</a></dd> -->
	<dd class="xans-record-"><a href="/bbs/qalist.php" class="btn">1:1문의</a></dd>
	<?php if ($member['mb_type'] == 1 || $is_admin =="super") { ?>
	<dd class="xans-record-"><a href="/shop/itemuselist_pharm.php" class="btn">약사님 전용 상품 사용후기</a></dd>
	<?php } ?>
</dl>	
<?php }?>

</div>


<!-- <div id="snb" class="snb_cc">
	<h2 class="tit_snb">고객센터</h2>
	<div class="inner_snb">
		<ul class="list_menu">
			<li>
			<a href="/bbs/faq.php">FAQ</a>
			</li>
			<li><a href="/bbs/board.php?bo_table=notice">공지사항</a></li>
			<li >
				<a href="/bbs/board.php?bo_table=free">자유게시판</a>
			</li>
			<li>
				<a href="/bbs/board.php?bo_table=qa">질문/답변</a>
			</li>
			<li>
				<a href="/bbs/qalist.php">1:1문의</a>
			</li>

		</ul>
	</div>
</div>
 -->
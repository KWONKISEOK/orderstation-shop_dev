<?php
include_once('./_common.php');


// 세션에 저장된 토큰과 폼값으로 넘어온 토큰을 비교 
if ($_GET["token"] && get_session("ss_token") == $_GET["token"]) { 
    set_session("ss_token", ""); // 맞으면 세션을 지워 다시 입력폼을 통해서 들어오도록 한다. 
} else { 
    alert_close("인증번호 발송시 오류가 발생하였습니다."); 
    exit; 
} 

$default['de_icode_server_ip'] = "211.172.232.124"; 
$default['de_icode_id'] = "아이코드 회원아이디"; 
$default['de_icode_pw'] = "아이코드 패스워드"; 
$default['de_icode_server_port'] = "7295"; 
$default['de_sms_hp'] = "보내는 사람 전화번호"; 

// SMS BEGIN -------------------------------------------------------- 
$receive_number = preg_replace("/[^0-9]/", "", $hp); // 수신자번호 
$send_number = preg_replace("/[^0-9]/", "", $default['de_sms_hp']); // 발신자번호 

$certify_number = rand(100000, 999999); 

echo $certify_number;


// 생성된 인증번호를 세션에 저장함 
// form 에서 넘어온 인증번호와 비교하여 같으면 글쓰기 허용함 
set_session("ss_hp_certify_number", $certify_number); 

exit;

$sms_contents = ""; 
$sms_contents .= $certify_number; 
$sms_contents .= "\n\n인증번호 입니다."; 

if ($receive_number) { 
    include_once("$g4[path]/lib/icode.sms.lib.php"); 
    $SMS = new SMS; // SMS 연결 
    $SMS->SMS_con($default['de_icode_server_ip'], $default['de_icode_id'], $default['de_icode_pw'], $default['de_icode_server_port']); 
    $SMS->Add($receive_number, $send_number, $default['de_icode_id'], stripslashes($sms_contents), ""); 
    $SMS->Send(); 
} 
// SMS END  -------------------------------------------------------- 

alert_close("인증번호를 전송하였습니다. 인증번호를 확인 후 입력하여 주십시오."); 


?> 
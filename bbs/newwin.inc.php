<?php
if (!defined("_GNUBOARD_")) exit; // 개별 페이지 접근 불가

if (!defined('_SHOP_')) {
    $pop_division = 'comm';
} else {
    $pop_division = 'shop';
}

// PC 팝업 노출 최대 갯수는 3개
$sql = " select * from {$g5['new_win_table']}
          where '".G5_TIME_YMDHIS."' between nw_begin_time and nw_end_time
            and nw_device IN ( 'both', 'pc' ) and nw_division IN ( 'both', '".$pop_division."' ) and nw_service_view_1 = 'Y'
          order by nw_order asc limit 3";
$result = sql_query($sql, false);

$cnt = sql_num_rows($result);
?>

<style>
    /*
    * ------------------------------
    * popup
    * ------------------------------
    */

    
    .modal-popup .modal-wrap {
        margin: 0 20px;
    }

    /* event */
    .modal-popup.modal-event .modal-wrap {
        width: auto;
        min-width: 0;
    }

    /*
    * ------------------------------
    * popup - content
    * ------------------------------
    */
    .modal-popup .modal-body {
        background: #fff;
    }
      /*
    * ------------------------------
    * popup - foot
    * ------------------------------
    */
    .modal-popup .modal-foot {
        display: flex;
        align-items: center;
        justify-content: space-between;
        height: 35px;
        border-top: 1px solid #eee;
    }
    .modal-foot .btn-text {
        border: 0;
        background: none;
        color: #252525;
        font-size: 13px;
        padding: 8px 20px;
    }

    /*
    * ------------------------------
    * 체크박스
    * ------------------------------
    */
    .form-checkbox input[type="checkbox"] + label{
        font-size: 14px;
        letter-spacing: -0.35px;
    }
    .form-checkbox input[type="checkbox"] + label:before {/*체크해제 상태*/
        background-image: url(/theme/basic/img/event-check-off.png);
    }
    .form-checkbox input[type="checkbox"]:checked + label:before { /*체크된 상태*/
        background-image: url(/theme/basic/img/event-check-on.png);
    }
    /*
    * ------------------------------
    * 스크롤바
    * ------------------------------
    */
    /* width */
    ::-webkit-scrollbar {
        width: 10px;
        height: 10px;
    }
    /* Track */
    ::-webkit-scrollbar-track {
        background: #eee; 
        border-radius: 5px;
    }
    /* Handle */
    ::-webkit-scrollbar-thumb {
        background: #777; 
        border-radius: 5px;
    }
    /*
    * ------------------------------
    * 이미지
    * ------------------------------
    */

    .event-image-box {
        width: 100%;
        height: 400px;
        margin: 0;
        overflow-x: auto;
        overflow-y: hidden;
        
    }

    .event-image-box::before, .event-image-box::after {
        display: table;
        clear: both;
        content: '';
    }

    .event-image-box .image {
        float: left;
        width: 370px;
        height: 400px;
        border-right: 1px solid #eee;
        box-sizing: border-box;
    }
    .event-image-box .image:last-child {
        border-right: none;  
    }
    

    .event-image-box .image img {
        width: 100%;
        /* height: 100%; */
    }

</style>

<!-- 팝업레이어 시작 { -->
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/jquery-ui.min.js"></script>

<?php if (!$_COOKIE["os_main_pop"] && $cnt > 0) { ?>
<script>

    $("html, body").css({"overflow":"hidden", "height":"100%"});
    $("#main_pop").bind("touchmove", function(e) {
        e.preventDefault();
    });

</script>
<!-- [D] 팝업열기 방법1 : .on 클래스 추가시 팝업 view -->
<!-- [D] 팝업열기 방법2 : data-pop(팝업창) 의 이름과 data-pop-btn(팝업여는 버튼) 의 이름이 같아야 함 -->
<section id="main_pop" class="modal-popup modal-event on" data-pop="eventPop">
    <div class="modal-wrap">
        <div class="modal-body">
            <div class="event-image-box">
                <?php
                for ($i = 0; $nw = sql_fetch_array($result); $i++) {
                    $pop_view = "false";

                    // 이미 체크 되었다면 Continue
                    if ($_COOKIE["os_main_pop"])
                        continue;

                    //전체
                    if ($nw['nw_type'] == "A" || $nw['nw_type'] == "") {
                        $pop_view = "true";
                    }
                    //일반
                    if ($nw['nw_type'] == "B" && $member["mb_type"] == "0") {
                        $pop_view = "true";
                    }
                    //약국
                    if ($nw['nw_type'] == "C" && $member["mb_type"] == "1") {
                        $pop_view = "true";
                    }
                    ?>

                    <?php if ($pop_view == "true") { ?>
                        <div class="image">
                            <a href="<?php echo $nw['nw_href'] ?>" target="_self">
                            <?php echo conv_content($nw['nw_content'], 1); ?>
                            </a>
                        </div>
                    <?php } ?>

                <?php } ?>
            </div>
        </div>
        <div class="modal-foot">
            <button type="button" id="main_pop_reject" class="btn-text" data-close-time="24">오늘은 그만보기</button>
            <button type="button" data-pop-btn="eventPop" class="btn-text modal-close">닫기</button>
        </div>
    </div>
</section>
<?php } ?>

<script>
    $(document).ready(function () {
        popupShowHide(); //팝업
        eventImgSize(); //이벤트 이미지 사이즈
        $(window).resize(function() {
            eventImgSize();
        });
        $("#main_pop_reject").click(function () {
            var exp_time = $(this).data('close-time');
            set_cookie('os_main_pop', 1, exp_time, g5_cookie_domain);
            $('.modal-close').click();
        });
    });
    /* =====================================
    * 임의 작업( 개발 시 지워주세요)
    * =====================================*/
    function eventImgSize() {
        var windowWidth = $(window).width();
        var imgLength = $('.event-image-box .image').size();
        var imgWidth = $('.event-image-box .image').width();
        if( imgLength == 1 ) { //이미지가 1개일때
            $('.event-image-box').css("width","370px");
        } else if(imgLength == 2 ) { //이미지가 2개일때
            $('.event-image-box').css("width","740px");
        } else if (imgLength == 3) { //이미지가 3개일때
            $('.event-image-box').css("width","1110px");
        }
    }

    /* =====================================
    * 레이어 팝업
    * =====================================*/
    function popupShowHide() {
        var openBtn = '[data-pop-btn]',
            closeBtn = '.modal-close';

        function getTarget(t) {
            return $(t).attr('data-pop-btn');
        }

        function open(t) { //팝업열기
            var showTarget = $('[data-pop="' + t + '"]');
            showTarget.addClass('on').attr('aria-modal', true).find('.modal-close').focus();
            showTarget.find('.modal-close').data('activeTarget', t);
        }

        function close(t) { //팝업닫기
            var activeTarget = $('[data-pop="' + t + '"]');
            activeTarget.removeClass('on').removeAttr('aria-modal'); //팝업닫기
            $('[data-pop-btn="' + t + '"]').focus(); //버튼으로 포커스 가도록
        }

        $(document).on('click', openBtn, function (e) {
            e.preventDefault();
            open(getTarget(this));
        });

        $(document).on('click', closeBtn, function (e) {
            e.preventDefault();
            close($(this).data('activeTarget'));

            $("html, body").css({"overflow":"auto", "height":"auto"});
            $('#main_pop').unbind('touchmove');
        });
    }
</script>
<!-- } 팝업레이어 끝 -->
<?
//해당약국을 추천약국으로 지정한 고객주문건 
include_once ('../not_login_common.php');

$add_sql = "";

if( empty($start_date) || empty($end_date) || empty($pharm_code) ){
	echo "파라미터 오류";
	exit;
}
if( !is_numeric($page) ){
	echo "잘못된 접근입니다.";
	exit;	
}
if( $page_size !="" ){
	if( !is_numeric($page_size) ){
		echo "잘못된 접근입니다.";
		exit;	
	}
}else{
	$page_size = 10;
}

if( $order_name != "" ){
	$add_sql .= " and a.od_name = '".$order_name."' ";
}
if( $order_hp != "" ){
	$add_sql .= " and replace( a.od_hp , '-' , '' ) = '".$order_hp."' ";
}
if( $order_gubun != "" ){
	$add_sql .= " and a.od_gubun = '".$order_gubun."' ";
}

$sql  = " select a.od_id
			 from tbl_shop_order a inner join tbl_shop_order_detail b on a.od_id = b.od_id 
					  inner join tbl_shop_order_receiver c on b.od_id = c.od_id and b.od_num = c.od_num
					  left join tbl_member d on b.comp_code = d.comp_code 
			 where 1=1 and a.pharm_custno = '$pharm_code' and c.od_date1 between '$start_date 00:00:00' and '$end_date 23:59:59' {$add_sql} 
			 order by a.od_id desc, c.od_num, c.od_seq ";

			//echo "<pre>".$sql."</pre>" ;

$res = sql_query($sql);
if(!$res) exit('Cannot run query.');

//총갯수구하기
$total_cnt = mysqli_num_rows($res);

$rows = $page_size; //리스트에 뿌려질 갯수 셋팅 
$total_page  = ceil($total_cnt / $rows);  // 전체 페이지 계산
if( empty($page) ){
	$page = 1; // 페이지가 없으면 첫 페이지 (1 페이지)
}
$from_record = ($page - 1) * $rows; // 시작 열을 구함


$response->total_cnt = $total_cnt ;
$response->total_page = $total_page ;

$sql  = " select a.od_id
				,a.mb_id
				,a.od_name
				,a.pharm_name
				,a.pharm_manager
				,a.od_name
				,a.od_tel
				,replace( a.od_hp , '-' , '' ) as od_hp
				,a.od_zip1
				,a.od_zip2
				,a.od_addr1
				,a.od_addr2
				,a.od_addr3
				,a.od_addr_jibeon					 
				,a.od_settle_case
				,a.pharm_custno
				,a.od_cart_price
				,a.od_gubun
				,a.od_mobile
				,a.od_memo
				,a.od_receipt_point
				,a.od_receipt_price
				,a.od_coupon
				,a.pharm_manager_dept
				,b.it_id
				,b.io_id
				,b.od_num
				,b.it_name
				,b.od_send_cost
				,b.od_option
				,b.od_period_yn
				,b.od_period_cnt
				,b.od_sale_price
				,b.od_dodome_price
				,b.od_supply_price
				,b.od_drug_price
				,b.od_incen
				,b.od_itm_sale_price
				,b.od_itm_dodome_price
				,b.od_itm_supply_price
				,b.od_itm_drug_price
				,b.od_itm_incen
				,b.od_total_sale_price
				,b.od_total_dodome_price
				,b.od_total_supply_price
				,b.od_total_drug_price
				,b.od_total_incen					 
				,c.od_b_name
				,c.od_b_tel
				,replace( c.od_b_hp , '-' , '' ) as od_b_hp
				,c.od_b_zip1
				,c.od_b_zip2
				,c.od_b_addr1
				,c.od_b_addr2
				,c.od_b_addr3
				,c.od_b_addr_jibeon
				,c.od_seq
				,c.od_date1
				,c.od_date2
				,c.od_date3
				,c.od_date4
				,c.od_qty
				,c.od_period_date
				,c.od_status
				,c.od_pay_yn
				,c.od_delivery_company
				,c.od_invoice
				,c.od_trans_memo
				,d.comp_name
			 from tbl_shop_order a inner join tbl_shop_order_detail b on a.od_id = b.od_id 
					  inner join tbl_shop_order_receiver c on b.od_id = c.od_id and b.od_num = c.od_num
					  left join tbl_member d on b.comp_code = d.comp_code 
			 where 1=1 and a.pharm_custno = '$pharm_code' and c.od_date1 between '$start_date 00:00:00' and '$end_date 23:59:59' {$add_sql}
			 order by a.od_id desc, c.od_num, c.od_seq limit {$from_record}, {$rows} ";

$res = sql_query($sql);
if(!$res) exit('Cannot run query.');


$i = 0;
while($row = mysqli_fetch_array($res,MYSQL_ASSOC)) {

	$response->rows[$i]['r_1']	= $row['od_id']; //주문번호
	$response->rows[$i]['r_2']	= $row['it_name']; //상품명
	$response->rows[$i]['r_3']	= $row['od_qty']; //주문수량
	$response->rows[$i]['r_4']	= $row['pharm_name']; //약국명
	$response->rows[$i]['r_5']	= $row['od_name']; //주문자
	$response->rows[$i]['r_6']	= $row['od_hp']; //주문자핸드폰
	$response->rows[$i]['r_7']	= $row['od_b_name']; //받는분
	$response->rows[$i]['r_8']	= $row['od_b_hp']; //받는분핸드폰
	$response->rows[$i]['r_9']	= $row['od_gubun']; //1:고객주문,2:약국주문,3:일반주문

$i++;
}

echo json_encode($response,JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE);

?>
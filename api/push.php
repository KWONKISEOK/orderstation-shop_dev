<?php
include_once('./_common.php');
include_once(G5_LIB_PATH.'/json.lib.php');

$cmd = post_if_set('cmd');
//json_encode($cmd);
//die();
if (! $cmd) res_json('110');	// PARAMETER_NOT_FOUND

// push 발송을 위한 디바이스 토큰을 신규등록 또는 갱신한다
if ($cmd == 'register-token') {
	$mb_id = post_if_set('mb_id');	// 회원 아이디	
	$token = post_if_set('token');	// 디바이스 토큰
	$os    = post_if_set('os');		// 디바이스 OS (A: Android, I: iOS)
	
	//echo "mb_id=" . $mb_id . "|token=" . $token . "|os=" . $os;
	//die();

	if (! $mb_id || ! $token || ! $os) res_json('110');	// PARAMETER_NOT_FOUND
	
	if (! exist_mb_id($mb_id)) res_json('120');	// DATA_NOT_FOUND
	
	// 회원의 os별 디바이스 토큰의 존재여부를 조회한다
	if (! exist_mb_id_token($mb_id, $os)) {
		// 디바이스 토큰 신규 등록
		$result = register_mb_token($token, $mb_id, $os);
		if ($result)
			res_json('100');	// SUCCESS
		else
			res_json('910');	// ERROR_DB_INSERT
	} else {
		// 기존 디바이스 토큰 갱신
		$result = update_mb_token($token, $mb_id, $os);
		if ($result)
			res_json('100');	// SUCCESS
		else
			res_json('920');	// ERROR_DB_UPDATE
	}
}

// 회원에게 push를 발송한다
if ($cmd == 'send-push') {
	$mb_id   = post_if_set('mb_id');	// 회원 아이디	
	$title = post_if_set('title');	    // 타이틀
	$message = post_if_set('message');	// 메시지 내용
	
	if (! $mb_id || ! $message || !$title) res_json('110');   // PARAMETER_NOT_FOUND
	if (! exist_mb_id($mb_id)) res_json('120');	   // DATA_NOT_FOUND

	// 회원의 디바이스 토큰 정보를 조회한다
	$result = get_mb_id_token($mb_id);

	if (! $result) res_json('120');	// DATA_NOT_FOUND

	for ($i = 0; $i < count($result); $i++) {
		$token = $result[$i]['token'];
		$os    = $result[$i]['os'];

		send_push($os, $title, $message, $token);
	}
	
	res_json('100');	// SUCCESS
}

// 앱 버전 정보를 조회한다
if ($cmd == 'app-version') {
	$os = post_if_set('os');	// 회원 아이디	
	
	if (! $os) res_json('110');   // PARAMETER_NOT_FOUND
	
	if (! get_app_version($os)) res_json('120');	   // DATA_NOT_FOUND

	$version = get_app_version($os);
	if (! $version) res_json('120');	// DATA_NOT_FOUND

	res_json('100', $version);
}


?>
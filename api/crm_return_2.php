<?
//해당약국을 추천약국으로 지정한 고객주문건 
include_once ('../not_login_common.php');

if( empty($start_date) || empty($end_date) || empty($pharm_code) ){
	echo "파라미터 오류";
	exit;
}
if( !is_numeric($page) ){
	echo "잘못된 접근입니다.";
	exit;	
}
if( $page_size !="" ){
	if( !is_numeric($page_size) ){
		echo "잘못된 접근입니다.";
		exit;	
	}
}else{
	$page_size = 10;
}

$sql  = " 	
			select * from(
				select it_id 
				from tbl_shop_item where it_crm_yn > 0
			) as x left join (		
			
					 select b.it_id , sum( case when a.pharm_custno = '$pharm_code' then 1 else 0  end ) as it_id_cnt
					 from tbl_shop_order a inner join tbl_shop_order_detail b on a.od_id = b.od_id 
							  inner join tbl_shop_order_receiver c on b.od_id = c.od_id and b.od_num = c.od_num
					 where 1=1 and c.od_date1 between '$start_date 00:00:00' and '$end_date 23:59:59' 
					 group by b.it_id
					
			 ) as y on x.it_id = y.it_id ";

			//echo "<pre>".$sql."</pre>" ;

$res = sql_query($sql);
if(!$res) exit('Cannot run query.');

//총갯수구하기
$total_cnt = mysqli_num_rows($res);

$rows = $page_size; //리스트에 뿌려질 갯수 셋팅 
$total_page  = ceil($total_cnt / $rows);  // 전체 페이지 계산
if( empty($page) ){
	$page = 1; // 페이지가 없으면 첫 페이지 (1 페이지)
}
$from_record = ($page - 1) * $rows; // 시작 열을 구함

$response->total_cnt = $total_cnt ;
$response->total_page = $total_page ;

$sql  = "
			select  
				x.it_id , x.it_name , x.it_maker , x.it_origin , x.it_brand , x.it_model , ifnull(y.it_id_cnt,0) as it_id_cnt , x.it_crm_yn
			from(
				select it_id , it_name , it_maker , it_origin , it_brand , it_model , it_crm_yn
				from tbl_shop_item where it_crm_yn > 0
			) as x left join (		
			
					 select b.it_id , sum( case when a.pharm_custno = '$pharm_code' then 1 else 0  end ) as it_id_cnt
					 from tbl_shop_order a inner join tbl_shop_order_detail b on a.od_id = b.od_id 
							  inner join tbl_shop_order_receiver c on b.od_id = c.od_id and b.od_num = c.od_num
					 where 1=1 and c.od_date1 between '$start_date 00:00:00' and '$end_date 23:59:59' 
					 group by b.it_id
					
			 ) as y on x.it_id = y.it_id limit {$from_record}, {$rows}
		";

$res = sql_query($sql);
if(!$res) exit('Cannot run query.');


$i = 0;
while($row = mysqli_fetch_array($res,MYSQL_ASSOC)) {

	$response->rows[$i]['r_1']	= $row['it_id']; //상품코드
	$response->rows[$i]['r_2']	= $row['it_name']; //상품명
	$response->rows[$i]['r_3']	= $row['it_maker']; //제조사
	$response->rows[$i]['r_4']	= $row['it_origin']; //원산지
	$response->rows[$i]['r_5']	= $row['it_brand']; //브랜드
	$response->rows[$i]['r_6']	= $row['it_model']; //규격
	$response->rows[$i]['r_7']	= $row['it_id_cnt']; //상품주문건수
	$response->rows[$i]['r_8']	= $row['it_crm_yn']; //CRM재고관리 대상값

$i++;
}

echo json_encode($response,JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE);

?>
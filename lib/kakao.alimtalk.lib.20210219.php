<?php
if (!defined('_GNUBOARD_')) exit;

/**
* 카카오 알림톡 발송을 관장하는 메인 클래스이다.
*
*/
class AlimTalk {

	// 슈어엠 API url
	var $service_url = 'https://api.surem.com/alimtalk/v2/json';
	// 슈어엠 제공 아이디
	var $usercode = 'onkorderstn';
	// 슈어엡 제공 회사코드
    var $deptcode = '4P-WX1-DY';
	// 발신프로필키
    var $yellowid_key;

	// 문자 재전송시 발신번호
	var $from = '15445462';
	
	var $button_set;
	
	var $messages;
	var $messagesArr;

    function __construct($uuid = 'orderstation')
    {
        switch ($uuid) {
        case 'hahaha':  // 하하하 발신프로필키
            $this->yellowid_key = '1383cdb086bf6c1f320fa63c64ebbfc5b57ce96c';
            break;
        case 'orderstation':    // 오더스테이션 발신프로필키
            //$this->yellowid_key = '1383cdb086bf6c1f320fa63c64ebbfc5b57ce96c';
            $this->yellowid_key = '2e8414a6a58431935aa1e44d91f3edc7e1e8e93c';
            break;
        case 'isbre':    // 이즈브레 발신프로필키
            //$this->yellowid_key = '1383cdb086bf6c1f320fa63c64ebbfc5b57ce96c';
            $this->yellowid_key = 'b96b0d8dbb6dd6421775b2a1b8076bbe2fb7c2d5';
            break;
        default:
            $this->yellowid_key = '2e8414a6a58431935aa1e44d91f3edc7e1e8e93c';
            break;
        }
    }

	/**
	 * 발송할 메시지를 생성한다.
	 * 
	 * $message_id    : 메시지 고유키 값. 미입력시 단보전송은 1이며, 동보전송은 1씩 증가
     * $to            : 국가번호 포함 전화번호. 예시) 821012345678
     * $text          : 요청할 템플릿 내용.
     * $code : 요청할 템플릿 코드명. 예시) onk_in_001
	 * $reserved_time : 예약 발송시 예약시간(12자리). 미입력시 즉시 전송. 예시) 201912310910 
	 * $re_send       : 알림톡 전송 실패시 문자 재전송 여부. 미입력시 N. (Y: $text 문구로 전송, R: $re_text 문구로 전송)
	 *
	 */
	function set_message($code, $to, $text, $re_send='Y', $reserved_time=null)
	{
		$this->message = array(
			//'message_id' => '1',
			'to' => $to,
			'text' => $text,
			'from' => $this->from,
			'template_code' => $code
		);

		if ($re_send == 'Y') {
			$re_text = $text;
		} else if ($re_send == 'R') {
			$re_text = $text;	// 데체 메시지를 미리 정의해서 조회 필요.
		}

		if ($re_send != 'N') {
			$this->message = array_merge($this->message, array(
				're_send' => $re_send,
				're_text' => $re_text			
			));
		}
		
		if ($reserved_time) {
			$this->message = array_merge($this->message, array(
				'reserved_time' => $reserved_time
			));
		}
		
		//$this->set_button($code);
		//$this->message = array_merge($this->message, array('buttons' => $this->button_set));

		$this->messagesArr = array($this->message);		
	}
	
	/**
	 * 카카오 알림톡을 발송한다.
	 *
	 */
	public function send()
	{
		$curl_post_data = array(
			'usercode' => $this->usercode,
			'deptcode' => $this->deptcode,
			'yellowid_key' => $this->yellowid_key,
			'messages' => $this->messagesArr
		);
	 
		$this->sys_log('send message: ', $this->messagesArr);
	 
		//$json_data = json_encode($curl_post_data, JSON_UNESCAPED_UNICODE);
		$json_data = json_encode($curl_post_data);
		
		$header = array(
			'Content-Type: application/json', 
			'Content-Length: ' . strlen($json_data)
		);
		
		$ch = curl_init($this->service_url);
		
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
		curl_setopt($ch, CURLOPT_URL, $this->service_url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($ch, CURLOPT_POST, 1);   
	 
		$response = curl_exec($ch);
	 
		if($response === FALSE){
			die(curl_error($ch));
		}
	 
		$responseData = json_decode($response, TRUE);

		//var_dump($responseData);
		//echo $responseData['code'];
		//echo $responseData['message'];
		//echo $responseData['results'];
		//echo $responseData['additional_information'];

		$this->sys_log('result message: ', $responseData);
		
		if ($responseData['code'] == '200')	// Success
			return true;
		else
			return false;
	}

    /**
     * 국가번호 포함 전화번호 형식으로 설정
     *
     */
    function set_phone_number($to)
    {
        $to = str_replace('-', '', $to);
        return  '82' . substr($to, 1, strlen($to));
    }

    /**
     * 버튼 정보(최대 5개 등록 가능)를 생성한다.
     * TODO: 사용할 경우에는 버튼 정보 추가 정의 필요
     */
    function set_button($code)
    {
        $buttons = array();

        switch ($code) {
            case 'onk_in_001':
                $buttons = array(
                    array(
                        'button_name' => '오더스테이션 이동',
                        'button_url' => 'http://orderstation.kr'
                    ),
                    array(
                        'button_name' => '오더스테이션 회원가입',
                        'button_url' => 'http://orderstation.kr/bbs/register.php'
                    )
                );
                break;
            case 'orderstation_member_01':
                $buttons = array(
                    array(
                        'button_name' => '오더스테이션 이동',
                        'button_url' => 'http://orderstation.kr'
                    )
                );
                break;
            default:
                $buttons = array(
                    array(
                        'button_name' => '오더스테이션 이동',
                        'button_url' => 'http://orderstation.kr'
                    )
                );
                break;
        }

        $this->button_set = $buttons;
    }

    /**
     * 템플릿 코드에 해당하는 템플릿 내용 조회
     *
     */
    function get_template($code)
    {
        switch ($code) {
            // 오더스테이션 템플릿 -------------------------------------------------------------------------------------

            // 회원 관리
            case 'orderstation_member_00':	// 신규회원가입축하 (수신: 고객)
                $template = '[오더스테이션]]#{이름}고객님 회원가입을 축하드립니다. 오더스테이션만의 건강한 먹거리와, 다양한 건강정보를 약사님과의 1:1 건강 상담과 함께 누려보세요! .신규 가입 포인트 #{금액}원 쿠폰을 지급해 드리오니 마이페이지를 확인하세요~!';
                break;
            case 'orderstation_member_02':	// 회원가입승인 (수신: 고객)
                $template = '[오더스테이션]#{이름}님의 가입정보가 사용승인처리되었습니다. 감사합니다.';
                break;
            case 'orderstation_member_03':	// 가입_휴대폰인증번호 (수신: 고객)
                $template = '[오더스테이션] 휴대폰 인증번호 #{인증번호} 위 번호를 인증창에 입력하세요.';
                break;

            // 1:1 건강상담
            case 'orderstation_qna_01': // 1:1건강상담접수 => 1:1 건강상담 (수신:	고객)
                $template = '[오더스테이션]#{이름} 고객님, 1:1건강상담 문의가 접수되었습니다.감사합니다.';
                break;
            case 'orderstation_qna_02': // 1:1건강상담답변요청 => 1:1 건강상담 (수신:	약국)
                $template = '[오더스테이션]#{이름} 고객님의 1:1건강상담 문의가 접수되었습니다.답변해주시면 감사하겠습니다.';
                break;
			case 'orderstation_qna_02_1': // 1:1건강상담답변요청 => 1:1 건강상담 (수신:	약국)
                $template = '[오더스테이션]#{이름} 고객님의 1:1건강상담 문의가 접수되었습니다.답변해주시면 감사하겠습니다.';
                break;
            case 'orderstation_qna_03':   // 1:1건강상담답변완료 => 1:1 건강상담 (수신: 고객)
                $template = '[오더스테이션]#{이름} 고객님,1:1건강상담 문의에 대한 답변이 등록되었습니다.감사합니다.';
                break;
			case 'orderstation_qna_03_1':   // 1:1건강상담답변완료 => 1:1 건강상담 (수신: 고객)
                $template = '[오더스테이션]#{이름} 고객님,1:1건강상담 문의에 대한 답변이 등록되었습니다.감사합니다.';
                break;
            // 고객문의 내역
            case 'orderstation_qna_04': // 고객문의접수 => 고객문의내역 (수신: 고객)
                $template = '[오더스테이션] #{이름} 고객님, 고객 문의가 접수되었습니다.감사합니다.';
                break;
            case 'orderstation_qna_05': // 고객문의답변요청 => 고객문의내역 (수신: OS관리자)
                $template = '[오더스테이션] #{이름} 고객님의 문의가 접수되었습니다.답변해주시면 감사하겠습니다.';
                break;
            case 'orderstation_qna_06': // 고객문의답변완료 => 고객문의내역 (수신: 고객)
				$template = '[오더스테이션] #{이름} 고객님, 고객문의에 대한 답변이 등록되었습니다.감사합니다.'; 
                break;

            // 상품후기 등록
            case 'orderstation_qna_07': // 상품후기등록쿠폰 => 상품후기등록 (수신: 고객)
                $template = '[오더스테이션] #{이름}고객님, 후기 등록에 감사드립니다. OS앱회원 후기등록 쿠폰 #{금액}원이 지급되었습니다. 후기등록 쿠폰은 지급 후 30일 동안(사용기한; 30일) 사용할 수 있습니다. 저희 오더스테이션의 많은 이용 부탁드립니다. 감사합니다.';
                break;

            // 건강상담 등록
            case 'orderstation_qna_08': // 건강상담등록쿠폰 => 건강상담등록 (수신: 고객)
                $template = '[오더스테이션]#{이름}고객님, 건강상담 등록에 감사드립니다. OS앱회원 건강상담 쿠폰 #{금액}원이 지급되었습니다. 건강상담 쿠폰은 지급 후 7일 동안(사용기한; 7일) 사용할 수 있습니다. 저희 오더스테이션의 많은 이용 부탁드립니다. 감사합니다.';
                break;

            // 주문 관리
            case 'orderstation_order_01':	// 주문완료-번호 => 주문완료시 (수신: 고객/약국/공급사)
                $template = '[오더스테이션] #{이름} 고객님의 주문입니다. 주문번호는 #{주문번호}입니다.';
                break;
            //case 'orderstation_order_02':	// 주문완료-정기주문번호
            //    $template = '[오더스테이션] #{이름} 고객님의 ○회 정기주문입니다. 주문번호는 #{주문번호}입니다.';
            //    break;
            case 'orderstation_order_03':	// 발송완료-정기주문 => 정기주문배송 (수신: 고객)
                $template = '[오더스테이션] #{이름} 고객님, #{상품명}(#{회차}회차)이 #{배송지}로 발송되었습니다.';
                break;
            case 'orderstation_order_04':	// 발송완료-일반주문 => 일반배송 (수신: 고객)
                $template = '[오더스테이션] #{이름} 고객님, #{상품명}이 #{배송지}로 발송되었습니다.';
                break;
            case 'orderstation_order_04_01': // 이즈브레 10+10 정기주문 마지막 횟차 발송완료-일반주문 => 일반배송 (수신: 고객)
                $template = '[오더스테이션] 이즈브레 정기주문 5회, 마지막회차 배송되었습니다. 오더스테이션의 많은 관심 감사드립니다.';
                break;
            case 'orderstation_order_05':	// 주문취소건 => 주문취소시 (수신: 고객)
                $template = '[오더스테이션] #{이름} 고객님, 주문번호 #{주문번호}가 정상적으로 취소되었습니다.';
                break;
            case 'orderstation_order_06':	// 첫구매포인트 => 첫구매 (수신: 고객)
                $template = '[오더스테이션] #{이름}고객님,  첫 구매에 감사드립니다. #{금액}쿠폰을 지급해 드리오니 \'마이페이지에서 확인하세요~! 감사합니다.';
                break;
            case 'orderstation_order_07':	// 정기주문주문완료메시지 => 정기주문완료시 (수신: 공급사)
                $template = '[오더스테이션] #{이름} 고객님의 #{회차}회 정기주문입니다. 주문번호는 #{주문번호}입니다.';
                break;
            case 'orderstation_order_08':	// 확정후5일 이후에 배송으로 변경하지 않은것들 => 관리자로그인시 (수신: 공급사)
                $template = '[오더스테이션] 송장번호 입력 시기가 지났습니다. 배송선택 및 송장입력해주세요. 주문번호는 #{주문번호}입니다.';
                break;
			/*
            case 'orderstation_order_09':	// 약국주문일경우 영업자에게 보냄
                $template = '[오더스테이션] #{약국명} 주문입니다. 주문번호는 #{주문번호}입니다.';
                break; */
            case 'orderstation_order_10':
                $template = '[오더스테이션] #{이름} 고객님의 주문입니다. 주문번호는 #{주문번호}입니다.'.Chr(10).'입금계좌 : #{계좌번호}'.Chr(10).'입금기한은 #{입금기한}이며 입금기한내 미입금시 주문이 자동 취소됩니다.'.Chr(10).'감사합니다.';
                break;
            case 'orderstation_order_11':
                $template = '[오더스테이션] #{이름} 고객님의 주문입니다. 주문번호는 #{주문번호}입니다.'.Chr(10).Chr(10).'입금계좌 : #{계좌번호}'.Chr(10).'입금기한 : #{입금기한}'.Chr(10).Chr(10).'입금기한내 미입금시 주문이 자동 취소됩니다.'.Chr(10).Chr(10).'감사합니다.';
                break;
            case 'orderstation_order_11_1':
                $template = '[오더스테이션] #{이름} 고객님의 무통장입금 주문건(주문번호:#{주문번호}) 입금 되었습니다.';
                break;
            case 'orderstation_order_12':
                $template = '[오더스테이션]'.Chr(10).'안녕하세요.'.Chr(10).Chr(10).'#{이름} 고객님의 주문 비밀번호 재발급 안내 드립니다.'.Chr(10).Chr(10).'- 주문번호 : #{주문번호}'.Chr(10).'- 재발급 비밀번호 : #{주문비밀번호}'.Chr(10).Chr(10).'문의사항은 고객센터로 전화주시면 도움드리겠습니다.'.Chr(10).'☎고객센터 1544-5462'.Chr(10).Chr(10).'감사합니다.';
                break;
            case 'orderstation_order_01_1':	// 약국주문, 약국에서 고객주문일경우 영업자에게 보냄
                $template = '[오더스테이션] #{약국명}, #{고객명} 고객님의 주문입니다. 주문번호는 #{주문번호}입니다.';
                break;
            case 'orderstation_pay_01':	// 미입금취소 => 미입금 (수신: 고객)
                $template = '[오더스테이션]주문번호 #{주문번호}가 미입금으로인해 취소되었습니다.';
                break;
            case 'orderstation_pay_02':	// 카드비정상_1 => 빌링결제실패 (수신: 고객)
                $template = '카드비정상으로 오더스테이션에서 결제불가합니다.';
                break;
            case 'orderstation_pay_03':	// 카드비정상_2 => 빌링결제실패 (수신: 01090480403 (한영수부장))
                $template = '카드비정상으로 오더스테이션에서 결제불가한 건이있습니다';
                break;

            // 하하하 템플릿 -------------------------------------------------------------------------------------------

            case 'crm_member_001':	// 약국 방문 고객에게 환영 메시지
                $template = '안녕하세요 #{이름} 고객님, #{약국명}의 건강 관리 대상 고객으로 등록되신 것을 환영합니다. 궁금하신 점이 있으면 약사님과 상의하시고 오늘도 건강한 하루 되세요!';
                break;
            case 'orderstation_member_01':	// 회원가입축하메시지
                $template = '안녕하세요 #{이름} 고객님, 오더스테이션 가입을 환영합니다! 오더스테이션만의 건강한 먹거리와, 다양한 건강정보를 약사님과의 1:1 상담과 함께 누려보세요!';
                break;
			// 클리노믹스 템플릿
			case 'clinomics_01':	// 회원가입축하메시지
                $template = '   #{이름}님 구매해주셔서 감사합니다.'.Chr(10).'신청해주신 제노피(유전자분석서비스)가 완료되었습니다.'.Chr(10).'지금 오더스테이션(www.orderstation.co.kr)에서 결과지 확인이 가능합니다.'.Chr(10).'구매하신 약국의 약사님과 결과지에 대해 상담해보세요.'.Chr(10).'[결과지 확인 바로가기]'.Chr(10).'#{결과지확인URL경로}';
                break;
			case 'clinomics_02':	// 회원가입축하메시지
                $template = '	#{이름}님 구매해주셔서 감사합니다.'.Chr(10).'신청해주신 제노비(장내 미생물 분석 서비스)가 완료되었습니다.'.Chr(10).'지금 오더스테이션(www.orderstation.co.kr)에서 결과지 확인이 가능합니다.'.Chr(10).'구매하신 약국의 약사님과 결과지에 대해 상담해보세요.'.Chr(10).'[결과지 확인 바로가기]'.Chr(10).'#{결과지확인URL경로}';
                break;

			// 이즈브레 템플릿 -------------------------------------------------------------------------------------------
            case 'isbre_002':	// 신규회원가입축하 (수신: 고객)
                $template = '[이즈브레몰] 안녕하세요 #{이름} 고객님, 이즈브레몰에 가입해주셔서 감사합니다. 신규 가입 감사 쿠폰 #{금액}원이 지급되었습니다. 마이페이지에서 꼭 확인하세요!';
                break;
            case 'isbre_006':	// 가입_휴대폰인증번호 (수신: 고객)
                $template = '[이즈브레몰] 휴대폰 인증번호 #{인증번호} 위 번호를 인증창에 입력하세요.';
                break;
            case 'isbre_008': // 고객문의답변요청 => 고객문의내역 (수신: OS관리자)
                $template = '[이즈브레몰] 관리자님, 고객 문의 게시판에 #{이름} 고객님의 문의가 접수되었습니다. 답변 부탁드립니다.';
                break;
            case 'isbre_007': // 고객문의답변완료 => 고객문의내역 (수신: 고객)
				$template = '[이즈브레몰] #{이름} 고객님, 고객문의에 대한 답변이 등록되었습니다. 감사합니다.'; 
                break;
            // 주문 관리
            case 'isbre_005':	// 주문완료-번호 => 주문완료시 (수신: 고객/약국/공급사)
                $template = '[이즈브레몰] #{이름} 고객님의 주문입니다. 주문번호는 #{주문번호}입니다.';
                break;
            case 'isbre_004':	// 발송완료-일반주문 => 일반배송 (수신: 고객)
                $template = '[이즈브레몰] #{이름} 고객님, #{상품명}이 #{배송지}로 발송되었습니다.';
                break;
            case 'isbre_003':	// 주문취소건 => 주문취소시 (수신: 고객)
                $template = '[이즈브레몰] #{이름} 고객님, 주문번호 #{주문번호}가 정상적으로 취소되었습니다.';
                break;
            case 'isbre_009':	// 주문취소건 => 주문취소시 (수신: 고객)
                $template = '[이즈브레몰] #{이름} 고객님. 고객님께서 구매하신 [#{상품명}] 의 주문이 정상적으로 완료 되었습니다. 주문번호는 #{주문번호}입니다. 이즈브레몰을 이용해주셔서 감사합니다.';
                break;


        }

        return $template;
    }
	
	// 로그 생성
	function sys_log($sys, $mess = '')
	{
		$dir = $_SERVER['DOCUMENT_ROOT'].'/logs/alimtalk/';
		//$dir = '/var/www/html/orderstation-shop-dev/logs/alimtalk/';
		$this->makedirs($dir, '0755');

		$filename = 'alimtalk_' . date('Ymd') . '.log';
		$fp = fopen($dir . $filename, "a+");
		if ($fp == null)
			return;

		if (is_array($mess)) {
			// $mess = serialize($mess);
			$mess = $this->_contextToString($mess);
		}

		// fwrite($fp, date("Y-m-d H:i:s") . " " . $sys . " " . $mess . "\n");
		fwrite($fp, date("Y-m-d H:i:s") . " " . $sys . "\n" . $mess . "\n");

		fclose($fp);
	}

	function _contextToString($context)
	{
		$export = '';
		foreach ($context as $key => $value) {
			$export .= "{$key}: ";
			$export .= preg_replace(array(
				'/=>\s+([a-zA-Z])/im',
				'/array\(\s+\)/im',
				'/^  |\G  /m'
			), array(
				'=> $1',
				'array()',
				'    '
			), str_replace('array (', 'array(', var_export($value, true)));
			$export .= PHP_EOL;
		}

		return str_replace(array(
			'\\\\',
			'\\\''
		), array(
			'\\',
			'\''
		), rtrim($export));
	}
	
	function makedirs($dirpath, $mode = 0777)
	{
		return is_dir($dirpath) || @mkdir($dirpath, $mode, true);
	}

}

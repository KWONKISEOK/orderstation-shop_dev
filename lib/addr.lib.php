<?php echo '<?xml version="1.0" encoding="utf-8"?>'; ?>
<?php
include $_SERVER['DOCUMENT_ROOT'] . "/include/DBConn.php";
?>
    <ROW>
		<?php
		$db = new DBConn( "orderstation" );

		if ( $_REQUEST["step"] == "1" ) {
			$SQL = "SELECT Distinct GUGUN ";
			$SQL .= "FROM tbl_addr ";
			$SQL .= "WHERE SIDO = '" . $_REQUEST["MEM_SIDO"] . "' ";
			$SQL .= "ORDER BY GUGUN ";
		} else {
			$SQL = "SELECT Distinct DONG ";
			$SQL .= "FROM tbl_addr ";
			$SQL .= "WHERE SIDO = '" . $_REQUEST["MEM_SIDO"] . "' AND GUGUN = '" . $_REQUEST["MEM_GUGUN"] . "' ";
			$SQL .= "ORDER BY DONG ";
		}

		$res = $db->ExecSQL( $SQL );

		while ( $row = mysqli_fetch_row( $res ) ) {
			?>
            <CELL>
                <NAME><?php echo $row[0]; ?></NAME>
            </CELL>
			<?php
		}
		?>
    </ROW>
<?php
$db->Disconnect();
?>
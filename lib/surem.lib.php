<?php
if (!defined('_GNUBOARD_')) exit;

class SureM {

	var $_usercode  = 'onkorderstn';		// usercode (surem 아이디)
	var $_reqname  = '오더스테이션';		// 회신자명
	var $_reqphone  = '15445462';		// 회신자 번호
	var $_callname  = '';				// 수신자명
	var $_callphone = '';				// 수신자 번호
	var $_subject 	= '';				// MMS 제목 (sms일 땐 ''로 해도 됨)
	var $_msg 		= '';				// 문자내용
	var $_reqtime 	= '00000000000000';	// 예약문자 전송시 'YYYYmmddHHMMss', 즉시전송시 '00000000000000'
	var $_result 	= '0';				// Default = 0, ( 0 : 즉시전송(숫자 0) R : 예약전송 )
	var $_kind 		= 'S';				// M : MMS, S : SMS, I : 국제문자, L : 국제 MMS
	
	var $_sms_table = 'orderstation_sms.SUREData_temp';
	var $_query 	= '';
	
    function __construct($mode = 'production')
    {
        switch ($mode) {
        case 'testing':
            $this->_sms_table = 'orderstation_sms.SUREData_temp';
            break;
        case 'development':
            $this->_sms_table = 'orderstation_sms.SUREData';
            break;			
        case 'production':
			$this->_sms_table = 'orderstation_sms.SUREData';
            break;
        default:
            $this->_sms_table = 'orderstation_sms.SUREData_temp';
			break;
        }
    }

	public function insert_query_builder($callname, $callphone, $subject, $msg, $reqtime, $result, $kind) 
	{
		$this->_callname  = $callname;
		$this->_callphone = $callphone;
		$this->_subject   = ($kind == 'S') ? $this->_subject : $subject;
		$this->_msg       = $msg;
		$this->_reqtime   = ($result != 'R') ? $this->_reqtime : $reqtime; 
		$this->_result    = $result;
		$this->_kind      = $kind;
		
		$sql  = " INSERT INTO " . $this->_sms_table . " (USERCODE, REQNAME, REQPHONE, CALLNAME, CALLPHONE, SUBJECT, MSG, REQTIME, RESULT, KIND) ";
		$sql .= " VALUES (";
		$sql .= "	'" . $this->_usercode  . "', ";
		$sql .= "   '" . $this->_reqname   . "', ";
		$sql .= "   '" . $this->_reqphone  . "', ";
		$sql .= "   '" . $this->_callname  . "', ";
		$sql .= "   '" . $this->_callphone . "', ";
		$sql .= "   '" . $this->_subject   . "', ";
		$sql .= "   '" . $this->_msg       . "', ";
		$sql .= "   '" . $this->_reqtime   . "', ";
		$sql .= "   '" . $this->_result    . "', ";
		$sql .= "   '" . $this->_kind      . "'); ";
		
		return $this->_query = $sql;
	}

}

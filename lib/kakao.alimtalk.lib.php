<?php
if (!defined('_GNUBOARD_')) exit;

/**
* 카카오 알림톡 발송을 관장하는 메인 클래스이다.
*
*/
class AlimTalk {

	// 슈어엠 API url
	var $service_url = 'https://api.surem.com/alimtalk/v2/json';
	// 슈어엠 제공 아이디
	var $usercode = 'onkorderstn';
	// 슈어엡 제공 회사코드
    var $deptcode = '4P-WX1-DY';
	// 발신프로필키
    var $yellowid_key;

	// 문자 재전송시 발신번호
	var $from = '15445462';
	
	var $button_set;
	
	var $messages;
	var $messagesArr;

    function __construct($uuid = 'orderstation')
    {
        switch ($uuid) {
        case 'hahaha':  // 하하하 발신프로필키
            $this->yellowid_key = '1383cdb086bf6c1f320fa63c64ebbfc5b57ce96c';
            break;
        case 'orderstation':    // 오더스테이션 발신프로필키
            //$this->yellowid_key = '1383cdb086bf6c1f320fa63c64ebbfc5b57ce96c';
            $this->yellowid_key = '2e8414a6a58431935aa1e44d91f3edc7e1e8e93c';
            break;
        case 'isbre':    // 이즈브레 발신프로필키
            //$this->yellowid_key = '1383cdb086bf6c1f320fa63c64ebbfc5b57ce96c';
            $this->yellowid_key = 'b96b0d8dbb6dd6421775b2a1b8076bbe2fb7c2d5';
            break;
        default:
            $this->yellowid_key = '2e8414a6a58431935aa1e44d91f3edc7e1e8e93c';
            break;
        }
    }

	/**
	 * 발송할 메시지를 생성한다.
	 * 
	 * $message_id    : 메시지 고유키 값. 미입력시 단보전송은 1이며, 동보전송은 1씩 증가
     * $to            : 국가번호 포함 전화번호. 예시) 821012345678
     * $text          : 요청할 템플릿 내용.
     * $code : 요청할 템플릿 코드명. 예시) onk_in_001
	 * $reserved_time : 예약 발송시 예약시간(12자리). 미입력시 즉시 전송. 예시) 201912310910 
	 * $re_send       : 알림톡 전송 실패시 문자 재전송 여부. 미입력시 N. (Y: $text 문구로 전송, R: $re_text 문구로 전송)
	 *
	 */
	function set_message($code, $to, $text, $re_send='Y', $reserved_time=null)
	{
		$this->message = array(
			//'message_id' => '1',
			'to' => $to,
			'text' => $text,
			'from' => $this->from,
			'template_code' => $code
		);

		if ($re_send == 'Y') {
			$re_text = $text;
		} else if ($re_send == 'R') {
			$re_text = $text;	// 데체 메시지를 미리 정의해서 조회 필요.
		}

		if ($re_send != 'N') {
			$this->message = array_merge($this->message, array(
				're_send' => $re_send,
				're_text' => $re_text			
			));
		}
		
		if ($reserved_time) {
			$this->message = array_merge($this->message, array(
				'reserved_time' => $reserved_time
			));
		}
		
		//$this->set_button($code);
		//$this->message = array_merge($this->message, array('buttons' => $this->button_set));

		$this->messagesArr = array($this->message);		
	}
	
	/**
	 * 카카오 알림톡을 발송한다.
	 *
	 */
	public function send()
	{
		$curl_post_data = array(
			'usercode' => $this->usercode,
			'deptcode' => $this->deptcode,
			'yellowid_key' => $this->yellowid_key,
			'messages' => $this->messagesArr
		);
	 
		$this->sys_log('send message: ', $this->messagesArr);
	 
		//$json_data = json_encode($curl_post_data, JSON_UNESCAPED_UNICODE);
		$json_data = json_encode($curl_post_data);
		
		$header = array(
			'Content-Type: application/json', 
			'Content-Length: ' . strlen($json_data)
		);
		
		$ch = curl_init($this->service_url);
		
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
		curl_setopt($ch, CURLOPT_URL, $this->service_url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($ch, CURLOPT_POST, 1);   
	 
		$response = curl_exec($ch);
	 
		if($response === FALSE){
			die(curl_error($ch));
		}
	 
		$responseData = json_decode($response, TRUE);

		//var_dump($responseData);
		//echo $responseData['code'];
		//echo $responseData['message'];
		//echo $responseData['results'];
		//echo $responseData['additional_information'];

		$this->sys_log('result message: ', $responseData);
		
		if ($responseData['code'] == '200')	// Success
			return true;
		else
			return false;
	}

    /**
     * 국가번호 포함 전화번호 형식으로 설정
     *
     */
    function set_phone_number($to)
    {
        $to = str_replace('-', '', $to);
        return  '82' . substr($to, 1, strlen($to));
    }

    /**
     * 버튼 정보(최대 5개 등록 가능)를 생성한다.
     * TODO: 사용할 경우에는 버튼 정보 추가 정의 필요
     */
    function set_button($code)
    {
        $buttons = array();

        switch ($code) {
            case 'onk_in_001':
                $buttons = array(
                    array(
                        'button_name' => '오더스테이션 이동',
                        'button_url' => 'http://orderstation.kr'
                    ),
                    array(
                        'button_name' => '오더스테이션 회원가입',
                        'button_url' => 'http://orderstation.kr/bbs/register.php'
                    )
                );
                break;
            case 'orderstation_member_01':
                $buttons = array(
                    array(
                        'button_name' => '오더스테이션 이동',
                        'button_url' => 'http://orderstation.kr'
                    )
                );
                break;
            default:
                $buttons = array(
                    array(
                        'button_name' => '오더스테이션 이동',
                        'button_url' => 'http://orderstation.kr'
                    )
                );
                break;
        }

        $this->button_set = $buttons;
    }

    /**
     * 템플릿 코드에 해당하는 템플릿 내용 조회
     *
     */
    function get_template($code)
    {
        $enter = chr(13) . chr(10);
        switch ($code) {
            // 오더스테이션 템플릿 -------------------------------------------------------------------------------------

            /*  회원관리 */
            // 회원가입
            case 'os_member_001':
                $template = '[오더스테이션]'.$enter.'안녕하세요, #{이름} 고객님!'.$enter.$enter.'회원가입 감사드립니다. (축하)'.$enter.'#{이름}님의 회원가입 정보 안내드립니다.'.$enter.$enter.
                    '- 오더스테이션 ID : #{아이디}'.$enter.$enter.'♥신규가입 감사 쿠폰♥'.$enter.'#{금액}원 지급완료 !'.$enter.'바로 마이페이지를 확인해보세요'.$enter.$enter.
                    '카카오톡 채널 친구추가를 하시면'.$enter.'다양한 프로모션 정보를 빠르게 받아보실 수 있습니다~!'.$enter.$enter.
                    '▶추가하기 :'.$enter.'http://pf.kakao.com/_EQxogC'.$enter.$enter.
                    '▶오더스테이션 바로가기'.$enter.'https://www.orderstation.co.kr/shop/'.$enter.'☎고객센터 1544-5462';
                break;

            // 인증번호
            case 'os_member_002':
                $template = '[오더스테이션]'.$enter.'휴대폰 인증번호 : #{인증번호}'.$enter.'위 번호를 인증창에 입력해주세요.';
                break;

            /* 1:1 건강상담 */
            // 1:1 건강상담 답변 요청(OS관리자)
            case 'os_qna_001':
                $template = '[오더스테이션]'.$enter.'약사님 안녕하세요.'.$enter.$enter.'약사님의 약국을 지정약국으로 선택한 #{이름} 고객님의 1:1건강상담 문의가 접수되었습니다.'.$enter.$enter.
                    '오더스테이션 방문하시어'.$enter.'답변 부탁드리겠습니다^^'.$enter.$enter.'감사합니다.'.$enter.$enter.
                    '▶오더스테이션 바로가기'.$enter.'https://www.orderstation.co.kr/shop/'.$enter.'☎고객센터 1544-5462';
                break;

            // 1:1 건강상담 답변 요청(약국)
            case 'os_qna_001_1':
                $template = '[오더스테이션]'.$enter.'약사님 안녕하세요.'.$enter.$enter.'약사님의 약국을 지정약국으로 선택한 #{이름} 고객님의 1:1건강상담 문의가 접수되었습니다.'.$enter.$enter.
                    '오더스테이션 방문하시어'.$enter.'답변 부탁드리겠습니다^^'.$enter.$enter.'감사합니다.'.$enter.$enter.
                    '▶오더스테이션 바로가기'.$enter.'https://www.orderstation.co.kr/shop/'.$enter.'☎고객센터 1544-5462';
                break;

            // 1:1 건강상담 답변완료(고객)
            case 'os_qna_002':
                $template = '[오더스테이션]'.$enter.'#{이름} 고객님, 안녕하세요.'.$enter.$enter.'등록해주신 1:1건강상담 문의에 대한 답변이 등록완료 되었습니다.'.$enter.$enter.
                    '오더스테이션 방문하시어'.$enter.'확인 부탁드리겠습니다^^'.$enter.$enter.'감사합니다.'.$enter.$enter.
                    '▶오더스테이션 바로가기'.$enter.'https://www.orderstation.co.kr/shop/'.$enter.'☎고객센터 1544-5462';
                break;

            /* 고객 문의 내역 */
            // 문의 답변 요청(OS관리자)
            case 'os_qna_003':
                $template = '[오더스테이션]'.$enter.'#{이름} 고객님의 문의가 접수되었습니다.'.$enter.'빠른 답변 부탁드리겠습니다.'.$enter.$enter.
                    '▶오더스테이션 바로가기'.$enter.'https://www.orderstation.co.kr/shop/';
                break;

            // 문의 답변 완료(고객, 약국)
            case 'os_qna_004':
                $template = '[오더스테이션]'.$enter.'#{이름} 고객님, 안녕하세요.'.$enter.$enter.'등록해주신 문의에 대한 답변이 등록완료 되었습니다.'.$enter.$enter.
                    '오더스테이션 방문하시어'.$enter.'확인 부탁드리겠습니다^^'.$enter.$enter.'감사합니다.'.$enter.$enter.
                    '▶오더스테이션 바로가기'.$enter.'https://www.orderstation.co.kr/shop/'.$enter.'☎고객센터 1544-5462';
                break;

            /* 쿠폰 */
            // 쿠폰 만료 임박(고객)
            case 'os_coupon_002':
                $template = '[오더스테이션]'.$enter.'#{이름} 고객님, 안녕하세요.'.$enter.$enter.
                    '고객님의'.$enter.'#{쿠폰명}'.$enter.'유효기간이 얼마 남지 않았습니다.'.$enter.$enter.
                    '※ 쿠폰안내 ※'.$enter.'- 쿠폰명 : #{쿠폰명}'.$enter.'- 사용종료일 : #{쿠폰유효기간}'.$enter.$enter.
                    '▶오더스테이션 바로가기'.$enter.'https://www.orderstation.co.kr/shop/'.$enter.'☎고객센터 1544-5462';
                break;

            // 상품 후기 등록(고객)
            case 'os_coupon_003':
                $template = '[오더스테이션]'.$enter.'#{이름} 고객님, 안녕하세요.'.$enter.$enter.'상품 후기 등록 정말 감사드립니다♥'.$enter.'감사한 마음을 담아'.$enter.'쿠폰을 전달드립니다 !'.$enter.$enter.
                    '※ 쿠폰안내 ※'.$enter.'- 쿠폰명 : 후기등록 감사 쿠폰'.$enter.'- 금액 : 1000원 할인'.$enter.'- 사용기한 : 지급 후 30일'.$enter.$enter.
                    '▶오더스테이션 바로가기'.$enter.'https://www.orderstation.co.kr/shop/'.$enter.'☎고객센터 1544-5462';
                break;

            /* 주문관리 */
            // 주문 완료(고객, 약국)
            case 'os_order_001':
                $template = '[오더스테이션]'.$enter.'#{이름} 고객님, 안녕하세요.'.$enter.$enter.'오더스테이션 주문이 완료되었습니다.'.$enter.$enter.
                    '- 날짜 : #{주문일시}'.$enter.'- 주문번호 : #{주문번호}'.$enter.'- 주문상품 : #{주문상품}'.$enter.'- 수령인 : #{수령인}'.$enter.$enter.
                    '주문해주셔서 감사드리며,'.$enter.'빠른 배송을 위해 최선을 다하겠습니다.'.$enter.$enter.
                    '▶오더스테이션 바로가기'.$enter.'https://www.orderstation.co.kr/shop/'.$enter.'☎고객센터 1544-5462';
                break;

            // 주문 완료(공급사)
            case 'os_order_002':
                $template = '[오더스테이션]'.$enter.'안녕하세요.'.$enter.'#{이름}님 주문이 완료되었습니다.'.$enter.$enter.
                    '오더스테이션 관리자 페이지에서 확인 후,'.$enter.'주문건 발송처리 부탁드리겠습니다.'.$enter.$enter.
                    '- 날짜 : #{주문일시}'.$enter.'- 주문번호 : #{주문번호}'.$enter.'- 주문상품 : #{주문상품}'.$enter.'- 수령인 : #{수령인}'.$enter.$enter.'감사합니다.'.$enter.$enter.
                    '▶오더스테이션 바로가기'.$enter.'https://www.orderstation.co.kr/shop/'.$enter.'☎고객센터 1544-5462';
                break;

            // 발송완료_일반주문(고객, 약국) orderformtransupdate.php
            case 'os_order_003':
                $template = '[오더스테이션]'.$enter.'#{이름} 고객님, 안녕하세요.'.$enter.$enter.'주문하신 상품이 발송되었습니다.'.$enter.$enter.
                    '- 주문번호 : #{주문번호}'.$enter.'- 상품명 : #{주문상품}'.$enter.'- 택배사 : #{택배사}'.$enter.'- 송장번호 : #{송장번호}'.$enter.$enter.
                    '공휴일 제외 1-2일 내에 배송될 예정이며,'.$enter.'택배사 사정에 따라 지연될 수 있습니다.'.$enter.$enter.
                    '상품을 받으신후 후기도 잊지말아주세요♥'.$enter.$enter.
                    '▶오더스테이션 바로가기'.$enter.'https://www.orderstation.co.kr/shop/'.$enter.'☎고객센터 1544-5462';
                break;

            // 발송완료_다중배송(고객, 약국) orderformtransupdate.php
            case 'os_order_003_1':
                $template = '[오더스테이션]'.$enter.'#{이름} 고객님, 안녕하세요.'.$enter.$enter.'주문하신 상품이 발송되었습니다.'.$enter.$enter.
                    '- 주문번호 : #{주문번호}'.$enter.'- 상품명 : #{주문상품}'.$enter.'- 수령인 : #{수령인}'.$enter.$enter.
                    '공휴일 제외 1-2일 내에 배송될 예정이며,'.$enter.'택배사 사정에 따라 지연될 수 있습니다.'.$enter.$enter.
                    '상품을 받으신후 후기도 잊지말아주세요♥'.$enter.$enter.
                    '▶오더스테이션 바로가기'.$enter.'https://www.orderstation.co.kr/shop/'.$enter.'☎고객센터 1544-5462';
                break;

            // 주문 취소건(고객)
            case 'os_order_004':
                $template = '[오더스테이션]'.$enter.'#{이름} 고객님, 안녕하세요.'.$enter.$enter.'주문상품이 정상적으로 취소되었습니다.'.$enter.$enter.
                    '- 주문번호 : #{주문번호}'.$enter.'- 상품명 : #{주문상품}'.$enter.$enter.'감사합니다.'.$enter.$enter.
                    '▶오더스테이션 바로가기'.$enter.'https://www.orderstation.co.kr/shop/'.$enter.'☎고객센터 1544-5462';
                break;

            // 약국주문(영업자)
            case 'os_order_005':
                $template = '[오더스테이션]'.$enter.'#{이름}님, 안녕하세요.'.$enter.$enter.'#{약국명}, #{고객명} 고객님의 주문입니다.'.$enter.$enter.
                    '- 주문번호 : #{주문번호}'.$enter.'- 상품명 : #{주문상품}'.$enter.$enter.
                    '▶오더스테이션 바로가기'.$enter.'https://www.orderstation.co.kr/shop/'.$enter.'☎고객센터 1544-5462';
                break;

            // 무통장입금주문(고객)
            case 'os_order_006':
                $template = '[오더스테이션]'.$enter.'#{이름} 고객님, 안녕하세요.'.$enter.$enter.'무통장입금 주문이 완료되었습니다.'.$enter.$enter.
                    '- 날짜 : #{주문일시}'.$enter.'- 주문번호 : #{주문번호}'.$enter.'- 주문상품 : #{주문상품}'.$enter.'- 입금계좌 : #{계좌번호}'.$enter.
                    '- 입금기한 : #{입금기한}'.$enter.'- 금액 : #{입금요청금액}'.$enter.$enter.
                    '입금기한내 미입금시 주문이 자동 취소 됩니다.'.$enter.'감사합니다.'.$enter.$enter.
                    '▶오더스테이션 바로가기'.$enter.'https://www.orderstation.co.kr/shop/'.$enter.'☎고객센터 1544-5462';
                break;

            // 무통장입금확인(고객)
            case 'os_order_007':
                $template = '[오더스테이션]'.$enter.'#{이름} 고객님, 안녕하세요.'.$enter.$enter.'무통장입금 주문이 입금확인되었습니다.'.$enter.$enter.
                    '- 날짜 : #{주문일시}'.$enter.'- 주문번호 : #{주문번호}'.$enter.'- 주문상품 : #{주문상품}'.$enter.$enter.
                    '주문해주셔서 감사드리며,'.$enter.'빠른 배송을 위해 최선을 다하겠습니다.'.$enter.$enter.
                    '▶오더스테이션 바로가기'.$enter.'https://www.orderstation.co.kr/shop/'.$enter.'☎고객센터 1544-5462';
                break;

            // 미입금취소건(고객)
            case 'os_order_008':
                $template = '[오더스테이션]'.$enter.'#{이름} 고객님, 안녕하세요.'.$enter.$enter.'무통장입금 주문이 기한내 미입금으로 인해 취소되었습니다.'.$enter.$enter.
                    '- 날짜 : #{주문일시}'.$enter.'- 주문번호 : #{주문번호}'.$enter.'- 주문상품 : #{주문상품}'.$enter.$enter.
                    '▶오더스테이션 바로가기'.$enter.'https://www.orderstation.co.kr/shop/'.$enter.'☎고객센터 1544-5462';
                break;

            // 무통장입금확인(공급사)
            case 'os_order_009':
                $template = '[오더스테이션]'.$enter.'안녕하세요.'.$enter.$enter.'#{고객명}님의'.$enter.'무통장입금 주문이 입금완료되었습니다.'.$enter.$enter.
                    '오더스테이션 관리자 페이지에서 확인 후,'.$enter.'주문건 발송처리 부탁드리겠습니다.'.$enter.$enter.
                    '- 날짜 : #{주문일시}'.$enter.'- 주문번호 : #{주문번호}'.$enter.'- 주문상품 : #{주문상품}'.$enter.'- 수령인 : #{수령인}'.$enter.$enter.
                    '감사합니다.'.$enter.$enter.'▶오더스테이션 바로가기'.$enter.'https://www.orderstation.co.kr/shop/'.$enter.'☎고객센터 1544-5462';
                break;


            // 하하하 템플릿 -------------------------------------------------------------------------------------------

            case 'crm_member_001':	// 약국 방문 고객에게 환영 메시지
                $template = '안녕하세요 #{이름} 고객님, #{약국명}의 건강 관리 대상 고객으로 등록되신 것을 환영합니다. 궁금하신 점이 있으면 약사님과 상의하시고 오늘도 건강한 하루 되세요!';
                break;
            case 'orderstation_member_01':	// 회원가입축하메시지
                $template = '안녕하세요 #{이름} 고객님, 오더스테이션 가입을 환영합니다! 오더스테이션만의 건강한 먹거리와, 다양한 건강정보를 약사님과의 1:1 상담과 함께 누려보세요!';
                break;
			// 클리노믹스 템플릿
			case 'clinomics_01':	// 회원가입축하메시지
                $template = '   #{이름}님 구매해주셔서 감사합니다.'.Chr(10).'신청해주신 제노피(유전자분석서비스)가 완료되었습니다.'.Chr(10).'지금 오더스테이션(www.orderstation.co.kr)에서 결과지 확인이 가능합니다.'.Chr(10).'구매하신 약국의 약사님과 결과지에 대해 상담해보세요.'.Chr(10).'[결과지 확인 바로가기]'.Chr(10).'#{결과지확인URL경로}';
                break;
			case 'clinomics_02':	// 회원가입축하메시지
                $template = '	#{이름}님 구매해주셔서 감사합니다.'.Chr(10).'신청해주신 제노비(장내 미생물 분석 서비스)가 완료되었습니다.'.Chr(10).'지금 오더스테이션(www.orderstation.co.kr)에서 결과지 확인이 가능합니다.'.Chr(10).'구매하신 약국의 약사님과 결과지에 대해 상담해보세요.'.Chr(10).'[결과지 확인 바로가기]'.Chr(10).'#{결과지확인URL경로}';
                break;

			// 이즈브레 템플릿 -------------------------------------------------------------------------------------------
            case 'isbre_002':	// 신규회원가입축하 (수신: 고객)
                $template = '[이즈브레몰] 안녕하세요 #{이름} 고객님, 이즈브레몰에 가입해주셔서 감사합니다. 신규 가입 감사 쿠폰 #{금액}원이 지급되었습니다. 마이페이지에서 꼭 확인하세요!';
                break;
            case 'isbre_006':	// 가입_휴대폰인증번호 (수신: 고객)
                $template = '[이즈브레몰] 휴대폰 인증번호 #{인증번호} 위 번호를 인증창에 입력하세요.';
                break;
            case 'isbre_008': // 고객문의답변요청 => 고객문의내역 (수신: OS관리자)
                $template = '[이즈브레몰] 관리자님, 고객 문의 게시판에 #{이름} 고객님의 문의가 접수되었습니다. 답변 부탁드립니다.';
                break;
            case 'isbre_007': // 고객문의답변완료 => 고객문의내역 (수신: 고객)
				$template = '[이즈브레몰] #{이름} 고객님, 고객문의에 대한 답변이 등록되었습니다. 감사합니다.'; 
                break;
            // 주문 관리
            case 'isbre_005':	// 주문완료-번호 => 주문완료시 (수신: 고객/약국/공급사)
                $template = '[이즈브레몰] #{이름} 고객님의 주문입니다. 주문번호는 #{주문번호}입니다.';
                break;
            case 'isbre_004':	// 발송완료-일반주문 => 일반배송 (수신: 고객)
                $template = '[이즈브레몰] #{이름} 고객님, #{상품명}이 #{배송지}로 발송되었습니다.';
                break;
            case 'isbre_003':	// 주문취소건 => 주문취소시 (수신: 고객)
                $template = '[이즈브레몰] #{이름} 고객님, 주문번호 #{주문번호}가 정상적으로 취소되었습니다.';
                break;
            case 'isbre_009':	// 주문취소건 => 주문취소시 (수신: 고객)
                $template = '[이즈브레몰] #{이름} 고객님. 고객님께서 구매하신 [#{상품명}] 의 주문이 정상적으로 완료 되었습니다. 주문번호는 #{주문번호}입니다. 이즈브레몰을 이용해주셔서 감사합니다.';
                break;


        }

        return $template;
    }
	
	// 로그 생성
	function sys_log($sys, $mess = '')
	{
		$dir = $_SERVER['DOCUMENT_ROOT'].'/logs/alimtalk/';
		//$dir = '/var/www/html/orderstation-shop-dev/logs/alimtalk/';
		$this->makedirs($dir, '0755');

		$filename = 'alimtalk_' . date('Ymd') . '.log';
		$fp = fopen($dir . $filename, "a+");
		if ($fp == null)
			return;

		if (is_array($mess)) {
			// $mess = serialize($mess);
			$mess = $this->_contextToString($mess);
		}

		// fwrite($fp, date("Y-m-d H:i:s") . " " . $sys . " " . $mess . "\n");
		fwrite($fp, date("Y-m-d H:i:s") . " " . $sys . "\n" . $mess . "\n");

		fclose($fp);
	}

	function _contextToString($context)
	{
		$export = '';
		foreach ($context as $key => $value) {
			$export .= "{$key}: ";
			$export .= preg_replace(array(
				'/=>\s+([a-zA-Z])/im',
				'/array\(\s+\)/im',
				'/^  |\G  /m'
			), array(
				'=> $1',
				'array()',
				'    '
			), str_replace('array (', 'array(', var_export($value, true)));
			$export .= PHP_EOL;
		}

		return str_replace(array(
			'\\\\',
			'\\\''
		), array(
			'\\',
			'\''
		), rtrim($export));
	}
	
	function makedirs($dirpath, $mode = 0777)
	{
		return is_dir($dirpath) || @mkdir($dirpath, $mode, true);
	}

}

<?php
if (!defined('_GNUBOARD_')) exit;

/*************************************************************************
**
**  일반 함수 모음
**
*************************************************************************/

// 마이크로 타임을 얻어 계산 형식으로 만듦
function get_microtime()
{
    list($usec, $sec) = explode(" ",microtime());
    return ((float)$usec + (float)$sec);
}


// 한페이지에 보여줄 행, 현재페이지, 총페이지수, URL
function get_paging($write_pages, $cur_page, $total_page, $url, $add="")
{
    //$url = preg_replace('#&amp;page=[0-9]*(&amp;page=)$#', '$1', $url);
    $url = preg_replace('#&amp;page=[0-9]*#', '', $url) . '&amp;page=';

    $str = '';
    if ($cur_page > 1) {
        $str .= '<a href="'.$url.'1'.$add.'" class="pg_page pg_start">처음</a>'.PHP_EOL;
    }

    $start_page = ( ( (int)( ($cur_page - 1 ) / $write_pages ) ) * $write_pages ) + 1;
    $end_page = $start_page + $write_pages - 1;

    if ($end_page >= $total_page) $end_page = $total_page;

    if ($start_page > 1) $str .= '<a href="'.$url.($start_page-1).$add.'" class="pg_page pg_prev">이전</a>'.PHP_EOL;

    if ($total_page > 1) {
        for ($k=$start_page;$k<=$end_page;$k++) {
            if ($cur_page != $k)
                $str .= '<a href="'.$url.$k.$add.'" class="pg_page">'.$k.'<span class="sound_only">페이지</span></a>'.PHP_EOL;
            else
                $str .= '<span class="sound_only">열린</span><strong class="pg_current">'.$k.'</strong><span class="sound_only">페이지</span>'.PHP_EOL;
        }
    }

    if ($total_page > $end_page) $str .= '<a href="'.$url.($end_page+1).$add.'" class="pg_page pg_next">다음</a>'.PHP_EOL;

    if ($cur_page < $total_page) {
        $str .= '<a href="'.$url.$total_page.$add.'" class="pg_page pg_end">맨끝</a>'.PHP_EOL;
    }

    if ($str)
        return "<nav class=\"pg_wrap\"><span class=\"pg\">{$str}</span></nav>";
    else
        return "";
}

// 페이징 코드의 <nav><span> 태그 다음에 코드를 삽입
function page_insertbefore($paging_html, $insert_html)
{
    if(!$paging_html)
        $paging_html = '<nav class="pg_wrap"><span class="pg"></span></nav>';

    return preg_replace("/^(<nav[^>]+><span[^>]+>)/", '$1'.$insert_html.PHP_EOL, $paging_html);
}

// 페이징 코드의 </span></nav> 태그 이전에 코드를 삽입
function page_insertafter($paging_html, $insert_html)
{
    if(!$paging_html)
        $paging_html = '<nav class="pg_wrap"><span class="pg"></span></nav>';

    if(preg_match("#".PHP_EOL."</span></nav>#", $paging_html))
        $php_eol = '';
    else
        $php_eol = PHP_EOL;

    return preg_replace("#(</span></nav>)$#", $php_eol.$insert_html.'$1', $paging_html);
}

// 변수 또는 배열의 이름과 값을 얻어냄. print_r() 함수의 변형
function print_r2($var)
{
    ob_start();
    print_r($var);
    $str = ob_get_contents();
    ob_end_clean();
    $str = str_replace(" ", "&nbsp;", $str);
    echo nl2br("<span style='font-family:Tahoma, 굴림; font-size:9pt;'>$str</span>");
}

function debug($var, $type=false)
{
    $chk_ip = array('59.6.135.109');

    if (in_array($_SERVER['REMOTE_ADDR'], $chk_ip)) {
        ob_start();
        print_r($var);
        $str = ob_get_contents();
        ob_end_clean();
        $str = str_replace(" ", "&nbsp;", $str);

        echo '<div style="background-color: #262626; color: white;">';
        echo "!!!!! DEBUG START (O&K 사무실에서만 보입니다. - 디지털전략부)!!!!!<br>";
        echo nl2br("<span style='font-family:Tahoma, 굴림; font-size:9pt;'>$str</span><br>");
        echo "!!!!! DEBUG END !!!!!";
        echo '</div>';

        if ($type == true) {
            exit;
        }
    }
}

function debug2($var, $type=false)
{
    ob_start();
    print_r($var);
    $str = ob_get_contents();
    ob_end_clean();
    $str = str_replace(" ", "&nbsp;", $str);

    echo '<div style="background-color: #262626; color: white;width: auto;;">';
    echo nl2br("<span style='font-family:Tahoma, 굴림; font-size:9pt;'>$str</span>");
    echo '</div>';

    if ($type == true) {
        exit;
    }
}

function ip_chk() {

    $chk_ip = array('59.6.135.109');

    if (in_array($_SERVER['REMOTE_ADDR'], $chk_ip)) {
        return true;
    } else {
        return false;
    }

}

// 메타태그를 이용한 URL 이동
// header("location:URL") 을 대체
function goto_url($url)
{
    $url = str_replace("&amp;", "&", $url);
    //echo "<script> location.replace('$url'); </script>";

    if (!headers_sent())
        header('Location: '.$url);
    else {
        echo '<script>';
        echo 'location.replace("'.$url.'");';
        echo '</script>';
        echo '<noscript>';
        echo '<meta http-equiv="refresh" content="0;url='.$url.'" />';
        echo '</noscript>';
    }
    exit;
}


// 세션변수 생성
function set_session($session_name, $value)
{
    static $check_cookie = null;

    if( $check_cookie === null ){
        $cookie_session_name = session_name();
        if( ! ($cookie_session_name && isset($_COOKIE[$cookie_session_name]) && $_COOKIE[$cookie_session_name]) && ! headers_sent() ){
            @session_regenerate_id(false);
        }

        $check_cookie = 1;
    }

    if (PHP_VERSION < '5.3.0')
        session_register($session_name);
    // PHP 버전별 차이를 없애기 위한 방법
    $$session_name = $_SESSION[$session_name] = $value;
}


// 세션변수값 얻음
function get_session($session_name)
{
    return isset($_SESSION[$session_name]) ? $_SESSION[$session_name] : '';
}


// 쿠키변수 생성
function set_cookie($cookie_name, $value, $expire)
{
    global $g5;

    setcookie(md5($cookie_name), base64_encode($value), G5_SERVER_TIME + $expire, '/', G5_COOKIE_DOMAIN);
}


// 쿠키변수값 얻음
function get_cookie($cookie_name)
{
    $cookie = md5($cookie_name);
    if (array_key_exists($cookie, $_COOKIE))
        return base64_decode($_COOKIE[$cookie]);
    else
        return "";
}


// 경고메세지를 경고창으로
function alert($msg='', $url='', $error=true, $post=false)
{
    global $g5, $config, $member;
    global $is_admin;

    $msg = $msg ? strip_tags($msg, '<br>') : '올바른 방법으로 이용해 주십시오.';

    $header = '';
    if (isset($g5['title'])) {
        $header = $g5['title'];
    }
    include_once(G5_BBS_PATH.'/alert.php');
    exit;
}


// 경고메세지 출력후 창을 닫음
function alert_close($msg, $error=true)
{
    global $g5;
    
    $msg = strip_tags($msg, '<br>');

    $header = '';
    if (isset($g5['title'])) {
        $header = $g5['title'];
    }
    include_once(G5_BBS_PATH.'/alert_close.php');
    exit;
}

// confirm 창
function confirm($msg, $url1='', $url2='', $url3='')
{
    global $g5;

    if (!$msg) {
        $msg = '올바른 방법으로 이용해 주십시오.';
        alert($msg);
    }

    if(!trim($url1) || !trim($url2)) {
        $msg = '$url1 과 $url2 를 지정해 주세요.';
        alert($msg);
    }

    if (!$url3) $url3 = clean_xss_tags($_SERVER['HTTP_REFERER']);

    $msg = str_replace("\\n", "<br>", $msg);

    $header = '';
    if (isset($g5['title'])) {
        $header = $g5['title'];
    }
    include_once(G5_BBS_PATH.'/confirm.php');
    exit;
}


// way.co.kr 의 wayboard 참고
function url_auto_link($str)
{
    global $g5;
    global $config;

    // 140326 유창화님 제안코드로 수정
    // http://sir.kr/pg_lecture/461
    // http://sir.kr/pg_lecture/463
    $attr_nofollow = (function_exists('check_html_link_nofollow') && check_html_link_nofollow('url_auto_link')) ? ' rel="nofollow"' : '';
    $str = str_replace(array("&lt;", "&gt;", "&amp;", "&quot;", "&nbsp;", "&#039;"), array("\t_lt_\t", "\t_gt_\t", "&", "\"", "\t_nbsp_\t", "'"), $str);
    //$str = preg_replace("`(?:(?:(?:href|src)\s*=\s*(?:\"|'|)){0})((http|https|ftp|telnet|news|mms)://[^\"'\s()]+)`", "<A HREF=\"\\1\" TARGET='{$config['cf_link_target']}'>\\1</A>", $str);
    $str = preg_replace("/([^(href=\"?'?)|(src=\"?'?)]|\(|^)((http|https|ftp|telnet|news|mms):\/\/[a-zA-Z0-9\.-]+\.[가-힣\xA1-\xFEa-zA-Z0-9\.:&#!=_\?\/~\+%@;\-\|\,\(\)]+)/i", "\\1<A HREF=\"\\2\" TARGET=\"{$config['cf_link_target']}\" $attr_nofollow>\\2</A>", $str);
    $str = preg_replace("/(^|[\"'\s(])(www\.[^\"'\s()]+)/i", "\\1<A HREF=\"http://\\2\" TARGET=\"{$config['cf_link_target']}\" $attr_nofollow>\\2</A>", $str);
    $str = preg_replace("/[0-9a-z_-]+@[a-z0-9._-]{4,}/i", "<a href=\"mailto:\\0\" $attr_nofollow>\\0</a>", $str);
    $str = str_replace(array("\t_nbsp_\t", "\t_lt_\t", "\t_gt_\t", "'"), array("&nbsp;", "&lt;", "&gt;", "&#039;"), $str);

    /*
    // 속도 향상 031011
    $str = preg_replace("/&lt;/", "\t_lt_\t", $str);
    $str = preg_replace("/&gt;/", "\t_gt_\t", $str);
    $str = preg_replace("/&amp;/", "&", $str);
    $str = preg_replace("/&quot;/", "\"", $str);
    $str = preg_replace("/&nbsp;/", "\t_nbsp_\t", $str);
    $str = preg_replace("/([^(http:\/\/)]|\(|^)(www\.[^[:space:]]+)/i", "\\1<A HREF=\"http://\\2\" TARGET='{$config['cf_link_target']}'>\\2</A>", $str);
    //$str = preg_replace("/([^(HREF=\"?'?)|(SRC=\"?'?)]|\(|^)((http|https|ftp|telnet|news|mms):\/\/[a-zA-Z0-9\.-]+\.[\xA1-\xFEa-zA-Z0-9\.:&#=_\?\/~\+%@;\-\|\,]+)/i", "\\1<A HREF=\"\\2\" TARGET='$config['cf_link_target']'>\\2</A>", $str);
    // 100825 : () 추가
    // 120315 : CHARSET 에 따라 링크시 글자 잘림 현상이 있어 수정
    $str = preg_replace("/([^(HREF=\"?'?)|(SRC=\"?'?)]|\(|^)((http|https|ftp|telnet|news|mms):\/\/[a-zA-Z0-9\.-]+\.[가-힣\xA1-\xFEa-zA-Z0-9\.:&#=_\?\/~\+%@;\-\|\,\(\)]+)/i", "\\1<A HREF=\"\\2\" TARGET='{$config['cf_link_target']}'>\\2</A>", $str);

    // 이메일 정규표현식 수정 061004
    //$str = preg_replace("/(([a-z0-9_]|\-|\.)+@([^[:space:]]*)([[:alnum:]-]))/i", "<a href='mailto:\\1'>\\1</a>", $str);
    $str = preg_replace("/([0-9a-z]([-_\.]?[0-9a-z])*@[0-9a-z]([-_\.]?[0-9a-z])*\.[a-z]{2,4})/i", "<a href='mailto:\\1'>\\1</a>", $str);
    $str = preg_replace("/\t_nbsp_\t/", "&nbsp;" , $str);
    $str = preg_replace("/\t_lt_\t/", "&lt;", $str);
    $str = preg_replace("/\t_gt_\t/", "&gt;", $str);
    */

    return $str;
}


// url에 http:// 를 붙인다
function set_http($url)
{
    if (!trim($url)) return;

    if (!preg_match("/^(http|https|ftp|telnet|news|mms)\:\/\//i", $url))
        $url = "http://" . $url;

    return $url;
}


// 파일의 용량을 구한다.
//function get_filesize($file)
function get_filesize($size)
{
    //$size = @filesize(addslashes($file));
    if ($size >= 1048576) {
        $size = number_format($size/1048576, 1) . "M";
    } else if ($size >= 1024) {
        $size = number_format($size/1024, 1) . "K";
    } else {
        $size = number_format($size, 0) . "byte";
    }
    return $size;
}


// 게시글에 첨부된 파일을 얻는다. (배열로 반환)
function get_file($bo_table, $wr_id)
{
    global $g5, $qstr;

    $file['count'] = 0;
    $sql = " select * from {$g5['board_file_table']} where bo_table = '$bo_table' and wr_id = '$wr_id' order by bf_no ";
    $result = sql_query($sql);
    while ($row = sql_fetch_array($result))
    {
        $no = $row['bf_no'];
        $file[$no]['href'] = G5_BBS_URL."/download.php?bo_table=$bo_table&amp;wr_id=$wr_id&amp;no=$no" . $qstr;
        $file[$no]['download'] = $row['bf_download'];
        // 4.00.11 - 파일 path 추가
        $file[$no]['path'] = G5_DATA_URL.'/file/'.$bo_table;
        $file[$no]['size'] = get_filesize($row['bf_filesize']);
        $file[$no]['datetime'] = $row['bf_datetime'];
        $file[$no]['source'] = addslashes($row['bf_source']);
        $file[$no]['bf_content'] = $row['bf_content'];
        $file[$no]['content'] = get_text($row['bf_content']);
        //$file[$no]['view'] = view_file_link($row['bf_file'], $file[$no]['content']);
        $file[$no]['view'] = view_file_link($row['bf_file'], $row['bf_width'], $row['bf_height'], $file[$no]['content']);
        $file[$no]['file'] = $row['bf_file'];
        $file[$no]['image_width'] = $row['bf_width'] ? $row['bf_width'] : 640;
        $file[$no]['image_height'] = $row['bf_height'] ? $row['bf_height'] : 480;
        $file[$no]['image_type'] = $row['bf_type'];
        $file['count']++;
    }

    return $file;
}


// 폴더의 용량 ($dir는 / 없이 넘기세요)
function get_dirsize($dir)
{
    $size = 0;
    $d = dir($dir);
    while ($entry = $d->read()) {
        if ($entry != '.' && $entry != '..') {
            $size += filesize($dir.'/'.$entry);
        }
    }
    $d->close();
    return $size;
}


/*************************************************************************
**
**  그누보드 관련 함수 모음
**
*************************************************************************/


// 게시물 정보($write_row)를 출력하기 위하여 $list로 가공된 정보를 복사 및 가공
function get_list($write_row, $board, $skin_url, $subject_len=40 ,$allsee='')
{
    global $g5, $config;
    global $qstr, $page;

    //$t = get_microtime();

    // 배열전체를 복사
    $list = $write_row;
    unset($write_row);

	if ($allsee == 1){
		$board_type = $list['bo_table']; //전체보기 일때 
	} else {
		$board_type = $board['bo_table'];
	}

    $board_notice = array_map('trim', explode(',', $board['bo_notice']));
    $list['is_notice'] = in_array($list['wr_id'], $board_notice);

    if ($subject_len)
        $list['subject'] = conv_subject($list['wr_subject'], $subject_len, '…');
    else
        $list['subject'] = conv_subject($list['wr_subject'], $board['bo_subject_len'], '…');

    // 목록에서 내용 미리보기 사용한 게시판만 내용을 변환함 (속도 향상) : kkal3(커피)님께서 알려주셨습니다.
    if ($board['bo_use_list_content'])
	{
		$html = 0;
		if (strstr($list['wr_option'], 'html1'))
			$html = 1;
		else if (strstr($list['wr_option'], 'html2'))
			$html = 2;

        $list['content'] = conv_content($list['wr_content'], $html);
	}

    $list['comment_cnt'] = '';
    if ($list['wr_comment'])
        $list['comment_cnt'] = "<span class=\"cnt_cmt\">".$list['wr_comment']."</span>";

    // 당일인 경우 시간으로 표시함
    $list['datetime'] = substr($list['wr_datetime'],0,10);
    $list['datetime2'] = $list['wr_datetime'];
    if ($list['datetime'] == G5_TIME_YMD)
        $list['datetime2'] = substr($list['datetime2'],11,5);
    else
        $list['datetime2'] = substr($list['datetime2'],5,5);
    // 4.1
    $list['last'] = substr($list['wr_last'],0,10);
    $list['last2'] = $list['wr_last'];
    if ($list['last'] == G5_TIME_YMD)
        $list['last2'] = substr($list['last2'],11,5);
    else
        $list['last2'] = substr($list['last2'],5,5);

    $list['wr_homepage'] = get_text($list['wr_homepage']);

    $tmp_name = get_text(cut_str($list['wr_name'], $config['cf_cut_name'])); // 설정된 자리수 만큼만 이름 출력
    $tmp_name2 = cut_str($list['wr_name'], $config['cf_cut_name']); // 설정된 자리수 만큼만 이름 출력

	/* 공지사항 , 건강지킴이 제외 이름노출시 * 표 */
	if( $board['bo_table'] != "notice" && $board['bo_table'] != ("allsee" || "health" || "healthrecipe" || "gallery")){
		$tmp_name = CutStringInfo($tmp_name,1);	
		$tmp_name2 = CutStringInfo($tmp_name2,1);
	}

    if ($board['bo_use_sideview'])
        $list['name'] = get_sideview($list['mb_id'], $tmp_name2, $list['wr_email'], $list['wr_homepage']);
    else
        $list['name'] = '<span class="'.($list['mb_id']?'sv_member':'sv_guest').'">'.$tmp_name.'</span>';

    $reply = $list['wr_reply'];

    $list['reply'] = strlen($reply)*20;

    $list['icon_reply'] = '';
    if ($list['reply'])
        $list['icon_reply'] = '<img src="'.$skin_url.'/img/icon_reply.gif" class="icon_reply" alt="답변글">';

    $list['icon_link'] = '';
    if ($list['wr_link1'] || $list['wr_link2'])
        $list['icon_link'] = '<i class="fa fa-link" aria-hidden="true"></i> ';

    // 분류명 링크
    $list['ca_name_href'] = G5_BBS_URL.'/board.php?bo_table='.$board_type.'&amp;sca='.urlencode($list['ca_name']);

    $list['href'] = G5_BBS_URL.'/board.php?bo_table='.$board_type.'&amp;wr_id='.$list['wr_id'].$qstr;
    $list['comment_href'] = $list['href'];

    $list['icon_new'] = '';
    if ($board['bo_new'] && $list['wr_datetime'] >= date("Y-m-d H:i:s", G5_SERVER_TIME - ($board['bo_new'] * 3600)))
        $list['icon_new'] = '<img src="'.$skin_url.'/img/icon_new.gif" class="title_icon" alt="새글"> ';

    $list['icon_hot'] = '';
    if ($board['bo_hot'] && $list['wr_hit'] >= $board['bo_hot'])
        $list['icon_hot'] = '<i class="fa fa-heart" aria-hidden="true"></i> ';

    $list['icon_secret'] = '';
    if (strstr($list['wr_option'], 'secret'))
        $list['icon_secret'] = '<i class="fa fa-lock" aria-hidden="true"></i> ';

    // 링크
    for ($i=1; $i<=G5_LINK_COUNT; $i++) {
        $list['link'][$i] = set_http(get_text($list["wr_link{$i}"]));
        $list['link_href'][$i] = G5_BBS_URL.'/link.php?bo_table='.$board['bo_table'].'&amp;wr_id='.$list['wr_id'].'&amp;no='.$i.$qstr;
        $list['link_hit'][$i] = (int)$list["wr_link{$i}_hit"];
    }

    // 가변 파일
    if ($board['bo_use_list_file'] || ($list['wr_file'] && $subject_len == 255) /* view 인 경우 */) {
        $list['file'] = get_file($board['bo_table'], $list['wr_id']);
    } else {
        $list['file']['count'] = $list['wr_file'];
    }

    if ($list['file']['count'])
        $list['icon_file'] = '<i class="fa fa-download" aria-hidden="true"></i> ';

    return $list;
}

// get_list 의 alias
function get_view($write_row, $board, $skin_url)
{
    return get_list($write_row, $board, $skin_url, 255);
}


// set_search_font(), get_search_font() 함수를 search_font() 함수로 대체
function search_font($stx, $str)
{
    global $config;

    // 문자앞에 \ 를 붙입니다.
    $src = array('/', '|');
    $dst = array('\/', '\|');

    if (!trim($stx) && $stx !== '0') return $str;

    // 검색어 전체를 공란으로 나눈다
    $s = explode(' ', $stx);

    // "/(검색1|검색2)/i" 와 같은 패턴을 만듬
    $pattern = '';
    $bar = '';
    for ($m=0; $m<count($s); $m++) {
        if (trim($s[$m]) == '') continue;
        // 태그는 포함하지 않아야 하는데 잘 안되는군. ㅡㅡa
        //$pattern .= $bar . '([^<])(' . quotemeta($s[$m]) . ')';
        //$pattern .= $bar . quotemeta($s[$m]);
        //$pattern .= $bar . str_replace("/", "\/", quotemeta($s[$m]));
        $tmp_str = quotemeta($s[$m]);
        $tmp_str = str_replace($src, $dst, $tmp_str);
        $pattern .= $bar . $tmp_str . "(?![^<]*>)";
        $bar = "|";
    }

    // 지정된 검색 폰트의 색상, 배경색상으로 대체
    $replace = "<b class=\"sch_word\">\\1</b>";

    return preg_replace("/($pattern)/i", $replace, $str);
}


// 제목을 변환
function conv_subject($subject, $len, $suffix='')
{
    return get_text(cut_str($subject, $len, $suffix));
}

// 내용을 변환
function conv_content($content, $html, $filter=true)
{
    global $config, $board;

    if ($html)
    {
        $source = array();
        $target = array();

        $source[] = "//";
        $target[] = "";

        if ($html == 2) { // 자동 줄바꿈
            $source[] = "/\n/";
            $target[] = "<br/>";
        }

        // 테이블 태그의 개수를 세어 테이블이 깨지지 않도록 한다.
        $table_begin_count = substr_count(strtolower($content), "<table");
        $table_end_count = substr_count(strtolower($content), "</table");
        for ($i=$table_end_count; $i<$table_begin_count; $i++)
        {
            $content .= "</table>";
        }

        $content = preg_replace($source, $target, $content);

        if($filter)
            $content = html_purifier($content);
    }
    else // text 이면
    {
        // & 처리 : &amp; &nbsp; 등의 코드를 정상 출력함
        $content = html_symbol($content);

        // 공백 처리
		//$content = preg_replace("/  /", "&nbsp; ", $content);
		$content = str_replace("  ", "&nbsp; ", $content);
		$content = str_replace("\n ", "\n&nbsp;", $content);

        $content = get_text($content, 1);
        $content = url_auto_link($content);
    }

    return $content;
}

function check_html_link_nofollow($type=''){
    return true;
}

// http://htmlpurifier.org/
// Standards-Compliant HTML Filtering
// Safe  : HTML Purifier defeats XSS with an audited whitelist
// Clean : HTML Purifier ensures standards-compliant output
// Open  : HTML Purifier is open-source and highly customizable
function html_purifier($html)
{
    $f = file(G5_PLUGIN_PATH.'/htmlpurifier/safeiframe.txt');
    $domains = array();
    foreach($f as $domain){
        // 첫행이 # 이면 주석 처리
        if (!preg_match("/^#/", $domain)) {
            $domain = trim($domain);
            if ($domain)
                array_push($domains, $domain);
        }
    }
    // 내 도메인도 추가
    array_push($domains, $_SERVER['HTTP_HOST'].'/');
    $safeiframe = implode('|', $domains);

    include_once(G5_PLUGIN_PATH.'/htmlpurifier/HTMLPurifier.standalone.php');
    include_once(G5_PLUGIN_PATH.'/htmlpurifier/extend.video.php');
    $config = HTMLPurifier_Config::createDefault();
    // data/cache 디렉토리에 CSS, HTML, URI 디렉토리 등을 만든다.
    $config->set('Cache.SerializerPath', G5_DATA_PATH.'/cache');
    $config->set('HTML.SafeEmbed', false);
    $config->set('HTML.SafeObject', false);
    $config->set('Output.FlashCompat', false);
    $config->set('HTML.SafeIframe', true);
    if( (function_exists('check_html_link_nofollow') && check_html_link_nofollow('html_purifier')) ){
        $config->set('HTML.Nofollow', true);    // rel=nofollow 으로 스팸유입을 줄임
    }
    $config->set('URI.SafeIframeRegexp','%^(https?:)?//('.$safeiframe.')%');
    $config->set('Attr.AllowedFrameTargets', array('_blank'));
    //유튜브, 비메오 전체화면 가능하게 하기
    $config->set('Filter.Custom', array(new HTMLPurifier_Filter_Iframevideo()));
    $purifier = new HTMLPurifier($config);
    return $purifier->purify($html);
}


// 검색 구문을 얻는다.
function get_sql_search($search_ca_name, $search_field, $search_text, $search_operator='and')
{
    global $g5;

    $str = "";
    if ($search_ca_name)
        $str = " ca_name = '$search_ca_name' ";

    $search_text = strip_tags(($search_text));
    $search_text = trim(stripslashes($search_text));

    if (!$search_text && $search_text !== '0') {
        if ($search_ca_name) {
            return $str;
        } else {
            return '0';
        }
    }

    if ($str)
        $str .= " and ";

    // 쿼리의 속도를 높이기 위하여 ( ) 는 최소화 한다.
    $op1 = "";

    // 검색어를 구분자로 나눈다. 여기서는 공백
    $s = array();
    $s = explode(" ", $search_text);

    // 검색필드를 구분자로 나눈다. 여기서는 +
    $tmp = array();
    $tmp = explode(",", trim($search_field));
    $field = explode("||", $tmp[0]);
    $not_comment = "";
    if (!empty($tmp[1]))
        $not_comment = $tmp[1];

    $str .= "(";
    for ($i=0; $i<count($s); $i++) {
        // 검색어
        $search_str = trim($s[$i]);
        if ($search_str == "") continue;

        // 인기검색어
        insert_popular($field, $search_str);

        $str .= $op1;
        $str .= "(";

        $op2 = "";
        for ($k=0; $k<count($field); $k++) { // 필드의 수만큼 다중 필드 검색 가능 (필드1+필드2...)

            // SQL Injection 방지
            // 필드값에 a-z A-Z 0-9 _ , | 이외의 값이 있다면 검색필드를 wr_subject 로 설정한다.
            $field[$k] = preg_match("/^[\w\,\|]+$/", $field[$k]) ? strtolower($field[$k]) : "wr_subject";

            $str .= $op2;
            switch ($field[$k]) {
                case "mb_id" :
                case "wr_name" :
                    $str .= " $field[$k] = '$s[$i]' ";
                    break;
                case "wr_hit" :
                case "wr_good" :
                case "wr_nogood" :
                    $str .= " $field[$k] >= '$s[$i]' ";
                    break;
                // 번호는 해당 검색어에 -1 을 곱함
                case "wr_num" :
                    $str .= "$field[$k] = ".((-1)*$s[$i]);
                    break;
                case "wr_ip" :
                case "wr_password" :
                    $str .= "1=0"; // 항상 거짓
                    break;
                // LIKE 보다 INSTR 속도가 빠름
                default :
                    if (preg_match("/[a-zA-Z]/", $search_str))
                        $str .= "INSTR(LOWER($field[$k]), LOWER('$search_str'))";
                    else
                        $str .= "INSTR($field[$k], '$search_str')";
                    break;
            }
            $op2 = " or ";
        }
        $str .= ")";

        $op1 = " $search_operator ";
    }
    $str .= " ) ";
    if ($not_comment)
        $str .= " and wr_is_comment = '0' ";

    return $str;
}


// 게시판 테이블에서 하나의 행을 읽음
function get_write($write_table, $wr_id)
{
    return sql_fetch(" select * from $write_table where wr_id = '$wr_id' ");
}


// 게시판의 다음글 번호를 얻는다.
function get_next_num($table)
{
    // 가장 작은 번호를 얻어
    $sql = " select min(wr_num) as min_wr_num from $table ";
    $row = sql_fetch($sql);
    // 가장 작은 번호에 1을 빼서 넘겨줌
    return (int)($row['min_wr_num'] - 1);
}


// 그룹 설정 테이블에서 하나의 행을 읽음
function get_group($gr_id)
{
    global $g5;

    return sql_fetch(" select * from {$g5['group_table']} where gr_id = '$gr_id' ");
}


// 회원 정보를 얻는다.
function get_member($mb_id, $fields='*')
{
    global $g5;

	$sql = " 
		select $fields , 
			ifnull( ( select pharm_control from {$g5['member_table']} where mb_id = mt.mb_recommend limit 1 ) , 'N' ) as choice_recommend_pharm_control,
			ifnull( ( select pharm_name from {$g5['member_table']} where mb_id = mt.mb_recommend limit 1 ) , 'N' ) as choice_recommend_pharm_name,
			ifnull( ( select mb_name from {$g5['member_table']} where mb_id = mt.mb_recommend limit 1 ) , 'N' ) as choice_recommend_name
		from {$g5['member_table']} mt where mb_id = TRIM('$mb_id') ";

    return sql_fetch( $sql );
}


// 날짜, 조회수의 경우 높은 순서대로 보여져야 하므로 $flag 를 추가
// $flag : asc 낮은 순서 , desc 높은 순서
// 제목별로 컬럼 정렬하는 QUERY STRING
function subject_sort_link($col, $query_string='', $flag='asc')
{
    global $sst, $sod, $sfl, $stx, $page, $sca;

    $q1 = "sst=$col";
    if ($flag == 'asc')
    {
        $q2 = 'sod=asc';
        if ($sst == $col)
        {
            if ($sod == 'asc')
            {
                $q2 = 'sod=desc';
            }
        }
    }
    else
    {
        $q2 = 'sod=desc';
        if ($sst == $col)
        {
            if ($sod == 'desc')
            {
                $q2 = 'sod=asc';
            }
        }
    }

    $arr_query = array();
    $arr_query[] = $query_string;
    $arr_query[] = $q1;
    $arr_query[] = $q2;
    $arr_query[] = 'sfl='.$sfl;
    $arr_query[] = 'stx='.$stx;
    $arr_query[] = 'sca='.$sca;
    $arr_query[] = 'page='.$page;
    $qstr = implode("&amp;", $arr_query);

    return "<a href=\"{$_SERVER['SCRIPT_NAME']}?{$qstr}\">";
}


// 관리자 정보를 얻음
function get_admin($admin='super', $fields='*')
{
    global $config, $group, $board;
    global $g5;

    $is = false;
    if ($admin == 'board') {
        $mb = sql_fetch("select {$fields} from {$g5['member_table']} where mb_id in ('{$board['bo_admin']}') limit 1 ");
        $is = true;
    }

    if (($is && !$mb['mb_id']) || $admin == 'group') {
        $mb = sql_fetch("select {$fields} from {$g5['member_table']} where mb_id in ('{$group['gr_admin']}') limit 1 ");
        $is = true;
    }

    if (($is && !$mb['mb_id']) || $admin == 'super') {
        $mb = sql_fetch("select {$fields} from {$g5['member_table']} where mb_id in ('{$config['cf_admin']}') limit 1 ");
    }

    return $mb;
}


// 관리자인가?
function is_admin($mb_id)
{
    global $config, $group, $board;

    if (!$mb_id) return;

    if ($config['cf_admin'] == $mb_id) return 'super';
    if (isset($group['gr_admin']) && ($group['gr_admin'] == $mb_id)) return 'group';
    if (isset($board['bo_admin']) && ($board['bo_admin'] == $mb_id)) return 'board';
    return '';
}


// 분류 옵션을 얻음
// 4.00 에서는 카테고리 테이블을 없애고 보드테이블에 있는 내용으로 대체
function get_category_option($bo_table='', $ca_name='')
{
    global $g5, $board, $is_admin;

    $categories = explode("|", $board['bo_category_list'].($is_admin?"|공지":"")); // 구분자가 | 로 되어 있음
    $str = "";
    for ($i=0; $i<count($categories); $i++) {
        $category = trim($categories[$i]);
        if (!$category) continue;

        $str .= "<option value=\"$categories[$i]\"";
        if ($category == $ca_name) {
            $str .= ' selected="selected"';
        }
        $str .= ">$categories[$i]</option>\n";
    }

    return $str;
}


// 게시판 그룹을 SELECT 형식으로 얻음
function get_group_select($name, $selected='', $event='')
{
    global $g5, $is_admin, $member;

    $sql = " select gr_id, gr_subject from {$g5['group_table']} a ";
    if ($is_admin == "group") {
        $sql .= " left join {$g5['member_table']} b on (b.mb_id = a.gr_admin)
                  where b.mb_id = '{$member['mb_id']}' ";
    }
    $sql .= " order by a.gr_id ";

    $result = sql_query($sql);
    $str = "<select id=\"$name\" name=\"$name\" $event>\n";
    for ($i=0; $row=sql_fetch_array($result); $i++) {
        if ($i == 0) $str .= "<option value=\"\">선택</option>";
        $str .= option_selected($row['gr_id'], $selected, $row['gr_subject']);
    }
    $str .= "</select>";
    return $str;
}


function option_selected($value, $selected, $text='')
{
    if (!$text) $text = $value;
    if ($value == $selected)
        return "<option value=\"$value\" selected=\"selected\">$text</option>\n";
    else
        return "<option value=\"$value\">$text</option>\n";
}


// '예', '아니오'를 SELECT 형식으로 얻음
function get_yn_select($name, $selected='1', $event='')
{
    $str = "<select name=\"$name\" $event>\n";
    if ($selected) {
        $str .= "<option value=\"1\" selected>예</option>\n";
        $str .= "<option value=\"0\">아니오</option>\n";
    } else {
        $str .= "<option value=\"1\">예</option>\n";
        $str .= "<option value=\"0\" selected>아니오</option>\n";
    }
    $str .= "</select>";
    return $str;
}


// 적립금 부여
function insert_cash($mb_id, $cash, $content, $rel_table = '', $rel_id = '', $rel_action = '', $expire = 0, $od_id)
{
    global $config;
    global $g5;

    // 적립금이 없다면 업데이트 할 필요 없음
    if ($cash == 0) {
        return 0;
    }

    // 회원아이디가 없다면 업데이트 할 필요 없음
    if ($mb_id == '') {
        return 0;
    }

    $mb = sql_fetch(" select mb_id from {$g5['member_table']} where mb_id = '$mb_id' ");
    if (!$mb['mb_id']) {
        return 0;
    }

    // 회원적립금
    $mb_cash = get_cash_sum($mb_id);

    // 이미 등록된 내역이라면 건너뜀
        $sql = " select count(*) as cnt from {$g5['cash_table']}
                  where mb_id = '$mb_id'
                    and ca_content = '$content'
                    and ca_rel_od_id = '$od_id'";
        $row = sql_fetch($sql);
        if ($row['cnt'])
            return -1;

    // 적립금 건별 생성
    $ca_expire_date = date('Y-m-d', strtotime('+365days', G5_SERVER_TIME));

    // 관련 주문이 있는 경우 해당주문의 만료 기간 조회
    if ($od_id) {
        $ca_rel = sql_fetch("select ca_rel_ca_id from {$g5['cash_table']} where ca_rel_od_id = '$od_id'");
        $ca_expire = sql_fetch("select ca_expire_date from {$g5['cash_table']} where ca_id = '{$ca_rel['ca_rel_ca_id']}'");
    }

    if ($ca_expire) {
        $ca_expire_date = $ca_expire['ca_expire_date'];
    }


    $ca_expired = 0;
    if ($cash < 0) {
        $ca_expired = 1;
        $ca_expire_date = G5_TIME_YMD;
    }

    $ca_mb_cash = $mb_cash + $cash;

    $sql = " insert into {$g5['cash_table']}
                set mb_id = '$mb_id',
                    ca_datetime = '" . G5_TIME_YMDHIS . "',
                    ca_content = '" . addslashes($content) . "',
                    ca_cash = '$cash',
                    ca_use_cash = '0',
                    ca_mb_cash = '$ca_mb_cash',
                    ca_expired = '$ca_expired',
                    ca_expire_date = '$ca_expire_date',
                    ca_rel_table = '$rel_table',
                    ca_rel_od_id = '$od_id',
                    ca_rel_action = '$rel_action' ";

    sql_query($sql);
    $ca_id = sql_insert_id();

    // 적립금를 사용한 경우 적립금 내역에 사용금액 기록
    if ($cash < 0) {
        $ca_rel_ca_id = insert_use_cash($mb_id, $cash);

        // 적립금를 사용한 id를 관련 필드에 update
        if ($ca_rel_ca_id != false) {
            $sql = " update {$g5['cash_table']} set ca_rel_ca_id = '{$ca_rel_ca_id}', ca_rel_od_id = '{$od_id}' where ca_id = '$ca_id' ";
            sql_query($sql);
        }
    }

    // 적립금 UPDATE
    $sql = " update {$g5['member_table']} set mb_cash = '$ca_mb_cash' where mb_id = '$mb_id' ";
    sql_query($sql);

    return 1;
}

// 사용적립금 입력
function insert_use_cash($mb_id, $cash, $ca_id = '')
{
    global $g5, $config;
    $return = false;

    if ($config['cf_cash_term'])
        $sql_order = " order by ca_expire_date asc, ca_id asc ";
    else
        $sql_order = " order by ca_id asc ";

    $cash1 = abs($cash);
    $sql = " select ca_id, ca_cash, ca_use_cash
                from {$g5['cash_table']}
                where mb_id = '$mb_id'
                  and ca_id <> '$ca_id'
                  and ca_expired = '0'
                  and ca_cash > ca_use_cash
                $sql_order ";
    $result = sql_query($sql);
    for ($i = 0; $row = sql_fetch_array($result); $i++) {
        $cash2 = $row['ca_cash'];
        $cash3 = $row['ca_use_cash'];

        if (($cash2 - $cash3) > $cash1) {
            $sql = " update {$g5['cash_table']}
                        set ca_use_cash = ca_use_cash + '$cash1'
                        where ca_id = '{$row['ca_id']}' ";
            sql_query($sql);

            $return = $row['ca_id'];
            break;
        } else {
            $cash4 = $cash2 - $cash3;
            $sql = " update {$g5['cash_table']}
                        set ca_use_cash = ca_use_cash + '$cash4',
                            ca_expired = '100'
                        where ca_id = '{$row['ca_id']}' ";
            sql_query($sql);
            $cash1 -= $cash4;

            $return = $row['ca_id'];
        }
    }

    return $return;
}

// 사용적립금 삭제
function delete_use_cash($mb_id, $cash)
{
    global $g5, $config;

    if ($config['cf_cash_term'])
        $sql_order = " order by ca_expire_date desc, ca_id desc ";
    else
        $sql_order = " order by ca_id desc ";

    $cash1 = abs($cash);
    $sql = " select ca_id, ca_use_cash, ca_expired, ca_expire_date
                from {$g5['cash_table']}
                where mb_id = '$mb_id'
                  and ca_expired <> '1'
                  and ca_use_cash > 0
                $sql_order ";
    $result = sql_query($sql);
    for ($i = 0; $row = sql_fetch_array($result); $i++) {
        $cash2 = $row['ca_use_cash'];

        $ca_expired = $row['ca_expired'];
        if (($row['ca_expired'] == 100 || $row['ca_expired'] == 1) && ($row['ca_expire_date'] == '9999-12-31' || $row['ca_expire_date'] >= G5_TIME_YMD))
            $ca_expired = 0;

        if ($cash2 > $cash1) {
            $sql = " update {$g5['cash_table']}
                        set ca_use_cash = ca_use_cash - '$cash1',
                            ca_expired = '$ca_expired'
                        where ca_id = '{$row['ca_id']}' ";
            sql_query($sql);
            break;
        } else {
            $sql = " update {$g5['cash_table']}
                        set ca_use_cash = '0',
                            ca_expired = '$ca_expired'
                        where ca_id = '{$row['ca_id']}' ";
            sql_query($sql);

            $cash1 -= $cash2;
        }
    }
}

// 소멸적립금 삭제
function delete_expire_cash($mb_id, $cash)
{
    global $g5, $config;

    $cash1 = abs($cash);
    $sql = " select ca_id, ca_use_cash, ca_expired, ca_expire_date
                from {$g5['cash_table']}
                where mb_id = '$mb_id'
                  and ca_expired = '1'
                  and ca_cash >= 0
                  and ca_use_cash > 0
                order by ca_expire_date desc, ca_id desc ";
    $result = sql_query($sql);
    for ($i = 0; $row = sql_fetch_array($result); $i++) {
        $cash2 = $row['ca_use_cash'];
        $ca_expired = '0';
        $ca_expire_date = '9999-12-31';
        if ($config['cf_cash_term'] > 0)
            $ca_expire_date = date('Y-m-d', strtotime('+' . ($config['cf_cash_term'] - 1) . ' days', G5_SERVER_TIME));

        if ($cash2 > $cash1) {
            $sql = " update {$g5['cash_table']}
                        set ca_use_cash = ca_use_cash - '$cash1',
                            ca_expired = '$ca_expired',
                            ca_expire_date = '$ca_expire_date'
                        where ca_id = '{$row['ca_id']}' ";
            sql_query($sql);
            break;
        } else {
            $sql = " update {$g5['cash_table']}
                        set ca_use_cash = '0',
                            ca_expired = '$ca_expired',
                            ca_expire_date = '$ca_expire_date'
                        where ca_id = '{$row['ca_id']}' ";
            sql_query($sql);

            $cash1 -= $cash2;
        }
    }
}

// 적립금 내역 합계
function get_cash_sum($mb_id)
{
    global $g5, $config;

    if ($config['cf_cash_term'] > 0) {
        // 소멸적립금가 있으면 내역 추가
        $expire_cash = get_expire_cash($mb_id);
        if ($expire_cash > 0) {
            $mb = get_member($mb_id, 'mb_cash');
            $content = '적립금 소멸 (기간 만료)';
            $rel_table = '@expire';
            $rel_id = $mb_id;
            $rel_action = 'expire' . '-' . uniqid('');
            $cash = $expire_cash * (-1);
            $ca_mb_cash = $mb['mb_cash'] + $cash;
            $ca_expire_date = G5_TIME_YMD;
            $ca_expired = 1;

            $sql = " insert into {$g5['cash_table']}
                        set mb_id = '$mb_id',
                            ca_datetime = '" . G5_TIME_YMDHIS . "',
                            ca_content = '" . addslashes($content) . "',
                            ca_cash = '$cash',
                            ca_use_cash = '0',
                            ca_mb_cash = '$ca_mb_cash',
                            ca_expired = '$ca_expired',
                            ca_expire_date = '$ca_expire_date',
                            ca_rel_table = '$rel_table',
                            ca_rel_id = '$rel_id',
                            ca_rel_action = '$rel_action' ";
            sql_query($sql);

            // 적립금를 사용한 경우 적립금 내역에 사용금액 기록
            if ($cash < 0) {
                insert_use_cash($mb_id, $cash);
            }
        }

        // 유효기간이 있을 때 기간이 지난 적립금 expired 체크
        $sql = " update {$g5['cash_table']}
                    set ca_expired = '1'
                    where mb_id = '$mb_id'
                      and ca_expired <> '1'
                      and ca_expire_date <> '9999-12-31'
                      and ca_expire_date < '" . G5_TIME_YMD . "' ";
        sql_query($sql);
    }

    // 적립금합
    $sql = " select sum(ca_cash) as sum_ca_cash
                from {$g5['cash_table']}
                where mb_id = '$mb_id' ";
    $row = sql_fetch($sql);

    return $row['sum_ca_cash'];
}

// 현재 적립금 확인 후 갱신
function cash_expire_check($mb_id)
{
    global $g5, $config;

        // 소멸적립금가 있으면 내역 추가
        $expire_cash = get_expire_cash($mb_id);
        if ($expire_cash > 0) {
            $mb = get_member($mb_id, 'mb_cash');
            $content = '적립금 소멸 (기간 만료)';
            $rel_table = '@expire';
            $rel_id = $mb_id;
            $rel_action = 'expire' . '-' . uniqid('');
            $cash = $expire_cash * (-1);
            $ca_mb_cash = $mb['mb_cash'] + $cash;
            $ca_expire_date = G5_TIME_YMD;
            $ca_expired = 1;

            $sql = " insert into {$g5['cash_table']}
                        set mb_id = '$mb_id',
                            ca_datetime = '" . G5_TIME_YMDHIS . "',
                            ca_content = '" . addslashes($content) . "',
                            ca_cash = '$cash',
                            ca_use_cash = '0',
                            ca_mb_cash = '$ca_mb_cash',
                            ca_expired = '$ca_expired',
                            ca_expire_date = '$ca_expire_date',
                            ca_rel_table = '$rel_table',
                            ca_rel_id = '$rel_id',
                            ca_rel_action = '$rel_action' ";
            sql_query($sql);

            // 적립금를 사용한 경우 적립금 내역에 사용금액 기록
            if ($cash < 0) {
                insert_use_cash($mb_id, $cash);
            }
        }

        // 유효기간이 있을 때 기간이 지난 적립금 expired 체크
        $sql = " update {$g5['cash_table']}
                    set ca_expired = '1'
                    where mb_id = '$mb_id'
                      and ca_expired <> '1'
                      and ca_expire_date <> '9999-12-31'
                      and ca_expire_date < '" . G5_TIME_YMD . "' ";
        sql_query($sql);

    // 적립금 내역의 합
    $sql = " select sum(ca_cash) as sum_ca_cash from {$g5['cash_table']} where mb_id = '$mb_id' ";
    $row = sql_fetch($sql);
    // 현재 회원 테이블의 적립금
    $sql = " select mb_cash from {$g5['member_table']} where mb_id = '$mb_id' ";
    $mb = sql_fetch($sql);

    if ($mb['mb_cash'] != $row['sum_ca_cash']) {
        // 적립금 UPDATE
        $sql = " update {$g5['member_table']} set mb_cash = '{$row['sum_ca_cash']}' where mb_id = '$mb_id' ";
        sql_query($sql);

        return $row['sum_ca_cash'];
    } else {
        return $mb['mb_cash'];
    }
}

// 소멸 적립금
function get_expire_cash($mb_id)
{
    global $g5, $config;

    if ($config['cf_cash_term'] == 0)
        return 0;

    $sql = " select sum(ca_cash - ca_use_cash) as sum_cash
                from {$g5['cash_table']}
                where mb_id = '$mb_id'
                  and ca_expired = '0'
                  and ca_expire_date <> '9999-12-31'
                  and ca_expire_date < '" . G5_TIME_YMD . "' ";
    $row = sql_fetch($sql);

    return $row['sum_cash'];
}

// 적립금 삭제
function delete_cash($mb_id, $rel_table, $rel_id, $rel_action)
{
    global $g5;

    $result = false;
    if ($rel_table || $rel_id || $rel_action) {
        // 적립금 내역정보
        $sql = " select * from {$g5['cash_table']}
                    where mb_id = '$mb_id'
                      and ca_rel_table = '$rel_table'
                      and ca_rel_id = '$rel_id'
                      and ca_rel_action = '$rel_action' ";
        $row = sql_fetch($sql);

        if ($row['ca_cash'] < 0) {
            $mb_id = $row['mb_id'];
            $ca_cash = abs($row['ca_cash']);

            delete_use_cash($mb_id, $ca_cash);
        } else {
            if ($row['ca_use_cash'] > 0) {
                insert_use_cash($row['mb_id'], $row['ca_use_cash'], $row['ca_id']);
            }
        }

        $result = sql_query(" delete from {$g5['cash_table']}
                     where mb_id = '$mb_id'
                       and ca_rel_table = '$rel_table'
                       and ca_rel_id = '$rel_id'
                       and ca_rel_action = '$rel_action' ", false);

        // ca_mb_cash에 반영
        $sql = " update {$g5['cash_table']}
                    set ca_mb_cash = ca_mb_cash - '{$row['ca_cash']}'
                    where mb_id = '$mb_id'
                      and ca_id > '{$row['ca_id']}' ";
        sql_query($sql);

        // 적립금 내역의 합을 구하고
        $sum_cash = get_cash_sum($mb_id);

        // 적립금 UPDATE
        $sql = " update {$g5['member_table']} set mb_cash = '$sum_cash' where mb_id = '$mb_id' ";
        $result = sql_query($sql);
    }

    return $result;
}



// 포인트 부여
function insert_point($mb_id, $point, $content='', $rel_table='', $rel_id='', $rel_action='', $expire=0, $od_id)
{
    global $config;
    global $g5;
    global $is_admin;
    global $TempIdArray; // common.php 선언

    // Temp ID 확인
    if (!empty($mb_id) && !empty($TempIdArray)) {
        if(in_array( $mb_id , $TempIdArray ) ){
            $config['cf_use_point'] = 1;
        }
    }

    // 포인트 사용을 하지 않는다면 return
    if (!$config['cf_use_point']) { return 0; }

    // 포인트가 없다면 업데이트 할 필요 없음
    if ($point == 0) { return 0; }

    // 회원아이디가 없다면 업데이트 할 필요 없음
    if ($mb_id == '') { return 0; }
    $mb = sql_fetch(" select mb_id from {$g5['member_table']} where mb_id = '$mb_id' ");
    if (!$mb['mb_id']) { return 0; }

    // 회원포인트
    $mb_point = get_point_sum($mb_id);

    // 이미 등록된 내역이라면 건너뜀
    if ($rel_table || $rel_id || $rel_action)
    {
        $sql = " select count(*) as cnt from {$g5['point_table']}
                  where mb_id = '$mb_id'
                    and po_rel_table = '$rel_table'
                    and po_rel_id = '$rel_id'
                    and po_rel_action = '$rel_action' ";
        $row = sql_fetch($sql);
        if ($row['cnt'])
            return -1;
    }

    // 포인트 건별 생성
    $po_expire_date = '9999-12-31';
    if($config['cf_point_term'] > 0) {
        if($expire > 0)
            $po_expire_date = date('Y-m-d', strtotime('+'.($expire - 1).' days', G5_SERVER_TIME));
        else
            $po_expire_date = date('Y-m-d', strtotime('+'.($config['cf_point_term'] - 1).' days', G5_SERVER_TIME));
    }

    // 관련 주문이 있는 경우 해당주문의 만료 기간 조회
    if ($od_id) {
        $po_rel = sql_fetch("select po_rel_po_id from {$g5['point_table']} where po_rel_od_id = '$od_id'");
        $po_expire = sql_fetch("select po_expire_date from {$g5['point_table']} where po_id = '{$po_rel['po_rel_po_id']}'");
    }

    if ($po_expire) {
        $po_expire_date = $po_expire['po_expire_date'];
    }

    $po_expired = 0;
    if($point < 0) {
        $po_expired = 1;
        $po_expire_date = G5_TIME_YMD;
    }

    $po_mb_point = $mb_point + $point;

    $sql = " insert into {$g5['point_table']}
                set mb_id = '$mb_id',
                    po_datetime = '".G5_TIME_YMDHIS."',
                    po_content = '".addslashes($content)."',
                    po_point = '$point',
                    po_use_point = '0',
                    po_mb_point = '$po_mb_point',
                    po_expired = '$po_expired',
                    po_expire_date = '$po_expire_date',
                    po_rel_table = '$rel_table',
                    po_rel_id = '$rel_id',
                    po_rel_action = '$rel_action' ";
    sql_query($sql);
    $po_id = sql_insert_id();

    // 포인트를 사용한 경우 포인트 내역에 사용금액 기록
    if($point < 0) {
        $po_rel_po_id = insert_use_point($mb_id, $point);

        // 포인트를 사용한 id를 관련 필드에 update
        if ($po_rel_po_id != false) {
            $sql = " update {$g5['point_table']} set po_rel_po_id = '{$po_rel_po_id}', po_rel_od_id = '{$od_id}' where po_id = '$po_id' ";
            sql_query($sql);
        }
    }

    // 포인트 UPDATE
    $sql = " update {$g5['member_table']} set mb_point = '$po_mb_point' where mb_id = '$mb_id' ";
    sql_query($sql);

    return 1;
}

// 사용포인트 입력
function insert_use_point($mb_id, $point, $po_id='')
{
    global $g5, $config;
    $return = false;

    if($config['cf_point_term'])
        $sql_order = " order by po_expire_date asc, po_id asc ";
    else
        $sql_order = " order by po_id asc ";

    $point1 = abs($point);
    $sql = " select po_id, po_point, po_use_point
                from {$g5['point_table']}
                where mb_id = '$mb_id'
                  and po_id <> '$po_id'
                  and po_expired = '0'
                  and po_point > po_use_point
                $sql_order ";
    $result = sql_query($sql);
    for($i=0; $row=sql_fetch_array($result); $i++) {
        $point2 = $row['po_point'];
        $point3 = $row['po_use_point'];

        if(($point2 - $point3) > $point1) {
            $sql = " update {$g5['point_table']}
                        set po_use_point = po_use_point + '$point1'
                        where po_id = '{$row['po_id']}' ";
            sql_query($sql);

            $return = $row['po_id'];
            break;
        } else {
            $point4 = $point2 - $point3;
            $sql = " update {$g5['point_table']}
                        set po_use_point = po_use_point + '$point4',
                            po_expired = '100'
                        where po_id = '{$row['po_id']}' ";
            sql_query($sql);
            $point1 -= $point4;

            $return = $row['po_id'];
        }
    }

    return $return;
}

// 사용포인트 삭제
function delete_use_point($mb_id, $point)
{
    global $g5, $config;

    if($config['cf_point_term'])
        $sql_order = " order by po_expire_date desc, po_id desc ";
    else
        $sql_order = " order by po_id desc ";

    $point1 = abs($point);
    $sql = " select po_id, po_use_point, po_expired, po_expire_date
                from {$g5['point_table']}
                where mb_id = '$mb_id'
                  and po_expired <> '1'
                  and po_use_point > 0
                $sql_order ";
    $result = sql_query($sql);
    for($i=0; $row=sql_fetch_array($result); $i++) {
        $point2 = $row['po_use_point'];

        $po_expired = $row['po_expired'];
        if(($row['po_expired'] == 100 || $row['po_expired'] == 1) && ($row['po_expire_date'] == '9999-12-31' || $row['po_expire_date'] >= G5_TIME_YMD))
            $po_expired = 0;

        if($point2 > $point1) {
            $sql = " update {$g5['point_table']}
                        set po_use_point = po_use_point - '$point1',
                            po_expired = '$po_expired'
                        where po_id = '{$row['po_id']}' ";
            sql_query($sql);
            break;
        } else {
            $sql = " update {$g5['point_table']}
                        set po_use_point = '0',
                            po_expired = '$po_expired'
                        where po_id = '{$row['po_id']}' ";
            sql_query($sql);

            $point1 -= $point2;
        }
    }
}

// 소멸포인트 삭제
function delete_expire_point($mb_id, $point)
{
    global $g5, $config;

    $point1 = abs($point);
    $sql = " select po_id, po_use_point, po_expired, po_expire_date
                from {$g5['point_table']}
                where mb_id = '$mb_id'
                  and po_expired = '1'
                  and po_point >= 0
                  and po_use_point > 0
                order by po_expire_date desc, po_id desc ";
    $result = sql_query($sql);
    for($i=0; $row=sql_fetch_array($result); $i++) {
        $point2 = $row['po_use_point'];
        $po_expired = '0';
        $po_expire_date = '9999-12-31';
        if($config['cf_point_term'] > 0)
            $po_expire_date = date('Y-m-d', strtotime('+'.($config['cf_point_term'] - 1).' days', G5_SERVER_TIME));

        if($point2 > $point1) {
            $sql = " update {$g5['point_table']}
                        set po_use_point = po_use_point - '$point1',
                            po_expired = '$po_expired',
                            po_expire_date = '$po_expire_date'
                        where po_id = '{$row['po_id']}' ";
            sql_query($sql);
            break;
        } else {
            $sql = " update {$g5['point_table']}
                        set po_use_point = '0',
                            po_expired = '$po_expired',
                            po_expire_date = '$po_expire_date'
                        where po_id = '{$row['po_id']}' ";
            sql_query($sql);

            $point1 -= $point2;
        }
    }
}

// 포인트 내역 합계
function get_point_sum($mb_id)
{
    global $g5, $config;

    if($config['cf_point_term'] > 0) {
        // 소멸포인트가 있으면 내역 추가
        $expire_point = get_expire_point($mb_id);
        if($expire_point > 0) {
            $mb = get_member($mb_id, 'mb_point');
            $content = '포인트 소멸 (기간 만료)';
            $rel_table = '@expire';
            $rel_id = $mb_id;
            $rel_action = 'expire'.'-'.uniqid('');
            $point = $expire_point * (-1);
            $po_mb_point = $mb['mb_point'] + $point;
            $po_expire_date = G5_TIME_YMD;
            $po_expired = 1;

            $sql = " insert into {$g5['point_table']}
                        set mb_id = '$mb_id',
                            po_datetime = '".G5_TIME_YMDHIS."',
                            po_content = '".addslashes($content)."',
                            po_point = '$point',
                            po_use_point = '0',
                            po_mb_point = '$po_mb_point',
                            po_expired = '$po_expired',
                            po_expire_date = '$po_expire_date',
                            po_rel_table = '$rel_table',
                            po_rel_id = '$rel_id',
                            po_rel_action = '$rel_action' ";
            sql_query($sql);

            // 포인트를 사용한 경우 포인트 내역에 사용금액 기록
            if($point < 0) {
                insert_use_point($mb_id, $point);
            }
        }

        // 유효기간이 있을 때 기간이 지난 포인트 expired 체크
        $sql = " update {$g5['point_table']}
                    set po_expired = '1'
                    where mb_id = '$mb_id'
                      and po_expired <> '1'
                      and po_expire_date <> '9999-12-31'
                      and po_expire_date < '".G5_TIME_YMD."' ";
        sql_query($sql);
    }

    // 포인트합
    $sql = " select sum(po_point) as sum_po_point
                from {$g5['point_table']}
                where mb_id = '$mb_id' ";
    $row = sql_fetch($sql);

    return $row['sum_po_point'];
}

// 현재 포인트 확인 후 갱신
function point_expire_check($mb_id)
{
    global $g5, $config;

    if($config['cf_point_term'] > 0) {
        // 소멸포인트가 있으면 내역 추가
        $expire_point = get_expire_point($mb_id);
        if($expire_point > 0) {
            $mb = get_member($mb_id, 'mb_point');
            $content = '포인트 소멸 (기간 만료)';
            $rel_table = '@expire';
            $rel_id = $mb_id;
            $rel_action = 'expire'.'-'.uniqid('');
            $point = $expire_point * (-1);
            $po_mb_point = $mb['mb_point'] + $point;
            $po_expire_date = G5_TIME_YMD;
            $po_expired = 1;

            $sql = " insert into {$g5['point_table']}
                        set mb_id = '$mb_id',
                            po_datetime = '".G5_TIME_YMDHIS."',
                            po_content = '".addslashes($content)."',
                            po_point = '$point',
                            po_use_point = '0',
                            po_mb_point = '$po_mb_point',
                            po_expired = '$po_expired',
                            po_expire_date = '$po_expire_date',
                            po_rel_table = '$rel_table',
                            po_rel_id = '$rel_id',
                            po_rel_action = '$rel_action' ";
            sql_query($sql);

            // 포인트를 사용한 경우 포인트 내역에 사용금액 기록
            if($point < 0) {
                insert_use_point($mb_id, $point);
            }
        }

        // 유효기간이 있을 때 기간이 지난 포인트 expired 체크
        $sql = " update {$g5['point_table']}
                    set po_expired = '1'
                    where mb_id = '$mb_id'
                      and po_expired <> '1'
                      and po_expire_date <> '9999-12-31'
                      and po_expire_date < '".G5_TIME_YMD."' ";
        sql_query($sql);
    }

    // 포인트 내역의 합
    $sql = " select sum(po_point) as sum_po_point
                from {$g5['point_table']}
                where mb_id = '$mb_id' ";
    $row = sql_fetch($sql);

    // 현재 회원 테이블의 포인트
    $sql = " select mb_point from {$g5['member_table']} where mb_id = '$mb_id' ";
    $mb = sql_fetch($sql);

    if ($mb['mb_point'] != $row['sum_po_point']) {
        // 포인트 UPDATE
        $sql = " update {$g5['member_table']} set mb_point = '{$row['sum_po_point']}' where mb_id = '$mb_id' ";
        sql_query($sql);

        return $row['sum_po_point'];
    } else {
        return $mb['mb_point'];
    }
}

// 소멸 포인트
function get_expire_point($mb_id)
{
    global $g5, $config;

    if($config['cf_point_term'] == 0)
        return 0;

    $sql = " select sum(po_point - po_use_point) as sum_point
                from {$g5['point_table']}
                where mb_id = '$mb_id'
                  and po_expired = '0'
                  and po_expire_date <> '9999-12-31'
                  and po_expire_date < '".G5_TIME_YMD."' ";
    $row = sql_fetch($sql);

    return $row['sum_point'];
}

// 포인트 삭제
function delete_point($mb_id, $rel_table, $rel_id, $rel_action)
{
    global $g5;

    $result = false;
    if ($rel_table || $rel_id || $rel_action)
    {
        // 포인트 내역정보
        $sql = " select * from {$g5['point_table']}
                    where mb_id = '$mb_id'
                      and po_rel_table = '$rel_table'
                      and po_rel_id = '$rel_id'
                      and po_rel_action = '$rel_action' ";
        $row = sql_fetch($sql);

        if($row['po_point'] < 0) {
            $mb_id = $row['mb_id'];
            $po_point = abs($row['po_point']);

            delete_use_point($mb_id, $po_point);
        } else {
            if($row['po_use_point'] > 0) {
                insert_use_point($row['mb_id'], $row['po_use_point'], $row['po_id']);
            }
        }

        $result = sql_query(" delete from {$g5['point_table']}
                     where mb_id = '$mb_id'
                       and po_rel_table = '$rel_table'
                       and po_rel_id = '$rel_id'
                       and po_rel_action = '$rel_action' ", false);

        // po_mb_point에 반영
        $sql = " update {$g5['point_table']}
                    set po_mb_point = po_mb_point - '{$row['po_point']}'
                    where mb_id = '$mb_id'
                      and po_id > '{$row['po_id']}' ";
        sql_query($sql);

        // 포인트 내역의 합을 구하고
        $sum_point = get_point_sum($mb_id);

        // 포인트 UPDATE
        $sql = " update {$g5['member_table']} set mb_point = '$sum_point' where mb_id = '$mb_id' ";
        $result = sql_query($sql);
    }

    return $result;
}

// 회원 레이어
function get_sideview($mb_id, $name='', $email='', $homepage='')
{
    global $config;
    global $g5;
    global $bo_table, $sca, $is_admin, $member;

    $email_enc = new str_encrypt();
    $email = $email_enc->encrypt($email);
    $homepage = set_http(clean_xss_tags($homepage));

    $name     = get_text($name, 0, true);
    $email    = get_text($email);
    $homepage = get_text($homepage);

    $tmp_name = "";
    if ($mb_id) {
        //$tmp_name = "<a href=\"".G5_BBS_URL."/profile.php?mb_id=".$mb_id."\" class=\"sv_member\" title=\"$name 자기소개\" rel="nofollow" target=\"_blank\" onclick=\"return false;\">$name</a>";
        $tmp_name = '<a href="'.G5_BBS_URL.'/profile.php?mb_id='.$mb_id.'" class="sv_member" title="'.$name.' 자기소개" target="_blank" rel="nofollow" onclick="return false;">';

        if ($config['cf_use_member_icon']) {
            $mb_dir = substr($mb_id,0,2);
            $icon_file = G5_DATA_PATH.'/member/'.$mb_dir.'/'.$mb_id.'.gif';

            if (file_exists($icon_file)) {
                $width = $config['cf_member_icon_width'];
                $height = $config['cf_member_icon_height'];
                $icon_file_url = G5_DATA_URL.'/member/'.$mb_dir.'/'.$mb_id.'.gif';
                $tmp_name .= '<span class="profile_img"><img src="'.$icon_file_url.'" width="'.$width.'" height="'.$height.'" alt=""></span>';

                if ($config['cf_use_member_icon'] == 2) // 회원아이콘+이름
                    $tmp_name = $tmp_name.' '.$name;
            } else {
                if( defined('G5_THEME_NO_PROFILE_IMG') ){
                    $tmp_name .= G5_THEME_NO_PROFILE_IMG;
                } else if( defined('G5_NO_PROFILE_IMG') ){
                    $tmp_name .= G5_NO_PROFILE_IMG;
                }
                if ($config['cf_use_member_icon'] == 2) // 회원아이콘+이름
                    $tmp_name = $tmp_name.' '.$name;
            }
        } else {
            $tmp_name = $tmp_name.' '.$name;
        }
        $tmp_name .= '</a>';

        $title_mb_id = '['.$mb_id.']';
    } else {
        if(!$bo_table)
            return $name;

        $tmp_name = '<a href="'.G5_BBS_URL.'/board.php?bo_table='.$bo_table.'&amp;sca='.$sca.'&amp;sfl=wr_name,1&amp;stx='.$name.'" title="'.$name.' 이름으로 검색" class="sv_guest" rel="nofollow" onclick="return false;">'.$name.'</a>';
        $title_mb_id = '[비회원]';
    }

    $str = "<span class=\"sv_wrap\">\n";
    $str .= $tmp_name."\n";

    $str2 = "<span class=\"sv\">\n";
    if($mb_id)
        $str2 .= "<a href=\"".G5_BBS_URL."/memo_form.php?me_recv_mb_id=".$mb_id."\" onclick=\"win_memo(this.href); return false;\">쪽지보내기</a>\n";
    if($email)
        $str2 .= "<a href=\"".G5_BBS_URL."/formmail.php?mb_id=".$mb_id."&amp;name=".urlencode($name)."&amp;email=".$email."\" onclick=\"win_email(this.href); return false;\">메일보내기</a>\n";
    if($homepage)
        $str2 .= "<a href=\"".$homepage."\" target=\"_blank\">홈페이지</a>\n";
    if($mb_id)
        $str2 .= "<a href=\"".G5_BBS_URL."/profile.php?mb_id=".$mb_id."\" onclick=\"win_profile(this.href); return false;\">자기소개</a>\n";
    if($bo_table) {
        if($mb_id)
            $str2 .= "<a href=\"".G5_BBS_URL."/board.php?bo_table=".$bo_table."&amp;sca=".$sca."&amp;sfl=mb_id,1&amp;stx=".$mb_id."\">아이디로 검색</a>\n";
        else
            $str2 .= "<a href=\"".G5_BBS_URL."/board.php?bo_table=".$bo_table."&amp;sca=".$sca."&amp;sfl=wr_name,1&amp;stx=".$name."\">이름으로 검색</a>\n";
    }
    if($mb_id)
        $str2 .= "<a href=\"".G5_BBS_URL."/new.php?mb_id=".$mb_id."\">전체게시물</a>\n";
    if($is_admin == "super" && $mb_id) {
        $str2 .= "<a href=\"".G5_ADMIN_URL."/member_form.php?w=u&amp;mb_id=".$mb_id."\" target=\"_blank\">회원정보변경</a>\n";
        $str2 .= "<a href=\"".G5_ADMIN_URL."/point_list.php?sfl=mb_id&amp;stx=".$mb_id."\" target=\"_blank\">포인트내역</a>\n";
    }
    $str2 .= "</span>\n";
    $str .= $str2;
    $str .= "\n<noscript class=\"sv_nojs\">".$str2."</noscript>";

    $str .= "</span>";

    return $str;
}


// 파일을 보이게 하는 링크 (이미지, 플래쉬, 동영상)
function view_file_link($file, $width, $height, $content='')
{
    global $config, $board;
    global $g5;
    static $ids;

    if (!$file) return;

    $ids++;

    // 파일의 폭이 게시판설정의 이미지폭 보다 크다면 게시판설정 폭으로 맞추고 비율에 따라 높이를 계산
    if ($width > $board['bo_image_width'] && $board['bo_image_width'])
    {
        $rate = $board['bo_image_width'] / $width;
        $width = $board['bo_image_width'];
        $height = (int)($height * $rate);
    }

    // 폭이 있는 경우 폭과 높이의 속성을 주고, 없으면 자동 계산되도록 코드를 만들지 않는다.
    if ($width)
        $attr = ' width="'.$width.'" height="'.$height.'" ';
    else
        $attr = '';

    if (preg_match("/\.({$config['cf_image_extension']})$/i", $file)) {
        $img = '<a href="'.G5_BBS_URL.'/view_image.php?bo_table='.$board['bo_table'].'&amp;fn='.urlencode($file).'" target="_blank" class="view_image">';
        $img .= '<img src="'.G5_DATA_URL.'/file/'.$board['bo_table'].'/'.urlencode($file).'" alt="'.$content.'" '.$attr.'>';
        $img .= '</a>';

        return $img;
    }
}


// view_file_link() 함수에서 넘겨진 이미지를 보이게 합니다.
// {img:0} ... {img:n} 과 같은 형식
function view_image($view, $number, $attribute)
{
    if ($view['file'][$number]['view'])
        return preg_replace("/>$/", " $attribute>", $view['file'][$number]['view']);
    else
        //return "{".$number."번 이미지 없음}";
        return "";
}


/*
// {link:0} ... {link:n} 과 같은 형식
function view_link($view, $number, $attribute)
{
    global $config;

    if ($view['link'][$number]['link'])
    {
        if (!preg_match("/target/i", $attribute))
            $attribute .= " target='$config['cf_link_target']'";
        return "<a href='{$view['link'][$number]['href']}' $attribute>{$view['link'][$number]['link']}</a>";
    }
    else
        return "{".$number."번 링크 없음}";
}
*/


function cut_str($str, $len, $suffix="…")
{
    $arr_str = preg_split("//u", $str, -1, PREG_SPLIT_NO_EMPTY);
    $str_len = count($arr_str);

    if ($str_len >= $len) {
        $slice_str = array_slice($arr_str, 0, $len);
        $str = join("", $slice_str);

        return $str . ($str_len > $len ? $suffix : '');
    } else {
        $str = join("", $arr_str);
        return $str;
    }
}


// TEXT 형식으로 변환
function get_text($str, $html=0, $restore=false)
{
    $source[] = "<";
    $target[] = "&lt;";
    $source[] = ">";
    $target[] = "&gt;";
    $source[] = "\"";
    $target[] = "&#034;";
    $source[] = "\'";
    $target[] = "&#039;";

    if ($restore)
        $str = str_replace($target, $source, $str);

    // 3.31
    // TEXT 출력일 경우 &amp; &nbsp; 등의 코드를 정상으로 출력해 주기 위함
    if ($html == 0) {
        $str = html_symbol($str);
    }

    if ($html) {
        $source[] = "\n";
        $target[] = "<br/>";
    }

    return str_replace($source, $target, $str);
}

function str_empty_chk($str) {
    if (empty($str)) {
        return '-';
    } else {
        return $str;
    }
}


/*
// HTML 특수문자 변환 htmlspecialchars
function hsc($str)
{
    $trans = array("\"" => "&#034;", "'" => "&#039;", "<"=>"&#060;", ">"=>"&#062;");
    $str = strtr($str, $trans);
    return $str;
}
*/

// 3.31
// HTML SYMBOL 변환
// &nbsp; &amp; &middot; 등을 정상으로 출력
function html_symbol($str)
{
    return preg_replace("/\&([a-z0-9]{1,20}|\#[0-9]{0,3});/i", "&#038;\\1;", $str);
}


/*************************************************************************
**
**  SQL 관련 함수 모음
**
*************************************************************************/

// DB 연결
function sql_connect($host, $user, $pass, $db=G5_MYSQL_DB)
{
    global $g5;

    if(function_exists('mysqli_connect') && G5_MYSQLI_USE) {
        $link = mysqli_connect($host, $user, $pass, $db);

        // 연결 오류 발생 시 스크립트 종료
        if (mysqli_connect_errno()) {
            die('Connect Error: '.mysqli_connect_error());
        }
    } else {
        $link = mysql_connect($host, $user, $pass);
    }

    return $link;
}


// DB 선택
function sql_select_db($db, $connect)
{
    global $g5;

    if(function_exists('mysqli_select_db') && G5_MYSQLI_USE)
        return @mysqli_select_db($connect, $db);
    else
        return @mysql_select_db($db, $connect);
}


function sql_set_charset($charset, $link=null)
{
    global $g5;

    if(!$link)
        $link = $g5['connect_db'];

    if(function_exists('mysqli_set_charset') && G5_MYSQLI_USE)
        mysqli_set_charset($link, $charset);
    else
        mysql_query(" set names {$charset} ", $link);
}


// mysqli_query 와 mysqli_error 를 한꺼번에 처리
// mysql connect resource 지정 - 명랑폐인님 제안
function sql_query($sql, $error=G5_DISPLAY_SQL_ERROR, $link=null)
{
    global $g5;

    if(!$link)
        $link = $g5['connect_db'];

    // Blind SQL Injection 취약점 해결
    $sql = trim($sql);
    // union의 사용을 허락하지 않습니다.
    //$sql = preg_replace("#^select.*from.*union.*#i", "select 1", $sql);
    $sql = preg_replace("#^select.*from.*[\s\(]+union[\s\)]+.*#i ", "select 1", $sql);
    // `information_schema` DB로의 접근을 허락하지 않습니다.
    $sql = preg_replace("#^select.*from.*where.*`?information_schema`?.*#i", "select 1", $sql);

    if(function_exists('mysqli_query') && G5_MYSQLI_USE) {
        if ($error) {
            $result = @mysqli_query($link, $sql) or die("<p>$sql<p>" . mysqli_errno($link) . " : " .  mysqli_error($link) . "<p>error file : {$_SERVER['SCRIPT_NAME']}");
        } else {
            $result = @mysqli_query($link, $sql);
        }
    } else {
        if ($error) {
            $result = @mysql_query($sql, $link) or die("<p>$sql<p>" . mysql_errno() . " : " .  mysql_error() . "<p>error file : {$_SERVER['SCRIPT_NAME']}");
        } else {
            $result = @mysql_query($sql, $link);
        }
    }

    return $result;
}


// 쿼리를 실행한 후 결과값에서 한행을 얻는다.
function sql_fetch($sql, $error=G5_DISPLAY_SQL_ERROR, $link=null)
{
    global $g5;

    if(!$link)
        $link = $g5['connect_db'];

    $result = sql_query($sql, $error, $link);
    //$row = @sql_fetch_array($result) or die("<p>$sql<p>" . mysqli_errno() . " : " .  mysqli_error() . "<p>error file : $_SERVER['SCRIPT_NAME']");
    $row = sql_fetch_array($result);
    return $row;
}

// 결과값에서 즉시 한행 데이터를 얻는다 (단 row가 1인 경우 사용)
function sql_fetch_row($result)
{
    if (sql_num_rows($result) == 1) {
        $row = sql_fetch_array($result);

        return $row;
    } else {
        return false;
    }
}


// 결과값에서 한행 연관배열(이름으로)로 얻는다.
function sql_fetch_array($result)
{
    if(function_exists('mysqli_fetch_assoc') && G5_MYSQLI_USE)
        $row = @mysqli_fetch_assoc($result);
    else
        $row = @mysql_fetch_assoc($result);

    return $row;
}

// 결과값에서 한행 연관배열(이름으로)로 얻는다.
function sql_fetch_result($sql, $link=null)
{
    $arr = array();
    $result = sql_query($sql, G5_DISPLAY_SQL_ERROR, $link);
    while ($row = sql_fetch_array($result)) {
        $arr[] = $row;
    }

    return $arr;
}


// $result에 대한 메모리(memory)에 있는 내용을 모두 제거한다.
// sql_free_result()는 결과로부터 얻은 질의 값이 커서 많은 메모리를 사용할 염려가 있을 때 사용된다.
// 단, 결과 값은 스크립트(script) 실행부가 종료되면서 메모리에서 자동적으로 지워진다.
function sql_free_result($result)
{
    if(function_exists('mysqli_free_result') && G5_MYSQLI_USE)
        return mysqli_free_result($result);
    else
        return mysql_free_result($result);
}


function sql_password($value)
{
    // mysql 4.0x 이하 버전에서는 password() 함수의 결과가 16bytes
    // mysql 4.1x 이상 버전에서는 password() 함수의 결과가 41bytes
    $row = sql_fetch(" select password('$value') as pass ");

    return $row['pass'];
}


function sql_insert_id($link=null)
{
    global $g5;

    if(!$link)
        $link = $g5['connect_db'];

    if(function_exists('mysqli_insert_id') && G5_MYSQLI_USE)
        return mysqli_insert_id($link);
    else
        return mysql_insert_id($link);
}


function sql_num_rows($result)
{
    if(function_exists('mysqli_num_rows') && G5_MYSQLI_USE)
        return @mysqli_num_rows($result);
    else
        return mysql_num_rows($result);
}


function sql_field_names($table, $link=null)
{
    global $g5;

    if(!$link)
        $link = $g5['connect_db'];

    $columns = array();

    $sql = " select * from `$table` limit 1 ";
    $result = sql_query($sql, $link);

    if(function_exists('mysqli_fetch_field') && G5_MYSQLI_USE) {
        while($field = mysqli_fetch_field($result)) {
            $columns[] = $field->name;
        }
    } else {
        $i = 0;
        $cnt = mysql_num_fields($result);
        while($i < $cnt) {
            $field = mysql_fetch_field($result, $i);
            $columns[] = $field->name;
            $i++;
        }
    }

    return $columns;
}


function sql_error_info($link=null)
{
    global $g5;

    if(!$link)
        $link = $g5['connect_db'];

    if(function_exists('mysqli_error') && G5_MYSQLI_USE) {
        return mysqli_errno($link) . ' : ' . mysqli_error($link);
    } else {
        return mysql_errno($link) . ' : ' . mysql_error($link);
    }
}


// PHPMyAdmin 참고
function get_table_define($table, $crlf="\n")
{
    global $g5;

    // For MySQL < 3.23.20
    $schema_create .= 'CREATE TABLE ' . $table . ' (' . $crlf;

    $sql = 'SHOW FIELDS FROM ' . $table;
    $result = sql_query($sql);
    while ($row = sql_fetch_array($result))
    {
        $schema_create .= '    ' . $row['Field'] . ' ' . $row['Type'];
        if (isset($row['Default']) && $row['Default'] != '')
        {
            $schema_create .= ' DEFAULT \'' . $row['Default'] . '\'';
        }
        if ($row['Null'] != 'YES')
        {
            $schema_create .= ' NOT NULL';
        }
        if ($row['Extra'] != '')
        {
            $schema_create .= ' ' . $row['Extra'];
        }
        $schema_create     .= ',' . $crlf;
    } // end while
    sql_free_result($result);

    $schema_create = preg_replace('/,' . $crlf . '$/', '', $schema_create);

    $sql = 'SHOW KEYS FROM ' . $table;
    $result = sql_query($sql);
    while ($row = sql_fetch_array($result))
    {
        $kname    = $row['Key_name'];
        $comment  = (isset($row['Comment'])) ? $row['Comment'] : '';
        $sub_part = (isset($row['Sub_part'])) ? $row['Sub_part'] : '';

        if ($kname != 'PRIMARY' && $row['Non_unique'] == 0) {
            $kname = "UNIQUE|$kname";
        }
        if ($comment == 'FULLTEXT') {
            $kname = 'FULLTEXT|$kname';
        }
        if (!isset($index[$kname])) {
            $index[$kname] = array();
        }
        if ($sub_part > 1) {
            $index[$kname][] = $row['Column_name'] . '(' . $sub_part . ')';
        } else {
            $index[$kname][] = $row['Column_name'];
        }
    } // end while
    sql_free_result($result);

    while (list($x, $columns) = @each($index)) {
        $schema_create     .= ',' . $crlf;
        if ($x == 'PRIMARY') {
            $schema_create .= '    PRIMARY KEY (';
        } else if (substr($x, 0, 6) == 'UNIQUE') {
            $schema_create .= '    UNIQUE ' . substr($x, 7) . ' (';
        } else if (substr($x, 0, 8) == 'FULLTEXT') {
            $schema_create .= '    FULLTEXT ' . substr($x, 9) . ' (';
        } else {
            $schema_create .= '    KEY ' . $x . ' (';
        }
        $schema_create     .= implode($columns, ', ') . ')';
    } // end while

    $schema_create .= $crlf . ') ENGINE=MyISAM DEFAULT CHARSET=utf8';

    return $schema_create;
} // end of the 'PMA_getTableDef()' function


// 리퍼러 체크
function referer_check($url='')
{
    /*
    // 제대로 체크를 하지 못하여 주석 처리함
    global $g5;

    if (!$url)
        $url = G5_URL;

    if (!preg_match("/^http['s']?:\/\/".$_SERVER['HTTP_HOST']."/", $_SERVER['HTTP_REFERER']))
        alert("제대로 된 접근이 아닌것 같습니다.", $url);
    */
}


// 한글 요일
function get_yoil($date, $full=0)
{
    $arr_yoil = array ('일', '월', '화', '수', '목', '금', '토');

    $yoil = date("w", strtotime($date));
    $str = $arr_yoil[$yoil];
    if ($full) {
        $str .= '요일';
    }
    return $str;
}


// 날짜를 select 박스 형식으로 얻는다
function date_select($date, $name='')
{
    global $g5;

    $s = '';
    if (substr($date, 0, 4) == "0000") {
        $date = G5_TIME_YMDHIS;
    }
    preg_match("/([0-9]{4})-([0-9]{2})-([0-9]{2})/", $date, $m);

    // 년
    $s .= "<select name='{$name}_y'>";
    for ($i=$m['0']-3; $i<=$m['0']+3; $i++) {
        $s .= "<option value='$i'";
        if ($i == $m['0']) {
            $s .= " selected";
        }
        $s .= ">$i";
    }
    $s .= "</select>년 \n";

    // 월
    $s .= "<select name='{$name}_m'>";
    for ($i=1; $i<=12; $i++) {
        $s .= "<option value='$i'";
        if ($i == $m['2']) {
            $s .= " selected";
        }
        $s .= ">$i";
    }
    $s .= "</select>월 \n";

    // 일
    $s .= "<select name='{$name}_d'>";
    for ($i=1; $i<=31; $i++) {
        $s .= "<option value='$i'";
        if ($i == $m['3']) {
            $s .= " selected";
        }
        $s .= ">$i";
    }
    $s .= "</select>일 \n";

    return $s;
}


// 시간을 select 박스 형식으로 얻는다
// 1.04.00
// 경매에 시간 설정이 가능하게 되면서 추가함
function time_select($time, $name="")
{
    preg_match("/([0-9]{2}):([0-9]{2}):([0-9]{2})/", $time, $m);

    // 시
    $s .= "<select name='{$name}_h'>";
    for ($i=0; $i<=23; $i++) {
        $s .= "<option value='$i'";
        if ($i == $m['0']) {
            $s .= " selected";
        }
        $s .= ">$i";
    }
    $s .= "</select>시 \n";

    // 분
    $s .= "<select name='{$name}_i'>";
    for ($i=0; $i<=59; $i++) {
        $s .= "<option value='$i'";
        if ($i == $m['2']) {
            $s .= " selected";
        }
        $s .= ">$i";
    }
    $s .= "</select>분 \n";

    // 초
    $s .= "<select name='{$name}_s'>";
    for ($i=0; $i<=59; $i++) {
        $s .= "<option value='$i'";
        if ($i == $m['3']) {
            $s .= " selected";
        }
        $s .= ">$i";
    }
    $s .= "</select>초 \n";

    return $s;
}


// DEMO 라는 파일이 있으면 데모 화면으로 인식함
function check_demo()
{
    global $is_admin;
    if ($is_admin != 'super' && file_exists(G5_PATH.'/DEMO'))
        alert('데모 화면에서는 하실(보실) 수 없는 작업입니다.');
}


// 문자열이 한글, 영문, 숫자, 특수문자로 구성되어 있는지 검사
function check_string($str, $options)
{
    global $g5;

    $s = '';
    for($i=0;$i<strlen($str);$i++) {
        $c = $str[$i];
        $oc = ord($c);

        // 한글
        if ($oc >= 0xA0 && $oc <= 0xFF) {
            if ($options & G5_HANGUL) {
                $s .= $c . $str[$i+1] . $str[$i+2];
            }
            $i+=2;
        }
        // 숫자
        else if ($oc >= 0x30 && $oc <= 0x39) {
            if ($options & G5_NUMERIC) {
                $s .= $c;
            }
        }
        // 영대문자
        else if ($oc >= 0x41 && $oc <= 0x5A) {
            if (($options & G5_ALPHABETIC) || ($options & G5_ALPHAUPPER)) {
                $s .= $c;
            }
        }
        // 영소문자
        else if ($oc >= 0x61 && $oc <= 0x7A) {
            if (($options & G5_ALPHABETIC) || ($options & G5_ALPHALOWER)) {
                $s .= $c;
            }
        }
        // 공백
        else if ($oc == 0x20) {
            if ($options & G5_SPACE) {
                $s .= $c;
            }
        }
        else {
            if ($options & G5_SPECIAL) {
                $s .= $c;
            }
        }
    }

    // 넘어온 값과 비교하여 같으면 참, 틀리면 거짓
    return ($str == $s);
}


// 한글(2bytes)에서 마지막 글자가 1byte로 끝나는 경우
// 출력시 깨지는 현상이 발생하므로 마지막 완전하지 않은 글자(1byte)를 하나 없앰
function cut_hangul_last($hangul)
{
    global $g5;

    // 한글이 반쪽나면 ?로 표시되는 현상을 막음
    $cnt = 0;
    for($i=0;$i<strlen($hangul);$i++) {
        // 한글만 센다
        if (ord($hangul[$i]) >= 0xA0) {
            $cnt++;
        }
    }

    return $hangul;
}


// 테이블에서 INDEX(키) 사용여부 검사
function explain($sql)
{
    if (preg_match("/^(select)/i", trim($sql))) {
        $q = "explain $sql";
        echo $q;
        $row = sql_fetch($q);
        if (!$row['key']) $row['key'] = "NULL";
        echo " <font color=blue>(type={$row['type']} , key={$row['key']})</font>";
    }
}

// 악성태그 변환
function bad_tag_convert($code)
{
    global $view;
    global $member, $is_admin;

    if ($is_admin && $member['mb_id'] != $view['mb_id']) {
        //$code = preg_replace_callback("#(\<(embed|object)[^\>]*)\>(\<\/(embed|object)\>)?#i",
        // embed 또는 object 태그를 막지 않는 경우 필터링이 되도록 수정
        $code = preg_replace_callback("#(\<(embed|object)[^\>]*)\>?(\<\/(embed|object)\>)?#i",
                    create_function('$matches', 'return "<div class=\"embedx\">보안문제로 인하여 관리자 아이디로는 embed 또는 object 태그를 볼 수 없습니다. 확인하시려면 관리권한이 없는 다른 아이디로 접속하세요.</div>";'),
                    $code);
    }

    return preg_replace("/\<([\/]?)(script|iframe|form)([^\>]*)\>?/i", "&lt;$1$2$3&gt;", $code);
}


// 토큰 생성
function _token()
{
    return md5(uniqid(rand(), true));
}


// 불법접근을 막도록 토큰을 생성하면서 토큰값을 리턴
function get_token()
{
    $token = md5(uniqid(rand(), true));
    set_session('ss_token', $token);

    return $token;
}


// POST로 넘어온 토큰과 세션에 저장된 토큰 비교
function check_token()
{
    set_session('ss_token', '');
    return true;
}


// 문자열에 utf8 문자가 들어 있는지 검사하는 함수
// 코드 : http://in2.php.net/manual/en/function.mb-check-encoding.php#95289
function is_utf8($str)
{
    $len = strlen($str);
    for($i = 0; $i < $len; $i++) {
        $c = ord($str[$i]);
        if ($c > 128) {
            if (($c > 247)) return false;
            elseif ($c > 239) $bytes = 4;
            elseif ($c > 223) $bytes = 3;
            elseif ($c > 191) $bytes = 2;
            else return false;
            if (($i + $bytes) > $len) return false;
            while ($bytes > 1) {
                $i++;
                $b = ord($str[$i]);
                if ($b < 128 || $b > 191) return false;
                $bytes--;
            }
        }
    }
    return true;
}


// UTF-8 문자열 자르기
// 출처 : https://www.google.co.kr/search?q=utf8_strcut&aq=f&oq=utf8_strcut&aqs=chrome.0.57j0l3.826j0&sourceid=chrome&ie=UTF-8
function utf8_strcut( $str, $size, $suffix='...' )
{
        $substr = substr( $str, 0, $size * 2 );
        $multi_size = preg_match_all( '/[\x80-\xff]/', $substr, $multi_chars );

        if ( $multi_size > 0 )
            $size = $size + intval( $multi_size / 3 ) - 1;

        if ( strlen( $str ) > $size ) {
            $str = substr( $str, 0, $size );
            $str = preg_replace( '/(([\x80-\xff]{3})*?)([\x80-\xff]{0,2})$/', '$1', $str );
            $str .= $suffix;
        }

        return $str;
}


/*
-----------------------------------------------------------
    Charset 을 변환하는 함수
-----------------------------------------------------------
iconv 함수가 있으면 iconv 로 변환하고
없으면 mb_convert_encoding 함수를 사용한다.
둘다 없으면 사용할 수 없다.
*/
function convert_charset($from_charset, $to_charset, $str)
{

    if( function_exists('iconv') )
        return iconv($from_charset, $to_charset, $str);
    elseif( function_exists('mb_convert_encoding') )
        return mb_convert_encoding($str, $to_charset, $from_charset);
    else
        die("Not found 'iconv' or 'mbstring' library in server.");
}


// mysqli_real_escape_string 의 alias 기능을 한다.
function sql_real_escape_string($str, $link=null)
{
    global $g5;

    if(!$link)
        $link = $g5['connect_db'];
    
    if(function_exists('mysqli_connect') && G5_MYSQLI_USE) {
        return mysqli_real_escape_string($link, $str);
    }

    return mysql_real_escape_string($str, $link);
}

function escape_trim($field)
{
    $str = call_user_func(G5_ESCAPE_FUNCTION, $field);
    return $str;
}


// $_POST 형식에서 checkbox 엘리먼트의 checked 속성에서 checked 가 되어 넘어 왔는지를 검사
function is_checked($field)
{
    return !empty($_POST[$field]);
}


function abs_ip2long($ip='')
{
    $ip = $ip ? $ip : $_SERVER['REMOTE_ADDR'];
    return abs(ip2long($ip));
}


function get_selected($field, $value)
{
    return ($field==$value) ? ' selected="selected"' : '';
}


function get_checked($field, $value)
{
    return ($field==$value) ? ' checked="checked"' : '';
}


function is_mobile()
{
    return preg_match('/'.G5_MOBILE_AGENT.'/i', $_SERVER['HTTP_USER_AGENT']);
}


/*******************************************************************************
    유일한 키를 얻는다.

    결과 :

        년월일시분초00 ~ 년월일시분초99
        년(4) 월(2) 일(2) 시(2) 분(2) 초(2) 100분의1초(2)
        총 16자리이며 년도는 2자리로 끊어서 사용해도 됩니다.
        예) 2008062611570199 또는 08062611570199 (2100년까지만 유일키)

    사용하는 곳 :
    1. 게시판 글쓰기시 미리 유일키를 얻어 파일 업로드 필드에 넣는다.
    2. 주문번호 생성시에 사용한다.
    3. 기타 유일키가 필요한 곳에서 사용한다.
*******************************************************************************/
// 기존의 get_unique_id() 함수를 사용하지 않고 get_uniqid() 를 사용한다.
function get_uniqid()
{
    global $g5;

    sql_query(" LOCK TABLE {$g5['uniqid_table']} WRITE ");
    while (1) {
        // 년월일시분초에 100분의 1초 두자리를 추가함 (1/100 초 앞에 자리가 모자르면 0으로 채움)
        //$key = date('YmdHis', time()) . str_pad((int)(microtime()*100), 2, "0", STR_PAD_LEFT);
		$sql = "select max(uq_id) uq_id from {$g5['uniqid_table']} where left(uq_id, 8) = '".date('Ymd', time())."'";
        $res = sql_fetch($sql);
        $uq_id = $res['uq_id'];
		if($uq_id=='') {
			$key = date('Ymd', time()).'00001';
		} else {
			$key = str_pad($uq_id+1, 5,"0",STR_PAD_LEFT);
		}

        $result = sql_query(" insert into {$g5['uniqid_table']} set uq_id = '$key', uq_ip = '{$_SERVER['REMOTE_ADDR']}' ", false);
        if ($result) break; // 쿼리가 정상이면 빠진다.

        // insert 하지 못했으면 일정시간 쉰다음 다시 유일키를 만든다.
        usleep(10000); // 100분의 1초를 쉰다
    }
    sql_query(" UNLOCK TABLES ");

    return $key;
}


// CHARSET 변경 : euc-kr -> utf-8
function iconv_utf8($str)
{
    return iconv('euc-kr', 'utf-8', $str);
}


// CHARSET 변경 : utf-8 -> euc-kr
function iconv_euckr($str)
{
    return iconv('utf-8', 'euc-kr', $str);
}


// PC 또는 모바일 사용인지를 검사
function check_device($device)
{
    global $is_admin;

    if ($is_admin) return;

    if ($device=='pc' && G5_IS_MOBILE) {
        alert('PC 전용 게시판입니다.', G5_URL);
    } else if ($device=='mobile' && !G5_IS_MOBILE) {
        alert('모바일 전용 게시판입니다.', G5_URL);
    }
}


// 게시판 최신글 캐시 파일 삭제
function delete_cache_latest($bo_table)
{
    if (!preg_match("/^([A-Za-z0-9_]{1,20})$/", $bo_table)) {
        return;
    }

    $files = glob(G5_DATA_PATH.'/cache/latest-'.$bo_table.'-*');
    if (is_array($files)) {
        foreach ($files as $filename)
            unlink($filename);
    }
}

// 게시판 첨부파일 썸네일 삭제
function delete_board_thumbnail($bo_table, $file)
{
    if(!$bo_table || !$file)
        return;

    $fn = preg_replace("/\.[^\.]+$/i", "", basename($file));
    $files = glob(G5_DATA_PATH.'/file/'.$bo_table.'/thumb-'.$fn.'*');
    if (is_array($files)) {
        foreach ($files as $filename)
            unlink($filename);
    }
}

// 에디터 이미지 얻기
function get_editor_image($contents, $view=true)
{
    if(!$contents)
        return false;

    // $contents 중 img 태그 추출
    if ($view)
        $pattern = "/<img([^>]*)>/iS";
    else
        $pattern = "/<img[^>]*src=[\'\"]?([^>\'\"]+[^>\'\"]+)[\'\"]?[^>]*>/i";
    preg_match_all($pattern, $contents, $matchs);

    return $matchs;
}

// 에디터 썸네일 삭제
function delete_editor_thumbnail($contents)
{
    if(!$contents)
        return;

    // $contents 중 img 태그 추출
    $matchs = get_editor_image($contents);

    if(!$matchs)
        return;

    for($i=0; $i<count($matchs[1]); $i++) {
        // 이미지 path 구함
        $imgurl = @parse_url($matchs[1][$i]);
        $srcfile = $_SERVER['DOCUMENT_ROOT'].$imgurl['path'];

        $filename = preg_replace("/\.[^\.]+$/i", "", basename($srcfile));
        $filepath = dirname($srcfile);
        $files = glob($filepath.'/thumb-'.$filename.'*');
        if (is_array($files)) {
            foreach($files as $filename)
                unlink($filename);
        }
    }
}

// 1:1문의 첨부파일 썸네일 삭제
function delete_qa_thumbnail($file)
{
    if(!$file)
        return;

    $fn = preg_replace("/\.[^\.]+$/i", "", basename($file));
    $files = glob(G5_DATA_PATH.'/qa/thumb-'.$fn.'*');
    if (is_array($files)) {
        foreach ($files as $filename)
            unlink($filename);
    }
}

// 스킨 style sheet 파일 얻기
function get_skin_stylesheet($skin_path, $dir='')
{
    if(!$skin_path)
        return "";

    $str = "";
    $files = array();

    if($dir)
        $skin_path .= '/'.$dir;

    $skin_url = G5_URL.str_replace("\\", "/", str_replace(G5_PATH, "", $skin_path));

    if(is_dir($skin_path)) {
        if($dh = opendir($skin_path)) {
            while(($file = readdir($dh)) !== false) {
                if($file == "." || $file == "..")
                    continue;

                if(is_dir($skin_path.'/'.$file))
                    continue;

                if(preg_match("/\.(css)$/i", $file))
                    $files[] = $file;
            }
            closedir($dh);
        }
    }

    if(!empty($files)) {
        sort($files);

        foreach($files as $file) {
            $str .= '<link rel="stylesheet" href="'.$skin_url.'/'.$file.'?='.date("md").'">'."\n";
        }
    }

    return $str;

    /*
    // glob 를 이용한 코드
    if (!$skin_path) return '';
    $skin_path .= $dir ? '/'.$dir : '';

    $str = '';
    $skin_url = G5_URL.str_replace('\\', '/', str_replace(G5_PATH, '', $skin_path));

    foreach (glob($skin_path.'/*.css') as $filepath) {
        $file = str_replace($skin_path, '', $filepath);
        $str .= '<link rel="stylesheet" href="'.$skin_url.'/'.$file.'?='.date('md').'">'."\n";
    }
    return $str;
    */
}

// 스킨 javascript 파일 얻기
function get_skin_javascript($skin_path, $dir='')
{
    if(!$skin_path)
        return "";

    $str = "";
    $files = array();

    if($dir)
        $skin_path .= '/'.$dir;

    $skin_url = G5_URL.str_replace("\\", "/", str_replace(G5_PATH, "", $skin_path));

    if(is_dir($skin_path)) {
        if($dh = opendir($skin_path)) {
            while(($file = readdir($dh)) !== false) {
                if($file == "." || $file == "..")
                    continue;

                if(is_dir($skin_path.'/'.$file))
                    continue;

                if(preg_match("/\.(js)$/i", $file))
                    $files[] = $file;
            }
            closedir($dh);
        }
    }

    if(!empty($files)) {
        sort($files);

        foreach($files as $file) {
            $str .= '<script src="'.$skin_url.'/'.$file.'"></script>'."\n";
        }
    }

    return $str;
}

// file_put_contents 는 PHP5 전용 함수이므로 PHP4 하위버전에서 사용하기 위함
// http://www.phpied.com/file_get_contents-for-php4/
if (!function_exists('file_put_contents')) {
    function file_put_contents($filename, $data) {
        $f = @fopen($filename, 'w');
        if (!$f) {
            return false;
        } else {
            $bytes = fwrite($f, $data);
            fclose($f);
            return $bytes;
        }
    }
}


// HTML 마지막 처리
function html_end()
{
    global $html_process;

    return $html_process->run();
}

function add_stylesheet($stylesheet, $order=0)
{
    global $html_process;

    if(trim($stylesheet))
        $html_process->merge_stylesheet($stylesheet, $order);
}

function add_javascript($javascript, $order=0)
{
    global $html_process;

    if(trim($javascript))
        $html_process->merge_javascript($javascript, $order);
}

/* ........................ 메타 추가  ......................................................................................*/
function add_meta($meta, $order=0) { 
	global $html_process; 

	if(trim($meta)) 
		$html_process->merge_meta($meta, $order); 
} 
/* ........................ 메타 추가  ......................................................................................*/


class html_process {
    protected $css = array();
    protected $js  = array();

	/* ........................ 메타 추가  ......................................................................................*/
	protected $meta  = array();

	function merge_meta($meta, $order) {
		$links = $this->meta;
		$is_merge = true;

		foreach($links as $metatag) {
			if($metatag[1] == $meta) {
				$is_merge = false;
				break;
			}
		}

		if($is_merge)
			$this->meta[] = array($order, $meta);
	}
	/* ........................ 메타 추가  ......................................................................................*/


    function merge_stylesheet($stylesheet, $order)
    {
        $links = $this->css;
        $is_merge = true;

        foreach($links as $link) {
            if($link[1] == $stylesheet) {
                $is_merge = false;
                break;
            }
        }

        if($is_merge)
            $this->css[] = array($order, $stylesheet);
    }

    function merge_javascript($javascript, $order)
    {
        $scripts = $this->js;
        $is_merge = true;

        foreach($scripts as $script) {
            if($script[1] == $javascript) {
                $is_merge = false;
                break;
            }
        }

        if($is_merge)
            $this->js[] = array($order, $javascript);
    }

    function run()
    {
        global $config, $g5, $member;

        // 현재접속자 처리
        $tmp_sql = " select count(*) as cnt from {$g5['login_table']} where lo_ip = '{$_SERVER['REMOTE_ADDR']}' ";
        $tmp_row = sql_fetch($tmp_sql);

        if ($tmp_row['cnt']) {
            $tmp_sql = " update {$g5['login_table']} set mb_id = '{$member['mb_id']}', lo_datetime = '".G5_TIME_YMDHIS."', lo_location = '{$g5['lo_location']}', lo_url = '{$g5['lo_url']}' where lo_ip = '{$_SERVER['REMOTE_ADDR']}' ";
            sql_query($tmp_sql, FALSE);
        } else {
            $tmp_sql = " insert into {$g5['login_table']} ( lo_ip, mb_id, lo_datetime, lo_location, lo_url ) values ( '{$_SERVER['REMOTE_ADDR']}', '{$member['mb_id']}', '".G5_TIME_YMDHIS."', '{$g5['lo_location']}',  '{$g5['lo_url']}' ) ";
            sql_query($tmp_sql, FALSE);

            // 시간이 지난 접속은 삭제한다
            sql_query(" delete from {$g5['login_table']} where lo_datetime < '".date("Y-m-d H:i:s", G5_SERVER_TIME - (60 * $config['cf_login_minutes']))."' ");

            // 부담(overhead)이 있다면 테이블 최적화
            //$row = sql_fetch(" SHOW TABLE STATUS FROM `$mysql_db` LIKE '$g5['login_table']' ");
            //if ($row['Data_free'] > 0) sql_query(" OPTIMIZE TABLE $g5['login_table'] ");
        }

        // 회원 접속 경로 로그 수집
        insert_member_log();

        $buffer = ob_get_contents();
        ob_end_clean();

        $stylesheet = '';
        $links = $this->css;

        if(!empty($links)) {
            foreach ($links as $key => $row) {
                $order[$key] = $row[0];
                $index[$key] = $key;
                $style[$key] = $row[1];
            }

            array_multisort($order, SORT_ASC, $index, SORT_ASC, $links);

            foreach($links as $link) {
                if(!trim($link[1]))
                    continue;

                $link[1] = preg_replace('#\.css([\'\"]?>)$#i', '.css?ver='.G5_CSS_VER.'$1', $link[1]);

                $stylesheet .= PHP_EOL.$link[1];
            }
        }

        $javascript = '';
        $scripts = $this->js;
        $php_eol = '';

        unset($order);
        unset($index);

        if(!empty($scripts)) {
            foreach ($scripts as $key => $row) {
                $order[$key] = $row[0];
                $index[$key] = $key;
                $script[$key] = $row[1];
            }

            array_multisort($order, SORT_ASC, $index, SORT_ASC, $scripts);

            foreach($scripts as $js) {
                if(!trim($js[1]))
                    continue;

                $js[1] = preg_replace('#\.js([\'\"]?>)$#i', '.js?ver='.G5_JS_VER.'$1', $js[1]);

                $javascript .= $php_eol.$js[1];
                $php_eol = PHP_EOL;
            }
        }

        /* ........................ 메타 추가 C.m.A ......................................................................................*/
            $meta = '';
            $metas = $this->meta;
 
            unset($order);
            unset($index);
 
           if(!empty($metas)) {
                foreach ($metas as $key => $row) {
                    $order[$key] = $row[0];
                    $index[$key] = $key;
                    $style[$key] = $row[1];
                }
 
                array_multisort($order, SORT_ASC, $index, SORT_ASC, $metas);
 
                foreach($metas as $metatag) {
                    if(!trim($metatag[1]))
                        continue;
 
                    $meta .= PHP_EOL.$metatag[1];
                }
            }
        /* ........................ 메타 추가 C.m.A ......................................................................................*/
 

        /*
        </title>
        <link rel="stylesheet" href="default.css">
        밑으로 스킨의 스타일시트가 위치하도록 하게 한다.
        */
        $buffer = preg_replace('#(</title>[^<]*<link[^>]+>)#', "$1$stylesheet", $buffer);

        /*
        </head>
        <body>
        전에 스킨의 자바스크립트가 위치하도록 하게 한다.
        */
        $nl = '';
        if($javascript)
            $nl = "\n";
        $buffer = preg_replace('#(</head>[^<]*<body[^>]*>)#', "$javascript{$nl}$1", $buffer);

        /* ........................ 메타 추가 C.m.A ......................................................................................*/
             $buffer = preg_replace('#(<head>[^<]*<meta[^>]+>)#', "$1$meta", $buffer);
             /* 사용법
                // add_meta('메타태그 구문', 출력순서); //숫자가 작을 수록 먼저 출력됨
                add_meta('<meta name="Title" content="Javascript,C.m.A API 이야기(http://chongmoa.com)" />', 0); 
                add_meta('<meta name="Subject" content="Javascript,C.m.A API 이야기,API,OPEN API,XML,PHP,MYSQL,Jvascript,Html,chongmoa.com,홈페이지,XpressEngine,XpressEngine 팁,XpressEngine tip, cms,쇼핑몰,큐알코드생성,QR Code Generator,IP Address Lookup,유닉스타임변환,Unix Timestamp Converter,유닉스타임을 날짜로 변환,Unix timestamp to DateTime,IFRAME 및 Object으로 선택후 유튜브 태그를 생성해주는 기능,유튜브 태그 생성기,CSV TO Actionscript Converter,CSV TO HTML Converter,CSV TO JSON - Properties Converter,CSV TO JSON - Column Arrays Converter,CSV TO JSON - Row Arrays Converter,CSV TO JSON - Dictionary Converter,CSV TO XML - Properties Converter,CSV TO XML - Nodes Converter,CSV TO XML - Illustrator Converter,CSV TO MySQL Converter,CSV TO ASP/VBScript Converter,CSV TO PHP Converter,CSV TO Python - Dict Converter,CSV TO Ruby Converter, Pixel to Em Converter [CSS 단위별 폰트크기(px, em, %, pt)]" />', 0); 
                add_meta('<link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://chongmoa.com/javascript/api" />', 0); 
                add_meta('<meta name="twitter:url" content="http://chongmoa.com/19287" />', 0); 
                add_meta('<meta property="og:title" content="C.m.A API 이야기(Mokulsha!)" />, 0); 
             */
        /* ........................ 메타 추가 C.m.A ......................................................................................*/
 

        return $buffer;
    }
}

// 사이트 접속 경로 로그 생성
function insert_member_log()
{
    global $config, $g5, $member;

    $s_cart_id = get_session('ss_cart_id');
    $hostname=$_SERVER["HTTP_HOST"];
    $url_refer=$_SERVER['HTTP_REFERER'];
    $user_agent  = escape_trim(clean_xss_tags($_SERVER['HTTP_USER_AGENT']));

    $year = date('Y');
    $table_name = 'tbl_member_log_'.$year;

    // 해당 테이블이 없는 경우 생성
    if(!sql_query(" DESCRIBE ".$table_name, false)) {
        sql_query("
        CREATE TABLE `$table_name` (
        `auto_num` INT(11) NOT NULL AUTO_INCREMENT,
        `lo_ip` VARCHAR(255) NOT NULL DEFAULT '' COMMENT '로그인ip' COLLATE 'utf8_unicode_ci',
        `session_num` BIGINT(20) UNSIGNED NOT NULL COMMENT '세션',
        `url_gubun` VARCHAR(255) NOT NULL DEFAULT '' COMMENT 'url확인' COLLATE 'utf8_unicode_ci',
        `mb_id` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '아이디' COLLATE 'utf8_unicode_ci',
        `lo_datetime` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '접속시간',
        `lo_location` TEXT NOT NULL COMMENT '접속페이지' COLLATE 'utf8_unicode_ci',
        `lo_url` TEXT NOT NULL COMMENT '접속 url' COLLATE 'utf8_unicode_ci',
        `vi_agent` VARCHAR(255) NOT NULL DEFAULT '' COMMENT '접속브라우저' COLLATE 'utf8_unicode_ci',
        `c_time` VARCHAR(255) NOT NULL DEFAULT '' COMMENT '체류시간' COLLATE 'utf8_unicode_ci',
        PRIMARY KEY (`auto_num`) USING BTREE,
        INDEX `session_num` (`session_num`) USING BTREE
        )
        COMMENT='자체로그쌓기위한 테이블'
        COLLATE='utf8_unicode_ci'
        ENGINE=InnoDB
        ; ", false);
    }

    // 접속한 페이지 로그 저장 및 이전 페이지 c_time UPDATE
    if ($s_cart_id != 0) {
        if ($member['mb_type'] != "9" && $member['mb_type'] != "7") {
            $tmp_sql = " select lo_datetime as cnt,auto_num from $table_name where auto_num = (select MAX(auto_num) from $table_name where session_num='$s_cart_id')";
            $tmp_row2 = sql_fetch($tmp_sql);
            $auto_num_b = $tmp_row2['auto_num'];
            if ($tmp_row2['cnt']) {
                $c_time = strtotime(date("Y-m-d H:i:s")) - strtotime($tmp_row2['cnt']);
                if ($c_time < 1800) { // 1800초는 30분
                    sql_query(" update $table_name set c_time = '$c_time' where auto_num = '$auto_num_b' and c_time ='0' and session_num='$s_cart_id' ");
                }
            }

            $tmp_sql = " insert into $table_name ( lo_ip, url_gubun, mb_id, lo_datetime, lo_location, lo_url ,session_num, vi_agent, c_time) values ( '{$_SERVER['REMOTE_ADDR']}', '$hostname' ,'{$member['mb_id']}', '".G5_TIME_YMDHIS."', '{$g5['lo_location']}', '{$g5['lo_url']}' ,'$s_cart_id', '$user_agent','0') ";
            sql_query($tmp_sql, FALSE);
        }
    }
}

// 휴대폰번호의 숫자만 취한 후 중간에 하이픈(-)을 넣는다.
function hyphen_hp_number($hp)
{
    $hp = preg_replace("/[^0-9]/", "", $hp);
    return preg_replace("/([0-9]{3})([0-9]{3,4})([0-9]{4})$/", "\\1-\\2-\\3", $hp);
}


// 로그인 후 이동할 URL
function login_url($url='')
{
    if (!$url) $url = G5_URL;

    return urlencode(clean_xss_tags(urldecode($url)));
}


// $dir 을 포함하여 https 또는 http 주소를 반환한다.
function https_url($dir, $https=true)
{
    if ($https) {
        if (G5_HTTPS_DOMAIN) {
            $url = G5_HTTPS_DOMAIN.'/'.$dir;
        } else {
            $url = G5_URL.'/'.$dir;
        }
    } else {
        if (G5_DOMAIN) {
            $url = G5_DOMAIN.'/'.$dir;
        } else {
            $url = G5_URL.'/'.$dir;
        }
    }

    return $url;
}


// 게시판의 공지사항을 , 로 구분하여 업데이트 한다.
function board_notice($bo_notice, $wr_id, $insert=false)
{
    $notice_array = explode(",", trim($bo_notice));

    if($insert && in_array($wr_id, $notice_array))
        return $bo_notice;

    $notice_array = array_merge(array($wr_id), $notice_array);
    $notice_array = array_unique($notice_array);
    foreach ($notice_array as $key=>$value) {
        if (!trim($value))
            unset($notice_array[$key]);
    }
    if (!$insert) {
        foreach ($notice_array as $key=>$value) {
            if ((int)$value == (int)$wr_id)
                unset($notice_array[$key]);
        }
    }
    return implode(",", $notice_array);
}


// goo.gl 짧은주소 만들기
function googl_short_url($longUrl)
{
    global $config;

    // Get API key from : http://code.google.com/apis/console/
    // URL Shortener API ON
    $apiKey = $config['cf_googl_shorturl_apikey'];

    $postData = array('longUrl' => $longUrl);
    $jsonData = json_encode($postData);

    $curlObj = curl_init();

    curl_setopt($curlObj, CURLOPT_URL, 'https://www.googleapis.com/urlshortener/v1/url?key='.$apiKey);
    curl_setopt($curlObj, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curlObj, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($curlObj, CURLOPT_HEADER, 0);
    curl_setopt($curlObj, CURLOPT_HTTPHEADER, array('Content-type:application/json'));
    curl_setopt($curlObj, CURLOPT_POST, 1);
    curl_setopt($curlObj, CURLOPT_POSTFIELDS, $jsonData);

    $response = curl_exec($curlObj);

    //change the response json string to object
    $json = json_decode($response);

    curl_close($curlObj);

    return $json->id;
}


// 임시 저장된 글 수
function autosave_count($mb_id)
{
    global $g5;

    if ($mb_id) {
        $row = sql_fetch(" select count(*) as cnt from {$g5['autosave_table']} where mb_id = '$mb_id' ");
        return (int)$row['cnt'];
    } else {
        return 0;
    }
}

// 본인확인내역 기록
function insert_cert_history($mb_id, $company, $method)
{
    global $g5;

    $sql = " insert into {$g5['cert_history_table']}
                set mb_id = '$mb_id',
                    cr_company = '$company',
                    cr_method = '$method',
                    cr_ip = '{$_SERVER['REMOTE_ADDR']}',
                    cr_date = '".G5_TIME_YMD."',
                    cr_time = '".G5_TIME_HIS."' ";
    sql_query($sql);
}

// 인증시도회수 체크
function certify_count_check($mb_id, $type)
{
    global $g5, $config;

    if($config['cf_cert_use'] != 2)
        return;

    if($config['cf_cert_limit'] == 0)
        return;

    $sql = " select count(*) as cnt from {$g5['cert_history_table']} ";

    if($mb_id) {
        $sql .= " where mb_id = '$mb_id' ";
    } else {
        $sql .= " where cr_ip = '{$_SERVER['REMOTE_ADDR']}' ";
    }

    $sql .= " and cr_method = '".$type."' and cr_date = '".G5_TIME_YMD."' ";

    $row = sql_fetch($sql);

    switch($type) {
        case 'hp':
            $cert = '휴대폰';
            break;
        case 'ipin':
            $cert = '아이핀';
            break;
        default:
            break;
    }

    if((int)$row['cnt'] >= (int)$config['cf_cert_limit'])
        alert_close('오늘 '.$cert.' 본인확인을 '.$row['cnt'].'회 이용하셔서 더 이상 이용할 수 없습니다.');
}

// 1:1문의 설정로드
function get_qa_config($fld='*')
{
    global $g5;

    $sql = " select $fld from {$g5['qa_config_table']} ";
    $row = sql_fetch($sql);

    return $row;
}

// get_sock 함수 대체
if (!function_exists("get_sock")) {
    function get_sock($url)
    {
        // host 와 uri 를 분리
        //if (ereg("http://([a-zA-Z0-9_\-\.]+)([^<]*)", $url, $res))
        if (preg_match("/http:\/\/([a-zA-Z0-9_\-\.]+)([^<]*)/", $url, $res))
        {
            $host = $res[1];
            $get  = $res[2];
        }

        // 80번 포트로 소캣접속 시도
        $fp = fsockopen ($host, 80, $errno, $errstr, 30);
        if (!$fp)
        {
            //die("$errstr ($errno)\n");

            echo "$errstr ($errno)\n";
            return null;
        }
        else
        {
            fputs($fp, "GET $get HTTP/1.0\r\n");
            fputs($fp, "Host: $host\r\n");
            fputs($fp, "\r\n");

            // header 와 content 를 분리한다.
            while (trim($buffer = fgets($fp,1024)) != "")
            {
                $header .= $buffer;
            }
            while (!feof($fp))
            {
                $buffer .= fgets($fp,1024);
            }
        }
        fclose($fp);

        // content 만 return 한다.
        return $buffer;
    }
}

// 인증, 결제 모듈 실행 체크
function module_exec_check($exe, $type)
{
    $error = '';
    $is_linux = false;
    if(strtoupper(substr(PHP_OS, 0, 3)) !== 'WIN')
        $is_linux = true;

    // 모듈 파일 존재하는지 체크
    if(!is_file($exe)) {
        $error = $exe.' 파일이 존재하지 않습니다.';
    } else {
        // 실행권한 체크
        if(!is_executable($exe)) {
            if($is_linux)
                $error = $exe.'\n파일의 실행권한이 없습니다.\n\nchmod 755 '.basename($exe).' 과 같이 실행권한을 부여해 주십시오.';
            else
                $error = $exe.'\n파일의 실행권한이 없습니다.\n\n'.basename($exe).' 파일에 실행권한을 부여해 주십시오.';
        } else {
            // 바이너리 파일인지
            if($is_linux) {
                $search = false;
                $isbinary = true;
                $executable = true;

                switch($type) {
                    case 'ct_cli':
                        exec($exe.' -h 2>&1', $out, $return_var);

                        if($return_var == 139) {
                            $isbinary = false;
                            break;
                        }

                        for($i=0; $i<count($out); $i++) {
                            if(strpos($out[$i], 'KCP ENC') !== false) {
                                $search = true;
                                break;
                            }
                        }
                        break;
                    case 'pp_cli':
                        exec($exe.' -h 2>&1', $out, $return_var);

                        if($return_var == 139) {
                            $isbinary = false;
                            break;
                        }

                        for($i=0; $i<count($out); $i++) {
                            if(strpos($out[$i], 'CLIENT') !== false) {
                                $search = true;
                                break;
                            }
                        }
                        break;
                    case 'okname':
                        exec($exe.' D 2>&1', $out, $return_var);

                        if($return_var == 139) {
                            $isbinary = false;
                            break;
                        }

                        for($i=0; $i<count($out); $i++) {
                            if(strpos(strtolower($out[$i]), 'ret code') !== false) {
                                $search = true;
                                break;
                            }
                        }
                        break;
                }

                if(!$isbinary || !$search) {
                    $error = $exe.'\n파일을 바이너리 타입으로 다시 업로드하여 주십시오.';
                }
            }
        }
    }

    if($error) {
        $error = '<script>alert("'.$error.'");</script>';
    }

    return $error;
}

// 주소출력
function print_address($addr1, $addr2, $addr3, $addr4)
{
    $address = get_text(trim($addr1));
    $addr2   = get_text(trim($addr2));
    $addr3   = get_text(trim($addr3));

    if($addr4 == 'N') {
        if($addr2)
            $address .= ' '.$addr2;
    } else {
        if($addr2)
            $address .= ', '.$addr2;
    }

    if($addr3)
        $address .= ' '.$addr3;

    return $address;
}

// input vars 체크
function check_input_vars()
{
    $max_input_vars = ini_get('max_input_vars');

    if($max_input_vars) {
        $post_vars = count($_POST, COUNT_RECURSIVE);
        $get_vars = count($_GET, COUNT_RECURSIVE);
        $cookie_vars = count($_COOKIE, COUNT_RECURSIVE);

        $input_vars = $post_vars + $get_vars + $cookie_vars;

        if($input_vars > $max_input_vars) {
            alert('폼에서 전송된 변수의 개수가 max_input_vars 값보다 큽니다.\\n전송된 값중 일부는 유실되어 DB에 기록될 수 있습니다.\\n\\n문제를 해결하기 위해서는 서버 php.ini의 max_input_vars 값을 변경하십시오.');
        }
    }
}

// HTML 특수문자 변환 htmlspecialchars
function htmlspecialchars2($str)
{
    $trans = array("\"" => "&#034;", "'" => "&#039;", "<"=>"&#060;", ">"=>"&#062;");
    $str = strtr($str, $trans);
    return $str;
}

// date 형식 변환
function conv_date_format($format, $date, $add='')
{
    if($add)
        $timestamp = strtotime($add, strtotime($date));
    else
        $timestamp = strtotime($date);

    return date($format, $timestamp);
}

// 검색어 특수문자 제거
function get_search_string($stx)
{
    $stx_pattern = array();
    $stx_pattern[] = '#\.*/+#';
    $stx_pattern[] = '#\\\*#';
    $stx_pattern[] = '#\.{2,}#';
    $stx_pattern[] = '#[/\'\"%=*\#\(\)\|\+\&\!\$~\{\}\[\]`;:\?\^\,]+#';

    $stx_replace = array();
    $stx_replace[] = '';
    $stx_replace[] = '';
    $stx_replace[] = '.';
    $stx_replace[] = '';

    $stx = preg_replace($stx_pattern, $stx_replace, $stx);

    return $stx;
}

// XSS 관련 태그 제거
function clean_xss_tags($str)
{
    $str = preg_replace('#</*(?:applet|b(?:ase|gsound|link)|embed|frame(?:set)?|i(?:frame|layer)|l(?:ayer|ink)|meta|object|s(?:cript|tyle)|title|xml)[^>]*+>#i', '', $str);

    return $str;
}

// unescape nl 얻기
function conv_unescape_nl($str)
{
    $search = array('\\r', '\r', '\\n', '\n');
    $replace = array('', '', "\n", "\n");

    return str_replace($search, $replace, $str);
}

// 회원 삭제
function member_delete($mb_id)
{
    global $config;
    global $g5;

    $sql = " select mb_name, mb_nick, mb_ip, mb_recommend, mb_memo, mb_level from {$g5['member_table']} where mb_id= '".$mb_id."' ";
    $mb = sql_fetch($sql);

    // 이미 삭제된 회원은 제외
    if(preg_match('#^[0-9]{8}.*삭제함#', $mb['mb_memo']))
        return;

    if ($mb['mb_recommend']) {
        $row = sql_fetch(" select count(*) as cnt from {$g5['member_table']} where mb_id = '".addslashes($mb['mb_recommend'])."' ");
        if ($row['cnt'])
            insert_point($mb['mb_recommend'], $config['cf_recommend_point'] * (-1), $mb_id.'님의 회원자료 삭제로 인한 추천인 포인트 반환', "@member", $mb['mb_recommend'], $mb_id.' 추천인 삭제');
    }

    // 회원자료는 정보만 없앤 후 아이디는 보관하여 다른 사람이 사용하지 못하도록 함 : 061025
    $sql = " update {$g5['member_table']} set mb_password = '', mb_level = 1, mb_email = '', mb_homepage = '', mb_tel = '', mb_hp = '', mb_zip1 = '', mb_zip2 = '', mb_addr1 = '', mb_addr2 = '', mb_birth = '', mb_sex = '', mb_signature = '', mb_memo = '".date('Ymd', G5_SERVER_TIME)." 삭제함\n{$mb['mb_memo']}' where mb_id = '{$mb_id}' ";
    sql_query($sql);

    // 포인트 테이블에서 삭제
    sql_query(" delete from {$g5['point_table']} where mb_id = '$mb_id' ");

    // 그룹접근가능 삭제
    sql_query(" delete from {$g5['group_member_table']} where mb_id = '$mb_id' ");

    // 쪽지 삭제
    sql_query(" delete from {$g5['memo_table']} where me_recv_mb_id = '$mb_id' or me_send_mb_id = '$mb_id' ");

    // 스크랩 삭제
    sql_query(" delete from {$g5['scrap_table']} where mb_id = '$mb_id' ");

    // 관리권한 삭제
    sql_query(" delete from {$g5['auth_table']} where mb_id = '$mb_id' ");

    // 그룹관리자인 경우 그룹관리자를 공백으로
    sql_query(" update {$g5['group_table']} set gr_admin = '' where gr_admin = '$mb_id' ");

    // 게시판관리자인 경우 게시판관리자를 공백으로
    sql_query(" update {$g5['board_table']} set bo_admin = '' where bo_admin = '$mb_id' ");

    //소셜로그인에서 삭제 또는 해제
    if(function_exists('social_member_link_delete')){
        social_member_link_delete($mb_id);
    }

    // 아이콘 삭제
    @unlink(G5_DATA_PATH.'/member/'.substr($mb_id,0,2).'/'.$mb_id.'.gif');
}

// 이메일 주소 추출
function get_email_address($email)
{
    preg_match("/[0-9a-z._-]+@[a-z0-9._-]{4,}/i", $email, $matches);

    return $matches[0];
}

// 파일명에서 특수문자 제거
function get_safe_filename($name)
{
    $pattern = '/["\'<>=#&!%\\\\(\)\*\+\?]/';
    $name = preg_replace($pattern, '', $name);

    return $name;
}

// 파일명 치환
function replace_filename($name)
{
    @session_start();
    $ss_id = session_id();
    $usec = get_microtime();
    $file_path = pathinfo($name);
    $ext = $file_path['extension'];
    $return_filename = sha1($ss_id.$_SERVER['REMOTE_ADDR'].$usec); 
    if( $ext )
        $return_filename .= '.'.$ext;

    return $return_filename;
}

// 아이코드 사용자정보
function get_icode_userinfo($id, $pass)
{
    $res = get_sock('http://www.icodekorea.com/res/userinfo.php?userid='.$id.'&userpw='.$pass);
    $res = explode(';', $res);
    $userinfo = array(
        'code'      => $res[0], // 결과코드
        'coin'      => $res[1], // 고객 잔액 (충전제만 해당)
        'gpay'      => $res[2], // 고객의 건수 별 차감액 표시 (충전제만 해당)
        'payment'   => $res[3]  // 요금제 표시, A:충전제, C:정액제
    );

    return $userinfo;
}

// 인기검색어 입력
function insert_popular($field, $str)
{
    global $g5;

    if(!in_array('mb_id', $field)) {
        $sql = " insert into {$g5['popular_table']} set pp_word = '{$str}', pp_date = '".G5_TIME_YMD."', pp_ip = '{$_SERVER['REMOTE_ADDR']}' ";
        sql_query($sql, FALSE);
    }
}

// 문자열 암호화
function get_encrypt_string($str)
{
    if(defined('G5_STRING_ENCRYPT_FUNCTION') && G5_STRING_ENCRYPT_FUNCTION) {
        $encrypt = call_user_func(G5_STRING_ENCRYPT_FUNCTION, $str);
    } else {
        $encrypt = sql_password($str);
    }

    return $encrypt;
}

// 비밀번호 비교
function check_password($pass, $hash)
{
    $password = get_encrypt_string($pass);

    return ($password === $hash);
}

// 동일한 host url 인지
function check_url_host($url, $msg='', $return_url=G5_URL, $is_redirect=false)
{
    if(!$msg)
        $msg = 'url에 타 도메인을 지정할 수 없습니다.';

    $p = @parse_url($url);
    $host = preg_replace('/:[0-9]+$/', '', $_SERVER['HTTP_HOST']);
    $is_host_check = false;
    
    // url을 urlencode 를 2번이상하면 parse_url 에서 scheme와 host 값을 가져올수 없는 취약점이 존재함
    if ( $is_redirect && !isset($p['host']) && urldecode($url) != $url ){
        $i = 0;
        while($i <= 3){
            $url = urldecode($url);
            if( urldecode($url) == $url ) break;
            $i++;
        }

        if( urldecode($url) == $url ){
            $p = @parse_url($url);
        } else {
            $is_host_check = true;
        }
    }

    if(stripos($url, 'http:') !== false) {
        if(!isset($p['scheme']) || !$p['scheme'] || !isset($p['host']) || !$p['host'])
            alert('url 정보가 올바르지 않습니다.', $return_url);
    }

    //php 5.6.29 이하 버전에서는 parse_url 버그가 존재함
    //php 7.0.1 ~ 7.0.5 버전에서는 parse_url 버그가 존재함
    if ( $is_redirect && (isset($p['host']) && $p['host']) ) {
        $bool_ch = false;
        foreach( array('user','host') as $key) {
            if ( isset( $p[ $key ] ) && strpbrk( $p[ $key ], ':/?#@' ) ) {
                $bool_ch = true;
            }
        }
        if( $bool_ch ){
            $regex = '/https?\:\/\/'.$host.'/i';
            if( ! preg_match($regex, $url) ){
                $is_host_check = true;
            }
        }
    }

    if ((isset($p['scheme']) && $p['scheme']) || (isset($p['host']) && $p['host']) || $is_host_check) {
        //if ($p['host'].(isset($p['port']) ? ':'.$p['port'] : '') != $_SERVER['HTTP_HOST']) {
        if ( ($p['host'] != $host) || $is_host_check ) {
            echo '<script>'.PHP_EOL;
            echo 'alert("url에 타 도메인을 지정할 수 없습니다.");'.PHP_EOL;
            echo 'document.location.href = "'.$return_url.'";'.PHP_EOL;
            echo '</script>'.PHP_EOL;
            echo '<noscript>'.PHP_EOL;
            echo '<p>'.$msg.'</p>'.PHP_EOL;
            echo '<p><a href="'.$return_url.'">돌아가기</a></p>'.PHP_EOL;
            echo '</noscript>'.PHP_EOL;
            exit;
        }
    }
}

// QUERY STRING 에 포함된 XSS 태그 제거
function clean_query_string($query, $amp=true)
{
    $qstr = trim($query);

    parse_str($qstr, $out);

    if(is_array($out)) {
        $q = array();

        foreach($out as $key=>$val) {
            $key = strip_tags(trim($key));
            $val = trim($val);

            switch($key) {
                case 'wr_id':
                    $val = (int)preg_replace('/[^0-9]/', '', $val);
                    $q[$key] = $val;
                    break;
                case 'sca':
                    $val = clean_xss_tags($val);
                    $q[$key] = $val;
                    break;
                case 'sfl':
                    $val = preg_replace("/[\<\>\'\"\\\'\\\"\%\=\(\)\s]/", "", $val);
                    $q[$key] = $val;
                    break;
                case 'stx':
                    $val = get_search_string($val);
                    $q[$key] = $val;
                    break;
                case 'sst':
                    $val = preg_replace("/[\<\>\'\"\\\'\\\"\%\=\(\)\s]/", "", $val);
                    $q[$key] = $val;
                    break;
                case 'sod':
                    $val = preg_match("/^(asc|desc)$/i", $val) ? $val : '';
                    $q[$key] = $val;
                    break;
                case 'sop':
                    $val = preg_match("/^(or|and)$/i", $val) ? $val : '';
                    $q[$key] = $val;
                    break;
                case 'spt':
                    $val = (int)preg_replace('/[^0-9]/', '', $val);
                    $q[$key] = $val;
                    break;
                case 'page':
                    $val = (int)preg_replace('/[^0-9]/', '', $val);
                    $q[$key] = $val;
                    break;
                case 'w':
                    $val = substr($val, 0, 2);
                    $q[$key] = $val;
                    break;
                case 'bo_table':
                    $val = preg_replace('/[^a-z0-9_]/i', '', $val);
                    $val = substr($val, 0, 20);
                    $q[$key] = $val;
                    break;
                case 'gr_id':
                    $val = preg_replace('/[^a-z0-9_]/i', '', $val);
                    $q[$key] = $val;
                    break;
                default:
                    $val = clean_xss_tags($val);
                    $q[$key] = $val;
                    break;
            }
        }

        if($amp)
            $sep = '&amp;';
        else
            $sep ='&';

        $str = http_build_query($q, '', $sep);
    } else {
        $str = clean_xss_tags($qstr);
    }

    return $str;
}

function get_device_change_url()
{
	if (G5_URL) {
        $href = $p['scheme'].'://'.$p['host'];
        if(isset($p['port']) && $p['port'])
        $href .= ':'.$p['port'];
	}
    $q = array();
    $device = 'device='.(G5_IS_MOBILE ? 'pc' : 'mobile');

    if($_SERVER['QUERY_STRING']) {
        foreach($_GET as $key=>$val) {
            if($key == 'device')
                continue;

            $key = strip_tags($key);
            $val = strip_tags($val);

            if($key && $val)
                $q[$key] = $val;
        }
    }

    if(!empty($q)) {
        $query = http_build_query($q, '', '&amp;');
        $href .= '?'.$query.'&amp;'.$device;
    } else {
        $href .= '?'.$device;
    }

    return $href;
}

// 스킨 path
function get_skin_path($dir, $skin)
{
    global $config;

    if(preg_match('#^theme/(.+)$#', $skin, $match)) { // 테마에 포함된 스킨이라면
        $theme_path = '';
        $cf_theme = trim($config['cf_theme']);

        $theme_path = G5_PATH.'/'.G5_THEME_DIR.'/'.$cf_theme;
        if(G5_IS_MOBILE) {
            $skin_path = $theme_path.'/'.G5_MOBILE_DIR.'/'.G5_SKIN_DIR.'/'.$dir.'/'.$match[1];
            if(!is_dir($skin_path))
                $skin_path = $theme_path.'/'.G5_SKIN_DIR.'/'.$dir.'/'.$match[1];
        } else {
            $skin_path = $theme_path.'/'.G5_SKIN_DIR.'/'.$dir.'/'.$match[1];
        }
    } else {
        if(G5_IS_MOBILE)
            $skin_path = G5_MOBILE_PATH.'/'.G5_SKIN_DIR.'/'.$dir.'/'.$skin;
        else
            $skin_path = G5_SKIN_PATH.'/'.$dir.'/'.$skin;
    }

    return $skin_path;
}

// 스킨 url
function get_skin_url($dir, $skin)
{
    $skin_path = get_skin_path($dir, $skin);

    return str_replace(G5_PATH, G5_URL, $skin_path);
}

// 발신번호 유효성 체크
function check_vaild_callback($callback){
   $_callback = preg_replace('/[^0-9]/','', $callback);

   /**
   * 1588 로시작하면 총8자리인데 7자리라 차단
   * 02 로시작하면 총9자리 또는 10자리인데 11자리라차단
   * 1366은 그자체가 원번호이기에 다른게 붙으면 차단
   * 030으로 시작하면 총10자리 또는 11자리인데 9자리라차단
   */

   if( substr($_callback,0,4) == '1588') if( strlen($_callback) != 8) return false;
   if( substr($_callback,0,2) == '02')   if( strlen($_callback) != 9  && strlen($_callback) != 10 ) return false;
   if( substr($_callback,0,3) == '030')  if( strlen($_callback) != 10 && strlen($_callback) != 11 ) return false;

   if( !preg_match("/^(02|0[3-6]\d|01(0|1|3|5|6|7|8|9)|070|080|007)\-?\d{3,4}\-?\d{4,5}$/",$_callback) &&
       !preg_match("/^(15|16|18)\d{2}\-?\d{4,5}$/",$_callback) ){
             return false;
   } else if( preg_match("/^(02|0[3-6]\d|01(0|1|3|5|6|7|8|9)|070|080)\-?0{3,4}\-?\d{4}$/",$_callback )) {
             return false;
   } else {
             return true;
   }
}

// 문자열 암복호화
class str_encrypt
{
    var $salt;
    var $lenght;

    function __construct($salt='')
    {
        if(!$salt)
            $this->salt = md5(preg_replace('/[^0-9A-Za-z]/', substr(G5_MYSQL_USER, -1), G5_MYSQL_PASSWORD));
        else
            $this->salt = $salt;

        $this->length = strlen($this->salt);
    }

    function encrypt($str)
    {
        $length = strlen($str);
        $result = '';

        for($i=0; $i<$length; $i++) {
            $char    = substr($str, $i, 1);
            $keychar = substr($this->salt, ($i % $this->length) - 1, 1);
            $char    = chr(ord($char) + ord($keychar));
            $result .= $char;
        }

        return base64_encode($result);
    }

    function decrypt($str) {
        $result = '';
        $str    = base64_decode($str);
        $length = strlen($str);

        for($i=0; $i<$length; $i++) {
            $char    = substr($str, $i, 1);
            $keychar = substr($this->salt, ($i % $this->length) - 1, 1);
            $char    = chr(ord($char) - ord($keychar));
            $result .= $char;
        }

        return $result;
    }
}

// 불법접근을 막도록 토큰을 생성하면서 토큰값을 리턴
function get_write_token($bo_table)
{
    $token = md5(uniqid(rand(), true));
    set_session('ss_write_'.$bo_table.'_token', $token);

    return $token;
}


// POST로 넘어온 토큰과 세션에 저장된 토큰 비교
function check_write_token($bo_table)
{
    if(!$bo_table)
        alert('올바른 방법으로 이용해 주십시오.', G5_URL);

    $token = get_session('ss_write_'.$bo_table.'_token');
    set_session('ss_write_'.$bo_table.'_token', '');

    if(!$token || !$_REQUEST['token'] || $token != $_REQUEST['token'])
        alert('올바른 방법으로 이용해 주십시오.', G5_URL);

    return true;
}

function get_member_profile_img($mb_id='', $width='', $height='', $alt='profile_image', $title=''){
    global $member;

    static $no_profile_cache = '';
    static $member_cache = array();
    
    $src = '';

    if( $mb_id ){
        if( isset($member_cache[$mb_id]) ){
            $src = $member_cache[$mb_id];
        } else {
            $member_img = G5_DATA_PATH.'/member_image/'.substr($mb_id,0,2).'/'.$mb_id.'.gif';
            if (is_file($member_img)) {
                $member_cache[$mb_id] = $src = str_replace(G5_DATA_PATH, G5_DATA_URL, $member_img);
            }
        }
    }

    if( !$src ){
        if( !empty($no_profile_cache) ){
            $src = $no_profile_cache;
        } else {
            // 프로필 이미지가 없을때 기본 이미지
            $no_profile_img = (defined('G5_THEME_NO_PROFILE_IMG') && G5_THEME_NO_PROFILE_IMG) ? G5_THEME_NO_PROFILE_IMG : G5_NO_PROFILE_IMG;
            $tmp = array();
            preg_match( '/src="([^"]*)"/i', $foo, $tmp );
            $no_profile_cache = $src = isset($tmp[1]) ? $tmp[1] : G5_IMG_URL.'/no_profile.gif';
        }
    }

    if( $src ){
        $attributes = array('src'=>$src, 'width'=>$width, 'height'=>$height, 'alt'=>$alt, 'title'=>$title);

        $output = '<img';
        foreach ($attributes as $name => $value) {
            if (!empty($value)) {
                $output .= sprintf(' %s="%s"', $name, $value);
            }
        }
        $output .= '>';

        return $output;
    }

    return '';
}

function get_head_title($title){
    global $g5;

    if( isset($g5['board_title']) && $g5['board_title'] ){
        $title = $g5['board_title'];
    }

    return $title;
}

function is_use_email_certify(){
    global $config;

    if( $config['cf_use_email_certify'] && function_exists('social_is_login_check') ){
        if( $config['cf_social_login_use'] && (get_session('ss_social_provider') || social_is_login_check()) ){      //소셜 로그인을 사용한다면
            $tmp = (defined('G5_SOCIAL_CERTIFY_MAIL') && G5_SOCIAL_CERTIFY_MAIL) ? 1 : 0;
            return $tmp;
        }
    }

    return $config['cf_use_email_certify'];
}

function get_real_client_ip(){

    if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
        return $_SERVER['HTTP_X_FORWARDED_FOR'];

    return $_SERVER['REMOTE_ADDR'];
}

function get_call_func_cache($func, $args=array()){
    
    static $cache = array();

    $key = md5(serialize($args));

    if( isset($cache[$func]) && isset($cache[$func][$key]) ){
        return $cache[$func][$key];
    }

    $result = null;

    try{
        $cache[$func][$key] = $result = call_user_func_array($func, $args);
    } catch (Exception $e) {
        return null;
    }
    
    return $result;
}

// include 하는 경로에 data file 경로가 포함되어 있는지 체크합니다.
function is_include_path_check($path='', $is_input='')
{
    if( $path ){
        if ($is_input){

            if( stripos($path, 'php://') !== false || stripos($path, 'zlib://') !== false || stripos($path, 'bzip2://') !== false || stripos($path, 'zip://') !== false || stripos($path, 'data:text/') !== false || stripos($path, 'data://') !== false ){
                return false;
            }

            try {
                // whether $path is unix or not
                $unipath = strlen($path)==0 || $path{0}!='/';
                $unc = substr($path,0,2)=='\\\\'?true:false;
                // attempts to detect if path is relative in which case, add cwd
                if(strpos($path,':') === false && $unipath && !$unc){
                    $path=getcwd().DIRECTORY_SEPARATOR.$path;
                    if($path{0}=='/'){
                        $unipath = false;
                    }
                }

                // resolve path parts (single dot, double dot and double delimiters)
                $path = str_replace(array('/', '\\'), DIRECTORY_SEPARATOR, $path);
                $parts = array_filter(explode(DIRECTORY_SEPARATOR, $path), 'strlen');
                $absolutes = array();
                foreach ($parts as $part) {
                    if ('.'  == $part){
                        continue;
                    }
                    if ('..' == $part) {
                        array_pop($absolutes);
                    } else {
                        $absolutes[] = $part;
                    }
                }
                $path = implode(DIRECTORY_SEPARATOR, $absolutes);
                // resolve any symlinks
                // put initial separator that could have been lost
                $path = !$unipath ? '/'.$path : $path;
                $path = $unc ? '\\\\'.$path : $path;
            } catch (Exception $e) {
                //echo 'Caught exception: ',  $e->getMessage(), "\n";
                return false;
            }

            if( preg_match('/\/data\/(file|editor)\/[A-Za-z0-9_]{1,20}\//', $path) ){
                return false;
            }
        }

        $extension = pathinfo($path, PATHINFO_EXTENSION);
        
        if($extension && preg_match('/(jpg|jpeg|png|gif|bmp|conf)$/i', $extension)) {
            return false;
        }
    }

    return true;
}

function option_array_checked($option, $arr=array()){
    $checked = '';

    if( !is_array($arr) ){
        $arr = explode(',', $arr);
    }

    if ( !empty($arr) && in_array($option, (array) $arr) ){
        $checked = 'checked="checked"';
    }

    return $checked;
}

function surem_result($result)
{
	switch ($result) {
	case 'Y': $str = "전송"; break;
	case 'N': $str = "실패"; break;
	case '2': $str = "성공"; break;
	case '4': $str = "실패"; break;		
	}
	return $str;
}

function surem_error_code($result, $errorCode)
{
	$suremCode = array(
		"Y" => array(
			"79" => "슈어엠에서 정상적으로 전송.<br>통신사로부터 결과를 못 받아온 상태"
		),
		"N" => array(
			"-1"  => "Socket 에러 or Connect 에러",
			"-2"  => "Socket 에러",
			"-3"  => "IOException, MMS 파일 경로에 없음",
			"67"  => "아이디의 발송 건수 초과 (영업 담당자에게 문의)",
			"69"  => "알 수 없는 시스템 에러",
			"70"  => "아이디의 발송 건수 초과 (영업 담당자에게 문의)",
			"72"  => "제한된 IP 주소 (슈어엠측에 문의)",
			"73"  => "USERCODE에 아이디를 잘못 입력한 경우 혹은 기본료를 미납한 경우",
			"75"  => "컬럼오류(‘SeqNo’ int 자료형 초과 입력)",
			"78"  => "컨텐츠 파일 경로 오류",
			"79"  => "컨텐츠 파일 경로 오류",
			"80"  => "수신번호가 틀린 경우 발생",
			"84"  => "컨텐츠 Type이 틀린 경우",
			"85"  => "확장자가 jpg 이외의 그림파일을 전송 하려고 할 때 발생.",
			"99"  => "기타 오류",
			"400" => "클라이언트측 요청을 서버에서 올바르게 처리하지 못 했을 때 발생한다.",
			"401" => "파라미터의 정보를 잘못 입력했을 경우 발생<br>additional_information 키가 추가적으로 반환되며 잘못된 파라미터의 정보를 안내 받을 수 있다.",
			"415" => "요청 헤더의 Content-Type 가 application/json 이 아닐 경우 발생한다.",
			"500" => "서버에서 발생한 일반적인 오류이다. (기술 문의 필요)",
			"501" => "슈어엠 측에 등록되지 않은 아이디일 경우 발생한다.",
			"502" => "yellowid_key를 발급받지 않아 사용이 허용되지 않은 경우 발생한다.",
			"505" => "요청 클라이언트의 아이피가 등록이 되어있지 않은 경우 발생한다.",
			"555" => "전송 요청에 대한 처리건이 없을 경우 발생한다."
		),
		"4" => array(
			"103"    => "메시지 형식 오류",
			"105"    => "잘못된 번호",
			"107"    => "비가입자, 결번, 서비스 정지",
			"108"    => "Power-Off",
			"303"    => "메시지 형식 오류",
			"1011"   => "기타 실패",
			"1016"   => "스팸 차단",
			"1052"   => "릴레이 서버에서 판단시, 메시지 내용 중에 SPAM 단어 포함",
			"3030"   => "데이터 복호화 에러",
			"3034"   => "메시지 길이 오류",
			"901111" => "기타 실패",
			"10666"  => "내부 필터링 (회신번호 or 단어)",
			"10777"  => "기타 실패",
			"20203"  => "필드 형식이 잘못된 경우",
			"20206"  => "TIMEOUT",
			"20207"  => "SMC 운영자가 메시지 삭제",
			"20212"  => "착신 거절",
			"20300"  => "메시지 내용 없음 / 수신자 전화번호 없음 / 스팸성 문구 삽입",
			"20400"  => "유효기간 초과",
			"20403"  => "단말기 Power Off",
			"20410"  => "잘못된 전화번호",
			"20413"  => "단말기에서 착신 거절",
			"20414"  => "기타 실패",
			"20415"  => "망 에러",
			"20418"  => "일시 서비스 정지",
			"20666"  => "세칙위반 실패",
			"20777"  => "기타 실패",
			"20999"  => "기타 실패",
			"30111"  => "동일착번제한",
			"30112"  => "레포트 수신 시간 만료",
			"30116"  => "번호 세칙 실패",
			"30202"  => "착신 가입자 없음",
			"30203"  => "비가입자, 결번, 서비스 정지",
			"30204"  => "단말기 Power-Off",
			"30207"  => "UNKNOWN / 단말기 형식 오류",
			"30209"  => "번호 이동된 가입자",
			"30210"  => "SMS 착신 전환 회수 초과",
			"30212"  => "SKT 가입자 없음",
			"30216"  => "수신번호가 오류인 경우 (자리수 부족 & 초과)",
			"30245"  => "메시지 전송 불가 (단말기 착신 거부 상태)",
			"30253"  => "전송 실패 (무선망), 단말기 일시 정지",
			"30254"  => "전송 실패 (무선망 -> 단말기단) , 가입자 VLR 없음",
			"90116"  => "회신번호 세칙 위반 실패",
			"90666"  => "키사 스팸 신고 실패",
			"90667"  => "발신번호 미등록 실패",
			"90668"  => "회신번호 세칙위반 실패",
			"90777"  => "기타 실패",
			"101000" => "성공",
			"102002" => "Content-length 오류",
			"102003" => "MIME 형식 오류 (컨텐츠 개체, 사이즈, 타입 등의 오류)",
			"103000" => "MMS 미지원 단말",
			"109013" => "번호 도용 차단서비스 가입",
			"109998" => "내부 중복 실패",
			"109999" => "기타 실패",
			"202082" => "통신사 자체 형식 오류",
			"202092" => "폰 번호가 형식에 어긋난 경우",
			"202094" => "잘못된 전화번호",
			"202101" => "이통사에서 SPAM 처리",
			"202102" => "착신 거절",
			"202172" => "서비스 불가 단말기",
			"209999" => "기타실패",
			"302003" => "미등록 사용자 정보 오류",
			"304005" => "일반적인 서비스 에러",
			"304007" => "서비스 거부 오류",
			"304301" => "미가입자 에러 오류",
			"304305" => "비가용폰 오류",
			"304307" => "일시정지 가입자 오류",
			"308012" => "글로벌 단말 Delivery Report Request 수신 / Response 전송 실패",
			"308200" => "MMSC 전송 시 알 수 없는 오류",
			"309999" => "기타 실패",
			"902101" => "올바른 컨텐츠가 아님",
			"902113" => "SOAP Element 인 Subject 정보 오류",
			"904005" => "일반적인 서비스 에러",
			"904007" => "서비스 거부 오류",
			"904305" => "비가용폰 오류",
			"904307" => "일시정지 가입자 오류",		
			"905101" => "HLR 착신 전환 조회 실패",
			"907777" => "기타 실패",
			"908012" => "글로벌 단말 Delivery Report Request 수신",
			"908200" => "MMSC 전송 시 알 수 없는 오류",
			"909998" => "중복 실패",
			"909999" => "기타 실패"		
		)
	);	
	
	return $suremCode[$result][$errorCode];
}

function surem_kind($kind)
{
	switch ($kind) {
	case 'S': $str = "SMS"; break;
	case 'M': $str = "MMS"; break;
	case 'L': $str = "국제SMS"; break;
	case 'T': $str = "알림톡"; break;		
	}
	return $str;
}


/**
 * Calls a Stored Procedure and returns the results as an array of rows.
 * @param mysqli $dbLink An open mysqli object.
 * @param string $procName The name of the procedure to call.
 * @param string $params The parameter string to be used
 * @return array An array of rows returned by the call.
 */
function c_mysqli_call($procName, $params="")
{
	global $g5;
	$dbLink = $g5['connect_db'];

    if(!$dbLink) {
        throw new Exception("The MySQLi connection is invalid.");
    }
    else
    {
        // Execute the SQL command.
        // The multy_query method is used here to get the buffered results,
        // so they can be freeded later to avoid the out of sync error.
        $sql = "CALL {$procName}({$params});";

        $sqlSuccess = $dbLink->multi_query($sql);
        if($sqlSuccess)
        {
            if($dbLink->more_results())
            {
                // Get the first buffered result set, the one with our data.
                $result = $dbLink->use_result();
                $output = array();
                // Put the rows into the outpu array
                while($row = $result->fetch_assoc())
                {
                    $output[] = $row;
                }
                // Free the first result set.
                // If you forget this one, you will get the "out of sync" error.
                $result->free();
                // Go through each remaining buffered result and free them as well.
                // This removes all extra result sets returned, clearing the way
                // for the next SQL command.
                while($dbLink->more_results() && $dbLink->next_result())
                {
                    $extraResult = $dbLink->use_result();
                    if($extraResult instanceof mysqli_result){
                        $extraResult->free();
                    }
                }
                return $output;
            }
            else
            {
                return false;
            }
        }
        else
        {
            throw new Exception("The call failed: " . $dbLink->error);
        }
    }
}



/*
밀리의서재 프로모션 쿠폰발송 $ec_gubun -> 1:오더스테이션 신규 가입자 , 2:5만원 이상 구매자
*/
function EventCouponSend($mb_id,$mb_name,$mb_hp,$ec_gubun){

	/* 핸드폰 번호가 있을때만 실행 */
	if( !empty($mb_hp) ){

		$sql = " select count(*) as cnt from event_coupon where mb_id = '$mb_id'; ";
		$row = sql_fetch($sql);

		/* 쿠폰을 받은적이 없다면 실행 */
		if( $row['cnt'] == 0 ){

			$ec_idx = "";

			/* update 할 idx 값 가져오기 */
			$sql2 = " select ec_idx , ec_code from event_coupon where trim(mb_id) = '' and trim(ec_gubun) = '' limit 1; ";
			$row2 = sql_fetch($sql2);

			$ec_idx = $row2['ec_idx'];

			if( !empty( $ec_idx ) ){
				
				$sql3 = " update event_coupon set mb_id = '$mb_id' , mb_hp = '$mb_hp' , ec_gubun = '$ec_gubun' , ec_regtime = '".G5_TIME_YMDHIS."' 
						  where ec_idx = ".$ec_idx."; ";
				sql_query($sql3);

				$_subject = "오더스테이션 X 밀리의 서재 프로모션 쿠폰";

				$_msg = " 오더스테이션 X 밀리의 서재 프로모션 당첨을 축하합니다! \n ";
				$_msg .= "밀리의 서재 프리미엄 서비스 구독권 <이용 안내> \n\n ";
				$_msg .= "* 쿠폰번호 : ".$row2['ec_code']." \n\n ";
				$_msg .= "1. http://voucher.millie.co.kr 에 접속해주세요. \n ";
				$_msg .= "2. 받으신 구독코드를 복사하여 해당 창에 입력해 주세요. \n ";
				$_msg .= "3. 회원가입 또는 로그인을 진행해주세요. \n ";
				$_msg .= "4. 구독 신청 절차 진행 후, 결제정보를 입력해주세요. (실결제는 무료 구독권 이용 후, 서비스를 유지하는 경우에만 발생합니다.) \n\n ";
				$_msg .= "* 밀리의 서재 가입시 본 쿠폰 사용과 무관하게 첫 달 무료 서비스를 이용하실 수 있습니다. \n ";
				$_msg .= "* 이미 가입하여 첫 달 무료 서비스를 이용하신 분은 본 쿠폰으로 1개월 무료 혜택을 받으실 수 있습니다. \n ";
				$_msg .= "* 밀리의 서재에 신규 가입하여 처음 구독하시는 분은 총 2개월간 이용 가능합니다. \n\n ";
				$_msg .= "* 쿠폰 사용 유효기간: 2019년 12월 31일 \n\n ";
				$_msg .= "밀리의 서재 고객센터 070-7510-5415 \n ";
				$_msg .= "평일 10:00 ~ 18:00 | 점심시간 13:00 ~ 14:00 ";
				
				/* MMS 보내기 */
				$sql4 = " insert into orderstation_sms.SUREData(
							USERCODE, 
							REQNAME, 
							REQPHONE, 
							CALLNAME, 
							CALLPHONE, 
							SUBJECT, 
							MSG, 
							REQTIME, 
							RESULT, 
							KIND
						  )values(
							'onkorderstn',
							'오더스테이션',
							'15445462',
							'".$mb_name."',
							'".$mb_hp."',
							'".$_subject."',
							'".$_msg."',
							'00000000000000',
							'0',
							'M'
						  );
				";
				sql_query($sql4);

			}

		}

	}

	return "";

}


function filefilter($data){
	
	$returnTxt = "";
	$returnTxt = preg_replace ("/[ #\&\+\-%@=\/\\\:;,\'\"\^`~\|\!\?\*$#<>()\[\]\{\}]/i", "_",  $data);
	return $returnTxt;

}

function return_it_info_gubun($GetValue){

	$returnText = "";

	$GetValue = trim($GetValue);

	if( $GetValue == "의류" ){

		$returnText = "wear";
	
	}else if( $GetValue == "구두/신발" ){

		$returnText = "shoes";
	
	}else if( $GetValue == "가방" ){

		$returnText = "bag";
	
	}else if( $GetValue == "패선잡화(모자/벨트/액세서리)" ){

		$returnText = "fashion";
	
	}else if( $GetValue == "침구류/커튼" ){

		$returnText = "bedding";
	
	}else if( $GetValue == "가구(침대/소파/싱크대/DIY제품)" ){

		$returnText = "furniture";
	
	}else if( $GetValue == "영상가전(TV류)" ){

		$returnText = "image_appliances";
	
	}else if( $GetValue == "가정용전기제품(냉장고/세탁기/식기세척기/전자레인지)" ){

		$returnText = "home_appliances";
	
	}else if( $GetValue == "계절가전(에어컨/온풍기)" ){

		$returnText = "season_appliances";
	
	}else if( $GetValue == "사무용기기(컴퓨터/노트북/프린터)" ){

		$returnText = "office_appliances";
	
	}else if( $GetValue == "광학기기(디지털카메라/캠코더)" ){

		$returnText = "optics_appliances";
	
	}else if( $GetValue == "소형전자(MP3/전자사전등)" ){

		$returnText = "microelectronics";
	
	}else if( $GetValue == "휴대폰" ){

		$returnText = "mobile";
	
	}else if( $GetValue == "네비게이션" ){

		$returnText = "navigation";
	
	}else if( $GetValue == "자동차용품(자동차부품/기타자동차용품)" ){

		$returnText = "car";
	
	}else if( $GetValue == "의료기기" ){

		$returnText = "medical";
	
	}else if( $GetValue == "주방용품" ){

		$returnText = "kitchenware";
	
	}else if( $GetValue == "화장품" ){

		$returnText = "cosmetics";
	
	}else if( $GetValue == "귀금속/보석/시계류" ){

		$returnText = "jewelry";
	
	}else if( $GetValue == "식품(농수산물)" ){

		$returnText = "food";
	
	}else if( $GetValue == "가공식품" ){

		$returnText = "general_food";
	
	}else if( $GetValue == "건강기능식품" ){

		$returnText = "diet_food";
	
	}else if( $GetValue == "영유아용품" ){

		$returnText = "kids";
	
	}else if( $GetValue == "악기" ){

		$returnText = "instrument";
	
	}else if( $GetValue == "스포츠용품" ){

		$returnText = "sports";
	
	}else if( $GetValue == "서적" ){

		$returnText = "books";
	
	}else if( $GetValue == "호텔/팬션예약" ){

		$returnText = "reserve";
	
	}else if( $GetValue == "여행패키지" ){

		$returnText = "travel";
	
	}else if( $GetValue == "항공권" ){

		$returnText = "airline_ticket";
	
	}else if( $GetValue == "자동차대여서비스(렌터카)" ){

		$returnText = "rent_car";
	
	}else if( $GetValue == "물품대여서비스(정수기,비데,공기청정기 등)" ){

		$returnText = "rental_water";
	
	}else if( $GetValue == "물품대여서비스(서적,유아용품,행사용품 등)" ){

		$returnText = "rental_etc";
	
	}else if( $GetValue == "디지털콘텐츠(음원,게임,인터넷강의 등)" ){

		$returnText = "digital_contents";
	
	}else if( $GetValue == "상품권/쿠폰" ){

		$returnText = "gift_card";
	
	}else if( $GetValue == "기타" ){

		$returnText = "etc";
	
	}

	return $returnText;

}

function return_it_info_value($GetValue){

	$returnText = "";

	$GetValue = trim($GetValue);

	if( $GetValue == "의류" ){

		$returnText = "a:8:{s:8:\"material\";s:22:\"상품페이지 참고\";s:5:\"color\";s:22:\"상품페이지 참고\";s:4:\"size\";s:22:\"상품페이지 참고\";s:5:\"maker\";s:22:\"상품페이지 참고\";s:7:\"caution\";s:22:\"상품페이지 참고\";s:16:\"manufacturing_ym\";s:22:\"상품페이지 참고\";s:8:\"warranty\";s:22:\"상품페이지 참고\";s:2:\"as\";s:22:\"상품페이지 참고\";}";
	
	}else if( $GetValue == "구두/신발" ){

		$returnText = "a:9:{s:8:\"material\";s:22:\"상품페이지 참고\";s:5:\"color\";s:22:\"상품페이지 참고\";s:4:\"size\";s:22:\"상품페이지 참고\";s:6:\"height\";s:22:\"상품페이지 참고\";s:5:\"maker\";s:22:\"상품페이지 참고\";s:6:\"madein\";s:22:\"상품페이지 참고\";s:7:\"caution\";s:22:\"상품페이지 참고\";s:8:\"warranty\";s:22:\"상품페이지 참고\";s:2:\"as\";s:22:\"상품페이지 참고\";}";
	
	}else if( $GetValue == "가방" ){

		$returnText = "a:9:{s:4:\"kind\";s:22:\"상품페이지 참고\";s:8:\"material\";s:22:\"상품페이지 참고\";s:5:\"color\";s:22:\"상품페이지 참고\";s:4:\"size\";s:22:\"상품페이지 참고\";s:5:\"maker\";s:22:\"상품페이지 참고\";s:6:\"madein\";s:22:\"상품페이지 참고\";s:7:\"caution\";s:22:\"상품페이지 참고\";s:8:\"warranty\";s:22:\"상품페이지 참고\";s:2:\"as\";s:22:\"상품페이지 참고\";}";
	
	}else if( $GetValue == "패선잡화(모자/벨트/액세서리)" ){

		$returnText = "a:8:{s:4:\"kind\";s:22:\"상품페이지 참고\";s:8:\"material\";s:22:\"상품페이지 참고\";s:4:\"size\";s:22:\"상품페이지 참고\";s:5:\"maker\";s:22:\"상품페이지 참고\";s:6:\"madein\";s:22:\"상품페이지 참고\";s:7:\"caution\";s:22:\"상품페이지 참고\";s:8:\"warranty\";s:22:\"상품페이지 참고\";s:2:\"as\";s:22:\"상품페이지 참고\";}";
	
	}else if( $GetValue == "침구류/커튼" ){

		$returnText = "a:9:{s:8:\"material\";s:22:\"상품페이지 참고\";s:5:\"color\";s:22:\"상품페이지 참고\";s:4:\"size\";s:22:\"상품페이지 참고\";s:9:\"component\";s:22:\"상품페이지 참고\";s:5:\"maker\";s:22:\"상품페이지 참고\";s:6:\"madein\";s:22:\"상품페이지 참고\";s:7:\"caution\";s:22:\"상품페이지 참고\";s:8:\"warranty\";s:22:\"상품페이지 참고\";s:2:\"as\";s:22:\"상품페이지 참고\";}";
	
	}else if( $GetValue == "가구(침대/소파/싱크대/DIY제품)" ){

		$returnText = "a:11:{s:12:\"product_name\";s:22:\"상품페이지 참고\";s:13:\"certification\";s:22:\"상품페이지 참고\";s:5:\"color\";s:22:\"상품페이지 참고\";s:9:\"component\";s:22:\"상품페이지 참고\";s:8:\"material\";s:22:\"상품페이지 참고\";s:5:\"maker\";s:22:\"상품페이지 참고\";s:6:\"madein\";s:22:\"상품페이지 참고\";s:4:\"size\";s:22:\"상품페이지 참고\";s:8:\"delivery\";s:22:\"상품페이지 참고\";s:8:\"warranty\";s:22:\"상품페이지 참고\";s:2:\"as\";s:22:\"상품페이지 참고\";}";
	
	}else if( $GetValue == "영상가전(TV류)" ){

		$returnText = "a:8:{s:8:\"material\";s:22:\"상품페이지 참고\";s:5:\"color\";s:22:\"상품페이지 참고\";s:4:\"size\";s:22:\"상품페이지 참고\";s:5:\"maker\";s:22:\"상품페이지 참고\";s:7:\"caution\";s:22:\"상품페이지 참고\";s:16:\"manufacturing_ym\";s:22:\"상품페이지 참고\";s:8:\"warranty\";s:22:\"상품페이지 참고\";s:2:\"as\";s:22:\"상품페이지 참고\";}";
	
	}else if( $GetValue == "가정용전기제품(냉장고/세탁기/식기세척기/전자레인지)" ){

		$returnText = "a:13:{s:12:\"product_name\";s:22:\"상품페이지 참고\";s:10:\"model_name\";s:22:\"상품페이지 참고\";s:13:\"certification\";s:22:\"상품페이지 참고\";s:13:\"rated_voltage\";s:22:\"상품페이지 참고\";s:17:\"power_consumption\";s:22:\"상품페이지 참고\";s:17:\"energy_efficiency\";s:22:\"상품페이지 참고\";s:13:\"released_date\";s:22:\"상품페이지 참고\";s:5:\"maker\";s:22:\"상품페이지 참고\";s:6:\"madein\";s:22:\"상품페이지 참고\";s:4:\"size\";s:22:\"상품페이지 참고\";s:21:\"display_specification\";s:22:\"상품페이지 참고\";s:8:\"warranty\";s:22:\"상품페이지 참고\";s:2:\"as\";s:22:\"상품페이지 참고\";}";
	
	}else if( $GetValue == "계절가전(에어컨/온풍기)" ){

		$returnText = "a:14:{s:12:\"product_name\";s:22:\"상품페이지 참고\";s:10:\"model_name\";s:22:\"상품페이지 참고\";s:13:\"certification\";s:22:\"상품페이지 참고\";s:13:\"rated_voltage\";s:22:\"상품페이지 참고\";s:17:\"power_consumption\";s:22:\"상품페이지 참고\";s:17:\"energy_efficiency\";s:22:\"상품페이지 참고\";s:13:\"released_date\";s:22:\"상품페이지 참고\";s:5:\"maker\";s:22:\"상품페이지 참고\";s:6:\"madein\";s:22:\"상품페이지 참고\";s:4:\"size\";s:22:\"상품페이지 참고\";s:4:\"area\";s:22:\"상품페이지 참고\";s:18:\"installation_costs\";s:22:\"상품페이지 참고\";s:8:\"warranty\";s:22:\"상품페이지 참고\";s:2:\"as\";s:22:\"상품페이지 참고\";}";
	
	}else if( $GetValue == "사무용기기(컴퓨터/노트북/프린터)" ){

		$returnText = "a:14:{s:12:\"product_name\";s:22:\"상품페이지 참고\";s:10:\"model_name\";s:22:\"상품페이지 참고\";s:13:\"certification\";s:22:\"상품페이지 참고\";s:13:\"rated_voltage\";s:22:\"상품페이지 참고\";s:17:\"power_consumption\";s:22:\"상품페이지 참고\";s:17:\"energy_efficiency\";s:22:\"상품페이지 참고\";s:13:\"released_date\";s:22:\"상품페이지 참고\";s:5:\"maker\";s:22:\"상품페이지 참고\";s:6:\"madein\";s:22:\"상품페이지 참고\";s:4:\"size\";s:22:\"상품페이지 참고\";s:6:\"weight\";s:22:\"상품페이지 참고\";s:13:\"specification\";s:22:\"상품페이지 참고\";s:8:\"warranty\";s:22:\"상품페이지 참고\";s:2:\"as\";s:22:\"상품페이지 참고\";}";
	
	}else if( $GetValue == "광학기기(디지털카메라/캠코더)" ){

		$returnText = "a:11:{s:12:\"product_name\";s:22:\"상품페이지 참고\";s:10:\"model_name\";s:22:\"상품페이지 참고\";s:13:\"certification\";s:22:\"상품페이지 참고\";s:13:\"released_date\";s:22:\"상품페이지 참고\";s:5:\"maker\";s:22:\"상품페이지 참고\";s:6:\"madein\";s:22:\"상품페이지 참고\";s:4:\"size\";s:22:\"상품페이지 참고\";s:6:\"weight\";s:22:\"상품페이지 참고\";s:13:\"specification\";s:22:\"상품페이지 참고\";s:8:\"warranty\";s:22:\"상품페이지 참고\";s:2:\"as\";s:22:\"상품페이지 참고\";}";
	
	}else if( $GetValue == "소형전자(MP3/전자사전등)" ){

		$returnText = "a:13:{s:12:\"product_name\";s:22:\"상품페이지 참고\";s:10:\"model_name\";s:22:\"상품페이지 참고\";s:13:\"certification\";s:22:\"상품페이지 참고\";s:13:\"rated_voltage\";s:22:\"상품페이지 참고\";s:17:\"power_consumption\";s:22:\"상품페이지 참고\";s:13:\"released_date\";s:22:\"상품페이지 참고\";s:5:\"maker\";s:22:\"상품페이지 참고\";s:6:\"madein\";s:22:\"상품페이지 참고\";s:4:\"size\";s:22:\"상품페이지 참고\";s:6:\"weight\";s:22:\"상품페이지 참고\";s:13:\"specification\";s:22:\"상품페이지 참고\";s:8:\"warranty\";s:22:\"상품페이지 참고\";s:2:\"as\";s:22:\"상품페이지 참고\";}";
	
	}else if( $GetValue == "휴대폰" ){

		$returnText = "a:14:{s:12:\"product_name\";s:22:\"상품페이지 참고\";s:10:\"model_name\";s:22:\"상품페이지 참고\";s:13:\"certification\";s:22:\"상품페이지 참고\";s:13:\"released_date\";s:22:\"상품페이지 참고\";s:5:\"maker\";s:22:\"상품페이지 참고\";s:6:\"madein\";s:22:\"상품페이지 참고\";s:4:\"size\";s:22:\"상품페이지 참고\";s:6:\"weight\";s:22:\"상품페이지 참고\";s:7:\"telecom\";s:22:\"상품페이지 참고\";s:12:\"join_process\";s:22:\"상품페이지 참고\";s:12:\"extra_burden\";s:22:\"상품페이지 참고\";s:13:\"specification\";s:22:\"상품페이지 참고\";s:8:\"warranty\";s:22:\"상품페이지 참고\";s:2:\"as\";s:22:\"상품페이지 참고\";}";
	
	}else if( $GetValue == "네비게이션" ){

		$returnText = "a:15:{s:12:\"product_name\";s:22:\"상품페이지 참고\";s:10:\"model_name\";s:22:\"상품페이지 참고\";s:13:\"certification\";s:22:\"상품페이지 참고\";s:13:\"rated_voltage\";s:22:\"상품페이지 참고\";s:17:\"power_consumption\";s:22:\"상품페이지 참고\";s:13:\"released_date\";s:22:\"상품페이지 참고\";s:5:\"maker\";s:22:\"상품페이지 참고\";s:6:\"madein\";s:22:\"상품페이지 참고\";s:4:\"size\";s:22:\"상품페이지 참고\";s:6:\"weight\";s:22:\"상품페이지 참고\";s:13:\"specification\";s:22:\"상품페이지 참고\";s:11:\"update_cost\";s:22:\"상품페이지 참고\";s:15:\"freecost_period\";s:22:\"상품페이지 참고\";s:8:\"warranty\";s:22:\"상품페이지 참고\";s:2:\"as\";s:22:\"상품페이지 참고\";}";
	
	}else if( $GetValue == "자동차용품(자동차부품/기타자동차용품)" ){

		$returnText = "a:10:{s:12:\"product_name\";s:22:\"상품페이지 참고\";s:10:\"model_name\";s:22:\"상품페이지 참고\";s:13:\"released_date\";s:22:\"상품페이지 참고\";s:13:\"certification\";s:22:\"상품페이지 참고\";s:5:\"maker\";s:22:\"상품페이지 참고\";s:6:\"madein\";s:22:\"상품페이지 참고\";s:4:\"size\";s:22:\"상품페이지 참고\";s:11:\"apply_model\";s:22:\"상품페이지 참고\";s:8:\"warranty\";s:22:\"상품페이지 참고\";s:2:\"as\";s:22:\"상품페이지 참고\";}";
	
	}else if( $GetValue == "의료기기" ){

		$returnText = "a:15:{s:12:\"product_name\";s:22:\"상품페이지 참고\";s:10:\"model_name\";s:22:\"상품페이지 참고\";s:14:\"license_number\";s:22:\"상품페이지 참고\";s:11:\"advertising\";s:22:\"상품페이지 참고\";s:13:\"certification\";s:22:\"상품페이지 참고\";s:13:\"rated_voltage\";s:22:\"상품페이지 참고\";s:17:\"power_consumption\";s:22:\"상품페이지 참고\";s:13:\"released_date\";s:22:\"상품페이지 참고\";s:5:\"maker\";s:22:\"상품페이지 참고\";s:6:\"madein\";s:22:\"상품페이지 참고\";s:18:\"appliances_purpose\";s:22:\"상품페이지 참고\";s:16:\"appliances_usage\";s:22:\"상품페이지 참고\";s:21:\"display_specification\";s:22:\"상품페이지 참고\";s:8:\"warranty\";s:22:\"상품페이지 참고\";s:2:\"as\";s:22:\"상품페이지 참고\";}";
	
	}else if( $GetValue == "주방용품" ){

		$returnText = "a:11:{s:12:\"product_name\";s:22:\"상품페이지 참고\";s:10:\"model_name\";s:22:\"상품페이지 참고\";s:8:\"material\";s:22:\"상품페이지 참고\";s:9:\"component\";s:22:\"상품페이지 참고\";s:4:\"size\";s:22:\"상품페이지 참고\";s:13:\"released_date\";s:22:\"상품페이지 참고\";s:5:\"maker\";s:22:\"상품페이지 참고\";s:6:\"madein\";s:22:\"상품페이지 참고\";s:18:\"import_declaration\";s:22:\"상품페이지 참고\";s:8:\"warranty\";s:22:\"상품페이지 참고\";s:2:\"as\";s:22:\"상품페이지 참고\";}";
	
	}else if( $GetValue == "화장품" ){

		$returnText = "a:12:{s:8:\"capacity\";s:22:\"상품페이지 참고\";s:13:\"specification\";s:22:\"상품페이지 참고\";s:15:\"expiration_date\";s:22:\"상품페이지 참고\";s:5:\"usage\";s:22:\"상품페이지 참고\";s:5:\"maker\";s:22:\"상품페이지 참고\";s:11:\"distributor\";s:22:\"상품페이지 참고\";s:6:\"madein\";s:22:\"상품페이지 참고\";s:14:\"mainingredient\";s:22:\"상품페이지 참고\";s:13:\"certification\";s:22:\"상품페이지 참고\";s:7:\"caution\";s:22:\"상품페이지 참고\";s:8:\"warranty\";s:22:\"상품페이지 참고\";s:2:\"as\";s:22:\"상품페이지 참고\";}";
	
	}else if( $GetValue == "귀금속/보석/시계류" ){

		$returnText = "a:12:{s:8:\"material\";s:22:\"상품페이지 참고\";s:6:\"purity\";s:22:\"상품페이지 참고\";s:4:\"band\";s:22:\"상품페이지 참고\";s:6:\"weight\";s:22:\"상품페이지 참고\";s:5:\"maker\";s:22:\"상품페이지 참고\";s:6:\"madein\";s:22:\"상품페이지 참고\";s:4:\"size\";s:22:\"상품페이지 참고\";s:7:\"caution\";s:22:\"상품페이지 참고\";s:13:\"specification\";s:22:\"상품페이지 참고\";s:16:\"provide_warranty\";s:22:\"상품페이지 참고\";s:8:\"warranty\";s:22:\"상품페이지 참고\";s:2:\"as\";s:22:\"상품페이지 참고\";}";
	
	}else if( $GetValue == "식품(농수산물)" ){

		$returnText = "a:11:{s:6:\"weight\";s:22:\"상품페이지 참고\";s:8:\"quantity\";s:22:\"상품페이지 참고\";s:4:\"size\";s:22:\"상품페이지 참고\";s:8:\"producer\";s:22:\"상품페이지 참고\";s:6:\"origin\";s:22:\"상품페이지 참고\";s:17:\"manufacturing_ymd\";s:22:\"상품페이지 참고\";s:15:\"expiration_date\";s:22:\"상품페이지 참고\";s:11:\"law_content\";s:22:\"상품페이지 참고\";s:19:\"product_composition\";s:22:\"상품페이지 참고\";s:4:\"keep\";s:22:\"상품페이지 참고\";s:2:\"as\";s:22:\"상품페이지 참고\";}";
	
	}else if( $GetValue == "가공식품" ){

		$returnText = "a:13:{s:9:\"food_type\";s:22:\"상품페이지 참고\";s:8:\"producer\";s:22:\"상품페이지 참고\";s:8:\"location\";s:22:\"상품페이지 참고\";s:17:\"manufacturing_ymd\";s:22:\"상품페이지 참고\";s:15:\"expiration_date\";s:22:\"상품페이지 참고\";s:6:\"weight\";s:22:\"상품페이지 참고\";s:8:\"quantity\";s:22:\"상품페이지 참고\";s:11:\"ingredients\";s:22:\"상품페이지 참고\";s:19:\"nutrition_component\";s:22:\"상품페이지 참고\";s:20:\"genetically_modified\";s:22:\"상품페이지 참고\";s:9:\"baby_food\";s:22:\"상품페이지 참고\";s:13:\"imported_food\";s:22:\"상품페이지 참고\";s:2:\"as\";s:22:\"상품페이지 참고\";}";
	
	}else if( $GetValue == "건강기능식품" ){

		$returnText = "a:16:{s:9:\"food_type\";s:22:\"상품페이지 참고\";s:8:\"producer\";s:22:\"상품페이지 참고\";s:8:\"location\";s:22:\"상품페이지 참고\";s:17:\"manufacturing_ymd\";s:22:\"상품페이지 참고\";s:15:\"expiration_date\";s:22:\"상품페이지 참고\";s:6:\"waight\";s:22:\"상품페이지 참고\";s:8:\"quantity\";s:22:\"상품페이지 참고\";s:11:\"ingredients\";s:22:\"상품페이지 참고\";s:9:\"nutrition\";s:22:\"상품페이지 참고\";s:13:\"specification\";s:22:\"상품페이지 참고\";s:6:\"intake\";s:22:\"상품페이지 참고\";s:7:\"disease\";s:22:\"상품페이지 참고\";s:20:\"genetically_modified\";s:22:\"상품페이지 참고\";s:10:\"display_ad\";s:22:\"상품페이지 참고\";s:13:\"imported_food\";s:22:\"상품페이지 참고\";s:2:\"as\";s:22:\"상품페이지 참고\";}";
	
	}else if( $GetValue == "영유아용품" ){

		$returnText = "a:14:{s:12:\"product_name\";s:22:\"상품페이지 참고\";s:10:\"model_name\";s:22:\"상품페이지 참고\";s:13:\"certification\";s:22:\"상품페이지 참고\";s:4:\"size\";s:22:\"상품페이지 참고\";s:6:\"weight\";s:22:\"상품페이지 참고\";s:5:\"color\";s:22:\"상품페이지 참고\";s:8:\"material\";s:22:\"상품페이지 참고\";s:3:\"age\";s:22:\"상품페이지 참고\";s:13:\"released_date\";s:22:\"상품페이지 참고\";s:5:\"maker\";s:22:\"상품페이지 참고\";s:6:\"madein\";s:22:\"상품페이지 참고\";s:7:\"caution\";s:22:\"상품페이지 참고\";s:8:\"warranty\";s:22:\"상품페이지 참고\";s:2:\"as\";s:22:\"상품페이지 참고\";}";
	
	}else if( $GetValue == "악기" ){

		$returnText = "a:10:{s:4:\"size\";s:22:\"상품페이지 참고\";s:5:\"color\";s:22:\"상품페이지 참고\";s:8:\"material\";s:22:\"상품페이지 참고\";s:10:\"components\";s:22:\"상품페이지 참고\";s:13:\"released_date\";s:22:\"상품페이지 참고\";s:5:\"maker\";s:22:\"상품페이지 참고\";s:6:\"madein\";s:22:\"상품페이지 참고\";s:23:\"detailed_specifications\";s:22:\"상품페이지 참고\";s:8:\"warranty\";s:22:\"상품페이지 참고\";s:2:\"as\";s:22:\"상품페이지 참고\";}";
	
	}else if( $GetValue == "스포츠용품" ){

		$returnText = "a:13:{s:12:\"product_name\";s:22:\"상품페이지 참고\";s:10:\"model_name\";s:22:\"상품페이지 참고\";s:4:\"size\";s:22:\"상품페이지 참고\";s:6:\"weight\";s:22:\"상품페이지 참고\";s:5:\"color\";s:22:\"상품페이지 참고\";s:8:\"material\";s:22:\"상품페이지 참고\";s:10:\"components\";s:22:\"상품페이지 참고\";s:13:\"released_date\";s:22:\"상품페이지 참고\";s:5:\"maker\";s:22:\"상품페이지 참고\";s:6:\"madein\";s:22:\"상품페이지 참고\";s:23:\"detailed_specifications\";s:22:\"상품페이지 참고\";s:8:\"warranty\";s:22:\"상품페이지 참고\";s:2:\"as\";s:22:\"상품페이지 참고\";}";
	
	}else if( $GetValue == "서적" ){

		$returnText = "a:8:{s:12:\"product_name\";s:22:\"상품페이지 참고\";s:6:\"author\";s:22:\"상품페이지 참고\";s:9:\"publisher\";s:22:\"상품페이지 참고\";s:4:\"size\";s:22:\"상품페이지 참고\";s:5:\"pages\";s:22:\"상품페이지 참고\";s:10:\"components\";s:22:\"상품페이지 참고\";s:12:\"publish_date\";s:22:\"상품페이지 참고\";s:11:\"description\";s:22:\"상품페이지 참고\";}";
	
	}else if( $GetValue == "호텔/팬션예약" ){

		$returnText = "a:10:{s:8:\"location\";s:22:\"상품페이지 참고\";s:13:\"lodgment_type\";s:22:\"상품페이지 참고\";s:5:\"grade\";s:22:\"상품페이지 참고\";s:9:\"room_type\";s:22:\"상품페이지 참고\";s:13:\"room_capacity\";s:22:\"상품페이지 참고\";s:19:\"extra_person_charge\";s:22:\"상품페이지 참고\";s:10:\"facilities\";s:22:\"상품페이지 참고\";s:16:\"provided_service\";s:22:\"상품페이지 참고\";s:19:\"cancellation_policy\";s:22:\"상품페이지 참고\";s:16:\"booking_contacts\";s:22:\"상품페이지 참고\";}";
	
	}else if( $GetValue == "여행패키지" ){

		$returnText = "a:12:{s:13:\"travel_agency\";s:22:\"상품페이지 참고\";s:6:\"flight\";s:22:\"상품페이지 참고\";s:13:\"travel_period\";s:22:\"상품페이지 참고\";s:8:\"schedule\";s:22:\"상품페이지 참고\";s:14:\"maximum_people\";s:22:\"상품페이지 참고\";s:14:\"minimum_people\";s:22:\"상품페이지 참고\";s:17:\"accomodation_info\";s:22:\"상품페이지 참고\";s:7:\"details\";s:22:\"상품페이지 참고\";s:17:\"additional_charge\";s:22:\"상품페이지 참고\";s:19:\"cancellation_policy\";s:22:\"상품페이지 참고\";s:15:\"travel_warnings\";s:22:\"상품페이지 참고\";s:16:\"booking_contacts\";s:22:\"상품페이지 참고\";}";
	
	}else if( $GetValue == "항공권" ){

		$returnText = "a:9:{s:16:\"charge_condition\";s:22:\"상품페이지 참고\";s:10:\"round_trip\";s:22:\"상품페이지 참고\";s:15:\"expiration_date\";s:22:\"상품페이지 참고\";s:11:\"restriction\";s:22:\"상품페이지 참고\";s:20:\"ticket_delivery_mean\";s:22:\"상품페이지 참고\";s:9:\"seat_type\";s:22:\"상품페이지 참고\";s:17:\"additional_charge\";s:22:\"상품페이지 참고\";s:19:\"cancellation_policy\";s:22:\"상품페이지 참고\";s:16:\"booking_contacts\";s:22:\"상품페이지 참고\";}";
	
	}else if( $GetValue == "자동차대여서비스(렌터카)" ){

		$returnText = "a:7:{s:5:\"model\";s:22:\"상품페이지 참고\";s:18:\"ownership_transfer\";s:22:\"상품페이지 참고\";s:17:\"additional_charge\";s:22:\"상품페이지 참고\";s:9:\"fuel_cost\";s:22:\"상품페이지 참고\";s:17:\"vehicle_breakdown\";s:22:\"상품페이지 참고\";s:19:\"cancellation_policy\";s:22:\"상품페이지 참고\";s:2:\"as\";s:22:\"상품페이지 참고\";}";
	
	}else if( $GetValue == "물품대여서비스(정수기,비데,공기청정기 등)" ){

		$returnText = "a:8:{s:12:\"product_name\";s:22:\"상품페이지 참고\";s:10:\"model_name\";s:22:\"상품페이지 참고\";s:21:\"transfer_of_ownership\";s:22:\"상품페이지 참고\";s:11:\"maintenance\";s:22:\"상품페이지 참고\";s:23:\"consumer_responsibility\";s:22:\"상품페이지 참고\";s:6:\"refund\";s:22:\"상품페이지 참고\";s:13:\"specification\";s:22:\"상품페이지 참고\";s:2:\"as\";s:22:\"상품페이지 참고\";}";
	
	}else if( $GetValue == "물품대여서비스(서적,유아용품,행사용품 등)" ){

		$returnText = "a:6:{s:12:\"product_name\";s:22:\"상품페이지 참고\";s:10:\"model_name\";s:22:\"상품페이지 참고\";s:21:\"transfer_of_ownership\";s:22:\"상품페이지 참고\";s:23:\"consumer_responsibility\";s:22:\"상품페이지 참고\";s:6:\"refund\";s:22:\"상품페이지 참고\";s:2:\"as\";s:22:\"상품페이지 참고\";}";
	
	}else if( $GetValue == "디지털콘텐츠(음원,게임,인터넷강의 등)" ){

		$returnText = "a:8:{s:8:\"producer\";s:22:\"상품페이지 참고\";s:12:\"terms_of_use\";s:22:\"상품페이지 참고\";s:10:\"use_period\";s:22:\"상품페이지 참고\";s:14:\"product_offers\";s:22:\"상품페이지 참고\";s:14:\"minimum_system\";s:22:\"상품페이지 참고\";s:21:\"transfer_of_ownership\";s:22:\"상품페이지 참고\";s:11:\"maintenance\";s:22:\"상품페이지 참고\";s:2:\"as\";s:22:\"상품페이지 참고\";}";
	
	}else if( $GetValue == "상품권/쿠폰" ){

		$returnText = "a:6:{s:6:\"isseur\";s:22:\"상품페이지 참고\";s:15:\"expiration_date\";s:22:\"상품페이지 참고\";s:12:\"terms_of_use\";s:22:\"상품페이지 참고\";s:9:\"use_store\";s:22:\"상품페이지 참고\";s:13:\"refund_policy\";s:22:\"상품페이지 참고\";s:2:\"as\";s:22:\"상품페이지 참고\";}";
	
	}else if( $GetValue == "기타" ){

		$returnText = "a:6:{s:12:\"product_name\";s:22:\"상품페이지 참고\";s:10:\"model_name\";s:22:\"상품페이지 참고\";s:16:\"certified_by_law\";s:22:\"상품페이지 참고\";s:6:\"origin\";s:22:\"상품페이지 참고\";s:5:\"maker\";s:22:\"상품페이지 참고\";s:2:\"as\";s:22:\"상품페이지 참고\";}";
	
	}

	return $returnText;

}

//결제요청시 사용되는 카드코드값
function GetBankName( $code ) {
	$arrBank = array(
		"11" => "비씨카드",
		"12" => "삼성카드",
		"14" => "신한카드(구.LG카드 포함)",
		"21" => "글로벌 VISA",
		"22" => "글로벌 MASTER",
		"23" => "글로벌 JCB",
		"26" => "중국은련카드",
		"32" => "광주카드",
		"33" => "전북카드",
		"34" => "하나카드",
		"35" => "산업카드",
		"41" => "NH카드",
		"43" => "씨티카드",
		"44" => "우리카드",
		"48" => "신협체크카드",
		"51" => "수협카드",
		"52" => "제주카드",
		"54" => "MG새마을금고체크",
		"55" => "케이뱅크카드",
		"56" => "카카오뱅크",
		"71" => "우체국체크",
		"95" => "저축은행체크",
		"01" => "외환카드",
		"03" => "롯데카드",
		"04" => "현대카드",
		"06" => "국민카드"
	);

	return $arrBank[ $code ];
}

// 파일 체크
function file_check ($path) {
    $result = false;
    $file_path = '../'.$path;

    if (@file_exists($file_path)) {
        $result = true;
    }

    return $result;
}

// Dummy Image 생성
function dummyimage ($width, $height) {
    $size = $width.'x'.$height;
    $img_url = "https://dummyimage.com/".$size."/ffc091/ffc091";

    return $img_url;
}

// 관리자 로그
function insert_admin_log($depth1, $depth2, $subject='', $content='', $key='', $seq='', $url='', $data='', $data_old='')
{
    global $is_admin, $g5, $member, $admin_menu, $admin_sub_menu;

    if ($is_admin == 'vender') {
        return;
    }

    //$user_agent = escape_trim(clean_xss_tags($_SERVER['HTTP_USER_AGENT']));
    $year = date('Y');
    $table_name = 'tbl_admin_log_'.$year;

    // 해당 테이블이 없는 경우 생성
    if (!sql_query(" DESCRIBE ".$table_name, false)) {
        sql_query("
        CREATE TABLE `$table_name` (
            `al_num` INT(11) NOT NULL AUTO_INCREMENT,
            `al_admin_id` VARCHAR(25) NULL DEFAULT '' COLLATE 'utf8_general_ci',
            `al_url` VARCHAR(50) NULL DEFAULT '' COLLATE 'utf8_general_ci',
            `al_depth1` VARCHAR(50) NULL DEFAULT '' COMMENT '1뎁스 메뉴' COLLATE 'utf8_general_ci',
            `al_depth2` VARCHAR(50) NULL DEFAULT '' COMMENT '2뎁스 메뉴' COLLATE 'utf8_general_ci',
            `al_subject` VARCHAR(50) NULL DEFAULT '' COMMENT '작업내역 명칭' COLLATE 'utf8_general_ci',
            `al_content` VARCHAR(50) NULL DEFAULT '' COMMENT '설명' COLLATE 'utf8_general_ci',
            `al_key` VARCHAR(50) NULL DEFAULT '' COMMENT '고유 아이디' COLLATE 'utf8_general_ci',
            `al_seq` VARCHAR(2) NULL DEFAULT '' COMMENT '고유 아이디 N번' COLLATE 'utf8_general_ci',
            `al_data` TEXT NULL COLLATE 'utf8_general_ci',
            `al_data_old` TEXT NULL COMMENT '변경 전 데이터 (UPDATE시 입력)' COLLATE 'utf8_general_ci',
            `al_ip` VARCHAR(25) NULL DEFAULT '' COLLATE 'utf8_general_ci',
            `al_date` DATETIME NULL DEFAULT '0000-00-00 00:00:00',
            PRIMARY KEY (`al_num`) USING BTREE
        )
        COMMENT='관리자 로그 테이블'
        COLLATE='utf8_general_ci'
        ENGINE=InnoDB
        ;", false);
    }

    // $data json_encode 필요
    if (!empty($data)) {
        $data = log_json_encode($data);
    }
    if (!empty($data_old)) {
        $data_old = log_json_encode($data_old);
    }

    $insert_data = array();
    //$insert_data['al_admin_id'] = $member['mb_id'];
    !empty($url) ? $insert_data['al_url'] = $_SERVER['REQUEST_URI'] : $insert_data['al_url'] = '';
    !empty($key) ? $insert_data['al_key'] = $key : $insert_data['al_key'] = '';
    !empty($seq) ? $insert_data['al_seq'] = $seq : $insert_data['al_seq'] = '';
    $insert_data['al_depth1'] = $admin_menu[$depth1];
    $insert_data['al_depth2'] = $admin_sub_menu[$depth1][$depth2][0];
    !empty($subject) ? $insert_data['al_subject'] = $subject : $insert_data['al_subject'] = '';
    !empty($content) ? $insert_data['al_content'] = $content : $insert_data['al_content'] = '';
    !empty($data) ? $insert_data['al_data'] = $data : $insert_data['al_data'] = '';
    !empty($data_old) ? $insert_data['al_data_old'] = $data_old : $insert_data['al_data_old'] = '';
    $insert_data['al_ip'] = $_SERVER['REMOTE_ADDR'];
    $insert_data['al_date'] = G5_TIME_YMDHIS;

    $insert_add_sql = "al_admin_id='".$member['mb_id']."'";
    foreach ($insert_data as $key => $val) {
        $insert_add_sql .= ", ".$key."='".$val."'";
    }

    $insert_sql = "
        INSERT INTO $table_name
        SET 
        $insert_add_sql
    ";
    sql_query($insert_sql, FALSE);
}

function log_json_encode ($data)
{
    // addslashes 처리
    $data = addslashes_array($data);

    // tab 제거
    $data = str_tab_remove($data);

    // JSON_UNESCAPED_UNICODE - 한글 처리
    return json_encode($data, JSON_UNESCAPED_UNICODE);
}

// 문자열에서 tab이 있는 경우 제거
function str_tab_remove ($data)
{
    if (is_array($data) !== false) {

        $result = array();
        foreach($data as $key => $value) {
            if (is_array($value) === false) {
                $result[$key] = preg_replace("/\t+/", "", $value);
            } else {
                $result[$key] = $value;
            }
        }
        return $result;

    } else {

        if (preg_match("/\t+/", $data) !== false) {
            $data = preg_replace("/\t+/", "", $data);
        }

        return $data;
    }
}

// 문자열 공백 모두 제거
function str_white_remove ($data)
{
    if (is_array($data) !== false) {
        $result = array();
        foreach($data as $key => $value) {
            if (is_array($value) === false) {
                $result[$key] = preg_replace("/\s+/", "", $value);
            } else {
                $result[$key] = $value;
            }
        }
        return $result;
    } else {
        return preg_replace("/\s+/", "", $data);
    }
}

function addslashes_array ($data)
{
    if (is_array($data) !== false) {
        $result = array();
        foreach($data as $key => $value) {
            if (is_array($value) === false) {
                $result[$key] = addslashes($value);
            } else {
                $result[$key] = $value;
            }
        }
        return $result;
    } else {
        return addslashes($data);
    }
}

?>
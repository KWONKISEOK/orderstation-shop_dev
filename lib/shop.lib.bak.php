<?php
//==============================================================================
// 쇼핑몰 라이브러리 모음 시작
//==============================================================================

/*
간편 사용법 : 상품유형을 1~5 사이로 지정합니다.
$disp = new item_list(1);
echo $disp->run();


유형+분류별로 노출하는 경우 상세 사용법 : 상품유형을 지정하는 것은 동일합니다.
$disp = new item_list(1);
// 사용할 스킨을 바꿉니다.
$disp->set_list_skin("type_user.skin.php");
// 1단계분류를 20으로 시작되는 분류로 지정합니다.
$disp->set_category("20", 1);
echo $disp->run();


분류별로 노출하는 경우 상세 사용법
// type13.skin.php 스킨으로 3개씩 2줄을 폭 150 사이즈로 분류코드 30 으로 시작되는 상품을 노출합니다.
$disp = new item_list(0, "type13.skin.php", 3, 2, 150, 0, "30");
echo $disp->run();


이벤트로 노출하는 경우 상세 사용법
// type13.skin.php 스킨으로 3개씩 2줄을 폭 150 사이즈로 상품을 노출합니다.
// 스킨의 경로는 스킨 파일의 절대경로를 지정합니다.
$disp = new item_list(0, G5_SHOP_SKIN_PATH.'/list.10.skin.php', 3, 2, 150, 0);
// 이벤트번호를 설정합니다.
$disp->set_event("12345678");
echo $disp->run();

참고) 영카트4의 display_type 함수와 사용방법이 비슷한 class 입니다.
      display_category 나 display_event 로 사용하기 위해서는 $type 값만 넘기지 않으면 됩니다.
*/

class item_list
{
    // 상품유형 : 기본적으로 1~5 까지 사용할수 있으며 0 으로 설정하는 경우 상품유형별로 노출하지 않습니다.
    // 분류나 이벤트로 노출하는 경우 상품유형을 0 으로 설정하면 됩니다.
    protected $type;

    protected $list_skin;
    protected $list_mod;
    protected $list_row;
    protected $img_width;
    protected $img_height;

    // 상품상세보기 경로
    protected $href = "";

    // select 에 사용되는 필드
    protected $fields = "*";

    // 분류코드로만 사용하는 경우 상품유형($type)을 0 으로 설정하면 됩니다.
    protected $ca_id = "";
    protected $ca_id2 = "";
    protected $ca_id3 = "";

    // 노출순서
    protected $order_by = "it_order, it_id desc";

    // 상품의 이벤트번호를 저장합니다.
    protected $event = "";

	protected $mb_type = "0";

    // 스킨의 기본 css 를 다른것으로 사용하고자 할 경우에 사용합니다.
    protected $css = "";

    // 상품의 사용여부를 따져 노출합니다. 0 인 경우 모든 상품을 노출합니다.
    protected $use = 1;

    // 모바일에서 노출하고자 할 경우에 true 로 설정합니다.
    protected $is_mobile = false;

    // 기본으로 보여지는 필드들
    protected $view_it_id    = false;       // 상품코드
    protected $view_it_img   = true;        // 상품이미지
    protected $view_it_name  = true;        // 상품명
    protected $view_it_basic = true;        // 기본설명
    protected $view_it_price = true;        // 판매가격
    protected $view_it_cust_price = false;  // 소비자가
    protected $view_it_icon = false;        // 아이콘
    protected $view_sns = false;            // SNS

    // 몇번째 class 호출인지를 저장합니다.
    protected $count = 0;

    // true 인 경우 페이지를 구한다.
    protected $is_page = false;

    // 페이지 표시를 위하여 총 상품수를 구합니다.
    public $total_count = 0;

    // sql limit 의 시작 레코드
    protected $from_record = 0;

    // 외부에서 쿼리문을 넘겨줄 경우에 담아두는 변수
    protected $query = "";

    // 쿼리문을 넘겨줄때 사용
    protected $return_query = "";

    // 상품 정렬: 가격 최댓값
    protected $maxPrice = "";

    // 상품 정렬: 가격 최솟값
    protected $minPrice = "";


    // $type        : 상품유형 (기본으로 1~5까지 사용)
    // $list_skin   : 상품리스트를 노출할 스킨을 설정합니다. 스킨위치는 skin/shop/쇼핑몰설정스킨/type??.skin.php
    // $list_mod    : 1줄에 몇개의 상품을 노출할지를 설정합니다.
    // $list_row    : 상품을 몇줄에 노출할지를 설정합니다.
    // $img_width   : 상품이미지의 폭을 설정합니다.
    // $img_height  : 상품이미지의 높이을 설정합니다. 0 으로 설정하는 경우 썸네일 이미지의 높이는 폭에 비례하여 생성합니다.
    //function __construct($type=0, $list_skin='', $list_mod='', $list_row='', $img_width='', $img_height=0, $ca_id='') {
    function __construct($list_skin='', $list_mod='', $list_row='', $img_width='', $img_height=0) {
        $this->list_skin  = $list_skin;
        $this->list_mod   = $list_mod;
        $this->list_row   = $list_row;
        $this->img_width  = $img_width;
        $this->img_height = $img_height;
        $this->set_href(G5_SHOP_URL.'/item.php?it_id=');
        $this->count++;
    }

    function set_type($type) {
        $this->type = $type;
        if ($type) {
            $this->set_list_skin($this->list_skin);
            $this->set_list_mod($this->list_mod);
            $this->set_list_row($this->list_row);
            $this->set_img_size($this->img_width, $this->img_height);
        }
    }
	function set_comp($comp) {
        $this->comp = $comp;
        if ($comp) {
            $this->set_list_skin($this->list_skin);
            $this->set_list_mod($this->list_mod);
            $this->set_list_row($this->list_row);
            $this->set_img_size($this->img_width, $this->img_height);
        }
    }

    // 분류코드로 검색을 하고자 하는 경우 아래와 같이 인수를 넘겨줍니다.
    // 1단계 분류는 (분류코드, 1)
    // 2단계 분류는 (분류코드, 2)
    // 3단계 분류는 (분류코드, 3)
    function set_category($ca_id, $level=1) {
        if ($level == 2) {
            $this->ca_id2 = $ca_id;
        } else if ($level == 3) {
            $this->ca_id3 = $ca_id;
        } else {
            $this->ca_id = $ca_id;
        }
    }

    // 이벤트코드를 인수로 넘기게 되면 해당 이벤트에 속한 상품을 노출합니다.
    function set_event($ev_id) {
        $this->event = $ev_id;
    }

    // 리스트 스킨을 바꾸고자 하는 경우에 사용합니다.
    // 리스트 스킨의 위치는 skin/shop/쇼핑몰설정스킨/type??.skin.php 입니다.
    // 특별히 설정하지 않는 경우 상품유형을 사용하는 경우는 쇼핑몰설정 값을 그대로 따릅니다.
    function set_list_skin($list_skin) {
        global $default;
        if ($this->is_mobile) {
            $this->list_skin = $list_skin ? $list_skin : G5_MSHOP_SKIN_PATH.'/'.preg_replace('/[^A-Za-z0-9 _ .-]/', '', $default['de_mobile_type'.$this->type.'_list_skin']);
        } else {
            $this->list_skin = $list_skin ? $list_skin : G5_SHOP_SKIN_PATH.'/'.preg_replace('/[^A-Za-z0-9 _ .-]/', '', $default['de_type'.$this->type.'_list_skin']);
        }
    }

    // 1줄에 몇개를 노출할지를 사용한다.
    // 특별히 설정하지 않는 경우 상품유형을 사용하는 경우는 쇼핑몰설정 값을 그대로 따릅니다.
    function set_list_mod($list_mod) {
        global $default;
        if ($this->is_mobile) {
            $this->list_mod = $list_mod ? $list_mod : $default['de_mobile_type'.$this->type.'_list_mod'];
        } else {
            $this->list_mod = $list_mod ? $list_mod : $default['de_type'.$this->type.'_list_mod'];
        }
    }

    // 몇줄을 노출할지를 사용한다.
    // 특별히 설정하지 않는 경우 상품유형을 사용하는 경우는 쇼핑몰설정 값을 그대로 따릅니다.
    function set_list_row($list_row) {
        global $default;
        if ($this->is_mobile) {
            $this->list_row = $list_row ? $list_row : $default['de_mobile_type'.$this->type.'_list_row'];
        } else {
            $this->list_row = $list_row ? $list_row : $default['de_type'.$this->type.'_list_row'];
        }
        if (!$this->list_row)
            $this->list_row = 1;
    }

    // 노출이미지(썸네일생성)의 폭, 높이를 설정합니다. 높이를 0 으로 설정하는 경우 쎰네일 비율에 따릅니다.
    // 특별히 설정하지 않는 경우 상품유형을 사용하는 경우는 쇼핑몰설정 값을 그대로 따릅니다.
    function set_img_size($img_width, $img_height=0) {
        global $default;
        if ($this->is_mobile) {
            $this->img_width = $img_width ? $img_width : $default['de_mobile_type'.$this->type.'_img_width'];
            $this->img_height = $img_height ? $img_height : $default['de_mobile_type'.$this->type.'_img_height'];
        } else {
            $this->img_width = $img_width ? $img_width : $default['de_type'.$this->type.'_img_width'];
            $this->img_height = $img_height ? $img_height : $default['de_type'.$this->type.'_img_height'];
        }
    }

    // 특정 필드만 select 하는 경우에는 필드명을 , 로 구분하여 "field1, field2, field3, ... fieldn" 으로 인수를 넘겨줍니다.
    function set_fields($str) {
        $this->fields = $str;
    }

    // 특정 필드로 정렬을 하는 경우 필드와 정렬순서를 , 로 구분하여 "field1 desc, field2 asc, ... fieldn desc " 으로 인수를 넘겨줍니다.
    function set_order_by($str) {
        $this->order_by = $str;
    }
	
	public function set_mb_type($str) {
        $this->mb_type = $str;
    }


    // 사용하는 상품외에 모든 상품을 노출하려면 0 을 인수로 넘겨줍니다.
    function set_use($use) {
        $this->use = $use;
    }

    // 모바일로 사용하려는 경우 true 를 인수로 넘겨줍니다.
    function set_mobile($mobile=true) {
        $this->is_mobile = $mobile;
    }

    // 스킨에서 특정 필드를 노출하거나 하지 않게 할수 있습니다.
    // 가령 소비자가는 처음에 노출되지 않도록 설정되어 있지만 노출을 하려면
    // ("it_cust_price", true) 와 같이 인수를 넘겨줍니다.
    // 이때 인수로 넘겨주는 값은 스킨에 정의된 필드만 가능하다는 것입니다.
    function set_view($field, $view=true) {
        $this->{"view_".$field} = $view;
    }

    // anchor 태그에 하이퍼링크를 다른 주소로 걸거나 아예 링크를 걸지 않을 수 있습니다.
    // 인수를 "" 공백으로 넘기면 링크를 걸지 않습니다.
    function set_href($href) {
        $this->href = $href;
    }

    // ul 태그의 css 를 교체할수 있다. "sct sct_abc" 를 인수로 넘기게 되면
    // 기존의 ul 태그에 걸린 css 는 무시되며 인수로 넘긴 css 가 사용됩니다.
    function set_css($css) {
        $this->css = $css;
    }

    // 페이지를 노출하기 위해 true 로 설정할때 사용합니다.
    function set_is_page($is_page) {
        $this->is_page = $is_page;
    }

    // select ... limit 의 시작값
    function set_from_record($from_record) {
        $this->from_record = $from_record;
    }

    // 외부에서 쿼리문을 넘겨줄 경우에 담아둡니다.
    function set_query($query) {
        $this->query = $query;
    }

    // 상품 정렬: 가격 기준으로 했을때
    function set_price_list($maxPrice, $minPrice) {
        $this->maxPrice = $maxPrice;
        $this->minPrice = $minPrice;
    }

	// 실행될 쿼리를 리턴시키기 위해 사용
	function return_query(){
        return $this->return_query;
	}

    // class 에 설정된 값으로 최종 실행합니다.
    function run() {

        global $g5, $config, $member, $default;

        if ($this->query) {

            $sql = $this->query;
            $result = sql_query($sql);
            $this->total_count = @sql_num_rows($result);

        } else {

            $where = array();
            if ($this->use) {
                $where[] = " it_use = '1' and it_status = '3' ";
            }

            if ($this->type) {
                $where[] = " it_type{$this->type} = '1' ";
            }
			if ($this->comp) {
                $where[] = " comp_code = '{$this->comp}' ";
            }

            // 회원별 상품 노출 조건
			if ($this->mb_type == '1' || $this->mb_type == '9') {
                if ($member['mb_referee'] == '5') {
                    // 얼라이언스회원
                    $where[] = " (it_web_view = '1' or it_new_view = '1') ";
                } else {
                    // 약국, 관리자
                    $where[] = " it_web_view = '1' ";
                }
            } else {
                // 일반 회원
				$where[] = " it_app_view = '1' ";
			}
			
			if ($this->is_mobile != '1') {
				$where[] = " it_only_mobile_display = 0 ";
			}

            if ($this->ca_id || $this->ca_id2 || $this->ca_id3) {
                $where_ca_id = array();
                if ($this->ca_id) {
                    $where_ca_id[] = " ca_id like '{$this->ca_id}%' ";
                }
                if ($this->ca_id2) {
                    $where_ca_id[] = " ca_id2 like '{$this->ca_id2}%' ";
                }
                if ($this->ca_id3) {
                    $where_ca_id[] = " ca_id3 like '{$this->ca_id3}%' ";
                }
                $where[] = " ( " . implode(" or ", $where_ca_id) . " ) ";
            }

            if ($this->order_by) {
                $sql_order = " order by {$this->order_by} ";
            }

            if ($this->event) {
                $sql_select = " select {$this->fields} ";
                $sql_common = " from `{$g5['g5_shop_event_item_table']}` a left join `{$g5['g5_shop_item_table']}` b on (a.it_id = b.it_id) ";
                $where[] = " a.ev_id = '{$this->event}' ";
            } else {
                $sql_select = " select {$this->fields} ";
                $sql_common = " from `{$g5['g5_shop_item_table']}` ";
            }

            if ($this->minPrice){
                $where[] = "b.it_price >'{$this->minPrice}' and b.it_price <= '{$this->maxPrice}'";
            }

            $sql_where = " where " . implode(" and ", $where);
            $sql_limit = " limit " . $this->from_record . " , " . ($this->list_mod * $this->list_row);

            $sql = $sql_select . $sql_common . $sql_where . $sql_order . $sql_limit;
			//echo $sql;
			//echo 'is_mobile=' . $this->is_mobile;

			// 실행된 쿼리를 리턴
			$this->return_query = $sql;

            $result = sql_query($sql);

            if ($this->is_page) {
                $sql2 = " select count(*) as cnt " . $sql_common . $sql_where;
                $row2 = sql_fetch($sql2);
                $this->total_count = $row2['cnt'];
            }

        }

        $file = $this->list_skin;

        if ($this->list_skin == "") {
            return $this->count."번 item_list() 의 스킨파일이 지정되지 않았습니다.";
        } else if (!file_exists($file)) {
            return $file." 파일을 찾을 수 없습니다.";
        } else {
            ob_start();
            $list_mod = $this->list_mod;
            include($file);
            $content = ob_get_contents();
            ob_end_clean();
            return $content;
        }
    }
}


// 장바구니 건수 검사
function get_cart_count($cart_id)
{
    global $g5, $default;

    $sql = " select DISTINCT(count(ct_id)) as cnt from {$g5['g5_shop_cart_table']} where od_id = '$cart_id' AND service_type IS NULL";
    $row = sql_fetch($sql);
    $cnt = (int)$row['cnt'];
    return $cnt;
}


// 이미지를 얻는다
function get_image($img, $width=0, $height=0, $img_id='')
{
    global $g5, $default;

    $full_img = G5_DATA_PATH.'/item/'.$img;

    if (file_exists($full_img) && $img)
    {
        if (!$width)
        {
            $size = getimagesize($full_img);
            $width = $size[0];
            $height = $size[1];
        }
        $str = '<img src="'.G5_DATA_URL.'/item/'.$img.'" alt="" width="'.$width.'" height="'.$height.'"';

        if($img_id)
            $str .= ' id="'.$img_id.'"';

        $str .= '>';
    }
    else
    {
        $str = '<img src="'.G5_SHOP_URL.'/img/no_image.gif" alt="" ';
        if ($width)
            $str .= 'width="'.$width.'" height="'.$height.'"';
        else
            $str .= 'width="'.$default['de_mimg_width'].'" height="'.$default['de_mimg_height'].'"';

        if($img_id)
            $str .= ' id="'.$img_id.'"'.
        $str .= '>';
    }

    return $str;
}


// 상품 이미지를 얻는다
function get_it_image($it_id, $width, $height=0, $anchor=false, $img_id='', $img_alt='', $is_crop=false)
{
    global $g5;

    if(!$it_id || !$width)
        return '';

    $sql = " select it_id, it_img1, it_img2, it_img3, it_img4, it_img5, it_img6, it_img7, it_img8, it_img9, it_img10 from {$g5['g5_shop_item_table']} where it_id = '$it_id' ";
    $row = sql_fetch($sql);

    if(!$row['it_id'])
        return '';

    for($i=1;$i<=10; $i++) {
        $file = G5_DATA_PATH.'/item/'.$row['it_img'.$i];
        if(is_file($file) && $row['it_img'.$i]) {
            $size = @getimagesize($file);
            if($size[2] < 1 || $size[2] > 3)
                continue;

            $filename = basename($file);
            $filepath = dirname($file);
            $img_width = $size[0];
            $img_height = $size[1];

            break;
        }
    }

    if($img_width && !$height) {
        $height = round(($width * $img_height) / $img_width);
    }

    if($filename) {
        //thumbnail($filename, $source_path, $target_path, $thumb_width, $thumb_height, $is_create, $is_crop=false, $crop_mode='center', $is_sharpen=true, $um_value='80/0.5/3')
        $thumb = thumbnail($filename, $filepath, $filepath, $width, $height, false, $is_crop, 'center', false, $um_value='80/0.5/3');
    }

    if($thumb) {
        $file_url = str_replace(G5_PATH, G5_URL, $filepath.'/'.$thumb);
        $img = '<img src="'.$file_url.'" width="'.$width.'" height="'.$height.'" alt="'.$img_alt.'"';
    } else {
        $img = '<img src="'.G5_SHOP_URL.'/img/no_image.gif" width="'.$width.'"';
        if($height)
            $img .= ' height="'.$height.'"';
        $img .= ' alt="'.$img_alt.'"';
    }

    if($img_id)
        $img .= ' id="'.$img_id.'"';
    $img .= '>';

    if($anchor)
        $img = '<a href="'.G5_SHOP_URL.'/item.php?it_id='.$it_id.'">'.$img.'</a>';

    return $img;
}


// 상품이미지 썸네일 생성
function get_it_thumbnail($img, $width, $height=0, $id='', $is_crop=false)
{
    $str = '';

    $file = G5_DATA_PATH.'/item/'.$img;
    if(is_file($file))
        $size = @getimagesize($file);

    if($size[2] < 1 || $size[2] > 3)
        return '';

    $img_width = $size[0];
    $img_height = $size[1];
    $filename = basename($file);
    $filepath = dirname($file);

    if($img_width && !$height) {
        $height = round(($img_width * $img_height) / $img_width);
    }

    $thumb = thumbnail($filename, $filepath, $filepath, $img_width, $img_height, false, $is_crop, 'center', false, $um_value='80/0.5/3');

    if($thumb) {
        $file_url = str_replace(G5_PATH, G5_URL, $filepath.'/'.$thumb);
        $str = '<img src="'.$file_url.'"';
        if($id)
            $str .= ' id="'.$id.'"';
        $str .= ' alt="">';
    }

    return $str;
}


// 옵션이미지 썸네일 생성
function get_it_thumbnail2($img, $width, $height=0, $id='', $is_crop=false)
{
    $str = '';

    $file = G5_DATA_PATH.'/option/'.$img;
    if(is_file($file))
        $size = @getimagesize($file);

    if($size[2] < 1 || $size[2] > 3)
        return '';

    $img_width = $size[0];
    $img_height = $size[1];
    $filename = basename($file);
    $filepath = dirname($file);

    if($img_width && !$height) {
        $height = round(($width * $img_height) / $img_width);
    }

    $thumb = thumbnail($filename, $filepath, $filepath, $width, $height, false, $is_crop, 'center', false, $um_value='80/0.5/3');

    if($thumb) {
        $file_url = str_replace(G5_PATH, G5_URL, $filepath.'/'.$thumb);
        $str = '<img src="'.$file_url.'" width="'.$width.'" height="'.$height.'"';
        if($id)
            $str .= ' id="'.$id.'"';
        $str .= ' alt="">';
    }

    return $str;
}

// 이미지 URL 을 얻는다.
function get_it_imageurl($it_id)
{
    global $g5;

    $sql = " select it_img1, it_img2, it_img3, it_img4, it_img5, it_img6, it_img7, it_img8, it_img9, it_img10
                from {$g5['g5_shop_item_table']}
                where it_id = '$it_id' ";
    $row = sql_fetch($sql);
    $filepath = '';

    for($i=1; $i<=10; $i++) {
        $img = $row['it_img'.$i];
        $file = G5_DATA_PATH.'/item/'.$img;
        if(!is_file($file))
            continue;

        $size = @getimagesize($file);
        if($size[2] < 1 || $size[2] > 3)
            continue;

        $filepath = $file;
        break;
    }

    if($filepath)
        $str = str_replace(G5_PATH, G5_URL, $filepath);
    else
        $str = G5_SHOP_URL.'/img/no_image.gif';

    return $str;
}


// 상품의 재고 (창고재고수량 - 주문대기수량)
function get_it_stock_qty($it_id)
{
    global $g5;

    $sql = " select it_stock_qty from {$g5['g5_shop_item_table']} where it_id = '$it_id' ";
    $row = sql_fetch($sql);
    $jaego = (int)$row['it_stock_qty'];

    // 재고에서 빼지 않았고 주문인것만
//    $sql = " select SUM(ct_qty) as sum_qty
//               from {$g5['g5_shop_cart_table']}
//              where it_id = '$it_id'
//                and io_id = ''
//                and ct_stock_use = 0
//                and ct_status in ('주문', '입금', '준비') ";
//    $row = sql_fetch($sql);
//    $daegi = (int)$row['sum_qty'];

    return $jaego;
}


// 옵션의 정기주문 할인
function get_option_dis_price($it_id, $io_id, $type)
{
    global $g5;
	
	
	//옵션가
    $sql = " select io_price, io_period_rate1, io_period_cnt1
                from {$g5['g5_shop_item_option_table']}
                where it_id = '$it_id' and io_id = '$io_id' and io_type = '$type' and io_use = '1' ";
    $row = sql_fetch($sql);
    $io_price = (int)$row['io_price'];
	$io_period_rate = (int)$row['io_period_rate1'];
	$io_period_cnt1 = (int)$row['io_period_cnt1'];
	$totalprice = 0;
	if($io_period_rate > 0) {
		//상품의 기본가
		$sql = " select it_price
					from {$g5['g5_shop_item_table']}
					where it_id = '$it_id'  ";
		$row = sql_fetch($sql);
		$it_price = (int)$row['it_price'];

		$totalprice = ((($it_price + $io_price ) * ($io_period_rate*0.01)) * $io_period_cnt1)-$it_price;
	}
   
    return $totalprice;
}

// 옵션의 정기주문 할인
function get_option_dis_price_new($it_id, $io_id, $type)
{
    global $g5,$member;
	
	
	//옵션가
    $sql = " select io_price, io_drug_price, io_period_rate1, io_period_cnt1
                from {$g5['g5_shop_item_option_table']}
                where it_id = '$it_id' and io_id = '$io_id' and io_type = '$type' and io_use = '1' ";
    $row = sql_fetch($sql);
	//소비자인지 약국인지에 따른 가격분리요청에 의해 수정
	//mb_type = 1 -> 약국일때 가격
	if( $member["mb_type"] == "1" ){
		$io_price = (int)$row['io_drug_price'];
	}else{
		$io_price = (int)$row['io_price'];
	}

	$io_period_rate = (int)$row['io_period_rate1'];
	$io_period_cnt1 = (int)$row['io_period_cnt1'];
	$totalprice = 0;
	if($io_period_rate > 0) {
		//상품의 기본가
		$sql = " select it_price, it_drug_price
					from {$g5['g5_shop_item_table']}
					where it_id = '$it_id'  ";
		$row = sql_fetch($sql);
		//소비자인지 약국인지에 따른 가격분리요청에 의해 수정
		//mb_type = 1 -> 약국일때 가격
		if( $member["mb_type"] == "1" ){
			$it_price = (int)$row['it_drug_price'];
		}else{
			$it_price = (int)$row['it_price'];
		}

		$totalprice = ((($it_price + $io_price ) * ($io_period_rate*0.01)) * $io_period_cnt1)-$it_price;
	}
   
    return $totalprice;
}

// 옵션의 재고 (창고재고수량 - 주문대기수량)
function get_option_stock_qty($it_id, $io_id, $type)
{
    global $g5;

    $sql = " select io_stock_qty
                from {$g5['g5_shop_item_option_table']}
                where it_id = '$it_id' and io_id = '$io_id' and io_type = '$type' and io_use = '1' ";

    debug2("옵션 재고 : ".$sql);

    $row = sql_fetch($sql);
    $jaego = (int)$row['io_stock_qty'];

    // 재고에서 빼지 않았고 주문인것만
//    $sql = " select SUM(ct_qty) as sum_qty
//               from {$g5['g5_shop_cart_table']}
//              where it_id = '$it_id'
//                and io_id = '$io_id'
//                and io_type = '$type'
//                and ct_stock_use = 0
//                and ct_status in ('주문', '입금', '준비') ";
//    $row = sql_fetch($sql);
//    $daegi = (int)$row['sum_qty'];

    return $jaego;
}


// 큰 이미지
function get_large_image($img, $it_id, $btn_image=true)
{
    global $g5;

    if (file_exists(G5_DATA_PATH.'/item/'.$img) && $img != '')
    {
        $size   = getimagesize(G5_DATA_PATH.'/item/'.$img);
        $width  = $size[0];
        $height = $size[1];
        $str = '<a href="javascript:popup_large_image(\''.$it_id.'\', \''.$img.'\', '.$width.', '.$height.', \''.G5_SHOP_URL.'\')">';
        if ($btn_image)
            $str .= '큰이미지</a>';
    }
    else
        $str = '';
    return $str;
}


// 금액 표시
function display_price($price, $tel_inq=false)
{
    if ($tel_inq)
        $price = '전화문의';
    else
        $price = number_format($price, 0);

    return $price;
}


// 금액표시
// $it : 상품 배열
function get_price($it)
{
    global $member;

    if ($it['it_tel_inq']) return '전화문의';

    $price = $it['it_price'];

    return (int)$price;
}


// 포인트 표시
function display_point($point)
{
    return number_format($point, 0).'점';
}


// 포인트를 구한다
function get_point($amount, $point)
{
    return (int)($amount * $point / 100);
}


// 상품이미지 업로드
function it_img_upload($srcfile, $filename, $dir)
{
    if($filename == '')
        return '';

    $size = @getimagesize($srcfile);
    if($size[2] < 1 || $size[2] > 3)
        return '';

    //php파일도 getimagesize 에서 Image Type Flag 를 속일수 있다
    if (!preg_match('/\.(gif|jpe?g|png)$/i', $filename))
        return '';

    if(!is_dir($dir)) {
        @mkdir($dir, G5_DIR_PERMISSION);
        @chmod($dir, G5_DIR_PERMISSION);
    }

    $pattern = "/[#\&\+\-%@=\/\\:;,'\"\^`~\|\!\?\*\$#<>\(\)\[\]\{\}]/";

    $filename = preg_replace("/\s+/", "", $filename);
    $filename = preg_replace( $pattern, "", $filename);

    $filename = preg_replace_callback(
                          "/[가-힣]+/",
                          create_function('$matches', 'return base64_encode($matches[0]);'),
                          $filename);

    $filename = preg_replace( $pattern, "", $filename);
    $prepend = '';

    // 동일한 이름의 파일이 있으면 파일명 변경
    if(is_file($dir.'/'.$filename)) {
        for($i=0; $i<20; $i++) {
            $prepend = str_replace('.', '_', microtime(true)).'_';

            if(is_file($dir.'/'.$prepend.$filename)) {
                usleep(mt_rand(100, 10000));
                continue;
            } else {
                break;
            }
        }
    }

    $filename = $prepend.$filename;

    upload_file($srcfile, $filename, $dir);

    $file = str_replace(G5_DATA_PATH.'/item/', '', $dir.'/'.$filename);

    return $file;
}



// 상품이미지 업로드
function it_img_upload2($srcfile, $filename, $dir)
{
    if($filename == '')
        return '';

    $size = @getimagesize($srcfile);
    if($size[2] < 1 || $size[2] > 3)
        return '';

    //php파일도 getimagesize 에서 Image Type Flag 를 속일수 있다
    if (!preg_match('/\.(gif|jpe?g|png)$/i', $filename))
        return '';

    if(!is_dir($dir)) {
        @mkdir($dir, G5_DIR_PERMISSION);
        @chmod($dir, G5_DIR_PERMISSION);
    }

    $pattern = "/[#\&\+\-%@=\/\\:;,'\"\^`~\|\!\?\*\$#<>\(\)\[\]\{\}]/";

    $filename = preg_replace("/\s+/", "", $filename);
    $filename = preg_replace( $pattern, "", $filename);

    $filename = preg_replace_callback(
                          "/[가-힣]+/",
                          create_function('$matches', 'return base64_encode($matches[0]);'),
                          $filename);

    $filename = preg_replace( $pattern, "", $filename);
    $prepend = '';

    // 동일한 이름의 파일이 있으면 파일명 변경
    if(is_file($dir.'/'.$filename)) {
        for($i=0; $i<20; $i++) {
            $prepend = str_replace('.', '_', microtime(true)).'_';

            if(is_file($dir.'/'.$prepend.$filename)) {
                usleep(mt_rand(100, 10000));
                continue;
            } else {
                break;
            }
        }
    }

    $filename = $prepend.$filename;

    upload_file($srcfile, $filename, $dir);

    $file = str_replace(G5_DATA_PATH.'/item_mod/', '', $dir.'/'.$filename);

    return $file;
}

// 옵션이미지 업로드
function it_img_upload3($srcfile, $filename, $dir)
{
    if($filename == '')
        return '';

	if( strpos($dir, chr(30)) !== false ){
		$split_dir = explode(chr(30), $dir);
		$dir = $split_dir[0];
	}

    $size = @getimagesize($srcfile);
    if($size[2] < 1 || $size[2] > 3)
        return '';

    //php파일도 getimagesize 에서 Image Type Flag 를 속일수 있다
    if (!preg_match('/\.(gif|jpe?g|png)$/i', $filename))
        return '';

    if(!is_dir($dir)) {
        @mkdir($dir, G5_DIR_PERMISSION);
        @chmod($dir, G5_DIR_PERMISSION);
    }

    $pattern = "/[#\&\+\-%@=\/\\:;,'\"\^`~\|\!\?\*\$#<>\(\)\[\]\{\}]/";

    $filename = preg_replace("/\s+/", "", $filename);
    $filename = preg_replace( $pattern, "", $filename);

    $filename = preg_replace_callback(
                          "/[가-힣]+/",
                          create_function('$matches', 'return base64_encode($matches[0]);'),
                          $filename);

    $filename = preg_replace( $pattern, "", $filename);
    $prepend = '';

    // 동일한 이름의 파일이 있으면 파일명 변경
    if(is_file($dir.'/'.$filename)) {
        for($i=0; $i<20; $i++) {
            $prepend = str_replace('.', '_', microtime(true)).'_';

            if(is_file($dir.'/'.$prepend.$filename)) {
                usleep(mt_rand(100, 10000));
                continue;
            } else {
                break;
            }
        }
    }

    $filename = $prepend.$filename;

    upload_file($srcfile, $filename, $dir);

    $file = str_replace(G5_DATA_PATH.'/option/', '', $dir.'/'.$filename);

    return $file;
}

// 파일을 업로드 함
function upload_file($srcfile, $destfile, $dir)
{
    if ($destfile == "") return false;
    // 업로드 한후 , 퍼미션을 변경함
    @move_uploaded_file($srcfile, $dir.'/'.$destfile);
    @chmod($dir.'/'.$destfile, G5_FILE_PERMISSION);
    return true;
}


function message($subject, $content, $align="left", $width="450")
{
    $str = "
        <table width=\"$width\" cellpadding=\"4\" align=\"center\">
            <tr><td class=\"line\" height=\"1\"></td></tr>
            <tr>
                <td align=\"center\">$subject</td>
            </tr>
            <tr><td class=\"line\" height=\"1\"></td></tr>
            <tr>
                <td>
                    <table width=\"100%\" cellpadding=\"8\" cellspacing=\"0\">
                        <tr>
                            <td class=\"leading\" align=\"$align\">$content</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr><td class=\"line\" height=\"1\"></td></tr>
        </table>
        <br>
        ";
    return $str;
}


// 시간이 비어 있는지 검사
function is_null_time($datetime)
{
    // 공란 0 : - 제거
    //$datetime = ereg_replace("[ 0:-]", "", $datetime); // 이 함수는 PHP 5.3.0 에서 배제되고 PHP 6.0 부터 사라집니다.
    $datetime = preg_replace("/[ 0:-]/", "", $datetime);
    if ($datetime == "")
        return true;
    else
        return false;
}


// 출력유형, 스킨파일, 1라인이미지수, 총라인수, 이미지폭, 이미지높이
// 1.02.01 $ca_id 추가
//function display_type($type, $skin_file, $list_mod, $list_row, $img_width, $img_height, $ca_id="")
function display_type($type, $list_skin='', $list_mod='', $list_row='', $img_width='', $img_height='', $ca_id='')
{
    global $member, $g5, $config, $default;

    if (!$default["de_type{$type}_list_use"]) return "";

    $list_skin  = $list_skin  ? $list_skin  : $default["de_type{$type}_list_skin"];
    $list_mod   = $list_mod   ? $list_mod   : $default["de_type{$type}_list_mod"];
    $list_row   = $list_row   ? $list_row   : $default["de_type{$type}_list_row"];
    $img_width  = $img_width  ? $img_width  : $default["de_type{$type}_img_width"];
    $img_height = $img_height ? $img_height : $default["de_type{$type}_img_height"];

    // 상품수
    $items = $list_mod * $list_row;

    // 1.02.00
    // it_order 추가
    $sql = " select * from {$g5['g5_shop_item_table']} where it_use = '1' and it_type{$type} = '1' ";
    if ($ca_id) $sql .= " and ca_id like '$ca_id%' ";
    $sql .= " order by it_order, it_id desc limit $items ";
    $result = sql_query($sql);
    /*
    if (!sql_num_rows($result)) {
        return false;
    }
    */

    //$file = G5_SHOP_PATH.'/'.$skin_file;
    $file = G5_SHOP_SKIN_PATH.'/'.$list_skin;
    if (!file_exists($file)) {
        return G5_SHOP_SKIN_URL.'/'.$list_skin.' 파일을 찾을 수 없습니다.';
    } else {
        $td_width = (int)(100 / $list_mod);
        ob_start();
        include $file;
        $content = ob_get_contents();
        ob_end_clean();
        return $content;
    }
}


// 모바일 유형별 상품 출력
function mobile_display_type($type, $skin_file, $list_row, $img_width, $img_height, $ca_id="")
{
    global $member, $g5, $config;

    // 상품수
    $items = $list_row;

    // 1.02.00
    // it_order 추가
    $sql = " select * from {$g5['g5_shop_item_table']} where it_use = '1' and it_type{$type} = '1' ";
    if ($ca_id) $sql .= " and ca_id like '$ca_id%' ";
    $sql .= " order by it_order, it_id desc limit $items ";
    $result = sql_query($sql);
    /*
    if (!sql_num_rows($result)) {
        return false;
    }
    */

    $file = G5_MSHOP_PATH.'/'.$skin_file;
    if (!file_exists($file)) {
        echo $file.' 파일을 찾을 수 없습니다.';
    } else {
        //$td_width = (int)(100 / $list_mod);
        include $file;
    }
}


// 분류별 출력
// 스킨파일번호, 1라인이미지수, 총라인수, 이미지폭, 이미지높이 , 분류번호
function display_category($no, $list_mod, $list_row, $img_width, $img_height, $ca_id="")
{
    global $member, $g5;

    // 상품수
    $items = $list_mod * $list_row;

    $sql = " select * from {$g5['g5_shop_item_table']} where it_use = '1'";
    if ($ca_id)
        $sql .= " and ca_id LIKE '{$ca_id}%' ";
    $sql .= " order by it_order, it_id desc limit $items ";
    $result = sql_query($sql);
    if (!sql_num_rows($result)) {
        return false;
    }

    $file = G5_SHOP_PATH.'/maintype'.$no.'.inc.php';
    if (!file_exists($file)) {
        echo $file.' 파일을 찾을 수 없습니다.';
    } else {
        $td_width = (int)(100 / $list_mod);
        include $file;
    }
}


// 별
function get_star($score)
{
    $star = round($score, 1);
    if ($star > 5) $star = 5;
    else if ($star < 0) $star = 0;

    return $star;
}


// 별 이미지
function get_star_image($it_id)
{
    global $g5;

    $sql = "select (SUM(is_score) / COUNT(*)) as score from {$g5['g5_shop_item_use_table']} where it_id = '$it_id' /*and is_confirm = 1*/ ";
    $row = sql_fetch($sql);

    return get_star($row['score']);
}


// 메일 보내는 내용을 HTML 형식으로 만든다.
function email_content($str)
{
    global $g5;

    $s = "";
    $s .= "<html><head><meta http-equiv=\"content-type\" content=\"text/html; charset={$g5['charset']}\"><title>메일</title>\n";
    $s .= "<body>\n";
    $s .= $str;
    $s .= "</body>\n";
    $s .= "</html>";

    return $s;
}


// 타임스탬프 형식으로 넘어와야 한다.
// 시작시간, 종료시간
function gap_time($begin_time, $end_time)
{
    $gap = $end_time - $begin_time;
    $time['days']    = (int)($gap / 86400);
    $time['hours']   = (int)(($gap - ($time['days'] * 86400)) / 3600);
    $time['minutes'] = (int)(($gap - ($time['days'] * 86400 + $time['hours'] * 3600)) / 60);
    $time['seconds'] = (int)($gap - ($time['days'] * 86400 + $time['hours'] * 3600 + $time['minutes'] * 60));
    return $time;
}


// 공란없이 이어지는 문자 자르기 (wayboard 참고 (way.co.kr))
function continue_cut_str($str, $len=80)
{
    /*
    $pattern = "[^ \n<>]{".$len."}";
    return eregi_replace($pattern, "\\0\n", $str);
    */
    $pattern = "/[^ \n<>]{".$len."}/";
    return preg_replace($pattern, "\\0\n", $str);
}


// 제목별로 컬럼 정렬하는 QUERY STRING
// $type 이 1이면 반대
function title_sort($col, $type=0)
{
    global $sort1, $sort2;
    global $_SERVER;
    global $page;
    global $doc;

    $q1 = 'sort1='.$col;
    if ($type) {
        $q2 = 'sort2=desc';
        if ($sort1 == $col) {
            if ($sort2 == 'desc') {
                $q2 = 'sort2=asc';
            }
        }
    } else {
        $q2 = 'sort2=asc';
        if ($sort1 == $col) {
            if ($sort2 == 'asc') {
                $q2 = 'sort2=desc';
            }
        }
    }
    #return "$_SERVER[SCRIPT_NAME]?$q1&amp;$q2&amp;page=$page";
    return "{$_SERVER['SCRIPT_NAME']}?$q1&amp;$q2&amp;page=$page";
}


// 세션값을 체크하여 이쪽에서 온것이 아니면 메인으로
function session_check()
{
    global $g5;

    if (!trim(get_session('ss_uniqid')))
        gotourl(G5_SHOP_URL);
}


// 상품 선택옵션
function get_item_options($mb_type, $it_id, $subject, $is_div='')
{
    global $g5;

    if(!$it_id || !$subject)
        return '';

    $sql = " select * from {$g5['g5_shop_item_option_table']} where io_type = '0' and it_id = '$it_id' and io_use = '1' order by io_no asc ";
    $result = sql_query($sql);
    if(!sql_num_rows($result))
        return '';

    $str = '';
    $subj = explode(',', $subject);
    $subj_count = count($subj);

    if($subj_count > 1) {
        $options = array();

        // 옵션항목 배열에 저장
        for($i=0; $row=sql_fetch_array($result); $i++) {
            $opt_id = explode(chr(30), $row['io_id']);

            for($k=0; $k<$subj_count; $k++) {
                if(!is_array($options[$k]))
                    $options[$k] = array();

                if($opt_id[$k] && !in_array($opt_id[$k], $options[$k]))
                    $options[$k][] = $opt_id[$k];
            }
        }

        // 옵션선택목록 만들기
        for($i=0; $i<$subj_count; $i++) {
            $opt = $options[$i];
            $opt_count = count($opt);
            $disabled = '';
            if($opt_count) {
                $seq = $i + 1;
                if($i > 0)
                    $disabled = ' disabled="disabled"';

                if($is_div === 'div') {
                    $str .= '<div class="get_item_options">'.PHP_EOL;
                    //$str .= '<label for="it_option_'.$seq.'">'.$subj[$i].'</label>'.PHP_EOL;
                } else {
                    $str .= '<tr>'.PHP_EOL;
                    //$str .= '<th><label for="it_option_'.$seq.'">'.$subj[$i].'</label></th>'.PHP_EOL;
                }

				if( empty($subj[$i]) || $subj[$i] == "" ){
					$subj[$i] = "선택";
				}

                $select = '<select id="it_option_'.$seq.'" class="it_option"'.$disabled.'>'.PHP_EOL;
                $select .= '<option value="">'.$subj[$i].'</option>'.PHP_EOL;
                for($k=0; $k<$opt_count; $k++) {
                    $opt_val = $opt[$k];
                    if(strlen($opt_val)) {

						$sql = " 
							select io_name from {$g5['g5_shop_item_option_table']} where io_type = '0' and it_id = '$it_id' and io_use = '1' 
							and SUBSTRING_INDEX(io_id,CHAR(30),1) = '".$opt_val."'
						";
						//echo $sql."<br>";
						$rs_tmp = sql_fetch($sql);

						if($rs_tmp["io_name"]){
							//1차옵션
							$select .= '<option value="'.$opt_val.'">'.$rs_tmp["io_name"].'</option>'.PHP_EOL;
						}
                    }
                }
                $select .= '</select>'.PHP_EOL;

                if($is_div === 'div') {
                    $str .= '<span>'.$select.'</span>'.PHP_EOL;
                    $str .= '</div>'.PHP_EOL;
                } else {
                    $str .= '<td>'.$select.'</td>'.PHP_EOL;
                    $str .= '</tr>'.PHP_EOL;
                }
            }
        }
    } else {
        if($is_div === 'div') {
            $str .= '<div class="get_item_options">'.PHP_EOL;
            //$str .= '<label for="it_option_1">'.$subj[0].'</label>'.PHP_EOL;
        } else {
            $str .= '<tr>'.PHP_EOL;
            //$str .= '<th><label for="it_option_1">'.$subj[0].'</label></th>'.PHP_EOL;
        }
        $select = '<select id="it_option_1" class="it_option">'.PHP_EOL;
        $select .= '<option value="">'.$subj[0].'</option>'.PHP_EOL;
        for($i=0; $row=sql_fetch_array($result); $i++) {

			$io_price = $row['io_price'];
            if($io_price >= 0)
				if($row['io_period_rate1'] > 0) {
					//상품의 기본가
					$sql = " select it_price
								from {$g5['g5_shop_item_table']}
								where it_id = '$it_id'  ";
					$row2 = sql_fetch($sql);
					$it_price = (int)$row2['it_price'];

					$io_period_rate = (int)$row['io_period_rate1'];
					$io_period_cnt1 = (int)$row['io_period_cnt1'];


					$price = ((($it_price + $row['io_price'] ) * ($io_period_rate*0.01)) * $io_period_cnt1) - $it_price;
					$io_price = $price;
					$price = '&nbsp;&nbsp;+ '.number_format($price).'원';


				} else {

					$price = '&nbsp;&nbsp;+ '.number_format($io_price).'원';
				}
            else
                $price = '&nbsp;&nbsp; '.number_format($io_price).'원';

            if($row['io_stock_qty'] < 1)
                $soldout = '&nbsp;&nbsp;[품절]';
            else
                $soldout = '';

			if($mb_type=='1') {
				$price = $price.', '.number_format($row['io_incen']).'P';
			}

			// 2차선택
            $select .= '<option value="'.$row['io_id'].','.$io_price.','.$row['io_stock_qty'].','.$row['io_name'].'">'.$row['io_name'].$price.$soldout.'</option>'.PHP_EOL;

        }
        $select .= '</select>'.PHP_EOL;

        if($is_div === 'div') {
            $str .= '<span>'.$select.'</span>'.PHP_EOL;
            $str .= '</div>'.PHP_EOL;
        } else {
            $str .= '<td>'.$select.'</td>'.PHP_EOL;
            $str .= '</tr>'.PHP_EOL;
        }

    }
    return $str;
}


// 상품 추가옵션
function get_item_supply($it_id, $subject, $is_div='')
{
    global $g5;

    if(!$it_id || !$subject)
        return '';

    $sql = " select * from {$g5['g5_shop_item_option_table']} where io_type = '1' and it_id = '$it_id' and io_use = '1' order by io_no asc ";
    $result = sql_query($sql);
    if(!sql_num_rows($result))
        return '';

    $str = '';

    $subj = explode(',', $subject);
    $subj_count = count($subj);
    $options = array();

    // 옵션항목 배열에 저장
    for($i=0; $row=sql_fetch_array($result); $i++) {
        $opt_id = explode(chr(30), $row['io_id']);

        if($opt_id[0] && !array_key_exists($opt_id[0], $options))
            $options[$opt_id[0]] = array();

        if(strlen($opt_id[1])) {
            if($row['io_price'] >= 0)
                $price = '&nbsp;&nbsp;+ '.number_format($row['io_price']).'원';
            else
                $price = '&nbsp;&nbsp; '.number_format($row['io_price']).'원';
            $io_stock_qty = get_option_stock_qty($it_id, $row['io_id'], $row['io_type']);

            if($io_stock_qty < 1)
                $soldout = '&nbsp;&nbsp;[품절]';
            else
                $soldout = '';

            $options[$opt_id[0]][] = '<option value="'.$opt_id[1].','.$row['io_price'].','.$io_stock_qty.','.$opt_id[1].'">'.$opt_id[1].$price.$soldout.'</option>';
        }
    }

    // 옵션항목 만들기
    for($i=0; $i<$subj_count; $i++) {
        $opt = $options[$subj[$i]];
        $opt_count = count($opt);
        if($opt_count) {
            $seq = $i + 1;
            if($is_div === 'div') {
                $str .= '<div class="get_item_supply">'.PHP_EOL;
                $str .= '<span style="display:none;"><label for="it_supply_'.$seq.'">'.$subj[$i].'</label></span>'.PHP_EOL;
            } else {
                $str .= '<tr>'.PHP_EOL;
                $str .= '<th><span style="display:none;"><label for="it_supply_'.$seq.'">'.$subj[$i].'</label></span></th>'.PHP_EOL;
            }

            $select = '<select id="it_supply_'.$seq.'" class="it_supply">'.PHP_EOL;
            $select .= '<option value="">'.$subj[$i].'</option>'.PHP_EOL;
            for($k=0; $k<$opt_count; $k++) {
                $opt_val = $opt[$k];
                if($opt_val) {
                    $select .= $opt_val.PHP_EOL;
                }
            }
            $select .= '</select>'.PHP_EOL;
            
            if($is_div === 'div') {
                $str .= '<span class="td_sit_sel">'.$select.'</span>'.PHP_EOL;
                $str .= '</div>'.PHP_EOL;
            } else {
                $str .= '<td class="td_sit_sel">'.$select.'</td>'.PHP_EOL;
                $str .= '</tr>'.PHP_EOL;
            }
        }
    }

    return $str;
}
function testaaa(){
    alert(selectoption);
}
// 상품 선택옵션
function get_item_options_new($mb_type, $it_id, $subject, $is_div='div', $opt)
{
    global $g5;

    $selectoption = selectoption;

    if(!$it_id || !$subject)
        return '';

    $sql = " select * from {$g5['g5_shop_item_option_table']} where io_type = '0' and it_id = '$it_id' and io_use = '1' order by io_no asc ";
    debug2("option_table : ".$sql);
    $result = sql_query($sql);

    $sql2 = " select * from {$g5['g5_shop_item_option_table']} where io_type = '0' and it_id = '$it_id' and io_use = '1' group by io_name order by io_no asc ";

    debug2("sql2 : ".$sql2);
    $result2 = sql_query($sql2);


    debug2("옵션갯수 : ".sql_num_rows($result));
    if(!sql_num_rows($result))
        return '';

    $str = '';

    $subj = explode(',', $subject);
    $subj_count = count($subj);
    $testoption = "<script>document.write(selectoption);</script>";
    echo $testoption;


    debug2("subj : ".$subject);
    debug2("subj_count : ".$subj_count);


    // 선택지 2개 이상
    if($subj_count > 1) {
        $options = array();

            $str .= '<div class="option-dropdown" id="get_item_options" disabled="disabled">' . PHP_EOL;
            $str .= '    <button class="btn-dropdown" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> 상품을 선택해주세요
                    </button>';
            $str .= '    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">';
        // 옵션항목 배열에 저장
        for($i=0; $row=sql_fetch_array($result); $i++) {
            $opt_id = explode(chr(30), $row['io_id']);
            debug2("색상 옵션 : ".$row['io_id']);
            for($k=0; $k<$subj_count; $k++) {
                if(!is_array($options[$k]))
                    $options[$k] = array();

                if($opt_id[$k] && !in_array($opt_id[$k], $options[$k]))
                    $options[$k][] = $opt_id[$k];
            }
        }
        for($i=0; $row=sql_fetch_array($result2); $i++) {
            $str .= '<div class="dropdown-item" data-value="">';
            $str .= '    <span class="option-desc io-name">'.$row['io_name'].'</span>';
            $str .= '</div>';
        }
        $str .= '</div>';
        $str .= '</div>';
        $str .= ' <input type="button" onclick="alert(selectoption)" value="테스트"/>';

        // 두번째 옵션
        $str .= '<div class="option-dropdown" id="get_item_options" >'.PHP_EOL;
        $str .= '    <button class="btn-dropdown" type="button" id="MenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" disabled="disabled">
                    옵션
                    </button>';
        $str .= '    <div class="dropdown-menu" aria-labelledby="MenuButton" >';

        $sql = " select * from {$g5['g5_shop_item_option_table']} where io_type = '0' and it_id = '$it_id' and io_use = '1' and io_name='".$selectoption."'  order by io_no asc ";
        debug2("option_table : ".$sql);
        $result = sql_query($sql);
            for($i=0; $row=sql_fetch_array($result); $i++) {

                //상품의 기본가
                $sql = " select it_price from {$g5['g5_shop_item_table']} where it_id = '$it_id'  ";
                debug2("상품 기본가 : ".$sql);
                $row2 = sql_fetch($sql);
                $it_price = (int)$row2['it_price'];

                $price = '&nbsp;&nbsp; '.number_format($row['io_price']).'원';

                debug2("price : " .$price);


                if($member['mb_type']==1) {
                    $price = $price.','.number_format($row['io_incen']).'P';
                }

                $io_stock_qty = get_option_stock_qty($it_id, $row['io_id'], $row['io_type']);


                if($io_stock_qty < 1)
                    $soldout = '&nbsp;&nbsp;[품절]';
                else
                    $soldout = '';

                $str .= '<div class="dropdown-item" data-value="'.$row['io_id'].','.$price.','.$row['io_stock_qty'].','.$row['io_name'].','.$row['io_drug_price'].'">';
                $str .= '    <span class="option-desc">'.$row['io_txt'].'</span>';
                $str .= '    <span class="option-price"><span class="price"> + '.$price.$soldout.'</span></span>';
                $str .= '</div>';



            }
        $str .= '</div>';
        $str .= '</div>';

    } else {
        $str .= '<div class="option-dropdown" id="get_item_options">'.PHP_EOL;
        $str .= '    <button class="btn-dropdown" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    상품을 선택해주세요
                    </button>';
        $str .= '    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">';
        for($i=0; $row=sql_fetch_array($result); $i++) {

            //상품의 기본가
            $sql = " select it_price
								from {$g5['g5_shop_item_table']}
								where it_id = '$it_id'  ";
            $row2 = sql_fetch($sql);
            $it_price = (int)$row2['it_price'];

            $io_price = $row['io_price'];
            if($io_price >= 0) {
                if($row['io_period_rate1'] > 0) {
                    $io_period_rate = (int)$row['io_period_rate1'];
                    $io_period_cnt1 = (int)$row['io_period_cnt1'];

                    $price = ((($it_price + $row['io_price'] ) * ($io_period_rate*0.01)) * $io_period_cnt1) - $it_price;
                    $io_price = $price;
                    $price = number_format($price);
                } else {
                    $price = number_format($io_price);
                }
            } else {
                $price = number_format($io_price);
            }

            /*if ($io_price == 0) {
                $price = number_format($it_price);
            }*/

            if($row['io_stock_qty'] < 1) {
                $soldout = '&nbsp;&nbsp;[품절]';
            } else {
                $soldout = '';
            }

            $str .= '<div class="dropdown-item" data-value="'.$row['io_id'].','.$io_price.','.$row['io_stock_qty'].','.$row['io_name'].','.$row['io_drug_price'].'">';
            $str .= '    <span class="option-desc">'.$row['io_name'].'</span>';
            $str .= '    <span class="option-price"><span class="price"> + '.$price.$soldout.'</span><span class="unit">원</span></span>';
            $str .= '</div>';
        }
        $str .= '</div>';
        $str .= '</div>';

    }

    return $str;
}



function print_item_options($it_id, $cart_id)
{
    global $g5;

    $sql = " select ct_option, ct_qty, it_id, io_price, io_id, io_type
                from {$g5['g5_shop_cart_table']} where it_id = '$it_id' and od_id = '$cart_id' order by io_type asc, ct_id asc ";
    $result = sql_query($sql);

    $str = '';
    for($i=0; $row=sql_fetch_array($result); $i++) {
        if($i == 0)
            $str .= '<ul>'.PHP_EOL;
        $price_plus = '';

		$io_price = $row['io_price'];

        if( $io_price >= 0) {

			//정기주문 옵션가 반영
			$optionprice = get_option_dis_price($row['it_id'], $row['io_id'],$row['io_type']);
			if ($optionprice > 0) {
				$io_price = $optionprice;
			}

            $price_plus = '+';
		}
        $str .= '<li>'.get_text($row['ct_option']).' '.$row['ct_qty'].'개 ('.$price_plus.display_price($io_price).')</li>'.PHP_EOL;
    }

    if($i > 0)
        $str .= '</ul>';

    return $str;
}


//옵션화면에 표현될때  + 금액된 총액 같이 표현되게 해주기
function print_item_options_new($it_id, $cart_id)
{
    global $g5 , $member;

	$return_tot_drug_price = 0;
	$return_tot_price = 0;

	$sql = "
			select 
					a.ct_option, a.ct_qty, a.it_id, a.io_id, a.io_type ,e.io_price ,e.io_drug_price , b.it_price , b.it_drug_price
			from tbl_shop_cart a left join tbl_shop_item b on a.it_id = b.it_id	
								 left join tbl_shop_item_option e ON a.it_id = e.it_id and a.io_id = e.io_id
			where a.it_id = '$it_id' and a.od_id = '$cart_id' order by a.io_type asc, a.ct_id asc 
	";

    $result = sql_query($sql);

    $str = '';
    for($i=0; $row=sql_fetch_array($result); $i++) {
        if($i == 0)
            $str .= '<ul>'.PHP_EOL;
        $price_plus = '';

		$io_drug_price = $row['io_drug_price'];
		$io_price = $row['io_price'];

        if( $io_price >= 0) {

			//정기주문 옵션가 반영
			$optionprice = get_option_dis_price_new($row['it_id'], $row['io_id'],$row['io_type']);
			if ($optionprice > 0) {
				$io_price = $optionprice;
			}

            $price_plus = '+';
		}

		$tot_drug_price = $row['it_drug_price'] + $io_drug_price;
		$tot_price = $row['it_price'] + $io_price;

		$return_tot_drug_price = $return_tot_drug_price + ($tot_drug_price * $row['ct_qty']);
		$return_tot_price = $return_tot_price + ($tot_price * $row['ct_qty']);

        //$str .= '<li>'.get_text($row['ct_option']).' '.$row['ct_qty'].'개 ('.$price_plus.display_price($io_price).')</li>'.PHP_EOL;
		//소비자인지 약국인지에 따른 가격분리요청에 의해 수정
		//mb_type = 1 -> 약국인데 모바일로접속하면 소비자가를 보여준다.ㅡ,.ㅡ; 약국이면서 모바일이아니면 약국가를 보여준다. ㅡ,.ㅡ;
		if( $member["mb_type"] == "1" && !is_mobile() ){
			$str .= '<li>'.get_text($row['ct_option']).' '.$row['ct_qty'].'개 ('.display_price($tot_drug_price).')</li>'.PHP_EOL;
		}else{
			$str .= '<li>'.get_text($row['ct_option']).' '.$row['ct_qty'].'개 ('.display_price($tot_price).')</li>'.PHP_EOL;		
		}
    }

    if($i > 0)
        $str .= '</ul>';

	//옵션내용 , 소비자가합 , 약국가합 리턴
    return $str."|".$return_tot_price."|".$return_tot_drug_price;
}

//옵션화면에 표현될때  + 금액된 총액 같이 표현되게 해주기
function print_item_options_new_mobile($it_id, $cart_id)
{
    global $g5 , $member;

    $return_tot_drug_price = 0;
    $return_tot_price = 0;

    $sql = "
			select 
					a.ct_option, a.ct_qty, a.it_id, a.io_id, a.io_type ,e.io_price ,e.io_drug_price , b.it_price , b.it_drug_price
			from tbl_shop_cart a left join tbl_shop_item b on a.it_id = b.it_id	
								 left join tbl_shop_item_option e ON a.it_id = e.it_id and a.io_id = e.io_id
			where a.it_id = '$it_id' and a.od_id = '$cart_id' order by a.io_type asc, a.ct_id asc 
	";

    $result = sql_query($sql);

    $str = '';
    for($i=0; $row=sql_fetch_array($result); $i++) {
        if($i == 0)
            $str .= '<ul>'.PHP_EOL;
        $price_plus = '';

        $io_drug_price = $row['io_drug_price'];
        $io_price = $row['io_price'];

        if( $io_price >= 0) {

            //정기주문 옵션가 반영
            $optionprice = get_option_dis_price_new($row['it_id'], $row['io_id'],$row['io_type']);
            if ($optionprice > 0) {
                $io_price = $optionprice;
            }

            $price_plus = '+';
        }

        $tot_drug_price = $row['it_drug_price'] + $io_drug_price;
        $tot_price = $row['it_price'] + $io_price;

        $return_tot_drug_price = $return_tot_drug_price + ($tot_drug_price * $row['ct_qty']);
        $return_tot_price = $return_tot_price + ($tot_price * $row['ct_qty']);

        //$str .= '<li>'.get_text($row['ct_option']).' '.$row['ct_qty'].'개 ('.$price_plus.display_price($io_price).')</li>'.PHP_EOL;
        //소비자인지 약국인지에 따른 가격분리요청에 의해 수정
        //mb_type = 1 -> 약국인데 모바일로접속하면 소비자가를 보여준다.ㅡ,.ㅡ; 약국이면서 모바일이아니면 약국가를 보여준다. ㅡ,.ㅡ;
        if( $member["mb_type"] == "1" && !is_mobile() ){
            $str .= '<li>'.get_text($row['ct_option']).' '.$row['ct_qty'].'개 ('.display_price($tot_drug_price).')</li>'.PHP_EOL;
        }else{
            $str .= '<li>'.get_text($row['ct_option']).' '.$row['ct_qty'].'개 ('.display_price($tot_price).')</li>'.PHP_EOL;
        }
    }

    if($i > 0)
        $str .= '</ul>';

    //옵션내용 , 소비자가합 , 약국가합 리턴
    return $str."|".$return_tot_price."|".$return_tot_drug_price;
}


// 일자형식변환
function date_conv($date, $case=1)
{
    if ($case == 1) { // 년-월-일 로 만들어줌
        $date = preg_replace("/([0-9]{4})([0-9]{2})([0-9]{2})/", "\\1-\\2-\\3", $date);
    } else if ($case == 2) { // 년월일 로 만들어줌
        $date = preg_replace("/-/", "", $date);
    }

    return $date;
}


// 배너출력
function display_banner($position, $skin='')
{
    global $g5;

    if (!$position) $position = '왼쪽';
    if (!$skin) $skin = 'boxbanner.skin.php';

    $skin_path = G5_SHOP_SKIN_PATH.'/'.$skin;
    if(G5_IS_MOBILE)
        $skin_path = G5_MSHOP_SKIN_PATH.'/'.$skin;

    if(file_exists($skin_path)) {
        // 접속기기
        $sql_device = " and ( bn_device = 'both' or bn_device = 'pc' ) ";
        if(G5_IS_MOBILE)
            $sql_device = " and ( bn_device = 'both' or bn_device = 'mobile' ) ";

        // 배너 출력
        $sql = " select * from {$g5['g5_shop_banner_table']} where '".G5_TIME_YMDHIS."' between bn_begin_time and bn_end_time $sql_device and bn_position = '$position' order by bn_order, bn_id desc ";
        $result = sql_query($sql);

        include $skin_path;
    } else {
        echo '<p>'.str_replace(G5_PATH.'/', '', $skin_path).'파일이 존재하지 않습니다.</p>';
    }
}
// 배너출력
function main_banner($position, $limit=0)
{
    global $g5 , $member;

	$add_sql = "";
    $limit_sql = "";

	//로그아웃 상태일때는 전체로 체크된것들만 보이게 하기 
	if( empty($member['mb_type']) ){
		$add_sql = " and bn_view = 'a' ";
	}
	//약국회원 로그인이 아닐때 노출대상이 소비자인것들만 보이게하기
	if( !empty($member['mb_type']) && $member['mb_type'] != "1" ){
		$add_sql = " and bn_view in( 'a' , 'b' ) ";
	}
	//약국회원 로그인일때 노출대상이 약사인것들만 보이게 하기 
	if( !empty($member['mb_type']) && $member['mb_type'] == "1" ){
		$add_sql = " and bn_view in( 'a' , 'c' ) ";
	}
	//관리자일때는 모두보이게
	if( !empty($member['mb_type']) && $member['mb_type'] == "9" ){
		$add_sql = " and bn_view in( 'a' , 'b' , 'c' ) ";
	}

    if (!$position) $position = '왼쪽';
	if ($limit > 0) $limit_sql = " limit $limit ";

	// 접속기기
	$sql_device = " and ( bn_device = 'both' or bn_device = 'pc' ) and service_view_1 = 'Y'  ";

	if(G5_IS_MOBILE)
		$sql_device = " and ( bn_device = 'both' or bn_device = 'mobile' ) and service_view_1 = 'Y'  ";

	// 배너 출력
	$sql = " select * from {$g5['g5_shop_banner_table']} where '".G5_TIME_YMDHIS."' between bn_begin_time and bn_end_time $sql_device and bn_position = '$position' $add_sql order by bn_order, bn_id desc $limit_sql";
	$result = sql_query($sql);


	return $result;  
}

// 배너 시간 체크


// 1.00.02
// 파일번호, 이벤트번호, 1라인이미지수, 총라인수, 이미지폭, 이미지높이
// 1.02.01 $ca_id 추가
function display_event($no, $event, $list_mod, $list_row, $img_width, $img_height, $ca_id="")
{
    global $member, $g5;

    // 상품수
    $items = $list_mod * $list_row;

    // 1.02.00
    // b.it_order 추가
    $sql = " select b.* from {$g5['g5_shop_event_item_table']} a, {$g5['g5_shop_item_table']} b where a.it_id = b.it_id and b.it_use = '1' and a.ev_id = '$event' ";
    if ($ca_id) $sql .= " and ca_id = '$ca_id' ";
    $sql .= " order by b.it_order, a.it_id desc limit $items ";
    $result = sql_query($sql);
    if (!sql_num_rows($result)) {
        return false;
    }

    $file = G5_SHOP_PATH.'/maintype'.$no.'.inc.php';
    if (!file_exists($file)) {
        echo $file.' 파일을 찾을 수 없습니다.';
    } else {
        $td_width = (int)(100 / $list_mod);
        include $file;
    }
}


function get_yn($val, $case='')
{
    switch ($case) {
        case '1' : $result = ($val > 0) ? 'Y' : 'N'; break;
        default :  $result = ($val > 0) ? '예' : '아니오';
    }
    return $result;
}


// 상품명과 건수를 반환
function get_goods($cart_id)
{
    global $g5;

    // 상품명만들기
    $row = sql_fetch(" select a.it_id, b.it_name from {$g5['g5_shop_cart_table']} a, {$g5['g5_shop_item_table']} b where a.it_id = b.it_id and a.od_id = '$cart_id' order by ct_id limit 1 ");
    // 상품명에 "(쌍따옴표)가 들어가면 오류 발생함
    $goods['it_id'] = $row['it_id'];
    $goods['full_name']= $goods['name'] = addslashes($row['it_name']);
    // 특수문자제거
    $goods['full_name'] = preg_replace ("/[ #\&\+\-%@=\/\\\:;,\.'\"\^`~\_|\!\?\*$#<>()\[\]\{\}]/i", "",  $goods['full_name']);

    // 상품건수
    $row = sql_fetch(" select count(*) as cnt from {$g5['g5_shop_cart_table']} where od_id = '$cart_id' ");
    $cnt = $row['cnt'] - 1;
    if ($cnt)
        $goods['full_name'] .= ' 외 '.$cnt.'건';
    $goods['count'] = $row['cnt'];

    return $goods;
}


// 패턴의 내용대로 해당 디렉토리에서 정렬하여 <select> 태그에 적용할 수 있게 반환
function get_list_skin_options($pattern, $dirname='./', $sval='')
{
    $str = '<option value="">선택</option>'.PHP_EOL;

    unset($arr);
    $handle = opendir($dirname);
    while ($file = readdir($handle)) {
        if (preg_match("/$pattern/", $file, $matches)) {
            $arr[] = $matches[0];
        }
    }
    closedir($handle);

    sort($arr);
    foreach($arr as $value) {
        if($value == $sval)
            $selected = ' selected="selected"';
        else
            $selected = '';

        $str .= '<option value="'.$value.'"'.$selected.'>'.$value.'</option>'.PHP_EOL;
    }

    return $str;
}

// 특정상품 회원별 노출, 접근 처리
function get_item_auth_member($mb_id, $it_id)
{
    global $g5, $member;

    $sql = "SELECT * FROM tbl_shop_item_auth WHERE it_id='$it_id' and mb_id='$mb_id'";
    $result = sql_query($sql);

    if (!sql_num_rows($result)) {
        return false;
    }

    return true;

}

// 예외처리 되는 상품인지 확인
function get_item_auth_check($id, $type=1)
{

    if ($type == 1) { // 상품 유무 체크
        $sql = "SELECT * FROM tbl_shop_item_auth WHERE it_id='$id' and t_code='0'";
    } else if ($type == 2) { // 회원 유무 체크
        $sql = "SELECT * FROM tbl_shop_item_auth WHERE mb_id='$id' and t_code='0'";
    }

    $result = sql_query($sql);
    if (!sql_num_rows($result)) {
        return false;
    } else {
        return true;
    }
}


// 일자 시간을 검사한다.
function check_datetime($datetime)
{
    if ($datetime == "0000-00-00 00:00:00")
        return true;

    $year   = substr($datetime, 0, 4);
    $month  = substr($datetime, 5, 2);
    $day    = substr($datetime, 8, 2);
    $hour   = substr($datetime, 11, 2);
    $minute = substr($datetime, 14, 2);
    $second = substr($datetime, 17, 2);

    $timestamp = mktime($hour, $minute, $second, $month, $day, $year);

    $tmp_datetime = date("Y-m-d H:i:s", $timestamp);
    if ($datetime == $tmp_datetime)
        return true;
    else
        return false;
}


// 경고메세지를 경고창으로
function alert_opener($msg='', $url='')
{
    global $g5;

    if (!$msg) $msg = '올바른 방법으로 이용해 주십시오.';

    echo "<meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\">"; 
    echo "<script>";
    echo "alert(\"$msg\");";
    echo "
		try{
			opener.location.href=\"$url\";
			self.close();
		}catch(e){
			parent.window.$.featherlight.current().close();
			parent.location.reload();
		}
		";
    echo "</script>";
    exit;
}


// option 리스트에 selected 추가
function conv_selected_option($options, $value)
{
    if(!$options)
        return '';

    $options = str_replace('value="'.$value.'"', 'value="'.$value.'" selected', $options);

    return $options;
}


// 주문서 번호를 얻는다.
function get_new_od_id()
{
    global $g5;

    // 주문서 테이블 Lock 걸고
    sql_query(" LOCK TABLES {$g5['g5_shop_order_table']} READ, {$g5['g5_shop_order_table']} WRITE ", FALSE);
    // 주문서 번호를 만든다.
    $date = date("ymd", time());    // 2002년 3월 7일 일경우 020307
    $sql = " select max(od_id) as max_od_id from {$g5['g5_shop_order_table']} where SUBSTRING(od_id, 1, 6) = '$date' ";
    $row = sql_fetch($sql);
    $od_id = $row['max_od_id'];
    if ($od_id == 0)
        $od_id = 1;
    else
    {
        $od_id = (int)substr($od_id, -4);
        $od_id++;
    }
    $od_id = $date . substr("0000" . $od_id, -4);
    // 주문서 테이블 Lock 풀고
    sql_query(" UNLOCK TABLES ", FALSE);

    return $od_id;
}


// cart id 설정
function set_cart_id($direct)
{
    global $g5, $default, $member;

    if ($direct) {
        $tmp_cart_id = get_session('ss_cart_direct');
        if(!$tmp_cart_id) {
            $tmp_cart_id = get_uniqid();
            set_session('ss_cart_direct', $tmp_cart_id);
        }
    } else {
        // 비회원장바구니 cart id 쿠키설정
        if($default['de_guest_cart_use']) {
            $tmp_cart_id = get_cookie('ck_guest_cart_id');
            if($tmp_cart_id) {
                set_session('ss_cart_id', $tmp_cart_id);
                //set_cookie('ck_guest_cart_id', $tmp_cart_id, ($default['de_cart_keep_term'] * 86400));
            } else {
                $tmp_cart_id = get_uniqid();
                set_session('ss_cart_id', $tmp_cart_id);
                set_cookie('ck_guest_cart_id', $tmp_cart_id, ($default['de_cart_keep_term'] * 86400));
            }
        } else {
            $tmp_cart_id = get_session('ss_cart_id');
            if(!$tmp_cart_id) {
                $tmp_cart_id = get_uniqid();
                set_session('ss_cart_id', $tmp_cart_id);
            }
        }

        // 보관된 회원장바구니 자료 cart id 변경
        if($member['mb_id'] && $tmp_cart_id) {
            $sql = " update {$g5['g5_shop_cart_table']}
                        set od_id = '$tmp_cart_id'
                        where mb_id = '{$member['mb_id']}'
                          and ct_direct = '0'
                          and ct_status = '쇼핑' {$service_type_and_query} ";
            sql_query($sql);
        }
    }
}


// 상품 목록 : 관련 상품 출력
function relation_item($it_id, $width, $height, $rows=3)
{
    global $g5;

    $str = '';

    if(!$it_id)
        return $str;

    $sql = " select b.it_id, b.it_name, b.it_price, b.it_tel_inq from {$g5['g5_shop_item_relation_table']} a left join {$g5['g5_shop_item_table']} b on ( a.it_id2 = b.it_id ) where a.it_id = '$it_id' order by ir_no asc limit 0, $rows ";
    $result = sql_query($sql);

    for($i=0; $row=sql_fetch_array($result); $i++) {
        if($i == 0) {
            $str .= '<span class="sound_only">관련 상품 시작</span>';
            $str .= '<ul class="sct_rel_ul">';
        }

        $it_name = get_text($row['it_name']); // 상품명
        $it_price = get_price($row); // 상품가격
        if(!$row['it_tel_inq'])
            $it_price = display_price($it_price);

        $img = get_it_image($row['it_id'], $width, $height);

        $str .= '<li class="sct_rel_li"><a href="'.G5_SHOP_URL.'/item.php?it_id='.$row['it_id'].'" class="sct_rel_a">'.$img.'</a></li>';
    }

    if($i > 0)
        $str .= '</ul><span class="sound_only">관련 상품 끝</span>';

    return $str;
}


// 상품이미지에 유형 아이콘 출력
function item_icon($it)
{
    global $g5;

    $icon = '<span class="sit_icon">';

    if ($it['it_type1'])
        $icon .= '<span class="shop_icon shop_icon_1">BEST</span>';

    if ($it['it_type2'])
        $icon .= '<span class="shop_icon shop_icon_2">추천</span>';

    if ($it['it_type3'])
        $icon .= '<span class="shop_icon shop_icon_3">신상품</span>';

    if ($it['it_type4'])
        $icon .= '<span class="shop_icon shop_icon_4">BEST</span>';

    if ($it['it_type5'])
        $icon .= '<span class="shop_icon shop_icon_5">할인</span>';


    // 쿠폰상품
    $sql = " select count(*) as cnt
                from {$g5['g5_shop_coupon_table']}
                where cp_start <= '".G5_TIME_YMD."'
                  and cp_end >= '".G5_TIME_YMD."'
                  and (
                        ( cp_method = '0' and cp_target = '{$it['it_id']}' )
                        OR
                        ( cp_method = '1' and ( cp_target IN ( '{$it['ca_id']}', '{$it['ca_id2']}', '{$it['ca_id3']}' ) ) )
                      ) ";
    $row = sql_fetch($sql);
    if($row['cnt'])
        $icon .= '<span class="shop_icon shop_icon_coupon">쿠폰</span>';

    // 품절
    if (is_soldout($it['it_id']))
        $icon .= '<br><span class="shop_icon_soldout">Sold Out</span>';

    $icon .= '</span>';

    return $icon;
}


// sns 공유하기
function get_sns_share_link($sns, $url, $title, $img)
{
    global $config;

    if(!$sns)
        return '';

    switch($sns) {
        case 'facebook':
            $str = '<a href="https://www.facebook.com/sharer/sharer.php?u='.urlencode($url).'&amp;p='.urlencode($title).'" class="share-facebook" target="_blank"><img src="'.$img.'" alt="페이스북에 공유"></a>';
            break;
        case 'twitter':
            $str = '<a href="https://twitter.com/share?url='.urlencode($url).'&amp;text='.urlencode($title).'" class="share-twitter" target="_blank"><img src="'.$img.'" alt="트위터에 공유"></a>';
            break;
        case 'googleplus':
            $str = '<a href="https://plus.google.com/share?url='.urlencode($url).'" class="share-googleplus" target="_blank"><img src="'.$img.'" alt="구글플러스에 공유"></a>';
            break;

        case 'kakaotalk':
            if($config['cf_kakao_js_apikey'])
                $str = '<a id="kakao-link-btn" href="javascript:;" class="share-kakaotalk"><img src="'.$img.'" alt="카카오톡 링크보내기"></a>';
            break;
        // 상품페이지에서 사용되는 태그 (신규추가 20210225)
        case 'facebook_new': //
            $str = '<a href="https://www.facebook.com/sharer/sharer.php?u='.urlencode($url).'&amp;p='.urlencode($title).'" class="icon share-facebook" target="_blank"></a>';
            break;
        case 'kakaotalk_new':
            if($config['cf_kakao_js_apikey'])
                $str = '<a id="kakao-link-btn" href="javascript:;" class="icon share-kakao" title="카카오톡"></a>';
            break;
    }

    return $str;
}


// 상품이미지 썸네일 삭제
function delete_item_thumbnail($dir, $file)
{
    if(!$dir || !$file)
        return;

    $filename = preg_replace("/\.[^\.]+$/i", "", $file); // 확장자제거

    $files = glob($dir.'/thumb-'.$filename.'*');

    if(is_array($files)) {
        foreach($files as $thumb_file) {
            @unlink($thumb_file);
        }
    }
}


// 쿠폰번호 생성함수
function get_coupon_id()
{
    $len = 16;
    $chars = "ABCDEFGHJKLMNPQRSTUVWXYZ123456789";

    srand((double)microtime()*1000000);

    $i = 0;
    $str = '';

    while ($i < $len) {
        $num = rand() % strlen($chars);
        $tmp = substr($chars, $num, 1);
        $str .= $tmp;
        $i++;
    }

    $str = preg_replace("/([0-9A-Z]{4})([0-9A-Z]{4})([0-9A-Z]{4})([0-9A-Z]{4})/", "\\1-\\2-\\3-\\4", $str);

    return $str;
}


// 주문의 금액, 배송비 과세금액 등의 정보를 가져옴
function get_order_info($od_id)
{
    global $g5;

    // 주문정보
    $sql = " select * from {$g5['g5_shop_order_table']} where od_id = '$od_id' ";
    $od = sql_fetch($sql);

    if(!$od['od_id'])
        return false;

    $info = array();

    // 장바구니 주문금액정보
    $sql = " 
	  select SUM(cp_price) as coupon , 
	  case when ct_period_yn != 'Y' then SUM(IF(io_type = 1, (io_price * ct_qty), ((ct_price + io_price) * ct_qty))) 
		   when ct_period_yn = 'Y' then SUM(IF(io_type = 1, (io_price * ct_qty*ct_period_cnt), ((ct_price + io_price) * ct_qty)*ct_period_cnt))
	  END as price , 
	  case when ct_period_yn != 'Y' then SUM( IF( ct_notax = 0, ( IF(io_type = 1, (io_price * ct_qty), ( (ct_price + io_price) * ct_qty) ) - cp_price ), 0 ) )
		   when ct_period_yn = 'Y' then SUM( IF( ct_notax = 0, ( IF(io_type = 1, (io_price * ct_qty*ct_period_cnt), ( (ct_price + io_price) * ct_qty)*ct_period_cnt ) - cp_price ), 0 ) )
	  END AS tax_mny , 
	  case when ct_period_yn != 'Y' then SUM( IF( ct_notax = 1, ( IF(io_type = 1, (io_price * ct_qty), ( (ct_price + io_price) * ct_qty) ) - cp_price ), 0 ) ) 
		   when ct_period_yn = 'Y' then SUM( IF( ct_notax = 1, ( IF(io_type = 1, (io_price * ct_qty*ct_period_cnt), ( (ct_price + io_price) * ct_qty)*ct_period_cnt ) - cp_price ), 0 ) )    
	  END as free_mny
	  from {$g5['g5_shop_cart_table']} where od_id = '$od_id' and ct_status IN ( '주문', '입금', '준비', '배송', '완료' ) ";

    $sum = sql_fetch($sql);

    $cart_price = $sum['price'];
    $cart_coupon = $sum['coupon'];

    // 배송비
    $send_cost = get_sendcost($od_id);

    $od_coupon = $od_send_coupon = 0;

    if($od['mb_id']) {
        // 주문할인 쿠폰
        $sql = " select a.cp_id, a.cp_type, a.cp_price, a.cp_trunc, a.cp_minimum, a.cp_maximum
                    from {$g5['g5_shop_coupon_table']} a right join {$g5['g5_shop_coupon_log_table']} b on ( a.cp_id = b.cp_id )
                    where b.od_id = '$od_id'
                      and b.mb_id = '{$od['mb_id']}'
                      and a.cp_method = '2'  ";
        $cp = sql_query($sql);

        //$tot_od_price = $cart_price - $cart_coupon;

		for($i=0; $row=sql_fetch_array($cp); $i++) {

			if($row['cp_id']) {
				
				$od_coupon += $row['cp_price'];

			}

		}

        // 배송쿠폰 할인
        $sql2 = " select a.cp_id, a.cp_type, a.cp_price, a.cp_trunc, a.cp_minimum, a.cp_maximum
                    from {$g5['g5_shop_coupon_table']} a right join {$g5['g5_shop_coupon_log_table']} b on ( a.cp_id = b.cp_id )
                    where b.od_id = '$od_id'
                      and b.mb_id = '{$od['mb_id']}'
                      and a.cp_method = '3'   ";
        $cp2 = sql_query($sql2);

		for($i=0; $row2=sql_fetch_array($cp2); $i++) {

			if($row2['cp_id']) {
				
				$od_send_coupon += $row2['cp_price'];

			}

		}
    }

    // 과세, 비과세 금액정보
    $tax_mny = $sum['tax_mny'];
    $free_mny = $sum['free_mny'];

    if($od['od_tax_flag']) {
        $tot_tax_mny = ( $tax_mny + $send_cost + $od['od_send_cost2'] )
                       - ( $od_coupon + $od_send_coupon + $od['od_receipt_point'] );
        if($tot_tax_mny < 0) {
            $free_mny += $tot_tax_mny;
            $tot_tax_mny = 0;
        }
    } else {
        $tot_tax_mny = ( $tax_mny + $free_mny + $send_cost + $od['od_send_cost2'] )
                       - ( $od_coupon + $od_send_coupon + $od['od_receipt_point'] );
        $free_mny = 0;
    }

    $od_tax_mny = round($tot_tax_mny / 1.1);
    $od_vat_mny = $tot_tax_mny - $od_tax_mny;
    $od_free_mny = $free_mny;

    // 장바구니 취소금액 정보
    $sql = " select ifnull(
			  case when ct_period_yn != 'Y' then SUM(IF(io_type = 1, (io_price * ct_qty), ((ct_price + io_price) * ct_qty))) 
				   when ct_period_yn = 'Y' then SUM(IF(io_type = 1, (io_price * ct_qty*ct_period_cnt), ((ct_price + io_price) * ct_qty)*ct_period_cnt))
			  END , 0 ) as price from {$g5['g5_shop_cart_table']} where od_id = '$od_id' and ct_status IN ( '취소', '반품', '품절' ) ";

    $sum = sql_fetch($sql);
    $cancel_price = $sum['price'];

    // 미수금액
    $od_misu = ( $cart_price + $send_cost + $od['od_send_cost2'] )
               - ( $cart_coupon + $od_coupon + $od_send_coupon )
               - ( $od['od_receipt_price'] + $od['od_receipt_point'] - $od['od_refund_price'] );

    // 장바구니상품금액
    $od_cart_price = $cart_price + $cancel_price;

    // 결과처리
    $info['od_cart_price']      = $od_cart_price;
    $info['od_send_cost']       = $send_cost;
    $info['od_coupon']          = $od_coupon;
    $info['od_send_coupon']     = $od_send_coupon;
    $info['od_cart_coupon']     = $cart_coupon;
    $info['od_tax_mny']         = $od_tax_mny;
    $info['od_vat_mny']         = $od_vat_mny;
    $info['od_free_mny']        = $od_free_mny;
    $info['od_cancel_price']    = $cancel_price;
    $info['od_misu']            = $od_misu;

    return $info;
}


// 상품포인트
function get_item_point($it, $io_id='', $trunc=10)
{
    global $g5;

    $it_point = 0;

    if($it['it_point_type'] > 0) {
        $it_price = $it['it_price'];

        if($it['it_point_type'] == 2 && $io_id) {
            $sql = " select io_id, io_price
                        from {$g5['g5_shop_item_option_table']}
                        where it_id = '{$it['it_id']}'
                          and io_id = '$io_id'
                          and io_type = '0'
                          and io_use = '1' ";
            $opt = sql_fetch($sql);

            if($opt['io_id'])
                $it_price += $opt['io_price'];
        }

        $it_point = floor(($it_price * ($it['it_point'] / 100) / $trunc)) * $trunc;
    } else {
        $it_point = $it['it_point'];
    }

    return $it_point;
}


// 배송비 구함
function get_sendcost($cart_id, $selected=1)
{
    global $default, $g5;

    $send_cost = 0;
    $total_price = 0;
    $total_send_cost = 0;
    $diff = 0;

	//공급사별 배송비 구하기

    $sql = " select distinct it_id
                from {$g5['g5_shop_cart_table']}
                where od_id = '$cart_id'
                  and ct_send_cost = '0'
                  and ct_status IN ( '쇼핑', '주문', '입금', '준비', '배송', '완료' )
                  and ct_select = '$selected' ";

    $result = sql_query($sql);
    for($i=0; $sc=sql_fetch_array($result); $i++) {
        // 합계
        $sql = " select SUM(IF(io_type = 1, (io_price * ct_qty), ((ct_price + io_price) * ct_qty))) as price,
                        SUM(ct_qty) as qty
                    from {$g5['g5_shop_cart_table']}
                    where it_id = '{$sc['it_id']}'
                      and od_id = '$cart_id'
                      and ct_status IN ( '쇼핑', '주문', '입금', '준비', '배송', '완료' )
                      and ct_select = '$selected'";
        $sum = sql_fetch($sql);

        $send_cost = get_item_sendcost($sc['it_id'], $sum['price'], $sum['qty'], $cart_id);

        if($send_cost > 0)
            $total_send_cost += $send_cost;
		
		/*
        if($default['de_send_cost_case'] == '차등' && $send_cost == -1) {
            $total_price += $sum['price'];
            $diff++;
        }
		*/
    }

	/*
    $send_cost = 0;
    if($default['de_send_cost_case'] == '차등' && $total_price >= 0 && $diff > 0) {
        // 금액별차등 : 여러단계의 배송비 적용 가능
        $send_cost_limit = explode(";", $default['de_send_cost_limit']);
        $send_cost_list  = explode(";", $default['de_send_cost_list']);
        $send_cost = 0;
        for ($k=0; $k<count($send_cost_limit); $k++) {
            // 총판매금액이 배송비 상한가 보다 작다면
            if ($total_price < preg_replace('/[^0-9]/', '', $send_cost_limit[$k])) {
                $send_cost = preg_replace('/[^0-9]/', '', $send_cost_list[$k]);
                break;
            }
        }
    }
    return ($total_send_cost + $send_cost);
	*/

	return ($total_send_cost);
}



// 상품별 배송비
function get_item_sendcost($it_id, $price, $qty, $cart_id)
{
    global $g5, $default;

    $sql = " select it_id, it_sc_type, it_sc_method, it_sc_price, it_sc_minimum, it_sc_qty, it_sc_cost
                from {$g5['g5_shop_cart_table']}
                where it_id = '$it_id'
                  and od_id = '$cart_id'
                order by ct_id
                limit 1 ";
    $ct = sql_fetch($sql);
    if(!$ct['it_id'])
        return 0;
	/*
    if($ct['it_sc_type'] > 1) {
        if($ct['it_sc_type'] == 2) { // 조건부무료
            if($price >= $ct['it_sc_minimum'])
                $sendcost = 0;
            else
                $sendcost = $ct['it_sc_price'];
        } else if($ct['it_sc_type'] == 3) { // 유료배송
            $sendcost = $ct['it_sc_price'];
        } else { // 수량별 부과
            if(!$ct['it_sc_qty'])
                $ct['it_sc_qty'] = 1;

            $q = ceil((int)$qty / (int)$ct['it_sc_qty']);
            $sendcost = (int)$ct['it_sc_price'] * $q;
        }
    } else if($ct['it_sc_type'] == 1) { // 무료배송
        $sendcost = 0;
    } else {
        $sendcost = -1;
    }
	*/
	$sendcost = $ct['it_sc_cost'];

    return $sendcost;
}


// 가격비교 사이트 상품 배송비
function get_item_sendcost2($it_id, $price, $qty)
{
    global $g5, $default;

    $sql = " select it_id, it_sc_type, it_sc_method, it_sc_price, it_sc_minimum, it_sc_qty
                from {$g5['g5_shop_item_table']}
                where it_id = '$it_id' ";
    $it = sql_fetch($sql);
    if(!$it['it_id'])
        return 0;

    $sendcost = 0;

    // 쇼핑몰 기본설정을 사용할 때
    if($it['it_sc_type'] == 0)
    {
        if($default['de_send_cost_case'] == '차등') {
            // 금액별차등 : 여러단계의 배송비 적용 가능
            $send_cost_limit = explode(";", $default['de_send_cost_limit']);
            $send_cost_list  = explode(";", $default['de_send_cost_list']);

            for ($k=0; $k<count($send_cost_limit); $k++) {
                // 총판매금액이 배송비 상한가 보다 작다면
                if ($price < preg_replace('/[^0-9]/', '', $send_cost_limit[$k])) {
                    $sendcost = preg_replace('/[^0-9]/', '', $send_cost_list[$k]);
                    break;
                }
            }
        }
    }
    else
    {
        if($it['it_sc_type'] > 1) {
            if($it['it_sc_type'] == 2) { // 조건부무료
                if($price >= $it['it_sc_minimum'])
                    $sendcost = 0;
                else
                    $sendcost = $it['it_sc_price'];
            } else if($it['it_sc_type'] == 3) { // 유료배송
                $sendcost = $it['it_sc_price'];
            } else { // 수량별 부과
                if(!$it['it_sc_qty'])
                    $it['it_sc_qty'] = 1;

                $q = ceil((int)$qty / (int)$it['it_sc_qty']);
                $sendcost = (int)$it['it_sc_price'] * $q;
            }
        } else if($it['it_sc_type'] == 1) { // 무료배송
            $sendcost = 0;
        }
    }

    return $sendcost;
}


// 쿠폰 사용체크
function is_used_coupon($mb_id, $cp_id)
{
    global $g5;

    $used = false;

    $sql = " select count(*) as cnt from {$g5['g5_shop_coupon_log_table']} where mb_id = '$mb_id' and cp_id = '$cp_id' ";
    $row = sql_fetch($sql);

    if($row['cnt'])
        $used = true;

    return $used;
}

// 첫구매 체크
function is_first_order($mb_id)
{
    global $g5;

    $used = true;

    $sql = " select count(*) as cnt from {$g5['g5_shop_order_table']} where mb_id = '$mb_id' ";
    $row = sql_fetch($sql);

    if($row['cnt'])
        $used = false;

    return $used;
}


// 품절상품인지 체크
function is_soldout($it_id)
{
    global $g5;

    // 상품정보
    $sql = " select it_soldout, it_stock_qty from {$g5['g5_shop_item_table']} where it_id = '$it_id' ";
    $it = sql_fetch($sql);

    if($it['it_soldout'] || $it['it_stock_qty'] <= 0)
        return true;

    $count = 0;
    $soldout = false;

    // 상품에 선택옵션 있으면..
    $sql = " select count(*) as cnt from {$g5['g5_shop_item_option_table']} where it_id = '$it_id' and io_type = '0' ";
    $row = sql_fetch($sql);

    if($row['cnt']) {
        $sql = " select io_id, io_type, io_stock_qty
                    from {$g5['g5_shop_item_option_table']}
                    where it_id = '$it_id'
                      and io_type = '0'
                      and io_use = '1' ";
        $result = sql_query($sql);

        for($i=0; $row=sql_fetch_array($result); $i++) {
            // 옵션 재고수량
            $stock_qty = get_option_stock_qty($it_id, $row['io_id'], $row['io_type']);

            if($stock_qty <= 0)
                $count++;
        }

        // 모든 선택옵션 품절이면 상품 품절
        if($i == $count)
            $soldout = true;
    } else {
        // 상품 재고수량
        $stock_qty = get_it_stock_qty($it_id);

        if($stock_qty <= 0)
            $soldout = true;
    }

    return $soldout;
}

// 상품후기 작성가능한지 체크
function check_itemuse_write($it_id, $mb_id, $close=true)
{
    global $g5, $default, $is_admin;

    if(!$is_admin && $default['de_item_use_write'])
    {
        $sql = " select count(*) as cnt
                    from tbl_shop_order_detail a, tbl_shop_order_receiver b, tbl_shop_order c
                    where a.od_id = b.od_id
					  and a.od_num = b.od_num
					  and a.od_id = c.od_id
					  and a.it_id = '$it_id'
                      and c.mb_id = '$mb_id'
                      and b.od_status in ('배송', '완료') ";
        $row = sql_fetch($sql);

        if($row['cnt'] == 0)
        {
            if($close)
                alert_close('사용후기는 주문이 완료된 경우에만 작성하실 수 있습니다.');
            else
                alert('사용후기는 주문하신 상품의 상태가 완료인 경우에만 작성하실 수 있습니다.');
        }
    }
}


// 구매 본인인증 체크
function shop_member_cert_check($id, $type)
{
    global $g5, $member;

    $msg = '';

    switch($type)
    {
        case 'item':
            $sql = " select ca_id, ca_id2, ca_id3 from {$g5['g5_shop_item_table']} where it_id = '$id' ";
            $it = sql_fetch($sql);

            $seq = '';
            for($i=0; $i<3; $i++) {
                $ca_id = $it['ca_id'.$seq];

                if(!$ca_id)
                    continue;

                $sql = " select ca_cert_use, ca_adult_use from {$g5['g5_shop_category_table']} where ca_id = '$ca_id' ";
                $row = sql_fetch($sql);

                // 본인확인체크
                if($row['ca_cert_use'] && !$member['mb_certify']) {
                    if($member['mb_id'])
                        $msg = '회원정보 수정에서 본인확인 후 이용해 주십시오.';
                    else
                        $msg = '본인확인된 로그인 회원만 이용할 수 있습니다.';

                    break;
                }

                // 성인인증체크
                if($row['ca_adult_use'] && !$member['mb_adult']) {
                    if($member['mb_id'])
                        $msg = '본인확인으로 성인인증된 회원만 이용할 수 있습니다.\\n회원정보 수정에서 본인확인을 해주십시오.';
                    else
                        $msg = '본인확인으로 성인인증된 회원만 이용할 수 있습니다.';

                    break;
                }

                if($i == 0)
                    $seq = 1;
                $seq++;
            }

            break;
        case 'list':
            $sql = " select * from {$g5['g5_shop_category_table']} where ca_id = '$id' ";
            $ca = sql_fetch($sql);

            // 본인확인체크
            if($ca['ca_cert_use'] && !$member['mb_certify']) {
                if($member['mb_id'])
                    $msg = '회원정보 수정에서 본인확인 후 이용해 주십시오.';
                else
                    $msg = '본인확인된 로그인 회원만 이용할 수 있습니다.';
            }

            // 성인인증체크
            if($ca['ca_adult_use'] && !$member['mb_adult']) {
                if($member['mb_id'])
                    $msg = '본인확인으로 성인인증된 회원만 이용할 수 있습니다.\\n회원정보 수정에서 본인확인을 해주십시오.';
                else
                    $msg = '본인확인으로 성인인증된 회원만 이용할 수 있습니다.';
            }

            break;
        default:
            break;
    }

    return $msg;
}


// 배송조회버튼 생성
function get_delivery_inquiry($company, $invoice, $class='')
{
    if(!$company || !$invoice)
        return '';

    $dlcomp = explode(")", str_replace("(", "", G5_DELIVERY_COMPANY));
	
	$invoice = str_replace("-", "", $invoice);

    for($i=0; $i<count($dlcomp); $i++) {
        if(strstr($dlcomp[$i], $company)) {
            list($com, $url, $tel) = explode("^", $dlcomp[$i]);
            break;
        }
    }

    $str = '';
    if($com && $url) {
        if (G5_IS_MOBILE) {
            $str .= '<a href="'.$url.$invoice.'" target="_self"';
        } else {
            $str .= '<a href="'.$url.$invoice.'" target="_blank"';
        }

        if($class)
            $str .= ' class="'.$class.'"';
        $str .='>배송조회</a>';
        if($tel)
            $str .= ' (문의전화: '.$tel.')';
    }

    return $str;
}

// 배송조회버튼만 생성
function get_delivery_inquiry_button($company, $invoice, $class='')
{
    if(!$company || !$invoice)
        return '';

    $dlcomp = explode(")", str_replace("(", "", G5_DELIVERY_COMPANY));
	
	$invoice = str_replace("-", "", $invoice);

    for($i=0; $i<count($dlcomp); $i++) {
        if(strstr($dlcomp[$i], $company)) {
            list($com, $url, $tel) = explode("^", $dlcomp[$i]);
            break;
        }
    }

    $str = '';
    if($com && $url) {
        $str .= '<a href="'.$url.$invoice.'" target="_self"';
		//$str .= '<a href="'.$url.$invoice.'" target="_blank"';
        if($class)
            $str .= ' class="'.$class.'"';
        $str .='>배송조회</a>';
        
    }

    return $str;
}

// 사용후기의 확인된 건수를 상품테이블에 저장합니다.
function update_use_cnt($it_id)
{
    global $g5;
    $row = sql_fetch(" select count(*) as cnt from {$g5['g5_shop_item_use_table']} where it_id = '{$it_id}' /*and is_confirm = 1*/ ");
    return sql_query(" update {$g5['g5_shop_item_table']} set it_use_cnt = '{$row['cnt']}' where it_id = '{$it_id}' ");
}


// 사용후기의 선호도(별) 평균을 상품테이블에 저장합니다.
function update_use_avg($it_id)
{
    global $g5;
    $row = sql_fetch(" select count(*) as cnt, sum(is_score) as total from {$g5['g5_shop_item_use_table']} where it_id = '{$it_id}' /*and is_confirm = 1*/ ");
    $average = ($row['total'] && $row['cnt']) ? $row['total'] / $row['cnt'] : 0;
    return sql_query(" update {$g5['g5_shop_item_table']} set it_use_avg = '$average' where it_id = '{$it_id}' ");
}

//오늘본상품 데이터
function get_view_today_items($is_cache=false)
{
    global $g5;
    
    $tv_idx = get_session("ss_tv_idx");

    if( !$tv_idx ){
        return array();
    }

    static $cache = array();

    if( $is_cache && !empty($cache) ){
        return $cache;
    }

    for ($i=1;$i<=$tv_idx;$i++){

        $tv_it_idx = $tv_idx - ($i - 1);
        $tv_it_id = get_session("ss_tv[$tv_it_idx]");

        $rowx = sql_fetch(" select * from {$g5['g5_shop_item_table']} where it_id = '$tv_it_id' ");
        if(!$rowx['it_id'])
            continue;
        
        $key = $rowx['it_id'];

        $cache[$key] = $rowx;
    }

    return $cache;
}

//오늘본상품 갯수 출력
function get_view_today_items_count()
{
    $tv_datas = get_view_today_items(true);

    return count($tv_datas);
}

//장바구니 간소 데이터 가져오기
function get_boxcart_datas($is_cache=false)
{
    global $g5;
    
    $cart_id = get_session("ss_cart_id");

    if( !$cart_id ){
        return array();
    }

    static $cache = array();

    if( $is_cache && !empty($cache) ){
        return $cache;
    }

    $sql  = " select * from {$g5['g5_shop_cart_table']} ";
    $sql .= " where od_id = '".$cart_id."' group by it_id ";
    $result = sql_query($sql);
    for ($i=0; $row=sql_fetch_array($result); $i++)
    {
        $key = $row['it_id'];
        $cache[$key] = $row;
    }

    return $cache;
}

//장바구니 간소 데이터 갯수 출력
function get_boxcart_datas_count()
{
    $cart_datas = get_boxcart_datas(true);

    return count($cart_datas);
}

//위시리스트 데이터 가져오기
function get_wishlist_datas($mb_id, $is_cache=false)
{
    global $g5, $member;

    if( !$mb_id ){
        $mb_id = $member['mb_id'];

        if( !$mb_id ) return array();
    }

    static $cache = array();

    if( $is_cache && isset($cache[$mb_id]) ){
        return $cache[$mb_id];
    }

    $sql  = " select a.it_id, b.it_name from {$g5['g5_shop_wish_table']} a, {$g5['g5_shop_item_table']} b ";
    $sql .= " where a.mb_id = '".$mb_id."' and a.it_id  = b.it_id order by a.wi_id desc ";
    $result = sql_query($sql);
    for ($i=0; $row=sql_fetch_array($result); $i++)
    {
        $key = $row['it_id'];
        $cache[$mb_id][$key] = $row;
    }

    return $cache[$mb_id];
}

//위시리스트 데이터 갯수 출력
function get_wishlist_datas_count($mb_id='')
{
    global $member;

    if( !$mb_id ){
        $mb_id = $member['mb_id'];

        if( !$mb_id ) return 0;
    }

    $wishlist_datas = get_wishlist_datas($mb_id, true);

    return count($wishlist_datas);
}

//각 상품에 대한 위시리스트 담은 갯수 출력
function get_wishlist_count_by_item($it_id='')
{
    global $g5;

    $sql = "select count(a.it_id) as num from {$g5['g5_shop_wish_table']} a, {$g5['g5_shop_item_table']} b where a.it_id  = b.it_id and a.it_id='{$it_id}'";
    $row = sql_fetch($sql);

    return (int) $row['num'];
}

// 회원별 위시리스트 상품 유무 확인
function get_wishlist_my_item_check($it_id='')
{
    global $member;
    global $g5;

    if (!$member['mb_id']) {
        return 0;
    }

    $sql  = " select count(a.it_id) as num from {$g5['g5_shop_wish_table']} a, {$g5['g5_shop_item_table']} b ";
    $sql .= " where a.mb_id = '".$member['mb_id']."' and a.it_id  = b.it_id and a.it_id='{$it_id}'";
    $row = sql_fetch($sql);

    return (int) $row['num'];
}

//주문데이터 또는 개인결제 주문데이터 가져오기
function get_shop_order_data($od_id, $type='item')
{
    global $g5;
    
    $od_id = clean_xss_tags($od_id);

    if( $type == 'personal' ){
        $row = sql_fetch("select * from {$g5['g5_shop_personalpay_table']} where pp_id = $od_id ", false);
    } else {
        $row = sql_fetch("select * from {$g5['g5_shop_order_table']} where od_id = $od_id ", false);
    }

    return $row;
}

function exists_inicis_shop_order($oid, $pp=array(), $od_time='', $od_ip='')
{

    $od_ip = $od_ip ? $od_ip : $_SERVER['REMOTE_ADDR'];

    //개인결제
    if( $pp ) {
        $hash_data = md5($pp['pp_id'].$pp['pp_price'].$pp['pp_time']);
        if( $hash_data == get_session('ss_personalpay_hash') ){
            // 개인결제번호제거
            set_session('ss_personalpay_id', '');
            set_session('ss_personalpay_hash', '');

            $uid = md5($pp['pp_id'].$pp['pp_time'].$od_ip);
            set_session('ss_personalpay_uid', $uid);
            
            goto_url(G5_SHOP_URL.'/personalpayresult.php?pp_id='.$pp['pp_id'].'&amp;uid='.$uid.'&amp;ini_noti=1');
        } else {
            goto_url(G5_SHOP_URL.'/personalpayresult.php?pp_id='.$pp['pp_id'].'&amp;ini_noti=1');
        }
    } else {    //그렇지 않으면
        if (!$od_time){
            $od_time = G5_TIME_YMDHIS;
        }

        if( $oid == get_session('ss_order_id') ){
            // orderview 에서 사용하기 위해 session에 넣고
            $uid = md5($oid.$od_time.$od_ip);
            set_session('ss_orderview_uid', $uid);
            goto_url(G5_SHOP_URL.'/orderinquiryview.php?od_id='.$oid.'&amp;uid='.$uid.'&amp;ini_noti=1');
        } else {
            goto_url(G5_SHOP_URL.'/orderinquiryview.php?od_id='.$oid.'&amp;ini_noti=1');
        }
    }
    return '';
}

//------------------------------------------------------------------------------
// 주문포인트를 적립한다.
// 설정일이 지난 포인트 부여되지 않은 배송완료된 장바구니 자료에 포인트 부여
// 설정일이 0 이면 주문서 완료 설정 시점에서 포인트를 바로 부여합니다.
//------------------------------------------------------------------------------
function save_order_point($ct_status="완료")
{
    global $g5, $default;

    $beforedays = date("Y-m-d H:i:s", ( time() - (86400 * (int)$default['de_point_days']) ) ); // 86400초는 하루
    $sql = " select * from {$g5['g5_shop_cart_table']} where ct_status = '$ct_status' and ct_point_use = '0' and ct_time <= '$beforedays' ";
    $result = sql_query($sql);
    for ($i=0; $row=sql_fetch_array($result); $i++) {
        // 회원 ID 를 얻는다.
        $od_row = sql_fetch("select od_id, mb_id from {$g5['g5_shop_order_table']} where od_id = '{$row['od_id']}' ");
        if ($od_row['mb_id'] && $row['ct_point'] > 0) { // 회원이면서 포인트가 0보다 크다면
            $po_point = $row['ct_point'] * $row['ct_qty'];
            $po_content = "주문번호 {$od_row['od_id']} ({$row['ct_id']}) 배송완료";
            insert_point($od_row['mb_id'], $po_point, $po_content, "@delivery", $od_row['mb_id'], "{$od_row['od_id']},{$row['ct_id']}");
        }
        sql_query("update {$g5['g5_shop_cart_table']} set ct_point_use = '1' where ct_id = '{$row['ct_id']}' ");
    }
}


// 배송업체 리스트 얻기
function get_delivery_company($company)
{
    $option = '<option value="">없음</option>'.PHP_EOL;
    $option .= '<option value="자체배송" '.get_selected($company, '자체배송').'>자체배송</option>'.PHP_EOL;

    $dlcomp = explode(")", str_replace("(", "", G5_DELIVERY_COMPANY));
    for ($i=0; $i<count($dlcomp); $i++) {
        if (trim($dlcomp[$i])=="") continue;
        list($value, $url, $tel) = explode("^", $dlcomp[$i]);
        $option .= '<option value="'.$value.'" '.get_selected($company, $value).'>'.$value.'</option>'.PHP_EOL;
    }

    return $option;
}

// 사용후기 썸네일 생성
function get_itemuselist_thumbnail($it_id, $contents, $thumb_width, $thumb_height, $is_create=false, $is_crop=true, $crop_mode='center', $is_sharpen=true, $um_value='80/0.5/3')
{
    global $g5, $config;
    $img = $filename = $alt = "";

    if($contents) {
        $matches = get_editor_image($contents, false);

        for($i=0; $i<count($matches[1]); $i++)
        {
            // 이미지 path 구함
            $p = parse_url($matches[1][$i]);
            if(strpos($p['path'], '/'.G5_DATA_DIR.'/') != 0)
                $data_path = preg_replace('/^\/.*\/'.G5_DATA_DIR.'/', '/'.G5_DATA_DIR, $p['path']);
            else
                $data_path = $p['path'];

            $srcfile = G5_PATH.$data_path;

            if(preg_match("/\.({$config['cf_image_extension']})$/i", $srcfile) && is_file($srcfile)) {
                $size = @getimagesize($srcfile);
                if(empty($size))
                    continue;

                $filename = basename($srcfile);
                $filepath = dirname($srcfile);

                preg_match("/alt=[\"\']?([^\"\']*)[\"\']?/", $matches[0][$i], $malt);
                $alt = get_text($malt[1]);

                break;
            }
        }

        if($filename) {
            $thumb = thumbnail($filename, $filepath, $filepath, $thumb_width, $thumb_height, $is_create, $is_crop, $crop_mode, $is_sharpen, $um_value);

            if($thumb) {
                $src = G5_URL.str_replace($filename, $thumb, $data_path);
                $img = '<img src="'.$src.'" width="'.$thumb_width.'" height="'.$thumb_height.'" alt="'.$alt.'">';
            }
        }
    }

    if(!$img)
        $img = get_it_image($it_id, $thumb_width, $thumb_height);

    return $img;
}

// 장바구니 상품삭제 -> cron/cart_item_clean.php 참고
function cart_item_clean()
{
    global $g5, $default;

    // 장바구니 보관일
    $keep_term = $default['de_cart_keep_term'];
    if(!$keep_term)
        $keep_term = 15; // 기본값 15일

    // ct_select_time이 기준시간 이상 경과된 경우 변경
    if(defined('G5_CART_STOCK_LIMIT'))
        $cart_stock_limit = G5_CART_STOCK_LIMIT;
    else
        $cart_stock_limit = 3;

    $stocktime = 0;
    if($cart_stock_limit > 0) {
        if($cart_stock_limit > $keep_term * 24)
            $cart_stock_limit = $keep_term * 24;

        $stocktime = G5_SERVER_TIME - (3600 * $cart_stock_limit);
        $sql = " update {$g5['g5_shop_cart_table']}
                    set ct_select = '0'
                    where ct_select = '1'
                      and ct_status = '쇼핑'
                      and UNIX_TIMESTAMP(ct_select_time) < '$stocktime' ";
        sql_query($sql);
    }

    // 설정 시간이상 경과된 상품 삭제
    $statustime = G5_SERVER_TIME - (86400 * $keep_term);

    $sql = " delete from {$g5['g5_shop_cart_table']}
                where ct_status = '쇼핑'
                  and UNIX_TIMESTAMP(ct_time) < '$statustime' ";
    sql_query($sql);
}


// 임시주문 데이터로 주문 필드 생성
function make_order_field($data, $exclude)
{
    $field = '';

    foreach($data as $key=>$value) {
        if(!empty($exclude) && in_array($key, $exclude))
            continue;

        if(is_array($value)) {
            foreach($value as $k=>$v) {
                $field .= '<input type="hidden" name="'.$key.'['.$k.']" value="'.$v.'">'.PHP_EOL;
            }
        } else {
            $field .= '<input type="hidden" name="'.$key.'" value="'.$value.'">'.PHP_EOL;
        }
    }

    return $field;
}

//이니시스의 삼성페이 또는 L.pay 결제가 활성화 되어 있는지 체크합니다.
function is_inicis_simple_pay(){
    global $default;

    if ( $default['de_samsung_pay_use'] || $default['de_inicis_lpay_use'] ){
        return true;
    }

    return false;
}

//이니시스의 삼성페이 또는 L.pay 결제인지 확인합니다.
function is_inicis_order_pay($type){

    if( in_array($type, array('삼성페이', 'lpay') ) ){
        return true;
    }

    return false;
}

//결제방식 이름을 체크하여 치환 대상인 문자열은 따로 리턴합니다.
function check_pay_name_replace($payname){

    if( $payname === 'lpay' ){
        return 'L.pay';
    }

    return $payname;
}

// 다운로드한 쿠폰인지
function is_coupon_downloaded($mb_id, $cz_id)
{
    global $g5;

    if(!$mb_id)
        return false;

    $sql = " select count(*) as cnt from {$g5['g5_shop_coupon_table']} where mb_id = '$mb_id' and cz_id = '$cz_id' ";
    $row = sql_fetch($sql);

    return ($row['cnt'] > 0);
}

function get_comp_cost($type, $comp_code, $complist) {
	$value = 0;
	
	for ($i=0; $i <= count($complist); $i++) {
		if($comp_code == $complist[$i]['comp_code']) {
			if($type == 'price') {
				$value = $complist[$i]['price'];
			} else {
				$value = $complist[$i]['cnt'];
			}
		}
	}
	return $value ;
}

function dalcong_eventYN($mb_id,$it_id){

	global $g5;

	$value = "Y";

	if( $it_id == "S00603" || $it_id == "S00604" || $it_id == "S01756" ){

		//달콩 100원세트주문할때는 로그인한상태에서만 주문할수있다.
		if( !empty($mb_id) ){

			$sql = " 
			select a.od_id , a.it_name , datediff( now() , c.od_time ) as order_diff
			  from tbl_shop_order_detail a, tbl_shop_order_receiver b, tbl_shop_order c
			  where a.od_id = b.od_id
			  and a.od_num = b.od_num
			  and a.od_id = c.od_id
			  and a.it_id = '$it_id'
			  and c.mb_id = '$mb_id'
			  and b.od_status != '취소' /*and b.od_pay_yn = 'Y'*/ order by a.od_id desc limit 1
			";
			$row = sql_fetch($sql);

			if( !empty($row['od_id']) ){
				//한번 구매하면 3일 이후에 다시 구매가능하다.
				if( $row['order_diff'] >= 7 ){
					$value = "Y";
				}else if( $row['order_diff'] >= 1 && $row['order_diff'] < 7 ){
					$value = "N";
				}else if( $row['order_diff'] == 0 ){
					$value = "N";		
				}
			}

		}else{
		
			$value = "N";
					
		}

	}

	return $value ;
}



//달콩 소비자 회원일때 쿠폰 발행해주기 
//EventDalcongCoupon($member['mb_name'] , $member['mb_id'] , $member['mb_type']); 
function EventDalcongCoupon( $mb_name , $mb_id , $mb_type ){

	global $g5, $config;

	//일반소비자회원만 발급
	if( $mb_type == "0" ){

		//달콩 행복 SET_소비자 상품 구매후 입금여부확인
		$sql = " 
			 SELECT count(a.od_id) as pay_cnt
			 from tbl_shop_order a, tbl_shop_order_detail b, tbl_shop_order_receiver c, tbl_member d
			 where a.od_id = b.od_id 
			   and b.od_id = c.od_id 
			   and b.od_num = c.od_num
			   and b.comp_code = d.comp_code
			   AND b.it_id = 'S00604' AND a.mb_id = '$mb_id' /*AND c.od_pay_yn = 'Y'*/;	
		";
		$row = sql_fetch($sql);

		$sql = " select count(*) as coupont_cnt from {$g5['g5_shop_coupon_table']} where mb_id = '$mb_id' and cp_target = 'DALCONG' ; ";
		$row2 = sql_fetch($sql);

		// 입금확인된 S00604 제품의 구매수가 1개 이상인데 , 쿠폰을 받은적이 없다면 실행
		if( $row['pay_cnt'] >= 1 && $row2['coupont_cnt'] == 0 ){

				// 달콩이벤트 쿠폰발생 7일내 사용가능
				if($mb_id) {
					$j = 0;
					$create_coupon = false;

					do {
						$cp_id = get_coupon_id();

						$sql3 = " select count(*) as cnt from {$g5['g5_shop_coupon_table']} where cp_id = '$cp_id' ";
						$row3 = sql_fetch($sql3);

						if(!$row3['cnt']) {
							$create_coupon = true;
							break;
						} else {
							if($j > 20)
								break;
						}
					} while(1);

					if($create_coupon) {
						$cp_subject = '달콩이벤트 쿠폰';
						$cp_method = 0;
						$cp_target = 'DALCONG';
						$cp_start = G5_TIME_YMD;
						$cp_end = date("Y-m-d", (G5_SERVER_TIME + (86400 * 7)));
						$cp_type = 1;
						$cp_price = 10;//$default['de_member_reg_coupon_price'];
						$cp_trunc = 10;
						$cp_minimum = 0;//$default['de_member_reg_coupon_minimum'];
						$cp_maximum = 0;

						$sql = " INSERT INTO {$g5['g5_shop_coupon_table']}
									( cp_id, cp_subject, cp_method, cp_target, mb_id, cp_start, cp_end, cp_type, cp_price, cp_trunc, cp_minimum, cp_maximum, cp_datetime )
								VALUES
									( '$cp_id', '$cp_subject', '$cp_method', '$cp_target', '$mb_id', '$cp_start', '$cp_end', '$cp_type', '$cp_price', '$cp_trunc', '$cp_minimum', '$cp_maximum', '".G5_TIME_YMDHIS."' ) ";

						$res = sql_query($sql, false);

					}
				}
		}

	}
}


function CardInfoText(){

	$ReturnTxt = "
	<div id='card_info' style='margin-top:5px;text-align:center;display:none;'>
		<div class='tbl_head03 tbl_wrap'>
		<table>
		<thead>
		<tr>
			<th scope='col'>카드사</th>
			<th scope='col'>할부적용 금액</th>
			<th scope='col'>할부개월</th>
			<th scope='col'>신청방법</th>
		</tr>
		<tr>
			<td>하나카드</td>
			<td>5만원 이상</td>
			<td>2~3개월</td>
			<td rowspan='8'>별도 신청 없이 적용</td>
		</tr>
		<tr>
			<td>현대카드</td>
			<td>5만원 이상</td>
			<td>2~5개월</td>
		</tr>
		<tr>
			<td>국민카드</td>
			<td>5만원 이상</td>
			<td>2~5개월</td>
		</tr>
		<tr>
			<td>신한카드</td>
			<td>5만원 이상</td>
			<td>2~5개월</td>
		</tr>
		<tr>
			<td>삼성카드</td>
			<td>5만원 이상</td>
			<td>2~5개월</td>
		</tr>
		<tr>
			<td>비씨카드</td>
			<td>5만원 이상</td>
			<td>2~6개월</td>
		</tr>
		<tr>
			<td>NH카드</td>
			<td>5만원 이상</td>
			<td>2~6개월</td>
		</tr>
		<tr>
			<td>씨티카드</td>
			<td>5만원 이상</td>
			<td>2~6개월</td>
		</tr>
		</thead>
		<tbody>
		</tbody>
		</table>
		</div>
	</div>	
	";

	return $ReturnTxt;
}


function CardInfoText2(){

	$ReturnTxt = "
	<div id='card_info2' style='margin-top:5px;text-align:center;display:none;'>
		<div class='tbl_head03 tbl_wrap'>
		<table border='1'>
		<thead>
		  <tr>
			<th scope='col'>카드사</th>
			<th scope='col'>할부적용 금액</th>
			<th scope='col'>고객부담</th>
			<th scope='col'>면제</th>
			<th scope='col'>신청방법</th>
		  </tr>
		  <tr>
			<td rowspan='5'>삼성카드</td>
		  </tr>
		  <tr>
			<td>10개월</td>
			<td>1~3회차</td>
			<td>4~10회차</td>
			<td rowspan='8'>별도 신청 없이 적용</td>
		  </tr>
		  <tr>
			<td>12개월</td>
			<td>1~4회차</td>
			<td>5~12회차</td>
		  </tr>
		  <tr>
			<td>18개월</td>
			<td>1~6회차</td>
			<td>7~18회차</td>
		  </tr>
		  <tr>
			<td>24개월</td>
			<td>1~8회차</td>
			<td>9~24회차</td>
		  </tr>
		  <tr>
			<td rowspan='3'>현대카드</td>
			<td>6개월</td>
			<td>1~2회차</td>
			<td>3~6회차</td>
		  </tr>
		  <tr>
			<td>10개월</td>
			<td>1~3회차</td>
			<td>4~10회차</td>
		  </tr>
		  <tr>
			<td>12개월</td>
			<td>1~4회차</td>
			<td>5~12회차</td>
		  </tr>
		  <tr>
			<td>국민카드</td>
			<td>10개월</td>
			<td>1~2회차</td>
			<td>3~10회차</td>
		  </tr>
		  <tr>
			<td rowspan='6'>비씨카드</td>
			<td>7개월</td>
			<td rowspan='4'>1~2회차</td>
			<td>3~7회차</td>
			<td rowspan='6'>사전 신청 고객에 한함<br>신청방법 : ARS<br>비씨카드 : 1899-5772<br>*하나BC카드 제외</td>
		  </tr>
		  <tr>
			<td>8개월</td>
			<td>3~8회차</td>
		  </tr>
		  <tr>
			<td>9개월</td>
			<td>3~9회차</td>
		  </tr>
		  <tr>
			<td>10개월</td>
			<td>3~10회차</td>
		  </tr>
		  <tr>
			<td>11개월</td>
			<td rowspan='2'>1~3회차</td>
			<td>4~11회차</td>
		  </tr>
		  <tr>
			<td>12개월</td>
			<td>4~12회차</td>
		  </tr>
		  <tr>
			<td rowspan='6'>NH카드</td>
			<td>7개월</td>
			<td rowspan='4'>1~2회차</td>
			<td>3~7회차</td>
			<td rowspan='6'>사전 신청 고객에 한함<br>신청방법 : ARS<br>NH카드 : 1644-2009 </td>
		  </tr>
		  <tr>
			<td>8개월</td>
			<td>3~8회차</td>
		  </tr>
		  <tr>
			<td>9개월</td>
			<td>3~9회차</td>
		  </tr>
		  <tr>
			<td>10개월</td>
			<td>3~10회차</td>
		  </tr>
		  <tr>
			<td>11개월</td>
			<td rowspan='2'>1~3회차</td>
			<td>4~11회차</td>
		  </tr>
		  <tr>
			<td>12개월</td>
			<td>4~12회차</td>
		  </tr>
		</thead>
		</table>
		</div>
	</div>	
	";

	return $ReturnTxt;
}

function CardInfoText3(){

    $muija = sql_fetch_row(main_banner('무이자_item', 1));
    $img = G5_DATA_URL."/banner/".$muija['bn_id']."?ver=".G5_YM_VER;

	$ReturnTxt = "
	<div id='card_info3' style='margin-top:5px;text-align:center;'>
		<div class='tbl_head03 tbl_wrap'>
			<img src='".$img."' style='max-width:100%;height:auto;'/>
		</div>
	</div>	
	";

	return $ReturnTxt;

}

function CutStringInfo($GetText,$GetNum){
    $strlen     = mb_strlen($GetText, 'utf-8');
    $firstStr     = mb_substr($GetText, 0, $GetNum, 'utf-8');
    $lastStr     = "*";//mb_substr($GetText, -1, 1, 'utf-8');
    return $strlen == 2 ? $firstStr . str_repeat('*', mb_strlen($GetText, 'utf-8') - 1) : $firstStr . str_repeat("*", $strlen - 2) . $lastStr;
}



 /*  사용법  :  send_attachment('클라이언트에게 보여줄 파일명', '서버측 파일 경로', [캐싱할 기간], [속도 제한]);
 *
 *             아래의 예는 foo.jpg라는 파일을 사진.jpg라는 이름으로 다운로드한다.
 *             send_attachment('사진.jpg', '/srv/www/files/uploads/foo.jpg');
 *
 *             아래의 예는 bar.mp3라는 파일을 24시간 동안 캐싱하고 다운로드 속도를 300KB/s로 제한한다.
 *             send_attachment('bar.mp3', '/srv/www/files/uploads/bar.mp3', 60 * 60 * 24, 300);
 *
 *  반환값  :  전송에 성공한 경우 true, 실패한 경우 false를 반환한다. 
 */
function send_attachment($filename, $server_filename, $expires = 0, $speed_limit = 0) {
    
    // 서버측 파일명을 확인한다.
    
    if (!file_exists($server_filename) || !is_readable($server_filename)) {
        return false;
    }
    if (($filesize = filesize($server_filename)) == 0) {
        return false;
    }
    if (($fp = @fopen($server_filename, 'rb')) === false) {
        return false;
    }
    
    // 파일명에 사용할 수 없는 문자를 모두 제거하거나 안전한 문자로 치환한다.
    
    $illegal = array('\\', '/', '<', '>', '{', '}', ':', ';', '|', '"', '~', '`', '@', '#', '$', '%', '^', '&', '*', '?');
    $replace = array('', '', '(', ')', '(', ')', '_', ',', '_', '', '_', '\'', '_', '_', '_', '_', '_', '_', '', '');
    $filename = str_replace($illegal, $replace, $filename);
    $filename = preg_replace('/([\\x00-\\x1f\\x7f\\xff]+)/', '', $filename);
    
    // 유니코드가 허용하는 다양한 공백 문자들을 모두 일반 공백 문자(0x20)로 치환한다.
    
    $filename = trim(preg_replace('/[\\pZ\\pC]+/u', ' ', $filename));
    
    // 위에서 치환하다가 앞뒤에 점이 남거나 대체 문자가 중복된 경우를 정리한다.
    
    $filename = trim($filename, ' .-_');
    $filename = preg_replace('/__+/', '_', $filename);
    if ($filename === '') {
        return false;
    }
    
    // 브라우저의 User-Agent 값을 받아온다.
    
    $ua = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : '';
    $old_ie = (bool)preg_match('#MSIE [3-8]\.#', $ua);
    
    // 파일명에 숫자와 영문 등만 포함된 경우 브라우저와 무관하게 그냥 헤더에 넣는다.
    
    if (preg_match('/^[a-zA-Z0-9_.-]+$/', $filename)) {
        $header = 'filename="' . $filename . '"';
    }
    
    // IE 9 미만 또는 Firefox 5 미만의 경우.
    
    elseif ($old_ie || preg_match('#Firefox/(\d+)\.#', $ua, $matches) && $matches[1] < 5) {
        $header = 'filename="' . rawurlencode($filename) . '"';
    }
    
    // Chrome 11 미만의 경우.
    
    elseif (preg_match('#Chrome/(\d+)\.#', $ua, $matches) && $matches[1] < 11) {
        $header = 'filename=' . $filename;
    }
    
    // Safari 6 미만의 경우.
    
    elseif (preg_match('#Safari/(\d+)\.#', $ua, $matches) && $matches[1] < 6) {
        $header = 'filename=' . $filename;
    }
    
    // 안드로이드 브라우저의 경우. (버전에 따라 여전히 한글은 깨질 수 있다. IE보다 못한 녀석!)
    
    elseif (preg_match('#Android #', $ua, $matches)) {
        $header = 'filename="' . $filename . '"';
    }
    
    // 그 밖의 브라우저들은 RFC2231/5987 표준을 준수하는 것으로 가정한다.
    // 단, 만약에 대비하여 Firefox 구 버전 형태의 filename 정보를 한 번 더 넣어준다.
    
    else {
        $header = "filename*=UTF-8''" . rawurlencode($filename) . '; filename="' . rawurlencode($filename) . '"';
    }
    
    // 캐싱이 금지된 경우...
    
    if (!$expires) {
        
        // 익스플로러 8 이하 버전은 SSL 사용시 no-cache 및 pragma 헤더를 알아듣지 못한다.
        // 그냥 알아듣지 못할 뿐 아니라 완전 황당하게 오작동하는 경우도 있으므로
        // 캐싱 금지를 원할 경우 아래와 같은 헤더를 사용해야 한다.
        
        if ($old_ie) {
            header('Cache-Control: private, must-revalidate, post-check=0, pre-check=0');
            header('Expires: Sat, 01 Jan 2000 00:00:00 GMT');
        }
        
        // 그 밖의 브라우저들은 말을 잘 듣는 착한 어린이!
        
        else {
            header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
            header('Expires: Sat, 01 Jan 2000 00:00:00 GMT');
        }
    }
    
    // 캐싱이 허용된 경우...
    
    else {
        header('Cache-Control: max-age=' . (int)$expires);
        header('Expires: ' . gmdate('D, d M Y H:i:s', time() + (int)$expires) . ' GMT');
    }
    
    // 이어받기를 요청한 경우 여기서 처리해 준다.
    
    if (isset($_SERVER['HTTP_RANGE']) && preg_match('/^bytes=(\d+)-/', $_SERVER['HTTP_RANGE'], $matches)) {
        $range_start = $matches[1];
        if ($range_start < 0 || $range_start > $filesize) {
            header('HTTP/1.1 416 Requested Range Not Satisfiable');
            return false;
        }
        header('HTTP/1.1 206 Partial Content');
        header('Content-Range: bytes ' . $range_start . '-' . ($filesize - 1) . '/' . $filesize);
        header('Content-Length: ' . ($filesize - $range_start));
    } else {
        $range_start = 0;
        header('Content-Length: ' . $filesize);
    }
    
    // 나머지 모든 헤더를 전송한다.
    
    header('Accept-Ranges: bytes');
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; ' . $header);
    
    // 출력 버퍼를 비운다.
    // 파일 앞뒤에 불필요한 내용이 붙는 것을 막고, 메모리 사용량을 줄이는 효과가 있다.
    
    while (ob_get_level()) {
        ob_end_clean();
    }
    
    // 파일을 64KB마다 끊어서 전송하고 출력 버퍼를 비운다.
    // readfile() 함수 사용시 메모리 누수가 발생하는 경우가 가끔 있다.
    
    $block_size = 16 * 1024;
    $speed_sleep = $speed_limit > 0 ? round(($block_size / $speed_limit / 1024) * 1000000) : 0;
    
    $buffer = '';
    if ($range_start > 0) {
        fseek($fp, $range_start);
        $alignment = (ceil($range_start / $block_size) * $block_size) - $range_start;
        if ($alignment > 0) {
            $buffer = fread($fp, $alignment);
            echo $buffer; unset($buffer); flush();
        }
    }
    while (!feof($fp)) {
        $buffer = fread($fp, $block_size);
        echo $buffer; unset($buffer); flush();
        usleep($speed_sleep);
    }
    
    fclose($fp);
    
    // 전송에 성공했으면 true를 반환한다.
    
    return true;
}


/* 공급사 팝업 띄워주는 함수 */
function ViewPopup($comp_code){

	global $member;

	//공급사일때만...
	if( $member["mb_type"] == "7" ){

		$sql = " SELECT count(wr_id) as view_cnt FROM (
				 SELECT distinct a.wr_id , a.wr_subject , a.wr_datetime , a.wr_1 , a.wr_2 , a.wr_3 , a.wr_4 , b.wr_comp_code , 
						  IFNULL( b.wr_confirm_yn , 'N' ) AS wr_confirm_yn , b.wr_del_yn , b.wr_confirm_time
				 FROM 
				 tbl_write_vender a left join tbl_write_vender_choice b ON a.wr_id = b.wr_id AND b.wr_comp_code = '".$comp_code."'
									  left join tbl_member c ON b.wr_comp_code = c.comp_code 
			 ) AS X where (X.wr_4 = 'A' and X.wr_confirm_yn != 'Y') 
					OR ( X.wr_4 = 'C' and X.wr_comp_code = '".$comp_code."' AND X.wr_confirm_yn != 'Y' )  ";
		
		//echo "<pre>".$sql."</pre>";

		$row = sql_fetch($sql);

		if( $row["view_cnt"] > 0 ){
		
?>
		<style>
			#popupLayer {display:none;border:5px solid #cccccc;margin:0;padding:5px;background-color:#ffffff;z-index:5;}
			#popupLayer .b-close {position:absolute;top:10px;right:25px;color:#f37a20;font-weight:bold;cursor:hand;}
			#popupLayer .popupContent {margin:0;padding:0;text-align:center;border:0;}
			#popupLayer .popupContent iframe {width:900px;height:500px;border:0;padding:0px;margin:0;z-index:10;}
		</style>

		<div id="popupLayer">
			<div class="popupContent"></div>
			<div class="b-close">
				<button type="button" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button"><span class="ui-button-text">&nbsp;X&nbsp;</span></button>
			</div>
		</div>

		<script src="/js/jquery.bpopup.min.js"></script>

		<script>
		
		$('#popupLayer').bPopup({

			modalClose: false,
            content:'iframe', //'ajax', 'iframe' or 'image'
			iframeAttr:'frameborder=auto',
			iframeAttr:'frameborder=0',
            contentContainer:'.popupContent',
            loadUrl:'/adm/viewpopup.php'

		});

        $('#opt1').click(function(){
            alert("Aaaaa");
        });

		
		</script>
<?

		}

	}

}





//소비자회원일때 주문할일쿠폰 1만원 짜리 180일 설정
//EventManCoupon($member['mb_id'] , $member['mb_hp'] , $member['mb_type']); 
function EventManCoupon( $mb_id , $mb_hp , $mb_type ){

	global $g5, $config;

	//일반소비자회원만 발급
	if( $mb_type == "0" ){

		//event_phone 테이블에 있으면서 회원테이블에 가입된회원
		$sql = " 
			 SELECT COUNT(mb_id) AS mb_cnt FROM tbl_member AS A INNER JOIN event_phone AS B ON A.mb_hp = B.mb_hp WHERE A.mb_type = '0'
			 and A.mb_hp = '{$mb_hp}' AND B.mb_hp = '{$mb_hp}' ;
		";
		$row = sql_fetch($sql);

		$sql = " select count(*) as coupont_cnt from {$g5['g5_shop_coupon_table']} where mb_id = '$mb_id' and cp_subject = '우리 약사님 칭찬해~! 프로모션' ; ";
		$row2 = sql_fetch($sql);

		// 회원이있으면서 , 쿠폰을 받은적이 없다면 실행
		if( $row['mb_cnt'] >= 1 && $row2['coupont_cnt'] == 0 ){

				// 달콩이벤트 쿠폰발생 7일내 사용가능
				if($mb_id) {
					$j = 0;
					$create_coupon = false;

					do {
						$cp_id = get_coupon_id();

						$sql3 = " select count(*) as cnt from {$g5['g5_shop_coupon_table']} where cp_id = '$cp_id' ";
						$row3 = sql_fetch($sql3);

						if(!$row3['cnt']) {
							$create_coupon = true;
							break;
						} else {
							if($j > 20)
								break;
						}
					} while(1);

					if($create_coupon) {
						$cp_subject = '우리 약사님 칭찬해~! 프로모션';
						$cp_method = 2;
						$cp_target = '';
						$cp_start = G5_TIME_YMD;
						$cp_end = date("Y-m-d", (G5_SERVER_TIME + (86400 * 180)));
						$cp_type = 0;
						$cp_price = 10000;//$default['de_member_reg_coupon_price'];
						$cp_trunc = 1;
						$cp_minimum = 2000;//$default['de_member_reg_coupon_minimum'];
						$cp_maximum = 0;

						$sql = " INSERT INTO {$g5['g5_shop_coupon_table']}
									( cp_id, cp_subject, cp_method, cp_target, mb_id, cp_start, cp_end, cp_type, cp_price, cp_trunc, cp_minimum, cp_maximum, cp_datetime )
								VALUES
									( '$cp_id', '$cp_subject', '$cp_method', '$cp_target', '$mb_id', '$cp_start', '$cp_end', '$cp_type', '$cp_price', '$cp_trunc', '$cp_minimum', '$cp_maximum', '".G5_TIME_YMDHIS."' ) ";

						$res = sql_query($sql, false);

					}
				}
		}

	}
}

function get_mshop_category($ca_id, $len, $mb_type='1')
{
    global $g5, $member;

    if($mb_type=='1' || $mb_type=='9') {

        $sql = " select ca_id, ca_name from {$g5['g5_shop_category_table']}
							where ca_use = '1' and ca_web_view=1";
        if($ca_id)
            $sql .= " and ca_id like '$ca_id%' ";
        if ($member['mb_referee'] != 4)
            $sql .= " and ca_staff_use=0 ";

        $sql .= " and length(ca_id) = '$len' order by ca_order, ca_id ";
    } else {
        $sql = " select ca_id, ca_name from {$g5['g5_shop_category_table']}
							where ca_use = '1' and ca_app_view=1";
        if($ca_id)
            $sql .= " and ca_id like '$ca_id%' ";
        if ($member['mb_referee'] != 4)
            $sql .= " and ca_staff_use=0 ";

        $sql .= " and length(ca_id) = '$len' order by ca_order, ca_id ";
    }

    return $sql;
}

function holiday_chk ($date)
{
    global $holiday_arr, $yoil_str;

    $count = 0;
    $term = 3;
    $flag = false;
    $result = false;

    $chk_date = $date;
    //debug2('처음 입력된 날짜 : '.$chk_date);

    while ($flag == false) {
        $chk_date = date('Y-m-d', strtotime($chk_date. '+1 days'));
        $chk_yoil = $yoil_str[date('w', strtotime($chk_date))];

        if ($chk_yoil == '토' || $chk_yoil == '일') {
            //debug2('주말이 포함되어있습니다.');
        } else {
            if (in_array($chk_date, $holiday_arr)) {
                //debug2('공휴일이라 불가 : '.$chk_date);
            } else {
                $count++;
            }
        }

        if ($count == $term) {
            $flag = true;
            $result = date('Y-m-d 18:00:00', strtotime($chk_date));
        }
    }

    return $result;
}


//==============================================================================
// 쇼핑몰 라이브러리 모음 끝
//==============================================================================
?>
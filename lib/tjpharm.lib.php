<?php

/**
 * @param $data
 * @param int $type (post type)
 */
function tsg_price_update ($data, $type=0) {

    $method_type = "";
    switch ($type) {
        case 0:
            $method_type = "price";

            $curl_data = $data;
            break;
        case 1:
            $method_type = "prices";

            foreach ($data as $k => $row) {

                $curl_data[$k] = array(
                    "ItmNo" => $row['ITMNO'],
                    "MallGB" => $row['MALL_GB'],
                    "MallCode" => $row['MALL_CODE'],
                    "CompNm1" => $row['COMP_NM1'],
                    "CompNm2" => $row['COMP_NM2'],
                    "TjCost" => intval(str_int_replace($row['TJ_COST'])),
                    "CompCost1" => intval(str_int_replace($row['COMP_COST1'])),
                    "CompCost2" => intval(str_int_replace($row['COMP_COST2'])),
                    "TjSaleCost" => intval(str_int_replace($row['TJ_SALE_COST'])),
                    "CompSaleCost1" => intval(str_int_replace($row['COMP_SALE_COST1'])),
                    "CompSaleCost2" => intval(str_int_replace($row['COMP_SALE_COST2'])),
                    "TjStock" => intval(str_int_replace($row['TJ_STOCK'])),
                    "CompStock1" => intval(str_int_replace($row['COMP_STOCK1'])),
                    "CompStock2" => intval(str_int_replace($row['COMP_STOCK2']))
                );

            }
            break;
        default:
            break;
    }

    if ($method_type != '') {

        $host = "http://203.251.44.107/ONKService";
        $method = "/api/crawler/".$method_type;
        $url = $host.$method;

        $json_data = json_encode($curl_data, JSON_UNESCAPED_UNICODE);

        $header = array(
            'Content-Type: application/json',
            'Content-Length: '.strlen($json_data)
        );

        $ch = curl_init(); //curl 초기화
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header); // Header 선언해야 json 전송 가능
        curl_setopt($ch, CURLOPT_URL, $url); //URL 지정하기
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); //요청 결과를 문자열로 반환
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10); //connection timeout 10초
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); //원격 서버의 인증서가 유효한지 검사 안함

        curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data); //POST data
        curl_setopt($ch, CURLOPT_POST, true); //true시 post 전송

        $response = curl_exec($ch);
        curl_close($ch);

        $result = json_decode($response, true);
        return $result;
    } else {
        return false;
    }

}

// DataLake & TJ API 모두 업데이트
function tsg_update ($mall_code, $mall_type) {

    include_once ('../python-admin/common/common.lib.php');

    ### Data Lake DB Connect ###
    $host = 'onk-database-datalake-instance-1.cyy8jt4en2pr.ap-northeast-2.rds.amazonaws.com';
    $db_user = 'dbadmin';
    $db_pass = 'CfTW4EaffFKQbAmSaqq7';
    $db_name = 'tjerp';

    $datalake_db = mysqli_connect($host, $db_user, $db_pass, $db_name);
    if (mysqli_connect_errno()) {
        die('Connect Error: '.mysqli_connect_error());
    }
    sql_set_charset('utf8', $datalake_db);

    # 변수에 한글이 포함될 경우 현재 서버의 locale을 확인하여 변경해주도록 한다.
    $locale = shell_exec('locale -a | grep ko_KR');
    if (strpos($locale, 'ko_KR') !== false) {
        putenv("LANG=ko_KR.UTF-8");
        setlocale(LC_ALL, 'ko_KR.utf8');
    }

    $tsg_table = 'TSG_MALL_INFO';

    $sql = "select * from {$tsg_table} where MALL_CODE='$mall_code' AND MALL_GB='$mall_type' limit 1";
    $info = sql_fetch($sql, false, $datalake_db);


    if (!empty($mall_code)) {
        if ($mall_type == 'D') {
            $data = json_decode(exec('python3 py/theshop_search.py '.$mall_code), true);
        } else if ($mall_type == 'H') {
            $data = json_decode(exec('python3 py/hmpmall_search.py '.$mall_code), true);
        } else if ($mall_type == 'P') {
            $data = json_decode(exec('python3 py/pharmsnet_search.py '.$mall_code), true);
        }

        $result = array();
        if ($data) {

            $num = 0;
            foreach ($data as $k => $v) {

                // 문자열 공백 제거
                $price = preg_replace('/\xc2\xa0/', '', $v['ggs_price'][0]);
                $sale_price = preg_replace('/\xc2\xa0/', '', $v['ggs_sale_price'][0]);
                $jg = preg_replace('/\xc2\xa0/', '', $v['ggs_jg'][0]);

                // 숫자 형태로 처리하기 위해 체크
                $price = (!is_numeric($price)) ? str_replace(',', '', $price) : $price;
                $sale_price = (!is_numeric($sale_price)) ? str_replace(',', '', $sale_price) : $sale_price;
                $jg = (!is_numeric($jg)) ? str_replace(',', '', $jg) : $jg;

                $price = str_replace('-', '', $price);
                $sale_price = str_replace('-', '', $sale_price);
                $jg = str_replace('-', '', $jg);

                $result[$num]['product_key'] = $v['product_key'][0];
                $result[$num]['product_name'] = $v['product_name'][0];
                $result[$num]['ggs_name'] = $v['ggs_name'][0];
                $result[$num]['ggs_price'] = (empty($price)) ? '-' : number_format($price);
                $result[$num]['ggs_sale_price'] = empty($sale_price) ? '-' : number_format($sale_price);
                $result[$num]['ggs_jg'] = empty($jg) ? '-' : number_format($jg);
                $result[$num]['ggs_update'] = date('Y-m-d H:i:s');

                /* 기존 데이터 비교 */
                switch ($num) {
                    case 0:
                        $result[$num]['ggs_price_compare'] = empty($price) ? '-' : number_sign($price-$info['TJ_COST']);
                        $result[$num]['ggs_sale_price_compare'] = empty($sale_price) ? '-' : number_sign($sale_price-$info['TJ_SALE_COST']);
                        $result[$num]['ggs_jg_compare'] = empty($jg) ? '-' : number_sign($jg-$info['TJ_STOCK']);

                        $up['TJ_COST'] = empty($price) ? 0 : $price;
                        $up['TJ_SALE_COST'] = empty($sale_price) ? 0 : $sale_price;
                        $up['TJ_STOCK'] = empty($jg) ? 0 : $jg;
                        break;
                    case 1:
                        $up['COMP_COST1'] = $result[$num]['ggs_price_compare'] = empty($price) ? '-' : number_sign($price-$info['COMP_COST1']);
                        $up['COMP_SALE_COST1'] = $result[$num]['ggs_sale_price_compare'] = empty($sale_price) ? '-' : number_sign($sale_price-$info['COMP_SALE_COST1']);
                        $up['COMP_STOCK1'] = $result[$num]['ggs_jg_compare'] = empty($jg) ? '-' : number_sign($jg-$info['COMP_STOCK1']);

                        $up['COMP_COST1'] = empty($price) ? 0 : $price;
                        $up['COMP_SALE_COST1'] = empty($sale_price) ? 0 : $sale_price;
                        $up['COMP_STOCK1'] = empty($jg) ? 0 : $jg;
                        break;
                    case 2:
                        $up['COMP_COST2'] = $result[$num]['ggs_price_compare'] = empty($price) ? '-' : number_sign($price-$info['COMP_COST2']);
                        $up['COMP_SALE_COST2'] = $result[$num]['ggs_sale_price_compare'] = empty($sale_price) ? '-' : number_sign($sale_price-$info['COMP_SALE_COST2']);
                        $up['COMP_STOCK2'] = $result[$num]['ggs_jg_compare'] = empty($jg) ? '-' : number_sign($jg-$info['COMP_STOCK2']);

                        $up['COMP_COST2'] = empty($price) ? 0 : $price;
                        $up['COMP_SALE_COST2'] = empty($sale_price) ? 0 : $sale_price;
                        $up['COMP_STOCK2'] = empty($jg) ? 0 : $jg;
                        break;
                }

                $num++;
            }

            /*************** 트랜잭션 관련 ****************/
            $error_cnt = 0;
            mysqli_autocommit($datalake_db, false);
            /*************** 트랜잭션 관련 ****************/

            $sql = "UPDATE {$tsg_table}
                SET
                TJ_COST = ".$up['TJ_COST'].",
                TJ_SALE_COST = ".$up['TJ_SALE_COST'].",
                TJ_STOCK = ".$up['TJ_STOCK'].",
                COMP_COST1 = ".$up['COMP_COST1'].",
                COMP_SALE_COST1 = ".$up['COMP_SALE_COST1'].",
                COMP_STOCK1 = ".$up['COMP_STOCK1'].",
                COMP_COST2 = ".$up['COMP_COST2'].",
                COMP_SALE_COST2 = ".$up['COMP_SALE_COST2'].",
                COMP_STOCK2 = ".$up['COMP_STOCK2'].",
                SYSIDATE = '".date('Y-m-d')."',
                SYSITIME = '".date('H:i:s')."'
                WHERE SEQ='$info[SEQ]'
        ";
            sql_query($sql, false, $datalake_db);

            /*************** 트랜잭션 관련 ****************/
            if (mysqli_errno($datalake_db)) {
                $error_cnt += 1;
            }
            /*************** 트랜잭션 관련 ****************/

            /*************** 트랜잭션 관련 ****************/
            if ($error_cnt > 0) {
                mysqli_rollback($datalake_db);
                mysqli_close($datalake_db);
            } else {
                mysqli_commit($datalake_db);
            }
            /*************** 트랜잭션 관련 ****************/

        }

        // 티제이팜 API 연동
        if ($error_cnt == 0) { // 에러가 없을 경우 실행

            $curl_data = array(
                "ItmNo" => $info['ITMNO'],
                "MallGB" => $mall_type,
                "MallCode" => $mall_code,
                "CompNm1" => $info['COMP_NM1'],
                "CompNm2" => $info['COMP_NM2'],
                "TjCost" => $up['TJ_COST'],
                "CompCost1" => $up['COMP_COST1'],
                "CompCost2" => $up['COMP_COST2'],
                "TjSaleCost" => $up['TJ_SALE_COST'],
                "CompSaleCost1" => $up['COMP_SALE_COST1'],
                "CompSaleCost2" => $up['COMP_SALE_COST2'],
                "TjStock" => $up['TJ_STOCK'],
                "CompStock1" => $up['COMP_STOCK1'],
                "CompStock2" => $up['COMP_STOCK2']
            );

            // return -> StatusCode : 200, Message : OK
            tsg_price_update($curl_data);

        }

    }
}

function tsgLogWrite($msg, $target='') {
    $path = PYTHON_ADM_PATH."/py/logs/";
    $file = "tsgLog_".date("Ymd").".log";
    $msg_head = "[".date("Y-m-d")." ".date("H:i:s")."][P=".getmypid()."]";
    $mall = " [".$target."]";

    // 디렉토리 확인
    if (!is_dir($path)) {
        mkdir($path, 0755);
    }
    // 파일이 없는 경우 먼저 생성
    if (!file_exists($path.$file)) {
        fopen($path.$file, "w");
    }
    // 파일 유무 확인
    if (!($fp = fopen($path.$file, "a+")))
        return 0;
    fwrite($fp, $msg_head);

    ob_start();
    print_r($msg.$mall);
    $ob_msg = ob_get_contents();
    ob_clean();

    if (fwrite($fp, " ".$ob_msg."\n") === FALSE) {
        fclose($fp);
        return 0;
    }
    fclose($fp);
    return 1;
}

function str_int_replace($value) {

    if ($value) {
        $result = str_replace(',', '', trim($value));
        return $result;
    } else {
        return $value;
    }

}
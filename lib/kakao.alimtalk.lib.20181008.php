<?php
if (!defined('_GNUBOARD_')) exit;

/**
* 카카오 알림톡 발송을 관장하는 메인 클래스이다.
*
*/
class AlimTalk {

	// 슈어엠 API url
	var $service_url = 'https://api.surem.com/alimtalk/v2/json';
	// 슈어엠 제공 아이디
	var $usercode = 'onkorderstn';
	// 슈어엡 제공 회사코드
    var $deptcode = '4P-WX1-DY';
	// 발신프로필키
    var $yellowid_key;

	// 문자 재전송시 발신번호
	var $from = '15445462';
	
	var $button_set;
	
	var $messages;
	var $messagesArr;

    function __construct($uuid = 'orderstation')
    {
        switch ($uuid) {
        case 'hahaha':  // 하하하 발신프로필키
            $this->yellowid_key = '1383cdb086bf6c1f320fa63c64ebbfc5b57ce96c';
            break;
        case 'orderstation':    // 오더스테이션 발신프로필키
            //$this->yellowid_key = '1383cdb086bf6c1f320fa63c64ebbfc5b57ce96c';
            $this->yellowid_key = '2e8414a6a58431935aa1e44d91f3edc7e1e8e93c';
            break;
        default:
            $this->yellowid_key = '2e8414a6a58431935aa1e44d91f3edc7e1e8e93c';
            break;
        }
    }

	/**
	 * 발송할 메시지를 생성한다.
	 * 
	 * $message_id    : 메시지 고유키 값. 미입력시 단보전송은 1이며, 동보전송은 1씩 증가
     * $to            : 국가번호 포함 전화번호. 예시) 821012345678
     * $text          : 요청할 템플릿 내용.
     * $code : 요청할 템플릿 코드명. 예시) onk_in_001
	 * $reserved_time : 예약 발송시 예약시간(12자리). 미입력시 즉시 전송. 예시) 201912310910 
	 * $re_send       : 알림톡 전송 실패시 문자 재전송 여부. 미입력시 N. (Y: $text 문구로 전송, R: $re_text 문구로 전송)
	 *
	 */
	function set_message($code, $to, $text, $re_send='Y', $reserved_time=null)
	{
		$this->message = array(
			//'message_id' => '1',
			'to' => $to,
			'text' => $text,
			'from' => $this->from,
			'template_code' => $code
		);

		if ($re_send == 'Y') {
			$re_text = $text;
		} else if ($re_send == 'R') {
			$re_text = $text;	// 데체 메시지를 미리 정의해서 조회 필요.
		}

		if ($re_send != 'N') {
			$this->message = array_merge($this->message, array(
				're_send' => $re_send,
				're_text' => $re_text			
			));
		}
		
		if ($reserved_time) {
			$this->message = array_merge($this->message, array(
				'reserved_time' => $reserved_time
			));
		}
		
		//$this->set_button($code);
		//$this->message = array_merge($this->message, array('buttons' => $this->button_set));

		$this->messagesArr = array($this->message);		
	}
	
	/**
	 * 카카오 알림톡을 발송한다.
	 *
	 */
	public function send()
	{
		$curl_post_data = array(
			'usercode' => $this->usercode,
			'deptcode' => $this->deptcode,
			'yellowid_key' => $this->yellowid_key,
			'messages' => $this->messagesArr
		);
	 
		//$json_data = json_encode($curl_post_data, JSON_UNESCAPED_UNICODE);
		$json_data = json_encode($curl_post_data);
		
		$header = array(
			'Content-Type: application/json', 
			'Content-Length: ' . strlen($json_data)
		);
		
		$ch = curl_init($this->service_url);
		
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
		curl_setopt($ch, CURLOPT_URL, $this->service_url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($ch, CURLOPT_POST, 1);   
	 
		$response = curl_exec($ch);
	 
		if($response === FALSE){
			die(curl_error($ch));
		}
	 
		$responseData = json_decode($response, TRUE);

		//var_dump($responseData);
		//echo $responseData['code'];
		//echo $responseData['message'];
		//echo $responseData['results'];
		//echo $responseData['additional_information'];

		if ($responseData['code'] == '200')	// Success
			return true;
		else
			return false;
	}

    /**
     * 국가번호 포함 전화번호 형식으로 설정
     *
     */
    function set_phone_number($to)
    {
        $to = str_replace('-', '', $to);
        return  '82' . substr($to, 1, strlen($to));
    }

    /**
     * 버튼 정보(최대 5개 등록 가능)를 생성한다.
     * TODO: 사용할 경우에는 버튼 정보 추가 정의 필요
     */
    function set_button($code)
    {
        $buttons = array();

        switch ($code) {
            case 'onk_in_001':
                $buttons = array(
                    array(
                        'button_name' => '오더스테이션 이동',
                        'button_url' => 'http://orderstation.kr'
                    ),
                    array(
                        'button_name' => '오더스테이션 회원가입',
                        'button_url' => 'http://orderstation.kr/bbs/register.php'
                    )
                );
                break;
            case 'orderstation_member_01':
                $buttons = array(
                    array(
                        'button_name' => '오더스테이션 이동',
                        'button_url' => 'http://orderstation.kr'
                    )
                );
                break;
            default:
                $buttons = array(
                    array(
                        'button_name' => '오더스테이션 이동',
                        'button_url' => 'http://orderstation.kr'
                    )
                );
                break;
        }

        $this->button_set = $buttons;
    }

    /**
     * 템플릿 코드에 해당하는 템플릿 내용 조회
     *
     */
    function get_template($code)
    {
        switch ($code) {
            // 오더스테이션 템플릿 -------------------------------------------------------------------------------------

            // 회원 관리
            case 'orderstation_member_00':	// 신규회원가입축하 (수신: 고객)
                $template = '[오더스테이션] ]#{이름}고객님 회원가입을 축하드립니다. 오더스테이션만의 건강한 먹거리와, 다양한 건강정보를 약사님과의 1:1 건강 상담과 함께 누려보세요! .신규 가입 포인트 #{금액}원 쿠폰을 지급해 드리오니 마이페이지를 확인하세요~!';
                break;
            case 'orderstation_member_02':	// 회원가입승인 (수신: 고객)
                $template = '[오더스테이션]#{이름}님의 가입정보가 사용승인처리되었습니다. 감사합니다.';
                break;
            case 'orderstation_member_03':	// 가입_휴대폰인증번호 (수신: 고객)
                $template = '[오더스테이션] 휴대폰 인증번호 #{인증번호} 위 번호를 인증창에 입력하세요.';
                break;

            // 1:1 건강상담
            case 'orderstation_qna_01': // 1:1건강상담접수 => 1:1 건강상담 (수신:	고객)
                $template = '[오더스테이션]#{이름} 고객님, 1:1건강상담 문의가 접수되었습니다.감사합니다.';
                break;
            case 'orderstation_qna_02': // 1:1건강상담답변요청 => 1:1 건강상담 (수신:	약국)
                $template = '[오더스테이션]]#{이름} 고객님의 1:1건강상담 문의가 접수되었습니다.답변해주시면 감사하겠습니다.';
                break;
            case 'orderstation_qna_03':   // 1:1건강상담답변완료 => 1:1 건강상담 (수신: 고객)
                $template = '[오더스테이션]]#{이름} 고객님,1:1건강상담 문의에 대한 답변이 등록되었습니다.감사합니다.';
                break;

            // 고객문의 내역
            case 'orderstation_qna_04': // 고객문의접수 => 고객문의내역 (수신: 고객)
                $template = '[오더스테이션] #{이름} 고객님, 고객 문의가 접수되었습니다.감사합니다.';
                break;
            case 'orderstation_qna_05': // 고객문의답변요청 => 고객문의내역 (수신: OS관리자)
                $template = '[오더스테이션] #{이름} 고객님의 문의가 접수되었습니다.답변해주시면 감사하겠습니다.';
                break;
            case 'orderstation_qna_06': // 고객문의답변완료 => 고객문의내역 (수신: 고객)
				$template = '[오더스테이션] #{이름} 고객님, 고객문의에 대한 답변이 등록되었습니다.감사합니다.'; 
                break;

            // 상품후기 등록
            case 'orderstation_qna_07': // 상품후기등록쿠폰 => 상품후기등록 (수신: 고객)
                $template = '[오더스테이션] #{이름}고객님, 후기 등록에 감사드립니다. OS앱회원 후기등록 쿠폰 #{금액}원이 지급되었습니다. 후기등록 쿠폰은 지급 후 7일 동안(사용기한; 7일) 사용할 수 있습니다. 저희 오더스테이션의 많은 이용 부탁드립니다. 감사합니다.';
                break;

            // 건강상담 등록
            case 'orderstation_qna_08': // 건강상담등록쿠폰 => 건강상담등록 (수신: 고객)
                $template = '[오더스테이션]#{이름}고객님, 건강상담 등록에 감사드립니다. OS앱회원 건강상담 쿠폰 #{금액}원이 지급되었습니다. 건강상담 쿠폰은 지급 후 7일 동안(사용기한; 7일) 사용할 수 있습니다. 저희 오더스테이션의 많은 이용 부탁드립니다. 감사합니다.';
                break;

            // 주문 관리
            case 'orderstation_order_01':	// 주문완료-번호 => 주문완료시 (수신: 고객/약국/공급사)
                $template = '[오더스테이션] #{이름} 고객님의 주문입니다. 주문번호는 #{주문번호}입니다.';
                break;
            //case 'orderstation_order_02':	// 주문완료-정기주문번호
            //    $template = '[오더스테이션] #{이름} 고객님의 ○회 정기주문입니다. 주문번호는 #{주문번호}입니다.';
            //    break;
            case 'orderstation_order_03':	// 발송완료-정기주문 => 정기주문배송 (수신: 고객)
                $template = '[오더스테이션] #{이름} 고객님, #{상품명}(#{회차}회차)이 #{배송지}로 발송되었습니다.';
                break;
            case 'orderstation_order_04':	// 발송완료-일반주문 => 일반배송 (수신: 고객)
                $template = '[오더스테이션] #{이름} 고객님, #{상품명}이 #{배송지}로 발송되었습니다.';
                break;
            case 'orderstation_order_05':	// 주문취소건 => 주문취소시 (수신: 고객)
                $template = '[오더스테이션] #{이름} 고객님, 주문번호 #{주문번호}가 정상적으로 취소되었습니다.';
                break;
            case 'orderstation_order_06':	// 첫구매포인트 => 첫구매 (수신: 고객)
                $template = '[오더스테이션] #{이름}고객님,  첫 구매에 감사드립니다. #{금액}쿠폰을 지급해 드리오니 \'마이페이지에서 확인하세요~! 감사합니다.';
                break;
            case 'orderstation_order_07':	// 정기주문주문완료메시지 => 정기주문완료시 (수신: 공급사)
                $template = '[오더스테이션] #{이름} 고객님의 #{회차}회 정기주문입니다. 주문번호는 #{주문번호}입니다.';
                break;

            case 'orderstation_pay_01':	// 미입금취소 => 미입금 (수신: 고객)
                $template = '[오더스테이션]주문번호 #{주문번호}가 미입금으로인해 취소되었습니다.';
                break;
            case 'orderstation_pay_02':	// 카드비정상_1 => 빌링결제실패 (수신: 고객)
                $template = '카드비정상으로 오더스테이션에서 결제불가합니다.';
                break;
            case 'orderstation_pay_03':	// 카드비정상_2 => 빌링결제실패 (수신: 01090480403 (한영수부장))
                $template = '카드비정상으로 오더스테이션에서 결제불가한 건이있습니다';
                break;

            // 하하하 템플릿 -------------------------------------------------------------------------------------------

            case 'crm_member_001':	// 약국 방문 고객에게 환영 메시지
                $template = '안녕하세요 #{이름} 고객님, #{약국명}의 건강 관리 대상 고객으로 등록되신 것을 환영합니다. 궁금하신 점이 있으면 약사님과 상의하시고 오늘도 건강한 하루 되세요!';
                break;
            case 'orderstation_member_01':	// 회원가입축하메시지
                $template = '안녕하세요 #{이름} 고객님, 오더스테이션 가입을 환영합니다! 오더스테이션만의 건강한 먹거리와, 다양한 건강정보를 약사님과의 1:1 상담과 함께 누려보세요!';
                break;
        }

        return $template;
    }

}

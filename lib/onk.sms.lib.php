<?php
if (!defined('_GNUBOARD_')) exit;

/**
* 문자메시지 발송을 관장하는 메인 클래스이다.
*
*/
class OnkSms {

	// 슈어엠 API url
	var $sms_url = 'https://api.surem.com/sms/v1/json';
	var $lms_url = 'https://api.surem.com/lms/v1/json';
	
	// 슈어엠 제공 아이디
	var $usercode = 'onkorderstn';
	// 슈어엡 제공 회사코드
    var $deptcode = '4P-WX1-DY';
	
	// 문자에 표시될 발신번호
	var $from = '15445462';
	
	var $messages = array();
	var $messagesArr = array();

	// 기본은 sms (S), lms (L)
	var $message_type = 'S';

	var $api_url = '';
	
	var $reserved_time = '';
	
    function __construct()
    {
		
    }

	function msg_length($msg) 
	{
		// 한글은 2byte로 계산
		$pattern = '/[\x{1100}-\x{11FF}\x{3130}-\x{318F}\x{AC00}-\x{D7AF}]+/u';
		preg_match_all($pattern, $msg, $match);
		$msg_mb_string = implode('', $match[0]);

		// 특수문자는 1byte로 계산
		$pattern = '/[^\x{1100}-\x{11FF}\x{3130}-\x{318F}\x{AC00}-\x{D7AF}0-9a-zA-Z]+/u';
		preg_match_all($pattern, $msg, $match);
		$msg_special_string = implode('', $match[0]);

		$real_length = strlen($msg) - strlen($msg_mb_string) - strlen($msg_special_string) + mb_strlen($msg_mb_string, 'utf-8') * 2 + mb_strlen($msg_special_string, 'utf-8');
		
		return $real_length;
	}
	
	function get_msg_type($msg) 
	{
		$msg_length = $this->msg_length($msg);
		if ($msg_length > 80) $this->message_type = 'L';
		
		return $this->message_type;
	}
	
	/**
	 * 발송할 sms메시지를 생성한다.
	 * 
	 * $message_id    : 메시지 고유키 값. 미입력시 단보전송은 1이며, 동보전송은 1씩 증가
     * $to            : 전화번호. 예시) 01012345678
     * $text          : 전송할 메시지
	 * $reserved_time : 예약 발송시 예약시간(12자리). 미입력시 즉시 전송. 예시) 201912310910 
	 *
	 */
	function set_sms_message($to, $text, $reserved_time=null)
	{
		$this->message_type = 'S';
		$this->api_url = $this->sms_url;
		$this->text = $text;
		if ($reserved_time) $this->reserved_time;
		
		$this->message = array(
			'to' => $to
		);
	}
	
	/**
	 * 발송할 lms메시지를 생성한다.
	 * 
	 * $message_id    : 메시지 고유키 값. 미입력시 단보전송은 1이며, 동보전송은 1씩 증가
     * $to            : 전화번호. 예시) 01012345678
	 * $subject		  : 전송할 메시지 제목
     * $text          : 전송할 메시지
	 * $reserved_time : 예약 발송시 예약시간(12자리). 미입력시 즉시 전송. 예시) 201912310910 
	 *
	 */
	function set_lms_message($to, $subject, $text, $reserved_time=null)
	{
		$this->message_type = 'L';
		$this->api_url = $this->lms_url;
		$this->subject = $subject;
		$this->text = $text;
		if ($reserved_time) $this->reserved_time;
		
		$this->message = array(
			'to' => $to,
		);

		if ($reserved_time) $this->reserved_time;	
	}	
	
	/**
	 * 메시지를 발송한다.
	 *
	 */
	public function send()
	{
		$this->messagesArr = array($this->message);
		
		$curl_post_data = array(
			'usercode' => $this->usercode,
			'deptcode' => $this->deptcode,
			'messages' => $this->messagesArr,
			'text'     => $this->text,
			'from'     => $this->from
		);
		
		if ($this->message_type == 'L') {
			$curl_post_data = array_merge($curl_post_data, array(
				'subject' => $this->subject
			));		
		}
		
		if ($this->reserved_time) {
			$curl_post_data = array_merge($curl_post_data, array(
				'reserved_time' => $this->reserved_time
			));
		}
	 
		//$json_data = json_encode($curl_post_data, JSON_UNESCAPED_UNICODE);
		$json_data = json_encode($curl_post_data);
		echo $json_data . '<br>';
		//die();
		
		$header = array(
			'Content-Type: application/json', 
			'Content-Length: ' . strlen($json_data)
		);
		
		$ch = curl_init($this->api_url);
		
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
		curl_setopt($ch, CURLOPT_URL, $this->api_url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($ch, CURLOPT_POST, 1);   
	 
		$response = curl_exec($ch);
	 
		if($response === FALSE){
			die(curl_error($ch));
		}
	 
		$responseData = json_decode($response, TRUE);

		//var_dump($responseData);
		//echo $responseData['code'];
		//echo $responseData['message'];
		//echo $responseData['results'];
		//echo $responseData['additional_information'];

		if ($responseData['code'] == '200')	// Success
			return true;
		else
			return false;
	}

}

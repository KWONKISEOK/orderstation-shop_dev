<?php
/**
 * config.php 개발환경 분기
 *
 */

$match = "orderstation.co.kr";
$match_test = "orderstation-dev-web-elb";
$hostname = $_SERVER["HTTP_HOST"];

if (strpos($hostname, $match_test) !== false) {
    // test
    include_once($g5_path['path'].'/config.test.php');
    define('G5_CONFIG_LIVE', FALSE);
} else if (
    (strpos($hostname, "127.0.0.1") !== false) ||
    (strpos($hostname, "localhost") !== false) ||
    (strpos($hostname, "10.100.11.72") !== false) ||
    (strpos($hostname, "10.100.11.61") !== false) ||
    (strpos($hostname, "10.0.2.2") !== false)) {
    // local
    include_once($g5_path['path'].'/config.local.php');
    define('G5_CONFIG_LIVE', FALSE);
} else if (strpos($hostname, $match) !== false) {
    // main
    include_once($g5_path['path'].'/config.main.php');
    define('G5_CONFIG_LIVE', TRUE);
} else {
    // main
    include_once($g5_path['path'].'/config.main.php');
    define('G5_CONFIG_LIVE', TRUE);
}
<?php
include_once('./_common.php');

if (G5_IS_MOBILE) {
    include_once(G5_THEME_MSHOP_PATH.'/index.php');
    return;
}

define("_INDEX_", TRUE);



//이즈브레 회원이 로그인시 , 추천약국정보가 ONK00 면 팝업 띄워주기.
if( $member['mb_recommend'] == "ONK00" ){
?>

	<style>
		.test_layer_wrap {position: absolute;top: 50%; left:50%; transform:translate(-50%, -50%);  z-index: 98;}
        .test_layer_wrap img {width: 370px; height: 466px;}
        #pop_layer {position:fixed;margin-top: -10px;}
	</style>
	<div class="test_layer_wrap" id="pop_layer">
        <div class="dimBg"></div>
        <img src="<?php echo G5_THEME_URL; ?>/img/main_pop_recom.png" alt="" usemap="#pop_layer_map">
        <map name="pop_layer_map" id="pop_layer_map">
            <area shape="rect" coords="333,14,355,36"  onclick="javascript:document.getElementById('pop_layer').style.display='none';" alt="" onfocus=this.blur()>
            <area shape="rect" coords="54,364,318,415" href="<?php echo G5_SHOP_URL.'/mypage.php'; ?>" alt="" onfocus=this.blur()>
        </map>
	</div>

<?

} //약국지정팝업종료 

include_once(G5_THEME_SHOP_PATH.'/shop.head.php');

?>

    <div class="bxslider-wrap grid5">
        <!-- 좌측 고정 카테고리 영역 -->


        <?php include(G5_SHOP_PATH.'/aside.php'); ?>

        <!-- 슬라이드 영역-->
        <div class="bx-wrapper">
            <div class="bx-viewport">
                <ul id="bxslider1">
                    <?php
                    $main_list = main_banner('메인');
                    for ($i = 0; $row = sql_fetch_array($main_list); $i++) { ?>
                        <li>
                            <a href="<?php echo G5_SHOP_URL.'/bannerhit.php?bn_id='.$row['bn_id']; ?>" target="_self">
                                <?php if (file_exists(G5_DATA_PATH.'/banner/'.$row['bn_id'])) { ?>
                                <img src="<?php echo G5_DATA_URL.'/banner/'.$row['bn_id']; ?>" alt="">
                                <?php } else { ?>
                                <img src="<?php echo dummyimage(1920, 480); ?>" alt="" >
                                <?php } ?>
                            </a>
                        </li>
                    <?php } ?>
                </ul>
            </div>
            <div class="bx-controls bx-has-controls-direction">
                <div class="bx-controls-direction">
                    <a class="bx-prev" href="">Prev</a>
                    <a class="bx-next" href="">Next</a>
                </div>
            </div>
        </div>
        <ul class="list" id="bxslider1-pager">
            <?php $main_list = main_banner('메인');
            for ($i = 0; $row = sql_fetch_array($main_list); $i++) { ?>
                <li class="page0<?php echo $i + 1; ?>">
                    <a class="" data-slide-index="<?php echo $i ?>" href="#"></a>
                </li>
            <?php } ?>

        </ul>
        <!-- //슬라이드 영역-->
    </div>
    <!-- } 메인이미지 끝 -->

    <!-- 추천상품 -->
    <div class="prod-slide">
        <div class="site-wrap">
            <div class="xans-element- xans-product xans-product-listmain-1 xans-product-listmain xans-product-1 ec-base-product bx2" style="width:1200px !important; height:364px !important;">

                <div class="title">
                    <h2>
                        <span style="display: ;">베스트상품</span>
                    </h2>
                </div>
                <ul class="prdList" id="bxslider2">
                    <?php
                    $sql = " select * from {$g5['g5_shop_item_table']} where it_main1='1' and it_use = '1' and $it_view_where order by it_order asc limit 20";
                    $result = sql_query($sql);

                    for ($i = 0; $row = sql_fetch_array($result); $i++) {
                        ?>

                        <li class="xans-record-">
                            <?php if ($row['it_cust_price'] > 0) { ?>
                                <span class="discount_rate " data-prod-custom="<?php echo $row['it_cust_price']; ?>" data-prod-price="<?php echo $row['it_price']; ?>"></span>
                                <?php if ($row['it_sc_type'] == "1") { ?>
                                    <span class="free_trans">무료배송</span>
                                    <?php if ($row['it_multisend'] == "1") { //세일,무배,다중배송 모두해당?>
                                        <span class="multi_trans_2">다중배송</span>
                                    <? } ?>
                                <?php } else { ?>
                                    <?php if ($row['it_multisend'] == "1") { ?>
                                        <span class="multi_trans_1">다중배송</span>
                                    <?php }
                                } ?>
                            <?php } else { ?>
                                <?php if ($row['it_sc_type'] == "1") { ?>
                                    <span class="free_trans_new">무료배송</span>
                                    <?php if ($row['it_multisend'] == "1") { ?>
                                        <span class="multi_trans_1">다중배송</span>
                                    <?php } ?>
                                <?php } else { ?>
                                    <?php if ($row['it_multisend'] == "1") { ?>
                                        <span class="multi_trans_0">다중배송</span>
                                    <?php }
                                }
                            } ?>
                            <div class="thumbnail">
                                <a href="<?php echo G5_SHOP_URL.'/item.php?it_id='.$row['it_id']; ?>">
                                    <img src="<?php echo G5_DATA_URL.'/item/'.$row['it_img1']; ?>" width="218" height="218" alt="<?php echo $row['it_name']; ?>" class="img_medium"/>
                                    <img src="<?php echo G5_DATA_URL.'/item/'.$row['it_img1']; ?>" width="218" height="218" alt="<?php echo $row['it_name']; ?>" class="img_small"/>
                                </a>
                            </div>
                            <div class="description">
                                <div class="block">
                                    <div class="block_in">
                                        <strong class="name">
                                            <a href="<?php echo G5_SHOP_URL.'/item.php?it_id='.$row['it_id']; ?>">
                                                <span style="font-size:12px;color:#555555;"><?php echo $row['it_name']; ?></span>
                                            </a>
                                        </strong>
                                        <ul>
                                            <?php if ($is_drug) { ?>
                                                <li class=" xans-record-">
                                                    <span class='sct_price_title'>공급가:</span>
                                                    <?php if ($row['it_drug_ori_price']) { ?>
                                                        <span style="text-decoration: line-through;"><?php echo number_format($row['it_drug_ori_price']); ?></span>
                                                    <?php } ?>
                                                    <span style="font-size:13px;color:#000000;font-weight:bold;"><?php echo number_format($row['it_drug_price']); ?>원</span>
                                                </li>
                                                <li class=" xans-record-">
                                                    <span class='sct_price_title'>소비자가:</span>
                                                    <?php if ($row['it_cust_price'] > 0) { ?>
                                                        <span style="text-decoration: line-through;"><?php echo number_format($row['it_cust_price']); ?></span>
                                                    <?php } ?>
                                                    <span style="font-size:13px;color:#000000;font-weight:bold;"><?php echo number_format($row['it_price']); ?>원</span>
                                                </li>
                                                <li class=" xans-record-">
                                                    <span class="sct_incen">포인트 <?php echo number_format($row['it_incen']); ?></span>
                                                </li>
                                            <?php } else { ?>
                                                <li class=" xans-record-">
                                                    <?php if ($row['it_cust_price'] > 0) { ?>
                                                        <span style="text-decoration: line-through;"><?php echo number_format($row['it_cust_price']); ?></span>
                                                    <?php } ?>
                                                    <span style="font-size:13px;color:#000000;font-weight:bold;"><?php echo number_format($row['it_price']); ?>원</span>
                                                </li>
                                            <?php } ?>

                                            <li class=" xans-record-">
                                                <span class="w200 desc_content"><?php echo $row['it_desc']; ?></span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </li>
                    <?php } ?>
                </ul>
            </div>
        </div>
    </div>
    <!-- //추천상품 -->
    <!-- 배너 -->
    <div class="dp-block2">
        <ul>
            <li>
                <a href="/shop/event.php?ev_id=1614648126">
                    <img src="<?php echo G5_THEME_URL; ?>/img/main_banner_hotdeal.jpg?date=20210302" alt=""/></a>
            </li>
        </ul>
    </div>
    <!-- //배너 -->
    <!-- 배너 -->
    <div class="dp-block3">
        <div class="site-wrap">
            <div class="block block_left">
                <a href="/shop/event.php?ev_id=1614245500">
                    <img src="<?php echo G5_THEME_URL; ?>/img/brand_pcimg_1.jpg?ver=<?php echo G5_IMG_VER;?>" alt=""/></a>
            </div>
            <div class="block block_center">
                <ul class="top">
                    <li>
                        <a href="/shop/event.php?ev_id=1614246149">
                            <img src="<?php echo G5_THEME_URL; ?>/img/brand_pcimg_2.jpg?ver=<?php echo G5_IMG_VER;?>" alt=""/></a>
                    </li>
                    <!-- <li><a href="#none"><img src="<?php echo G5_THEME_URL; ?>/img/main_banner_3_3.jpg" alt=""/></a></li> -->
                </ul>
                <ul class="btm">
                    <li>
                        <a href="/shop/event.php?ev_id=1614245883">
                            <img src="<?php echo G5_THEME_URL; ?>/img/brand_pcimg_3.jpg?ver=<?php echo G5_IMG_VER;?>" alt=""/></a>
                    </li>
                    <!-- <li><a href="#none"><img src="<?php echo G5_THEME_URL; ?>/img/main_banner_3_5.jpg" alt=""/></a></li> -->
                </ul>
            </div>
            <div class="block block_right bx3">
                <ul id="bxslider3">
                    <li class="item">
                        <a href="/shop/event.php?ev_id=1614246243">
                            <img src="<?php echo G5_THEME_URL; ?>/img/brand_pcimg_4.jpg?ver=<?php echo G5_IMG_VER;?>"/></a>
                    </li>
                    <li class="item">
                        <a href="/shop/event.php?ev_id=1614246331">
                            <img src="<?php echo G5_THEME_URL; ?>/img/brand_pcimg_5.jpg?ver=<?php echo G5_IMG_VER;?>"/></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- //배너 -->
    <!-- 상품진열 최저가 -->
    <div class="prod-slide" style="margin-top:-80px;border:0px solid #000;">
        <!-- 상품진열 최저가 -->
        <div class="xans-element- xans-product xans-product-listmain-5 xans-product-listmain xans-product-5 product-b ec-base-product bx2" style="background:#eaeced;">
            <div class="site-wrap " style="height:730px;border:0px solid #000;">
                <div class="title">
                    <h2>
                        <span style="display: ;">MD추천
                            <span>오더스테이션 MD가 자신있게 추천합니다.</span>
                        </span>
                    </h2>
                </div>

                <?
                $InputRows = array(array());

                // 총 갯수 구하기
                $sql = "SELECT COUNT(it_id) AS tot_cnt FROM (select it_id from tbl_shop_item where it_main2='1' and it_use = '1' and $it_view_where LIMIT 80) AS x";
                $result = sql_fetch($sql);

                $tot_cnt = $result["tot_cnt"];
                $loop_cnt = ceil($tot_cnt / 8);

                //echo "test -> ".$tot_cnt;

                // row list 구하기
                $sql = " select * from {$g5['g5_shop_item_table']} where it_main2='1' and it_use = '1' and $it_view_where order by it_id asc LIMIT 80 ";
                $array_result = sql_query($sql);
                // 결과가 있으면 배열에 넣기
                if ($array_result) {
                    for ($i = 0; $array_row = sql_fetch_array($array_result); $i++) {

                        //$InputRows[] = $array_row;
                        array_push($InputRows, $array_row);

                    }
                }

                ?>
                <div class="bxslider_md">
                    <?
                    //총 페이수만큼 돌린다.
                    for ($k = 1; $k <= $loop_cnt; $k++) {
                        ?>
                        <div class="cell">

                            <ul class="prdList grid4">
                                <?php

                                //첫페이지 fot문 조건
                                if ($k == 1) {
                                    $array_start_number = 1;
                                    $array_end_number = 8;
                                }
                                //2페이지이상 fot문 조건
                                if ($k > 1) {
                                    $array_start_number = (8 * $k) - 7;
                                    $array_end_number = $array_start_number + 7;
                                }

                                //페이지 조건만큼 돌리기
                                for ($i = $array_start_number; $i <= $array_end_number; $i++) {
                                    ?>

                                    <li onclick="location.href='<?php echo G5_SHOP_URL.'/item.php?it_id='.$InputRows[$i]['it_id']; ?>';" class="xans-record-">

                                        <?php if ($InputRows[$i]['it_cust_price'] > 0) { ?>
                                            <span class="discount_rate " data-prod-custom="<?php echo $InputRows[$i]['it_cust_price']; ?>" data-prod-price="<?php echo $InputRows[$i]['it_price']; ?>"></span>

                                            <?php if ($InputRows[$i]['it_sc_type'] == "1") { ?>
                                                <span class="free_trans_md">무료배송</span>
                                                <?php if ($InputRows[$i]['it_multisend'] == "1") { ?>
                                                    <span class="multi_trans_2">다중배송</span>
                                                <?php } ?>
                                            <?php } else { //무료때?>
                                                <?php if ($InputRows[$i]['it_multisend'] == "1") { ?>
                                                    <span class="multi_trans_1">다중배송</span>
                                                <?php }
                                            } ?>
                                        <?php } else {
                                            if ($InputRows[$i]['it_sc_type'] == "1") { ?>
                                                <span class="free_trans_new">무료배송</span>
                                                <?php if ($InputRows[$i]['it_multisend'] == "1") { ?>
                                                    <span class="multi_trans_1">다중배송</span>
                                                <?php } ?>
                                            <?php } else { ?>
                                                <?php if ($InputRows[$i]['it_multisend'] == "1") { ?>
                                                    <span class="multi_trans_0">다중배송</span>
                                                <?php }
                                            } ?>
                                        <?
                                        } ?>
                                        <div class="thumbnail">
                                            <a href="<?php echo G5_SHOP_URL.'/item.php?it_id='.$InputRows[$i]['it_id']; ?>">
                                                <img src="<?php echo G5_DATA_URL.'/item/'.$InputRows[$i]['it_img1']; ?>" width="280" height="280" alt="<?php echo $InputRows[$i]['it_name']; ?>"/>
                                            </a>
                                        </div>
                                        <div class="description" <?
                                        if (empty($InputRows[$i]['it_name'])) {
                                            ?> style="display:none;" <?
                                        } ?> >
                                            <div class="block">
                                                <div class="block_in">
                                                    <strong class="name">
                                                        <a href="<?php echo G5_SHOP_URL.'/item.php?it_id='.$InputRows[$i]['it_id']; ?>">
                                                            <span style="font-size:12px;color:#555555;"><?php echo $InputRows[$i]['it_name']; ?></span>
                                                        </a>
                                                    </strong>
                                                    <ul>
                                                        <?php //if($member['mb_type'] == '1') {
                                                        ?>
                                                        <?php if ($is_drug) { ?>
                                                            <li class=" xans-record-">
                                                                <span class='sct_price_title'>공급가:</span>
                                                                <?php if ($InputRows[$i]['it_drug_ori_price']) { ?>
                                                                    <span style="text-decoration: line-through;"><?php echo number_format($InputRows[$i]['it_drug_ori_price']); ?></span>
                                                                <?php } ?>
                                                                <span style="font-size:13px;color:#000000;font-weight:bold;"><?php echo number_format($InputRows[$i]['it_drug_price']); ?>원</span>
                                                            </li>
                                                            <li class=" xans-record-">
                                                                <span class='sct_price_title'>소비자가:</span>
                                                                <?php if ($InputRows[$i]['it_cust_price'] > 0) { ?>
                                                                    <span style="text-decoration: line-through;"><?php echo number_format($InputRows[$i]['it_cust_price']); ?></span>
                                                                <?php } ?>
                                                                <span style="font-size:13px;color:#000000;font-weight:bold;"><?php echo number_format($InputRows[$i]['it_price']); ?>원</span>
                                                            </li>
                                                            <li class=" xans-record-">
                                                                <span class="sct_incen">포인트 <?php echo number_format($InputRows[$i]['it_incen']); ?></span>
                                                            </li>
                                                        <?php } else { ?>
                                                            <li class=" xans-record-">
                                                                <?php if ($InputRows[$i]['it_cust_price'] > 0) { ?>
                                                                    <span style="text-decoration: line-through;"><?php echo number_format($InputRows[$i]['it_cust_price']); ?></span>
                                                                <?php } ?>
                                                                <span style="font-size:13px;color:#000000;font-weight:bold;"><?php echo number_format($InputRows[$i]['it_price']); ?>원</span>
                                                            </li>
                                                        <?php } ?>
                                                        <li class=" xans-record-">
                                                            <span class="w200 desc_content"><?php echo $InputRows[$i]['it_desc']; ?></span>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <?php
                                }
                                ?>
                            </ul>
                        </div>
                        <?
                    }
                    ?>
                </div>


            </div>
        </div>
        <!-- //상품진열 최저가 -->
    </div>
    <!-- //상품진열 최저가 -->

    <?php
    $sql_yoidae_chk = sql_query("select bn_id from tbl_shop_banner_trend order by bn_order asc limit 3");
    if(sql_num_rows($sql_yoidae_chk)) { ?>
    <!-- 상품진열  탭 -->
    <div class="dp-tab-wrap">
        <div class="site-wrap">
            <!-- 탭이름 -->
            <div class="dp-tab">
                <ul id="dp-tab1-list">
                    <?php
                    $sql_yoidae_li = " select bn_tabimg from tbl_shop_banner_trend order by bn_order asc limit 3";
                    $result_yoidae_li = sql_query($sql_yoidae_li);

                    for ($z = 1;$yoidae_li = sql_fetch_array($result_yoidae_li); $z++) {
                        switch ($z) {
                            case 1:
                                $onmouseover = "document.getElementById('one').style.display = 'block';document.getElementById('two').style.display = 'none';document.getElementById('three').style.display = 'none';";
                                break;
                            case 2:
                                $onmouseover = "document.getElementById('one').style.display = 'none';document.getElementById('two').style.display = 'block';document.getElementById('three').style.display = 'none';";
                                break;
                            case 3:
                                $onmouseover = "document.getElementById('one').style.display = 'none';document.getElementById('two').style.display = 'none';document.getElementById('three').style.display = 'block';";
                                break;
                            default:
                                $yoidae_id = '';
                                break;
                        }
                    ?>
                    <li id="li_one" data-cont=".tab-cont-<?php echo $z;?>" style="width:33.5% !important" onmouseover="<?php echo $onmouseover; ?>">
                        <img src="<?php echo G5_DATA_URL; ?>/banner_trend/<?php echo $yoidae_li['bn_tabimg']?>" alt=""/></li>
                    <?php } ?>
                </ul>
            </div>
            <!-- //탭이름 -->
            <!-- 상품영역 -->
            <div class="dp-tab-cont">
                <?php
                $sql_yoidae = " select * from tbl_shop_banner_trend order by bn_order asc limit 3";
                $result_yoidae = sql_query($sql_yoidae);

                for ($z = 1;$yoidae = sql_fetch_array($result_yoidae); $z++) {
                    switch ($z) {
                        case 1:
                            $yoidae_id = 'one';
                            break;
                        case 2:
                            $yoidae_id = 'two';
                            break;
                        case 3:
                            $yoidae_id = 'three';
                            break;
                        default:
                            $yoidae_id = '';
                            break;
                    }
                ?>
                <div id="dp-tab<?php echo $z;?>-cont">
                    <!-- 변경부분 -->
                    <div id="<?php echo $yoidae_id; ?>" class="xans-element- xans-product xans-product-listmain-2 xans-product-listmain xans-product-2 ec-base-product tab-cont-<?php echo $z; ?>">
                        <!-- 변경부분 -->
                        <div class="banner">
                            <a href="#none">
                                <img src="<?php echo G5_DATA_URL; ?>/banner_trend/<?php echo $yoidae['bn_pcimg']; ?>" alt=""/></a>
                        </div>
                        <!--
                            $count = 3
                            ※ 노출시킬 상품의 갯수를 숫자로 설정할 수 있으며, 설정하지 않을경우, 최대 200개로 자동제한됩니다.
                            ※ 상품 노출갯수가 많으면 쇼핑몰에 부하가 발생할 수 있습니다.
                        -->
                        <ul class="prdList grid3">
                            <?php
                            $limit_count = 3;
                            $sql = " select * from {$g5['g5_shop_item_table']} where it_id in (select it_id from tbl_shop_event_item where ev_id='".$yoidae['bn_event_id']."') limit $limit_count";
                            $result = sql_query($sql);
                            for ($i = 0; $row = sql_fetch_array($result); $i++) { ?>
                                <li class="xans-record-">
                                    <span class="discount_rate " data-prod-custom="<?php echo $row['it_price']; ?>" data-prod-price="<?php echo $row['it_price']; ?>"></span>
                                    <div class="thumbnail">
                                        <a href="<?php echo G5_SHOP_URL.'/item.php?it_id='.$row['it_id']; ?>">
                                            <img src="<?php echo G5_DATA_URL.'/item/'.$row['it_img1']; ?>" alt="<?php echo $row['it_name']; ?>" class="img_medium" width="210" height="210"/>
                                            <img src="<?php echo G5_DATA_URL.'/item/'.$row['it_img1']; ?>" alt="<?php echo $row['it_name']; ?>" class="img_small"  width="210" height="210"/>
                                        </a>

                                        <div class="iconarea">
                                            <?php if ($row['it_option_subject'] == '') { ?>
                                                <form action="./cartupdate.php" method="post">
                                                    <input type="hidden" name="it_id[]" value="<?php echo $row['it_id']; ?>">
                                                    <input type="hidden" name="sw_direct">
                                                    <input type="hidden" name="url">
                                                    <input type="hidden" name="io_type[<?php echo $row['it_id']; ?>][]" value="0">
                                                    <input type="hidden" name="ct_qty[<?php echo $row['it_id']; ?>][]" value="1">
                                                    <input type="hidden" name="io_id[<?php echo $row['it_id']; ?>][]" value="">
                                                    <input type="hidden" class="io_stock" value="9999">

                                                    <input type="image" src="<?php echo G5_THEME_URL; ?>/img/icon/icon_201805141409552500.png" class="ec-admin-icon cart"/>
                                                </form>
                                            <?php } ?>
                                            <form action="./wishupdate.php" method="post">
                                                <input type="hidden" name="it_id[]" value="<?php echo $row['it_id']; ?>">
                                                <input type="image" src="<?php echo G5_THEME_URL; ?>/img/icon/icon_201805141410035500.png" class="icon_img ec-product-listwishicon"/>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="description">
                                        <strong class="name">
                                            <a href="<?php echo G5_SHOP_URL.'/item.php?it_id='.$row['it_id']; ?>">
                                                <span style="font-size:12px;color:#555555;"><?php echo $row['it_name']; ?></span>
                                            </a>
                                        </strong>
                                        <ul class="">
                                            <?php if ($is_drug) { ?>
                                                <li class=" xans-record-">
                                                    <span class='sct_price_title'>공급가:</span>
                                                    <?php if ($row['it_drug_ori_price']) { ?>
                                                        <span style="text-decoration: line-through;"><?php echo number_format($row['it_drug_ori_price']); ?></span>
                                                    <?php } ?>
                                                    <span style="font-size:13px;color:#000000;font-weight:bold;"><?php echo number_format($row['it_drug_price']); ?>
                                                        원
                                                    </span>
                                                </li>
                                                <li class=" xans-record-">
                                                    <span class='sct_price_title'>소비자가:</span>
                                                    <?php if ($row['it_cust_price'] > 0) { ?>
                                                        <span style="text-decoration: line-through;"><?php echo number_format($row['it_cust_price']); ?></span>
                                                    <?php } ?>
                                                    <span style="font-size:13px;color:#000000;font-weight:bold;"><?php echo number_format($row['it_price']); ?>
                                                        원
                                                    </span>
                                                </li>
                                                <li class=" xans-record-">
                                                    <span class="sct_incen">
                                                        포인트 <?php echo number_format($row['it_incen']); ?></span>
                                                </li>
                                            <?php } else { ?>
                                                <li class=" xans-record-">
                                                    <?php if ($row['it_cust_price'] > 0) { ?>
                                                        <span style="text-decoration: line-through;"><?php echo number_format($row['it_cust_price']); ?></span>
                                                    <?php } ?>
                                                    <span style="font-size:13px;color:#000000;font-weight:bold;"><?php echo number_format($row['it_price']); ?>
                                                        원
                                                    </span>
                                                </li>
                                            <?php } ?>
                                        </ul>
                                    </div>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
    <!-- //상품진열  탭 -->
    <?php } ?>

    <div class="clear">
    </div>
    <!-- 상품진열 신상품 -->
    <div class="xans-element- xans-product xans-product-listmain-6 xans-product-listmain xans-product-6 ec-base-product">
        <div class="site-wrap">
            <div class="title">
                <h2>
                    <span style="display: ;">신상품
                        <span>새로나온 상품을 소개합니다</span>
                    </span>
                    <span style="display: none;"><img src="" alt="신상품 <span>새로나온 상품을 소개합니다</span>"/></span>
                </h2>
            </div>
            <ul class="prdList grid5">
                <?php
                $sql = " select * from {$g5['g5_shop_item_table']} where it_main3='1' and it_use = '1' and $it_view_where  order by it_id asc limit 10";
                $result = sql_query($sql);

                for ($i = 0; $row = sql_fetch_array($result); $i++) {
                    ?>
                    <li class="xans-record-">
                        <?php if ($row['it_cust_price'] > 0) { ?>
                            <span class="discount_rate " data-prod-custom="<?php echo $row['it_cust_price']; ?>" data-prod-price="<?php echo $row['it_price']; ?>"></span>
                            <?php if ($row['it_sc_type'] == "1") { ?>
                                <span class="free_trans_md">무료배송</span>
                            <?php } ?>
                            <?php if ($row['it_multisend'] == "1") { ?>
                                <span class="multi_trans_1">다중배송</span>
                            <?php } ?>
                            <?php
                        } else { //세일상품 아니면
                            ?>
                            <?php if ($row['it_sc_type'] == "1") { ?>
                                <span class="free_trans_new">무료배송</span>
                            <?php } ?>
                            <?php if ($row['it_multisend'] == "1") { ?>
                                <span class="multi_trans_0">다중배송</span>
                            <?php } ?>
                        <?php } ?>
                        <div class="thumbnail">
                            <a href="<?php echo G5_SHOP_URL.'/item.php?it_id='.$row['it_id']; ?>">
                                <img src="<?php echo G5_DATA_URL.'/item/'.$row['it_img1']; ?>" width="226" height="226" alt="<?php echo $row['it_name']; ?>" class="img_medium"/>
                                <img src="<?php echo G5_DATA_URL.'/item/'.$row['it_img1']; ?>" width="226" height="226" alt="<?php echo $row['it_name']; ?>" class="img_small"/>
                            </a>

                            <div class="iconarea">
                                <?php if ($row['it_option_subject'] == '') { ?>
                                    <form action="./cartupdate.php" method="post">
                                        <input type="hidden" name="it_id[]" value="<?php echo $row['it_id']; ?>">
                                        <input type="hidden" name="sw_direct">
                                        <input type="hidden" name="url">

                                        <input type="hidden" name="io_type[<?php echo $row['it_id']; ?>][]" value="0">
                                        <input type="hidden" name="ct_qty[<?php echo $row['it_id']; ?>][]" value="1">

                                        <input type="hidden" name="io_id[<?php echo $row['it_id']; ?>][]" value="">
                                        <input type="hidden" class="io_stock" value="9999">

                                        <input type="image" src="<?php echo G5_THEME_URL; ?>/img/icon/icon_201805141409552500.png" class="ec-admin-icon cart"/>
                                    </form>
                                <?php } ?>
                                <form action="./wishupdate.php" method="post">
                                    <input type="hidden" name="it_id[]" value="<?php echo $row['it_id']; ?>">
                                    <input type="image" src="<?php echo G5_THEME_URL; ?>/img/icon/icon_201805141410035500.png" class="icon_img ec-product-listwishicon"/>
                                </form>
                            </div>
                        </div>
                        <div class="description">
                            <strong class="name">
                                <a href="<?php echo G5_SHOP_URL.'/item.php?it_id='.$row['it_id']; ?>">
                                    <span style="font-size:12px;color:#555555;"><?php echo $row['it_name']; ?></span>
                                </a>
                            </strong>
                            <ul>
                                <?php if ($is_drug) { ?>
                                    <li class=" xans-record-">
                                        <span class='sct_price_title'>공급가:</span>
                                        <?php if ($row['it_drug_ori_price']) { ?>
                                            <span style="text-decoration: line-through;"><?php echo number_format($row['it_drug_ori_price']); ?></span>
                                        <?php } ?>
                                        <span style="font-size:13px;color:#000000;font-weight:bold;"><?php echo number_format($row['it_drug_price']); ?>원</span>
                                    </li>
                                    <li class=" xans-record-">
                                        <span class='sct_price_title'>소비자가:</span>
                                        <?php if ($row['it_cust_price'] > 0) { ?>
                                            <span style="text-decoration: line-through;"><?php echo number_format($row['it_cust_price']); ?></span>
                                        <?php } ?>
                                        <span style="font-size:13px;color:#000000;font-weight:bold;"><?php echo number_format($row['it_price']); ?>원</span>
                                    </li>
                                    <li class=" xans-record-">
                                        <span class="sct_incen">포인트 <?php echo number_format($row['it_incen']); ?></span>
                                    </li>
                                <?php } else { ?>
                                    <li class=" xans-record-">
                                        <?php if ($row['it_cust_price'] > 0) { ?>
                                            <span style="text-decoration: line-through;"><?php echo number_format($row['it_cust_price']); ?></span>
                                        <?php } ?>
                                        <span style="font-size:13px;color:#000000;font-weight:bold;"><?php echo number_format($row['it_price']); ?>
                                            원
                                        </span>
                                    </li>
                                <?php } ?>
                                <li class=" xans-record-">
                                    <span class="w200 desc_content"><?php echo $row['it_desc']; ?></span>
                                </li>
                            </ul>
                            <div class="icon">
                                <div class="promotion">
                                    <?php if ($row['it_type1'] == 1) { ?>
                                        <img src="<?php echo G5_THEME_URL; ?>/img/icon/icon_best.jpg" class="icon_img" alt="Best"/>
                                    <?php } ?>
                                    <?php if ($row['it_type2'] == 1) { ?>
                                        <img src="<?php echo G5_THEME_URL; ?>/img/icon/icon_recommand.jpg" class="icon_img" alt="추천"/>
                                    <?php } ?>
                                    <?php if ($row['it_type3'] == 1) { ?>
                                        <img src="<?php echo G5_THEME_URL; ?>/img/icon/icon_new.jpg" class="icon_img" alt="New"/>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <!-- //상품진열  신상품 -->
    <!-- 상품진열  세일상품 -->
    <div class="xans-element- xans-product xans-product-listmain-7 xans-product-listmain xans-product-7 ec-base-product">
        <div class="site-wrap">
            <div class="title">
                <h2>
                    <span style="display: ;">세일상품
                        <span>노마진으로 데려가세요</span>
                    </span>
                    <span style="display: none;"><img src="" alt="세일상품 <span>노마진으로 데려가세요</span>"/></span>
                </h2>
            </div>
            <ul class="prdList grid2">
                <?php
                $sql = " select * from {$g5['g5_shop_item_table']} where it_use = '1' and it_status=3 and it_cust_price >0 and $it_view_where  order by it_id asc limit 8";
                $result = sql_query($sql);

                for ($i = 0; $row = sql_fetch_array($result); $i++) {
                    ?>
                    <li class="xans-record-">
                        <?php if ($row['it_sc_type'] == "1") { ?>
                            <span class="free_trans">무료배송</span>
                        <?php } ?>
                        <?php if ($row['it_cust_price'] > 0) { ?>
                            <span class="discount_rate " data-prod-custom="<?php echo $row['it_cust_price']; ?>" data-prod-price="<?php echo $row['it_price']; ?>"></span>
                        <?php } ?>

                        <div class="thumbnail">
                            <a href="<?php echo G5_SHOP_URL.'/item.php?it_id='.$row['it_id']; ?>">
                                <img src="<?php echo G5_DATA_URL.'/item/'.$row['it_img1']; ?>" width="285" height="285" alt="<?php echo $row['it_name']; ?>" class="sct_img"/>
                                <!-- <img src="<?php echo G5_DATA_URL.'/item/'.$row['it_img1']; ?>" width="285" height="285" alt="<?php echo $row['it_name']; ?>" class="sct_img"/> -->
                            </a>
                            <div class="iconarea">
                                <?php if ($row['it_option_subject'] == '') { ?>
                                    <form action="./cartupdate.php" method="post">
                                        <input type="hidden" name="it_id[]" value="<?php echo $row['it_id']; ?>">
                                        <input type="hidden" name="sw_direct">
                                        <input type="hidden" name="url">

                                        <input type="hidden" name="io_type[<?php echo $row['it_id']; ?>][]" value="0">
                                        <input type="hidden" name="ct_qty[<?php echo $row['it_id']; ?>][]" value="1">

                                        <input type="hidden" name="io_id[<?php echo $row['it_id']; ?>][]" value="">
                                        <input type="hidden" class="io_stock" value="9999">

                                        <input type="image" src="<?php echo G5_THEME_URL; ?>/img/icon/icon_201805141409552500.png" class="ec-admin-icon cart"/>
                                    </form>
                                <?php } ?>
                                <form action="./wishupdate.php" method="post">
                                    <input type="hidden" name="it_id[]" value="<?php echo $row['it_id']; ?>">
                                    <input type="button" src="<?php echo G5_THEME_URL; ?>/img/icon/icon_201805141410035500.png" class="icon_img ec-product-listwishicon"/>
                                </form>
                            </div>
                        </div>
                        <div class="description">
                            <strong class="name">
                                <a href="<?php echo G5_SHOP_URL.'/item.php?it_id='.$row['it_id']; ?>">
                                    <span style="font-size:12px;color:#555555;"><?php echo $row['it_name']; ?></span>
                                </a>
                            </strong>
                            <ul>
                                <?php if ($is_drug) { ?>
                                    <li class=" xans-record-">
                                        <span class='sct_price_title'>공급가:</span>
                                        <?php if ($row['it_drug_ori_price']) { ?>
                                            <span style="text-decoration: line-through;"><?php echo number_format($row['it_drug_ori_price']); ?></span>
                                        <?php } ?>
                                        <span style="font-size:13px;color:#000000;font-weight:bold;"><?php echo number_format($row['it_drug_price']); ?>원</span>
                                    </li>
                                    <li class=" xans-record-">
                                        <span class='sct_price_title'>소비자가:</span>
                                        <?php if ($row['it_cust_price'] > 0) { ?>
                                            <span style="text-decoration: line-through;"><?php echo number_format($row['it_cust_price']); ?></span>
                                        <?php } ?>
                                        <span style="font-size:13px;color:#000000;font-weight:bold;"><?php echo number_format($row['it_price']); ?>원</span>
                                    </li>
                                    <li class=" xans-record-">
                                        <span class="sct_incen">포인트 <?php echo number_format($row['it_incen']); ?></span>
                                    </li>
                                <?php } else { ?>
                                    <li class=" xans-record-">
                                        &nbsp;
                                    </li>
                                    <li class=" xans-record-">
                                        <?php if ($row['it_cust_price'] > 0) { ?>
                                            <span style="text-decoration: line-through;"><?php echo number_format($row['it_cust_price']); ?></span>
                                        <?php } ?>
                                        <span style="font-size:14px;color:#000000;font-weight:bold;"><?php echo number_format($row['it_price']); ?>
                                            원
                                        </span>
                                    </li>
                                <?php } ?>
                                <li class=" xans-record-">
                                    <br/>
                                    <span class=""><?php echo $row['it_desc']; ?></span>
                                </li>
                            </ul>
                            <div class="icon">
                                <div class="promotion">
                                    <?php if ($row['it_type1'] == 1) { ?>
                                        <img src="<?php echo G5_THEME_URL; ?>/img/icon/icon_best.jpg" class="icon_img" alt="Best"/>
                                    <?php } ?>
                                    <?php if ($row['it_type2'] == 1) { ?>
                                        <img src="<?php echo G5_THEME_URL; ?>/img/icon/icon_recommand.jpg" class="icon_img" alt="추천"/>
                                    <?php } ?>
                                    <?php if ($row['it_type3'] == 1) { ?>
                                        <img src="<?php echo G5_THEME_URL; ?>/img/icon/icon_new.jpg" class="icon_img" alt="New"/>
                                    <?php } ?>
                                </div>
                            </div>

                        </div>
                    </li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <style>
        .sct_img {
            margin: -1px;
            border: 1px solid rgba(153, 153, 153, 0.1);
        }

        .sct_img:hover {
            border: 1px solid rgba(153, 153, 153, 0.6);
        }

        <!--
        변경부분

        -->

        .center_tab {
            background-color: gray;
        }

        .bg2 {
            background-color: #FFFFFF;
        }

        .bg3 {
            background-color: #F2F2F2;
        }

        <!--
        변경부분

        -->

    </style>

    <!-- 배너-->
    <div class="dp-block4 bx5">
        <ul id="bxslider5">
            <?php
            $main_bottom_list = main_banner('메인하단');
            for ($i = 0; $row = sql_fetch_array($main_bottom_list); $i++) { ?>
                <li>
                    <a href="<?php echo $row['bn_url']; ?>" target="_self">
                        <img src="<?php echo G5_DATA_URL.'/banner/'.$row['bn_id']; ?>" alt=""></a>
                </li>
            <?php } ?>
        </ul>
    </div><!-- //배너-->

    <script>
        /* 배너 페이지 표시 버튼 눌렀을때 정지되는 현상 막기위한 스크립트 */
        $(document).ready(function () {
            var slider = $('#bxslider5').bxSlider({
                onSliderLoad: function () {
                },
                mode: 'fade',
                slideMargin: 0,
                auto: true,
                speed: 300,
                pause: 2500,
                autoHover: true,
                adaptiveHeight: true,
                pager: true,
                onSlideAfter: function () {
                    slider.stopAuto();
                    slider.startAuto();
                }

            });

            var slider1 = $('#bxslider1').bxSlider({
                mode: 'fade',
                auto: true,
                autoHover: true,
                speed: 300,
                pause: 2500,
                adaptiveHeight: true,
                pagerCustom: '#bxslider1-pager',
                onSlideAfter: function () {
                    slider1.stopAuto();
                    slider1.startAuto();
                }

            });

            var slider_md = $('.bxslider_md').bxSlider({
                onSliderLoad: function () {
                    $('.prod-slide .bx2').css({
                        visibility: 'visible',
                        height: 'auto'
                    });
                    $('.site-wrap .bx-wrapper').css({'background-color': '#FFF'});
                },
                mode: 'horizontal',
                auto: true,
                autoHover: true,
                speed: 500,
                pause: 4000,
                //adaptiveHeight: true,
                pager: false,
                onSlideAfter: function () {
                    slider_md.stopAuto();
                    slider_md.startAuto();
                }
            });

        });

        $(window).scroll(function(event) {
            var current_top = parseInt($(this).scrollTop());

            // right quick
            if((current_top + 100) > 280){
                if (! $('#pop_layer').hasClass('fixed')) $('#pop_layer').addClass('fixed');
            }else{
                if ($('#pop_layer').hasClass('fixed')) {
                    $('#pop_layer').removeClass('fixed');
                }
            }
        });

    </script>


<?php
include_once(G5_THEME_SHOP_PATH.'/shop.tail.php');
?>
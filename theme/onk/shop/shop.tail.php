<?php
if (!defined("_GNUBOARD_")) exit; // 개별 페이지 접근 불가

if (G5_IS_MOBILE) {
    include_once(G5_THEME_MSHOP_PATH.'/shop.tail.php');
    return;
}

$admin = get_admin("super");

// 사용자 화면 우측과 하단을 담당하는 페이지입니다.
// 우측, 하단 화면을 꾸미려면 이 파일을 수정합니다.
?>

<!--
<div id="ft">
    <div class="ft_wr">
        <ul class="ft_ul">
            <li><a href="<?php echo G5_BBS_URL; ?>/content.php?co_id=company">회사소개</a></li>
            <li><a href="<?php echo G5_BBS_URL; ?>/content.php?co_id=provision">서비스이용약관</a></li>
            <li><a href="<?php echo G5_BBS_URL; ?>/content.php?co_id=privacy">개인정보처리방침</a></li>
            <li><a href="<?php echo get_device_change_url(); ?>">모바일버전</a></li>
        </ul>
        
        <a href="<?php echo G5_SHOP_URL; ?>/" id="ft_logo"><img src="<?php echo G5_DATA_URL; ?>/common/logo_img2" alt="처음으로"></a>

        <div class="ft_info">
            <span><b>회사명</b> <?php echo $default['de_admin_company_name']; ?></span>
            <span><b>주소</b> <?php echo $default['de_admin_company_addr']; ?></span><br>
            <span><b>사업자 등록번호</b> <?php echo $default['de_admin_company_saupja_no']; ?></span>
            <span><b>대표</b> <?php echo $default['de_admin_company_owner']; ?></span>
            <span><b>전화</b> <?php echo $default['de_admin_company_tel']; ?></span>
            <span><b>팩스</b> <?php echo $default['de_admin_company_fax']; ?></span><br>
           
            <span><b>통신판매업신고번호</b> <?php echo $default['de_admin_tongsin_no']; ?></span>
            <span><b>개인정보 보호책임자</b> <?php echo $default['de_admin_info_name']; ?></span>

            <?php if ($default['de_admin_buga_no']) echo '<span><b>부가통신사업신고번호</b> '.$default['de_admin_buga_no'].'</span>'; ?><br>
            Copyright &copy; 2001-2013 <?php echo $default['de_admin_company_name']; ?>. All Rights Reserved.
        </div>

        <div class="ft_cs">
            <h2>고객센터</h2>
            <strong>02-123-1234</strong>
            <p>월-금 am 9:00 - pm 05:00<br>점심시간 : am 12:00 - pm 01:00</p>
        </div>
        <button type="button" id="top_btn"><i class="fa fa-arrow-up" aria-hidden="true"></i><span class="sound_only">상단으로</span></button>
        <script>
        
        $(function() {
            $("#top_btn").on("click", function() {
                $("html, body").animate({scrollTop:0}, '500');
                return false;
            });
        });
        </script>
    </div>


</div>
-->
		</div>
	</div>

	<div class="clear">
	</div>
</div>
<div id="dp-footer">
	<!-- 커뮤니티 -->
	<div class="xans-element- xans-layout xans-layout-footer site-wrap dp-commu-wrap ">
		<div class="dp-box dp-box-1">
			<h5>고객센터</h5>
			<ul class="topbl callcenter">
				<li class="phone">1544-5462</li>
				<li class="runtime">운영시간 : 월~금 am10:00 ~ pm 18:00<br>
				 점심시간 : pm 12:00 ~ pm 13:00<br>
				 토, 일, 공휴일은 휴무 입니다.</li>
				<li class="mail">
				<img src="<?php echo G5_THEME_URL;?>/img/footer_ico_mail.gif" alt=""> &nbsp;<a href="mailto:master@orderstation.kr">master@orderstation.kr</a>
				</li>
			</ul>
			<ul class="bankinfo">
				<li>
				<h5>입금계좌안내</h5>
				</li>
				<li> 국민은행 140101-01-008595<br>
				</li>
			</ul>
		</div>
		<div class="dp-box dp-box-2">
			<h5>공지사항</h5>
			<p class="more">
				<a href="/bbs/board.php?bo_table=notice">더보기 <img src="<?php echo G5_THEME_URL;?>/img/footer_icon_more.gif" alt=""></a>
			</p>
			<div class="topbl">
				
<style>
.notice_footer {
	width:380px;
}
.notice_footer .dp-subject {
	background:url(<?php echo G5_THEME_URL;?>/img/main_board_bullet.jpg) no-repeat left;
	width:270px;
	float:left;
	text-align:left;
	text-overflow:ellipsis;
	overflow:hidden;
	white-space:nowrap;
	padding:2px 0 2px 10px;
}
.notice_footer .dp-subject a {
	font-size:12px;
	line-height:15px;
}
.notice_footer .dp-date {
	float:right;
	margin-right:10px;
	color:#888;
	padding:2px 0;
	font-size: 12px;
}
</style>


<div class="xans-element- xans-board xans-board-listpackage-1 xans-board-listpackage xans-board-1 notice_footer ">
	<?php			
		$sql = " select * from tbl_write_notice where wr_is_comment = 0 order by wr_num limit 0, 5 ";

		$result = sql_query($sql);

		for ($i=0; $row=sql_fetch_array($result); $i++)
		{

		?>

	<ul class="xans-element- xans-board xans-board-list-1 xans-board-list xans-board-1 xans-record-">

		<li>
		<div class="dp-subject">
			<a href="/bbs/board.php?bo_table=notice&wr_id=<?php echo $row['wr_id']?>" target="_parent"><?php echo $row['wr_subject']?></a>
		</div>
		<div class="dp-date">
			<?php echo substr($row['wr_datetime'], 0, 10)?>
		</div>
		</li>
	</ul>
	<?php } ?>
	<div class="clear">
	</div>
</div>






			</div>
			<div class="iconbl">
				<ul>
					<li><a href="/bbs/faq.php"><img src="<?php echo G5_THEME_URL;?>/img/footer_ico_1.gif" alt=""><br>
					고객센터</a></li>
					<li><a href="/shop/mypage.php"><img src="<?php echo G5_THEME_URL;?>/img/footer_ico_2.gif" alt=""><br>
					마이페이지</a></li>
					<li><a href="/shop/cart.php"><img src="<?php echo G5_THEME_URL;?>/img/footer_ico_3.gif" alt=""><br>
					장바구니</a></li>
					<li><a href="/shop/orderinquiry.php"><img src="<?php echo G5_THEME_URL;?>/img/footer_ico_4.gif" alt=""><br>
					주문조회</a></li>
					<li><a href="/shop/orderinquiry.php"><img src="<?php echo G5_THEME_URL;?>/img/footer_ico_5.gif" alt=""><br>
					배송조회</a></li>
				</ul>
			</div>
		</div>
		<div class="dp-box dp-box-3">
			<h5 class="bg-title"><a href="/shop/itemuselist.php"><img src="<?php echo G5_THEME_URL;?>/img/footer_commu_review.png" alt=""></a></h5>
			<div class="topbl">
				<?php			
					$sql = " select * from `{$g5['g5_shop_item_use_table']}` where service_type is null and  mb_id not in (select mb_id from tbl_member where mb_type='1') order by is_id desc limit 0, 5 ";
					

					$result = sql_query($sql);

					for ($i=0; $row=sql_fetch_array($result); $i++)
					{
						$star = $row['is_score'];

					?>

				<ul class="xans-element- xans-board xans-board-list-1 xans-board-list xans-board-1 xans-record-" style="margin-bottom:4px;">



					<li>
					<div class="dp-subject">
						<a href="/shop/itemuselist.php"><?php echo strip_tags($row['is_content']); ?></a>
					</div>
					<div>
						<img src="<?php echo G5_URL; ?>/shop/img/s_star<?php echo $star; ?>.png" width="80">
					</div>
					</li>
				</ul>
				<?php } ?>






			</div>
			<div class="qna">
				<h5><a href="/bbs/qalist.php">Q&amp;A</a></h5>
				<div class="leftarea">
					궁금하신 점이 있으시면 문의해주세요.<br>
					언제나 친절하게 상담해 드리겠습니다.
				</div>
				<div class="rightarea">
					<a href="/bbs/qalist.php">문의하러 가기</a>
				</div>
			</div>
		</div>
	</div>
	<!-- // 커뮤니티 -->
	<!-- 사이트 정보 -->
	<div class="dp-info-wrap">
		<div class="site-wrap">
			<ul class="util_menu">
				
				<li><a href="<?php echo G5_BBS_URL; ?>/content.php?co_id=company">회사소개</a></li>
				<li><a href="<?php echo G5_BBS_URL; ?>/content.php?co_id=provision">서비스이용약관</a></li>
				<li><a href="<?php echo G5_BBS_URL; ?>/content.php?co_id=privacy">개인정보처리방침</a></li>
				<li><a href="<?php echo get_device_change_url(); ?>">모바일버전</a></li>
				<li><a href="<?php echo G5_BBS_URL; ?>/qapartner.php">제휴문의</a></li>
			</ul>

		
			<div class="xans-element- xans-layout xans-layout-footer info">
				<span class="">상호 : <?php echo $default['de_admin_company_name']; ?> </span>
				| <span>대표자 : <?php echo $default['de_admin_company_owner']; ?></span>
				| <span>전화 : <?php echo $default['de_admin_company_tel']; ?></span>
				| <span>팩스 : <?php echo $default['de_admin_company_fax']; ?></span>
                | <span>사업자등록번호 : <?php echo $default['de_admin_company_saupja_no']; ?></span> &nbsp;&nbsp;<button class="btn" onclick="bizCommPop();" style="padding: 0;line-height: 14px;height: auto;font-size: 10px;">사업자정보확인</button>
                <br>
				<span>주소 : <?php echo $default['de_admin_company_addr']; ?> </span>
                | <span>개인정보관리책임자 : <?php echo $default['de_admin_info_name']; ?></span>
                | <span class="">메일 : <a href="mailto:master@orderstation.kr">master@orderstation.kr</a></span>
                <br>
				<span>통신판매번호 : <?php echo $default['de_admin_tongsin_no']; ?></span>
                | <span>건강기능식품판매업신고 : <?php echo $default['de_admin_health_food_no']; ?></span>
				<span></span>
				<br>
				<div class="copyright">
					Copyright &copy; 2001-2020 <?php echo $default['de_admin_company_name']; ?>. All Rights Reserved.
				</div>
			</div>
			<div class="btm_icon">
				<img src="<?php echo G5_THEME_URL;?>/img/btm_escrow.jpg" alt="">
				&nbsp;
			</div>
		</div>
	</div>
	<!-- // 사이트 정보 -->
</div>

<script>
    function bizCommPop() {
        var saupja_no = "<?php echo $default['de_admin_company_saupja_no']; ?>";
        var url =
            "http://www.ftc.go.kr/bizCommPop.do?wrkr_no=" + saupja_no;
        window.open(url, "bizCommPop", "width=750, height=700;");
    }
</script>

<!-- 참고: 결제를 위한 필수 영역 -->
<!-- <div id="progressPaybar" style="display:none;">
	<div id="progressPaybarBackground" class="layerProgress">
	</div>
	<div id="progressPaybarView">
		<div class="box">
			<p class="graph">
				<span><img src="../img.echosting.cafe24.com/skin/base_ko_KR/layout/txt_progress.gif" alt="현재 결제가 진행중입니다."></span>
				<span><img src="../img.echosting.cafe24.com/skin/base/layout/img_loading.gif" alt=""></span>
			</p>
			<p class="txt">
				 본 결제 창은 결제완료 후 자동으로 닫히며,결제 진행 중에 본 결제 창을 닫으시면<br>
				 주문이 되지 않으니 결제 완료 될 때 까지 닫지 마시기 바랍니다.
			</p>
		</div>
	</div>
</div> -->





 <script src="<?php echo G5_THEME_URL ?>/js/onk.js?ver=<?php echo G5_JS_VER; ?>3"></script> 




<?php
$sec = get_microtime() - $begin_time;
$file = $_SERVER['SCRIPT_NAME'];

if ($config['cf_analytics']) {
    echo $config['cf_analytics'];
}
?>

<script src="<?php echo G5_JS_URL; ?>/sns.js"></script>
<!-- } 하단 끝 -->

<?php
include_once(G5_THEME_PATH.'/tail.sub.php');
?>

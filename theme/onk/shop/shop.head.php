<?php
if (!defined("_GNUBOARD_")) exit; // 개별 페이지 접근 불가

if(G5_IS_MOBILE) {
    include_once(G5_THEME_MSHOP_PATH.'/shop.head.php');
    return;
}

include_once(G5_THEME_PATH.'/head.sub.php');

include_once(G5_LIB_PATH.'/outlogin.lib.php');
include_once(G5_LIB_PATH.'/poll.lib.php');
include_once(G5_LIB_PATH.'/visit.lib.php');
include_once(G5_LIB_PATH.'/connect.lib.php');
include_once(G5_LIB_PATH.'/popular.lib.php');

//include_once(G5_LIB_PATH.'/latest.lib.php');

?>

<?php include(G5_SHOP_PATH.'/terms_popup.php');?>

<div id="quick">
	<div class="recent_view">
		<div class="title">
			<a href="#">최근본상품</a>
		</div>
<?php
$tv_datas = get_view_today_items(true);

$tv_div['top'] = 0;
$tv_div['img_width'] = 60;
$tv_div['img_height'] = 60;
$tv_div['img_length'] = 2; // 한번에 보여줄 이미지 수

// add_stylesheet('css 구문', 출력순서); 숫자가 작을 수록 먼저 출력됨
//add_stylesheet('<link rel="stylesheet" href="'.G5_SHOP_SKIN_URL.'/style.css">', 0);


?>
<style>
#right_cart2 {
 padding: 1px 2px 1px 2px;
 background:#FF8224;
 margin-right:2px;
 margin-left:-4px;
 margin-top:6px;
 color:#ffffff;
 font-weight:bold;
 border-radius:50%;
 display:inline-block;
 width:13px;
 text-align:center;
 
}
</style>
<!-- 오늘 본 상품 시작 { -->
<div id="stv" >

    <?php if ($tv_datas) { // 오늘 본 상품이 1개라도 있을 때 ?>
    <?php
    $tv_tot_count = 0;
    $k = 0;
    $i = 1;
    foreach($tv_datas as $rowx)
    {
        if(!$rowx['it_id'])
            continue;
        
        $tv_it_id = $rowx['it_id'];

        if ($tv_tot_count % $tv_div['img_length'] == 0) $k++;

        $it_name = get_text($rowx['it_name']);
        $img = get_it_image($tv_it_id, $tv_div['img_width'], $tv_div['img_height'], $tv_it_id, '', $it_name);

        if ($tv_tot_count == 0) echo '<ul id="stv_ul">'.PHP_EOL;
        echo '<li class="stv_item c'.$k.'">'.PHP_EOL;
        echo '<div class="prd_img">';
        echo $img;
        //echo '</div>'.PHP_EOL;
        //echo '<div class="prd_name">';
        //echo cut_str($it_name, 10, '').PHP_EOL;
        //echo '</div>';
        //echo '<div class="prd_cost">';
        //echo number_format(get_price($rowx)).PHP_EOL;
        //echo '</div>'.PHP_EOL;
         echo '</li>'.PHP_EOL;

        $tv_tot_count++;
        $i++;
    }
    if ($tv_tot_count > 0) echo '</ul>'.PHP_EOL;
    ?>
        <div id="stv_btn"></div>
        <span id="stv_pg"></span>

        <script>
        $(function() {
            var itemQty = <?php echo $tv_tot_count; ?>; // 총 아이템 수량
            var itemShow = <?php echo $tv_div['img_length']; ?>; // 한번에 보여줄 아이템 수량
            if (itemQty > itemShow)
            {
                $('#stv_btn').append('<button type="button" id="up"><i class="fa fa-angle-left" aria-hidden="true"></i> 이전</button><button type="button" id="down">다음 <i class="fa fa-angle-right" aria-hidden="true"></i></button>');
            }
            var Flag = 1; // 페이지
            var itemCount = <?php echo $i-1; ?>;
            var EOFlag = parseInt(itemCount/itemShow); // 전체 리스트를 3(한 번에 보여줄 값)으로 나눠 페이지 최댓값을 구하고
            var itemRest = parseInt(itemCount%itemShow); // 나머지 값을 구한 후
            if (itemRest > 0) // 나머지 값이 있다면
            {
                EOFlag++; // 페이지 최댓값을 1 증가시킨다.
            }
            $('.c'+Flag).css('display','block');
            $('#stv_pg').text(Flag+'/'+EOFlag); // 페이지 초기 출력값
            $('#up').click(function() {
                if (Flag == 1)
                {
                    alert('목록의 처음입니다.');
                } else {
                    Flag--;
                    $('.c'+Flag).css('display','block');
                    $('.c'+(Flag+1)).css('display','none');
                }
                $('#stv_pg').text(Flag+'/'+EOFlag); // 페이지 값 재설정
            })
            $('#down').click(function() {
                if (Flag == EOFlag)
                {
                    alert('더 이상 목록이 없습니다.');
                } else {
                    Flag++;
                    $('.c'+Flag).css('display','block');
                    $('.c'+(Flag-1)).css('display','none');
                }
                $('#stv_pg').text(Flag+'/'+EOFlag); // 페이지 값 재설정
            });
        });
        </script>

        <?php } else { // 오늘 본 상품이 없을 때 ?>

        <p class="li_empty">없음</p>

        <?php } ?>
		</div>


		<div class="xans-element- xans-layout xans-layout-productrecent" style="display: none;">
			<div class="prev" style="cursor: pointer;">
				<img src="<?php echo G5_THEME_URL;?>/img/side_recent_prev.jpg" alt="이전상품">
			</div>
			<ul>
			</ul>
			<div class="next" style="cursor: pointer;">
				<img src="<?php echo G5_THEME_URL;?>/img/side_recent_next.jpg" alt="다음상품">
			</div>
		</div>
	</div>
	<!-- <div class="banner">
		<p>
			<a href="#" target="_blank"><img src="<?php echo G5_THEME_URL;?>/img/side_banner_1.jpg" alt=""></a>
		</p>
		<p>
			<a href="#" target="_blank"><img src="<?php echo G5_THEME_URL;?>/img/side_banner_2.jpg" alt=""></a>
		</p>
	</div> -->
	<p class="pageTop">
		<a href="javascript:page_top();" title=""><img src="<?php echo G5_THEME_URL;?>/img/side_top_arrow.png" alt="맨위로"> TOP</a>
	</p>

    <p style="margin-top:3px;">
        <!-- 하하하약국 -->
        <a href="/bbs/qa_pharm.php"><img src="<?php echo G5_THEME_URL;?>/img/hahaha_contract.png" alt="약국가입 문의" ></a>
    </p>
    <p style="margin-top:3px;">
        <!-- 무이자할부안내 오른쪽 배너이미지 -->
        <a href="../../bbs/qapartner.php"><img src="<?php echo G5_THEME_URL;?>/img/partner.jpg" alt="입점제휴" ></a>
    </p>
	<p style="margin-top:3px;">
		<!-- 무이자할부안내 오른쪽 배너이미지 -->
     <!--   <?php
/*        $muija = sql_fetch_row(main_banner('무이자_side', 1));
        */?>
		<a href="/shop/payinfo.php"><img src="<?php /*echo G5_DATA_URL.'/banner/'.$muija['bn_id']; */?>?ver=<?php /*echo G5_YM_VER; */?>" alt="신용카드무이자할부" ></a>-->
	</p>
	<p style="margin-top:3px;">
		<!-- 제노피 -->
		<a href="https://www.geno-p.com/" target="_blank"><img src="<?php echo G5_THEME_URL;?>/img/clinomics_left.png" alt="검사지결과" ></a>
	</p>


	<? if( $config['cf_time_sale_use'] == "Y" && !empty($config['cf_time_sale_it_id']) ){ ?>
	<p style="margin-top:3px;">
		<!--<a href="/shop/item.php?it_id=<?=$config['cf_time_sale_it_id']?>">-->
		<a href="/shop/event.php?ev_id=<?=$config['cf_time_sale_it_id']?>">
			<img src="<?php echo G5_THEME_URL;?>/img/time_sale.jpg" alt="타임세일">
		</a>
	</p>
	<? } ?>
	<!--
	<p style="margin-top:5px;">
		<a href="/shop/event.php?ev_id=1554683244">
			<img src="<?php echo G5_THEME_URL;?>/img/dalkong.jpg" alt="달콩100원의행복">
		</a>
	</p>
	-->
</div>
<!-- 우측 퀵메뉴 영역 -->
<div id="tbanner" style="height: 80px; display: none;">
    <ul class="image_list">
        <li data-num="1"><a href="https://www.orderstation.co.kr/shop/event.php?ev_id=1615782731" target="_self"><img src="<?php echo G5_THEME_URL;?>/img/top_bnr.jpg?ver=<?php echo G5_IMG_VER?>"></a></li>
    </ul>
    <!--
    <div class="page">
        <div class="prev">
        </div>
        <div class="next">
        </div>
    </div>
    -->
    <div class="close" style="height:100%; width: 5%"></div>
    <!--    <div class="oneday">-->
    <!--        <input type="checkbox" class="is_hide" id="chk_oneday_7" checked="checked"><label for="chk_oneday_7" > 오늘하루 열지않음</label>-->
    <!--    </div>-->
</div>

<div class="top-menu-wrap">
	<div class="inner">
		<div class="site-wrap">
			<div class="bookmark">
				<ul>
					<li><a href="/link/bookmark.html" target="_blank" onclick="winPop(this.href); return false;">즐겨찾기</a></li>
					<!-- <li><a href="/link/shortcut.html" target="_blank" onclick="winPop(this.href); return false;">PC 바로가기 아이콘 설치</a></li> -->
				</ul>
			</div>
			<!-- 스크롤 내렸을때 로고 -->
			<div class="xans-element- xans-layout xans-layout-logotop menu-logo ">
				<div class="vertical">
					<a href="/shop/">
		
					<img src="<?php echo G5_THEME_URL;?>/img/logo_new.jpg" alt="" ></a>
				</div>
			</div>
			<div class="float-right">
				<ul class="dp-util">
					<?php if ($is_member) { ?>
					<li><a href="<?php echo G5_BBS_URL; ?>/member_confirm.php?url=register_form.php">정보수정</a></li>
					<li><a href="<?php echo G5_BBS_URL; ?>/logout.php?url=shop">로그아웃</a></li>

					<!-- 로그인 후-->
					<li class="xans-element- xans-layout xans-layout-orderbasketcount" id="right_cart">
                        <a href="<?php echo G5_SHOP_URL; ?>/cart.php">장바구니<span class="EC-Layout-Basket-count"></span></a>
					</li>
                    <?php
                    set_cart_id($sw_direct);
                    if ($sw_direct) {
                        $tmp_cart_id = get_session('ss_cart_direct');
                    } else {
                        $tmp_cart_id = get_session('ss_cart_id');
                    }
                    $sql = "SELECT DISTINCT(count(ct_id)) AS cnt3 FROM {$g5['g5_shop_cart_table']} WHERE od_id='$tmp_cart_id' AND service_type IS NULL";
                    $row = sql_fetch($sql);
                    $cnt3 = (int)$row['cnt3'];
                    ?>
					<span id="right_cart2"><?php echo $cnt3?></span>

					<li><a href="<?php echo G5_SHOP_URL; ?>/mypage.php">마이페이지</a></li>

					<?php if ($member['mb_type'] > 1) {  ?>
					<li class="tnb_admin"><a href="<?php echo G5_ADMIN_URL; ?>/shop_admin/"><b>관리자</b></a></li>
					<?php }  ?>
					<?php } else { ?>

					<!-- 로그인 전-->
					<li class="xans-element- xans-layout xans-layout-statelogoff ">
                        <a href="<?php echo G5_BBS_URL; ?>/login.php?url=<?php echo $urlencode; ?>" class="login">로그인</a>
					</li>
                    <li class="xans-element- xans-layout xans-layout-statelogoff ">
                        <a href="<?php echo G5_SHOP_URL; ?>/cart.php" class="login">장바구니</a>
                    </li>
                    <li class="xans-element- xans-layout xans-layout-statelogoff ">
                        <a href="<?php echo G5_SHOP_URL; ?>/orderinquiry.php?url=<?php echo $urlencode; ?>" class="login">주문조회</a>
                    </li>
					<li class="xans-element- xans-layout xans-layout-statelogoff point_wrap ">
                        <a href="<?php echo G5_BBS_URL; ?>/register.php">회원가입</a>
					    <span class="point" style="top: 29.5296px;"><img src="<?php echo G5_THEME_URL;?>/img/top_icon_point.png" alt=""></span>
					</li>
					<li class="xans-element- xans-layout xans-layout-statelogoff "><a href="<?php echo G5_SHOP_URL; ?>/mypage.php">마이페이지<span class="ico"></span></a>
					<div class="dropdown">
						<p>
							<a href="<?php echo G5_SHOP_URL; ?>/cart.php">장바구니</a>
						</p>
						<p>
							<a href="<?php echo G5_SHOP_URL; ?>/mypage.php">회원정보</a>
						</p>
						<p>
							<a href="<?php echo G5_SHOP_URL; ?>/mypage.php">관심상품</a>
						</p>
						<p class="displaynone">
							<a href="#">'좋아요' 상품</a>
						</p>
						<p>
							<a href="<?php echo G5_SHOP_URL; ?>/mypage.php">적립금</a>
						</p>
						
						<p class="displaynone">
							<a href="<?php echo G5_SHOP_URL; ?>/mypage.php">1:1문의</a>
						</p>
						<p class="displaynone">
							<a href="<?php echo G5_SHOP_URL; ?>/mypage.php">쿠폰</a>
						</p>
						<p class="">
							<a href="<?php echo G5_SHOP_URL; ?>/mypage.php">게시물관리</a>
						</p>
						<p class="">
							<a href="<?php echo G5_SHOP_URL; ?>/mypage.php">배송주소록관리</a>
						</p>
						<p class="bg">
						</p>
						<p class="nologin">
							로그인이<br>
							필요합니다.
						</p>
					</div>
					</li>
					<?php } ?>
					<li>
					<a href="/bbs/board.php?bo_table=notice">커뮤니티<span class="ico"></span></a>
					<div class="xans-element- xans-layout xans-layout-boardinfo dropdown">
						<p class="xans-record-">
							<a href="/bbs/board.php?bo_table=notice">공지사항</a>
						</p>
						<p class="xans-record-">
							<a href="/bbs/faq.php">이용안내 FAQ</a>
						</p>
						<p class="xans-record-">
							<a href="/shop/itemqalist.php">상품 Q&amp;A</a>
						</p>
						<p class="xans-record-">
							<a href="/bbs/board.php?bo_table=free">자유게시판</a>
						</p>
						<p class="xans-record-">
							<a href="/bbs/board.php?bo_table=gallery">갤러리</a>
						</p>
						<!--
						<p class="xans-record-">
							<a href="/bbs/board.php?bo_table=qa">질문답변</a>
						</p>
						-->
					</div>
					</li>
					
				</ul>
			</div>
			<div class="clear">
			</div>
		</div>
	</div>
</div>
<div class="logo-wrap">
	<div class="site-wrap">
		<div class="logo ">
			 <div class="vertical">
				<a href="/shop/">
				<img src="<?php echo G5_THEME_URL;?>/img/logo_new.jpg" alt=""></a>
			</div> 
		</div>


		<!-- 상단 우측 배너 영역 -->
		<div class="top-banner">
            <ul class="banner" id="bx-topRight">
				<li><a href="/shop/event.php?ev_id=1620885474"><img src="<?php echo G5_THEME_URL;?>/img/home_top1.jpg?ver=<?php echo G5_IMG_VER;?>" width="260" alt=""></a></li>
				<!--<li><a href="/shop/item.php?it_id=354003"><img src="<?php echo G5_THEME_URL;?>/img/top_left_banner_02.jpg" alt=""></a></li>-->
				<li><a href="/shop/event.php?ev_id=1620886115"><img src="<?php echo G5_THEME_URL;?>/img/home_top2.jpg?ver=<?php echo G5_IMG_VER;?>_02" width="260" alt=""></a></li>
			</ul>
		</div>
		<div class="clear"></div>

		 <div class="ad-banner displaynone">
			<div class="banner"><a href="/shop/qalist.php"><img src="<?php echo G5_THEME_URL;?>/img/flt_bn.png"/></a></div>
			<div class="close"><img src="<?php echo G5_THEME_URL;?>/img/ad_pop_close.png"/></div>
		</div>


		
	</div>
</div>
<div class="cate-wrap">
	<div class="inner">
		<!--전체카테고리 레이어창-->
		<div class="all-menu-box">
			<div class="top-box">
				<div class="site-wrap">
					<div class="all-menu all-toggle"></div>
				</div>
			</div>


			<div class="conts-box">
				<div class="site-wrap">
					<div class="xans-element- xans-layout xans-layout-category cate-list">
						<?php
						// 1단계 분류 판매 가능한 것만
                        $hsql_add = "";
                        if (!($member['mb_referee'] == 4 || $member['mb_extra'] == 11|| $member['mb_type'] == 9)) {
                            $hsql_add = " and ca_staff_use=0 ";
                        }

						$hsql = " select ca_id, ca_name from {$g5['g5_shop_category_table']} where length(ca_id) = '2' and ca_use = '1' ".$hsql_add." order by ca_order, ca_id ";
						$hresult = sql_query($hsql);
						$gnb_zindex = 999; // gnb_1dli z-index 값 설정용
						for ($i=0; $row=sql_fetch_array($hresult); $i++)
						{
							$gnb_zindex -= 1; // html 구조에서 앞선 gnb_1dli 에 더 높은 z-index 값 부여
							// 2단계 분류 판매 가능한 것만
							if($member['mb_type']=='1') {
							$sql2 = " select ca_id, ca_name from {$g5['g5_shop_category_table']} where LENGTH(ca_id) = '4' and SUBSTRING(ca_id,1,2) = '{$row['ca_id']}' and ca_use = '1' and ca_web_view=1 ";
							} else {
							$sql2 = " select ca_id, ca_name from {$g5['g5_shop_category_table']} where LENGTH(ca_id) = '4' and SUBSTRING(ca_id,1,2) = '{$row['ca_id']}' and ca_use = '1' and ca_app_view=1 ";
							}

                            if (!($member['mb_referee'] == 4 || $member['mb_extra'] == 11 || $member['mb_type'] == 9)) {
                                $sql2 .= " and ca_staff_use=0 ";
                            }

							$sql_order_by = " order by ca_order, ca_id ";
                            $sql2 .= $sql_order_by;

							$result2 = sql_query($sql2);
							$count = sql_num_rows($result2);
						?>
						<dl class="xans-record-">
							<dt class="box">
							<div class="catename"><a href="<?php echo G5_SHOP_URL.'/list.php?ca_id='.$row['ca_id']; ?>" ><?php echo $row['ca_name']; ?></a></div>
							<div class="conts">
							<?php
							for ($j=0; $row2=sql_fetch_array($result2); $j++)
							{
							if ($j==0) echo '<ul>';
							?>
								<li ><a href="<?php echo G5_SHOP_URL; ?>/list.php?ca_id=<?php echo $row2['ca_id']; ?>" ><?php echo $row2['ca_name']; ?></a></li>
							<?php }
							if ($j>0) echo '</ul>';
							?>
							</div>
							</dt>
						</dl>
						<?php } ?>
					</div>
					<div class="clear"></div>
				</div>
			</div>
		</div>
		<!--//전체카테고리 레이어창-->

		<div class="menu-box">
		<?php include(G5_SHOP_PATH.'/aside.php');?>
		</div>
		
		<!-- //카테고리 영역 -->
		<div class="site-wrap">
			<div class="all-toggle">
				<img src="<?php echo G5_THEME_URL;?>/img/top_icon_list.gif">
				&nbsp;
			</div>
			<div class="menu-toggle">
				카테고리
				<div class="ss-arrow">
				</div>
			</div>
			<div class="top-menu">
				<ul>
					
					<li <?php if ($type==1) echo "class='red'";?>><a href="/shop/listtype.php?type=1"><img src="<?php echo G5_THEME_URL;?>/img/top_menu_icon_2.gif" alt=""> 베스트상품</a></li>					
					<li <?php if ($type==3) echo "class='red'";?>><a href="/shop/listtype.php?type=3"><img src="<?php echo G5_THEME_URL;?>/img/top_menu_icon_3.gif" alt=""> 신상품</a></li>
					<li <?php if ($type==2) echo "class='red'";?>><a href="/shop/listtype.php?type=2"><img src="<?php echo G5_THEME_URL;?>/img/top_menu_icon_1.gif" alt=""> MD추천상품</a></li>
					<!-- <li <?php if ($type==4) echo "class='red'";?>><a href="/shop/listtype.php?type=4"><img src="<?php echo G5_THEME_URL;?>/img/top_menu_icon_4.gif" alt=""> 당일배송상품</a></li> -->
					
					<li><a href="/shop/eventbanner.php"><img src="<?php echo G5_THEME_URL;?>/img/top_menu_icon_6.gif" alt=""> 이벤트</a></li>
					<li><a href="/bbs/board.php?bo_table=allsee"><img src="<?php echo G5_THEME_URL;?>/img/top_menu_icon_5.gif" alt=""> 건강지킴이</a></li>

				</ul>
			</div>
			<!-- <div class="add-cate brand-search"> -->
				<!-- <div class="middle">
					<img src="<?php echo G5_THEME_URL;?>/img/top_search.png" width="12" style="margin-right:3px;" alt=""> 브랜드찾기
				</div> -->
				<!-- <a href="/shop/couponzone.php"><img src="<?php echo G5_THEME_URL;?>/img/top_menu_icon_6.gif" alt=""> 쿠폰존</a> -->
			<!-- </div> -->
			<div class="search">
				<div class="dp-search">			
				<form id="searchBarForm" name="" action="<?php echo G5_SHOP_URL; ?>/search.php" onsubmit="return search_submit(this);">
					<input id="banner_action" name="banner_action" value="<?php echo stripslashes(get_text(get_search_string($q))); ?>" type="hidden">
					<div class="xans-element- xans-layout xans-layout-searchheader ">
						
						<input id="keyword" name="q"  class="inputTypeText" placeholder="" value="<?php echo stripslashes(get_text(get_search_string($q))); ?>" type="text">
						<div class="icon">
							<input class="search-btn" type="image" src="<?php echo G5_THEME_URL;?>/img/top_search.png" alt="검색" >
						</div>
					</div>
				</form>
				<script>
                    function search_submit(f) {
                        if (f.q.value.length < 2) {
                            alert("검색어는 두글자 이상 입력하십시오.");
                            f.q.select();
                            f.q.focus();
                            return false;
                        }
                        return true;
                    }

                    $(document).ready(function () {
                        //var tmp_cart_id = get_session('ss_cart_id');
                        $.post(
                            "<?php echo G5_SHOP_URL; ?>/ajax.cartcount.php",
                            function (data) {
                                $('#right_cart2').html(data);
                            });
                    });
                </script>
		

			</div>
		
			</div>
			
		</div>
	</div>
</div>

<div id="wrapper">





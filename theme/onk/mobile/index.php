<?php
include_once('./_common.php');

define("_INDEX_", TRUE);

include_once(G5_THEME_MSHOP_PATH.'/shop.head.php');
?>

    <!-- <script src="<?php echo G5_JS_URL; ?>/swipe.js"></script>
<script src="<?php echo G5_JS_URL; ?>/shop.mobile.main.js"></script> -->

<?php //echo display_banner('메인', 'mainbanner.10.skin.php'); ?>
<?php //echo display_banner('왼쪽', 'boxbanner.skin.php'); ?>


    <div id="contents">

        <div moduel="layout_logincheck">
            <!-- // 변수에 주소가 있는경우 입력한 주소로이동 $url = /member/login.html -->
        </div>

        <!-- 메인슬라이드 -->
        <div class="mainslide swiper-container">
            <ul id="bxslider_top_banner_auto">
                <?php
                $main_list = main_banner('메인');
                for ($i = 0; $row = sql_fetch_array($main_list); $i++) { ?>
                    <li class="item">
                        <a href="<?php echo G5_SHOP_URL.'/bannerhit.php?bn_id='.$row['bn_id']; ?>">
                            <img src="<?php echo G5_DATA_URL.'/banner/'.$row['bn_id']; ?>" width="100%" alt=""></a>
                    </li>
                <?php } ?>


            </ul>

        </div>

        <div class="nav">
            <div class="nav">
                <ul class="grid3">
                    <li class="color">
                        <a href="<?php echo G5_SHOP_URL; ?>/listtype.php?type=1">
                            <img src="<?php echo G5_THEME_URL.'/img/'.G5_MOBILE_DIR; ?>/top_menu_icon_1.jpg" alt="">베스트상품
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo G5_SHOP_URL; ?>/listtype.php?type=3">
                            <img src="<?php echo G5_THEME_URL.'/img/'.G5_MOBILE_DIR; ?>/top_menu_icon_2.jpg" alt="">신상품
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo G5_SHOP_URL; ?>/listtype.php?type=2">
                            <img src="<?php echo G5_THEME_URL.'/img/'.G5_MOBILE_DIR; ?>/top_menu_icon_3.jpg" alt="">MD추천상품
                        </a>
                    </li>
                    <li>
                        <a href="/shop/eventbanner.php">
                            <img src="<?php echo G5_THEME_URL.'/img/'.G5_MOBILE_DIR; ?>/top_menu_icon_6.jpg" alt="">이벤트
                        </a>
                    </li>
                    <li>
                        <a href="/shop/health.php">
                            <img src="<?php echo G5_THEME_URL.'/img/'.G5_MOBILE_DIR; ?>/top_menu_icon_4.jpg" alt="">건강지킴이
                        </a>
                    </li>

                    <li>
                        <a href="/shop/board.php">
                            <img src="<?php echo G5_THEME_URL.'/img/'.G5_MOBILE_DIR; ?>/top_menu_icon_5.jpg" alt="">고객센터
                        </a>
                    </li>
                </ul>
            </div>
        </div>

        <!-- 베스트상품 -->
        <div
                class="xans-element- xans-product xans-product-listmain-1 xans-product-listmain xans-product-1 ec-base-product typeThumb">

            <h2>베스트상품</h2>
            <ul class="prdList grid2">
                <?php
                if ($member['mb_type'] == '1' or $member['mb_type'] == '9') {
                    $sql = " select * from {$g5['g5_shop_item_table']} where it_main1='1' and it_use = '1' and it_web_view=1 order by it_id asc limit 12";
                } else {
                    $sql = " select * from {$g5['g5_shop_item_table']} where it_main1='1' and it_use = '1' and it_app_view=1 order by it_id asc limit 12";
                }

                $result = sql_query($sql);

                for ($i = 0; $row = sql_fetch_array($result); $i++) {
                    if ($row['it_cust_price'] > 0) {
                        $is_sale = true;
                    } else {
                        $is_sale = false;
                    }
                    ?>
                    <li class="xans-record-">

                        <?php if ($is_sale) {
                            if ($row['it_sc_type'] == "1") {
                                ?>
                                <span class="free_trans">무료배송</span>
                                <?php if ($row['it_multisend'] == "1") { ?>
                                    <span class="multi_trans_2">다중배송</span>
                                <?
                                } ?>
                                <span class="discount_rate " data-prod-custom="<?php echo $row['it_cust_price']; ?>" data-prod-price="<?php echo $row['it_price']; ?>"></span>
                            <?php } else {
                                if ($row['it_multisend'] == "1") {
                                    ?>
                                    <span class="multi_trans_1">다중배송</span>
                                <?php }
                            }
                        } else { ?>
                            <?php if ($row['it_sc_type'] == "1") { ?>
                                <span class="free_trans_new">무료배송</span>
                                <?php if ($row['it_multisend'] == "1") { ?>
                                    <span class="multi_trans_1">다중배송</span>
                                <?
                                } ?>
                            <?php } else { ?>
                                <?php if ($row['it_multisend'] == "1") { ?>
                                    <span class="multi_trans_0">다중배송</span>
                                <?
                                } ?>
                            <?
                            }
                        } ?>
                        <div class="thumbnail">
                            <a href="<?php echo G5_SHOP_URL.'/item.php?it_id='.$row['it_id']; ?>">
                                <img src="<?php echo G5_DATA_URL.'/item/'.$row['it_img1']; ?>" alt="<?php echo $row['it_name']; ?>">
                            </a>
                        </div>
                        <div class="description">
                            <div class="name ">
                                <a href="<?php echo G5_SHOP_URL.'/item.php?it_id='.$row['it_id']; ?>"><?php echo $row['it_name']; ?></a>
                            </div>
                            <ul class="spec">
                                <?php //if($member['mb_type'] == '1') {
                                ?>
                                <?php if ($is_drug) { ?>

                                    <li class="desc">P:<?php echo number_format($row['it_incen']); ?></li>
                                    <li class="price ">
                                        <?php if ($is_sale) { ?>
                                            <span style="text-decoration: line-through;color:#888;font-size:11px;"><?php echo number_format($row['it_cust_price']); ?></span>
                                        <?php } ?>
                                        <?php echo number_format($row['it_price']); ?>원
                                    </li>
                                <?php } else { ?>

                                    <li class="price ">
                                        <?php if ($is_sale) { ?>
                                            <span style="text-decoration: line-through;color:#888;font-size:11px;"><?php echo number_format($row['it_cust_price']); ?></span>
                                        <?php } ?>
                                        <?php echo number_format($row['it_price']); ?>원
                                    </li>
                                <?php } ?>

                                <li class="desc"><?php echo $row['it_desc']; ?></li>

                                <li class="icon">
                                    <?php if ($row['it_type1'] == 1) { ?>
                                        <img src="<?php echo G5_THEME_URL.'/img/'.G5_MOBILE_DIR; ?>/icon/icon_best.jpg" class="icon_img" alt="Best"/>
                                    <?php } ?>
                                    <?php if ($row['it_type2'] == 1) { ?>
                                        <img src="<?php echo G5_THEME_URL.'/img/'.G5_MOBILE_DIR; ?>/icon/icon_recommand.jpg" class="icon_img" alt="Best"/>
                                    <?php } ?>
                                    <?php if ($row['it_type3'] == 1) { ?>
                                        <img src="<?php echo G5_THEME_URL.'/img/'.G5_MOBILE_DIR; ?>/icon/icon_new.jpg" class="icon_img" alt="Best"/>
                                    <?php } ?>
                                </li>
                            </ul>
                        </div>
                    </li>

                <?php } ?>
            </ul>
        </div>

        <!-- 배너 -->
        <div class="dp-block2" style="margin:0px !important; margin-bottom:23px !important;">
            <ul>
                <li>
                    <a href="/shop/event.php?ev_id=1535674734">
                        <img src="<?php echo G5_THEME_URL.'/img/'.G5_MOBILE_DIR; ?>/main_banner_20190822_m.jpg?20190822" alt="">
                    </a>
                </li>
            </ul>
        </div>

        <!-- 탭 영역 -->
        <div class="tabProd" style="margin:0px !important; padding:0px !important; margin-bottom:0px !important; ">
            <div class="tab" style="margin-bottom:0px !important; padding-bottom: 0px !important; ">

                <ul id="bxslider_center_banner_auto1">
                    <div style="background-image:url(<?php echo G5_THEME_URL.'/img/'.G5_MOBILE_DIR; ?>/app_banner1_20200207.jpg); background-size: 100% 100% !important ; ">
                        <li>
                            <div
                                    class="xans-element- xans-product xans-product-listmain-2 xans-product-listmain xans-product-2 ec-base-product typeThumb tab-cont-1" style="background-color:#fffff !important; ">
                                <!-- $count = 6 $moreview = yes -->
                                <ul class="prd_list grid3" style="margin-top:38%; ">
                                    <?php
                                    $sql = " select * from {$g5['g5_shop_item_table']} where it_id in (select it_id from tbl_shop_event_item where ev_id=1580978318)";
                                    $result = sql_query($sql);

                                    for ($i = 0; $row = sql_fetch_array($result); $i++) {
                                        ?>

                                        <li class="xans-record-" style="width:30% !important;">
                                            <span class="discount_rate" data-prod-custom="<?php echo $row['it_price']; ?>" data-prod-price="<?php echo $row['it_price']; ?>"></span>
                                            <div class="thumbnail">
                                                <a href="<?php echo G5_SHOP_URL.'/item.php?it_id='.$row['it_id']; ?>">
                                                    <img src="<?php echo G5_DATA_URL.'/item/'.$row['it_img1']; ?>"
                                                         alt="<?php echo $row['it_name']; ?>"></a>

                                            </div>
                                            <div class="description">
                                                <div class="name " style="height:34px;">
                                                    <a
                                                            href="<?php echo G5_SHOP_URL.'/item.php?it_id='.$row['it_id']; ?>"><?php echo $row['it_name']; ?></a>
                                                </div>
                                                <ul class="spec">
                                                    <?php //if($member['mb_type'] == '1') {
                                                    ?>
                                                    <li class="desc" style="font-weight:normal !important;text-align:center !important; font-size:10.5px;"><?php echo $row['it_desc']; ?></li>
                                                    <?php if ($is_drug) { ?>

                                                    <li class="desc" style="font-weight:normal !important; font-size:12px; text-align:center !important;color:#000000;">
                                                        P:<?php echo number_format($row['it_incen']); ?></li>
                                                    <li class="price " style="font-weight:bolder !important; font-size:12px; text-align:center !important;color:#000000;"><?php echo number_format($row['it_price']); ?>
                                                        원
                                                        <?php } else { ?>

                                                    <li class="price " style="font-weight:bolder !important; font-size:12px; text-align:center !important;color:#000000;"><?php echo number_format($row['it_price']); ?>
                                                        원
                                                        <?php } ?>
                                                        <!--
                                            <li class="icon">
												<?php if ($row['it_type1'] == 1) { ?>
												<img src="<?php echo G5_THEME_URL.'/img/'.G5_MOBILE_DIR; ?>/icon/icon_best.jpg" class="icon_img" alt="Best"/>
												<?php } ?>
												<?php if ($row['it_type2'] == 1) { ?>
												<img src="<?php echo G5_THEME_URL.'/img/'.G5_MOBILE_DIR; ?>/icon/icon_recommand.jpg" class="icon_img" alt="Best"/>
												<?php } ?>
												<?php if ($row['it_type3'] == 1) { ?>
												<img src="<?php echo G5_THEME_URL.'/img/'.G5_MOBILE_DIR; ?>/icon/icon_new.jpg" class="icon_img" alt="Best"/>
												<?php } ?>
											</li>
											-->

                                                </ul>
                                            </div>
                                        </li>
                                    <?php } ?>

                                </ul>
                            </div>
                        </li>
                        </li>
                        </li>
                    </div>
                    <div style="background-image:url(<?php echo G5_THEME_URL.'/img/'.G5_MOBILE_DIR; ?>/app_banner2_20200207.jpg); background-size: 100% 100% !important">
                        <li>

                            <div
                                    class="xans-element- xans-product xans-product-listmain-3 xans-product-listmain xans-product-3 ec-base-product typeThumb tab-cont-2">
                                <!-- $count = 6 $moreview = yes -->
                                <ul class="prd_list grid3" style="margin-top:38%; ">
                                    <?php
                                    $sql = " select * from {$g5['g5_shop_item_table']} where it_id in (select it_id from tbl_shop_event_item where ev_id=1580978615)";
                                    $result = sql_query($sql);

                                    for ($i = 0; $row = sql_fetch_array($result); $i++) {
                                        ?>

                                        <li class="xans-record-" style="width:30% !important; ">
                                            <span class="discount_rate" data-prod-custom="<?php echo $row['it_price']; ?>" data-prod-price="<?php echo $row['it_price']; ?>"></span>
                                            <div class="thumbnail">
                                                <a href="<?php echo G5_SHOP_URL.'/item.php?it_id='.$row['it_id']; ?>">
                                                    <img src="<?php echo G5_DATA_URL.'/item/'.$row['it_img1']; ?>" alt="<?php echo $row['it_name']; ?>">
                                                </a>

                                            </div>
                                            <div class="description">
                                                <div class="name " style="height:34px;">
                                                    <a href="<?php echo G5_SHOP_URL.'/item.php?it_id='.$row['it_id']; ?>"><?php echo $row['it_name']; ?></a>
                                                </div>
                                                <ul class="spec">
                                                    <li class="desc" style="font-weight:normal !important;text-align:center !important; font-size:10.5px;"><?php echo $row['it_desc']; ?></li>
                                                    <?php //if($member['mb_type'] == '1') {
                                                    ?>
                                                    <?php if ($is_drug) { ?>

                                                    <li class="desc" style="font-weight:normal !important; font-size:12px; text-align:center !important;color:#000000;">
                                                        P:<?php echo number_format($row['it_incen']); ?></li>
                                                    <li class="price " style="font-weight:bolder !important; font-size:12px; text-align:center !important;color:#000000;"><?php echo number_format($row['it_price']); ?>
                                                        원
                                                        <?php } else { ?>

                                                    <li class="price " style="font-weight:bolder !important; font-size:12px; text-align:center !important;color:#000000;"><?php echo number_format($row['it_price']); ?>
                                                        원
                                                        <?php } ?>
                                                        <!--
                                            <li class="icon">
												<?php if ($row['it_type1'] == 1) { ?>
												<img src="<?php echo G5_THEME_URL.'/img/'.G5_MOBILE_DIR; ?>/icon/icon_best.jpg" class="icon_img" alt="Best"/>
												<?php } ?>
												<?php if ($row['it_type2'] == 1) { ?>
												<img src="<?php echo G5_THEME_URL.'/img/'.G5_MOBILE_DIR; ?>/icon/icon_recommand.jpg" class="icon_img" alt="Best"/>
												<?php } ?>
												<?php if ($row['it_type3'] == 1) { ?>
												<img src="<?php echo G5_THEME_URL.'/img/'.G5_MOBILE_DIR; ?>/icon/icon_new.jpg" class="icon_img" alt="Best"/>
												<?php } ?>
											</li>
											-->

                                                </ul>
                                            </div>
                                        </li>
                                    <?php } ?>
                                </ul>
                            </div>
                        </li>
                        </li>
                        </li>
                    </div>
                    <div style="background-image:url(<?php echo G5_THEME_URL.'/img/'.G5_MOBILE_DIR; ?>/app_banner3_20200207.jpg); background-size: 100% 100% !important">
                        <li>
                            <div
                                    class="xans-element- xans-product xans-product-listmain-4 xans-product-listmain xans-product-4 ec-base-product typeThumb tab-cont-3">
                                <!-- $count = 6 $moreview = yes -->
                                <ul class="prd_list grid3" style="margin-top:38%; ">
                                    <?php
                                    $sql = " select * from {$g5['g5_shop_item_table']} where it_id in (select it_id from tbl_shop_event_item where ev_id=1580978394)";
                                    $result = sql_query($sql);

                                    for ($i = 0; $row = sql_fetch_array($result); $i++) {
                                        ?>

                                        <li class="xans-record-" style="width:30% !important; ">
                                            <span class="discount_rate" data-prod-custom="<?php echo $row['it_price']; ?>" data-prod-price="<?php echo $row['it_price']; ?>"></span>
                                            <div class="thumbnail">
                                                <a href="<?php echo G5_SHOP_URL.'/item.php?it_id='.$row['it_id']; ?>">
                                                    <img src="<?php echo G5_DATA_URL.'/item/'.$row['it_img1']; ?>" alt="<?php echo $row['it_name']; ?>">
                                                </a>

                                            </div>
                                            <div class="description">
                                                <div class="name " style="height:34px;">
                                                    <a href="<?php echo G5_SHOP_URL.'/item.php?it_id='.$row['it_id']; ?>"><?php echo $row['it_name']; ?></a>
                                                </div>
                                                <ul class="spec">
                                                    <li class="desc" style="font-weight:normal !important;text-align:center !important; font-size:10.5px;"><?php echo $row['it_desc']; ?></li>
                                                    <?php //if($member['mb_type'] == '1') {
                                                    ?>
                                                    <?php if ($is_drug) { ?>

                                                    <li class="desc" style="font-weight:normal !important; font-size:12px; text-align:center !important;color:#000000;">
                                                        P:<?php echo number_format($row['it_incen']); ?></li>
                                                    <li class="price " style="font-weight:bolder !important; font-size:12px; text-align:center !important;color:#000000;"><?php echo number_format($row['it_price']); ?>
                                                        원
                                                        <?php } else { ?>

                                                    <li class="price " style="font-weight:bolder !important; font-size:12px; text-align:center !important;color:#000000;"><?php echo number_format($row['it_price']); ?>
                                                        원
                                                        <?php } ?>
                                                        <!--
                                            <li class="icon">
												<?php if ($row['it_type1'] == 1) { ?>
												<img src="<?php echo G5_THEME_URL.'/img/'.G5_MOBILE_DIR; ?>/icon/icon_best.jpg" class="icon_img" alt="Best"/>
												<?php } ?>
												<?php if ($row['it_type2'] == 1) { ?>
												<img src="<?php echo G5_THEME_URL.'/img/'.G5_MOBILE_DIR; ?>/icon/icon_recommand.jpg" class="icon_img" alt="Best"/>
												<?php } ?>
												<?php if ($row['it_type3'] == 1) { ?>
												<img src="<?php echo G5_THEME_URL.'/img/'.G5_MOBILE_DIR; ?>/icon/icon_new.jpg" class="icon_img" alt="Best"/>
												<?php } ?>
											</li>
											-->

                                                </ul>
                                            </div>
                                        </li>
                                    <?php } ?>
                                </ul>
                            </div>
                        </li>
                        </li>
                        </li>
                    </div>
                </ul>
            </div>
        </div>

        <!-- 배너 -->
        <div class="dp-block3" style="margin:0px !important; margin-top:22px !important;">
            <div class="box_top">
                <ul id="bxslider3">
                    <li class="item">
                        <a href="/shop/event.php?ev_id=1579238601" target="_self">
                            <img src="<?php echo G5_THEME_URL.'/img/'.G5_MOBILE_DIR; ?>/brand_app_img1.jpg"></a>
                    </li>
                    <li class="item">
                        <a href="/shop/event.php?ev_id=1579238666" target="_self">
                            <img src="<?php echo G5_THEME_URL.'/img/'.G5_MOBILE_DIR; ?>/brand_app_img2.jpg"></a>
                    </li>
                </ul>
            </div>
            <div class="box_btm">
                <ul>
                    <li>
                        <a href="/shop/event.php?ev_id=1579237919" target="_self">
                            <img src="<?php echo G5_THEME_URL.'/img/'.G5_MOBILE_DIR; ?>/brand_app_img3.jpg"></a>
                    </li>
                    <li>
                        <a href="/shop/event.php?ev_id=1579238878" target="_self">
                            <img src="<?php echo G5_THEME_URL.'/img/'.G5_MOBILE_DIR; ?>/brand_app_img4.jpg"></a>
                    </li>
                </ul>
            </div>
        </div>

        <!-- 최저가딜 -->
        <div class=" product-b swiper-container xans-element- xans-product xans-product-listmain-5 xans-product-listmain xans-product-5 ec-base-product typeGallery">

            <h2>MD추천
                <span>오더스테이션 MD가 자신있게 추천합니다.
                </span>
            </h2>

            <?
            $InputRows = array(array());

            // 총 갯수 구하기
            if ($member['mb_type'] == '1' or $member['mb_type'] == '9') {
                $sql = "
								SELECT COUNT(it_id) AS tot_cnt FROM(
									select it_id from tbl_shop_item where it_main2='1' and it_use = '1' and it_web_view=1 LIMIT 80
								) AS x				
							";
            } else {
                $sql = "
								SELECT COUNT(it_id) AS tot_cnt FROM(
									select it_id from tbl_shop_item where it_main2='1' and it_use = '1' and it_app_view=1 LIMIT 80
								) AS x				
							";
            }
            $result = sql_fetch($sql);

            $tot_cnt = $result["tot_cnt"];
            $loop_cnt = ceil($tot_cnt / 8);

            //echo "test -> ".$tot_cnt;

            // row list 구하기
            if ($member['mb_type'] == '1' or $member['mb_type'] == '9') {
                $sql = " select * from {$g5['g5_shop_item_table']} where it_main2='1' and it_use = '1' and it_web_view=1 order by it_id asc LIMIT 80 ";
            } else {
                $sql = " select * from {$g5['g5_shop_item_table']} where it_main2='1' and it_use = '1' and it_app_view=1 order by it_id asc LIMIT 80 ";
            }
            $array_result = sql_query($sql);
            // 결과가 있으면 배열에 넣기
            if ($array_result) {
                for ($i = 0; $array_row = sql_fetch_array($array_result); $i++) {

                    //$InputRows[] = $array_row;
                    array_push($InputRows, $array_row);

                }
            }

            ?>
            <div class="swiper-wrapper">
                <?
                //총 페이수만큼 돌린다.
                for ($k = 1; $k <= $loop_cnt; $k++) {
                    ?>
                    <div class="swiper-slide">

                        <ul class="prdList grid2">
                            <?php

                            //첫페이지 fot문 조건
                            if ($k == 1) {
                                $array_start_number = 1;
                                $array_end_number = 8;
                            }
                            //2페이지이상 fot문 조건
                            if ($k > 1) {
                                $array_start_number = (8 * $k) - 7;
                                $array_end_number = $array_start_number + 7;
                            }

                            //echo "test -> " . $array_start_number . "/" . $array_end_number;

                            //페이지 조건만큼 돌리기
                            for ($i = $array_start_number; $i <= $array_end_number; $i++) {

                                if ($InputRows[$i]['it_cust_price'] > 0) {
                                    $is_sale = true;
                                } else {
                                    $is_sale = false;
                                }

                                ?>
                                <li class="xans-record-">
                                    <?php if ($is_sale) { ?>
                                        <span class="discount_rate " data-prod-custom="<?php echo $InputRows[$i]['it_cust_price']; ?>" data-prod-price="<?php echo $InputRows[$i]['it_price']; ?>"></span>

                                        <?php if ($InputRows[$i]['it_sc_type'] == "1") { //세일상품이며 무료배송이면 할인율 옆에 배치?>
                                            <span class="free_trans_md">무료배송</span>
                                            <?php if ($InputRows[$i]['it_multisend'] == "1" && $is_drug) { ?>
                                                <span class="multi_trans_2">다중배송</span>
                                            <?php }
                                        } else {
                                            if ($InputRows[$i]['it_multisend'] == "1" && $is_drug) {
                                                ?>
                                                <span class="multi_trans_1">다중배송</span>
                                            <?php }
                                        }
                                    } else {
                                        if ($InputRows[$i]['it_sc_type'] == "1") { ?>
                                            <span class="free_trans_new">무료배송</span>
                                            <?php if ($InputRows[$i]['it_multisend'] == "1" && $is_drug) { ?>
                                                <span class="multi_trans_1">다중배송</span>
                                            <?
                                            }
                                        } else {
                                            if ($InputRows[$i]['it_multisend'] == "1" && $is_drug) {
                                                ?>
                                                <span class="multi_trans_0">다중배송</span>
                                            <?php }
                                        }
                                    } ?>
                                    <div class="thumbnail">
                                        <a href="<?php echo G5_SHOP_URL.'/item.php?it_id='.$InputRows[$i]['it_id']; ?>">
                                            <img src="<?php echo G5_DATA_URL.'/item/'.$InputRows[$i]['it_img1']; ?>" alt="<?php echo $InputRows[$i]['it_name']; ?>">
                                        </a>

                                    </div>
                                    <div class="description">
                                        <div class="name ">
                                            <a href="<?php echo G5_SHOP_URL.'/item.php?it_id='.$InputRows[$i]['it_id']; ?>"><?php echo $InputRows[$i]['it_name']; ?></a>
                                        </div>
                                        <ul class="spec">
                                            <?php //if($member['mb_type'] == '1') {
                                            ?>
                                            <?php if ($is_drug) { ?>

                                                <li class="desc">
                                                    P:<?php echo number_format($InputRows[$i]['it_incen']); ?></li>
                                                <li class="price ">
                                                    <?php if ($is_sale) { ?>
                                                        <span style="text-decoration: line-through;color:#888;font-size:11px;"><?php echo number_format($InputRows[$i]['it_cust_price']); ?></span>
                                                    <?php } ?>
                                                    <?php echo number_format($InputRows[$i]['it_price']); ?>원
                                                </li>
                                            <?php } else { ?>

                                                <li class="price ">
                                                    <?php if ($is_sale) { ?>
                                                        <span style="text-decoration: line-through;color:#888;font-size:11px;"><?php echo number_format($InputRows[$i]['it_cust_price']); ?></span>
                                                    <?php } ?>
                                                    <?php echo number_format($InputRows[$i]['it_price']); ?>원
                                                </li>
                                            <?php } ?>
                                            <li class="desc"><?php echo $InputRows[$i]['it_desc']; ?></li>

                                            <li class="icon">
                                                <?php if ($InputRows[$i]['it_type1'] == 1) { ?>
                                                    <img src="<?php echo G5_THEME_URL.'/img/'.G5_MOBILE_DIR; ?>/icon/icon_best.jpg" class="icon_img" alt="Best"/>
                                                <?php } ?>
                                                <?php if ($InputRows[$i]['it_type2'] == 1) { ?>
                                                    <img src="<?php echo G5_THEME_URL.'/img/'.G5_MOBILE_DIR; ?>/icon/icon_recommand.jpg" class="icon_img" alt="Best"/>
                                                <?php } ?>
                                                <?php if ($InputRows[$i]['it_type3'] == 1) { ?>
                                                    <img src="<?php echo G5_THEME_URL.'/img/'.G5_MOBILE_DIR; ?>/icon/icon_new.jpg" class="icon_img" alt="Best"/>
                                                <?php } ?>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                                <?php
                            }
                            ?>
                        </ul>
                    </div>
                    <?
                }
                ?>
            </div>

            <!-- <div
                class="xans-element- xans-product xans-product-listmore-5 xans-product-listmore xans-product-5 paginate typeMoreview">
                <a
                    href="#none"
                    onclick="try { $M.displayMore(5, 6, 0, 4, 0, false, 'S0000000', false, ''); } catch(e) { return false; }">
                    MORE(<span id="more_current_page_6" class="">1</span>/<span id="more_total_page_6" class="">4</span>)
                    <span class="icoMore"></span>
                </a>
            </div> -->

        </div>

        <!-- 신상품 -->
        <div
                class="xans-element- xans-product xans-product-listmain-6 xans-product-listmain xans-product-6 ec-base-product typeThumb">
            <!-- $count = 9 ※ 노출시킬 상품의 갯수를 숫자로 설정할 수 있으며, 설정하지 않을경우, 최대 200개로 자동제한됩니다. ※ 상품
            노출갯수가 많으면 쇼핑몰에 부하가 발생할 수 있습니다. $moreview = yes -->
            <h2>신상품
                <span>새로나온 상품을 소개합니다</span>
            </h2>
            <ul class="prdList grid2">
                <?php

                if ($is_drug) {
                    $sql = " select * from {$g5['g5_shop_item_table']} where it_main3='1' and it_use = '1' and it_web_view=1 order by it_id asc limit 10";
                } else {
                    $sql = " select * from {$g5['g5_shop_item_table']} where it_main3='1' and it_use = '1' and it_app_view=1 order by it_id asc limit 10";
                }
                $result = sql_query($sql);

                for ($i = 0; $row = sql_fetch_array($result); $i++) {
                    if ($row['it_cust_price'] > 0) {
                        $is_sale = true;
                    } else {
                        $is_sale = false;
                    }

                    ?>
                    <li class="xans-record-">
                        <?php if ($is_sale) { ?>
                            <span class="discount_rate " data-prod-custom="<?php echo $row['it_cust_price']; ?>" data-prod-price="<?php echo $row['it_price']; ?>"></span>


                        <?php } ?>
                        <div class="thumbnail">
                            <a href="<?php echo G5_SHOP_URL.'/item.php?it_id='.$row['it_id']; ?>">
                                <img src="<?php echo G5_DATA_URL.'/item/'.$row['it_img1']; ?>" alt="<?php echo $row['it_name']; ?>">
                            </a>

                        </div>
                        <div class="description">
                            <div class="name ">
                                <a href="<?php echo G5_SHOP_URL.'/item.php?it_id='.$row['it_id']; ?>"><?php echo $row['it_name']; ?></a>
                            </div>
                            <ul class="spec">
                                <?php //if($member['mb_type'] == '1') {
                                ?>
                                <?php if ($is_drug) { ?>

                                    <li class="desc">P:<?php echo number_format($row['it_incen']); ?></li>
                                    <li class="price ">
                                        <?php if ($is_sale) { ?>
                                            <span style="text-decoration: line-through;color:#888;font-size:11px;"><?php echo number_format($row['it_cust_price']); ?></span>
                                        <?php } ?>
                                        <?php echo number_format($row['it_price']); ?>원
                                    </li>
                                <?php } else { ?>

                                    <li class="price ">
                                        <?php if ($is_sale) { ?>
                                            <span style="text-decoration: line-through;color:#888;font-size:11px;"><?php echo number_format($row['it_cust_price']); ?></span>
                                        <?php } ?>
                                        <?php echo number_format($row['it_price']); ?>원
                                    </li>
                                <?php } ?>
                                <li class="desc"><?php echo $row['it_desc']; ?></li>

                                <li class="icon">
                                    <?php if ($row['it_type1'] == 1) { ?>
                                        <img src="<?php echo G5_THEME_URL.'/img/'.G5_MOBILE_DIR; ?>/icon/icon_best.jpg" class="icon_img" alt="Best"/>
                                    <?php } ?>
                                    <?php if ($row['it_type2'] == 1) { ?>
                                        <img src="<?php echo G5_THEME_URL.'/img/'.G5_MOBILE_DIR; ?>/icon/icon_recommand.jpg" class="icon_img" alt="Best"/>
                                    <?php } ?>
                                    <?php if ($row['it_type3'] == 1) { ?>
                                        <img src="<?php echo G5_THEME_URL.'/img/'.G5_MOBILE_DIR; ?>/icon/icon_new.jpg" class="icon_img" alt="Best"/>
                                    <?php } ?>
                                </li>
                            </ul>
                        </div>
                    </li>


                <?php } ?>


            </ul>
        </div>

        <!-- 세일상품 -->
        <div
                class="xans-element- xans-product xans-product-listmain-7 xans-product-listmain xans-product-7 ec-base-product typeThumb">
            <!-- $grid = 1 $count = 6 ※ 노출시킬 상품의 갯수를 숫자로 설정할 수 있으며, 설정하지 않을경우, 최대 200개로
            자동제한됩니다. ※ 상품 노출갯수가 많으면 쇼핑몰에 부하가 발생할 수 있습니다. $moreview = no $swipe = yes -->
            <h2>세일상품
                <span>노마진으로 데려가세요</span>
            </h2>
            <ul class="prdList grid2">
                <?php
                if ($is_drug) {
                    $sql = " select * from {$g5['g5_shop_item_table']} where it_use = '1' and it_status=3 and it_cust_price >0 and it_web_view=1  order by it_id asc limit 8";
                } else {
                    $sql = " select * from {$g5['g5_shop_item_table']} where it_use = '1' and it_status=3 and it_cust_price >0 and it_app_view=1  order by it_id asc limit 8";
                }

                $result = sql_query($sql);

                for ($i = 0; $row = sql_fetch_array($result); $i++) {
                    if ($row['it_cust_price'] > 0) {
                        $is_sale = true;
                    } else {
                        $is_sale = false;
                    }
                    ?>
                    <li class="xans-record-">
                        <?php if ($row['it_sc_type'] == "1") { ?>
                            <span class="free_trans">무료배송</span>
                        <?php } ?>
                        <?php if ($is_sale) { ?>
                            <span class="discount_rate " data-prod-custom="<?php echo $row['it_cust_price']; ?>" data-prod-price="<?php echo $row['it_price']; ?>"></span>
                        <?php } ?>
                        <div class="thumbnail">
                            <a href="<?php echo G5_SHOP_URL.'/item.php?it_id='.$row['it_id']; ?>">
                                <img src="<?php echo G5_DATA_URL.'/item/'.$row['it_img1']; ?>" alt="<?php echo $row['it_name']; ?>">
                            </a>

                        </div>
                        <div class="description">
                            <div class="name ">
                                <a href="<?php echo G5_SHOP_URL.'/item.php?it_id='.$row['it_id']; ?>"><?php echo $row['it_name']; ?></a>
                            </div>
                            <ul class="spec">
                                <?php //if($member['mb_type'] == '1') {
                                ?>
                                <?php if ($is_drug) { ?>

                                    <li class="desc">P:<?php echo number_format($row['it_incen']); ?></li>
                                    <li class="price ">
                                        <?php if ($is_sale) { ?>
                                            <span style="text-decoration: line-through;color:#888;font-size:11px;"><?php echo number_format($row['it_cust_price']); ?></span>
                                        <?php } ?>
                                        <?php echo number_format($row['it_price']); ?>원
                                    </li>
                                <?php } else { ?>

                                    <li class="price ">
                                        <?php if ($is_sale) { ?>
                                            <span style="text-decoration: line-through;color:#888;font-size:11px;"><?php echo number_format($row['it_cust_price']); ?></span>
                                        <?php } ?>
                                        <?php echo number_format($row['it_price']); ?>원
                                    </li>
                                <?php } ?>
                                <li class="desc"><?php echo $row['it_desc']; ?></li>
                                <li class="icon">
                                    <?php if ($row['it_type1'] == 1) { ?>
                                        <img src="<?php echo G5_THEME_URL.'/img/'.G5_MOBILE_DIR; ?>/icon/icon_best.jpg" class="icon_img" alt="Best"/>
                                    <?php } ?>
                                    <?php if ($row['it_type2'] == 1) { ?>
                                        <img src="<?php echo G5_THEME_URL.'/img/'.G5_MOBILE_DIR; ?>/icon/icon_recommand.jpg" class="icon_img" alt="recommand"/>
                                    <?php } ?>
                                    <?php if ($row['it_type3'] == 1) { ?>
                                        <img src="<?php echo G5_THEME_URL.'/img/'.G5_MOBILE_DIR; ?>/icon/icon_new.jpg" class="icon_img" alt="Best"/>
                                    <?php } ?>
                                </li>
                            </ul>
                        </div>
                    </li>

                <?php } ?>
            </ul>
        </div>

        <!-- 배너 -->
        <div class="dp-block4">
            <ul id="bxslider5">
                <li class="item">
                    <a href="/bbs/board.php?bo_table=health" target="_self">
                        <img src="<?php echo G5_THEME_URL.'/img/'.G5_MOBILE_DIR; ?>/m-footer01.jpg"></a>
                </li>
                <li class="item">
                    <a href="/bbs/board.php?bo_table=healthrecipe" target="_self">
                        <img src="<?php echo G5_THEME_URL.'/img/'.G5_MOBILE_DIR; ?>/m-footer02.jpg"></a>
                </li>
                <li class="item">
                    <a href="/shop/event.php?ev_id=1577414501" target="_self">
                        <img src="<?php echo G5_THEME_URL.'/img/'.G5_MOBILE_DIR; ?>/m-footer03.jpg"></a>
                </li>
            </ul>
        </div>

    </div>
    <hr class="layout">

    <script>
        /* 배너 페이지 표시 버튼 눌렀을때 정지되는 현상 막기위한 스크립트 */
        $(document).ready(function () {
            var slider = $('#bxslider5').bxSlider({
                onSliderLoad: function () {
                },
                mode: 'fade',
                slideMargin: 0,
                auto: true,
                speed: 800,
                pause: 2500,
                autoHover: false,
                adaptiveHeight: false,
                pager: true,
                onSlideAfter: function () {
                    slider.stopAuto();
                    slider.startAuto();
                }
            });

            // 추가 배너 자동재생 멈춤 현상 해결
            var bxslider_top_banner_auto1 = $('#bxslider_top_banner_auto').bxSlider({
                onSliderLoad: function () {
                },
                mode: 'fade',
                slideMargin: 0,
                auto: true,
                speed: 800,
                pause: 2500,
                autoHover: false,
                adaptiveHeight: false,
                pager: true,
                onSlideAfter: function () {
                    bxslider_top_banner_auto1.stopAuto();
                    bxslider_top_banner_auto1.startAuto();
                }
            });

            var bxslider_center_banner_auto1 = $('#bxslider_center_banner_auto1').bxSlider({
                onSliderLoad: function () {
                },
                auto: true,
                speed: 500,
                pause: 4000,
                mode: 'fade',
                autoControls: true,
                pager: true,
                minSlides: 1,
                maxSlides: 1,
                onSlideAfter: function () {
                    bxslider_center_banner_auto1.stopAuto();
                    bxslider_center_banner_auto1.startAuto();
                }
            });

        });


        jQuery(function ($) {
            var swiper = new Swiper('.product-b', {
                slidesPerView: 1,
                spaceBetween: 30,
                loop: true,
                autoplay: {delay: 4000, disableOnInteraction: false},
                pagination: {el: '.swiper-pagination', clickable: true}
            });
        });

    </script>

<?php
include_once(G5_THEME_MSHOP_PATH.'/shop.tail.php');
?>
<?php
if (!defined("_GNUBOARD_")) exit; // 개별 페이지 접근 불가

include_once(G5_THEME_PATH.'/head.sub.php');

include_once(G5_LIB_PATH.'/outlogin.lib.php');
/*
include_once(G5_LIB_PATH.'/visit.lib.php');
include_once(G5_LIB_PATH.'/connect.lib.php');
include_once(G5_LIB_PATH.'/popular.lib.php');
include_once(G5_LIB_PATH.'/latest.lib.php');
*/
$pageName = basename($_SERVER['PHP_SELF']);
$pageName = str_replace('.php', '', $pageName);
?>
<style>
    .categoryList a.view {
        padding: 0 10px 10px 10px;
    }

    .allBox {
        position: relative;
        float: left;
        width: 49%;

    }

    .left_box {
        transform: translateY(100px);
    <!-- top: 100 px;
    --> bottom: 0px;
        height: 147px;

    }

    .right_box {
        transform: translateY(100px);
        bottom: 0px;
        height: 147px;

    }

    #fontsize1 {
        font-size: 115%;
        font-weight: bold;
        line-height: 120%;
    }

    #right_cart2 {
        padding: 3px 4px 3px 3px;
        background: #FF8224;
        margin-left: 1px;
        color: #ffffff;
        font-weight: bold;
        border-radius: 50%;
        display: inline-block;
        width: 19px;

    }
</style>

<?php include(G5_MSHOP_PATH.'/terms_popup.php');?>

<div class="xans-element- xans-popup xans-popup-multipopup ">

</div>
<div id="wrap">
    <aside id="aside" class="xans-element- xans-layout xans-layout-slidepackage ">
        <a href="#none" class="btnClose">
            <img src="<?php echo G5_THEME_URL.'/img/'.G5_MOBILE_DIR; ?>/side-close.gif" width="30" alt="close"></a>


        <!-- 로그인시 -->
        <?php if ($is_member) { ?>
            </br>
            <div class="xans-element- xans-myshop xans-myshop-main myshop_ico ">


                <a href="<?php echo G5_SHOP_URL; ?>/cart.php" class="basketList">장바구니
                    <!-- <span class="xans_myshop_main_basket_cnt"><?php echo get_boxcart_datas_count(); ?></span> --></a>
                <a href="<?php echo G5_SHOP_URL; ?>/wishlist.php" class="wishList">관심상품
                    <!-- <span class="xans_myshop_main_interest_prd_cnt">0</span> --></a>
                <a href="<?php echo G5_SHOP_URL; ?>/orderinquiry.php" class="orderList">주문조회</a>
                <a href="/bbs/board.php?bo_table=notice" class="noticeList">공지사항</a>
                <a href="/shop/itemuselist.php" class="qnaList">Q&amp;A</a>
                <a href="/shop/itemuselist.php" class="reviewList">상품리뷰</a>
                <a href="<?php echo G5_SHOP_URL; ?>/mypage.php" class="myshopList">마이페이지</a>
                <a href="<?php echo G5_SHOP_URL; ?>/qalist.php" class="myshopList">1:1건강상담</a>
            </div>
        <?php } else { ?>
            <!-- 로그오프시 -->
            <div class="xans-element- xans-layout xans-layout-statelogoff toparea ">
                <div class="block">로그인하시면 멤버쉽혜택을 받으실 수 있습니다.</div>
                <div class="btnarea">
                    <br/>
                    <a href="<?php echo G5_BBS_URL; ?>/login.php?url=<?php echo $urlencode; ?>" class="btnStrong">로그인
                    </a>
                    <a href="/bbs/register.php" class="btnBasic">회원가입</a>
                </div>
            </div>
        <?php } ?>
        <div class="tab">
            <a href="#slideCateList" class="selected">카테고리</a>
            <a href="#slideBoardList">커뮤니티</a>
            <a href="#slideMyShop">마이쇼핑</a>
        </div>
        <!-- 카테고리 -->
        <div id="slideCateList" class="categoryCont">
            <!-- <h2 class="selected">카테고리<button type="button"
            class="icoCategory">펼침</button></h2> -->
            <ul class="xans-element- xans-layout xans-layout-category categoryList">
                <?php
                $result = sql_query(get_mshop_category('', 2, $member['mb_type']));
                for ($i = 0; $row = sql_fetch_array($result); $i++) {
                    ?>
                    <li id="cate<?php echo $row['ca_id']; ?>" class="xans-record-">
                        <a href="#none" class="cate" cate="?cate_no=<?php echo $row['ca_id']; ?>"><?php echo $row['ca_name']; ?></a>
                        <a href="<?php echo G5_SHOP_URL.'/list.php?ca_id='.$row['ca_id']; ?>" class="view">
                            <img src="<?php echo G5_THEME_URL.'/img/'.G5_MOBILE_DIR; ?>/side_cate_arrow.gif"></a>
                    </li>
                <?php } ?>

                <li id="cate70" class="xans-record-" style="line-height:32px; font-weight:bold; margin-left:12px;">
                    <a href="https://www.geno-p.com/">클리노믹스 검사결과지 확인</a>
                </li>

            </ul>

        </div>
        <!-- 커뮤니티 -->
        <div id="slideBoardList" class="categoryCont">
            <ul class="xans-element- xans-layout xans-layout-boardinfo">
                <li class="xans-record-">
                    <a href="/bbs/board.php?bo_table=notice">공지사항</a>
                </li>
                <li class="xans-record-">
                    <a href="/bbs/faq.php">이용안내 FAQ</a>
                </li>
                <li class="xans-record-">
                    <a href="/shop/itemuselist.php">상품 사용후기</a>
                </li>
                <li class="xans-record-">
                    <a href="/shop/itemqalist.php">상품 Q&A</a>
                </li>
                <li class="xans-record-">
                    <a href="/bbs/board.php?bo_table=free">자유게시판</a>
                </li>
                <li class="xans-record-">
                    <a href="/bbs/board.php?bo_table=health">365건강지킴이</a>
                </li>
                <li class="xans-record-">
                    <a href="/bbs/board.php?bo_table=healthrecipe">건강 레시피</a>
                </li>
                <li class="xans-record-">
                    <a href="/bbs/board.php?bo_table=gallery">OS뉴스레터</a>
                </li>
                <?php if ($member['mb_type'] == "1" || $member['mb_type'] == "9") { ?>
                    <li class="xans-record-">
                        <a href="/shop/itemuselist_pharm.php">약사님 상품 사용후기</a>
                    </li>
                <?php } ?>
            </ul>
        </div>
        <!-- 마이페이지 -->
        <div id="slideMyShop" class="categoryCont">
            <ul class="xans-element- xans-myshop xans-myshop-main ">
                <li class="">
                    <a href="<?php echo G5_SHOP_URL; ?>/orderinquiry.php">주문조회</a>
                </li>
                <li class="">
                    <a href="<?php echo G5_SHOP_URL; ?>/myauto.php">정기주문</a>
                </li>
                <li class="">
                    <a href="/bbs/member_confirm.php?url=register_form.php">회원정보</a>
                </li>

                <li class="">
                    <a href="<?php echo G5_SHOP_URL; ?>/wishlist.php">관심상품
                        <span class="count displaynone">
                            <!-- (<span class="xans_myshop_main_interest_prd_cnt">0</span>) --></span>
                    </a>
                </li>

                <!-- <li class="">
                        <a href="<?php echo G5_SHOP_URL; ?>/mypage.php">적립금</a>
                    </li> -->

                <li class=" displaynone">
                    <a href="/bbs/qalist.php">1:1 상담</a>
                </li>
                <li class=" displaynone">
                    <a href="<?php echo G5_SHOP_URL; ?>/coupon.php">쿠폰
                        <span class="count displaynone">
                            <!-- (<span class="xans_myshop_main_coupon_cnt">0</span>)</span> -->
                    </a>
                </li>

            </ul>
        </div>
        <footer id="footer" class="xans-element- xans-layout xans-layout-footerpackage ">
            <div class="footerMenu">
                <div class="xans-element- xans-layout xans-layout-info left " style=" background-color:#f9f9f9;">
                    <h5>상담센터</h5>
                    <p class="tel"><img src="<?php echo G5_THEME_URL.'/img/'.G5_MOBILE_DIR; ?>/btm_ico_call.jpg" alt="">1544-5462
                    </p>
                    <p class="runtime">운영시간 : 월~금 10:00 ~ 18:00<br>
                        점심시간 : 12:00 ~ 13:00<br>
                        토, 일, 공휴일은 휴무 입니다.
                    </p>
                    <p class="btn btn-call ">
                        <a href="tel:1544-5462">전화걸기</a>
                    </p>
                </div>
                <div class="right" style=" background-color:#f9f9f9;">
                    <h5>은행계좌안내</h5>
                    <p><strong>국민은행 140101-01-008595<strong></p>
                    <p class="name">예금주 : (주)오엔케이</p>
                    <p class="btn btn-board">
                        <a href="/shop/board.php">고객센터 바로가기</a>
                    </p>
                </div>
            </div>
        </footer>
    </aside>
    <hr class="layout">
    <div id="container">

        <header id="header">
            <div class="header">

                <div id="topBanner" class="swiper-container" style="height: 0">
                    <!--<ul class="swiper-wrapper">-->
                    <ul>
                        <li class="swiper-slide">
                            <a href="https://www.orderstation.co.kr/shop/event.php?ev_id=1615782731" target="_self">
                                <img src="<?php echo G5_THEME_URL.'/img/'.G5_MOBILE_DIR; ?>/app_top.jpg?ver=<?php echo G5_IMG_VER?>" width="100%" alt="">
                            </a>
                        </li>
                        <!--
                            <li class="swiper-slide">
                                <a href="#none"><img src="<?php echo G5_THEME_URL.'/img/'.G5_MOBILE_DIR; ?>/top_banner_02.jpg" width="100%" alt=""></a>
                            </li>
							-->
                    </ul>
                    <!--
                    <div class="swiper-pagination"></div>
                    -->
                    <div class="close">
                       <!-- <span class="line1"></span>
                        <span class="line2"></span>-->
                    </div>
                </div>


                <section id="topArea">
                    <nav class="xans-element- xans-layout xans-layout-mobilenavigation ">
                        <?php if ($pageName != 'index') { ?>
                            <div style="float:left;padding:4px;">
                                <a href="#none" onclick="history.go(-1);return false;">
                                    <img src="<?php echo G5_THEME_URL; ?>/img/mobile/btn_back.gif" width="28" alt="뒤로가기">
                                </a>
                            </div>
                        <?php } ?>
                        <ul>
                            <li>
                            </li>
                            <?php if ($is_member) { ?>
                                <?php if ($is_admin) { ?>
                                    <li>
                                        <a href="<?php echo G5_ADMIN_URL ?>/shop_admin/"><b>관리자</b></a>
                                    </li>
                                <?php } else { ?>
                                    <li>
                                        <a href="<?php echo G5_BBS_URL; ?>/member_confirm.php?url=register_form.php">
                                            정보수정
                                        </a>
                                    </li>
                                <?php } ?>

                                <!-- 로그인 후--->
                                <li>
                                    <? set_cart_id($sw_direct);
                                    ?>
                                    <a href="<?php echo G5_SHOP_URL; ?>/cart.php">장바구니</a>
                                    <span id="right_cart2"><?=get_cart_count(get_session('ss_cart_id'));?></span>
                                </li>

                                <li>
                                    <a href="<?php echo G5_SHOP_URL; ?>/mypage.php">마이페이지</a>
                                </li>

                                <li>
                                    <a href="<?php echo G5_BBS_URL; ?>/logout.php?url=shop">로그아웃</a>
                                </li>

                            <?php } else { ?>
                                <!-- 로그인 전--->
                                <li class="xans-element- xans-layout xans-layout-statelogoff ">
                                    <a href="<?php echo G5_BBS_URL; ?>/login.php?url=<?php echo $urlencode; ?>">로그인</a>
                                </li>
                                <li class="xans-element- xans-layout xans-layout-statelogoff ">
                                    <a href="<?php echo G5_SHOP_URL; ?>/orderinquiry.php?url=<?php echo $urlencode; ?>" class="login">
                                        주문조회
                                    </a>
                                </li>
                                <li class="xans-element- xans-layout xans-layout-statelogoff point_wrap ">
                                    <a href="<?php echo G5_BBS_URL ?>/register.php">회원가입</a>
                                    <span class="point">
                                        <img src="<?php echo G5_THEME_URL.'/img/'.G5_MOBILE_DIR; ?>/top_icon_point.png" alt="">
                                    </span>
                                </li>
                                <li>
                                    <a href="<?php echo G5_SHOP_URL; ?>/mypage.php">마이페이지</a>
                                </li>
                                <li>
                                    <a href="<?php echo G5_SHOP_URL; ?>/board.php">고객센터</a>
                                </li>

                            <?php } ?>


                        </ul>
                    </nav>
                    <form id="searchBarForm">
                        <!--<input id="banner_action" name="banner_action" value="<?php echo stripslashes(get_text(get_search_string($q))); ?>"  type="hidden"/>-->
                        <div
                                id="search_box"
                                class="xans-element- xans-layout xans-layout-searchheader searchBox ">
                            <fieldset>
                                <legend>검색</legend>
                                <div class="searchForm">
                                    <input id="keyword" name="q" class="inputTypeText"
                                           value="<?php echo stripslashes(get_text(get_search_string($q))); ?>"
                                           type="text" autocomplete="off" maxlength="20"/>
                                    <button type="button" class="btnDelete" id="searchKeyDelete">삭제</button>
                                </div>
                            </fieldset>
                            <div class="tab">
                                <ul class="searchTab displaynone">
                                    <li class="selected">
                                        <a href="#none">인기 검색어</a>
                                    </li>
                                    <li>
                                        <a href="#none">최근 검색어</a>
                                    </li>
                                </ul>
                            </div>

                            <?
                            $rand_unique_str = $_COOKIE["rand_unique_str"];
                            $key_sql = " select k_id , k_word from tbl_search_keyword where del_yn = 'N' and 
									( ( mb_id = '{$member['mb_id']}' and mb_id != '' ) or ( rand_string = '{$rand_unique_str}' and rand_string != '' ) ) 
									order by k_word asc limit 30  ";

                            $result = sql_query($key_sql);
                            ?>
                            <div class="xans-element- xans-search xans-search-recentkeyword">
                                <? // if( $_SERVER['REMOTE_ADDR'] == "192.168.0.1" ){ ?>
                                <!--<p class="">최근 검색어가 없습니다.</p>-->
                                <ul class="" style="height:auto;margin-bottom:2px;">
                                    <? for ($i = 0; $row = sql_fetch_array($result); $i++) { ?>
                                        <li class="SearchContain" data-keyvalue="<?=$row['k_word'];?>" style="height:40px;display:none;">
                                            <a href="<?php echo G5_SHOP_URL; ?>/search.php?q=<?=$row['k_word'];?>"><?=$row['k_word'];?></a>
                                            <button type="button" class="btnSearchDelete" data-keyid="<?=$row['k_id'];?>"></button>
                                        </li>
                                    <? } ?>
                                </ul>
                                <? // } ?>
                                <div class="button">
                                    <button type="button" class="btnDeleteAll" id="SearchDeleteAll">검색기록 전체삭제</button>
                                </div>
                            </div>

                            <button type="button" class="btnClose" id="search_cancel">닫기</button>
                            <button type="button" class="btnSearch" id="btnSearch">검색</button>
                        </div>
                        <div class="dimmed" style="display: none;"></div>

                    </form>

                    <div class="headerWrap">
                        <div class="inner">
                            <p class="category">
                                <a href="#none" class="fold">카테고리</a>
                            </p>
                            <!-- <p class="xans-element- xans-layout xans-layout-mobilehomebutton bookmark ">
                                <a href="#none" id="bookmark" onclick="$H.init();">즐겨찾기추가</a>
                            </p> -->
                            <h1>
                                <a href="/shop/"><img src="<?php echo G5_THEME_URL; ?>/img/mobile/mobile_logo_new.png">
                                </a>
                            </h1>
                            <p class="xans-element- xans-layout xans-layout-orderbasketcount basket ">
                                <a href="<?php echo G5_SHOP_URL; ?>/cart.php"
                                ">장바구니
                                <span class="count displaynone">0</span>
                                </a>
                            </p>
                            <p class="search">
                                <button type="button">검색</button>
                            </p>
                        </div>
                    </div>
                </section>
            </div>
        </header>
        <hr class="layout">

<?php
if (!defined("_GNUBOARD_")) exit; // 개별 페이지 접근 불가

$admin = get_admin("super");

// 사용자 화면 우측과 하단을 담당하는 페이지입니다.
// 우측, 하단 화면을 꾸미려면 이 파일을 수정합니다.
?>
		
		<footer id="footer" class="xans-element- xans-layout xans-layout-footerpackage ">
                <div class="footerMenu">
                    <div class="xans-element- xans-layout xans-layout-info left">
                        <h5>상담센터</h5>
                        <p class="tel "><img src="<?php echo G5_THEME_URL.'/img/'.G5_MOBILE_DIR;?>/btm_ico_call.jpg" alt="">1544-5462</p>
                        <p class="runtime">운영시간 : 월~금 10:00 ~ 18:00<br>
                            점심시간 : 12:00 ~ 13:00<br>
                            토, 일, 공휴일은 휴무 입니다.</p>
                        <p class="btn btn-call ">
                            <a href="tel:1544-5462">전화걸기</a>
                        </p>
                    </div>
                    <div class="right">
                        <h5>은행계좌안내</h5>
                        <p class="banknum">국민은행 140101-01-008595</p>
                        <!-- <p class="banknum">하나은행 000-000000-00000</p>
                        <p class="banknum">신한은행 000-000000-00000</p>
                        <p class="banknum">우리은행 000-000000-00000</p> -->
                        <p class="name"></p>
                        <p class="btn btn-board">
                            <a href="/shop/board.php">고객센터 바로가기</a>
                        </p>
                    </div>
                </div>
                <div class="util">
                    <ul>
                        <li>
                            <a href="/bbs/board.php?bo_table=notice" class="notice"><img src="<?php echo G5_THEME_URL.'/img/'.G5_MOBILE_DIR;?>/btm_menu_1.gif" alt=""><br>공지사항</a>
                        </li>
                        <li>
                            <a href="<?php echo G5_SHOP_URL; ?>/mypage.php"><img src="<?php echo G5_THEME_URL.'/img/'.G5_MOBILE_DIR;?>/side_my_list.png" alt=""><br>마이페이지</a>
							 
                        </li>
                        <li>
                            <a href="/shop/itemuselist.php"><img src="<?php echo G5_THEME_URL.'/img/'.G5_MOBILE_DIR;?>/btm_menu_3.gif" alt=""><br>상품리뷰</a>
                        </li>
                        <li>
                            <a href="/shop/itemqalist.php"><img src="<?php echo G5_THEME_URL.'/img/'.G5_MOBILE_DIR;?>/btm_menu_4.gif" alt=""><br>상품 Q/A</a>
                        </li>
                    </ul>
                    <ul>
                        <li>
                            <a href="<?php echo G5_SHOP_URL; ?>/cart.php"><img src="<?php echo G5_THEME_URL.'/img/'.G5_MOBILE_DIR;?>/btm_menu_5.gif" alt=""><br>장바구니</a>
                        </li>
                        <li>
                            <a href="<?php echo G5_SHOP_URL; ?>/orderinquiry.php"><img src="<?php echo G5_THEME_URL.'/img/'.G5_MOBILE_DIR;?>/btm_menu_6.gif" alt=""><br>주문내역</a>
                        </li>
                        <li>
                            <a href="<?php echo G5_SHOP_URL; ?>/wishlist.php"><img src="<?php echo G5_THEME_URL.'/img/'.G5_MOBILE_DIR;?>/btm_menu_7.gif" alt=""><br>관심상품</a>
                        </li>
                        <li>
                            <a href="<?php echo G5_SHOP_URL; ?>/orderinquiry.php"><img src="<?php echo G5_THEME_URL.'/img/'.G5_MOBILE_DIR;?>/btm_menu_8.gif" alt=""><br>배송조회</a>
                        </li>
                    </ul>
                </div>
                <ul class="policy">
					<!-- <li>
                        <a href="/link/os.apk">모바일앱다운</a>
                    </li> -->
                    <li>
                        <a href="<?php echo G5_BBS_URL; ?>/content.php?co_id=company">회사소개</a>
                    </li>
                    <li>
                        <a href="<?php echo G5_BBS_URL; ?>/content.php?co_id=provision">이용약관</a>
                    </li>
                    <li>
                        <a href="<?php echo G5_BBS_URL; ?>/content.php?co_id=privacy">
                            <strong>개인정보취급방침</strong>
                        </a>
                    </li>
                    <!-- <li>
                        <a href="shopinfo/guide.html">이용안내</a>
                    </li> -->
                    <li>
                        <?php
							if(G5_DEVICE_BUTTON_DISPLAY && G5_IS_MOBILE) { ?>
							<a href="<?php echo get_device_change_url(); ?>" id="device_change">PC 버전</a>
							<?php
							}

							if ($config['cf_analytics']) {
								echo $config['cf_analytics'];
							}
							?>
                    </li>
                </ul>

                <address class="xans-element- xans-layout xans-layout-footer ">
                    <div class="footerInfo">
                        <div class="info" style="line-height: 18px;">
                            <span>상점명 : <?php echo $default['de_admin_company_name']; ?></span>
                            |
                            <span>대표 : <?php echo $default['de_admin_company_owner']; ?></span>
                            |
                            <span>대표전화 : <?php echo $default['de_admin_company_tel']; ?></span>
                            <br>
                            <span>주소 : <?php echo $default['de_admin_company_addr']; ?></span>
                            <br>
                            <span>사업자등록번호 :<?php echo $default['de_admin_company_saupja_no']; ?></span>
                            &nbsp;<button onclick="bizCommPop();" style="background: #d9d9d9;text-decoration: none;vertical-align: middle;border-radius: 5px;">사업자정보확인</button>
                            <br>
                            <span>통신판매업신고 :<?php echo $default['de_admin_tongsin_no']; ?></span>
                            <br>
                            <span>건강기능식품판매업신고 : <?php echo $default['de_admin_health_food_no']; ?></span>
                            <br>
                            <span>개인정보관리책임 : <?php echo $default['de_admin_info_name']; ?></span>
                            |
                            <span>이메일 :
                                <a href="mailto:master@orderstation.kr">master@orderstation.kr</a>
                            </span>
                            <br>
                            
                        </div>
                    </div>
                </address>
                <div class="xans-element- xans-layout xans-layout-footer copyright ">
                    Copyright &copy; 2001-2013 <?php echo $default['de_admin_company_name']; ?>. All Rights Reserved.

                   
                </div>
            </footer>
            <!-- <div class="side_banner">
                <p>
                    <a href="http://talk.naver.com/WCCOX4" target="_blank"><img src="<?php echo G5_THEME_URL.'/img/'.G5_MOBILE_DIR;?>/side_banner_1.jpg" alt=""></a>
                </p>
                <p>
                    <a href="http://plus.kakao.com/home/@디자인퍼블릭" target="_blank"><img src="<?php echo G5_THEME_URL.'/img/'.G5_MOBILE_DIR;?>/side_banner_2.jpg" alt=""></a>
                </p>
            </div> -->

            <!-- <div id="progressPaybar" style="display:none;">
              
                <div id="progressPaybarBackground" class="layerProgress"></div>
                
                <div id="progressPaybarView">
                    <p class="graph">현재 결제가 진행중입니다.</p>
                    <p class="txt">
                        본 결제 창은 결제완료 후 자동으로 닫히며,
                        <br>
                        결제 진행 중에 본 결제 창을 닫으시면<br>
                        주문이 되지 않으니
                        <br>
                        결제 완료 될 때 까지 닫지 마시기 바랍니다.
                    </p>
                </div>
            </div> -->

            <div class="go-top">
                <a href="javascript:page_top();">
                    <i class="fa fa-angle-up fa-2x fa-inverse" aria-hidden="true"></i>
                </a>
            </div>

            <div class="xans-element- xans-mobilequick xans-mobilequick-display "></div>
        </div>
    </div>

<script src="<?php echo G5_THEME_URL ?>/js/onk.mobile.js?ver=<?php echo G5_JS_VER; ?>3"></script>

<script>
    function bizCommPop() {
        var saupja_no = "<?php echo $default['de_admin_company_saupja_no']; ?>";
        var url = "https://www.ftc.go.kr/bizCommPop.do?wrkr_no=" + saupja_no;

		ModalPop(url,'사업자정보','Y');
        //window.open(url, "bizCommPop", "width=750, height=700;");
    }
</script>

<script>
//자동 검색어 관련
$(document).ready(function() {

	var rand_unique_str;

	rand_unique_str = get_cookie("rand_unique_str"); //쿠키값

	if( rand_unique_str == null || rand_unique_str == "" ){

	   rand_unique_str = Math.random().toString(36).substr(2, 16); //고유 ID생성
	   rand_unique_str = encodeURIComponent( rand_unique_str );
	   strParam = "rand_unique_str="+rand_unique_str;

	   $.ajax({
			type: "POST",
			url: "/mobile/shop/keyword_rand_id_search.php",
			data: strParam,
			cache: false,
			async: false,
			dataType: "text",
			success: function(data) {
				if( data == "success" ){
					set_cookie("rand_unique_str", rand_unique_str, 365, g5_cookie_domain);
				}
			}
		});

	}else{

		set_cookie("rand_unique_str", rand_unique_str, 365, g5_cookie_domain);

	}
	<? if($q != ""){ ?>
	ChangeKeyword();
	<? }else{ ?>
	AllKeyword();
	<? } ?>

	$('#keyword').bind("change keydown input", function(e) {

		var keycode = e.keyCode || e.which;
		if (keycode == 13){

			if ( $('#keyword').val().length < 2 ){

					alert("검색어는 두글자 이상 입력하십시오.");
					$('#keyword').focus();
					return false;

			}else{
				SendInsert();
				e.preventDefault();
			}

		}

	});

	$('#keyword').bind("change keyup input", function(e) {

		var keyword_val = $('#keyword').val();

		if (e.keyCode != 13 && keyword_val != ""){
			ChangeKeyword();
		}
		if (e.keyCode != 13 && keyword_val == ""){
			$('.SearchContain').css("display","");
		}

	});

	$('.btnSearchDelete').click(function() {

	   var strParam = "";
	   var return_keyid = "";
	   var ObjectM = $(this);

	   var keyid = ObjectM.data('keyid');
	   var rand_unique_str = get_cookie("rand_unique_str"); //쿠키값
	   strParam = "keyid="+encodeURIComponent(keyid)+"&rand_unique_str="+encodeURIComponent(rand_unique_str);

	   $.ajax({
			type: "POST",
			url: "/mobile/shop/keyword_del_proc.php",
			data: strParam,
			cache: false,
			async: false,
			dataType: "text",
			success: function(data) {
				if( data == "success" ){
					ObjectM.parent().remove();						
				}
			}
		});

	});

	$('#SearchDeleteAll').click(function() {

		   var user_id = encodeURIComponent( "<?=$member['mb_id']?>" );
		   var rand_unique_str = get_cookie("rand_unique_str"); //쿠키값
		   strParam = "user_id="+user_id+"&rand_unique_str="+encodeURIComponent(rand_unique_str);

		   $.ajax({
				type: "POST",
				url: "/mobile/shop/keyword_all_del_proc.php",
				data: strParam,
				cache: false,
				async: false,
				dataType: "text",
				success: function(data) {

					if( data == "success" ){
						$('.SearchContain').remove();
					}
				}
			});

	});

	$('#searchKeyDelete').click(function() {

		AllKeyword();

	});

	$('#btnSearch').click(function() {

		if ( $('#keyword').val().length < 2 ){

				alert("검색어는 두글자 이상 입력하십시오.");
				$('#keyword').focus();
				return false;

		}
		SendInsert();

	});

	$('#keyword').focusin(function(){

		 $('.SearchContain').css("display","");

	});
/*
	$('#keyword').focusout(function(){

		 $('.SearchContain').css("display","none");

	});
*/

function SendInsert(){

	   var strParam;
	   var str_keyword;
	   var q;

	   var rand_unique_str = get_cookie("rand_unique_str"); //쿠키값

	   q=$('#keyword').val();
	   str_keyword = encodeURIComponent( q );
	   strParam = "str_keyword="+str_keyword+"&rand_unique_str="+encodeURIComponent(rand_unique_str);

	   $.ajax({
			type: "POST",
			url: "/mobile/shop/keyword_insert_proc.php",
			data: strParam,
			cache: false,
			async: false,
			dataType: "text",
			success: function(data) {

				if( data == "success" ){
					location.href="/shop/search.php?q="+q;
				}

			}
		});

}

function ChangeKeyword(){

	  var SearchValue = $('#keyword').val();
	  var SearchCompare = "";
	  //console.log(SearchValue);

	  $('.SearchContain').each(function() {

			SearchCompare = $( this ).data('keyvalue');

			if( SearchCompare.indexOf( SearchValue ) != -1 && SearchValue != "" ){
				$( this ).css("display","");
			}else{
				$( this ).css("display","none");			
			}

	  });

}

function AllKeyword(){

	var SearchValue = $('#keyword').val();
	var SearchCompare = "";
	 
	$('#keyword').focusin(function(){
		 $('.SearchContain').css("display","");
	});
	/*
	$('#keyword').focusout(function(){
		 $('.SearchContain').css("display","none");
	});
	*/

}

});


</script>

<?php
$sec = get_microtime() - $begin_time;
$file = $_SERVER['SCRIPT_NAME'];

if ($config['cf_analytics']) {
    echo $config['cf_analytics'];
}
?>

<script src="<?php echo G5_JS_URL; ?>/sns.js"></script>

<?php
include_once(G5_THEME_PATH.'/tail.sub.php');
?>

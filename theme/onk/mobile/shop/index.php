<?php
include_once('./_common.php');

define("_INDEX_", TRUE);


//이즈브레 회원이 로그인시 , 추천약국정보가 ONK00 면 팝업 띄워주기.
if( $member['mb_recommend'] == "ONK00" ){
?>

	<style>
		.test_layer_wrap {position: fixed;top: 25%; left:50%; transform:translate(-50%, -0%);  z-index: 999;}
        .test_layer_wrap img {width: 300px; height: 378px;}
	</style>
    <div class="test_layer_wrap" id="pop_layer">
        <div class="dimBg"></div>
        <img src="<?php echo G5_THEME_URL; ?>/img/main_pop_recom.png" alt="" usemap="#pop_layer_map">
        <map name="pop_layer_map" id="pop_layer_map">
            <area shape="rect" coords="269,9,288,30"  onclick="javascript:document.getElementById('pop_layer').style.display='none';" alt="" onfocus=this.blur()>
            <area shape="rect" coords="44,297,257,335" href="<?php echo G5_SHOP_URL.'/mypage.php'; ?>" alt="" onfocus=this.blur()>
        </map>
    </div>

<?

} //약국지정팝업종료 

include_once(G5_THEME_MSHOP_PATH.'/shop.head.php');

?>

<!-- <script src="<?php echo G5_JS_URL; ?>/swipe.js"></script>
<script src="<?php echo G5_JS_URL; ?>/shop.mobile.main.js"></script> -->

<?php //echo display_banner('메인', 'mainbanner.10.skin.php'); ?>
<?php //echo display_banner('왼쪽', 'boxbanner.skin.php'); ?>


    <div id="contents">

        <div moduel="layout_logincheck">
            <!-- // 변수에 주소가 있는경우 입력한 주소로이동 $url = /member/login.html -->
        </div>

        <!-- 메인슬라이드 -->
        <div class="mainslide swiper-container">
            <ul id="bxslider_top_banner_auto">
                <?php
                $main_list = main_banner('메인');
                for ($i = 0; $row = sql_fetch_array($main_list); $i++) { ?>
                    <li class="item">
                        <a href="<?php echo G5_SHOP_URL.'/bannerhit.php?bn_id='.$row['bn_id']; ?>"><img
                                    src="<?php echo G5_DATA_URL.'/banner/'.$row['bn_id']; ?>" width="100%" alt=""></a>
                    </li>
                <?php } ?>
            </ul>
        </div>

        <div class="nav">
            <div class="nav">
                <ul class="grid3">
                    <li class="color">
                        <a href="<?php echo G5_SHOP_URL; ?>/listtype.php?type=1"><img
                                    src="<?php echo G5_THEME_URL.'/img/'.G5_MOBILE_DIR; ?>/top_menu_icon_1.jpg" alt="">베스트상품</a>
                    </li>
                    <li>
                        <a href="<?php echo G5_SHOP_URL; ?>/listtype.php?type=3"><img
                                    src="<?php echo G5_THEME_URL.'/img/'.G5_MOBILE_DIR; ?>/top_menu_icon_2.jpg" alt="">신상품</a>
                    </li>
                    <li>
                        <a href="<?php echo G5_SHOP_URL; ?>/listtype.php?type=2"><img
                                    src="<?php echo G5_THEME_URL.'/img/'.G5_MOBILE_DIR; ?>/top_menu_icon_3.jpg" alt="">MD추천상품</a>
                    </li>
                    <li>
                        <a href="/shop/eventbanner.php"><img
                                    src="<?php echo G5_THEME_URL.'/img/'.G5_MOBILE_DIR; ?>/top_menu_icon_6.jpg" alt="">이벤트</a>
                    </li>
                    <li>
                        <a href="/shop/health.php"><img
                                    src="<?php echo G5_THEME_URL.'/img/'.G5_MOBILE_DIR; ?>/top_menu_icon_4.jpg" alt="">건강지킴이</a>
                    </li>

                    <li>
                        <a href="/shop/board.php"><img
                                    src="<?php echo G5_THEME_URL.'/img/'.G5_MOBILE_DIR; ?>/top_menu_icon_5.jpg" alt="">고객센터</a>
                    </li>
                </ul>
            </div>
        </div>

        <!-- 배너 -->
        <div class="dp-block2" style="margin:0px !important; margin-bottom:23px !important;">
            <ul>
                <li>
                    <a href="/shop/event.php?ev_id=1621471696">
                        <img src="<?php echo G5_THEME_URL.'/img/'.G5_MOBILE_DIR; ?>/main_banner_hotdeal.jpg?date=20210525" alt="">
                    </a>
                </li>
            </ul>
        </div>
        <!-- //배너 -->

        <!-- 베스트상품 -->
        <div class="xans-element- xans-product xans-product-listmain-1 xans-product-listmain xans-product-1 ec-base-product typeThumb">

            <h2>베스트상품</h2>
            <ul class="prdList grid2">
                <?php
                $sql = " select * from {$g5['g5_shop_item_table']} where it_main1='1' and it_use = '1' and $it_view_where order by it_order asc limit 12";
                $result = sql_query($sql);

                for ($i = 0; $row = sql_fetch_array($result); $i++) {
                    ?>
                    <li class="xans-record-">
                        <div class="thumbnail">
                            <a href="<?php echo G5_SHOP_URL.'/item.php?it_id='.$row['it_id']; ?>">
                                <img style="max-height: 180px" src="<?php echo G5_DATA_URL.'/item/'.$row['it_img1']; ?>"
                                     alt="<?php echo $row['it_name']; ?>">
                            </a>
                            <!-- 좋아요 -->
                            <div class="btn-set btn-set-lg">
                                <button id="<?php echo $row['it_id'];?>" class="icon icon-heart <?php if (get_wishlist_my_item_check($row['it_id'])) echo 'is-active'; ?>" title="좋아요" type="button" onclick="item_wish(document.fitem, '<?php echo $row['it_id']; ?>');">
                                    <input type="hidden" id="wishValue" value="<?php echo (get_wishlist_my_item_check($row['it_id'])) ? 'd' : 'w'; ?>">
                            </div>
                        </div>
                        <div class="description" style="margin-left: 7px; margin-top:4px;">
                            <p class="thumbnail-brand-name"><?php echo $row['it_brand']?> </p>
                            <div class="name ">
                                <a href="<?php echo G5_SHOP_URL.'/item.php?it_id='.$row['it_id']; ?>"><?php echo $row['it_name']; ?></a>
                            </div>
                            <ul class="spec">
                                <?php if ($is_drug) { ?>
                                    <li class="price">
                                        <span class="price-title">약국공급가</span>
                                        <div class="pay-area">
                                            <span class="pay-price">
                                                <span class="price"><?php echo number_format($row['it_drug_price']); ?></span>
                                                <span class="unit">원</span>
                                            </span>
                                            <p class="original-price-group">
                                                <del class="original-price"><span class="price"><?php echo ($row['it_cust_price'] > 0) ? number_format($row['it_cust_price']) : number_format($row['it_price']); ?></span><span class="unit">원</span></del>
                                            </p>
                                            <span class="discount-percent" data-prod-price="<?php echo ($row['it_drug_price'] > 0) ? $row['it_drug_price'] : $row['it_drug_ori_price']; ?>"
                                                  data-prod-custom="<?php echo ($row['it_cust_price'] > 0) ? $row['it_cust_price'] : $row['it_price']; ?>"></span>
                                        </div>

                                    </li>
                                <?php } else { ?>
                                    <li class="price" style="min-height: 37px;">
                                        <div class="pay-area">
                                            <span class="pay-price">
                                                <span class="price"><?php echo number_format($row['it_price']); ?></span>
                                                <span class="unit">원</span>
                                            </span>
                                            <?php if ($row['it_cust_price']) { ?>
                                                <p class="original-price-group">
                                                    <del class="original-price"><span class="price"><?php echo number_format($row['it_cust_price']); ?></span><span class="unit">원</span></del>
                                                </p>
                                            <?php } ?>
                                            <?php if ($row['it_cust_price'] > 0) { ?>
                                                <span class="discount-percent" data-prod-custom="<?php echo $row['it_cust_price']; ?>" data-prod-price="<?php echo $row['it_price']; ?>"></span>
                                            <?php } ?>
                                        </div>
                                    </li>
                                <?php } ?>
                                <li class="desc2">
                                    <p class="deliver-info">
                                        <?php if($is_drug) { ?>
                                            <span class="info">포인트
                                            <?php if($row['it_cust_price'] >0 && $row['it_drug_ori_price']>0) {
                                                echo number_format($row['it_cust_price'] - $row['it_drug_ori_price']);
                                            } else {
                                                echo number_format($row['it_price'] - $row['it_drug_price']);
                                            }?>
                                        </span>
                                        <?php } ?>
                                        <?php if($row['it_sc_type'] == "1"){?>
                                            <span class="info">무료배송</span>
                                        <?php } else { ?>
                                            <span class="info">유료배송</span>
                                        <?php }?>
                                    </p>
                                </li>
                                <li class=" xans-record-">
                                    <div class="panel-set">
                                        <?php if ($row['it_type3'] == 1) { ?>
                                            <span class="panel panel-new">NEW</span>
                                        <?php } ?>
                                        <?php if ($row['it_type1'] == 1) { ?>
                                            <span class="panel panel-best">BEST</span>
                                        <?php } ?>
                                        <?php if ($row['it_type2'] == 1) { ?>
                                            <span class="panel panel-reco">추천상품</span>
                                        <?php } ?>
                                        <?php if ($row['it_supply_subject'] == 1) { ?>
                                            <span class="panel panel-gift">추가증정</span>
                                        <?php } ?>
                                        <?php if ($row['it_company'] != 0 && ($member['mb_referee'] == 4|| $member['mb_type'] == 9)) { ?>
                                            <span class="panel panel-company">사내배송</span>
                                        <?php } ?>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </li>

                <?php } ?>
            </ul>
        </div>

        <?php
        $sql_yoidae_chk = sql_query("select bn_id from tbl_shop_banner_trend order by bn_order asc limit 3");
        if(sql_num_rows($sql_yoidae_chk)) { ?>
        <!-- 탭 영역 -->
        <div class="tabProd" style="margin:0px !important; padding:0px !important; margin-bottom:0px !important; ">
            <div class="tab" style="margin-bottom:0px !important; padding-bottom: 0px !important; ">
                <ul id="bxslider_center_banner_auto1">
                    <?php
                    $sql_yoidae = " select * from tbl_shop_banner_trend order by bn_order asc limit 3";
                    $result_yoidae = sql_query($sql_yoidae);

                    for ($z = 1;$yoidae = sql_fetch_array($result_yoidae); $z++) {
                        switch ($z) {
                            case 1:
                                $yoidae_id = 'one';
                                break;
                            case 2:
                                $yoidae_id = 'two';
                                break;
                            case 3:
                                $yoidae_id = 'three';
                                break;
                            default:
                                $yoidae_id = '';
                                break;
                        }
                        ?>
                        <div style="background-image:url(<?php echo G5_DATA_URL.'/banner_trend/'.$yoidae['bn_mimg']; ?>); background-size: 100% 100% !important ; ">
                            <li>
                                <div class="xans-element- xans-product xans-product-listmain-2 xans-product-listmain xans-product-2 ec-base-product typeThumb tab-cont-<?php echo $z; ?>"
                                     style="background-color:#fffff !important; ">
                                    <!-- $count = 6 $moreview = yes -->
                                    <ul class="prd_list grid3" style="margin-top:38%; ">
                                        <?php
                                        $sql = " select * from {$g5['g5_shop_item_table']} where it_id in (select it_id from tbl_shop_event_item where ev_id='".$yoidae['bn_event_id']."') order by rand() limit 2";
                                        $result = sql_query($sql);
                                        for ($i = 0; $row = sql_fetch_array($result); $i++) {
                                            ?>
                                            <li class="xans-record-" style="width:49% !important;">
                                                <div class="thumbnail" style="padding: 10px;">
                                                    <a href="<?php echo G5_SHOP_URL . '/item.php?it_id=' . $row['it_id']; ?>">
                                                        <img src="<?php echo G5_DATA_URL . '/item/' . $row['it_img1']; ?>"
                                                             alt="<?php echo $row['it_name']; ?>" width="166" height="166">
                                                    </a>
                                                    <!-- 좋아요 -->
                                                    <div class="btn-set btn-set-lg">
                                                        <button id="<?php echo $row['it_id'];?>" class="icon icon-heart <?php if (get_wishlist_my_item_check($row['it_id'])) echo 'is-active'; ?>" title="좋아요" type="button" onclick="item_wish(document.fitem, '<?php echo $row['it_id']; ?>');">
                                                            <input type="hidden" id="wishValue" value="<?php echo (get_wishlist_my_item_check($row['it_id'])) ? 'd' : 'w'; ?>">
                                                    </div>
                                                </div>
                                                <div class="description">
                                                    <div class="name-2">
                                                        <a href="<?php echo G5_SHOP_URL.'/item.php?it_id='.$row['it_id']; ?>"><?php echo $row['it_name']; ?></a>
                                                    </div>
                                                    <ul class="spec">
                                                        <!--<li class="desc" style="font-weight:normal !important;text-align:center !important; font-size:10.5px;"><?php echo $row['it_desc']; ?></li>-->
                                                        <?php if ($is_drug) { ?>
                                                            <li class="price-2">
                                                                <span class="price-title-2" style="margin-top: 0px;">약국공급가</span>
                                                                <div class="pay-area">
                                                                    <span class="pay-price">
                                                                        <span class="price" style="padding-left: 0px; padding-right: 0px;"><?php echo number_format($row['it_drug_price']); ?></span>
                                                                        <span class="unit">원</span>
                                                                    </span>
                                                                    <p class="original-price-group-2">
                                                                        <del class="original-price"><span class="price" style="padding-left: 0px; padding-right: 0px;"><?php echo ($row['it_cust_price'] > 0) ? number_format($row['it_cust_price']) : number_format($row['it_price']); ?></span><span class="unit">원</span></del>
                                                                    </p>
                                                                    <span class="discount-percent" data-prod-price="<?php echo ($row['it_drug_price'] > 0) ? $row['it_drug_price'] : $row['it_drug_ori_price']; ?>"
                                                                          data-prod-custom="<?php echo ($row['it_cust_price'] > 0) ? $row['it_cust_price'] : $row['it_price']; ?>"></span>
                                                                </div>
                                                            </li>
                                                        <?php } else { ?>
                                                            <li class="price-2">
                                                                <div class="pay-area">
                                                                    <span class="pay-price">
                                                                        <span class="price" style="padding-left: 0px; padding-right: 0px;"><?php echo number_format($row['it_price']); ?></span>
                                                                        <span class="unit">원</span>
                                                                    </span>
                                                                    <?php if ($row['it_cust_price']) { ?>
                                                                        <p class="original-price-group-2">
                                                                            <del class="original-price"><span class="price" style="padding-left: 0px; padding-right: 0px;"><?php echo number_format($row['it_cust_price']); ?></span><span class="unit">원</span></del>
                                                                        </p>
                                                                    <?php } ?>
                                                                    <?php if ($row['it_cust_price'] > 0) { ?>
                                                                        <span class="discount-percent" data-prod-custom="<?php echo $row['it_cust_price']; ?>" data-prod-price="<?php echo $row['it_price']; ?>"></span>
                                                                    <?php } ?>
                                                                </div>
                                                            </li>

                                                        <?php } ?>
                                                    </ul>
                                                </div>
                                            </li>
                                        <?php } ?>
                                    </ul>
                                </div>
                            </li>
                        </div>
                    <?php } ?>
                </ul>
            </div>
        </div>
        <?php } ?>

        <!-- 배너 -->
        <div class="dp-block3" style="margin:0px !important; margin-top:22px !important;">
            <div class="box_top">
                <ul id="bxslider3">
                    <li class="item">
                        <a href="/shop/event.php?ev_id=1620884764" target="_self"><img
                                    src="<?php echo G5_THEME_URL.'/img/'.G5_MOBILE_DIR; ?>/brand_mimg_1.jpg?ver=<?php echo G5_IMG_VER; ?>"></a>
                    </li>
                    <li class="item">
                        <a href="/shop/event.php?ev_id=1620884704" target="_self"><img
                                    src="<?php echo G5_THEME_URL.'/img/'.G5_MOBILE_DIR; ?>/brand_mimg_2.jpg?ver=<?php echo G5_IMG_VER; ?>"></a>
                    </li>
                </ul>
            </div>
            <div class="box_btm">
                <ul>
                    <li>
                        <a href="/shop/event.php?ev_id=1620884600" target="_self"><img
                                    src="<?php echo G5_THEME_URL.'/img/'.G5_MOBILE_DIR; ?>/brand_mimg_3.jpg?ver=<?php echo G5_IMG_VER; ?>"></a>
                    </li>
                    <li>
                        <a href="/shop/event.php?ev_id=1620885045" target="_self"><img
                                    src="<?php echo G5_THEME_URL.'/img/'.G5_MOBILE_DIR; ?>/brand_mimg_4.jpg?ver=<?php echo G5_IMG_VER; ?>"></a>
                    </li>
                </ul>
            </div>
        </div>

        <!-- 최저가딜 -->
        <div class=" product-b swiper-container xans-element- xans-product xans-product-listmain-5 xans-product-listmain xans-product-5 ec-base-product typeGallery">

            <h2>MD추천
                <span>오더스테이션 MD가 자신있게 추천합니다.</span>
            </h2>

            <?
            $InputRows = array(array());

            // 총 갯수 구하기
            $sql = "SELECT COUNT(it_id) AS tot_cnt FROM (select it_id from tbl_shop_item where it_main2='1' and it_use = '1' and $it_view_where LIMIT 80) AS x";
            $result = sql_fetch($sql);

            $tot_cnt = $result["tot_cnt"];
            $loop_cnt = ceil($tot_cnt / 8);

            //echo "test -> ".$tot_cnt;

            // row list 구하기
            $sql = " select * from {$g5['g5_shop_item_table']} where it_main2='1' and it_use = '1' and $it_view_where order by it_id asc LIMIT 80 ";
            $array_result = sql_query($sql);
            // 결과가 있으면 배열에 넣기
            if ($array_result) {
                for ($i = 0; $array_row = sql_fetch_array($array_result); $i++) {
                    //$InputRows[] = $array_row;
                    array_push($InputRows, $array_row);
                }
            }

            ?>
            <div class="swiper-wrapper">
                <?
                //총 페이수만큼 돌린다.
                for ($k = 1; $k <= $loop_cnt; $k++) {
                    ?>
                    <div class="swiper-slide">

                        <ul class="prdList grid2">
                            <?php

                            //첫페이지 fot문 조건
                            if ($k == 1) {
                                $array_start_number = 1;
                                $array_end_number = 8;
                            }
                            //2페이지이상 fot문 조건
                            if ($k > 1) {
                                $array_start_number = (8 * $k) - 7;
                                $array_end_number = $array_start_number + 7;
                            }

                            //echo "test -> " . $array_start_number . "/" . $array_end_number;

                            //페이지 조건만큼 돌리기
                            for ($i = $array_start_number; $i <= $array_end_number; $i++) {
                                ?>
                                <li class="xans-record-">
                                    <?php if ($InputRows[$i]['it_cust_price'] > 0) { ?>
                                        <span class="discount_rate "
                                              data-prod-custom="<?php echo $InputRows[$i]['it_cust_price']; ?>"
                                              data-prod-price="<?php echo $InputRows[$i]['it_price']; ?>"></span>

                                        <?php if ($InputRows[$i]['it_sc_type'] == "1") { //세일상품이며 무료<br>배송이면 할인율 옆에 배치?>
                                            <span class="free_trans_md">무료<br>배송</span>
                                            <?php if ($InputRows[$i]['it_multisend'] == "1") { ?>
                                                <span class="multi_trans_2">다중<br>배송</span>
                                            <?php }
                                        } else {
                                            if ($InputRows[$i]['it_multisend'] == "1") {
                                                ?>
                                                <span class="multi_trans_1">다중<br>배송</span>
                                            <?php }
                                        }
                                    } else {
                                        if ($InputRows[$i]['it_sc_type'] == "1") { ?>
                                            <span class="free_trans_new">무료<br>배송</span>
                                            <?php if ($InputRows[$i]['it_multisend'] == "1") { ?>
                                                <span class="multi_trans_1">다중<br>배송</span>
                                            <?
                                            }
                                        } else {
                                            if ($InputRows[$i]['it_multisend'] == "1") {
                                                ?>
                                                <span class="multi_trans_0">다중<br>배송</span>
                                            <?php }
                                        }
                                    } ?>
                                    <div class="thumbnail">
                                        <a href="<?php echo G5_SHOP_URL.'/item.php?it_id='.$InputRows[$i]['it_id']; ?>">
                                            <img src="<?php echo G5_DATA_URL.'/item/'.$InputRows[$i]['it_img1']; ?>"
                                                 alt="<?php echo $InputRows[$i]['it_name']; ?>"></a>
                                    </div>
                                </li>
                                <?php
                            }
                            ?>
                        </ul>
                    </div>
                    <?
                }
                ?>
            </div>

            <!-- <div
                class="xans-element- xans-product xans-product-listmore-5 xans-product-listmore xans-product-5 paginate typeMoreview">
                <a
                    href="#none"
                    onclick="try { $M.displayMore(5, 6, 0, 4, 0, false, 'S0000000', false, ''); } catch(e) { return false; }">
                    MORE(<span id="more_current_page_6" class="">1</span>/<span id="more_total_page_6" class="">4</span>)
                    <span class="icoMore"></span>
                </a>
            </div> -->

        </div>

        <!-- 신상품 -->
        <div class="xans-element- xans-product xans-product-listmain-6 xans-product-listmain xans-product-6 ec-base-product typeThumb">
            <!-- $count = 9 ※ 노출시킬 상품의 갯수를 숫자로 설정할 수 있으며, 설정하지 않을경우, 최대 200개로 자동제한됩니다. ※ 상품
            노출갯수가 많으면 쇼핑몰에 부하가 발생할 수 있습니다. $moreview = yes -->
            <h2>신상품
                <span>새로나온 상품을 소개합니다</span></h2>
            <ul class="prdList grid2">
                <?php
                $sql = " select * from {$g5['g5_shop_item_table']} where it_main3='1' and it_use = '1' and $it_view_where order by it_id asc limit 10";
                $result = sql_query($sql);

                for ($i = 0; $row = sql_fetch_array($result); $i++) {
                    ?>
                    <li class="xans-record-">
                        <div class="thumbnail">
                            <a href="<?php echo G5_SHOP_URL.'/item.php?it_id='.$row['it_id']; ?>">
                                <img style="max-height: 180px" src="<?php echo G5_DATA_URL.'/item/'.$row['it_img1']; ?>"
                                     alt="<?php echo $row['it_name']; ?>">
                            </a>
                            <!-- 좋아요 -->
                            <div class="btn-set btn-set-lg">
                                <button id="<?php echo $row['it_id'];?>" class="icon icon-heart <?php if (get_wishlist_my_item_check($row['it_id'])) echo 'is-active'; ?>" title="좋아요" type="button" onclick="item_wish(document.fitem, '<?php echo $row['it_id']; ?>');">
                                    <input type="hidden" id="wishValue" value="<?php echo (get_wishlist_my_item_check($row['it_id'])) ? 'd' : 'w'; ?>">
                            </div>
                        </div>
                        <div class="description" style="margin-left: 7px; margin-top:4px;">
                            <p class="thumbnail-brand-name"><?php
                                echo $row['it_brand'];?> </p>
                            <div class="name">
                                <a href="<?php echo G5_SHOP_URL.'/item.php?it_id='.$row['it_id']; ?>"><?php echo $row['it_name']; ?></a>
                            </div>
                            <ul class="spec">
                                <?php if ($is_drug) { ?>
                                    <li class="price">
                                        <span class="price-title">약국공급가</span>
                                        <div class="pay-area">
                                            <span class="pay-price">
                                                <span class="price"><?php echo number_format($row['it_drug_price']); ?></span>
                                                <span class="unit">원</span>
                                            </span>
                                                <p class="original-price-group">
                                                <del class="original-price"><span class="price"><?php echo ($row['it_cust_price'] > 0) ? number_format($row['it_cust_price']) : number_format($row['it_price']); ?></span><span class="unit">원</span></del>
                                                </p>
                                            <span class="discount-percent" data-prod-price="<?php echo ($row['it_drug_price'] > 0) ? $row['it_drug_price'] : $row['it_drug_ori_price']; ?>"
                                                  data-prod-custom="<?php echo ($row['it_cust_price'] > 0) ? $row['it_cust_price'] : $row['it_price']; ?>"></span>
                                        </div>
                                    </li>
                                <?php } else { ?>
                                    <li class="price" style="min-height: 37px;">
                                        <div class="pay-area">
                                            <span class="pay-price">
                                                <span class="price"><?php echo number_format($row['it_price']); ?></span>
                                                <span class="unit">원</span>
                                            </span>
                                            <?php if ($row['it_cust_price']) { ?>
                                                <p class="original-price-group">
                                                    <del class="original-price"><span class="price"><?php echo number_format($row['it_cust_price']); ?></span><span class="unit">원</span></del>
                                                </p>
                                            <?php } ?>
                                            <?php if ($row['it_cust_price'] > 0) { ?>
                                                <span class="discount-percent" data-prod-custom="<?php echo $row['it_cust_price']; ?>" data-prod-price="<?php echo $row['it_price']; ?>"></span>
                                            <?php } ?>
                                        </div>
                                    </li>
                                <?php } ?>
                                <li class="desc2">
                                    <p class="deliver-info">
                                        <?php if($is_drug) { ?>
                                            <span class="info">포인트
                                            <?php if($row['it_cust_price'] >0 && $row['it_drug_ori_price']>0) {
                                                echo number_format($row['it_cust_price'] - $row['it_drug_ori_price']);
                                            } else {
                                                echo number_format($row['it_price'] - $row['it_drug_price']);
                                            }?> |
                                        </span>
                                        <?php } ?>
                                        <?php if($row['it_sc_type'] == "1"){?>
                                            <span class="info">무료배송</span>
                                        <?php } else { ?>
                                            <span class="info">유료배송</span>
                                        <?php }?>
                                    </p>
                                </li>
                                <li class=" xans-record-">
                                    <div class="panel-set">
                                        <?php if ($row['it_type3'] == 1) { ?>
                                            <span class="panel panel-new">NEW</span>
                                        <?php } ?>
                                        <?php if ($row['it_type1'] == 1) { ?>
                                            <span class="panel panel-best">BEST</span>
                                        <?php } ?>
                                        <?php if ($row['it_type2'] == 1) { ?>
                                            <span class="panel panel-reco">추천상품</span>
                                        <?php } ?>
                                        <?php if ($row['it_supply_subject'] == 1) { ?>
                                            <span class="panel panel-gift">추가증정</span>
                                        <?php } ?>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </li>


                <?php } ?>


            </ul>
        </div>

        <!-- 세일상품 -->
        <div class="xans-element- xans-product xans-product-listmain-7 xans-product-listmain xans-product-7 ec-base-product typeThumb">
            <!-- $grid = 1 $count = 6 ※ 노출시킬 상품의 갯수를 숫자로 설정할 수 있으며, 설정하지 않을경우, 최대 200개로
            자동제한됩니다. ※ 상품 노출갯수가 많으면 쇼핑몰에 부하가 발생할 수 있습니다. $moreview = no $swipe = yes -->
            <h2>세일상품
                <span>노마진으로 데려가세요</span></h2>
            <ul class="prdList grid2">
                <?php
                $sql = " select * from {$g5['g5_shop_item_table']} where it_use = '1' and it_status=3 and it_cust_price >0 and $it_view_where  order by it_id asc limit 8";
                $result = sql_query($sql);

                for ($i = 0; $row = sql_fetch_array($result); $i++) {
                    ?>
                    <li class="xans-record-">
                        <div class="thumbnail">
                            <a href="<?php echo G5_SHOP_URL.'/item.php?it_id='.$row['it_id']; ?>">
                                <img style="max-height: 180px" src="<?php echo G5_DATA_URL.'/item/'.$row['it_img1']; ?>"
                                     alt="<?php echo $row['it_name']; ?>">
                            </a>
                            <!-- 좋아요 -->
                            <div class="btn-set btn-set-lg">
                                <button id="<?php echo $row['it_id'];?>" class="icon icon-heart <?php if (get_wishlist_my_item_check($row['it_id'])) echo 'is-active'; ?>" title="좋아요" type="button" onclick="item_wish(document.fitem, '<?php echo $row['it_id']; ?>');">
                                    <input type="hidden" id="wishValue" value="<?php echo (get_wishlist_my_item_check($row['it_id'])) ? 'd' : 'w'; ?>">
                            </div>
                        </div>
                        <div class="description" style="margin-left: 7px; margin-top:4px;">
                            <p class="thumbnail-brand-name"><?php echo $row['it_brand']?> </p>
                            <div class="name ">
                                <a href="<?php echo G5_SHOP_URL.'/item.php?it_id='.$row['it_id']; ?>"><?php echo $row['it_name']; ?></a>
                            </div>
                            <ul class="spec">
                                <?php if ($is_drug) { ?>
                                    <li class="price">
                                        <span class="price-title">약국공급가</span>
                                        <div class="pay-area">
                                            <span class="pay-price">
                                                <span class="price"><?php echo number_format($row['it_drug_price']); ?></span>
                                                <span class="unit">원</span>
                                            </span>
                                            <p class="original-price-group">
                                                <del class="original-price"><span class="price"><?php echo ($row['it_cust_price'] > 0) ? number_format($row['it_cust_price']) : number_format($row['it_price']); ?></span><span class="unit">원</span></del>
                                            </p>
                                            <span class="discount-percent" data-prod-price="<?php echo ($row['it_drug_price'] > 0) ? $row['it_drug_price'] : $row['it_drug_ori_price']; ?>"
                                                  data-prod-custom="<?php echo ($row['it_cust_price'] > 0) ? $row['it_cust_price'] : $row['it_price']; ?>"></span>
                                        </div>

                                    </li>
                                <?php } else { ?>
                                    <li class="price" style="min-height: 37px;">
                                        <div class="pay-area">
                                            <span class="pay-price">
                                                <span class="price"><?php echo number_format($row['it_price']); ?></span>
                                                <span class="unit">원</span>
                                            </span>
                                            <?php if ($row['it_cust_price']) { ?>
                                                <p class="original-price-group">
                                                    <del class="original-price"><span class="price"><?php echo number_format($row['it_cust_price']); ?></span><span class="unit">원</span></del>
                                                </p>
                                            <?php } ?>
                                            <?php if ($row['it_cust_price'] > 0) { ?>
                                                <span class="discount-percent" data-prod-custom="<?php echo $row['it_cust_price']; ?>" data-prod-price="<?php echo $row['it_price']; ?>"></span>
                                            <?php } ?>
                                        </div>
                                    </li>
                                <?php } ?>
                                <li class="desc2">
                                    <p class="deliver-info">
                                        <?php if($is_drug) { ?>
                                            <span class="info">포인트
                                            <?php if($row['it_cust_price'] >0 && $row['it_drug_ori_price']>0) {
                                                echo number_format($row['it_cust_price'] - $row['it_drug_ori_price']);
                                            } else {
                                                echo number_format($row['it_price'] - $row['it_drug_price']);
                                            }?>
                                        </span>
                                        <?php } ?>
                                        <?php if($row['it_sc_type'] == "1"){?>
                                            <span class="info">무료배송</span>
                                        <?php } else { ?>
                                            <span class="info">유료배송</span>
                                        <?php }?>
                                    </p>
                                </li>
                                <li class=" xans-record-">
                                    <div class="panel-set">
                                        <?php if ($row['it_type3'] == 1) { ?>
                                            <span class="panel panel-new">NEW</span>
                                        <?php } ?>
                                        <?php if ($row['it_type1'] == 1) { ?>
                                            <span class="panel panel-best">BEST</span>
                                        <?php } ?>
                                        <?php if ($row['it_type2'] == 1) { ?>
                                            <span class="panel panel-reco">추천상품</span>
                                        <?php } ?>
                                        <?php if ($row['it_supply_subject'] == 1) { ?>
                                            <span class="panel panel-gift">추가증정</span>
                                        <?php } ?>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </li>
                <?php } ?>
            </ul>
        </div>

        <!-- 배너 -->
        <div class="dp-block4">
            <ul id="bxslider5">
                <?php
                $main_bottom_list = main_banner('메인하단');
                for ($i = 0; $row = sql_fetch_array($main_bottom_list); $i++) { ?>
                    <li class="item">
                        <?php if ($row['bn_new_win'] == 0) { ?>
                        <a href="<?php echo $row['bn_url']; ?>" target="_self">
                            <img src="<?php echo G5_DATA_URL.'/banner/'.$row['bn_id']; ?>" alt="">
                        </a>
                        <?php } else { ?>
                        <a href="javascript:;" onclick="ModalPop('<?php echo $row["bn_url"];?>','','Y');">
                            <img src="<?php echo G5_DATA_URL.'/banner/'.$row['bn_id']; ?>" alt="">
                        </a>
                        <?php } ?>
                    </li>
                <?php } ?>
            </ul>
        </div>

    </div>
    <hr class="layout">

    <script>
        /* 배너 페이지 표시 버튼 눌렀을때 정지되는 현상 막기위한 스크립트 */
        $(document).ready(function () {
            var slider = $('#bxslider5').bxSlider({
                onSliderLoad: function () {
                },
                mode: 'fade',
                slideMargin: 0,
                auto: true,
                speed: 800,
                pause: 2500,
                autoHover: false,
                adaptiveHeight: false,
                pager: true,
                onSlideAfter: function () {
                    slider.stopAuto();
                    slider.startAuto();
                }
            });

            // 추가 배너 자동재생 멈춤 현상 해결
            var bxslider_top_banner_auto1 = $('#bxslider_top_banner_auto').bxSlider({
                onSliderLoad: function () {
                },
                mode: 'fade',
                slideMargin: 0,
                auto: true,
                speed: 800,
                pause: 2500,
                autoHover: false,
                adaptiveHeight: false,
                pager: true,
                onSlideAfter: function () {
                    bxslider_top_banner_auto1.stopAuto();
                    bxslider_top_banner_auto1.startAuto();
                }
            });

            var bxslider_center_banner_auto1 = $('#bxslider_center_banner_auto1').bxSlider({
                onSliderLoad: function () {
                },
                auto: false, //true로 변경
                speed: 500,
                pause: 4000,
                mode: 'fade',
                autoControls: true,
                pager: true,
                minSlides: 1,
                maxSlides: 1,
                onSlideAfter: function () {
                    bxslider_center_banner_auto1.stopAuto();
                    bxslider_center_banner_auto1.startAuto();
                }
            });

        });


        jQuery(function ($) {
            var swiper = new Swiper('.product-b', {
                slidesPerView: 1,
                spaceBetween: 30,
                loop: true,
                autoplay: {delay: 4000, disableOnInteraction: false},
                pagination: {el: '.swiper-pagination', clickable: true}
            });
        });

        // 할인율 표시
        $('.discount-percent').each(function(){

            var custom_price = parseInt($(this).attr('data-prod-custom'));
            var prod_price = parseInt($(this).attr('data-prod-price'));
            var rate = 0;
            if (!isNaN(custom_price) && !isNaN(prod_price)) {
                rate = Math.round((custom_price - prod_price) / custom_price * 100);
            }
            $(this).text(rate + '%');

            if (rate <= 0){
                $(this).hide();
            }else{
                $(this).show();
            }
        });


        // 상품보관
        function item_wish(f, it_id) {

            var f_data = $(f).serialize();
            $.ajax({
                type: "POST",
                data: f_data + "&it_id=" + it_id + "&mode=" + document.getElementById(it_id).value,
                url: g5_url+"/shop/ajax.wishupdate.php",
                cache: false,
                success: function(result) {
                    if (result == "w") {
                        document.getElementById(it_id).className = 'icon icon-heart is-active';
                        document.getElementById(it_id).value = 'd';
                        // location.replace('/shop/wishlist.php');
                    } else if (result == "d") {
                        document.getElementById(it_id).className = 'icon icon-heart';
                        /* $(".icon-heart").removeClass('is-active');*/
                        document.getElementById(it_id).value = 'w';
                    } else {
                        alert(result);
                    }
                }
            });
        }



    </script>

<?php
include_once(G5_THEME_MSHOP_PATH.'/shop.tail.php');
?>
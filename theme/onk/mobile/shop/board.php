<?php
include_once('./_common.php');

define("_INDEX_", TRUE);

include_once(G5_THEME_MSHOP_PATH.'/shop.head.php');
?>

<!-- <script src="<?php echo G5_JS_URL; ?>/swipe.js"></script>
<script src="<?php echo G5_JS_URL; ?>/shop.mobile.main.js"></script> -->



<div class="nav">
	<ul class="grid3">
		<li class="color">
			<a href="<?php echo G5_SHOP_URL;?>/listtype.php?type=1"><img src="<?php echo G5_THEME_URL.'/img/'.G5_MOBILE_DIR;?>/top_menu_icon_1.jpg" alt="">베스트상품</a>
		</li>
		<li>
			<a href="<?php echo G5_SHOP_URL;?>/listtype.php?type=3"><img src="<?php echo G5_THEME_URL.'/img/'.G5_MOBILE_DIR;?>/top_menu_icon_2.jpg" alt="">신상품</a>
		</li>
		<li>
			<a href="<?php echo G5_SHOP_URL;?>/listtype.php?type=2"><img src="<?php echo G5_THEME_URL.'/img/'.G5_MOBILE_DIR;?>/top_menu_icon_3.jpg" alt="">MD추천상품</a>
		</li>
		<li>
			<a href="/shop/eventbanner.php"><img src="<?php echo G5_THEME_URL.'/img/'.G5_MOBILE_DIR;?>/top_menu_icon_6.jpg" alt="">이벤트</a>
		</li>
		<li>
			<a href="/shop/health.php"><img src="<?php echo G5_THEME_URL.'/img/'.G5_MOBILE_DIR;?>/top_menu_icon_4.jpg" alt="">건강지킴이</a>
		</li>

		<li>
			<a href="/shop/board.php"><img src="<?php echo G5_THEME_URL.'/img/'.G5_MOBILE_DIR;?>/top_menu_icon_5.jpg" alt="">고객센터</a>
		</li>
	</ul>
</div>



<hr class="layout">
<div id="contents">
	<div id="titleArea">
		<h2>고객센터</h2>
		<span class="xans-element- xans-layout xans-layout-mobileaction "><a href="#none" onclick="history.go(-1);return false;"><img src="<?php echo G5_THEME_URL.'/img/'.G5_MOBILE_DIR;?>/btn_back.gif" width="33" alt="뒤로가기"></a>
		</span>
	</div>
	<div class="xans-element- xans-board xans-board-mainpackage ">
		<div class="xans-element- xans-layout xans-layout-boardinfo">
			<ul>
				<li class="xans-record-"><a href="/bbs/board.php?bo_table=notice">공지사항</a></li>
				<!-- <li class="xans-record-"><a href="/bbs/board.php?bo_table=event">뉴스/이벤트</a></li> -->
				<li class="xans-record-"><a href="/bbs/faq.php">이용안내 FAQ</a></li>
				<li class="xans-record-"><a href="/shop/itemuselist.php">상품 사용후기</a></li>
				<li class="xans-record-"><a href="/shop/itemqalist.php">상품 Q&A</a></li>
			</ul>
		</div>
	</div>
</div>
<hr class="layout">

<?php
include_once(G5_THEME_MSHOP_PATH.'/shop.tail.php');
?>
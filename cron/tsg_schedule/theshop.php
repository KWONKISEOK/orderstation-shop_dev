!#/usr/bin/php

<?php
include_once('./_common.php');
include_once('../../lib/tjpharm.lib.php');

exec('python3 ../../python-admin/py/theshop_all.py > ../../python-admin/py/theshop_output.txt');

### Data Lake DB Connect ###
$host = 'onk-database-datalake-instance-1.cyy8jt4en2pr.ap-northeast-2.rds.amazonaws.com';
$db_user = 'dbadmin';
$db_pass = 'CfTW4EaffFKQbAmSaqq7';
$db_name = 'tjerp';

$datalake_db = mysqli_connect($host, $db_user, $db_pass, $db_name);
if (mysqli_connect_errno()) {
    die('Connect Error: '.mysqli_connect_error());
}
sql_set_charset('utf8', $datalake_db);

$url = G5_PATH.'/python-admin/py/theshop_output.txt';

if (!file_exists($url)) {
    tsgLogWrite('No such file or directory', $url);
    exit;
} else if (filesize($url) < 1) {
    tsgLogWrite('crawling not working', $url);
    exit;
}

$file_content = file_get_contents($url);
$data = json_decode($file_content, true);

$last = 0;
$result = array();
for ($i = 0; $i < count($data['MALL_CODE']); $i++) {
    $result[$i]['MALL_GB'] = 'D';
    $result[$i]['MALL_CODE'] = $data['MALL_CODE'][$i];
    $result[$i]['ITMNO'] = $data['ITMNO'][$i];
    $result[$i]['ITM_NM'] = $data['ITM_NM'][$i];
    $result[$i]['ITM_NM_R'] = $data['ITM_NM2'][$i];
    $result[$i]['TJ_NM'] = $data['TJ_NM'][$i];
    $result[$i]['COMP_NM1'] = $data['COMP_NM1'][$i];
    $result[$i]['COMP_NM2'] = $data['COMP_NM2'][$i];
    $result[$i]['TJ_COST'] = $data['TJ_COST'][$i];
    $result[$i]['COMP_COST1'] = $data['COMP_COST1'][$i];
    $result[$i]['COMP_COST2'] = $data['COMP_COST2'][$i];
    $result[$i]['TJ_SALE_COST'] = $data['TJ_SALE_COST'][$i];
    $result[$i]['COMP_SALE_COST1'] = $data['COMP_SALE_COST1'][$i];
    $result[$i]['COMP_SALE_COST2'] = $data['COMP_SALE_COST2'][$i];
    $result[$i]['TJ_STOCK'] = $data['TJ_STOCK'][$i];
    $result[$i]['COMP_STOCK1'] = $data['COMP_STOCK1'][$i];
    $result[$i]['COMP_STOCK2'] = $data['COMP_STOCK2'][$i];
    $last = $i;
}

/**
 * DB 적재
 */

/*************** 트랜잭션 관련 ****************/
$error_cnt = 0;
mysqli_autocommit($datalake_db, false);
/*************** 트랜잭션 관련 ****************/

sql_query('INSERT INTO TSG_MALL_INFO_BAK (SELECT * FROM TSG_MALL_INFO WHERE MALL_GB="D")', false, $datalake_db);

sql_query('DELETE FROM TSG_MALL_INFO WHERE MALL_GB="D"', false, $datalake_db);

$num = 1;
foreach ($result as $k => $row) {

    $seq = date('ymdHis').'_'.$num;
    $sql = '';

    $sql = "INSERT INTO TSG_MALL_INFO 
            SET SEQ                 = '$seq',
                MALL_GB             = '$row[MALL_GB]',
                MALL_CODE           = '$row[MALL_CODE]', 
                ITMNO               = '$row[ITMNO]',
                ITM_NM              = '$row[ITM_NM]',
                TJ_NM               = '$row[TJ_NM]',
                COMP_NM1            = '$row[COMP_NM1]',
                COMP_NM2            = '$row[COMP_NM2]',
                TJ_COST             = ".int_price($row[TJ_COST]).",
                COMP_COST1          = ".int_price($row[COMP_COST1]).",
                COMP_COST2          = ".int_price($row[COMP_COST2]).",
                TJ_SALE_COST        = ".int_price($row[TJ_SALE_COST]).",
                COMP_SALE_COST1     = ".int_price($row[COMP_SALE_COST1]).",
                COMP_SALE_COST2     = ".int_price($row[COMP_SALE_COST2]).",
                TJ_STOCK            = ".int_price($row[TJ_STOCK]).",
                COMP_STOCK1         = ".int_price($row[COMP_STOCK1]).",
                COMP_STOCK2         = ".int_price($row[COMP_STOCK2]).",
                SYSIDATE            = '".date('Y-m-d')."',
                SYSITIME            = '".date('H:i:s')."'";
    sql_query($sql, false, $datalake_db);
    /*************** 트랜잭션 관련 ****************/
    if (mysqli_errno($datalake_db)) {
        $error_cnt += 1;
        break;
    }
    /*************** 트랜잭션 관련 ****************/

    $num++;
}

/*************** 트랜잭션 관련 ****************/
if ($error_cnt > 0) {
    mysqli_rollback($datalake_db);
    mysqli_close($datalake_db);
    echo 'ERROR - DATABASE ROLLBACK';
    exit;
} else {
    echo 'SUCCESS';
    mysqli_commit($datalake_db);
}
/*************** 트랜잭션 관련 ****************/

############## 티제이팜 API START ##############
if ($error_cnt == 0 && count($result) > 0) {
    $tsg_result = tsg_price_update($result, 1);
}
############## 티제이팜 API END ##############

function int_price($value)
{

    if ($value) {
        $result = str_replace(',', '', trim($value));
    }

    if (empty($result) || strlen($result) == 0 || !is_numeric($result)) {
        return 0;
    } else {
        return $result;
    }

}

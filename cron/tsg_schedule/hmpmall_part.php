!#/usr/bin/php

<?php
include_once('./_common.php');
include_once('../../lib/tjpharm.lib.php');

exec('python3 ../../python-admin/py/hmpmall_part.py > ../../python-admin/py/hmpmall_part_output.txt');

### Data Lake DB Connect ###
$host = 'onk-database-datalake-instance-1.cyy8jt4en2pr.ap-northeast-2.rds.amazonaws.com';
$db_user = 'dbadmin';
$db_pass = 'CfTW4EaffFKQbAmSaqq7';
$db_name = 'tjerp';

$datalake_db = mysqli_connect($host, $db_user, $db_pass, $db_name);
if (mysqli_connect_errno()) {
    die('Connect Error: '.mysqli_connect_error());
}
sql_set_charset('utf8', $datalake_db);

$url = G5_PATH.'/python-admin/py/hmpmall_part_output.txt';

if (!file_exists($url)) {
    tsgLogWrite('No such file or directory', $url);
    exit;
} else if (filesize($url) < 1) {
    tsgLogWrite('crawling not working', $url);
    exit;
}

$file_content = file_get_contents($url);
$data = json_decode($file_content, true);

$last = 0;
$result = array();
for ($i = 0; $i < count($data['MALL_CODE']); $i++) {
    $result[$i]['MALL_GB'] = 'H';
    $result[$i]['MALL_CODE'] = $data['MALL_CODE'][$i];
    $result[$i]['ITMNO'] = $data['ITMNO'][$i];
    $result[$i]['ITM_NM'] = $data['ITM_NM'][$i];
    $result[$i]['ITM_NM_R'] = $data['ITM_NM2'][$i];
    $result[$i]['TJ_NM'] = $data['TJ_NM'][$i];
    $result[$i]['COMP_NM1'] = $data['COMP_NM1'][$i];
    $result[$i]['COMP_NM2'] = $data['COMP_NM2'][$i];
    $result[$i]['TJ_COST'] = $data['TJ_COST'][$i];
    $result[$i]['COMP_COST1'] = $data['COMP_COST1'][$i];
    $result[$i]['COMP_COST2'] = $data['COMP_COST2'][$i];
    $result[$i]['TJ_SALE_COST'] = $data['TJ_SALE_COST'][$i];
    $result[$i]['COMP_SALE_COST1'] = $data['COMP_SALE_COST1'][$i];
    $result[$i]['COMP_SALE_COST2'] = $data['COMP_SALE_COST2'][$i];
    $result[$i]['TJ_STOCK'] = $data['TJ_STOCK'][$i];
    $result[$i]['COMP_STOCK1'] = $data['COMP_STOCK1'][$i];
    $result[$i]['COMP_STOCK2'] = $data['COMP_STOCK2'][$i];
    $last = $i;
}


############## 티제이팜 API START ##############
if (count($result) > 0) {
    $tsg_result = tsg_price_update($result, 1);
}
############## 티제이팜 API END ##############

function int_price($value)
{

    if ($value) {
        $result = str_replace(',', '', trim($value));
    }

    if (empty($result) || strlen($result) == 0 || !is_numeric($result)) {
        return 0;
    } else {
        return $result;
    }

}

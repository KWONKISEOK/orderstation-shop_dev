<?php
/**
 * 매일 로그인 DB에 기록
 * 23시 55분 기준으로 금일 날짜의 데이터를 수집
 */

include_once('../common.php');

$today = date('Y-m-d');

$sql = "select * from tbl_today_login where wdate='$today'";
$row = @sql_fetch($sql);

if ($row) {
    // exit;
    exit;
} else {
    $sql = "select * from tbl_member where mb_today_login like '$today%'";
    $result = @sql_query($sql);
    $total_count = @sql_num_rows($result);

    $sql = "insert into tbl_today_login (wdate, cnt) values ('$today', $total_count)";
    @sql_query($sql);

    while ($row = sql_fetch_array($result)) {
        $sql = "insert into tbl_today_login_view 
                set mb_id           = '$row[mb_id]',
                    mb_name         = '$row[mb_name]',
                    mb_email        = '$row[mb_email]',
                    mb_level        = '$row[mb_level]',
                    mb_type         = '$row[mb_type]',
                    mb_sex          = '$row[mb_sex]',
                    mb_birth        = '$row[mb_birth]',
                    mb_tel          = '$row[mb_tel]',
                    mb_hp           = '$row[mb_hp]',
                    mb_today_login  = '$row[mb_today_login]',
                    mb_login_ip     = '$row[mb_login_ip]'";
        @sql_query($sql, false);
    }
}
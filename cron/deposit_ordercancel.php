<?php
include_once('./_common.php');
include_once('./cron.lib.php');
/**
 * 무통장입금 입금 기한 만료 주문 자동 취소
 * 매일 24시 실행
 */

$batch = new CronLib();

// 크론탭 실행되는 현재 시각
$now_date = G5_TIME_YMDHIS;

$sql = "
SELECT a.*, b.it_name FROM tbl_shop_order a, tbl_shop_order_detail b
WHERE 
a.od_id = b.od_id AND
od_settle_case='무통장' AND 
od_misu > 0 AND 
od_gubun=3 AND 
od_status='주문' AND
od_bank_date < '{$now_date}'
limit 1
";
$result = sql_query($sql);

$batch->sys_log('check message: ', G5_TIME_YMDHIS, 'deposit');

for ($i = 1; $od = sql_fetch_array($result); $i++) {

    $od_name = $od['od_name'];
    $od_id = $od['od_id'];
    $od_hp = $od['od_hp'];
    $cancel_price = $od['od_cart_price'];
    $od_time = $od['od_time'];
    $it_name = $od['it_name'];

    // 장바구니 자료 취소
    sql_query(" update {$g5['g5_shop_cart_table']} set ct_status = '취소' where od_id = '$od_id' ");

    // 주문 취소 처리
    $sql = " update {$g5['g5_shop_order_table']}
            set od_send_cost = '0',
                od_send_cost2 = '0',
                od_receipt_price = '0',
                od_receipt_point = '0',
                od_misu = '0',
                od_cancel_price = '$cancel_price',
                od_cart_coupon = '0',
                od_coupon = '0',
                od_send_coupon = '0',
                od_status = '취소',
                od_shop_memo = concat(od_shop_memo,\"\\미입금 주문 자동 취소 - ".G5_TIME_YMDHIS."\")
            where od_id = '$od_id' ";
    sql_query($sql);

    // 주문 상세정보 처리
    $sql = " update tbl_shop_order_receiver
            set od_status = '취소'
            where od_id = '$od_id' ";
    sql_query($sql);

    // 주문취소 회원의 포인트를 되돌려 줌
    if ($od['od_receipt_point'] > 0)
        insert_point($member['mb_id'], $od['od_receipt_point'], "주문번호 $od_id 미입금 주문 자동 취소");

    // 쿠폰사용내역
    sql_query(" delete from  {$g5['g5_shop_coupon_log_table']} where od_id = '$od_id' ");

    // 실행된 주문번호 DATA JSON ENCODING
    $od_json_arr = array(
        'od_id' => $od_id,
        'mb_id' => $od['mb_id'],
        'od_name' => $od['od_name'],
        'od_status' => $od['od_status'],
        'od_settle_case' => $od['od_settle_case'],
        'od_bank_account' => $od['od_bank_account'],
        'od_bank_date' => $od['od_bank_date'],
        'od_cart_price' => $od['od_cart_price'],
        'od_send_cost' => $od['od_send_cost'],
        'od_send_cost2' => $od['od_send_cost2'],
        'od_cart_coupon' => $od['od_cart_coupon'],
        'od_receipt_point' => $od['od_receipt_point'],
        'od_misu' => $od['od_misu']
    );

    if ($error_cnt == 0) {
        // 미입금취소 - 주문자 알림톡 발송
        include_once(G5_LIB_PATH.'/kakao.alimtalk.lib.php');
        $recv_number = preg_replace('/[^0-9]/', '', $od_hp);
        if($recv_number) {
            // AlimTalk BEGIN --------------------------------------------------------
            $alimtalk = new AlimTalk();
            $to = $alimtalk->set_phone_number($recv_number);
            $template_code = 'os_order_008';
            $text = $alimtalk->get_template($template_code);
            $text = str_replace('#{이름}', $od_name, $text);
            $text = str_replace('#{주문번호}', $od_id, $text);
            $text = str_replace('#{주문일시}', $od_time, $text);
            $text = str_replace('#{주문상품}', $it_name, $text);
            $alimtalk->set_message($template_code, $to, $text);
            $alimtalk->send();
            // AlimTalk END   --------------------------------------------------------
        }

        $batch->sys_log('success message: ', json_encode($od_json_arr), 'deposit');

    } else {
        // 오류 주문번호 로그 적재
        $batch->sys_log('error message: ', json_encode($od_json_arr), 'deposit');
    }

}
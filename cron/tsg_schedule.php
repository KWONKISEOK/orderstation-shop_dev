!#/usr/bin/php

<?php
include_once('./_common.php');

exec('python3 ../python-admin/py/theshop_all.py > ../python-admin/py/theshop_output.txt');
exec('python3 ../python-admin/py/hmpmall_all.py > ../python-admin/py/hmpmall_output.txt');

### Data Lake DB Connect ###
$host = 'onk-database-datalake-instance-1.cyy8jt4en2pr.ap-northeast-2.rds.amazonaws.com';
$db_user = 'dbadmin';
$db_pass = 'CfTW4EaffFKQbAmSaqq7';
$db_name = 'tjerp';

$datalake_db = mysqli_connect($host, $db_user, $db_pass, $db_name);
if (mysqli_connect_errno()) {
    die('Connect Error: '.mysqli_connect_error());
}
sql_set_charset('utf8', $datalake_db);

$url = G5_PATH.'/python-admin/py/theshop_output.txt';

if (!file_exists($url)) {
    echo '파일이 없습니다.';
    exit;
}

$file_content = file_get_contents($url);
$data1 = json_decode($file_content, true);

$last = 0;
$result = array();
for ($i = 0; $i < count($data1['MALL_CODE']); $i++) {
    $result[$i]['MALL_GB'] = 'D';
    $result[$i]['MALL_CODE'] = $data1['MALL_CODE'][$i];
    $result[$i]['ITMNO'] = $data1['ITMNO'][$i];
    $result[$i]['ITM_NM'] = $data1['ITM_NM'][$i];
    $result[$i]['ITM_NM_R'] = $data1['ITM_NM2'][$i];
    $result[$i]['TJ_NM'] = $data1['TJ_NM'][$i];
    $result[$i]['COMP_NM1'] = $data1['COMP_NM1'][$i];
    $result[$i]['COMP_NM2'] = $data1['COMP_NM2'][$i];
    $result[$i]['TJ_COST'] = $data1['TJ_COST'][$i];
    $result[$i]['COMP_COST1'] = $data1['COMP_COST1'][$i];
    $result[$i]['COMP_COST2'] = $data1['COMP_COST2'][$i];
    $result[$i]['TJ_SALE_COST'] = $data1['TJ_SALE_COST'][$i];
    $result[$i]['COMP_SALE_COST1'] = $data1['COMP_SALE_COST1'][$i];
    $result[$i]['COMP_SALE_COST2'] = $data1['COMP_SALE_COST2'][$i];
    $result[$i]['TJ_STOCK'] = $data1['TJ_STOCK'][$i];
    $result[$i]['COMP_STOCK1'] = $data1['COMP_STOCK1'][$i];
    $result[$i]['COMP_STOCK2'] = $data1['COMP_STOCK2'][$i];
    $last = $i;
}

//debug2($result);

$url = G5_PATH.'/python-admin/py/hmpmall_output.txt';

if (!file_exists($url)) {
    echo '파일이 없습니다.';
    exit;
}

$file_content = file_get_contents($url);
$data2 = json_decode($file_content, true);

for ($j = $last + 1; $j < count($data2['MALL_CODE']); $j++) {
    $result[$j]['MALL_GB'] = 'H';
    $result[$j]['MALL_CODE'] = $data2['MALL_CODE'][$j];
    $result[$j]['ITMNO'] = $data2['ITMNO'][$j];
    $result[$j]['ITM_NM'] = $data2['ITM_NM'][$j];
    $result[$j]['ITM_NM_R'] = $data2['ITM_NM2'][$j];
    $result[$j]['TJ_NM'] = $data2['TJ_NM'][$j];
    $result[$j]['COMP_NM1'] = $data2['COMP_NM1'][$j];
    $result[$j]['COMP_NM2'] = $data2['COMP_NM2'][$j];
    $result[$j]['TJ_COST'] = $data2['TJ_COST'][$j];
    $result[$j]['COMP_COST1'] = $data2['COMP_COST1'][$j];
    $result[$j]['COMP_COST2'] = $data2['COMP_COST2'][$j];
    $result[$j]['TJ_SALE_COST'] = $data2['TJ_SALE_COST'][$j];
    $result[$j]['COMP_SALE_COST1'] = $data2['COMP_SALE_COST1'][$j];
    $result[$j]['COMP_SALE_COST2'] = $data2['COMP_SALE_COST2'][$j];
    $result[$j]['TJ_STOCK'] = $data2['TJ_STOCK'][$j];
    $result[$j]['COMP_STOCK1'] = $data2['COMP_STOCK1'][$j];
    $result[$j]['COMP_STOCK2'] = $data2['COMP_STOCK2'][$j];
}

//debug2($result); exit;
/**
 * DB 적재
 */

/*************** 트랜잭션 관련 ****************/
$error_cnt = 0;
mysqli_autocommit($datalake_db, false);
/*************** 트랜잭션 관련 ****************/

sql_query('INSERT INTO TSG_MALL_INFO_BAK (SELECT * FROM TSG_MALL_INFO)', false, $datalake_db);

sql_query('truncate table TSG_MALL_INFO', false, $datalake_db);

$num = 1;
foreach ($result as $k => $row) {

    $seq = date('ymdHis').'_'.$num;
    $sql = '';

    $sql = "INSERT INTO TSG_MALL_INFO 
            SET SEQ                 = '$seq',
                MALL_GB             = '$row[MALL_GB]',
                MALL_CODE           = '$row[MALL_CODE]', 
                ITMNO               = '$row[ITMNO]',
                ITM_NM              = '$row[ITM_NM]',
                TJ_NM               = '$row[TJ_NM]',
                COMP_NM1            = '$row[COMP_NM1]',
                COMP_NM2            = '$row[COMP_NM2]',
                TJ_COST             = ".int_price($row[TJ_COST]).",
                COMP_COST1          = ".int_price($row[COMP_COST1]).",
                COMP_COST2          = ".int_price($row[COMP_COST2]).",
                TJ_SALE_COST        = ".int_price($row[TJ_SALE_COST]).",
                COMP_SALE_COST1     = ".int_price($row[COMP_SALE_COST1]).",
                COMP_SALE_COST2     = ".int_price($row[COMP_SALE_COST2]).",
                TJ_STOCK            = ".int_price($row[TJ_STOCK]).",
                COMP_STOCK1         = ".int_price($row[COMP_STOCK1]).",
                COMP_STOCK2         = ".int_price($row[COMP_STOCK2]).",
                SYSIDATE            = '".date('Y-m-d')."',
                SYSITIME            = '".date('H:i:s')."'";
    sql_query($sql, false, $datalake_db);
    /*************** 트랜잭션 관련 ****************/
    if (mysqli_errno($datalake_db)) {
        $error_cnt += 1;
        break;
    }
    /*************** 트랜잭션 관련 ****************/

    $num++;
}

/*************** 트랜잭션 관련 ****************/
if ($error_cnt > 0) {
    mysqli_rollback($datalake_db);
    mysqli_close($datalake_db);
    echo "
	<script>
		alert('데이터베이스의 에러로 인해 롤백되었습니다.');
//		history.back();
	</script>
	";
    exit;
} else {
    echo '성공?';
    mysqli_commit($datalake_db);
}
/*************** 트랜잭션 관련 ****************/


function int_price($value)
{

    if ($value) {
        $result = str_replace(',', '', trim($value));
    }

    if (empty($result) || strlen($result) == 0 || !is_numeric($result)) {
        return 0;
    } else {
        return $result;
    }

}

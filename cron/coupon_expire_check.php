<?php
include_once('./_common.php');
include_once('./cron.lib.php');

/*
 * 쿠폰만료임박
 */

$batch = new CronLib();

// 크론탭 실행되는 현재 시각
$now_date = G5_TIME_YMDHIS;


$date = date('Y-m-d',strtotime(G5_TIME_YMD."+10 day")) ;

$sql = " SELECT a.mb_id, a.cp_subject, a.cp_end, b.mb_name, b.mb_hp 
        FROM {$g5['g5_shop_coupon_table']} a 
        INNER JOIN {$g5['member_table']} b 
        ON a.mb_id = b.mb_id 
        WHERE cp_end = '$date'";

$result = sql_query($sql);


$batch->sys_log('check message: ', G5_TIME_YMDHIS, 'deposit');

for ($i = 1; $od = sql_fetch_array($result); $i++) {

    $mb_hp = $od[mb_hp];
    $mb_name = $od[mb_name];
    $cp_subject = $od[cp_subject];
    $cp_end = $od[cp_end];

    include_once(G5_LIB_PATH.'/kakao.alimtalk.lib.php');

    // 쿠폰 만료 임박 (고객)
    $recv_number = preg_replace('/[^0-9]/', '', $mb_hp);
    if($recv_number) {
        // AlimTalk BEGIN --------------------------------------------------------
        $alimtalk = new AlimTalk();
        $to = $alimtalk->set_phone_number($recv_number);
        $template_code = 'os_coupon_002';
        $text = $alimtalk->get_template($template_code);
        $text = str_replace('#{이름}', $mb_name, $text);
        $text = str_replace('#{쿠폰명}', $cp_subject, $text);
        $text = str_replace('#{쿠폰유효기간}', $cp_end, $text);
        $alimtalk->set_message($template_code, $to, $text);
        $alimtalk->send();
        // AlimTalk END   --------------------------------------------------------
    }
}


?>
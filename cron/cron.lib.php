<?php

class CronLib {
    // 로그 생성
    function sys_log($sys, $mess = '', $title = '')
    {
        $dir = $_SERVER['DOCUMENT_ROOT'].'/cron/logs/'.date('Ym').'/';
        $this->makedirs($dir, '0755');

        $filename = $title.'_' . date('Ymd') . '.log';
        $fp = fopen($dir . $filename, "a+");
        if ($fp == null)
            return;

        if (is_array($mess)) {
            // $mess = serialize($mess);
            $mess = $this->_contextToString($mess);
        }

        // fwrite($fp, date("Y-m-d H:i:s") . " " . $sys . " " . $mess . "\n");
        fwrite($fp, date("Y-m-d H:i:s") . " " . $sys . "\n" . $mess . "\n");

        fclose($fp);
    }

    function _contextToString($context)
    {
        $export = '';
        foreach ($context as $key => $value) {
            $export .= "{$key}: ";
            $export .= preg_replace(array(
                '/=>\s+([a-zA-Z])/im',
                '/array\(\s+\)/im',
                '/^  |\G  /m'
            ), array(
                '=> $1',
                'array()',
                '    '
            ), str_replace('array (', 'array(', var_export($value, true)));
            $export .= PHP_EOL;
        }

        return str_replace(array(
            '\\\\',
            '\\\''
        ), array(
            '\\',
            '\''
        ), rtrim($export));
    }

    function makedirs($dirpath, $mode = 0777)
    {
        return is_dir($dirpath) || @mkdir($dirpath, $mode, true);
    }
}
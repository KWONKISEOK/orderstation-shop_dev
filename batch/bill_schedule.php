<?php
include_once('./_common.php');

/* 결제불가일자에 없으면서 이니시스에 전송되지 않은것 실행 */
$sql = "

	SELECT * FROM(

		SELECT 
			R_1.* , 
			(SELECT COUNT(idx) FROM tbl_noday WHERE set_day = LEFT( NOW() , 10 ) AND del_yn = 'N' ) AS Xcnt
		FROM(
		
			SELECT 
				a.od_id , c.od_num , c.od_seq , a.mb_id , a.od_name , a.pharm_name , a.pharm_manager , a.pharm_manager_dept , a.pharm_custno , 
				a.od_email , a.od_tel , a.od_hp , a.od_settle_case , a.od_pg , a.od_billkey , a.od_gubun , a.od_send_yn , 
				b.it_name, b.od_period_yn, b.od_period_cnt, b.od_total_sale_price , a.od_coupon ,
				c.od_period_date , c.od_bill_tno , c.od_pay_yn , c.od_status , c.od_invoice ,
				case when e.od_id is not NULL then 'Y' ELSE 'N' END AS Inicis_SendYN , a.service_type
			from tbl_shop_order a INNER join tbl_shop_order_detail b ON a.od_id = b.od_id
										 INNER join tbl_shop_order_receiver c ON a.od_id = c.od_id
										 LEFT JOIN tbl_schedule_history e ON c.od_id = e.od_id AND c.od_num = e.od_num AND c.od_seq = e.od_seq
										 AND e.od_billkey != ''
			WHERE a.od_billkey != '' AND a.od_settle_case = '신용카드' AND a.od_pg = 'inicis' AND b.od_period_yn = 'Y'
			AND c.od_period_date >= LEFT( DATE_ADD(NOW(), INTERVAL -14 DAY) , 10 ) and c.od_period_date <= LEFT( NOW() , 10 ) 
			AND c.od_pay_yn = 'N' AND c.od_status = '확정' AND c.od_status != '취소'
			
			/*
			UNION all
			
			SELECT 
				a.od_id , c.od_num , c.od_seq , a.mb_id , a.od_name , a.pharm_name , a.pharm_manager , a.pharm_manager_dept , a.pharm_custno , 
				a.od_email , a.od_tel , a.od_hp , a.od_settle_case , a.od_pg , a.od_billkey , a.od_gubun , a.od_send_yn , 
				b.it_name, b.od_period_yn, b.od_period_cnt, b.od_total_sale_price , a.od_coupon ,
				c.od_period_date , c.od_bill_tno , c.od_pay_yn , c.od_status , c.od_invoice ,
				case when e.od_id is not NULL then 'Y' ELSE 'N' END AS Inicis_SendYN , a.service_type
			from tbl_shop_order a INNER join tbl_shop_order_detail b ON a.od_id = b.od_id
										 INNER join tbl_shop_order_receiver c ON a.od_id = c.od_id
										 LEFT JOIN tbl_schedule_history e ON c.od_id = e.od_id AND c.od_num = e.od_num AND c.od_seq = e.od_seq
										 AND e.od_billkey != ''
			WHERE a.od_billkey != '' AND a.od_settle_case = '신용카드' AND a.od_pg = 'inicis' AND b.od_period_yn = 'Y'
			AND c.od_period_date >= LEFT( DATE_ADD(NOW(), INTERVAL -14 DAY) , 10 ) and c.od_period_date <= LEFT( NOW() , 10 ) 
			AND c.od_pay_yn = 'N' AND ( c.od_status = '배송' OR c.od_status = '완료' ) AND c.od_status != '취소'	
			*/
			
		) AS R_1
		
	) AS R_2 WHERE R_2.Xcnt = 0 AND R_2.Inicis_SendYN = 'N' LIMIT 5

";
$result = sql_query($sql);

for($i=1; $row=sql_fetch_array($result); $i++){

	$od_id				 = $row["od_id"];
	$od_num				 = $row["od_num"];
	$od_seq				 = $row["od_seq"];
	$mb_id				 = $row["mb_id"];
	$od_name			 = $row["od_name"];
	$pharm_name			 = $row["pharm_name"];
	$pharm_manager		 = $row["pharm_manager"];
	$pharm_manager_dept	 = $row["pharm_manager_dept"];
	$pharm_custno		 = $row["pharm_custno"];
	$od_email			 = $row["od_email"];
	$od_tel				 = $row["od_tel"];
	$od_hp				 = $row["od_hp"];
	$od_settle_case		 = $row["od_settle_case"];
	$od_pg				 = $row["od_pg"];
	$od_billkey			 = $row["od_billkey"];
	$od_gubun			 = $row["od_gubun"];
	$od_send_yn			 = $row["od_send_yn"];
	$it_name			 = $row["it_name"];
	$od_period_yn		 = $row["od_period_yn"];
	$od_period_cnt		 = $row["od_period_cnt"];
	$od_total_sale_price = $row["od_total_sale_price"];
	$od_coupon			 = $row["od_coupon"];
	$od_period_date		 = $row["od_period_date"];
	$od_bill_tno		 = $row["od_bill_tno"];
	$od_pay_yn			 = $row["od_pay_yn"];
	$od_status			 = $row["od_status"];
	$od_invoice			 = $row["od_invoice"];
	$service_type		 = $row["service_type"];
	$oid				 = $row["od_id"].$row["od_seq"];

	if($od_period_yn == 'Y' and $od_billkey !='' and $od_pay_yn=='N') {
	
			if( $service_type == "B" ){
				$where_query = " and service_type = 'B' ";
				$add_column = " ,service_type ";
				$add_value = " ,'B' ";
				$mid = 'order00002';
				$m_url = "https://safetystation.co.kr";
			}else{
				$where_query = " and service_type != 'B' ";
				$add_column = "";
				$add_value = "";
				$mid = 'order00001';
				$m_url = "https://www.orderstation.co.kr";
			}
			//결제금액(총 주문금액 / 횟수)
			$pay_amount =  (int)( ($od_total_sale_price-$od_coupon) / $od_period_cnt );

			$cancel_msg = iconv_euckr('쇼핑몰 운영자 빌링 승인요청');

			/* * ************************
			 * 1. 라이브러리 인클루드 *
			 * ************************ */
			require_once(G5_SHOP_PATH.'/inicis/libs/INIpay41Lib.php');

			/* * *************************************
			 * 2. INIpay41 클래스의 인스턴스 생성 *
			 * ************************************* */
			$inipay = new INIpay41;

			$inipay->m_inipayHome = "/var/www/html/orderstation-shop/shop/inicis/";      // INIpay Home (절대경로로 적절히 수정)
			$inipay->m_keyPw = "1111";				// 키패스워드(상점아이디에 따라 변경)
			$inipay->m_type = "reqrealbill";		// 고정 (절대 수정금지)
			$inipay->m_pgId = "INIpayBill";			// 고정 (절대 수정금지)
			$inipay->m_payMethod = "Card";          // 고정 (절대 수정금지)
			$inipay->m_billtype = "Card";           // 고정 (절대 수정금지)
			$inipay->m_subPgIp = "203.238.3.10";    // 고정 (절대 수정금지)
			$inipay->m_debug = "true";				// 로그모드("true"로 설정하면 상세한 로그가 생성됨)
			$inipay->m_mid = $mid;					// 상점아이디
			$inipay->m_billKey = $od_billkey;			// billkey 입력
			$inipay->m_goodName = iconv("UTF-8","EUC-KR", $it_name);       // 상품명 (최대 40자)
			$inipay->m_currency = 'WON';			// 화폐단위 
			$inipay->m_price = $pay_amount;			// 가격 
			$inipay->m_buyerName = iconv("UTF-8","EUC-KR", $od_name);       // 구매자 (최대 15자) 
			$inipay->m_buyerTel = $od_hp;			// 구매자이동전화 
			$inipay->m_buyerEmail = $od_email;		// 구매자이메일
			$inipay->m_cardQuota = '00';			// 할부기간
			$inipay->m_quotaInterest = 0;			// 무이자 할부 여부 (1:YES, 0:NO)
			$inipay->m_url = $m_url;				// 상점 인터넷 주소
			$inipay->m_authentification = '01';		//( 신용카드 빌링 관련 공인 인증서로 인증을 받은 경우 고정값 "01"로 세팅)  
			$inipay->m_oid = $oid;					//주문번호
			$inipay->m_merchantreserved1 = $MerchantReserved1;  // Tax : 부가세 , TaxFree : 면세 (예 : Tax=10&TaxFree=10) 
			$inipay->m_merchantreserved2 = $MerchantReserved2;  // 예비2
			$inipay->m_merchantreserved3 = $MerchantReserved3;  // 예비3

			/* * ******************************
			 * 3. 실시간 신용카드 빌링 요청 *
			 * ****************************** */
			$inipay->startAction();
			/* * **********************************************************
			 * 4. 실시간 신용카드 빌링 결과                             *
			 * ***********************************************************
			 *                                                        *
			 * $inipay->m_tid 	  // 거래번호							  *
			 * $inipay->m_resultCode  // "00"이면 성공                  *
			 * $inipay->m_resultMsg   // 결과에 대한 설명                 *
			 * $inipay->m_authCode    // 승인번호                       *
			 * $inipay->m_pgAuthDate  // 이니시스 승인날짜 (YYYYMMDD)   *
			 * $inipay->m_pgAuthTime  // 이니시스 승인시간 (HHMMSS)     *
			 * $inipay->m_prtcCode		// 부분취소가능여부 (1:가능 , 0:불가능)	*
			 *                                                          *
			 * ********************************************************** */
			$res_cd			= $inipay->m_resultCode;
			$res_msg		= $inipay->m_resultMsg;
			$od_bill_tno	= $inipay->m_tid;
			$get_cardCode	= $inipay->m_cardCode;
			$get_cardNumber = $inipay->m_cardNumber;

			if($res_cd == '00') {

				//정기주문 카드정보 저장해주기
				if( !empty($mb_id) && !empty($get_cardCode) && !empty($get_cardNumber) && !empty($od_billkey) ){

					$sql = " select count(m_idx) as m_cnt from tbl_bill_info where delYN = 'N' and mb_id = '".$mb_id."' 
							 and cardCode = '".$get_cardCode."' and cardNumber = '".$get_cardNumber."' ".$where_query."
						   ";
					$bill_info = sql_fetch($sql);

					if( $bill_info["m_cnt"] == 0 ){
						
						$sql = " insert into tbl_bill_info( mb_id , cardCode , cardNumber , od_billkey , regDate ".$add_column." ) 
								values( '".$mb_id."','".$get_cardCode."','".$get_cardNumber."','".$od_billkey."',now() ".$add_value." )
							   ";
						sql_query($sql, true);

					}

				}

				//승인정보 저장
				 $sql = " update tbl_shop_order_receiver set od_pay_price = '$pay_amount', od_bill_tno = '$od_bill_tno', od_pay_yn = 'Y', od_pay_date = '".G5_TIME_YMDHIS."' , od_bill_tno = '$od_bill_tno'
				 where od_id = '$od_id' and  od_num = '$od_num' and od_seq = '$od_seq' ";

				 sql_query($sql, true);
				
			}

			//히스토리 넣기
			$in_sql = "
				
				insert into tbl_schedule_history(
					 od_id  
					,od_num 
					,od_seq  
					,mb_id  
					,od_name 
					,pharm_name 
					,pharm_manager 
					,pharm_manager_dept 
					,pharm_custno 
					,od_email 
					,od_tel 
					,od_hp  
					,od_settle_case 
					,od_pg 
					,od_billkey 
					,od_gubun 
					,od_send_yn 
					,it_name
					,od_period_yn
					,od_period_cnt
					,od_total_sale_price 
					,od_coupon 
					,od_period_date 
					,od_bill_tno 
					,od_pay_yn 
					,od_status 
					,od_invoice 
					,res_cd
					,res_msg
					,cardCode
					,cardNumber
					,reg_date
					,service_type
				)values(
					 ".$od_id."
					,".$od_num."
					,".$od_seq."
					,'".$mb_id."'
					,'".$od_name."'
					,'".$pharm_name."'
					,'".$pharm_manager."'
					,'".$pharm_manager_dept."'
					,'".$pharm_custno."'
					,'".$od_email."'
					,'".$od_tel."'
					,'".$od_hp."'
					,'".$od_settle_case."'
					,'".$od_pg."'
					,'".$od_billkey."'
					,'".$od_gubun."'
					,'".$od_send_yn."'
					,'".$it_name."'
					,'".$od_period_yn."'
					,".$od_period_cnt."
					,".$od_total_sale_price."
					,".$od_coupon."
					,'".$od_period_date."'
					,'".$od_bill_tno."'
					,'".$od_pay_yn."'
					,'".$od_status."'
					,'".$od_invoice."'
					,'".$res_cd."'
					,'".iconv_utf8($res_msg)."'
					,'".$get_cardCode."'
					,'".$get_cardNumber."'
					,now()
					,'".$service_type."'
				)
			
			";

			sql_query($in_sql, true);

	}

	//echo "성공"."<br>";
	
}



?>
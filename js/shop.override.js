jQuery(function($){

    $(".2017_renewal_itemform select.it_supply").on("shop_sel_supply_process", function(e, param){
        
        var add_exec = param.add_exec;
        var $el = $(this);
        var val = $el.val();
		
        //블랙캣77님이 해당 코드에 도움을 주셨습니다.
        var eq = $("select.it_supply").index($(this));
        var item = $el.closest(".sit_option").find("label").eq(eq).text();

        if(!val) {
            alert(item+"을(를) 선택해 주십시오.");
            return false;
        }

        var info = val.split(",");

        // 재고체크
        if(parseInt(info[2]) < 1) {
            alert(info[0]+"은(는) 재고가 부족하여 구매할 수 없습니다.");
            return false;
        }

        var id = item+chr(30)+info[0];
        var option = item+":"+info[0];
        var price = info[1];
        var stock = info[2];
		var ioname = info[3];

        // 금액 음수 체크
        if(parseInt(price) < 0) {
            alert("구매금액이 음수인 상품은 구매할 수 없습니다.");
            return false;
        }

        if(add_exec) {
            if(same_option_check(option))
                return false;
            add_sel_option(1, id, option, price, stock, ioname);
        }

        return false;
    });

    if (typeof add_sel_option === "function") {

        add_sel_option = (function() {
            var cached_function = add_sel_option;

            return function() {

                if ($("#sit_ov_wrap").length) {

                    var a = arguments;
                    var type=a[0],
                        id=a[1],
                        option=a[2],
                        price=a[3],
                        stock=a[4],
					    ioname=a[5];

                    var item_code = $("input[name='it_id[]']").val();
					var option_add = true;
					//기 선택여부 체크
					$("input[name^=io_id]").each(function(index) {
						val = $(this).val();
						if(val == id) {
							alert("이미 선택한 상품입니다. 수량을 조정해주세요");
							option_add = false;
							return false;
						}
					});
					//기 선택여부 체크
					if(!option_add) return false;

                    var opt_prc;
                    if(parseInt(price) >= 0)
                        opt_prc = "+"+number_format(String(price));
                    else {
						price1 = Math.abs(price);
                        opt_prc = "-"+number_format(String(price1));
					}

                    // 옵션 선택시 나오는 상자
                    opt2 = "";
                    opt2 += "<div class=\"product-selected-box is-opened\">";
                    opt2 += "<input type=\"hidden\" name=\"io_type["+item_code+"][]\" value=\""+type+"\">";
                    opt2 += "<input type=\"hidden\" name=\"io_id["+item_code+"][]\" value=\""+id+"\">";
                    opt2 += "<input type=\"hidden\" name=\"io_value["+item_code+"][]\" value=\""+ioname+"\">";
                    opt2 += "<input type=\"hidden\" class=\"io_price\" value=\""+price+"\">";
                    opt2 += "<input type=\"hidden\" class=\"io_stock\" value=\""+stock+"\">";
                    opt2 += "<span class=\"selected-item\">"+ioname+"\ "+ id.substring(7,50)+"\</span>";
                    opt2 += "<button type=\"button\" class=\"icon icon-pop-close\" title=\"삭제\"><span class=\"sound_only\">삭제</span></button>";
                    opt2 += "<div class=\"input-count-box\">";
                    opt2 += "<button type=\"button\" class=\"count-minus\" title=\"빼기\"><span class=\"sound_only\">감소</span></button>";
                    opt2 += "<span class=\"count-text\">";
                    opt2 += "<input type=\"text\" id=\"\" title=\"수량입력\" name=\"ct_qty["+item_code+"][]\" value=\"1\" size=\"5\" class=\"input-default input-count\">";
                    opt2 += "</span>";
                    opt2 += "<button type=\"button\" class=\"count-plus\" title=\"더하기\"><span class=\"sound_only\">증가</span></button>";
                    opt2 += "</div>";
                    opt2 += "<span class=\"selected-price\"><span class=\"price\">"+opt_prc+"</span><span class=\"unit\">원</span></span>";
                    opt2 += "</div>";

                    if($("#it_sel_option .product-selected-box").length < 1) {
                        $("#it_sel_option").html(opt2);
                    } else {
                        if(type) {
                            // 추가옵션 개발 시 추가 부분
                        } else {
                            if($("#it_sel_option .product-selected-box").length > 0) {
                                // last로 해야 중복으로 추가되지않음.
                                $("#it_sel_option .product-selected-box:last").after(opt2);
                            } else {
                                $("#it_sel_option").html(opt2);
                            }
                        }
                    }

                    price_calculate();

                } else {

                    cached_function.apply(this, arguments); // use .apply() to call it

                }   //end if

            };
        }());
    }   //end if check function

    if (typeof price_calculate === "function") {
        price_calculate = (function() {
            var cached_function = price_calculate;

            return function() {
                
                if( $("#sit_ov_wrap").length ){

                    var it_price = parseInt($("input#it_price").val());

                    if(isNaN(it_price))
                        return;

                    var $el_prc = $("input.io_price");
                    var $el_qty = $("input[name^=ct_qty]");
                    var $el_type = $("input[name^=io_type]");
					var $sit_opt_prc = $("span.sit_opt_prc");
                    var $sit_opt_prc2 = $(".selected-price .price");
                    var price, type, qty, total = 0;
                    var tot_qty = 0; // 전체 수량 체크

                    $el_prc.each(function(index) {
                        price = parseInt($(this).val());
                        qty = parseInt($el_qty.eq(index).val());
                        type = $el_type.eq(index).val();

                        if(type == "0") { // 선택옵션
                            total += (it_price + price) * qty;
							$sit_opt_prc.eq(index).html( number_format(String( (it_price + price) * qty ) ) );
                            $sit_opt_prc2.eq(index).html( number_format(String( (it_price + price) * qty ) ) );
                        } else { // 추가옵션
                            total += price * qty;
							$sit_opt_prc.eq(index).html( number_format(String( price * qty ) ) );
                            $sit_opt_prc2.eq(index).html( number_format(String( price * qty ) ) );
                        }

                        tot_qty += qty;

                    });

                    $("#sit_tot_price").empty().html("<span>총 금액 </span><strong>"+number_format(String(total))+"</strong> 원");
                    $(".product-price-desc .total-price").empty().html(number_format(String(total)));
                    $("#it_total_cnt").html(number_format(String(tot_qty)));

                } else {
                    cached_function.apply(this, arguments); // use .apply() to call it
                }
                
            };
        }());
    }   //end if check function

});
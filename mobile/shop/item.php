<?php
include_once('./_common.php');
include_once(G5_LIB_PATH.'/iteminfo.lib.php');

$it_id = trim($_GET['it_id']);

// 분류사용, 상품사용하는 상품의 정보를 얻음
$sql = " select a.*,
                b.ca_name,
                b.ca_use
           from {$g5['g5_shop_item_table']} a,
                {$g5['g5_shop_category_table']} b
          where a.it_id = '$it_id'
            and a.ca_id = b.ca_id ";
$it = sql_fetch($sql);
if (!$it['it_id'])
    alert('자료가 없습니다.');
if (!($it['ca_use'] && $it['it_use'])) {
    if (!$is_admin)
        alert('판매가능한 상품이 아닙니다.');
}

// 회원구분에 따른 상품 접근 예외처리
if ($member['mb_type'] != "9") {
    if ($member['mb_referee'] != "5") { // 약국,일반회원의 경우
        /* 약국회원인데 상품노출구분에 약국구분이 체크가 되어있지 않다면 경고 띄워주고 주문 못하게 막기*/
        if ($member['mb_type'] == "1" && $it['it_web_view'] == "0") {
            echo "<script>
				alert('해당 상품은 회원별 비노출 상품입니다.');
				history.back();
			  </script>";
            exit;
        }

        /* 일반회원인데 상품노출구분에 일반회원이 체크가 되어있지 않다면 경고 띄워주고 주문 못하게 막기*/
        if ($member['mb_type'] == "0" && $it['it_app_view'] == "0") {
            echo "<script>
				alert('해당 상품은 회원별 비노출 상품입니다.');
				history.back();
			  </script>";
            exit;
        }
        if (get_item_auth_check($it['it_id'], 1) == true && get_item_auth_check($member['mb_id'], 2) == false) {
            echo "<script>
				alert('해당 상품은 회원별 비노출 상품입니다.');
				history.back();
			  </script>";
            exit;
        }
    } else { // 얼라이언스회원의 경우
        if ($member['mb_referee'] == "5" && $member['mb_type'] == "1" && $it['it_new_view'] == "0" && $it['it_web_view'] == "0") {
            echo "<script>
				alert('해당 상품은 회원별 비노출 상품입니다.');
				history.back();
			  </script>";
            exit;
        }
    }
}

// 분류 테이블에서 분류 상단, 하단 코드를 얻음
$sql = " select ca_mobile_skin_dir, ca_include_head, ca_include_tail, ca_cert_use, ca_adult_use
           from {$g5['g5_shop_category_table']}
          where ca_id = '{$it['ca_id']}' ";
$ca = sql_fetch($sql);

// 본인인증, 성인인증체크
if(!$is_admin) {
    $msg = shop_member_cert_check($it_id, 'item');
    if($msg)
        alert($msg, G5_SHOP_URL);
}

// 오늘 본 상품 저장 시작
// tv 는 today view 약자
$saved = false;
$tv_idx = (int)get_session("ss_tv_idx");
if ($tv_idx > 0) {
    for ($i=1; $i<=$tv_idx; $i++) {
        if (get_session("ss_tv[$i]") == $it_id) {
            $saved = true;
            break;
        }
    }
}

if (!$saved) {
    $tv_idx++;
    set_session("ss_tv_idx", $tv_idx);
    set_session("ss_tv[$tv_idx]", $it_id);
}
// 오늘 본 상품 저장 끝

// 조회수 증가
if (get_cookie('ck_it_id') != $it_id) {
    sql_query(" update {$g5['g5_shop_item_table']} set it_hit = it_hit + 1 where it_id = '$it_id' "); // 1증가
    set_cookie("ck_it_id", $it_id, 3600); // 1시간동안 저장
}

// 이전 상품보기
$sql = " select it_id, it_name from {$g5['g5_shop_item_table']}
          where it_id > '$it_id'
            and SUBSTRING(ca_id,1,4) = '".substr($it['ca_id'],0,4)."'
            and it_use = '1'
          order by it_id asc
          limit 1 ";
$row = sql_fetch($sql);
if ($row['it_id']) {
    $prev_title = '이전상품 <span>'.$row['it_name'].'</span>';
    $prev_href = '<a href="'.G5_SHOP_URL.'/item.php?it_id='.$row['it_id'].'" id="siblings_prev">';
    $prev_href2 = '</a>';
} else {
    $prev_title = '';
    $prev_href = '';
    $prev_href2 = '';
}

// 다음 상품보기
$sql = " select it_id, it_name from {$g5['g5_shop_item_table']}
          where it_id < '$it_id'
            and SUBSTRING(ca_id,1,4) = '".substr($it['ca_id'],0,4)."'
            and it_use = '1'
          order by it_id desc
          limit 1 ";
$row = sql_fetch($sql);
if ($row['it_id']) {
    $next_title = '다음 상품 <span>'.$row['it_name'].'</span>';
    $next_href = '<a href="'.G5_SHOP_URL.'/item.php?it_id='.$row['it_id'].'" id="siblings_next">';
    $next_href2 = '</a>';
} else {
    $next_title = '';
    $next_href = '';
    $next_href2 = '';
}

// 고객선호도 별점수
$star_score = get_star_image($it['it_id']);

// 관리자가 확인한 사용후기의 개수를 얻음
$sql = " select count(*) as cnt from `{$g5['g5_shop_item_use_table']}` where it_id = '{$it_id}' /*and is_confirm = '1'*/ ";
$row = sql_fetch($sql);
$item_use_count = $row['cnt'];

// 상품문의의 개수를 얻음
$sql = " select count(*) as cnt from `{$g5['g5_shop_item_qa_table']}` where it_id = '{$it_id}' ";
$row = sql_fetch($sql);
$item_qa_count = $row['cnt'];

if ($default['de_mobile_rel_list_use']) {
    // 관련상품의 개수를 얻음
    $sql = " select count(*) as cnt
               from {$g5['g5_shop_item_relation_table']} a
               left join {$g5['g5_shop_item_table']} b on (a.it_id2=b.it_id)
              where a.it_id = '{$it['it_id']}' and b.it_use='1' ";
    $row = sql_fetch($sql);
    $item_relation_count = $row['cnt'];
}

// 상품품절체크
if(G5_SOLDOUT_CHECK)
    $is_soldout = is_soldout($it['it_id']);

// 주문가능체크
$is_orderable = true;
if(!$it['it_use'] || $it['it_tel_inq'] || $is_soldout)
    $is_orderable = false;

if($is_orderable) {
    if(defined('G5_THEME_USE_OPTIONS_TRTD') && G5_THEME_USE_OPTIONS_TRTD){
        $option_item = get_item_options($member['mb_type'], $it['it_id'], $it['it_option_subject'], '');
        $option_item_new = get_item_options_new($member['mb_type'], $it['it_id'], $it['it_option_subject'], '');
        $supply_item = get_item_supply($it['it_id'], $it['it_supply_subject'], '');
    } else {
        // 선택 옵션 ( 기존의 tr td 태그로 가져오려면 'div' 를 '' 로 바꾸거나 또는 지워주세요 )
        $option_item = get_item_options($member['mb_type'], $it['it_id'], $it['it_option_subject'], 'div');
        $option_item_new = get_item_options_new($member['mb_type'], $it['it_id'], $it['it_option_subject'], 'div');

        // 추가 옵션 ( 기존의 tr td 태그로 가져오려면 'div' 를 '' 로 바꾸거나 또는 지워주세요 )
        $supply_item = get_item_supply($it['it_id'], $it['it_supply_subject'], 'div');
    }

    // 상품 선택옵션 수
    $option_count = 0;
    if($it['it_option_subject']) {
        $temp = explode(',', $it['it_option_subject']);
        $option_count = count($temp);
    }

    // 상품 추가옵션 수
    $supply_count = 0;
    if($it['it_supply_subject']) {
        $temp = explode(',', $it['it_supply_subject']);
        $supply_count = count($temp);
    }
}

// 스킨경로
$skin_dir = G5_MSHOP_SKIN_PATH;
$ca_dir_check = true;

if($it['it_mobile_skin']) {
    if(preg_match('#^theme/(.+)$#', $it['it_mobile_skin'], $match))
        $skin_dir = G5_THEME_MOBILE_PATH.'/'.G5_SKIN_DIR.'/shop/'.$match[1];
    else
        $skin_dir = G5_MOBILE_PATH.'/'.G5_SKIN_DIR.'/shop/'.$it['it_mobile_skin'];

    if(is_dir($skin_dir)) {
        $form_skin_file = $skin_dir.'/item.form.skin.php';

        if(is_file($form_skin_file))
            $ca_dir_check = false;
    }
}

if($ca_dir_check) {
    if($ca['ca_mobile_skin_dir']) {
        if(preg_match('#^theme/(.+)$#', $ca['ca_mobile_skin_dir'], $match))
            $skin_dir = G5_THEME_MOBILE_PATH.'/'.G5_SKIN_DIR.'/shop/'.$match[1];
        else
            $skin_dir = G5_MOBILE_PATH.'/'.G5_SKIN_DIR.'/shop/'.$ca['ca_mobile_skin_dir'];

        if(is_dir($skin_dir)) {
            $form_skin_file = $skin_dir.'/item.form.skin.php';

            if(!is_file($form_skin_file))
                $skin_dir = G5_MSHOP_SKIN_PATH;
        } else {
            $skin_dir = G5_MSHOP_SKIN_PATH;
        }
    }
}

define('G5_SHOP_CSS_URL', str_replace(G5_PATH, G5_URL, $skin_dir));

$g5['title'] = $it['it_name'].' &gt; '.$it['ca_name'];

include_once(G5_MSHOP_PATH.'/_head.php');
if($member['mb_id']=='chaeyoung' or $member['mb_id']=='admin') {
	include_once(G5_SHOP_PATH.'/settle_naverpay.inc.php');
}

// 상단 HTML
echo '<div id="sit_hhtml">'.conv_content($it['it_mobile_head_html'], 1).'</div>';
?>

<?php if($is_orderable) { ?>

<script src="<?php echo G5_JS_URL; ?>/shop.js?ver=<?php echo G5_JS_VER; ?>"></script>

<?php } ?>


<?php
if (G5_HTTPS_DOMAIN)
    $action_url = G5_HTTPS_DOMAIN.'/'.G5_SHOP_DIR.'/cartupdate.php';
else
    $action_url = G5_SHOP_URL.'/cartupdate.php';
?>

<style>

	/*모바일_부분*/

	.choice_wrap_2 {
		width: 100%;
		padding: 25px;
		background: #e7e7e9;
		font-family: 'KoPubDotum';
	}

	.choice_wrap_2 .choice_box1 {
		width: 100%;
		background: #FFF;
		height: auto;
		border: 2px solid #d3d4d6;
		border-radius: 20px;
		overflow: hidden;
	}

	.choice_wrap_2 .box1_left {
		width: 11%;
		height: auto;
		background: #e42a8d;
		/* fallback for old browsers */
		background: -webkit-linear-gradient(to right, #e42a8d, #bf236a);
		/* Chrome 10-25, Safari 5.1-6 */
		background: linear-gradient(to right, #e42a8d, #bf236a);
		/* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
		float: left;
		border-radius: 0px 0px 20px 0px;
		text-align: center;
		padding: 15px 0;
		color: #FFF;
		font-weight: 600;
	}

	.choice_wrap_2 .box1_right {
		width: 89%;
		padding: 60px 20px;
		float: left;
	}

	.choice_wrap_2 .br_left1 {
		float: left;
		width: 60%;
		font-size: 23px;
		word-break:break-word;
	}

	.choice_wrap_2 .br_left1 span {
		display: block;
		font-size: 35px;
		font-weight: 600;
		margin-bottom: 10px;
	}

	.choice_wrap_2 .br_right1 {
		width: 40%;
		float: left;
		padding: 0px 20px;
	}
	
	.choice_wrap_2 .br_right1 img {
		width: 100%;
	}

	.choice_wrap_2 .box1_left span:after {
		content: "";
		display: block;
		width: 50%;
		border-bottom: 1px solid #FFF;

		margin: 5px auto 0 auto;
	}

	.choice_wrap_2 .box1_left span {
		font-size: 20px;
		display: block;
	}

	.choice_wrap_2 .box1_left a {
		display: block;
		font-size: 50px;
		color:#fff;
	}

	/*모바일_부분2*/

	@media(max-width:760px) {

		.choice_wrap_2 .box1_left {
			width: 15%;
			padding: 10px 2px;
		}
		.choice_wrap_2 .box1_right {
			width: 85%;
			padding: 30px 20px;
		}
		.choice_wrap_2 .br_left1 span {
			font-size: 1.8em;
			letter-spacing: -1px;
		}
		.choice_wrap_2 .br_left1 {
			font-size: 1em;
		}
		.choice_wrap_2 .box1_left a {
			font-size: 2em;
			color:#fff;
		}
		.choice_wrap_2 .box1_left span {
			font-size: 1em;
		}
		.choice_wrap_2 {
			padding: 15px;
		}
		.choice_wrap_2 .br_right1 {
			padding: 0px 0px 0px 10px;
		}
	}

	@media(max-width:500px) {
		/*
		.choice_wrap_2 .br_left1 {
			font-size: 0.9em;
		}
		*/
		.choice_wrap_2 .box1_right {
			padding: 25px 15px;
		}
		.choice_wrap_2 .box1_left a {
			font-size: 1.5em;
			color:#fff;
		}
		.choice_wrap_2 .box1_left span {
			font-size: 1.2em;
		}
	}
	/*
	.img_wrpa1 {
		margin: 0 auto;
		width: 100%;
		height: auto;
	}

	.img_wrpa1>img {
		width: 100%
	}
	*/

	.up_divwrap2 {
		width: 100%;
		height: 85px;
		background: #f78e21;
		padding: 0 15px;
		position: relative;
		bottom: 85px;
		left: 0px;
		margin: 0 auto;
	}

	.up_divwrap2 img {
		position: absolute;
		bottom: 0px;
		width: 30%;
		right: 1%;
	}

	.up_divwrap2 i {
		float: left;
		display: block;
		line-height: 85px;
		font-size: 40px;
		width: 5%;
		color: #FFF;
	}

	.up_divwrap2 a {
		float: left;
		display: block;
		color: #FFF;
		width: 70%;
		line-height: 85px;
		font-size: 30px;
		padding: 0 15px;
		font-weight: 600;
		letter-spacing: -1px;
		text-overflow: ellipsis;
		white-space: nowrap;
		overflow: hidden
	}


	@media(max-width:1024px) {
		.up_divwrap2 {
			bottom: 75px;
			height: 75px;
		}
		.up_divwrap2 a {
			padding: 0 10px;
			font-size: 20px;
			line-height: 75px;
			width: 65%;
		}
		.up_divwrap2 i {
			line-height: 75px;
			font-size: 25px;
			width: 4%;
		}

		.up_divwrap2 img {
			width: 34%;
		}

	}

	@media(max-width:500px) {

		.up_divwrap2 {
			bottom: 55px;
			height: 55px;
		}
		.up_divwrap2 a {
			padding: 0 10px;
			font-size: 11px;
			line-height: 55px;
		}
		.up_divwrap2 i {
			line-height: 55px;
		}

	}


	@media(max-width:400px) {
		.up_divwrap2 img {
			width: 39%;
		}
		.up_divwrap2 {
			bottom: 45px;
			height: 45px;
		}
		.up_divwrap2 a {
			line-height: 45px;
			width: 60%;
		}
		.up_divwrap2 i {
			line-height: 45px;
			font-size: 18px;
			width: 4%;
		}

	}

</style>


<div id="sit">

    <?php

	//if( $_SERVER['REMOTE_ADDR'] == "121.162.38.65" || $_SERVER['REMOTE_ADDR'] == "59.6.135.109" ){
	if( $it_id == "S00140" || $it_id == "S00141" || $it_id == "S00142" || $it_id == "S00143" || $it_id == "S00144" || $it_id == "S00145" || $it_id == "188019" || $it_id == "S00687" || $it_id == "S00688" ){
		// 상품 구입폼
		include_once($skin_dir.'/item_test.form.skin.php');
	}else{
		// 상품 구입폼
		include_once($skin_dir.'/item.form.skin.php');
	}

    ?>

</div>

<script>

	$(document).ready(function(){

		$('select').find('option:first').attr('selected', 'selected');

	});

</script>


<?php
// 하단 HTML
echo conv_content($it['it_mobile_tail_html'], 1);

include_once(G5_MSHOP_PATH.'/_tail.php');
?>

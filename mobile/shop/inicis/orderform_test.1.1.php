<?php
if (!defined("_GNUBOARD_")) exit; // 개별 페이지 접근 불가

if(!function_exists('get_inicis_app_scheme')){
    function get_inicis_app_scheme(){
        $user_agent = $_SERVER['HTTP_USER_AGENT'];

        $iPod = stripos($user_agent,"iPod");
        $iPhone  = stripos($user_agent,"iPhone");
        $iPad    = stripos($user_agent,"iPad");

        if( $iPod || $iPhone || $iPad ){    //IOS 의 앱브라우저에서 ISP결제시 리다이렉트 safari로 돌아가는 문제가 있음
            if( preg_match('/NAVER\(inapp;/', $user_agent) ){       //네이버
                return 'app_scheme=naversearchapp://&';
            }
            else if( preg_match('/CriOS/', $user_agent) ){          //크롬
                return 'app_scheme=googlechromes://&';
            }
            else if( preg_match('/DaumDevice/', $user_agent) ){      //다음
                return 'app_scheme=daumapps://&';
            }
            else if( preg_match('/KAKAOTALK/', $user_agent) ){          //카카오톡
                return 'app_scheme=kakaotalk://&';
            }
            else if( preg_match('/(FBAN|FBAV)/', $user_agent) ){        //페이스북
                return 'app_scheme=fb://&';
            }
        }

        return '';
    }
}

require_once('./inicis/libs/INIStdPayUtil.php');
$SignatureUtil = new INIStdPayUtil();

//############################################
// 1.전문 필드 값 설정(***가맹점 개발수정***)
//1. 해쉬 데이터 생성 키 : 가맹점에 MID 와 함께 제공되는 상점 대칭키
//Ex) MID : INIBillTst / 상점대칭키 : b09LVzhuTGZVaEY1WmJoQnZzdXpRdz09
//$mid 			= "order00001";  								
//SmdtaGNmTzBwNXJMMEd0OTRwSXBHdz09	

//$mid 			= "INIBillTst";  								// 가맹점 ID(가맹점 수정후 고정)					
//$signKey 		= "SU5JTElURV9UUklQTEVERVNfS0VZU1RS"; 			// 가맹점에 제공된 키(이니라이트키) (가맹점 수정후 고정) !!!절대!! 전문 데이터로 설정금지
//############################################
// 여기에 설정된 값은 Form 필드에 동일한 값으로 설정
$mid 			= "order00001";  	//order00001 INIBillTst							// 가맹점 ID(가맹점 수정후 고정)					
$timestamp 		= $SignatureUtil->getTimestamp();   			// util에 의해서 자동생성
$orderNumber 	= $od_id; 						// 가맹점 주문번호(가맹점에서 직접 설정)
 //ashdata=SHA-256(mid + orderid + timestamp + merchantkey) 
$mKey 					= 'SmdtaGNmTzBwNXJMMEd0OTRwSXBHdz09';

$str = $mid.$orderNumber.$timestamp.$mKey;
$hashdata = $SignatureUtil->makeHash($str , "sha256");

/*
 **** 위변조 방지체크를 signature 생성 ***
 * oid, price, timestamp 3개의 키와 값을
 * key=value 형식으로 하여 '&'로 연결한 하여 SHA-256 Hash로 생성 된값
 * ex) oid=INIpayTest_1432813606995&price=819000&timestamp=2012-02-01 09:19:04.004
 * key기준 알파벳 정렬
 * timestamp는 반드시 signature생성에 사용한 timestamp 값을 timestamp input에 그데로 사용하여야함
 */
//$params = array(
//    "oid" => $orderNumber,
//    "price" => $price,
//    "timestamp" => $timestamp
//);

//$sign		= $SignatureUtil->makeSignature($params);

//$http_host 	= $_SERVER['HTTP_HOST'];


// 페이지 URL에서 고정된 부분을 적는다. 
// Ex) returnURL이 http://localhost:8082/demo/INIpayStdSample/INIStdPayReturn.jsp 라면
//                 http://localhost:8082/demo/INIpayStdSample 까지만 기입한다.
?>

<form name="sm_form" method="POST" action="https://inilite.inicis.com/inibill/inibill_card.jsp" >
<input type="hidden" name="version" value="1.0" >
<input type="hidden" name="mid" value="<?php echo $mid ?>" >
<input type="hidden" name="goodname" value="<?php echo $goods; ?>">
<input type="hidden" name="orderid" value="<?php echo $od_id ?>" >
<input type="hidden" name="price" value="<?php echo $tot_price; ?>"><br>
<input type="hidden" name="currency" value="WON" >
<input type="hidden" name="buyername" value="홍길동" >
<input type="hidden" name="buyertel" value="" >
<input type="hidden" name="buyeremail" value="" >
<input type="hidden" name="timestamp" value="<?php echo $timestamp ?>" >

<input type="hidden" name="returnurl" value="<?php echo $return_url_period.$od_id; ?>">

<input type="hidden" name="gopaymethod" value="" >
<input type="hidden" name="period" value="<?php echo $offerPeriod;?>" >
<input type="hidden" name="hashdata" value="<?php echo $hashdata ?>" >

<input type="hidden" name="P_OID"        value="<?php echo $od_id; ?>">
<input type="hidden" name="P_GOODS"      value="<?php echo $goods; ?>">
<input type="hidden" name="P_AMT"        value="<?php echo $tot_price; ?>">
<input type="hidden" name="P_UNAME"      value="">
<input type="hidden" name="P_MOBILE"     value="">
<input type="hidden" name="P_EMAIL"      value="">
<input type="hidden" name="P_MID"        value="<?php echo $default['de_inicis_mid']; ?>">
<input type="hidden" name="P_NEXT_URL"   value="<?php echo $next_url; ?>">
<input type="hidden" name="P_NOTI_URL"   value="<?php echo $noti_url; ?>">
<input type="hidden" name="P_RETURN_URL" value="">
<input type="hidden" name="P_HPP_METHOD" value="2">
<input type="hidden" name="P_RESERVED"   value="<?php echo get_inicis_app_scheme(); ?>bank_receipt=N&twotrs_isp=Y&block_isp=Y<?php echo $useescrow.$inicis_cardpoint; ?>">
<input type="hidden" name="DEF_RESERVED" value="<?php echo get_inicis_app_scheme(); ?>bank_receipt=N&twotrs_isp=Y&block_isp=Y<?php echo $useescrow.$inicis_cardpoint; ?>">
<input type="hidden" name="P_NOTI"       value="<?php echo $od_id; ?>">
<input type="hidden" name="P_QUOTABASE"  value="01:02:03:04:05:06:07:08:09:10:11:12"> 
<input type="hidden" name="P_SKIP_TERMS"      value="">



<input type="hidden" name="recvname"    value="">
<input type="hidden" name="recvtel"     value="">
<input type="hidden" name="recvaddr"    value="">
<input type="hidden" name="recvpostnum" value="">
<!-- 
<form name="sm_form" method="POST" action=" " >
<input type="hidden" name="P_OID"        value="<?php echo $od_id; ?>">
<input type="hidden" name="P_GOODS"      value="<?php echo $goods; ?>">
<input type="hidden" name="P_AMT"        value="<?php echo $tot_price; ?>">
<input type="hidden" name="P_UNAME"      value="">
<input type="hidden" name="P_MOBILE"     value="">
<input type="hidden" name="P_EMAIL"      value="">
<input type="hidden" name="P_MID"        value="<?php echo $default['de_inicis_mid']; ?>">
<input type="hidden" name="P_NEXT_URL"   value="<?php echo $next_url; ?>">
<input type="hidden" name="P_NOTI_URL"   value="<?php echo $noti_url; ?>">
<input type="hidden" name="P_RETURN_URL" value="">
<input type="hidden" name="P_HPP_METHOD" value="2">
<input type="hidden" name="P_RESERVED"   value="<?php echo get_inicis_app_scheme(); ?>bank_receipt=N&twotrs_isp=Y&block_isp=Y<?php echo $useescrow.$inicis_cardpoint; ?>">
<input type="hidden" name="DEF_RESERVED" value="<?php echo get_inicis_app_scheme(); ?>bank_receipt=N&twotrs_isp=Y&block_isp=Y<?php echo $useescrow.$inicis_cardpoint; ?>">
<input type="hidden" name="P_NOTI"       value="<?php echo $od_id; ?>">
<input type="hidden" name="P_QUOTABASE"  value="01:02:03:04:05:06:07:08:09:10:11:12"> 
<input type="hidden" name="P_SKIP_TERMS"      value="">
-->
<input type="hidden" name="good_mny"     value="<?php echo $tot_price; ?>" >

<?php if($default['de_tax_flag_use']) { ?>
<input type="hidden" name="P_TAX"        value="">
<input type="hidden" name="P_TAXFREE"    value="">
<?php } ?>
</form>
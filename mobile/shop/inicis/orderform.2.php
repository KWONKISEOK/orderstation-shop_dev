<?php
if (!defined("_GNUBOARD_")) exit; // 개별 페이지 접근 불가
?>

<input type="hidden" name="good_mny"          value="<?php echo $tot_price ?>" >
<input type="hidden" name="res_cd"            value="">                                     <!-- 결과 코드          -->

<input type="hidden" name="P_HASH"            value="">
<input type="hidden" name="P_TYPE"            value="">
<input type="hidden" name="P_UNAME"           value="">
<input type="hidden" name="P_AUTH_DT"         value="">
<input type="hidden" name="P_AUTH_NO"         value="">
<input type="hidden" name="P_HPP_CORP"        value="">
<input type="hidden" name="P_APPL_NUM"        value="">
<input type="hidden" name="P_VACT_NUM"        value="">
<input type="hidden" name="P_VACT_NAME"       value="">
<input type="hidden" name="P_VACT_BANK"       value="">
<input type="hidden" name="P_CARD_ISSUER"     value="">

<?php if($default['de_tax_flag_use']) { ?>
<input type="hidden" name="comm_tax_mny"      value="<?php echo $comm_tax_mny; ?>">         <!-- 과세금액    -->
<input type="hidden" name="comm_vat_mny"      value="<?php echo $comm_vat_mny; ?>">         <!-- 부가세     -->
<input type="hidden" name="comm_free_mny"     value="<?php echo $comm_free_mny; ?>">        <!-- 비과세 금액 -->
<?php } ?>

<div id="display_pay_button" class="btn_confirm">
	<? if ($member[choice_recommend_pharm_name] == "N" && $is_member == true) { ?>
		<strong>회원정보 누락으로 인해 주문불가합니다. 회원정보를 빠짐없이 입력해주세요.</strong>
	<?}else{?>

		<?
		//정기결제일때만 실행
		if( $period == "y" && $member['mb_type'] != "1" ){ 

			//결제내역 검사
			$sql2 = " select count(m_idx) as m_cnt from tbl_bill_info where mb_id = '{$member['mb_id']}' and delYN = 'N' and service_type is null ";
			$row2 = sql_fetch($sql2);

			if( $row2["m_cnt"] > 0 ){
		?>
			<span id="save_total_info">

				<div style="float:left;text-align:left;font-size:12px;margin-bottom:10px;width:100%;">

					<? if( !empty( $member['od_billkey'] ) ){ ?>
					<div style="margin-bottom:10px;">
						<select name="select_bill_info" id="select_bill_info" style="width:100%;height:30px;margin-top:10px;">
							<option value="">---저장된 결제수단 선택---</option>
							<option value="1">[<?=GetBankName($member["cardCode"])?>]&nbsp;[<?=$member["cardNumber"]?>]</option>
							<option value="2">새 결제수단</option>
						</select>
						<div id="bill_txt"></div>
					</div>

					<? } ?>

					<? if( empty( $member['od_billkey'] ) ){ ?>
						<label for="save_bill"><input type="checkbox" class="save_bill" id="save_bill" value="Y"><strong>결제수단 저장</strong></label>
					<? }else{ ?>
						<label for="save_bill"><input type="checkbox" class="save_bill" id="save_bill" value="Y"><strong>결제수단 목록보기</strong></label>
					<? } ?>

					<div id="save_info" style="clear:both;margin-top:10px;width:100%;display:none;">
						<?
							if( !empty($member['od_billkey']) ){
								$TmpSql = " and od_billkey != '{$member['od_billkey']}' ";
							}
							$sql2 = " select * from tbl_bill_info where mb_id = '{$member['mb_id']}' and delYN = 'N' and service_type is null  ".$TmpSql." order by regDate desc  ";
							$result2 = sql_query($sql2);
							
							$i = 0;
							for($i=0; $row2=sql_fetch_array($result2); $i++) {
						?>
							<div style="float:left;width:100%;text-align:left;font-size:13px;padding-left:8px;border:1px solid #eceff4;background:#fff;line-height:35px;">
								[<?=GetBankName($row2["cardCode"])?>]&nbsp; 
								[<?=$row2["cardNumber"]?>]&nbsp;&nbsp;
								<a href="javascript:SaveInfo('<?=$row2["m_idx"]?>');" class="btn_frmline" style="height:25px;margin:5px 0;">기본결제수단으로</a>
								<a href="javascript:DelInfo('<?=$row2["m_idx"]?>');" class="btn_frmline" style="height:25px;margin:5px 0;">삭제</a>
							</div>
						<?
							}
						?>
						<? if( $i == 0 ){ ?>
							<div style="float:left;width:100%;height:30px;text-align:left;font-size:12px;padding-left:8px;padding-top:6px;border:1px solid #eceff4;background:#fff;">
								결제수단 데이터가 없습니다.
							</div>
						<? } ?>
					</div>

				</div>


			</span>
		<?
			}

		}
		?>

<!--		<span id="show_req_btn"><input type="button" name="submitChecked" onClick="pay_approval();" value="결제하기" class="btn_submit"></span>-->
        <input type="hidden" name="submitChecked" value="결제하기">
        <button type="button" class="btn-payment" onclick="pay_approval();" id="show_req_btn"><span>결제하기</span></button>
		<button type="button" class="btn-payment" onclick="forderform_check();" id="show_pay_btn" style="display:none;"><span>주문하기</span></button>
		<!--<span id="show_pay_btn" style="display:none;"><input type="button" onClick="forderform_check();" value="주문하기" class="btn_submit"></span>-->
		<!--<a href="<?php /*echo G5_SHOP_URL; */?>" class="btn_cancel">취소</a>-->

	<?}?>
</div>
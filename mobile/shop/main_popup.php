<!doctype html>
<html lang="ko-KR">

<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

	<title>orderstation</title>
    <!-- jQuery library (served from Google) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bxslider/4.2.15/jquery.bxslider.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bxslider/4.2.15/jquery.bxslider.min.css" rel="stylesheet" />
    <style>
   

        /*
        * ------------------------------
        * popup
        * ------------------------------
        */

        .modal-popup {
            position: fixed;
            width: 100%;
            height: 100%;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            background-color: rgba(0, 0, 0, 0.7);
            opacity: 0;
            transition: all 0.3s;
            z-index: 9999;
            display: none;
            max-width: 768px;
            margin-left: auto;
            margin-right: auto;
            -webkit-align-items: center;
            align-items: center;
            -webkit-justify-content: center;
            justify-content: center;
        }

        .modal-popup.on {
            display: -webkit-flex;
            display: flex;
            opacity: 1;
        }
        .modal-popup .modal-wrap {
            display: flex;
            flex-direction: column;
            position: relative;
            max-width: 100%;
            max-height: 100%;
            margin: 0 20px;
            background: #f3f3f3;
            height: initial;
            overflow: hidden;
        }
        /* event */
        .modal-popup.modal-event .modal-wrap {
            max-width: 100%;
            width: 300px;
            margin: 0 40px;
        }
      

        /*
        * ------------------------------
        * popup - header
        * ------------------------------
        */

        /*
        * ------------------------------
        * popup - content
        * ------------------------------
        */
        .modal-popup .modal-body {
            position: relative;
            flex: 0 1 auto;
            padding: 20px 20px;
            background: #fff;
            overflow: auto;
        }
        /*
        * ------------------------------
        * popup - button
        * ------------------------------
        */
        .btn-right {
            display: flex;
            justify-content: flex-end;
            padding: 10px 20px;
            border-top: 1px solid #ccc;
            background: #fff;
        }
        .modal-btn {
            border: 0;
            background: none;
            color: #252525;
            font-size: 14px;
            font-weight: bold;
        }

        /*
        * ------------------------------
        * bxslider - 오버라이드
        * ------------------------------
        */
        .modal-popup.modal-event .bx-wrapper {
            margin-bottom: 20px;
            box-shadow: none;
            border: none;
        }
        .modal-popup.modal-event .bx-wrapper .bx-pager {
            padding-top: 0;
        }
        .modal-popup.modal-event .bx-wrapper .bx-controls-auto .bx-controls-auto-item, 
        .modal-popup.modal-event .bx-wrapper .bx-pager-item {
            vertical-align: middle;
        }
        .modal-popup.modal-event .bx-wrapper .bx-pager {
            font-size: 10px; 
        }
        .modal-popup.modal-event .bx-wrapper .bx-controls-auto, 
        .modal-popup.modal-event .bx-wrapper .bx-pager {
            bottom: -20px;
        }

        .event-slider {}
        .event-slider .slider { width: 100px; min-height: 100px;}
    </style>
</head>

<body>
    <!-- 모바일 팝업  -->
	<!-- [D] 팝업열기 방법1 : .on 클래스 추가시 팝업 view -->
	<!-- [D] 팝업열기 방법2 : data-pop(팝업창) 의 이름과 data-pop-btn(팝업여는 버튼) 의 이름이 같아야 함 -->
	
    <section class="modal-popup modal-event on" data-pop="eventPop">
		<div class="modal-wrap">
			<div class="modal-body">
                <div class="event-slider">
                    <div class="slider"><img src="https://source.unsplash.com/user/erondu/1600x900"></div>
                    <div class="slider"><img src="https://source.unsplash.com/user/erondu/1600x900"></div>
                    <div class="slider"><img src="https://source.unsplash.com/user/erondu/1600x900"></div>
                    <div class="slider"><img src="https://source.unsplash.com/user/erondu/1600x900"></div>
                </div>
			</div>
			<div class="btn-box btn-right">
				<button type="button" data-pop-btn="eventPop" class="modal-btn modal-close">닫기</button>
			</div>
		</div>
	</section>


   
 
    <script>
        $(document).ready(function(){
            $('.event-slider').bxSlider( {
                mode: 'horizontal',// 가로 방향 수평 슬라이드
                speed: 500,        // 이동 속도를 설정
                pager: true,      // 현재 위치 페이징 표시 여부 설정
                moveSlides: 1,     // 슬라이드 이동시 개수
                // slideWidth: 100,   // 슬라이드 너비
                // minSlides: 4,      // 최소 노출 개수
                // maxSlides: 4,      // 최대 노출 개수
                // slideMargin: 5,    // 슬라이드간의 간격
                // auto: true,        // 자동 실행 여부
                // autoHover: true,   // 마우스 호버시 정지 여부
                controls: true    // 이전 다음 버튼 노출 여부
            });
        });
    </script>
    <script>
        
    
    $(document).ready(function() {
        popupShowHide(); //팝업 
    });

    /* =====================================
    * 레이어 팝업
    * =====================================*/
    function popupShowHide() {
        var openBtn = '[data-pop-btn]',
            closeBtn = '.modal-close';

        function getTarget(t) {
            return $(t).attr('data-pop-btn');
        }
        function open(t) { //팝업열기
            var showTarget = $('[data-pop="' + t + '"]');
            showTarget.addClass('on').attr('aria-modal', true).find('.modal-close').focus();
            showTarget.find('.modal-close').data('activeTarget', t);
        }
        function close(t) { //팝업닫기
            var activeTarget = $('[data-pop="' + t + '"]');
            activeTarget.removeClass('on').removeAttr('aria-modal'); //팝업닫기
            $('[data-pop-btn="' + t + '"]').focus(); //버튼으로 포커스 가도록
        }
        $(document).on('click', openBtn, function (e) {
            e.preventDefault();
            open(getTarget(this));
        })
        .on('click', closeBtn, function (e) {
            e.preventDefault();
            close($(this).data('activeTarget'));
        })
    
    }
    </script>



</body>

</html>
<?php
include_once('./_common.php');

// 테마에 mypage.php 있으면 include
if(defined('G5_THEME_SHOP_PATH')) {
    $theme_mypage_file = G5_THEME_MSHOP_PATH.'/mypage.php';
    if(is_file($theme_mypage_file)) {
        include_once($theme_mypage_file);
        return;
        unset($theme_mypage_file);
    }
}

$g5['title'] = '마이페이지';
include_once(G5_MSHOP_PATH.'/_head.php');

// 쿠폰
$cp_count = 0;
$sql = " select cp_id
            from {$g5['g5_shop_coupon_table']}
            where mb_id IN ( '{$member['mb_id']}', '소비자회원' )
              and cp_start <= '".G5_TIME_YMD."'
              and cp_end >= '".G5_TIME_YMD."'  and service_type is null ";
$res = sql_query($sql);

for($k=0; $cp=sql_fetch_array($res); $k++) {
    if(!is_used_coupon($member['mb_id'], $cp['cp_id']))
        $cp_count++;
}


$sql = " select pharm_name, mb_name
            from {$g5['member_table']}
            where mb_id = '{$member['mb_recommend']}' ";
$pharm = sql_fetch($sql);

?>

<div id="smb_my">

    <section id="smb_my_ov">
        <h2>회원정보 개요</h2>
        <div class="my_name">
            <img src="<?php echo G5_THEME_IMG_URL ;?>/no_profile.gif" alt="프로필이미지" width="20"> <strong><?php echo $member['mb_id'] ? $member['mb_name'] : '비회원'; ?></strong>님
            <ul class="smb_my_act">
                <?php if ($is_admin == 'super') { ?><li><a href="<?php echo G5_ADMIN_URL; ?>/" class="btn_admin">관리자</a></li><?php } ?>
                <li><a href="<?php echo G5_BBS_URL; ?>/member_confirm.php?url=register_form.php" class="btn01">정보수정</a></li>
                <li><a href="<?php echo G5_BBS_URL; ?>/member_confirm.php?url=member_leave.php" onclick="return member_leave();" class="btn01">회원탈퇴</a></li>
            </ul>
        </div>
        <ul class="my_pocou">
			<?php if($member['mb_type'] == 0) { ?>
            <li  class="my_recommend">나의추천약사 &nbsp; <span><?php echo $pharm['pharm_name'];?></span>  &nbsp; <span><?php echo $pharm['mb_name'];?></span> 약사님</li>
			<?php } ?>
			<li  class="my_cou">보유쿠폰<a href="<?php echo G5_SHOP_URL; ?>/coupon.php" target="_blank" class="win_coupon">쿠폰보기 [ <?php echo number_format($cp_count); ?> ]</a></li>
            <?php if (in_array($member['mb_id'], $TempIdArray)) { ?>
				<li class="my_point">보유포인트
					<a href="<?php echo G5_BBS_URL; ?>/point.php" target="_blank" class="win_point"><?php echo number_format(point_expire_check($member['mb_id'])); ?>점</a>
				</li>
			<?php } ?>
            <?php if ($member['mb_referee']==4 || $member['mb_extra']==11) { ?>
            <li class="my_point">보유적립금
                <a href="<?php echo G5_BBS_URL; ?>/cash.php" target="_blank" class="win_cash"><?php echo number_format(cash_expire_check($member['mb_id'])); ?> 원</a>
            </li>
                <?php }?>
        </ul>
        <div class="my_info">
            <div class="my_info_wr">
                <strong>연락처</strong>
                <span><?php echo ($member['mb_hp'] ? $member['mb_hp'] : '미등록'); ?></span>
            </div>
            <div class="my_info_wr">
                <strong>E-Mail</strong>
                <span><?php echo ($member['mb_email'] ? $member['mb_email'] : '미등록'); ?></span>
            </div>
            <div class="my_info_wr">
                <strong>최종접속일시</strong>
                <span><?php echo $member['mb_today_login']; ?></span>
             </div>
            <div class="my_info_wr">
            <strong>회원가입일시</strong>
                <span><?php echo $member['mb_datetime']; ?></span>
            </div>
            <div class="my_info_wr ov_addr">
                <strong>주소</strong>
                <span><?php echo sprintf("(%s%s)", $member['mb_zip1'], $member['mb_zip2']).' '.print_address($member['mb_addr1'], $member['mb_addr2'], $member['mb_addr3'], $member['mb_addr_jibeon']); ?></span>
            </div>
        </div>
        <div class="my_ov_btn"><button type="button" class="btn_op_area"><i class="fa fa-caret-down" aria-hidden="true"></i><span class="sound_only">상세정보 보기</span></button></div>

    </section>

    <script>
    
        $(".btn_op_area").on("click", function() {
            $(".my_info").toggle();
            $(".fa-caret-down").toggleClass("fa-caret-up")
        });

    </script>
	<hr class="layout">
	<div id="contents">
		<div id="titleArea">
			<h2>마이페이지</h2>
			<span class="xans-element- xans-layout xans-layout-mobileaction "><a href="#none" onclick="history.go(-1);return false;"><img src="<?php echo G5_THEME_URL.'/img/'.G5_MOBILE_DIR;?>/btn_back.gif" width="33" alt="뒤로가기"></a>
			</span>
		</div>
		<div class="xans-element- xans-board xans-board-mainpackage ">
			<div class="xans-element- xans-layout xans-layout-boardinfo">
				<ul>
					<li class="xans-record-"><a href="<?php echo G5_SHOP_URL; ?>/orderinquiry.php">주문배송조회</a></li>
					<li class="xans-record-"><a href="<?php echo G5_SHOP_URL; ?>/myauto.php">정기주문내역</a></li>
					<?php if ($member['mb_type'] == 1) { ?>
					<li class="xans-record-"><a href="<?php echo G5_SHOP_URL; ?>/myuser.php">고객관리</a></li>
					<li class="xans-record-"><a href="<?php echo G5_SHOP_URL; ?>/myrecord.php">정산조회</a></li>					
					<li class="xans-record-"><a href="<?php echo G5_SHOP_URL; ?>/mybank.php">개인소득정보관리</a></li>
					<li class="xans-record-"><a href="<?php echo G5_SHOP_URL; ?>/myincom.php">개인소득지급관리</a></li>
					<?php } ?>

					<li class="xans-record-"><a href="<?php echo G5_SHOP_URL; ?>/qalist.php">1:1건강상담</a></li>
					<li class="xans-record-"><a href="<?php echo G5_SHOP_URL; ?>/wishlist.php">관심상품</a></li>

					<li class="xans-record-"><a href="/shop/itemuselist.php">상품 사용후기</a></li>
					<li class="xans-record-"><a href="/shop/itemqalist.php">상품 Q&A</a></li>
					<?php if( ( $member['mb_type'] == 0 && !empty($member['mb_id']) ) || $member['mb_id'] == "admin" ) { ?>
					<li class="xans-record-"><a href="/shop/coupon_reg.php">쿠폰 등록하기</a></li>
					<!--<li class="xans-record-"><a href="/shop/coupon_reg2.php">쿠폰 인증하기</a></li>-->
					<? } ?>
				</ul>
			</div>
		</div>
	</div>
	<hr class="layout">

</div>

<script>
$(function() {
    $(".win_coupon").click(function() {
        var new_win = window.open($(this).attr("href"), "win_coupon", "left=100,top=100,width=700, height=600, scrollbars=1");
        new_win.focus();
        return false;
    });
});

function member_leave()
{
    return confirm('정말 회원에서 탈퇴 하시겠습니까?')
}
</script>

<?php
include_once(G5_MSHOP_PATH.'/_tail.php');
?>
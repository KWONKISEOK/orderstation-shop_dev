<?php
include_once('./_common.php');

$sql = " select * from {$g5['g5_shop_event_table']}
          where ev_id = '$ev_id'
            and ev_use = 1 ";
$ev = sql_fetch($sql);
if (!$ev['ev_id'])
    alert('등록된 이벤트가 없습니다.');

$g5['title'] = $ev['ev_subject'];
include_once(G5_MSHOP_PATH.'/_head.php');

if ($is_admin)
    echo '<div class="sev_admin"><a href="'.G5_ADMIN_URL.'/shop_admin/itemeventform.php?w=u&amp;ev_id='.$ev['ev_id'].'" class="btn_admin">이벤트 관리</a></div>';
?>

<div id="sev_list">

	<!-- ********************* 소셜 공유하기 ********************* -->
	<div style="padding:10px 10px 10px 0;float:right;">

		<div class="social">
		
		<?php 
			$sns_url   = G5_SHOP_URL.'/event.php?ev_id='.$ev_id;
			$sns_title = $ev['ev_subject'];
			$sns_view_img = '';

			$str = get_sns_share_link('facebook', $sns_url, $sns_title, '/theme/onk/img/icon_facebook.gif');
			$str .= '&nbsp;';
			$str .= get_sns_share_link('twitter', $sns_url, $sns_title, '/theme/onk/img/icon_twitter.gif');
			echo $str;
		?>
		<?php echo get_sns_share_link('kakaotalk', $sns_url, $sns_title, G5_THEME_URL.'/img/kakaolink_btn.png'); ?>	
		
		</div>

	</div>
	<!-- ************************************************** -->

<!-- 이벤트 시작 { -->
<?
if($ev['ev_head_html'] =='') { ?>
<style>
 .function {
    overflow: hidden;
    text-align: right;
    line-height: 38px;
    margin-bottom: 20px;
}
.prdCount {
    float: left;
    color: #888888;
    font-size: 12px;
    letter-spacing: 1px;
}
.xans-product-normalmenu ul#type li {
    display: inline;
    border: 1px solid #e9e9e9;
    padding: 3px 10px 4px;
    font-size: 11px;
}
.xans-product-normalmenu ul#type li a{
    color: #9d9d9d;
}
.xans-product-normalmenu ul#type li .active {
    color: #ff8800;
}

</style>
<div class="xans-product-menupackage "><h2><span><?php echo $ev['ev_subject']; ?></span></h2></div>
<br/><br/>
<?php } ?>

<?php
//천년순수 홍삼
if($ev_id=='1555650939') {
?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jQuery-rwdImageMaps/1.6/jquery.rwdImageMaps.min.js"></script>
<script>
  $(document).ready(function(e) {
    $('img[usemap]').rwdImageMaps();
  });
</script>
<p><img src="/data/editor/1904/ad74a75fc936ee5021b87572d831e15c_1555650930_2585.jpg" usemap="#image-maps" style="width:100%;"><br style="clear:both;">&nbsp;</p>
<map name="image-maps">
<? if( !is_mobile() ){ ?>
<area  alt="" title="" href="https://docs.google.com/forms/d/e/1FAIpQLSee5PaxDofdLxp3f82rBGBIop0tqr-qmSeTBIE4kX57WW3LFg/viewform?usp=sf_link " shape="rect" coords="330,2102,871,2217" target="_blank"/>
<? }else{ ?>
<area  alt="" title="" href="javascript:alert('pc버전에서 신청해주세요');" shape="rect" coords="330,2102,871,2217" target="_blank"/>
<? } ?>
</map>
<?
//5월 가정의 달 기획전
}else if($ev_id=='1556499814') {
?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jQuery-rwdImageMaps/1.6/jquery.rwdImageMaps.min.js"></script>
<script>
  $(document).ready(function(e) {
    $('img[usemap]').rwdImageMaps();
  });
</script>
<p><img src="/data/editor/1904/d5ba22516a0acad82ec51248e16fa930_1556511727_1475.jpg" usemap="#image-maps" style="width:100%;"><br style="clear:both;">&nbsp;</p>
<map name="image-maps">
<area  alt="" title="" href="/shop/event.php?ev_id=1556500267" shape="rect" coords="260,1933,415,2096"		/>
<area  alt="" title="" href="/shop/event.php?ev_id=1556500374" shape="rect" coords="433,1934,588,2097"      />
<area  alt="" title="" href="/shop/event.php?ev_id=1556500434" shape="rect" coords="613,1935,768,2098"      />
<area  alt="" title="" href="/shop/event.php?ev_id=1556500582" shape="rect" coords="788,1934,943,2097"      />
<area  alt="" title="" href="/shop/event.php?ev_id=1556500685" shape="rect" coords="259,2117,414,2280"      />
<area  alt="" title="" href="/shop/event.php?ev_id=1556500706" shape="rect" coords="435,2118,590,2281"      />
<area  alt="" title="" href="/shop/event.php?ev_id=1556500776" shape="rect" coords="612,2116,767,2279"      />
<area  alt="" title="" href="/shop/event.php?ev_id=1556501025" shape="rect" coords="785,2116,940,2302"      />
<area  alt="" title="" href="/shop/event.php?ev_id=1556842047" shape="rect" coords="448,2326,752,2389"		/>
</map>
<?
//미세먼지&황사 기획전
}else if($ev_id=='1551673828') {
?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jQuery-rwdImageMaps/1.6/jquery.rwdImageMaps.min.js"></script>
<script>
  $(document).ready(function(e) {
    $('img[usemap]').rwdImageMaps();
  });
</script>
<p><img src="/data/editor/1903/5546fc5a6bc75c9d3d3c25aa1237f9be_1551675994_1549.jpg" usemap="#image-maps" style="width:100%;"><br style="clear:both;">&nbsp;</p>
<map name="image-maps">
<area  alt="" title="" href="/shop/event.php?ev_id=1551676085" shape="rect" coords="90,1437,417,1936"/>
<area  alt="" title="" href="/shop/event.php?ev_id=1551676863" shape="rect" coords="428,1440,773,1939"/>
<area  alt="" title="" href="/shop/event.php?ev_id=1551676911" shape="rect" coords="784,1439,1136,1938"/>
<area  alt="" title="" href="/shop/event.php?ev_id=1551676956" shape="rect" coords="66,2205,418,2704"/>
<area  alt="" title="" href="/shop/event.php?ev_id=1551677010" shape="rect" coords="423,2206,775,2705"/>
<area  alt="" title="" href="/shop/event.php?ev_id=1551677055" shape="rect" coords="783,2205,1135,2704"/>
<area  alt="" title="" href="/shop/event.php?ev_id=1551677107" shape="rect" coords="65,2983,417,3482"/>
<area  alt="" title="" href="/shop/event.php?ev_id=1551677218" shape="rect" coords="423,2984,775,3483"/>
<area  alt="" title="" href="/shop/event.php?ev_id=1551677263" shape="rect" coords="784,2983,1136,3482"/>
</map>
<?
}else if($ev_id=='1551743206') { //오에스데이 
?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jQuery-rwdImageMaps/1.6/jquery.rwdImageMaps.min.js"></script>
<script>
  $(document).ready(function(e) {
    $('img[usemap]').rwdImageMaps();
  });
</script>
<p><img src="/data/editor/1903/e0823dd6771d401c20b21a58352d57b8_1551743181_0795.jpg" usemap="#image-maps" style="width:100%;"><br style="clear:both;">&nbsp;</p>
<map name="image-maps">
<area  alt="" title="" href="/shop/event.php?ev_id=1551743408" shape="rect" coords="38,1119,585,1952"/>
<area  alt="" title="" href="/shop/event.php?ev_id=1551743537" shape="rect" coords="619,1117,1166,1950"/>
</map>
<?
}else if($ev_id=='1554422517') { //오에스데이 건강한 봄나들이
?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jQuery-rwdImageMaps/1.6/jquery.rwdImageMaps.min.js"></script>
<script>
  $(document).ready(function(e) {
    $('img[usemap]').rwdImageMaps();
  });
</script>
<p><img src="/data/editor/1904/50419d95e41fdd7a3bc606183fd5d45a_1554422542_5549.jpg" usemap="#image-maps" style="width:100%;"><br style="clear:both;">&nbsp;</p>
<map name="image-maps">
<area  alt="" title="" href="/shop/event.php?ev_id=1554422658" shape="rect" coords="25,1081,1175,1676"/>
</map>
<?
}else{

// 상단 HTML
echo '<div id="sev_hhtml" >'.conv_content($ev['ev_head_html'], 1).'</div>';

}

// 상품 출력순서가 있다면
if ($sort != "")
    $order_by = $sort.' '.$sortodr.' , b.it_order, b.it_id desc';
else
    $order_by = 'b.it_order, b.it_id desc';

if ($skin) {
    $skin = preg_replace('#\.+/#', '', $skin);
    $ev['ev_skin'] = $skin;
}

if($ev_id != 1530591545) {
	define('G5_SHOP_CSS_URL', G5_MSHOP_SKIN_URL);

	// 리스트 유형별로 출력
	$list_file = G5_SHOP_SKIN_PATH."/{$ev['ev_mobile_skin']}";
	if (file_exists($list_file))
	{
		include G5_MSHOP_SKIN_PATH.'/list.sort.skin.php';

		// 총몇개 = 한줄에 몇개 * 몇줄
		$items = $ev['ev_mobile_list_mod'] * $ev['ev_mobile_list_row'];
		// 페이지가 없으면 첫 페이지 (1 페이지)
		if ($page < 1) $page = 1;
		// 시작 레코드 구함
		$from_record = ($page - 1) * $items;

		$list = new item_list(G5_MSHOP_SKIN_PATH.'/'.$ev['ev_mobile_skin'], $ev['ev_mobile_list_mod'], $ev['ev_mobile_list_row'], $ev['ev_mobile_img_width'], $ev['ev_mobile_img_height']);
		$list->set_mb_type($member['mb_type']);
		$list->set_event($ev['ev_id']);
		$list->set_is_page(true);
		$list->set_mobile(true);
		$list->set_order_by($order_by);
		$list->set_from_record($from_record);
		$list->set_view('it_img', true);
		$list->set_view('it_id', false);
		$list->set_view('it_name', true);
		$list->set_view('it_cust_price', false);
		$list->set_view('it_price', true);
		$list->set_view('it_icon', true);
		$list->set_view('sns', true);
        $list->set_price_list($maxPrice, $minPrice);
		echo $list->run();

		//실행되는 쿼리를 얻어와서 맨 첫번째 이미지를 가지고 온다 og 태그대표이미지를 뿌려주기 위해서이다.
		$getquery = $list->return_query();
		
		$first_img = "";
		if( !empty($getquery) ){
			$get_event_row = sql_query($getquery);
			for ($i=0; $event_row=sql_fetch_array($get_event_row); $i++) {
				if( $i == 0 ){
					$first_img = G5_DATA_URL."/item/".$event_row['it_img1'];
				}else{
					break;
				}
			}
		}

		// where 된 전체 상품수
		$total_count = $list->total_count;
		// 전체 페이지 계산
		$total_page  = ceil($total_count / $items);
	}
	else
	{
		echo '<div align="center">'.$ev['ev_mobile_skin'].' 파일을 찾을 수 없습니다.<br>관리자에게 알려주시면 감사하겠습니다.</div>';
	}


	?>
	<?php
	$qstr .= 'skin='.$skin.'&amp;ev_id='.$ev_id.'&amp;sort='.$sort.'&amp;sortodr='.$sortodr;
	echo get_paging($config['cf_mobile_pages'], $page, $total_page, "{$_SERVER['SCRIPT_NAME']}?$qstr&amp;page=");
}
	?>

<?php
// 하단 HTML
echo '<div id="sev_thtml">'.conv_content($ev['ev_tail_html'], 1).'</div>';
?>
<!-- } 이벤트 끝 -->
</div>

<?
if($config['cf_kakao_js_apikey']) {
?>
<!-- <script src="https://developers.kakao.com/sdk/js/kakao.min.js"></script> -->
<script src="//developers.kakao.com/sdk/js/kakao.min.js"></script>
<!-- <script src="<?php echo G5_JS_URL; ?>/kakaolink.js"></script> -->
<?
}
?>

	<script type='text/javascript'>
	  //<![CDATA[
		// // 사용할 앱의 JavaScript 키를 설정해 주세요.
		Kakao.init("<?php echo $config['cf_kakao_js_apikey']; ?>");
		// // 카카오링크 버튼을 생성합니다. 처음 한번만 호출하면 됩니다.
		Kakao.Link.createDefaultButton({
		  container: '#kakao-link-btn',
		  objectType: 'feed',
		  content: {
			title: '<?php echo $sns_title;?>',
			description: '',
			imageUrl: '<?php echo $first_img;?>',
			link: {
			  mobileWebUrl: '<?php echo $sns_url;?>',
			  webUrl: '<?php echo $sns_url;?>'
			}
		  },
		  /*social: {
			likeCount: 286,
			commentCount: 45,
			sharedCount: 845
		  },*/
		  buttons: [{
			title: '이벤트 보기',
			link: {
			  mobileWebUrl: '<?php echo $sns_url;?>',
			  webUrl: '<?php echo $sns_url;?>'
			}
		  }]
		});
	  //]]>
	</script>			

<?php
include_once(G5_MSHOP_PATH.'/_tail.php');
?>

<?php
include_once('./_common.php');

// 테마에 cart.php 있으면 include
if(defined('G5_THEME_MSHOP_PATH')) {
    $theme_cart_file = G5_THEME_MSHOP_PATH.'/cart.php';
    if(is_file($theme_cart_file)) {
        include_once($theme_cart_file);
        return;
        unset($theme_cart_file);
    }
}

$g5['title'] = '장바구니';
include_once(G5_MSHOP_PATH.'/_head.php');


?>


<script src="<?php echo G5_JS_URL; ?>/shop.js?ver=<?php echo G5_JS_VER; ?>"></script>
<script src="<?php echo G5_JS_URL; ?>/shop.override.js?ver=<?php echo G5_JS_VER; ?>"></script>
<style>
.xans-order-tabinfo {
	margin:0 10px 0 10px ;
}
#titleArea {
    /*margin-left:10px;*/
}
.xans-order-tabinfo.tab.typeStrong.gFlex li.selected {
    border: 1px solid #444444;
    border-bottom: 0;
    box-sizing: border-box;
    height: 36px;
}
.xans-order-tabinfo.tab.typeStrong.gFlex li {
    float: left;
    width: 50%;
    height: 35px;
    font-size: 0;
    text-align: center;
    vertical-align: middle;
    border-bottom: 1px solid #444444;
}
.xans-order-tabinfo.tab.typeStrong.gFlex li a {
    display: inline-block;
    width: 100%;
    color: #000;
    font-size: 13px;
    line-height: 35px;
}


.mod_options_auto2 {
    padding: 0 7px;
    border: 1px solid #38b2b9;
    color: #38b2b9;
    background: #fff;
    height: 23px;
    line-height: 21px;
    margin: 5px 0 0;
}
</style>
<div id="sod_bsk">


    <form name="frmcartlist" id="sod_bsk_list" class="2017_renewal_itemform" method="post" action="<?php echo $cart_action_url; ?>">

<div id="titleArea" <?php if (!$is_member) echo 'style="border-bottom:1px solid #444444;"';?>>
    <h2>장바구니</h2>
    <!-- <span class="xans-element- xans-layout xans-layout-mobileaction "><a href="#none" onclick="history.go(-1);return false;">
	<img src="//img.echosting.cafe24.com/skin/mobile_ko_KR/layout/btn_back.gif" width="33" alt="뒤로가기"></a> -->
</span>
</div>

<?php if ($member[mb_type] != '1') { ?>
    <?php if ($is_member) { ?>
        <div class="xans-element- xans-order xans-order-tabinfo tab typeStrong gFlex ">
            <ul>
                <li class="<?php if ($period != 'y') echo 'selected'; ?> ">
                    <a href="/shop/cart.php">일반주문</a>
                </li>
                <li class="<?php if ($period == 'y') echo 'selected'; ?>">
                    <a href="/shop/cart.php?period=y">정기주문</a>
                </li>
            </ul>
        </div>
        <br/><br/>
    <?php } ?>
<?php } ?>

    <?//php if($cart_count) { ?>
	<!--
    <div id="sod_chk" style="display:none;">
        <label for="ct_all" class="sound_only">상품 전체</label>
        <input type="checkbox" name="ct_all" value="1" id="ct_all" checked>
        전체상품 선택 
    </div>
	-->

    <?//php } ?>

	<div id="CartInfo">

	<?
		//웹 장바구니 공통 include 페이지
		include_once(G5_MSHOP_PATH.'/cart_common.php');	
	?>

	</div>

	<?php if ($i != 0) { ?>
    <div id="sod_bsk_act" class="btn_confirm">

        <input type="hidden" name="url" value="<?php echo G5_SHOP_URL; ?>/orderform.php">
        <input type="hidden" name="act" value="">
        <input type="hidden" name="records" value="<?php echo $i; ?>">
		<input type="hidden" name="ct_period_yn" value="<?php echo $period; ?>">
        <input type="hidden" name="del_it_id" value="">

        <?php if ($naverpay_button_js) { ?>
        <div class="naverpay-cart"><?php echo $naverpay_request_js.$naverpay_button_js; ?></div>
        <?php } ?>
    </div>
    <?php } ?>


    </div>
    </form>

</div>

<script>

var ClickComp = "";

$(function() {
    var close_btn_idx;

    // 선택사항수정
    $(document).on('click', '.mod_options2',function(e) {
        var it_id = $(this).attr("id").replace("mod_opt_", "");
        var $this = $(this);
        close_btn_idx = $(".mod_options2").index($(this));

        $.post(
            "./cartoption.php",
            { it_id: it_id },
            function(data) {
                $("#mod_option_frm").remove();
                //$this.parent().parent().after("<div id=\"mod_option_frm\"></div>");
				$this.parent().parent().parent().before("<div id=\"mod_option_frm\"></div>");
                $("#mod_option_frm").html(data);
                price_calculate();
				e.stopImmediatePropagation();
            }
        );
    });
	// 정기주문
    $(document).on('click', '.mod_options_auto2',function(e) {
        var it_id = $(this).attr("id").replace("mod_opt_", "");
        var $this = $(this);
        close_btn_idx = $(".mod_options2").index($(this));

        $.post(
            "./cartoption_auto.php",
            { it_id: it_id },
            function(data) {
                $("#mod_option_frm").remove();
                //$this.after("<div id=\"mod_option_frm\"></div>");
				$this.parent().parent().parent().before("<div id=\"mod_option_frm\"></div>");
                $("#mod_option_frm").html(data);
				e.stopImmediatePropagation();
                //price_calculate();
            }
        );
    });

    // 옵션수정 닫기
    $(document).on("click", "#mod_option_close", function(e) {
        $("#mod_option_frm").remove();
        $("#win_mask, .window").hide();
        $(".mod_options2").eq(close_btn_idx).focus();
		e.stopImmediatePropagation();
    });
    $(document).on("click", "#win_mask", function(e) {
        $("#mod_option_frm").remove();
        $("#win_mask").hide();
        $(".mod_options2").eq(close_btn_idx).focus();
		e.stopImmediatePropagation();
    });

});

function fsubmit_check(f) {
    if($("input[name^=ct_chk]:checked").length < 1) {
        alert("구매하실 상품을 하나이상 선택해 주십시오.");
        return false;
    }

    return true;
}

function form_check(act,del_it_id) {
    var f = document.frmcartlist;
    var cnt = f.records.value;

    if (act == "buy")
    {
		
		<?php if($member['mb_type'] != 1 and $period=='y') { ?>
		if($("input[name^=ct_chk]:checked").length > 1) {
            alert("정기주문은 한상품씩 주문가능합니다.");
            return false;
        }
		<?php } ?>

		var i=0;
		var send_yn = "Y";
		$("input[name^=ct_chk]:checked").each(function() {

			if( parseInt( $("#ct_chk_"+i).data( "change_yn" ) ) > 0 ){
				send_yn = "N";
			}
			i++;

		});

		if( send_yn == "N" ){
			alert("가격변동이 있는 상품이 있습니다.가격변동상품을 삭제후 다시 주문하십시요.");
			return false;
		}

		<? if( $TimeGoodsYn == "Y" ){ ?>
			alert("타임세일 상품은 세일 시간에만 구매 가능합니다.");
			return false;		
		<? } ?>

		<? if( $item_Soldout == "Y" ){ ?>
			alert("품절된 상품이 있습니다. 삭제해주세요.");
			return false;		
		<? } ?>

        f.act.value = act;
        f.submit();
    }
    else if (act == "xdel")
    {
		f.del_it_id.value = del_it_id;
        f.act.value = act;
        f.submit();
		f.del_it_id.value = "";
    }
    else if (act == "alldelete")
    {
        f.act.value = act;
        f.submit();
    }
    else if (act == "seldelete")
    {
        if($("input[name^=ct_chk]:checked").length < 1) {
            alert("삭제하실 상품을 하나이상 선택해 주십시오.");
            return false;
        }

        f.act.value = act;
        f.submit();
    }

    return true;
}

function fnc_suselect2(str_gbn,obj) {
	if (str_gbn==0) {
		if (parseInt(document.getElementById(obj).value) > 1) {
			document.getElementById(obj).value = parseInt(document.getElementById(obj).value) -1;
		}
	} else {
		if (parseInt(document.getElementById(obj).value) < 5) {
			document.getElementById(obj).value = parseInt(document.getElementById(obj).value) +1;
		}
	}
}

var loopcnt=0;

//공급사 전체선택 트리거
$(".ct_all").trigger('click');

//공급사 상품선택 트리거
$("input[name^=ct_chk]").each(function() {

	$("#ct_chk_"+loopcnt).trigger('click');

	loopcnt++;

});

$(document).on('click', 'input[class^=ct_all]',function(e) {

	var i=0;
	ClickComp = $(this).data("comp");

	if( $(this).is(":checked") == true ){
	
		$("input[name^=ct_chk]").each(function() {

			if( $("#ct_chk_"+i).data("comp") == ClickComp ){
				$("#ct_chk_"+i).attr("checked", true);
			}
	
			i++;

		});

	}

	if( $(this).is(":checked") == false ){
	
		$("input[name^=ct_chk]").each(function() {

			if( $("#ct_chk_"+i).data("comp") == ClickComp ){
				$("#ct_chk_"+i).attr("checked", false);
			}
	
			i++;

		});

	}

});

$(document).on('click', 'input[class^=ct_all] , input[name^=ct_chk]',function(e) {

	var i=0;
	var it_id = "";
	var comp_code = "";
	var choice_cnt = 0;

	//공급사선택값
	$("input[class^=ct_all]").each(function() {
		
		var ThisCompCode = $(this).data("comp");

		if( $(this).is(":checked") ){

			if( comp_code != "" ){
				comp_code += "|";
			}

			comp_code += ThisCompCode;

		}
	
	});


	//상품선택값
	$("input[name^=ct_chk]").each(function() {

		if( $("#ct_chk_"+i).is(":checked") ){

			if( it_id != "" ){
				it_id += "|";
			}

			it_id += $("#ct_chk_"+i).data( "it_id" );
			choice_cnt++;

		}

		i++;
	
	});
	
	$.ajax({
		url: "/mobile/shop/cart_choice.php",
		type: 'POST',
		data: {
			'it_id': it_id,
			'comp_code': comp_code,
			'period': '<?=$period?>'
		},
		dataType: 'html',
		async: false,
		success: function(data, textStatus) {
			if (data.error) {
				alert(data.error);
				return false;
			} else {

				if( data == "" ){
					alert("다시 시도해주십시요.");
					location.reload();
				}else{
					
					$("#CartInfo").empty();
					$("#CartInfo").append(data);

					//선택한 상품의 갯수를 버튼에 표시해주기
					if( choice_cnt > 0 ){
						$(".btn_submit").html(choice_cnt+"건 주문하기");
					}


					//이벤트의 기본 동작을 중단한다.
					e.stopImmediatePropagation();

				}

			}
		}
	});

});

//공급사 전체선택 트리거
$(".ct_all").trigger('click');


$(document).on('click', '.smRLa1',function(e) {

	e.preventDefault();

	//몇번째 데이터인지 알아보기
	var index = $(".smRLa1").index(this);

	$(".crtonbox:eq(" + index + ")").slideToggle( 'fast' );

});

</script>

<?php
include_once(G5_MSHOP_PATH.'/_tail.php');
?>
<?php
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가

if ($_REQUEST['ct_period_yn']) $period = $_REQUEST['ct_period_yn'];

if($period == 'y') {
    // 정기주문
    $default['de_inicis_mid'] = INICIS_R_MID;
    $default['de_inicis_sign_key'] = INICIS_R_SIGN_KEY;
} else {
    // 일반주문
    $default['de_inicis_mid'] = INICIS_MID;
    $default['de_inicis_sign_key'] = INICIS_SIGN_KEY;

    //$default['de_inicis_mid'] = 'INIpayTest';
    //$default['de_inicis_sign_key'] = 'SU5JTElURV9UUklQTEVERVNfS0VZU1RS';

}

require_once(G5_MSHOP_PATH.'/settle_'.$default['de_pg_service'].'.inc.php');
require_once(G5_SHOP_PATH.'/settle_kakaopay.inc.php');

if( is_inicis_simple_pay() ){   //이니시스 삼성페이 또는 Lpay 사용시
    require_once(G5_MSHOP_PATH.'/samsungpay/incSamsungpayCommon.php');
}

$tablet_size = "1.0"; // 화면 사이즈 조정 - 기기화면에 맞게 수정(갤럭시탭,아이패드 - 1.85, 스마트폰 - 1.0)

// 개인결제번호제거
set_session('ss_personalpay_id', '');
set_session('ss_personalpay_hash', '');
?>

<?php
if($is_kakaopay_use) {
    require_once(G5_SHOP_PATH.'/kakaopay/orderform.1.php');
}
?>

<? if( $member[choice_recommend_pharm_control] == "Y" ){ ?>
    <!-- 출고통제약국일 경우 팝업띄워주고 주문하기 버튼 막기 -->
    <div id="PopupLayer_CONTROL" style="width:98%;height:417px;top:56px;left:5px;position:absolute;z-index:9999;border:0px solid #000;" >
        <div style="padding:5px;height:280px;background:#ee8a1a;">
            <center>
                <br><br>
                <div style="color:#fffaf4;font-weight:bold;font-size:28px;"><?=$member[choice_recommend_pharm_name]?> 폐업 안내</div>
                <br>
                <div style="color:#985000;font-weight:bold;font-size:11px;">
                    회원님의 추천약사이신 <?=$member[choice_recommend_pharm_name]?>&nbsp;<?=$member[choice_recommend_name]?>약사님이 <br>내부사정으로 인해 폐업하였음을 알려드립니다.
                </div>
                <br>
                <div style="color:#ffd4a4;font-weight:bold;font-size:10px;">
                    마이페이지 회원정보수정에서 추천약사를 재지정하거나 변경하실 수 있습니다.
                </div>
                <br>
                <div style="color:#985000;font-weight:bold;font-size:10px;">
                    * 추천약사 변경을 통해서만 주문 및 구매가 가능하오니 이 점 양해 바랍니다.
                </div>
                <div style="color:#6e3a00;font-weight:bold;font-size:11px;">
                    변경방법 :마이페이지->정보수정->약사검색해서 재지정하기->수정하기
                </div>
                <br>
                <div style="color:#ffd4a4;font-weight:bold;font-size:15px;">
                    고객센터 : 1544-5462(헬프라인)
                </div>
            </center>
        </div>
        <div style="background:#000; color:#FFFFFF; text-align:right; padding:0 5px;"><span class="b-close_CONTROL" style="cursor:pointer">Close</span></div>
    </div>
<? } ?>

<div>
    <h2 class="title-section">주문/결제</h2>
    <?php if (!$is_member) { ?>
        <div class="agree-wrap">
            <p class="title">비회원 정보 수집 동의</p>
            <div class="form-checkbox form-checkall">
                <input id="term_all" type="checkbox" class="checkbox">
                <label for="term_all">전체 동의합니다.</label>
            </div>
            <div class="agree-box">
                <div class="form-checkbox">
                    <input id="term_1" name="term_1" type="checkbox" class="checkbox">
                    <label for="term_1">(필수) 비회원 주문에 대한 개인정보수집 이용 동의</label>
                    <button type="button" class="btn-default btn-detail" onclick="terms();">상세보기</button>
                </div>
                <div class="form-checkbox">
                    <input id="term_2" name="term_2" type="checkbox" class="checkbox">
                    <label for="term_2">(필수) 이용약관 동의</label>
                    <!--<button type="button" class="btn-default btn-detail" onclick="terms();">상세보기</button>-->
                </div>
            </div>
        </div>
    <?php } ?>

    <?php
    ob_start();

    $tot_point = 0;
    $tot_sell_price = 0;
    $tot_left_price = 0; //소비자가총계
    $tot_right_price = 0; //약국가총계

    $goods = $goods_it_id = "";
    $goods_count = -1;

    //장바구니 검사
    $result = c_mysqli_call('SP_ORDER_INFO_NEW_VERSION', " '".$s_cart_id."','ORDER','".$member['mb_type']."','".$period."','','' ");

    $good_info = '';
    $it_send_cost = 0;
    $it_cp_count = 0;

    $comm_tax_mny = 0; // 과세금액
    $comm_vat_mny = 0; // 부가세
    $comm_free_mny = 0; // 면세금액
    $tot_tax_mny = 0;
    $period = 'n';

    $price_change_yn = ""; //가격변동여부
    $TimeGoodsYn = "N"; //타임세일 제품이 있는지 여부

    $i = 0;
    $TmpString = "";
    $LeftSum = 0; // 소비자가
    $RightSum = 0; // 약국가
    $trans_memo = "";
    $multi_cnt = 0;

    foreach ($result as $row) {

        //다중배송상품 계산
        if( $row[it_multisend] ){
            $multi_cnt++;
        }else{
            $multi_cnt--;
        }
        // 합계금액 계산
        $sql = " select SUM(IF(io_type = 1, (io_price * ct_qty), ((ct_price + io_price) * ct_qty))) as price,
                            SUM(ct_point * ct_qty) as point,
							SUM((select it_incen * a.ct_qty from tbl_shop_item where it_id = a.it_id))  as it_incen, 
                            SUM((select io_incen * a.ct_qty from tbl_shop_item_option where io_id = a.io_id limit 1) ) as io_incen, SUM(ct_qty) as qty,
                            SUM(ct_qty) as qty
                        from {$g5['g5_shop_cart_table']} a
                        where it_id = '{$row['it_id']}'
                          and od_id = '$s_cart_id' ";
        $sum = sql_fetch($sql);

        if (!$goods) {
            //$goods = addslashes($row[it_name]);
            //$goods = get_text($row[it_name]);
            $goods = preg_replace("/\?|\'|\"|\||\,|\&|\;/", "", $row['it_name']);
            $goods_it_id = $row['it_id'];
        }
        $goods_count++;

        // 에스크로 상품정보
        if ($default['de_escrow_use']) {
            if ($i > 0)
                $good_info .= chr(30);
            $good_info .= "seq=".($i + 1).chr(31);
            $good_info .= "ordr_numb={$od_id}_".sprintf("%04d", $i).chr(31);
            $good_info .= "good_name=".addslashes($row['it_name']).chr(31);
            $good_info .= "good_cntx=".$row['ct_qty'].chr(31);
            $good_info .= "good_amtx=".$row['ct_price'].chr(31);
        }

        $a1 = '<strong>';
        $a2 = '</strong>';
        $image_width = 80;
        $image_height = 80;
        $image = get_it_image($row['it_id'], $image_width, $image_height);

        $it_name = $a1.stripslashes($row['it_name']).$a2;
        $auto_order = '';
        if ($row['ct_period_yn'] == 'Y') {
            $auto_order = ' * 정기주문 시작:'.$row['ct_period_sdate'].'주기:'.$row['ct_period_week'].'횟수:'.$row['ct_period_cnt'];
            $period = 'y';//정기주문
            //정기주문 기간계산
            $sdate = $row['ct_period_sdate'];
            $edate = $row['ct_period_sdate'];
            $period_week = $row['ct_period_week'];
            $period_cnt = $row['ct_period_cnt'];

            for ($i = 2; $i <= $period_cnt; $i++) {
                $str_date = strtotime($edate);
                if ($period_week == '1') {
                    $str_date = strtotime($edate.'+7 days'); // +1 days로 하루를 더함
                }
                if ($period_week == '2') {
                    $str_date = strtotime($edate.'+14 days');
                }
                if ($period_week == '3') {
                    $str_date = strtotime($edate.'+21 days');
                }
                if ($period_week == '4') {
                    $str_date = strtotime($edate.'+28 days');
                }
                if ($period_week == '5') {
                    $str_date = strtotime("+".($i - 1)." month", strtotime($row['ct_period_sdate']));
                }
                $edate = date('Y-m-d', $str_date);
                //echo $edate;
            }
            if ($period_week == '5') {
                $offerPeriod = str_replace('-', '', $sdate).'-'.str_replace('-', '', $edate);
                $auto_order = ' * 정기주문 : '.$offerPeriod.' <br> 주기:지정일마다 횟수:'.$row['ct_period_cnt'];
            } else {
                $offerPeriod = str_replace('-', '', $sdate).'-'.str_replace('-', '', $edate);
                $auto_order = ' * 정기주문 : '.$offerPeriod.' <br> 주기:'.$row['ct_period_week'].'횟수:'.$row['ct_period_cnt'];
            }
            //echo $auto_order;
        }

        $sql = " select a.ct_option, a.ct_qty, a.it_id, a.io_id, a.io_type ,e.io_price ,e.io_drug_price , b.it_price , b.it_drug_price
			                 from tbl_shop_cart a left join tbl_shop_item b on a.it_id = b.it_id	
					         join tbl_shop_item_option e ON a.it_id = e.it_id and a.io_id = e.io_id
			                 where a.it_id = '".$row['it_id']."' and a.od_id = '".$s_cart_id."' order by a.io_type asc, a.ct_id asc ";

        $price = sql_query($sql);
        $it_price = $row['io_price'] + $row['it_price'];


        $it_options = print_item_options_new($row['it_id'], $s_cart_id);
        if ($it_options) {

            $TmpString = explode("|", $it_options);
            $LeftSum = $LeftSum + $TmpString[1];
            $RightSum = $RightSum + $TmpString[2];

            $it_name2 = $TmpString[0];
        }

        // 복합과세금액
        if ($default['de_tax_flag_use']) {
            if ($row['it_notax']) {
                $comm_free_mny += $sum['price'];
            } else {
                $tot_tax_mny += $sum['price'];
            }
        }

        //정기주문 예외처리 상품
        $optionprice = get_option_dis_price($row['it_id'], $row['io_id'], $row['io_type']);

        if ($optionprice > 0) {
            $sell_price = $sum['qty'] * ($row['ct_price'] + $optionprice);
        } else {
            if ($period_cnt > 0) {
                $sell_price = $sum['price'] * $period_cnt;
                $incen_sum = ($sum['it_incen'] + $sum['io_incen']) * $period_cnt;
            } else {
                $sell_price = $sum['price'];
                $incen_sum = $sum['it_incen'] + $sum['io_incen'];
            }
        }

        /* 상품별 배송비 정책 노출 */
        if ($row['it_sc_type'] != 1) {

            if ($row['it_sc_type'] == '2') $trans_memo = '자사 상품으로만 '.number_format($row['it_sc_minimum']).'원 이상 구매 시 무료 배송됩니다.';
            if ($row['it_sc_type'] == '3') $trans_memo = number_format($row['it_sc_price']).'원';
            //if($row['it_sc_type'] =='4') $trans_memo = number_format($row['it_sc_price']).'원 <br>(본 상품은 X개 단위 묶음포장 상품으로 X개 단위로 배송비 ooo원이 추가 부과됩니다.)';

        }

        // 쿠폰
        if ($is_member) {
            $cp_button = '';
            $cp_count = 0;

            $sql = " select cp_id
                            from {$g5['g5_shop_coupon_table']}
                            where mb_id IN ( '{$member['mb_id']}', '소비자회원' )
                              and cp_start <= '".G5_TIME_YMD."'
                              and cp_end >= '".G5_TIME_YMD."'
                              and cp_minimum <= '$sell_price'
                              and (
                                    ( cp_method = '0' and instr( cp_target , concat( concat('|','{$row['it_id']}'),'|' ) ) > 0 )
                                    OR
                                    ( cp_method = '1' and instr( cp_target , concat( concat('|','{$row['ca_id']}'),'|' ) ) > 0 )
                                    OR
                                    ( cp_method = '1' and instr( cp_target , concat( concat('|','{$row['ca_id2']}'),'|' ) ) > 0 )
                                    OR
                                    ( cp_method = '1' and instr( cp_target , concat( concat('|','{$row['ca_id3']}'),'|' ) ) > 0 )
                                  ) and service_type is null ";

            $res = sql_query($sql);

            for ($k = 0; $cp = sql_fetch_array($res); $k++) {
                if (is_used_coupon($member['mb_id'], $cp['cp_id']))
                    continue;

                $cp_count++;
            }

            if ($cp_count) {
                $cp_button = '<div class="li_cp"><button type="button" class="cp_btn">쿠폰사용</button></div>';
                $it_cp_count++;
            }
        }

        if ($row['cal_it_sc_price'] == 0) {
            $ct_send_cost = '무료';
        } else {
            $ct_send_cost = '주문시결제';
        }

        $sendcost = $row['cal_it_sc_price'];

        //배송비업데이트
        if ($sendcost >= 0) {
            $sql = " update tbl_shop_cart set it_sc_cost = '$sendcost' where ct_id='{$row['ct_id']}' ";
            $rtncost = sql_fetch($sql);
        }
        $total_cost = $total_cost + $sendcost;

        $p_point = 0;

        // 소비자가
        $sql_price = "select it_id, it_cust_price, it_price, it_drug_price, it_company, it_point_type from tbl_shop_item where it_id = '$row[it_id]'";
        $re_price = sql_fetch($sql_price);

         if($member['mb_referee'] == 4 || $member['mb_extra'] == 11) {
            $p_point = get_price_cash($re_price['it_point_type'], $re_price['it_price'] * $sum['qty'], $member['mb_grade']);
        }

        $it_company = $re_price['it_company'];

        $sql_com = "select * from tbl_shop_order_address_company";
        $re_com = sql_query($sql_com);


        $str = explode(',', $it_company);
        for($i = 0; $r=sql_fetch_array($re_com); $i++){
            switch ($str[$i]){
                case '1':
                    $com1++;
                    break;
                case '2':
                    $com2++;
                    break;
                case '3':
                    $com3++;
                    break;
                default:
                    break;
            }
        }
        if(count($result) == $com1){
            $company .= "1,";
        }
        if(count($result) == $com2){
            $company .= "2,";
        }
        if(count($result) == $com3){
            $company .= "3";
        }

        // 사내수령X 상품 하나라도 있는경우
        $it_com;
        if($it_company == 0){
            $it_com ++;
        }

        $cust = '';
        $sale_yn = '';
        $sale = '';

        if($re_price['it_cust_price']>0){ // 상품할인이 있는 경우
            $sale_yn = 'y';
            $cust = ($re_price['it_cust_price'] * $sum['qty']);
            $sale = ($cust - $LeftSum);

        }

        $tot_cust_price += ($sale + $LeftSum);
        $tot_sale += $sale;
        ?>

        <input type="hidden" name="it_id[<?php echo $i; ?>]" value="<?php echo $row['it_id']; ?>">
        <input type="hidden" name="it_name[<?php echo $i; ?>]" value="<?php echo get_text($row['it_name']); ?>">
        <input type="hidden" name="it_price[<?php echo $i; ?>]" value="<?php echo $sell_price; ?>">
        <input type="hidden" name="it_one_price[<?php echo $i; ?>]"    value="<?php echo $it_price; ?>">
        <?php if ($default['de_tax_flag_use']) { ?>
            <input type="hidden" name="it_notax[<?php echo $i; ?>]" value="<?php echo $row['it_notax']; ?>">
        <?php } ?>
        <input type="hidden" name="cp_id[<?php echo $i; ?>]" value="">
        <input type="hidden" name="cp_price[<?php echo $i; ?>]" value="0">
        <input type="hidden" name="it_sc_cost[<?php echo $i; ?>]" value="<?php echo $sendcost; ?>">
        <input type="hidden" name="plus_point[<?php echo $i; ?>]" value="<?php echo $p_point; ?>">
        <input type="hidden" id="sc_cost" value="<?php echo $row['it_sc_price'] ?>">
        <input type="hidden" id="sc_minimum" value="<?php echo $row['it_sc_minimum'] ?>">
        <input type="hidden" name="period_cnt[<?php echo $i; ?>]" id="period_cnt" value="<?php echo $period_cnt ?>">

        <div class="box-item">
            <div class="image-box">
                <div class="item-image"><?php echo $image; ?></div>
                <div class="item-desc">
                    <p class="product"><?php echo $it_name; ?></p>
                    <ul>
                        <li class="desc"><!--옵션 : --><?php echo $it_name2; ?>
                            <!--<span class="total-count">1</span>세트--></li>
                        <!--<li class="desc">수량 : <span class="total-count"><?php /*echo $sum['qty'] */?></span>개</li>-->
                    </ul>
                </div>
            </div>
        </div>
        <div class="box-item">
            <span class="label">상품 금액</span>
            <span class="pricebox">
                    <?php
                    if($cust != ''){ ?>
                        <span style="text-decoration:line-through; color: #757575; font-size: 13px;">
                     <?php echo number_format($cust).'<span class="unit">원</span>' ; //정가 ?>
                     </span>
                    <?php }?>
                   <span class="price"><?php echo number_format($LeftSum); ?></span><span class="unit">원</span></span>
        </div>
        <div class="box-item">
            <span class="label">수량</span>
            <span class="pricebox"><span class="total-count"><?php echo  number_format($sum['qty']); ?></span><span class="unit">&nbsp;개</span></span>
        </div>
        <div class="box-item">
            <span class="label">배송비</span>
            <span class="pricebox"><span class="total-count">
                    <?php echo ($sendcost == 0) ? '무료' : number_format($sendcost).'<span class="unit"> 원</span>'; ?></span></span>
        </div>
        <div class="box-item">
            <span class="label" style='font-size:0.98em;font-weight: bold;color:#ff006c;float: right;'>총금액</span>
            <span class="pricebox">
                <span class="total-count"><?php echo number_format($LeftSum + $sendcost); ?></span>
                <span class="unit">&nbsp;원</span>
            </span>
        </div>

        <?php
        $tot_left_price += $LeftSum;
        $tot_right_price += $RightSum;
        $tot_point += $point;
        $tot_sell_price += $sell_price;
        $tot_plus_point += $p_point;
        $RightSum = 0;
        $LeftSum = 0;
        $trans_memo = "";

        //가격변동 체크여부
        if ($row['price_change_yn_sum'] != 0) {
            $price_change_yn = "Y";
        }
        //타임세일상품 체크여부
        if ($row['it_type4'] == "1" && $time_sale_yn == "N") {
            $TimeGoodsYn = "Y";
        }

        $i++;
    } // for 끝

    if ($i == 0) {
        alert('장바구니가 비어 있습니다.', G5_SHOP_URL.'/cart.php');
    }

    // 복합과세처리
    if ($default['de_tax_flag_use']) {
        $comm_tax_mny = round(($tot_tax_mny + $total_cost) / 1.1);
        $comm_vat_mny = ($tot_tax_mny + $total_cost) - $comm_tax_mny;
    }
    ?>

    <?php if ($goods_count) $goods .= ' 외 '.$goods_count.'건'; ?>

    <?php
    $tot_price = $tot_sell_price + ($total_cost*$period_cnt);

    $content = ob_get_contents();
    ob_end_clean();

    if ($period == 'y') {
        require_once(G5_MSHOP_PATH.'/'.$default['de_pg_service'].'/orderform.1.1.php');
    } else {
        // 결제대행사별 코드 include (결제등록 필드)
        require_once(G5_MSHOP_PATH.'/'.$default['de_pg_service'].'/orderform.1.php');
    }
    if (is_inicis_simple_pay()) {   //이니시스 삼성페이 또는 lpay 사용시
        require_once(G5_MSHOP_PATH.'/samsungpay/orderform.1.php');
    }
    ?>

    <form name="forderform" method="post" action="<?php echo $order_action_url; ?>" autocomplete="off">
        <input type="hidden" name="od_price" value="<?php echo $tot_sell_price; ?>">
        <input type="hidden" name="org_od_price" value="<?php echo $tot_sell_price; ?>">
        <input type="hidden" name="od_send_cost" value="<?php echo $total_cost; ?>">
        <input type="hidden" name="od_send_cost_origin" value="<?php echo $total_cost; ?>">
        <input type="hidden" name="od_send_cost2" value="0">
        <input type="hidden" name="item_coupon" value="0">
        <input type="hidden" name="od_coupon" value="0">
        <input type="hidden" name="od_send_coupon" value="0">
        <input type="hidden" name="od_temp_point" value="0">
        <input type="hidden" name="it_company" id="it_company" value="<?php echo $it_company; ?>">
        <input type="hidden" name="company_sendcost" value="0">

        <!-- [D] 아코디언 기능이 들어가는 경우 .accordion-box, .accordion-title, .accordion-content 추가 -->
        <div class="border-top-bottom-style accordion-box">
            <h3 class="title-box accordion-title is-opened">
                <span class="left-side title-main">주문 상품<span>(<?php echo $i; ?>)</span></span>
                <span class="right-side"><i class="icon icon-accordion"></i></span>
            </h3>
            <div class="content-box accordion-content">
                <?php echo $content; ?>
            </div>
        </div>

        <div class="border-top-bottom-style">
            <h3 class="title-box">
                <span class="left-side title-main">주문자 정보</span>
                <?php if ($member['mb_type'] == 1) { ?>
                <span id="radio" class="ui-buttonset">
                    <input type="radio" id="od_gubun1" name="od_gubun" onclick="myaddr(1);od_gubun_chg1();" value="1" ><label for="od_gubun1" ><span class="">고객주문</span></label>
                    <input type="radio" id="od_gubun2" name="od_gubun" value="2" onclick="od_gubun_chg2()" checked ><label for="od_gubun2" ><span class="">약국주문</span></label>
                </span>
                <?php } else { ?>
                <input type="hidden" name="od_gubun" value="3">
                <?php }  ?>
            </h3>
            <div class="content-box">
                <ul class="box-item row">
                    <li class="tr">
                        <span class="label">주문하시는 분</span>
                        <input type="text" class="input-default" name="od_name" value="<?php echo get_text($member['mb_name']); ?>" id="od_name" required maxlength="20"/>
                    </li>
                    <?php if (!$is_member) { // 비회원이면 ?>
                    <li class="tr">
                        <span class="label">주문 비밀번호</span>
                        <input type="password" name="od_pwd" id="od_pwd" required class="input-default"/>
                    </li>
                    <li class="tr">
                        <span class="label">비밀번호 재확인</span>
                        <input type="password" name="od_pwd_chk" id="od_pwd_chk" required class="input-default"/>
                    </li>
                    <?php } ?>
                    <li class="tr">
                        <span class="label">휴대폰 번호</span>
                        <!--<div class="form-set">
                            <select name="" class="basic-select select-sm">
                                <option value="">선택</option>
                                <option value="">010</option>
                                <option value="">011</option>
                                <option value="">016</option>
                                <option value="">017</option>
                                <option value="">018</option>
                                <option value="">019</option>
                            </select>
                            <input type="text" name="" id="" required class="input-default" maxlength="20"/>
                            <input type="text" name="" id="" required class="input-default" maxlength="20"/>
                        </div>-->
                        <input type="text" name="od_hp" value="<?php echo get_text($member['mb_hp']); ?>" id="od_hp" required class="input-default">
                    </li>
                    <li class="tr" <?php echo ($is_member) ? 'style="display:none;"' : '';?>>
                        <span class="label">이메일</span>
                        <!--<div class="form-set">
                            <input type="text" name="" id="" required class="input-default" maxlength="20"/>
                            <select name="" class="basic-select select-sm">
                                <option value="">직접 입력</option>
                            </select>
                        </div>-->
                        <input type="email" name="od_email" value="<?php echo $member['mb_email']; ?>" id="od_email" required class="input-default" maxlength="100">
                    </li>
                </ul>
            </div>
        </div>

        <?php
        if($is_member) {
            // 배송지 이력
            $addr_list = '';
            $sep = chr(30);

            $addr_list .= '<input type="radio" name="ad_sel_addr" value="new" id="od_sel_addr_new">'.PHP_EOL;
            $addr_list .= '<label for="od_sel_addr_new">신규배송지</label>'.PHP_EOL;

            // 기본배송지
            $sql = " select *
                            from {$g5['g5_shop_order_address_table']}
                            where mb_id = '{$member['mb_id']}'
                                and ad_default = '1' ";
            $row = sql_fetch($sql);
            if($row['ad_id']) {
                $val1 = $row['ad_name'].$sep.$row['ad_tel'].$sep.$row['ad_hp'].$sep.$row['ad_zip1'].$sep.$row['ad_zip2'].$sep.$row['ad_addr1'].$sep.$row['ad_addr2'].$sep.$row['ad_addr3'].$sep.$row['ad_jibeon'].$sep.$row['ad_subject'];
                $addr_list .= '<br><input type="radio" name="ad_sel_addr" value="'.get_text($val1).'" id="ad_sel_addr_def">'.PHP_EOL;
                $addr_list .= '<label for="ad_sel_addr_def">기본배송지</label>'.PHP_EOL;
            }

            // 최근배송지
            $sql = " select *
                            from {$g5['g5_shop_order_address_table']}
                            where mb_id = '{$member['mb_id']}'
                                and ad_default = '0'
                            order by ad_id desc
                            limit 1 ";
            $result = sql_query($sql);
            for($i=0; $row=sql_fetch_array($result); $i++) {
                $val1 = $row['ad_name'].$sep.$row['ad_tel'].$sep.$row['ad_hp'].$sep.$row['ad_zip1'].$sep.$row['ad_zip2'].$sep.$row['ad_addr1'].$sep.$row['ad_addr2'].$sep.$row['ad_addr3'].$sep.$row['ad_jibeon'].$sep.$row['ad_subject'];
                $val2 = '<label for="ad_sel_addr_'.($i+1).'">최근배송지('.($row['ad_subject'] ? $row['ad_subject'] : $row['ad_name']).')</label>';
                $addr_list .= '<br><input type="radio" name="ad_sel_addr" value="'.get_text($val1).'" id="ad_sel_addr_'.($i+1).'"> '.PHP_EOL.$val2.PHP_EOL;
            }

            // 사내수령
            if(($member['mb_referee'] == 4 || $member['mb_type'] == 9) && $it_com ==''){
                $val2 = '<label for="addr_company">사내수령</label>';
                $addr_list .= '<br><input type="radio" onClick="myaddr4()" name="ad_sel_addr" value="C" id="addr_company"> '.PHP_EOL.$val2.PHP_EOL;

            }

            if($member['mb_type']==1) {
                //$addr_list .='<a href="javascript:myaddr(2);" >나의고객리스트</a>';
            } else {
                $addr_list .='<a href="'.G5_SHOP_URL.'/orderaddress.php" id="order_address">배송지목록</a>';
            }
        } else {
            // 주문자와 동일
            $addr_list .= '<input type="checkbox" name="ad_sel_addr" value="same" id="ad_sel_addr_same">'.PHP_EOL;
            $addr_list .= '<label for="ad_sel_addr_same">주문자와 동일</label>'.PHP_EOL;
        }

        if($config['cf_use_point']){
            $ca_point = $member['mb_point'];
            $ca_title = "포인트";
            $ca_type = "p";
            $display = display_point($ca_point);
        }else if($member['mb_referee'] == 4 || $member['mb_extra'] == 11){
            $ca_point = $member['mb_cash'];
            $ca_title = "적립금";
            $ca_type = "c";
            $display = display_cash($ca_point);
        }
        ?>
        <div class="border-top-bottom-style">
            <h3 class="title-box">
                <span class="left-side">
                    <span class="title-main">배송 정보</span>
                    <div class="radio">
                        <?php if ($multi_cnt == 1 && $sum['qty'] > 1 && $is_member && $period != 'y') { ?>
                            <span style="margin-left: 10px;">
                                <button type="button" class="btn_frmline btn_addsch" id="mylist_button" onclick="myaddr3(2);" style="display: none;">배송지 리스트</button>
                               <input type="hidden" name="multiChk" id="multiChk" value="3"/>
                                <input type="radio" name="ad_sel_addr" value="Y" id="multiYN">
                                <label for="multiYN">다중배송</label>
                                <input type="hidden" name="order_qty" id="order_qty" value="<?php echo $sum['qty']?>"/>
                            </span>
                        <?php } ?>
                        <?php if($is_member){ ?>
                        <span id="ad_sel_addr_same_area">
                            <input type="radio" name="ad_sel_addr" value="same" id="ad_sel_addr_same" style="margin-left: 7px">
                            <label for="ad_sel_addr_same">주문자와 동일</label>
                        </span>
                        <?php } else {?>
                            <span id="ad_sel_addr_same_area">
                            <input type="radio" name="ad_sel_addr2" value="same" id="ad_sel_addr_same2" style="margin-left: 7px">
                            <label for="ad_sel_addr_same2">주문자와 동일</label>
                        </span>
                        <?php }?>
                        <!-- <input type='checkbox' id='check1' name=''/> -->
                        <!-- <label for="check1">주문자 정보와 동일</label> -->
                    </div>
                </span>
            </h3>
            <div class="content-box">
                <div class="tbl_receiver_content_1" style="overflow:auto;">

                    <span class="tbl_head03 tbl_wrap" id="multi_receiver_list" style="display:none;">
                        <table border="1" id="multi_table">
                            <thead style="border-bottom: 1px solid #d3d3d3;">
                            <tr>
                                <th scope="col" style="width:30px;" align="Center"><input type="checkbox" name="all_check"/></th>
                                <th scope="col" style="width:68px;" align="Center">상품수량입력</th>
                                <!--                            <th scope="col" style="width:65px;" align="Center">성명</th>-->
                                <!--                            <th scope="col" style="width:65px;" align="Center">HP</th>-->
                                <th scope="col" style="text-align: center;">배송정보</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                        <br>
                           <center>
                                <input type="button" value="확정" id="multAdd" onclick="javascript:MultiAdd();" class="btn_submit" style="width:70px;height:30px;margin-bottom: 20px;"/>
                                <input type="button" value="선택삭제" onclick="javascript:MultiDel();" class="btn_submit" style="width:70px;height:30px;margin-bottom: 20px;background-color: grey"/>
                           </center>
                    </span>

                </div>
                <ul class="box-item row" id="tbl_receiver_content">
                    <?php if($is_member) { ?>
                    <!-- 회원 배송지 목록 -->
                    <li class="tr" style="margin-top: 6px;">
                        <div style="background: #f3f3f3;border: 1px solid #ccc;padding: 10px;width: 100%">
                            <?php echo $addr_list; ?>
                        </div>
                    </li>
                    <li class="tr">
                        <span class="label">배송지명</span>
                        <input type="text" class="input-default" name="ad_subject" id="ad_subject" style="margin-bottom: 18px;"/>
                        <span style="position: absolute;left: 100px;margin-top: 36px;">
                            <input type="checkbox" name="ad_default" id="ad_default" value="1" >
                            <label for="ad_default" class="ad_default">기본배송지로 설정</label>
                        </span>
                    </li>
                    <?php } ?>
                    <li class="tr">
                        <span class="label">받으시는 분</span>
                        <input type="text" class="input-default" name="od_b_name" id="od_b_name" required />
                    </li>
                    <li class="tr">
                        <span class="label">휴대폰 번호</span>
                        <!--<div class="form-set">
                            <select name="" class="basic-select select-sm">
                                <option value="">선택</option>
                                <option value="">010</option>
                                <option value="">011</option>
                                <option value="">016</option>
                                <option value="">017</option>
                                <option value="">018</option>
                                <option value="">019</option>
                            </select>
                            <input type="text" name="" id="" required class="input-default" maxlength="20"/>
                            <input type="text" name="" id="" required class="input-default" maxlength="20"/>
                        </div>-->
                        <input type="text" class="input-default" name="od_b_hp" id="od_b_hp" required >
                    </li>
                    <li class="tr">
                        <span class="label">배송 주소</span>
                        <!-- <div class="form-group"> -->
                        <div class="form-set">
                            <input type="text" name="od_b_zip" id="od_b_zip" required class="input-default" size="5" maxlength="6" readonly>
                            <button type="button" class="btn-default btn-address" name="od_zip" id="od_zip" onclick="win_zip('forderform', 'od_b_zip', 'od_b_addr1', 'od_b_addr2', 'od_b_addr3', 'od_b_addr_jibeon');">우편번호 찾기</button>
                        </div>
                    </li>
                    <li class="tr">
                        <span class="label"></span>
                        <div class="form-set">
                            <label for="" class="sound_only">기본주소<strong>필수</strong></label>
                            <input type="text" name="od_b_addr1" id="od_b_addr1" required class="input-default" readonly="readonly">
                            <!-- <label for="" class="sound_only">상세주소</label>
                            <input type="text" name="" id="" class="input-default" placeholder="상세주소"> -->
                        </div>
                        <!-- </div> -->
                    </li>
                    <li class="tr">
                        <span class="label"></span>
                        <div class="form-set">
                            <!-- <label for="" class="sound_only">기본주소<strong>필수</strong></label>
                            <input type="text" name="" id="" required class="input-default input-md" readonly="readonly"> -->
                            <label for="" class="sound_only">상세주소</label>
                            <input type="text" name="od_b_addr2" id="od_b_addr2" class="input-default" placeholder="상세주소">
                            <input type="hidden" name="od_b_addr3" id="od_b_addr3" class="input-default" placeholder="참고주소">
                            <input type="hidden" name="od_b_addr_jibeon" value="">
                        </div>
                        <!-- </div> -->
                    </li>
                    <li class="tr">
                        <span class="label">배송 메시지</span>
                        <div class="form-dropdown is-active">
                            <input class="dropdown-toggle" placeholder="배송 메시지를 선택해 주세요." name="od_trans_memo" id="od_trans_memo" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" readonly="readonly"/>
                            <div class="dropdown-menu" aria-labelledby="od_trans_memo" id="od_trans_memo_toggle">
                                <button class="dropdown-item trans_memo_value" type="button">부재 시 경비실에 맡겨주세요.</button>
                                <button class="dropdown-item trans_memo_value" type="button">부재 시 연락 부탁드립니다.</button>
                                <button class="dropdown-item trans_memo_value" type="button">부재 시 문 앞에 놓아주세요.</button>
                                <button class="dropdown-item trans_memo_value" type="button">직접입력</button>
                            </div>
                        </div>
                    </li>
                </ul>
                <script>
                    $('.form-dropdown').on('click', function () {
                        if($('.dropdown-menu').css('display') ==  'none') {
                            $('.dropdown-menu').css('display', 'block');
                        } else {
                            $('.dropdown-menu').css('display', 'none');
                        }
                    });
                </script>
                <ul class="pay-result">
                    <li>
                        <span class="label">소비자가</span>
                        <span class="pricebox">
                            <span class="price"><?php echo number_format($tot_cust_price); ?></span>
                            <span class="unit">원</span></span>
                    </li>
                    <?php if ($tot_sale > 0) { ?>
                        <li>
                            <span class="label">상품할인</span>
                            <span class="pricebox">
                <span class="price">- <?php echo number_format($tot_sale); ?></span>
                <span class="unit">원</span>
                    </span>
                        </li>
                    <?php } ?>
                    <?php if($is_member) {?>
                    <li>
                        <span class="label">쿠폰할인</span>
                        <span class="pricebox" id="ct_cp_price">
                            <span class="price">0</span>
                            <span class="unit">원</span>
                        </span>
                    </li>
                    <li>
                        <span class="label">사용 <?php echo $ca_title ?></span>
                        <span class="pricebox" id="ct_cp_point">
                            <span class="price">0</span>
                            <span class="unit">원</span>
                        </span>
                    </li>
                    <?php } ?>
                    <li>
                        <span class="label">배송비</span>
                        <span class="pricebox"><span class="sc_price"><?php echo number_format($total_cost);?></span><span class="unit"> 원</span></span>
                    </li>
                    <?php if($member['mb_referee'] == 4 || $member['mb_type'] == 9){?>
                    <li id="company_amount" style="visibility:hidden">
                        <span class="label">사내수령 배송비할인</span>
                        <span class="pricebox" id="ct_cp_point"><span class="company_amount">0</span><span class="unit">원</span></span>
                    </li>
                    <?}?>

                    <?php if($is_member) { ?>

                    <?php } ?>
                    <?php
                    if ($period == 'y') {?>
                    <li class="total">
                            <?php
                            if ($optionprice > 0) {
                                $tot_price = $tot_sell_price + ($total_cost*$period_cnt);
                            } else {
                                //$tot_price = ($tot_sell_price*$period_cnt) + $total_cost;
                                $tot_price = ($tot_sell_price) + ($total_cost*$period_cnt);
                            }?>
                        <span class="label">결제 예정 금액</span>
                        <span class="pricebox" id="ct_tot">
                         <span><?php echo number_format(($tot_price/$period_cnt));?></span><span class="unit">원</span>
                          &nbsp*&nbsp<span><?php echo number_format($period_cnt);?></span>회<span class="unit">
                            </span>
                        <?} ?>
                    </li>
                    <li class="total">
                        <span class="label">총 결제 금액</span>
                        <span class="pricebox" id="ct_tot_price">
                            <span class="price"><?php echo number_format($tot_price);?></span>
                            <span class="unit">원</span>
                        </span>
                    </li>
                </ul>
                <p class="table-info">비회원으로 주문하시면 쿠폰 할인, 포인트 적립 및 혜택을 이용하실 수 없습니다.</p>
            </div>
        </div>

        <?php if ($is_member) { ?>
        <div class="border-top-bottom-style">
            <h3 class="title-box">
                <span class="left-side title-main">할인 정보</span>
            </h3>
            <div class="content-box" style="padding-bottom: 10px;">
                <ul class="box-item row">
                    <?php
                    $oc_cnt = $sc_cnt = 0;
                    $ic_cnt = 0;
                    // 주문쿠폰
                    $sql = " select cp_id
                            from {$g5['g5_shop_coupon_table']}
                            where mb_id IN ('{$member['mb_id']}', '전체회원', '소비자회원')
                            and cp_method = '2'
                            and cp_start <= '".G5_TIME_YMD."'
                            and cp_end >= '".G5_TIME_YMD."'
                            and cp_minimum <= '$tot_sell_price' and service_type is null ";
                    $res = sql_query($sql);

                    for ($k = 0; $cp = sql_fetch_array($res); $k++) {
                        if (is_used_coupon($member['mb_id'], $cp['cp_id']))
                            continue;

                        $oc_cnt++;
                    }

                    if ($total_cost > 0) {
                        // 배송비쿠폰
                        $sql = " select cp_id
                                from {$g5['g5_shop_coupon_table']}
                                where mb_id IN ('{$member['mb_id']}', '전체회원', '소비자회원')
                                and cp_method = '3'
                                and cp_start <= '".G5_TIME_YMD."'
                                and cp_end >= '".G5_TIME_YMD."'
                                and cp_minimum <= '$tot_sell_price' and service_type is null ";
                        $res = sql_query($sql);

                        for ($k = 0; $cp = sql_fetch_array($res); $k++) {
                            if (is_used_coupon($member['mb_id'], $cp['cp_id']))
                                continue;

                            $sc_cnt++;
                        }
                    }

                    // 개별쿠폰
                    $sql = " select cp_id
                            from {$g5['g5_shop_coupon_table']}
                            where mb_id IN ('{$member['mb_id']}', '전체회원', '소비자회원')
                            and (cp_method = '0' or cp_method = '1')
                            and cp_start <= '".G5_TIME_YMD."'
                            and cp_end >= '".G5_TIME_YMD."'
                            and cp_minimum <= '$tot_sell_price' and service_type is null ";
                    $res = sql_query($sql);

                    for ($k = 0; $cp = sql_fetch_array($res); $k++) {
                        if (is_used_coupon($member['mb_id'], $cp['cp_id']))
                            continue;

                        $item_coupon = false;
                        foreach ($it_id_arr as $it_id) {
                            if ($item_coupon == true)
                                continue;
                            $sql = " select it_id, ca_id, ca_id2, ca_id3 from {$g5['g5_shop_item_table']} where it_id = '$it_id' ";
                            $it = sql_fetch($sql);

                            $sql = " select *
                                    from {$g5['g5_shop_coupon_table']}
                                    where cp_id = '{$cp['cp_id']}'
                                      and (
                                            ( cp_method = '0' and instr( cp_target , concat( concat('|','{$it['it_id']}'),'|' ) ) > 0 )
                                            OR
                                            ( cp_method = '1' and instr( cp_target , concat( concat('|','{$it['ca_id']}'),'|' ) ) > 0 )
                                            OR
                                            ( cp_method = '1' and instr( cp_target , concat( concat('|','{$it['ca_id2']}'),'|' ) ) > 0 )
                                            OR
                                            ( cp_method = '1' and instr( cp_target , concat( concat('|','{$it['ca_id3']}'),'|' ) ) > 0 )
                                          ) and service_type is null ";

                            $req = sql_query($sql);
                            $count = sql_num_rows($req);
                            if ($count > 0)
                                $item_coupon = true;

                        }

                        if ($item_coupon)
                            $ic_cnt++;
                    }

                    // 쿠폰
                    $cp_all_count = 0;
                    $sql = " select cp_id
                                from {$g5['g5_shop_coupon_table']}
                                where mb_id IN ( '{$member['mb_id']}', '소비자회원', '전체회원' )
                                  and cp_start <= '".G5_TIME_YMD."'
                                  and cp_end >= '".G5_TIME_YMD."' and service_type is null ";
                    $res = sql_query($sql);

                    for($k=0; $cp=sql_fetch_array($res); $k++) {
                        if(!is_used_coupon($member['mb_id'], $cp['cp_id']))
                            $cp_all_count++;
                    }
                    ?>
                    <li class="tr">
                        <span class="label">쿠폰</span>
                        <div class="form-set">
                            <input type="text" id="od_cp_price" value="0" readonly class="input-default" maxlength="20"/>
                            <input type="hidden" name="od_cp_id" value="">
                            <button type="button" class="btn-default btn-address" id="od_coupon_btn">쿠폰사용</button>
                        </div>
                    </li>
                    <p class="input-guide-sm" style="margin-left: 85px;font-weight: 500;">* 사용 가능 쿠폰 : <?php echo $oc_cnt+$sc_cnt+$ic_cnt; ?>장 / 보유 쿠폰 : <?php echo $cp_all_count; ?>장</p>

                    <?php
                    $temp_point = 0;
                    // 회원이면서 포인트사용이면
                    if (($is_member && $config['cf_use_point'] && $member['mb_point'] > 0) || $member['mb_referee'] == 4 || $member['mb_extra'] == 11) {
                        $temp_point = $tot_price;

                        //echo '<div class="sod_frm_point" "'.$point_view.'">';
                        //echo '<div><input type="hidden" name="max_temp_point" value="'.$tot_price.'"><label for="od_temp_point">사용 포인트</label> <input type="text" id="od_temp_point" name="od_temp_point" value="0" size="10"> 점</div>';
                        //echo '<div id="sod_frm_pt_info"><span><strong>보유포인트</strong>'.display_point($member['mb_point']).'</span><span class="max_point_box"><strong>최대사용가능포인트</strong><em id="use_max_point">'.display_point($tot_price).'</em></span></div>';
                        //echo '</div>';
                    ?>
                    <li class="tr">
                        <span class="label"><?php echo $ca_title ?></span>
                        <div class="form-set">
                            <input type="text" class="input-default" id="od_point" value="0" readonly>
                            <input type="hidden" name="max_temp_point" value="<?php echo $ca_point; ?>">
                            <button type="button" class="btn-default btn-address" onclick="$('#point_input_area').show();"><?php echo $ca_title ?> 입력</button>
                        </div>
                    </li>
                    <div id="point_input_area" style="display: none;margin: 5px 0 10px 85px;">
                        <input type="text" class="input-default" id="od_input_point" style="font-size: 12px;width: 80px;line-height: 10px;height: 37px;">
                        <button type="button" class="btn-default btn-detail" id="od_point_btn" style="display: inline-flex;height: 37px;"><?php echo $ca_title ?> 적용</button>
                        <button type="button" class="btn-default btn-detail" onclick="$('#od_input_point').val($('input[name=max_temp_point]').val()); calculate_order_price();" style="display: inline-flex;height: 37px;">
                            <span>전액사용</span>
                        </button>
                        <p class="input-guide-sm" style="margin: 5px 0 0 0;font-weight: 500;">* <span class="max_point_box">남은 <?php echo $ca_title ?> <?php echo $display; ?></span></p>
                    </div>
                    <?php } ?>
                </ul>
            </div>
        </div>
        <?php } ?>

        <?php
        $t_plus_point = number_format($tot_plus_point);?>
        <input type="hidden" name="point_type" value="<?php echo $ca_type?>">
        <?php if($t_plus_point > 0 && ($member['mb_referee'] == 4 || $member['mb_extra'] == 11)){?>
        <div class="border-top-bottom-style">
            <h3 class="title-box">
                <span class="left-side title-main">적립예정금액</span>
                <input type="hidden" name="t_plus_point" value="<?php echo $t_plus_point?>">
                <span class="left-side title-main" style="text-align: right"><div class="result_point" ><?php echo $t_plus_point?> </div>&nbsp;원</span>
            </h3>
            <div class="content-box" style="padding-bottom: 10px;">
            <p class="table-info">배송비를 제외한 총 구매금액을 기준으로 적립됩니다.</p>
            <p class="table-info">적립율은 상품별로 상이할 수 있습니다.</p>
            </div>
        </div>
        <?php } ?>
        <div class="border-top-style">
            <h3 class="title-box">
                <span class="left-side title-main">결제 수단</span>
            </h3>
            <div class="content-box">
                <div class="box-item pay-button-list">
                    <?php
                    if($member['mb_type'] != '1' or !$is_member) {
                        $multi_settle = 0;
                        $checked = '';

                        $escrow_title = "";
                        if ($default['de_escrow_use']) {
                            $escrow_title = "에스크로 ";
                        }

                        if ($is_kakaopay_use || $default['de_bank_use'] || $default['de_vbank_use'] || $default['de_iche_use'] || $default['de_card_use'] || $default['de_hp_use'] || $default['de_easy_pay_use'] || is_inicis_simple_pay()) {
                            echo '<div id="m_sod_frm_paysel"><ul>';
                        }
                        if($period != 'y') {
                            // 카카오페이
                            /*if($is_kakaopay_use) {
                                $multi_settle++;
                                echo '<li><input type="radio" id="od_settle_kakaopay" name="od_settle_case" value="KAKAOPAY" '.$checked.'> <label for="od_settle_kakaopay" class="kakaopay_icon lb_icon">KAKAOPAY</label></li>'.PHP_EOL;
                                $checked = '';
                            }*/

                            // 신용카드 사용
                            if ($default['de_card_use']) {
                                $multi_settle++;
                                //echo '<li><input type="radio" id="od_settle_card" name="od_settle_case" value="신용카드" '.$checked.'> <label for="od_settle_card" class="lb_icon card_icon">신용카드</label></li>'.PHP_EOL;
                                echo '<label class="btn-default"><input type="radio" id="od_settle_card" name="od_settle_case" value="신용카드" checked><span>신용카드</span></label>'.PHP_EOL;;
                                $checked = '';
                            }

                            // 가상계좌 사용
                            /*if ($default['de_vbank_use']) {
                                $multi_settle++;
                                echo '<li><input type="radio" id="od_settle_vbank" name="od_settle_case" value="가상계좌" '.$checked.'> <label for="od_settle_vbank" class="lb_icon vbank_icon">'.$escrow_title.'가상계좌</label></li>'.PHP_EOL;
                                $checked = '';
                            }*/

                            // 계좌이체 사용
                            if ($default['de_iche_use']) {
                                $multi_settle++;
                                //echo '<li><input type="radio" id="od_settle_iche" name="od_settle_case" value="계좌이체" '.$checked.'> <label for="od_settle_iche" class="lb_icon iche_icon">'.$escrow_title.'계좌이체</label></li>'.PHP_EOL;
                                echo '<label class="btn-default"><input type="radio" id="od_settle_iche" name="od_settle_case" value="계좌이체"><span>계좌이체</span></label>'.PHP_EOL;;
                                $checked = '';
                            }

                            // 무통장입금 사용
                            if ($default['de_bank_use']) {

                                $multi_settle++;
                                //echo '<li><input type="radio" id="od_settle_bank" name="od_settle_case" value="무통장" '.$checked.'> <label for="od_settle_bank" class="lb_icon  bank_icon">무통장입금</label></li>'.PHP_EOL;
                                echo '<label class="btn-default"><input type="radio" id="od_settle_bank" name="od_settle_case" value="무통장"><span>무통장입금</span></label>'.PHP_EOL;;
                                $checked = '';
                            }

                         /*   // 휴대폰 사용
                            if ($default['de_hp_use']) {
                                $multi_settle++;
                                //echo '<li><input type="radio" id="od_settle_hp" name="od_settle_case" value="휴대폰" '.$checked.'> <label for="od_settle_hp" class="lb_icon hp_icon">휴대폰</label></li>'.PHP_EOL;
                                echo '<label class="btn-default"><input type="radio" id="od_settle_hp" name="od_settle_case" value="휴대폰"><span>휴대폰</span></label>'.PHP_EOL;;
                                $checked = '';
                            }*/

                            if (1) {
                                $multi_settle++;
                                echo '<li style="display:none;"><input type="radio" id="od_settle_point" name="od_settle_case" value="포인트" '.$checked.'> <label for="od_settle_point" class="lb_icon bank_icon">포인트결제</label></li>'.PHP_EOL;
                                $checked = '';
                            }

                            // PG 간편결제
                            if($default['de_easy_pay_use']) {
                                switch($default['de_pg_service']) {
                                    case 'lg':
                                        $pg_easy_pay_name = 'PAYNOW';
                                        break;
                                    case 'inicis':
                                        $pg_easy_pay_name = 'KPAY';
                                        break;
                                    default:
                                        $pg_easy_pay_name = 'PAYCO';
                                        break;
                                }

                                $multi_settle++;
                                echo '<li><input type="radio" id="od_settle_easy_pay" name="od_settle_case" value="간편결제" '.$checked.'> <label for="od_settle_easy_pay" class="'.$pg_easy_pay_name.' lb_icon">'.$pg_easy_pay_name.'</label></li>'.PHP_EOL;
                                $checked = '';
                            }

                            //이니시스 삼성페이
                            if($default['de_samsung_pay_use']) {
                                echo '<li><input type="radio" id="od_settle_samsungpay" data-case="samsungpay" name="od_settle_case" value="삼성페이" '.$checked.'> <label for="od_settle_samsungpay" class="samsung_pay lb_icon">삼성페이</label></li>'.PHP_EOL;
                                $checked = '';
                            }

                            //이니시스 Lpay
                            if($default['de_inicis_lpay_use']) {
                                echo '<li><input type="radio" id="od_settle_inicislpay" data-case="lpay" name="od_settle_case" value="lpay" '.$checked.'> <label for="od_settle_inicislpay" class="inicis_lpay">L.pay</label></li>'.PHP_EOL;
                                $checked = '';
                            }

                            echo '</ul>';

                            /*$temp_point = 0;
                            // 회원이면서 포인트사용이면
                            if ($is_member && $config['cf_use_point'])
                            {
                                // 포인트 결제 사용 포인트보다 회원의 포인트가 크다면
                                if($member['mb_point'] > 0){

                                    $temp_point = $tot_price;

                                    echo '<div class="sod_frm_point" "'.$point_view.'">';
                                    echo '<div><input type="hidden" name="max_temp_point" value="'.$tot_price.'"><label for="od_temp_point">사용 포인트</label> <input type="text" id="od_temp_point" name="od_temp_point" value="0" size="10"> 점</div>';
                                    echo '<div id="sod_frm_pt_info"><span><strong>보유포인트</strong>'.display_point($member['mb_point']).'</span><span class="max_point_box"><strong>최대사용가능포인트</strong><em id="use_max_point">'.display_point($tot_price).'</em></span></div>';
                                    echo '</div>';
                                    $multi_settle++;
                                }
                            }*/

                            if ($default['de_bank_use']) {

                                // 은행계좌를 배열로 만든후
                                $str = explode("\n", trim($default['de_bank_account']));
                                if (count($str) <= 1)
                                {
                                    $bank_account = '<input type="hidden" name="od_bank_account" value="'.$str[0].'">'.$str[0].PHP_EOL;
                                }
                                else
                                {
                                    $bank_account = '<select name="od_bank_account" id="od_bank_account">'.PHP_EOL;
                                    $bank_account .= '<option value="">선택하십시오.</option>';
                                    for ($i=0; $i<count($str); $i++)
                                    {
                                        //$str[$i] = str_replace("\r", "", $str[$i]);
                                        $str[$i] = trim($str[$i]);
                                        $bank_account .= '<option value="'.$str[$i].'">'.$str[$i].'</option>'.PHP_EOL;
                                    }
                                    $bank_account .= '</select>'.PHP_EOL;
                                }
                                echo '<div id="settle_bank" style="display:none">';
                                echo '<label for="od_bank_account" class="sound_only">입금할 계좌</label>';
                                echo $bank_account;
                                echo '<br><br><label for="od_deposit_name">입금자명</label> ';
                                echo '<input type="text" name="od_deposit_name" id="od_deposit_name" size="10" maxlength="20">';
                                echo '</div>';
                            }

                            if ($default['de_bank_use'] || $default['de_vbank_use'] || $default['de_iche_use'] || $default['de_card_use'] || $default['de_hp_use'] || $default['de_easy_pay_use'] || is_inicis_simple_pay() ) {
                                echo '</div>';
                            }

                            if ($multi_settle == 0)
                                echo '<p>결제할 방법이 없습니다.<br>운영자에게 알려주시면 감사하겠습니다.</p>';
                        } else {

                            // 신용카드 사용
                            if ($default['de_card_use']) {
                                $multi_settle++;
                                //echo '<li><input type="radio" id="od_settle_card" name="od_settle_case" value="신용카드" '.$checked.'> <label for="od_settle_card" class="lb_icon card_icon">신용카드</label></li>'.PHP_EOL;
                                echo '<label class="btn-default"><input type="radio" id="od_settle_card" name="od_settle_case" value="신용카드" checked><span>신용카드</span></label>'.PHP_EOL;;
                                $checked = '';
                            }
                        }
                    } else {
                        ?>
                        <input type="hidden" id="od_settle_bank" name="od_settle_case" checked value="무통장">
                        <input type="hidden" name="od_bank_account" value="약국주문">
                        <input type="hidden" name="od_deposit_name" value="약국주문">
                        <?php
                    }
                    ?>
                </div>
                <div class="agree-wrap has-border">
                    <div class="form-checkbox form-checkall">
                        <input id="agree_all" name="agree_all" type="checkbox" class="checkbox">
                        <label for="agree_all">전체 동의합니다.</label>
                    </div>
                    <div class="hr"></div>
                    <div class="agree-box">
                        <div class="form-checkbox">
                            <input id="agree_1" name="agree_1" type="checkbox" class="checkbox">
                            <label for="agree_1">(필수) 주문 상품정보에 동의</label>
                            <!--<button type="button" class="btn-default btn-detail mt-5 ml-0" onclick="terms();">상세보기</button>-->
                        </div>
                        <div class="form-checkbox">
                            <input id="agree_2" name="agree_2" type="checkbox" class="checkbox">
                            <label for="agree_2">(필수) 결제 대행 서브스 이용을 위한<br/> 개인정보 제3자 제공 및 위탁 동의</label>
                            <!--<button type="button" class="btn-default btn-detail mt-5 ml-0" onclick="terms();">상세보기</button>-->
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php
        //가격변동이 없을때 주문할수 있음..
        if( $price_change_yn != "Y" ){
            // 출고통제약국일 경우 팝업띄워주고 주문하기 버튼 막기
            if ($member[choice_recommend_pharm_control] == "N" || $is_member == false) {
                // 결제대행사별 코드 include (결제대행사 정보 필드 및 주분버튼)
                require_once(G5_MSHOP_PATH.'/'.$default['de_pg_service'].'/orderform.2.php');
            }
        }else{

            if( $price_change_yn == "Y" ){
                $ViewMsg_1 = "alert('가격변동이 있는 상품이 있습니다.가격변동상품을 삭제후 다시 주문하십시요.');";
            }
            if( $TimeGoodsYn == "Y" ){
                $ViewMsg_2 = "alert('타임세일 상품은 세일 시간에만 구매 가능합니다.');";
            }

            ?>
            <div id="display_pay_button" class="btn_confirm">
                <span id="show_req_btn"><input type="button" name="submitChecked" onClick="<?=$ViewMsg_1?><?=$ViewMsg_2?>" value="결제하기" class="btn_submit"></span>
                <span id="show_pay_btn" style="display:none;"><input type="button" onClick="forderform_check();" value="주문하기" class="btn_submit"></span>
                <a href="<?php echo G5_SHOP_URL; ?>" class="btn_cancel">취소</a>
            </div>
            <?
        }

        if (is_inicis_simple_pay()) {   //삼성페이 또는 L.pay 사용시
            require_once(G5_MSHOP_PATH.'/samsungpay/orderform.2.php');
        }

        if ($is_kakaopay_use) {
            require_once(G5_SHOP_PATH.'/kakaopay/orderform.2.php');
        }
        ?>

        <div id="show_progress" style="display:none;">
            <img src="<?php echo G5_MOBILE_URL; ?>/shop/img/loading.gif" alt="">
            <span>주문완료 중입니다. 잠시만 기다려 주십시오.</span>
        </div>

        <?php
        if ($is_kakaopay_use) {
            require_once(G5_SHOP_PATH.'/kakaopay/orderform.3.php');
        }
        ?>
        <!--<button class="btn-payment"><span>결제하기</span></button>-->

    </form>

    <script>
        $(document).on('click', '.accordion-title', function () {
            if ($(this).hasClass('is-opened')) {
                $(this).next('.accordion-content').slideUp(200);
                $(this).removeClass('is-opened');
            } else {
                $(this).next('.accordion-content').slideDown(200);
                $(this).addClass('is-opened');
            }
        });
    </script>

</div>
<!-- 추가 0121 끝 -->

<?php
if( is_inicis_simple_pay() ){   //삼성페이 사용시
    require_once(G5_MSHOP_PATH.'/samsungpay/order.script.php');
}
?>
<script>
    function addr_company(){
        document.getElementById("company_amount").style.visibility = "visible";
        var send_cost = parseInt($("input[name=od_send_cost]").val());
        $(".company_amount").text("- "+send_cost);
        $("input[name=company_sendcost]").val(send_cost);

        if($("input[name=od_temp_point]").val() > 0 ){
            alert("포인트를 다시 입력 해주세요.")
            $(".point_amount").text("0");
            $("input[name=od_temp_point]").val(0);
            $("#od_point").val(0);
        }
        calculate_order_price();
    }

    var zipcode = "";

    $(function() {
        var $cp_btn_el;
        var $cp_row_el;

        $(".cp_btn").click(function() {
            $cp_btn_el = $(this);
            $cp_row_el = $(this).closest("li");
            $("#cp_frm").remove();
            var it_id = $cp_btn_el.closest("li").find("input[name^=it_id]").val();

            $.post(
                "./orderitemcoupon.php",
                { it_id: it_id,  sw_direct: "<?php echo $sw_direct; ?>" },
                function(data) {
                    $cp_btn_el.after(data);
                }
            );
        });

        $(document).on("click", ".cp_apply", function() {
            var $el = $(this).closest("tr");
            var cp_id = $el.find("input[name='f_cp_id[]']").val();
            var price = $el.find("input[name='f_cp_prc[]']").val();
            var subj = $el.find("input[name='f_cp_subj[]']").val();
            var sell_price;
            var sc_cost;

            if(parseInt(price) == 0) {
                if(!confirm(subj+"쿠폰의 할인 금액은 "+price+"원입니다.\n쿠폰을 적용하시겠습니까?")) {
                    return false;
                }
            }

            // 이미 사용한 쿠폰이 있는지
            var cp_dup = false;
            var cp_dup_idx;
            var $cp_dup_el;
            $("input[name^=cp_id]").each(function(index) {
                var id = $(this).val();

                if(id == cp_id) {
                    cp_dup_idx = index;
                    cp_dup = true;
                    $cp_dup_el = $(this).closest("li");;

                    return false;
                }
            });

            if(cp_dup) {
                var it_name = $("input[name='it_name["+cp_dup_idx+"]']").val();
                if(!confirm(subj+ "쿠폰은 "+it_name+"에 사용되었습니다.\n"+it_name+"의 쿠폰을 취소한 후 적용하시겠습니까?")) {
                    return false;
                } else {
                    coupon_cancel($cp_dup_el);
                    $("#cp_frm").remove();
                    $cp_dup_el.find(".cp_btn").text("쿠폰사용").removeClass("cp_mod").focus();
                    $cp_dup_el.find(".cp_cancel").remove();
                }
            }

            var $s_el = $cp_row_el.find(".total_price strong");
            sc_cost = parseInt($cp_row_el.find("input[name^=it_sc_cost]").val());
            sell_price = parseInt($cp_row_el.find("input[name^=it_price]").val());
            sell_price = sell_price - parseInt(price) + parseInt(sc_cost);

            if(sell_price < 0) {
                alert("쿠폰할인금액이 상품 주문금액보다 크므로 쿠폰을 적용할 수 없습니다.");
                return false;
            }
            $s_el.text(number_format(String(sell_price)));
            $cp_row_el.find("input[name^=cp_id]").val(cp_id);
            $cp_row_el.find("input[name^=cp_price]").val(price);

            calculate_total_price();
            $("#cp_frm").remove();
            $cp_btn_el.text("변경").addClass("cp_mod").focus();
            if(!$cp_row_el.find(".cp_cancel").size())
                $cp_btn_el.after("<button type=\"button\" class=\"cp_cancel\">취소</button>");
        });

        $(document).on("click", "#cp_close", function() {
            $("#cp_frm").remove();
            $cp_btn_el.focus();
        });

        $(document).on("click", ".cp_cancel", function() {
            coupon_cancel($(this).closest("li"));
            calculate_total_price();
            $("#cp_frm").remove();
            $(this).closest("li").find(".cp_btn").text("쿠폰사용").removeClass("cp_mod").focus();
            $(this).remove();
        });

        $("#od_coupon_btn").click(function() {
            $("#od_coupon_frm").remove();
            var $this = $(this);
            var price = parseInt($("input[name=org_od_price]").val()) - parseInt($("input[name=item_coupon]").val());
            if(price <= 0) {
                alert('상품금액이 0원이므로 쿠폰을 사용할 수 없습니다.');
                return false;
            }

            var arr_it_id = new Array();
            $("input[name^=it_id]").each(function(index) {
                var id = $(this).val();
                arr_it_id.push(id);
            });

            $.post(
                "./ordercoupon1.php",
                { price: price, it_id: arr_it_id},
                function(data) {
                    $this.after(data);
                }
            );
        });

        $(document).on("click", ".od_cp_apply", function() {
            var $el = $(this).closest("tr");
            var cp_id = $el.find("input[name='o_cp_id[]']").val();
            var price = parseInt($el.find("input[name='o_cp_prc[]']").val());
            var subj = $el.find("input[name='o_cp_subj[]']").val();
            var send_cost = $("input[name=od_send_cost]").val();
            var item_coupon = parseInt($("input[name=item_coupon]").val());
            var od_price = parseInt($("input[name=org_od_price]").val()) - item_coupon;
            var company_sendcost = parseInt($("input[name=company_sendcost]").val());

            if(price == 0) {
                alert("선택된 쿠폰이 없습니다.");
                return false;
                /*if(!confirm(subj+"쿠폰의 할인 금액은 "+price+"원입니다.\n쿠폰을 적용하시겠습니까?")) {
                    return false;
                }*/
            }

            if(od_price - price < 0) {
                alert("쿠폰할인금액이 주문금액보다 크므로 쿠폰을 적용할 수 없습니다.");
                return false;
            }

            $("input[name=sc_cp_id]").val("");
            $("#sc_coupon_btn").text("쿠폰적용");
            $("#sc_coupon_cancel").remove();

            $("input[name=od_price]").val(od_price - price);
            $("input[name=od_cp_id]").val(cp_id);
            $("input[name=od_coupon]").val(price);
            $("input[name=od_send_coupon]").val(0);
            $("#od_cp_price").val(price);
            $("#sc_cp_price").text(0);
            calculate_order_price();
            $("#od_coupon_frm").remove();
            $("#od_coupon_btn").text("변경").focus();
            if(!$("#od_coupon_cancel").size())
                $("#od_coupon_btn").after("<button type=\"button\" id=\"od_coupon_cancel\" class=\"btn-default btn-cancel\">취소</button>");
        });

        $(document).on("click", "#od_coupon_close", function() {
            $("#od_coupon_frm").remove();
            $("#od_coupon_btn").focus();
        });

        $(document).on("click", "#od_coupon_cancel", function() {
            var org_price = $("input[name=org_od_price]").val();
            var item_coupon = parseInt($("input[name=item_coupon]").val());
            $("input[name=od_price]").val(org_price - item_coupon);
            $("input[name=od_cp_id]").val("");
            $("input[name=sc_cp_id]").val("");
            $("input[name=od_coupon]").val(0);
            $("input[name=od_send_coupon]").val(0);
            $("#od_cp_price").val(0);
            $("#sc_cp_price").text(0);
            calculate_order_price();
            $("#od_coupon_frm").remove();
            $("#od_coupon_btn").text("쿠폰적용").focus();
            $(this).remove();
            $("#sc_coupon_btn").text("쿠폰적용");
            $("#sc_coupon_cancel").remove();
        });

        $("#sc_coupon_btn").click(function() {
            $("#sc_coupon_frm").remove();
            var $this = $(this);
            var price = parseInt($("input[name=od_price]").val());
            var send_cost = parseInt($("input[name=od_send_cost]").val());
            $.post(
                "./ordersendcostcoupon.php",
                { price: price, send_cost: send_cost },
                function(data) {
                    $this.after(data);
                }
            );
        });

        $(document).on("click", ".sc_cp_apply", function() {
            var $el = $(this).closest("tr");
            var cp_id = $el.find("input[name='s_cp_id[]']").val();
            var price = parseInt($el.find("input[name='s_cp_prc[]']").val());
            var subj = $el.find("input[name='s_cp_subj[]']").val();
            var send_cost = parseInt($("input[name=od_send_cost]").val());

            if(parseInt(price) == 0) {
                if(!confirm(subj+"쿠폰의 할인 금액은 "+price+"원입니다.\n쿠폰을 적용하시겠습니까?")) {
                    return false;
                }
            }

            $("input[name=sc_cp_id]").val(cp_id);
            $("input[name=od_send_coupon]").val(price);
            $("#sc_cp_price").text(number_format(String(price)));
            calculate_order_price();
            $("#sc_coupon_frm").remove();
            $("#sc_coupon_btn").text("변경").focus();
            if(!$("#sc_coupon_cancel").size())
                $("#sc_coupon_btn").after("<button type=\"button\" id=\"sc_coupon_cancel\" class=\"btn-default btn-cancel\">취소</button>");
        });

        $(document).on("click", "#sc_coupon_close", function() {
            $("#sc_coupon_frm").remove();
            $("#sc_coupon_btn").focus();
        });

        $(document).on("click", "#sc_coupon_cancel", function() {
            $("input[name=od_send_coupon]").val(0);
            $("#sc_cp_price").text(0);
            calculate_order_price();
            $("#sc_coupon_frm").remove();
            $("#sc_coupon_btn").text("쿠폰적용").focus();
            $(this).remove();
        });

        $("#od_b_addr2").focus(function() {
            var zip = $("#od_b_zip").val().replace(/[^0-9]/g, "");
            if(zip == "")
                return false;

            var code = String(zip);

            if(zipcode == code)
                return false;

            zipcode = code;
            calculate_sendcost(code);
        });

        $("#od_settle_bank").on("click", function() {
            $("[name=od_deposit_name]").val( $("[name=od_name]").val() );
            $("#settle_bank").show();
            $("#show_req_btn").css("display", "none");
            $("#show_pay_btn").css("display", "inline");
        });

        $("#od_settle_iche,#od_settle_card,#od_settle_vbank,#od_settle_hp,#od_settle_easy_pay,#od_settle_kakaopay,#od_settle_samsungpay").bind("click", function() {
            $("#settle_bank").hide();
            $(".sod_frm_point").show();
            $("#show_req_btn").css("display", "inline");
            $("#show_pay_btn").css("display", "none");
        });

        $("#od_settle_vbank").on("click", function() {
            $("[name=od_temp_point]").val(0);
            $(".sod_frm_point").hide();
        });


        $("input[name=ad_sel_addr2]").on("click", function() {
            var f = document.forderform;
            f.od_b_name.value = f.od_name.value;
            f.od_b_hp.value = f.od_hp.value;

        });

        // 배송지선택
        $("input[name=ad_sel_addr]").on("click", function() {
            var f = document.forderform;
            var addr = $(this).val().split(String.fromCharCode(30));
            var origin_sendcost =  $("input[name=od_send_cost_origin]").val();

            $("#multi_receiver_list").css("display", "none");
            $("#mylist_button").css("display", "none");
            $("#tbl_receiver_content").css("display", "");
            $(".tbl_receiver_content_1").height(0)
            $(".tbl_receiver_content_1").css("min-height", "");

            f.od_b_zip.disabled = false;
            f.ad_subject.readOnly = false;
            f.od_b_name.readOnly = false;
            f.od_b_hp.readOnly = false;
            f.od_b_addr2.readOnly = false;
            f.od_b_addr3.readOnly = false;
            f.od_zip.disabled = false;

            $(".sc_price").html(number_format(origin_sendcost));
            if (addr[0] == "same") {
                $(document).find(".m_cnt").remove();
                f.ad_subject.value = '';
                f.od_b_addr1.value = '';
                f.od_b_addr2.value = '';
                f.od_b_addr3.value = '';
                document.getElementById("company_amount").style.visibility = "hidden";
                $("input[name=company_sendcost]").val(0);
                $("input[name=od_send_cost]").val(origin_sendcost);
                calculate_order_price();
                gumae2baesong();
            } else if($("#addr_company").is(':checked')) {
            } else {
                if(addr[0] == "new") {
                    for(i=0; i<10; i++) {
                        addr[i] = "";
                    }
                }

                f.od_b_name.value        = addr[0];
                //f.od_b_tel.value         = addr[1];
                f.od_b_hp.value          = addr[2];
                f.od_b_zip.value         = addr[3] + addr[4];
                f.od_b_addr1.value       = addr[5];
                f.od_b_addr2.value       = addr[6];
                f.od_b_addr3.value       = addr[7];
                f.od_b_addr_jibeon.value = addr[8];
                f.ad_subject.value       = addr[9];

                var zip1 = addr[3].replace(/[^0-9]/g, "");
                var zip2 = addr[4].replace(/[^0-9]/g, "");

                var code = String(zip1) + String(zip2);

                if(zipcode != code) {
                    calculate_sendcost(code);
                }
                document.getElementById("company_amount").style.visibility = "hidden";
                $("input[name=company_sendcost]").val(0);
                calculate_order_price();

            }
        });

        // 배송지목록
        $("#order_address").on("click", function() {
            var origin_sendcost =  $("input[name=od_send_cost_origin]").val();
            $("input:radio[name^=ad_sel_addr]").attr("checked", false);
            var url = this.href;
            window.open(url, "win_address", "left=100,top=100,width=650,height=500,scrollbars=1");
            document.getElementById("company_amount").style.visibility = "hidden";
            $("input[name=company_sendcost]").val(0);
            $("input[name=od_send_cost]").val(origin_sendcost);
            calculate_order_price();
            return false;
        });

        // 모두선택
        $("input[name=all_check]").click(function () {
            if ($(this).is(":checked"))
                $("input[name^=multi_chk]").attr("checked", true);
            else
                $("input[name^=multi_chk]").attr("checked", false);
        });


        $(document).on("click", "#multiYN", function () {
            var origin_sendcost =  $("input[name=od_send_cost_origin]").val();
            if ($("input:radio[id='multiYN']").is(":checked")) {
                $("#multi_receiver_list").css("display", "");
                $("#mylist_button").css("display", "");
                $("#tbl_receiver_content").css("display", "none");
                $(".tbl_receiver_content_1").height('auto');
                $(".tbl_receiver_content_1").css("min-height", "180px");

                document.getElementById("company_amount").style.visibility = "hidden";
                MultiAdd();
            }
            calculate_order_price();
        });

        $(document).on("click", ".save_bill", function() {
            $("#save_info").toggle(100);
        });

        $(document).on("change", "#select_bill_info", function() {

            var SelectValue = $("#select_bill_info option:selected").val();
            var TmpTxt = "<font color='red'>선택한 결제수단으로 향후 결제 이용에 동의합니다</font>";

            if( SelectValue == "2" ){
                $("#bill_txt").html(TmpTxt);
            }else{
                $("#bill_txt").empty();
            }

        });

        $("#term_all").click(function() {
            if ($(this).prop('checked')) {
                $("input[name^=term_]").prop('checked', true);
            } else {
                $("input[name^=term_]").prop("checked", false);
            }
        });

        $("#agree_all").click(function() {
            if ($(this).prop('checked')) {
                $("input[name^=agree_]").prop('checked', true);
            } else {
                $("input[name^=agree_]").prop("checked", false);
            }
        });

        $('.trans_memo_value').click(function() {
            if ($(this).text() == "직접입력") {
                $('#od_trans_memo').attr('readonly', false);
                $('#od_trans_memo').attr('placeholder', '배송 메시지를 입력해 주세요.');
                $('#od_trans_memo').val('');
                $('#od_trans_memo').css('background', '#fff');

                setTimeout(function() {
                    $("#od_trans_memo").focus();
                }, 100);

                $('#od_trans_memo_toggle').remove();
            } else {
                $('#od_trans_memo').val($(this).text());
                $('#od_trans_memo').attr('placeholder', '배송 메시지를 선택해 주세요.');
                $('#od_trans_memo').attr('readonly', true);
            }
        });

    });

    function SaveInfo(GetIdx){

        if(confirm("선택하신 카드정보를 기본결제수단으로 등록하시겠습니까?")) {

            $.ajax({
                type: "POST",
                url: "./save_info.php",
                cache: false,
                async: false,
                data: { m_idx: GetIdx },
                dataType: "html",
                success: function(data) {

                    $("#save_total_info").empty();
                    $("#save_total_info").html(data);

                }
            });

        }

    }

    function DelInfo(GetIdx){

        if(confirm("선택하신 카드정보를 삭제하시겠습니까?")) {

            $.ajax({
                type: "POST",
                url: "./del_info.php",
                cache: false,
                async: false,
                data: { m_idx: GetIdx },
                dataType: "html",
                success: function(data) {
                    $("#save_info").empty();
                    $("#save_info").html(data);

                }
            });

        }

    }

    function od_gubun_chg2(){
        var f = document.forderform;

        /*frm.ord_name.value 		= "인포약국";
        frm.ord_tel1.value 		= "02";
        frm.ord_tel2.value 		= "391";
        frm.ord_tel3.value 		= "0660";
        frm.ord_hp1.value 		= "010";
        frm.ord_hp2.value 		= "3605";
        frm.ord_hp3.value 		= "5879";
        frm.ord_zipcode.value 	= "122-829";
        frm.ord_addr1.value 	= "서울특별시 은평구 녹번동 77~91";
        frm.ord_addr2.value 	= "83-54번지 원기빌딩 202호1212";*/

        f.od_name.value        = "<?php echo get_text($member['mb_name']); ?>";
        //f.od_tel.value         = "<?php echo get_text($member['mb_tel']); ?>";
        f.od_hp.value          = "<?php echo get_text($member['mb_hp']); ?>";
        f.od_zip.value         = "<?php echo $member['mb_zip1'].$member['mb_zip2']; ?>";
        f.od_addr1.value       = "<?php echo get_text($member['mb_addr1']) ?>";
        f.od_addr2.value       = "<?php echo get_text($member['mb_addr2']) ?>";
        f.od_addr3.value       = "<?php echo get_text($member['mb_addr3']) ?>";
        f.od_addr_jibeon.value = "<?php echo get_text($member['mb_addr_jibeon']); ?>";
        f.od_email.value       = "<?php echo $member['mb_email']; ?>";

    }

    function od_gubun_chg1(){
        var f = document.forderform;

        f.od_name.value        = "";
        //f.od_tel.value         = "";
        f.od_hp.value          = "";
        f.od_zip.value         = "";
        f.od_addr1.value       = "";
        f.od_addr2.value       = "";
        f.od_addr3.value       = "";
        f.od_addr_jibeon.value = "";
        f.od_email.value       = "";

    }
    function myaddr(gubun) {
        var url = "<?php echo G5_SHOP_URL;?>/orderuser.php?gubun="+gubun ;
        window.open(url, "win_address", "left=100,top=100,width=800,height=600,scrollbars=1");
        return false;
    }
    function myaddr2(gubun) {
        var url = "<?php echo G5_SHOP_URL;?>/orderuser.php?gubun="+gubun+"&multi_gubun=Y" ;
        window.open(url, "win_address", "left=100,top=100,width=800,height=600,scrollbars=1");
        //return false;
        return;
    }
    function myaddr3(gubun) {
        var url = "<?php echo G5_SHOP_URL;?>/orderaddress.php?gubun="+gubun+"&multi_gubun=Y" ;
        window.open(url, "win_address", "left=100,top=100,width=800,height=600,scrollbars=1");
        return false;
    }

    function myaddr4() {
        var it_company = $('#it_company').val();
        var od_name = $('#od_name').val();
        var url = "<?php echo G5_SHOP_URL;?>/orderaddresscompany.php?it_company="+it_company+"&od_name="+od_name+"";
        window.open(url, "win_address","width=350,height=400");
        return false;
    }


    function coupon_cancel($el)
    {
        var $dup_sell_el = $el.find(".total_price strong");
        var $dup_price_el = $el.find("input[name^=cp_price]");
        var org_sell_price = $el.find("input[name^=it_price]").val();

        $dup_sell_el.text(number_format(String(org_sell_price)));
        $dup_price_el.val(0);
        $el.find("input[name^=cp_id]").val("");
    }

    function calculate_total_price()
    {
        var $it_prc = $("input[name^=it_price]");
        var $cp_prc = $("input[name^=cp_price]");
        var tot_sell_price = sell_price = tot_cp_price = 0;
        var it_price, cp_price, it_notax;
        var tot_mny = comm_tax_mny = comm_vat_mny = comm_free_mny = tax_mny = vat_mny = 0;
        var send_cost = parseInt($("input[name=od_send_cost]").val());

        $it_prc.each(function(index) {
            it_price = parseInt($(this).val());
            cp_price = parseInt($cp_prc.eq(index).val());
            sell_price += it_price;
            tot_cp_price += cp_price;
        });

        tot_sell_price = sell_price - tot_cp_price + send_cost;

        $("#ct_tot_coupon").text(number_format(String(tot_cp_price))+" 원");
        $("#ct_tot_price").text(number_format(String(tot_sell_price)));

        $("input[name=good_mny]").val(tot_sell_price);
        $("input[name=od_price]").val(sell_price - tot_cp_price);
        $("input[name=item_coupon]").val(tot_cp_price);

        //$("input[name=od_temp_point]").val(0);
        <?php if($temp_point > 0 && $is_member) { ?>
        calculate_temp_point();
        <?php } ?>
        calculate_order_price();
    }

    function calculate_order_price()
    {
        var sell_price = parseInt($("input[name=org_od_price]").val());
        var send_cost = parseInt($("input[name=od_send_cost]").val());
        var send_cost2 = parseInt($("input[name=od_send_cost2]").val());
        var period_cnt = parseInt($("input[name^=period_cnt]").val());

        var is_member = '<?php echo ($is_member) ? 'Y' : 'N'; ?>';
        var tot_price;

        if (is_member == 'Y') {
            // 회원에게만 해당되는 변수
            var send_coupon = parseInt($("input[name=od_send_coupon]").val());
            var item_coupon = parseInt($("input[name=item_coupon]").val());
            var od_coupon = parseInt($("input[name=od_coupon]").val());
            var od_point = parseInt($("input[name=od_temp_point").val());
            var company_sendcost = '';
            if($("#addr_company").is(':checked')) {
            company_sendcost = parseInt($("input[name=company_sendcost").val());
            }
            if(period_cnt>1){
                send_cost*=period_cnt;
            }

            tot_price = sell_price + send_cost + send_cost2 - send_coupon - item_coupon - od_coupon - od_point - company_sendcost;
        } else {
            tot_price = sell_price + send_cost + send_cost2;
        }

        $("form[name=sm_form] input[name=good_mny]").val(tot_price);
        $("#ct_tot_price .price").text(number_format(String(tot_price)));

        // 쿠폰 포인트 사용금액 노출 처리
        $("#ct_cp_price .price").text(number_format(String(-(item_coupon + od_coupon))));
        $("#ct_cp_point .price").text(number_format(String(-od_point)));

        // 쿠폰을 사용하여 결제금액이 0원이 되는 경우
        /*if (tot_price == 0 && $("input[name=od_cp_id]").val() != '') {
            $('#od_settle_point').closest('li').show();
            $("input:radio[id='od_settle_point']").prop("checked", true);

            $("input:radio[id='od_settle_card']").attr('disabled', true);
            $("input:radio[id='od_settle_iche']").attr('disabled', true);
            $("input:radio[id='od_settle_hp']").attr('disabled', true);
        } else {
            $('#od_settle_point').closest('li').hide();
            $("input:radio[id='od_settle_point']").prop("checked", false);

            $("input:radio[id='od_settle_card']").attr('disabled', false);
            $("input:radio[id='od_settle_iche']").attr('disabled', false);
            $("input:radio[id='od_settle_hp']").attr('disabled', false);
        }*/
        <?php if($temp_point > 0 && $is_member) { ?>
        calculate_temp_point();
        <?php } ?>
    }

    function calculate_temp_point()
    {
        var sell_price = parseInt($("input[name=od_price]").val());
        var mb_point = parseInt(<?php echo $member['mb_point']; ?>);
        var max_point = parseInt(<?php echo $default['de_settle_max_point']; ?>);
        var point_unit = parseInt(<?php echo $default['de_settle_point_unit']; ?>);
        var temp_point = max_point;

        if(temp_point > sell_price)
            temp_point = sell_price;

        if(temp_point > mb_point)
            temp_point = mb_point;

        temp_point = parseInt(temp_point / point_unit) * point_unit;

        var ct_tot_price = $('input[name=good_mny]').val();

        //$("#use_max_point").text(number_format(String(ct_tot_price))+"점");
        //$("input[name=max_temp_point]").val(ct_tot_price);
    }

    function calculate_sendcost(code)
    {
        $.post(
            "./ordersendcost.php",
            { zipcode: code },
            function(data) {
                $("input[name=od_send_cost2]").val(data);
                $("#od_send_cost2").text(number_format(String(data)));

                zipcode = code;

                calculate_order_price();
            }
        );
    }

    function calculate_tax()
    {
        var $it_prc = $("input[name^=it_price]");
        var $cp_prc = $("input[name^=cp_price]");
        var sell_price = tot_cp_price = 0;
        var it_price, cp_price, it_notax;
        var tot_mny = comm_free_mny = tax_mny = vat_mny = 0;
        var send_cost = parseInt($("input[name=od_send_cost]").val());
        var send_cost2 = parseInt($("input[name=od_send_cost2]").val());
        var od_coupon = parseInt($("input[name=od_coupon]").val());
        var send_coupon = parseInt($("input[name=od_send_coupon]").val());
        var temp_point = 0;

        $it_prc.each(function(index) {
            it_price = parseInt($(this).val());
            cp_price = parseInt($cp_prc.eq(index).val());
            sell_price += it_price;
            tot_cp_price += cp_price;
            it_notax = $("input[name^=it_notax]").eq(index).val();
            if(it_notax == "1") {
                comm_free_mny += (it_price - cp_price);
            } else {
                tot_mny += (it_price - cp_price);
            }
        });

        if($("input[name=od_temp_point]").size())
            temp_point = parseInt($("input[name=od_temp_point]").val());

        tot_mny += (send_cost + send_cost2 - od_coupon - send_coupon - temp_point);
        if(tot_mny < 0) {
            comm_free_mny = comm_free_mny + tot_mny;
            tot_mny = 0;
        }

        tax_mny = Math.round(tot_mny / 1.1);
        vat_mny = tot_mny - tax_mny;
        $("input[name=comm_tax_mny]").val(tax_mny);
        $("input[name=comm_vat_mny]").val(vat_mny);
        $("input[name=comm_free_mny]").val(comm_free_mny);
    }

    /* 결제방법에 따른 처리 후 결제등록요청 실행 */
    var settle_method = "무통장";
    var temp_point = 0;

    function pay_approval()
    {
        var f = document.forderform;
        var point_pay = false; //포인트전액결제 여부

        var od_price = parseInt(f.od_price.value);
        var send_cost = parseInt(f.od_send_cost.value);
        var send_cost2 = parseInt(f.od_send_cost2.value);
        var send_coupon = parseInt(f.od_send_coupon.value);
        var period_cnt = parseInt(f.period_cnt.value);
        if(period_cnt > 1 ){
            send_cost*=period_cnt;
        }
        var TotalPrice = od_price + send_cost + send_cost2 - send_coupon;

        temp_point = 0;

        //사용포인트가 총 주문금액과 같은지 비교해서 포인트결제버튼 선택해주기
        if (typeof(f.od_temp_point) != "undefined") {
            if (f.od_temp_point.value){

                if( !$.isNumeric( f.od_temp_point.value ) ){
                    alert("사용 포인트를 숫자형식으로 입력해주세요");
                    return false;
                }

                temp_point = parseInt(f.od_temp_point.value);

                if( temp_point == TotalPrice ){
                    point_pay = true;
                }


            }
        }

        //쿠폰을 사용하여 총 주문금액이 0원인 경우 포인트결제로 인식
        if (TotalPrice == 0 && $("input[name=od_cp_id]").val() != '') {
            point_pay = true;
        }

        //전액 포인트결제일때는 pg 를 태우지 않음.
        if( point_pay == false ){

            // 재고체크
            var stock_msg = order_stock_check();
            if(stock_msg != "") {
                alert(stock_msg);
                return false;
            }

            var f = document.sm_form;
            var pf = document.forderform;

            // 필드체크
            if(!orderfield_check(pf))
                return false;

            // 금액체크
            if(!payment_check(pf))
                return false;

            // pg 결제 금액에서 포인트 금액 차감
            if(settle_method != "무통장") {
                var od_price = parseInt(pf.od_price.value);
                var send_cost = parseInt(pf.od_send_cost.value);
                var send_cost2 = parseInt(pf.od_send_cost2.value);
                var send_coupon = parseInt(pf.od_send_coupon.value);
                var period_cnt = parseInt(pf.period_cnt.value);
                if(period_cnt > 1 ){
                    send_cost*=period_cnt;
                }
                var company_sendcost = parseInt(pf.company_sendcost.value);

                <?php if (!$is_member) { ?>
                f.good_mny.value = od_price + send_cost + send_cost2;
                <?php } else {?>
                f.good_mny.value = od_price + send_cost + send_cost2 - send_coupon - temp_point - company_sendcost;
                <?php   }    ?>
            }

            // 카카오페이 지불
            if(settle_method == "KAKAOPAY") {
                <?php if($default['de_tax_flag_use']) { ?>
                pf.SupplyAmt.value = parseInt(pf.comm_tax_mny.value) + parseInt(pf.comm_free_mny.value);
                pf.GoodsVat.value  = parseInt(pf.comm_vat_mny.value);
                <?php } ?>
                pf.good_mny.value = f.good_mny.value;
                getTxnId(pf);
                return false;
            }

            var form_order_method = '';

            if( settle_method == "삼성페이" || settle_method == "lpay" ){
                form_order_method = 'samsungpay';
            }

            if( jQuery(pf).triggerHandler("form_sumbit_order_"+form_order_method) !== false ) {
                <?php if($default['de_pg_service'] == 'kcp') { ?>
                f.buyr_name.value = pf.od_name.value;
                f.buyr_mail.value = pf.od_email.value;
                f.buyr_tel1.value = pf.od_hp.value;
                f.buyr_tel2.value = pf.od_hp.value;
                f.rcvr_name.value = pf.od_b_name.value;
                f.rcvr_tel1.value = pf.od_b_hp.value;
                f.rcvr_tel2.value = pf.od_b_hp.value;
                f.rcvr_mail.value = pf.od_email.value;
                f.rcvr_zipx.value = pf.od_b_zip.value;
                f.rcvr_add1.value = pf.od_b_addr1.value;
                f.rcvr_add2.value = pf.od_b_addr2.value;
                f.settle_method.value = settle_method;
                if(settle_method == "간편결제")
                    f.payco_direct.value = "Y";
                else
                    f.payco_direct.value = "";
                <?php } else if($default['de_pg_service'] == 'lg') { ?>
                var pay_method = "";
                var easy_pay = "";
                switch(settle_method) {
                    case "계좌이체":
                        pay_method = "SC0030";
                        break;
                    case "가상계좌":
                        pay_method = "SC0040";
                        break;
                    case "휴대폰":
                        pay_method = "SC0060";
                        break;
                    case "신용카드":
                        pay_method = "SC0010";
                        break;
                    case "간편결제":
                        easy_pay = "PAYNOW";
                        break;
                }
                f.LGD_CUSTOM_FIRSTPAY.value = pay_method;
                f.LGD_BUYER.value = pf.od_name.value;
                f.LGD_BUYEREMAIL.value = pf.od_email.value;
                f.LGD_BUYERPHONE.value = pf.od_hp.value;
                f.LGD_AMOUNT.value = f.good_mny.value;
                f.LGD_EASYPAY_ONLY.value = easy_pay;
                <?php if($default['de_tax_flag_use']) { ?>
                f.LGD_TAXFREEAMOUNT.value = pf.comm_free_mny.value;
                <?php } ?>
                <?php } else if($default['de_pg_service'] == 'inicis') { ?>
                var paymethod = "";
                var width = 330;
                var height = 480;
                var xpos = (screen.width - width) / 2;
                var ypos = (screen.width - height) / 2;
                var position = "top=" + ypos + ",left=" + xpos;
                var features = position + ", width=320, height=440";
                var p_reserved = f.DEF_RESERVED.value;
                f.P_RESERVED.value = p_reserved;
                switch(settle_method) {
                    case "계좌이체":
                        paymethod = "bank";
                        break;
                    case "가상계좌":
                        paymethod = "vbank";
                        break;
                    case "휴대폰":
                        paymethod = "mobile";
                        break;
                    case "신용카드":
                        paymethod = "wcard";
                        f.P_RESERVED.value = f.P_RESERVED.value.replace("&useescrow=Y", "");
                        break;
                    case "간편결제":
                        paymethod = "wcard";
                        f.P_RESERVED.value = p_reserved+"&d_kpay=Y&d_kpay_app=Y";
                        break;
                    case "삼성페이":
                        paymethod = "wcard";
                        f.P_RESERVED.value = f.P_RESERVED.value.replace("&useescrow=Y", "")+"&d_samsungpay=Y";
                        //f.DEF_RESERVED.value = f.DEF_RESERVED.value.replace("&useescrow=Y", "");
                        f.P_SKIP_TERMS.value = "Y"; //약관을 skip 해야 제대로 실행됨
                        break;
                    case "lpay":
                        paymethod = "wcard";
                        f.P_RESERVED.value = f.P_RESERVED.value.replace("&useescrow=Y", "")+"&d_lpay=Y";
                        //f.DEF_RESERVED.value = f.DEF_RESERVED.value.replace("&useescrow=Y", "");
                        f.P_SKIP_TERMS.value = "Y"; //약관을 skip 해야 제대로 실행됨
                        break;
                }
                <?php if($period=='y') { ?>
                f.price.value = f.good_mny.value;
                <?php } ?>
                f.P_AMT.value = f.good_mny.value;
                f.P_UNAME.value = pf.od_name.value;
                f.P_MOBILE.value = pf.od_hp.value;
                f.P_EMAIL.value = pf.od_email.value;
                <?php if($default['de_tax_flag_use']) { ?>
                f.P_TAX.value = pf.comm_vat_mny.value;
                f.P_TAXFREE = pf.comm_free_mny.value;
                <?php } ?>
                f.P_RETURN_URL.value = "<?php echo $return_url.$od_id; ?>";
                <?php if($period=='y') { ?>
                f.action = "https://inilite.inicis.com/inibill/inibill_card.jsp";
                <?php } else { ?>
                f.action = "https://mobile.inicis.com/smart/" + paymethod + "/";
                <?php } ?>
                <?php } ?>

                // 주문 정보 임시저장
                var order_data = $(pf).serialize();
                var save_result = "";
                $.ajax({
                    type: "POST",
                    data: order_data,
                    url: g5_url+"/shop/ajax.orderdatasave.php",
                    cache: false,
                    async: false,
                    success: function(data) {
                        save_result = data;
                    }
                });

                if(save_result) {
                    alert(save_result);
                    return false;
                }

                //결제수단선택 select box 가 있다면 실행
                if( $("#select_bill_info").length > 0 ){

                    if( $("#select_bill_info").val() == "" ){
                        alert("저장된 결제수단을 선택해주세요.");
                        return false;
                    }
                    //저장된 카드 선택
                    if( $("#select_bill_info").val() == "1" ){
                        pf.method = "post";
                        pf.action = "<?=$order_action_url?>";
                        pf.submit();
                    }else{
                        f.submit();
                    }

                }else{
                    f.submit();
                }

            }

            return false;

        }else{

            forderform_check();

        }
    }

    function forderform_check()
    {
        var f = document.forderform;

        // 필드체크
        if(!orderfield_check(f))
            return false;

        // 금액체크
        if(!payment_check(f))
            return false;

        if(settle_method != "무통장" && settle_method != "포인트" && f.res_cd.value != "0000") {
            alert("결제등록요청 후 주문해 주십시오.");
            return false;
        }

        document.getElementById("display_pay_button").style.display = "none";
        document.getElementById("show_progress").style.display = "block";

        setTimeout(function() {
            f.submit();
        }, 300);
    }

    // 주문폼 필드체크
    function orderfield_check(f)
    {
        var offset;
        <?php if (!$is_member) { ?>
        // 비회원 주문 약관 체크
        if ($('#term_1').prop('checked') == false) {
            alert('개인정보수집 이용 동의를 해주세요.');
            offset = $("#term_1").offset();
            $('html, body').animate({scrollTop : offset.top-200}, 800);
            return false;
        }
        if ($('#term_2').prop('checked') == false) {
            alert('이용약관에 동의를 해주세요.');
            offset = $("#term_2").offset();
            $('html, body').animate({scrollTop : offset.top-200}, 800);
            return false;
        }
        <?php } ?>

        errmsg = "";
        errfld = "";
        var deffld = "";

        check_field(f.od_name, "주문하시는 분 이름을 입력하십시오.");
        if (typeof(f.od_pwd) != 'undefined')
        {
            clear_field(f.od_pwd);
            if( (f.od_pwd.value.length<3) || (f.od_pwd.value.search(/([^A-Za-z0-9]+)/)!=-1) )
                error_field(f.od_pwd, "회원이 아니신 경우 주문서 조회시 필요한 비밀번호를 3자리 이상 입력해 주십시오.");

            if (f.od_pwd.value != f.od_pwd_chk.value) {
                alert('주문 비밀번호를 확인 해주세요. 재확인 비밀번호와 일치하지 않습니다.');
                f.od_pwd_chk.focus();
                return false;
            }
        }
        <?php if (!$is_member) // 비회원 주문 핸드폰번호 체크 ?>
        check_field(f.od_hp, "주문하시는 분 핸드폰 번호를 입력하십시오.");
        //check_field(f.od_tel, "주문하시는 분 전화번호를 입력하십시오.");
        //check_field(f.od_addr1, "주소검색을 이용하여 주문하시는 분 주소를 입력하십시오.");
        //check_field(f.od_addr2, " 주문하시는 분의 상세주소를 입력하십시오.");
        //check_field(f.od_zip, "");

        clear_field(f.od_email);
        if(f.od_email.value=='' || f.od_email.value.search(/(\S+)@(\S+)\.(\S+)/) == -1)
            error_field(f.od_email, "E-mail을 바르게 입력해 주십시오.");

        if (typeof(f.od_hope_date) != "undefined")
        {
            clear_field(f.od_hope_date);
            if (!f.od_hope_date.value)
                error_field(f.od_hope_date, "희망배송일을 선택하여 주십시오.");
        }

        //다중배송일경우 총상품갯수와 분배갯수 체크
        if ($("input:radio[id='multiYN']").is(":checked")) {

            if($("#multiChk").val() == '3' ){
                alert("배송지를 등록해 주십시오.");
                return false;
            } else if($("#multiChk").val() =='0') {
                alert("배송 정보가 수정되었습니다. \n배송 정보를 확정하여 주십시오.");
                return false;
            }

        } else {
            check_field(f.od_b_name, "받으시는 분 이름을 입력하십시오.");
            check_field(f.od_b_hp, "받으시는 분 핸드폰 번호를 입력하십시오.");
            //check_field(f.od_b_tel, "받으시는 분 전화번호를 입력하십시오.");
            check_field(f.od_b_addr1, "주소검색을 이용하여 받으시는 분 주소를 입력하십시오.");
            //check_field(f.od_b_addr2, "받으시는 분의 상세주소를 입력하십시오.");
            check_field(f.od_b_zip, "");
        }

        var od_settle_hp = document.getElementById("od_settle_hp");
        if (od_settle_hp) {
            if (od_settle_hp.checked) {
                alert('휴대폰 결제는 준비중입니다.');
                return false;
            }
        }

        var od_settle_bank = document.getElementById("od_settle_bank");
        if (od_settle_bank) {
            if (od_settle_bank.checked) {
                check_field(f.od_bank_account, "계좌번호를 선택하세요.");
                check_field(f.od_deposit_name, "입금자명을 입력하세요.");
            }
        }

        // 배송비를 받지 않거나 더 받는 경우 아래식에 + 또는 - 로 대입
        f.od_send_cost.value = parseInt(f.od_send_cost.value);

        if (errmsg)
        {
            alert(errmsg);
            errfld.focus();
            return false;
        }

        <?php if($member['mb_type'] != '1' or !$is_member) { ?>
        var settle_case = document.getElementsByName("od_settle_case");
        var settle_check = false;

        var od_price = parseInt(f.od_price.value);
        var send_cost = parseInt(f.od_send_cost.value);
        var send_cost2 = parseInt(f.od_send_cost2.value);
        var send_coupon = parseInt(f.od_send_coupon.value);
        var company_sendcost = parseInt(f.company_sendcost.value);
        <?php if (!$is_member) { ?>
        var TotalPrice = od_price + send_cost + send_cost2;
        <?php } else {?>
        var TotalPrice = od_price + send_cost + send_cost2 - send_coupon - company_sendcost;
        <?php   }    ?>
        temp_point = 0;

        //사용포인트가 총 주문금액과 같은지 비교해서 포인트결제버튼 선택해주기
        if (typeof(f.od_temp_point) != "undefined") {
            if (f.od_temp_point.value){

                if( !$.isNumeric( f.od_temp_point.value ) ){
                    alert("사용 포인트를 숫자형식으로 입력해주세요");
                    return false;
                }

                temp_point = parseInt(f.od_temp_point.value);

                if( temp_point == TotalPrice ){
                    document.getElementById("od_settle_point").checked = true;
                    settle_method = "포인트";
                }


            }
        }

        for (i=0; i<settle_case.length; i++)
        {
            if (settle_case[i].checked)
            {
                settle_check = true;
                settle_method = settle_case[i].value;
                break;
            }
        }
        if (!settle_check)
        {
            alert("결제방식을 선택하십시오.");
            return false;
        }
        <?php } else { ?>
        var settle_case = document.getElementsByName("od_settle_case");
        settle_method = "무통장";
        <?php } ?>

        if ($('#agree_1').prop('checked') == false) {
            alert('주문 상품정보에 동의를 해주세요.');
            offset = $("#agree_1").offset();
            $('html, body').animate({scrollTop : offset.top-200}, 800);
            return false;
        }
        if ($('#agree_2').prop('checked') == false) {
            alert('결제 대행 서비스 이용을 위한 동의를 해주세요.');
            offset = $("#agree_2").offset();
            $('html, body').animate({scrollTop : offset.top-200}, 800);
            return false;
        }

        return true;
    }

    // 결제체크
    function payment_check(f)
    {
        var max_point = 0;
        var od_price = parseInt(f.od_price.value);
        var send_cost = parseInt(f.od_send_cost.value);
        var send_cost2 = parseInt(f.od_send_cost2.value);
        var send_coupon = parseInt(f.od_send_coupon.value);
        var company_sendcost = parseInt(f.company_sendcost.value);
        var period_cnt = parseInt(f.period_cnt.value);
        if(period_cnt > 1 ){
            send_cost*=period_cnt;
        }
        <?php if (!$is_member) { ?>
        var TotalPrice = od_price + send_cost + send_cost2;
        <?php } else {?>
        var TotalPrice = od_price + send_cost + send_cost2 - send_coupon - company_sendcost;
        <?php   }    ?>
        temp_point = 0;

        if (typeof(f.max_temp_point) != "undefined")
            var max_point  = parseInt(f.max_temp_point.value);

        if (typeof(f.od_temp_point) != "undefined") {
            if (f.od_temp_point.value)
            {
                var point_unit = parseInt(<?php echo $default['de_settle_point_unit']; ?>);
                temp_point = parseInt(f.od_temp_point.value);

                if (temp_point < 0) {
                    alert("포인트를 0 이상 입력하세요.");
                    f.od_temp_point.select();
                    return false;
                }

                if (temp_point > TotalPrice) {
                    alert("상품 주문금액 보다 많이 포인트결제할 수 없습니다.");
                    f.od_temp_point.select();
                    return false;
                }

                if (temp_point > <?php echo (int)$ca_point; ?>) {
                    alert("회원님의 포인트보다 많이 결제할 수 없습니다.");
                    f.od_temp_point.select();
                    return false;
                }

                if (temp_point > max_point) {
                    alert(max_point + "점 이상 결제할 수 없습니다.");
                    f.od_temp_point.select();
                    return false;
                }
                /*
                if (parseInt(parseInt(temp_point / point_unit) * point_unit) != temp_point) {
                    alert("포인트를 "+String(point_unit)+"점 단위로 입력하세요.");
                    f.od_temp_point.select();
                    return false;
                }
                */
            }
        }
        <?php if (!$is_member) { ?>
        var tot_price = od_price + send_cost + send_cost2;
        <?php } else {?>
        var tot_price = od_price + send_cost + send_cost2 - send_coupon - temp_point - company_sendcost;
        <?php   }    ?>

        if (document.getElementById("od_settle_iche")) {
            if (document.getElementById("od_settle_iche").checked) {
                /*
                if (tot_price < 150) {
                    alert("계좌이체는 150원 이상 결제가 가능합니다.");
                    return false;
                }
                */
                if (tot_price < 50) {
                    alert("계좌이체는 50원 이상 결제가 가능합니다.");
                    return false;
                }
            }
        }

        if (document.getElementById("od_settle_card")) {
            if (document.getElementById("od_settle_card").checked) {
                if (tot_price < 1000) {
                    alert("신용카드는 1000원 이상 결제가 가능합니다.");
                    return false;
                }
            }
        }

        if (document.getElementById("od_settle_hp")) {
            if (document.getElementById("od_settle_hp").checked) {
                if (tot_price < 350) {
                    alert("휴대폰은 350원 이상 결제가 가능합니다.");
                    return false;
                }
            }
        }

        <?php if($default['de_tax_flag_use']) { ?>
        calculate_tax();
        <?php } ?>

        return true;
    }

    // 구매자 정보와 동일합니다.
    function gumae2baesong() {
        var f = document.forderform;

        f.od_b_name.value = f.od_name.value;
        //f.od_b_tel.value  = f.od_tel.value;
        f.od_b_hp.value   = f.od_hp.value;
        f.od_b_zip.value  = f.od_zip.value;
        f.od_b_addr1.value = f.od_addr1.value;
        f.od_b_addr2.value = f.od_addr2.value;
        f.od_b_addr3.value = f.od_addr3.value;
        f.od_b_addr_jibeon.value = f.od_addr_jibeon.value;

        calculate_sendcost(String(f.od_b_zip.value));
    }

    <?php if ($default['de_hope_date_use']) { ?>
    $(function(){
        $("#od_hope_date").datepicker({ changeMonth: true, changeYear: true, dateFormat: "yy-mm-dd", showButtonPanel: true, yearRange: "c-99:c+99", minDate: "+<?php echo (int)$default['de_hope_date_after']; ?>d;", maxDate: "+<?php echo (int)$default['de_hope_date_after'] + 6; ?>d;" });
    });
    <?php } ?>
</script>

<script>
    $(".b-close_CONTROL").click(function(){
        $("#PopupLayer_CONTROL").hide();
    });


    //포인트 입력값 비교해서 체크변동시켜주기
    $('#od_temp_point').bind("change keyup input", function(event) {

        var f = document.forderform;
        var od_price = parseInt(f.od_price.value);
        var send_cost = parseInt(f.od_send_cost.value);
        var send_cost2 = parseInt(f.od_send_cost2.value);
        var send_coupon = parseInt(f.od_send_coupon.value);
        <?php if (!$is_member) { ?>
        var TotalPrice = od_price + send_cost + send_cost2;
        <?php } else {?>
        var TotalPrice = od_price + send_cost + send_cost2 - send_coupon;
        <?php   }    ?>
        temp_point = 0;

        //사용포인트가 총 주문금액과 같은지 비교해서 포인트결제버튼 선택해주기
        if (typeof(f.od_temp_point) != "undefined") {
            if (f.od_temp_point.value){

                if( !$.isNumeric( f.od_temp_point.value ) ){
                    alert("사용 포인트를 숫자형식으로 입력해주세요");
                    return false;
                }

                temp_point = parseInt(f.od_temp_point.value);

                if( temp_point == TotalPrice ){
                    document.getElementById("od_settle_point").checked = true;
                    settle_method = "포인트";
                }else{

                    if( document.getElementById("od_settle_point").checked == true ){
                        document.getElementById("od_settle_point").checked = false;
                    }

                }


            }
        }

    });

    $(document).on("click", "#od_point_btn", function() {

        var f = document.forderform;
        if ($.isNumeric(f.od_input_point.value) == false) {
            alert("숫자만 입력 가능합니다.");
            return false;
        }

        var od_price = parseInt(f.od_price.value);
        var send_cost = parseInt(f.od_send_cost.value);
        var send_cost2 = parseInt(f.od_send_cost2.value);
        var send_coupon = parseInt(f.od_send_coupon.value);
        var company_sendcost = parseInt(f.company_sendcost.value)
        <?php if (!$is_member) { ?>
        var TotalPrice = od_price + send_cost + send_cost2;
        <?php } else {?>
        var TotalPrice = od_price + send_cost + send_cost2 - send_coupon - company_sendcost;
        <?php   }    ?>

        var temp_point = parseInt(f.od_input_point.value);

        if (temp_point > TotalPrice) {
            alert("상품 주문금액 보다 많이 포인트결제할 수 없습니다.");
            return false;
        }

        f.od_temp_point.value = temp_point;
        f.od_point.value = temp_point;

        calculate_order_price();

        f.od_input_point.value = '';
        $('#point_input_area').hide();
    });

    function MultiAdd(){
        var multi_sum = 0;
        var order_cnt = 0;
        var cnt = 0;
        var sc_minimum = $('#sc_minimum').val();
        var sc_cost = $('#sc_cost').val();
        var it_sc_cost = 0;
        var multi_cnt = 0;
        var total_price =   $("input[name^=it_price]").val(); // 소계
        var total = 0;
        var it_one_price = $("input[name^=it_one_price]").val(); // 상품 한개 가격
        var multi_addr_cnt = 0; // 배송지갯수
        var cnt_html = "";

        $("input[class=multi_cnt]").each(function (index) {
            multi_cnt =  parseInt($(document).find(".multi_cnt").eq(index).val())
            multi_sum += multi_cnt;
            $("input[name^=multi_cnt]").eq(index).prop("type", "hidden");
            cnt_html = "<sapn class='m_cnt'>"+multi_cnt+"</sapn>";
            $("input[name^=multi_cnt]").eq(index).after(cnt_html);

            if(sc_minimum != ''){
                if(sc_minimum >= (multi_cnt * it_one_price) ){
                    cnt++;
                }
            } else{
                cnt++;
            }

            multi_addr_cnt++;
        });
        it_sc_cost = (cnt*sc_cost); // 유료배송지수*배송비

        total = parseInt(total_price) + parseInt(it_sc_cost);


        order_cnt = parseInt($(document).find("#order_qty").val()); // 총 주문 갯수

        $("input:checkbox[name^=multi_chk]").attr("checked", true);
        var chkbox = document.getElementsByName('multi_chk');
        var chk = false;
        for(var i=0 ; i<chkbox.length ; i++) {
            if(chkbox[i].checked) {
                chk = true;
            }
        }

        if(chk && (multi_sum == 0 || ( order_cnt != multi_sum ))){
            alert("다중배송시 상품수량입력이 잘못되었습니다.");
            $("input[name^=multi_cnt]").prop("type", "text");
            $(document).find(".m_cnt").remove();
            return false;
        }else if(chk){
            alert("배송지 등록이 완료되었습니다.");
            parseInt($("input[name=od_send_cost]").val(it_sc_cost));
            $(".sc_price").html(number_format(it_sc_cost));

            $(".total_price").html(number_format(total)); // 소계
            $("input[name=multi_addr_cnt]").val(multi_addr_cnt);

            $("input:checkbox[name^=multi_chk]").attr("checked", true);
            $("#multAdd").prop("type","hidden");
            $("#multiChk").val('1');

            calculate_order_price();
        }
    }

    function MultiDel(){

        var ChekSum = 0;
        var TrId = "";

        $("input[name=multi_chk]:checkbox").each(function(index, item) {

            if( $("input:checkbox[name=multi_chk]").eq(index).prop("checked") ){

                ChekSum++;

            }

        });

        if( ChekSum == 0 ){
            alert("삭제목록을 체크해주세요.");
            return;
        }

        jQuery('input:checkbox[name=multi_chk]:checked').parents("tr").remove();

        $("#multiChk").val('0');
    }

</script>


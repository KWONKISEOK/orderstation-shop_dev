<?php
include_once('./_common.php');

define("_MYUSER_", true);

//$od_pwd = get_encrypt_string($od_pwd);

if($sdate == '') {
	$sdate = date( "Y-m" ).'-01';
	$edate = date( "Y-m-d" );
}
$mb_name = $_POST['mb_name'];
$mb_id = $member['mb_id'];
// 회원인 경우
if ($is_member)
{
   $sql_common = " from tbl_shop_order a, tbl_shop_order_detail b
				 where a.od_id = b.od_id
				   and a.mb_id = '{$member['mb_id']}' 
				    and b.od_period_yn = 'Y'
					and substr(a.od_time, 1, 10) >= '$sdate'
					and substr(a.od_time, 1, 10) <= '$edate' {$service_type_and_query_a} "; 
}
else // 그렇지 않다면 로그인으로 가기
{
    goto_url(G5_BBS_URL.'/login.php?url='.urlencode(G5_SHOP_URL.'/orderinquiry.php'));
}

//if($mb_name) $sql_common .= " and mb_name like '%".$mb_name."%'";

// 테이블의 전체 레코드수만 얻음
$sql = " select count(*) as cnt " . $sql_common;
$row = sql_fetch($sql);
$total_count = $row['cnt'];

$rows = 10;//$config['cf_page_rows'];
$total_page  = ceil($total_count / $rows);  // 전체 페이지 계산
if ($page < 1) { $page = 1; } // 페이지가 없으면 첫 페이지 (1 페이지)
$from_record = ($page - 1) * $rows; // 시작 열을 구함

$g5['title'] = '고객관리';
include_once('./_head.php');




?>

<!-- 주문 내역 시작 { -->
<div id="sod_v">
    <p id="sod_v_info">정기주문 신청하신 내역을 조회하실 수 있습니다.</p>

    <?php
    $limit = " limit $from_record, $rows ";
?>

<form name="frm1" method="post" action="/shop/myauto.php" style="margin:0">	
	
	<div id="search-box">
		<span class="btndate" id="today">오늘</span>&nbsp;
		<span class="btndate" id="bday15">15일</span>&nbsp;
		<span class="btndate" id="bmonth1">1개월</span>&nbsp;
		<span class="btndate" id="bmonth3">3개월</span>&nbsp;&nbsp;
		<br/><br/>
          <input type="text" name="sdate" id="sdate" style="width:90px; text-align:center" class="datepickerbutton" value="<?php echo $sdate;?>" readonly>
          ~
          <input type="text" name="edate" id="edate" style="width:90px; text-align:center" class="datepickerbutton" value="<?php echo $edate;?>" readonly>
          <button class="btn03">조회</button>
	</div>

</form>

<style>
.btn03 {
    display: inline-block;
    padding: 5px;
    border: 1px solid #3b3c3f;
    background: #4b545e;
    color: #fff;
    text-decoration: none;
    vertical-align: middle;
}

.mypage .li_price_box {
    margin: 6px 6px;
    border-top: 1px solid #dcdcdc;
    padding: 10px 0 0;
	height:50px;
}
.mypage .prqty_price {
    border-right: 1px solid #dcdcdc;
	
}
.mypage .li_prqty_sp {
    float: left;
    width: 50%;
    display: block;
    line-height: 20px;
    padding: 0 7px;
    text-align: right;
}
.mypage .li_prqty_sp span {
    float: left;
}
</style>

<link rel="stylesheet" href="//code.jquery.com/ui/1.8.18/themes/base/jquery-ui.css" />
<script src="//code.jquery.com/ui/1.8.18/jquery-ui.min.js"></script>


<script>
    $.datepicker.setDefaults({
        dateFormat: 'yy-mm-dd',
        prevText: '이전 달',
        nextText: '다음 달',
        monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
        monthNamesShort: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
        dayNames: ['일', '월', '화', '수', '목', '금', '토'],
        dayNamesShort: ['일', '월', '화', '수', '목', '금', '토'],
        dayNamesMin: ['일', '월', '화', '수', '목', '금', '토'],
        showMonthAfterYear: true,
        yearSuffix: '년'
    });

    $(function() {
        $(".datepickerbutton").datepicker();
    });

	function setDate(kind) {
		var today = '<?php echo date("Y-m-d"); ?>';
		var bday15 = '<?php echo date("Y-m-d", strtotime('-15 days')); ?>';
		var bmonth1 = '<?php echo date("Y-m-d", strtotime('-1 month')); ?>';
		var bmonth3 = '<?php echo date("Y-m-d", strtotime('-3 month')); ?>';

		if(kind==1) {
			$("#sdate").val(today);
			$("#edate").val(today);
		}
		if(kind==2) {
			$("#sdate").val(bday15);
			$("#edate").val(today);
		}
		if(kind==3) {
			$("#sdate").val(bmonth1);
			$("#edate").val(today);
		}
		if(kind==4) {
			$("#sdate").val(bmonth3);
			$("#edate").val(today);
		}

	}
	$( "#today" ).click(function() {
	  setDate(1);
	});
	$( "#bday15" ).click(function() {
	  setDate(2);
	});
	$( "#bmonth1" ).click(function() {
	  setDate(3);
	});
	$( "#bmonth3" ).click(function() {
	  setDate(4);
	});

</script>

<div id="sod_inquiry">
    <ul class="mypage">
	<?php
    $sql = " select a.od_b_name
 	               , a.od_b_addr1
 	               , a.od_b_addr2
 	               , a.od_b_addr3
                   ,b.it_name 
				   ,b.od_option
				   ,b.od_period_week
				   ,b.od_period_cnt
				   ,a.od_time
				   ,a.od_id
				   ,b.od_num
				   ,(select count(*) from tbl_shop_order_receiver where od_id = b.od_id and od_num = b.od_num) od_cnt
				   ,(select count(*) from tbl_shop_order_receiver where od_id = b.od_id and od_num = b.od_num and od_pay_yn='Y') od_pay_cnt			  
			  $sql_common
              order by a.od_id desc $limit";

    $result = sql_query($sql);
    for ($i=0; $row=sql_fetch_array($result); $i++)
    {
		$od_id = $row['od_id'];
		switch($row['od_period_week']) {
            case '1':
                $period_week = '매주';
                break;
            case '2':
                $period_week = '2주마다';
                break;
            case '3':
                $period_week = '3주마다';
                break;
            case '4':
                $period_week = '4주마다';
                break;
            case '5':
                $period_week = '지정일마다';
                break;
           
        }
		if($row['od_cnt'] > $row['od_pay_cnt'] ) {
			$auto_status = '진행중';
		} else {
			$auto_status = '완료';
		}
        
	?>
 
    <li>
		<table style="margin:10px;">

		<tr>
			<td width="30%">상품명</td>
			<td><?php echo $row['it_name']; ?><br/>
			<?php if($row['it_name'] != $row['od_option'])
			{
                echo $row['od_option'];
			} ?>
            </td>
		</tr>
        </table>
        <table style="margin:10px;">
        <tr>
            <td width="30%">배송지</td>
            <td><?php
                echo $row['od_b_name']; ?><br/>
                <?php echo $row['od_b_addr1'];?> <br>
                <?php echo $row['od_b_addr2']; echo $row['od_b_addr3'];?></td>
        </tr>
        </table>


		<div class="li_price_box">
		   <span class="prqty_price li_prqty_sp"><span>상태 </span><?php echo$auto_status; ?></span>
			<span class="li_prqty_sp"><span>결제주기 </span><?php echo $period_week; ?></span>
			<span class="prqty_price li_prqty_sp"><span>완료결제 </span><?php echo $row['od_pay_cnt']; ?></span>
			<span class="li_prqty_sp"><span>남은결제 </span><?php echo $row['od_cnt'] - $row['od_pay_cnt']; ?></span>
		</div>

		<table style="margin:10px;">
		<tr><td colspan="2">
		<?php
			$sql2  = " select * from tbl_shop_order_receiver where od_id= ". $row['od_id']." and od_num= ". $row['od_num'];
			$result2 = sql_query($sql2);
			for ($j=0; $row2=sql_fetch_array($result2); $j++) { ?>
		
			<?php echo $row2['od_seq']; ?>회&nbsp;&nbsp;
			<?php echo $row2['od_period_date']; ?>&nbsp;&nbsp;
			<?php echo $row2['od_status']; ?>
			<br/>

			<?php } ?>

		</td></tr>
		</table>

    </li>
	<?php } ?>
  
   </ul>

</div>
<!-- } 주문 내역 목록 끝 -->

    <?php echo get_paging($config['cf_write_pages'], $page, $total_page, "{$_SERVER['SCRIPT_NAME']}?$qstr&amp;page="); ?>
</div>
<!-- } 주문 내역 끝 -->

<?php
include_once('./_tail.php');
?>

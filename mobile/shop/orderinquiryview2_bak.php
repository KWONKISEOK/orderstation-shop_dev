<?php
include_once('./_common.php');

// 테마에 orderinquiryview.php 있으면 include
if(defined('G5_THEME_SHOP_PATH')) {
    $theme_inquiryview_file = G5_THEME_MSHOP_PATH.'/orderinquiryview.php';
    if(is_file($theme_inquiryview_file)) {
        include_once($theme_inquiryview_file);
        return;
        unset($theme_inquiryview_file);
    }
}

$g5['title'] = '주문상세내역';
include_once(G5_MSHOP_PATH.'/_head.php');

// LG 현금영수증 JS
if($od['od_pg'] == 'lg') {
    if($default['de_card_test']) {
        echo '<script language="JavaScript" src="http://pgweb.uplus.co.kr:7085/WEB_SERVER/js/receipt_link.js"></script>'.PHP_EOL;
    } else {
        echo '<script language="JavaScript" src="http://pgweb.uplus.co.kr/WEB_SERVER/js/receipt_link.js"></script>'.PHP_EOL;
    }
}

$sql = "select * from {$g5['g5_shop_order_table']} where od_id = '$od_id' ";
$od = sql_fetch($sql);

$sql = "select sum(od_total_drug_price) as od_total_drug_price from {$g5['g5_shop_order_detail_table']} where od_id = '$od_id' {$service_type_and_query} ";
$od_dt = sql_fetch($sql);

?>

    <div id="sod_fin">

        <div id="sod_fin_no">주문번호 <strong><?php echo $od_id; ?></strong></div>

        <section class="sod_fin_list">
            <h2>주문하신 상품</h2>

            <?php
            $st_count1 = $st_count2 = 0;
            $custom_cancel = false;

            $sql = " select it_id, it_name, od_send_cost, 0 it_sc_type
                    from {$g5['g5_shop_order_detail_table']}
                    where od_id = '$od_id' {$service_type_and_query}
                    group by it_id
                    order by od_id ";
            $result = sql_query($sql);
            ?>
            <ul id="sod_list_inq" class="sod_list">
                <?php

                //완료가 아닌갯수 구하기
                $order_receiver_cnt = 0;
                $return_possible = ""; //2차옵션 상품일경우 반품을 막기위한 조치

                for($i=0; $row=sql_fetch_array($result); $i++) {
                    $image_width = 80;
                    $image_height = 80;
                    $image = get_it_image($row['it_id'], 80, 80, '', '', $row['it_name']);

                    $sql = " select a.od_id, a.it_name, a.od_option, a.od_qty qty, b.od_qty, a.od_price, od_total_sale_price, od_total_incen, b.od_status, b.od_period_date, a.od_period_cnt, a.od_send_cost,
				                b.od_delivery_company,b.od_invoice, a.od_num  , b.od_bill_tno , a.io_id, b.od_pay_yn
                            from {$g5['g5_shop_order_detail_table']} a, tbl_shop_order_receiver b
                            where a.od_id = b.od_id
							  and a.od_num = b.od_num
							  and a.od_id = '$od_id'
                              and it_id = '{$row['it_id']}' {$service_type_and_query_a} ";


                    $res = sql_query($sql);
                    $rowspan = sql_num_rows($res) + 1;

                    $ReceiptHtml = "";

                    ?>
                    <li class="sod_li">
                        <div class="li_name">
                            <a href="./item.php?it_id=<?php echo $row['it_id']; ?>"><strong><?php echo $row['it_name']; ?></strong></a>
                            <td headers="th_itreview" class="td_mngsmall" style="width:70px;">
                                <a href="javascript:ModalPop('./itemuseform.php?it_id=<?php echo $row['it_id'];?>&od_id=<?php echo $od_id; ?>','','N');" style="background-color:#3fb1b7; border-radius:4px; padding:5px;padding-top:5px;padding-bottom:5px; color:#FFFFFF" >후기쓰기</a>
                            </td>
                        </div>
                        <?php
                        for($k=0; $opt=sql_fetch_array($res); $k++) {

                            //2차옵션 상품이 끼어있으면 반품신청을 못한다.
                            if( strpos( $opt['io_id'] , chr(30) ) !== false ){
                                $return_possible = "false";
                            }

                            if( !empty($opt['od_bill_tno']) ) {
                                $receipt_script = 'window.open(\'https://iniweb.inicis.com/DefaultWebApp/mall/cr/cm/mCmReceipt_head.jsp?noTid='.$opt['od_bill_tno'].'&noMethod=1\',\'receipt\',\'width=430,height=700\');';
                                $link_script = "&nbsp;<a href=\"javascript:;\" onclick=".$receipt_script.">[영수증 출력]</a>";
                                $ReceiptHtml .= "<tr><td>".get_text($opt['od_option'])."-".get_text($opt['od_period_date'])."&nbsp;".$link_script."</td></tr>";
                            }

                            $od_invoice = '';

                            if($opt['od_delivery_company'] && $opt['od_invoice'])
                                $od_invoice = $opt['od_delivery_company'].' '.get_delivery_inquiry($opt['od_delivery_company'], $opt['od_invoice'], 'dvr_link');
                            //$od_invoice = '<i class="fa fa-truck" aria-hidden="true"></i> '.get_text($opt['od_delivery_company']).' '.get_text($opt['od_invoice']);

                            ?>
                            <div class="li_op_wr">
                                <div class="li_opt">&nbsp;
                                    <?
                                    if( $opt['od_status'] == "완료" || $opt['od_status'] == "반품반려" ){
                                        $order_receiver_cnt++;
                                        ?>
                                        <input type="checkbox" name="return_od_num[]" value="<?=$opt['od_num']?>">
                                    <? } ?>
                                    <?php echo get_text($opt['od_option']); ?> - <?php echo get_text($opt['od_period_date']); ?>
                                </div>
                                <a href="./item.php?it_id=<?php echo $row['it_id']; ?>" class="total_img"><?php echo $image; ?></a>
                                <span class="prqty_stat">
                                    <span class="sound_only">상태</span>
                                    <?php
                                    if ($opt['od_status'] == "주문" && $opt['od_pay_yn'] == "Y") {
                                        echo '입금';
                                    } else {
                                        echo $opt['od_status'];
                                    }
                                    ?>
                                </span>


                            </div>
                            <div class="li_prqty">
                                <span class="prqty_price li_prqty_sp"><span>소비자가 </span><?php
                                    if($opt[od_period_cnt] > 0) {
                                        $subprice= $opt['od_total_sale_price'] / $opt[od_period_cnt];
                                        $subincen= $opt['od_total_incen'] / $opt[od_period_cnt];
                                    } else {
                                        $subprice=$opt['od_total_sale_price'];
                                        $subincen= $opt['od_total_incen'] ;
                                    }
                                    echo number_format($subprice);

                                    ?></span>
                                <span class="prqty_qty li_prqty_sp"><span>수량 </span><?php echo number_format($opt['od_qty']); ?></span>
                                <span class="prqty_sc li_prqty_sp"><span>배송비 </span><?php echo number_format($opt['od_send_cost']); ?></span>
                                <span class="total_point li_prqty_sp"><span>적립포인트 </span><?php
                                    if($member['mb_type']==1) {
                                        echo number_format($subincen);
                                    }
                                    ?></span>

                            </div>
                            <div class="li_total">
                                <span class="total_price "><?php echo $od_invoice; ?></span>
                            </div>
                            <?php
                            $tot_point       += $point;

                            $st_count1++;
                            if($opt['od_status'] == '주문'|| (($opt['od_status'] == '입금') && ($od['od_settle_case'] == '신용카드')))
                                $st_count2++;
                        }
                        ?>

                    </li>
                    <?php
                }

                // 주문 상품의 상태가 모두 주문이면 고객 취소 가능
                if($st_count1 > 0 && $st_count1 == $st_count2)
                    $custom_cancel = true;
                ?>
            </ul>

            <div id="sod_sts_wrap">
                <div style="margin-bottom:10px;">
                    <button type="button" id="btn_frmline_1" class="btn_frmline_1" onclick="javascript:location.href='/shop/orderinquiry.php';">주문내역확인</button>&nbsp;
                    <button type="button" id="btn_frmline_2" class="btn_frmline_2" onclick="javascript:location.href='/';">메인으로가기</button>
                </div>
                <span class="sound_only">상품 상태 설명</span>
                <div class="btn_wr">
                    <button type="button" id="sod_sts_explan_open">상태설명보기</button>
                    <? if( $od['od_status'] == "반품반려" ){ ?>
                        <!--<button type="button" id="return_explan_open">반품반려 이유보기</button>-->
                    <? } ?>
                </div>
                <div id="sod_sts_explan">
                    <dl id="sod_fin_legend">
                        <dt>주문</dt>
                        <dd>주문이 접수되었습니다.</dd>
                        <dt>입금</dt>
                        <dd>입금(결제)이 완료 되었습니다.</dd>
                        <dt>준비</dt>
                        <dd>상품 준비 중입니다.</dd>
                        <dt>배송</dt>
                        <dd>상품 배송 중입니다.</dd>
                        <dt>완료</dt>
                        <dd>상품 배송이 완료 되었습니다.</dd>
                        <dt>반품접수</dt>
                        <dd>상품 반품접수가 완료 되었습니다.
                        <dt>반품반려</dt>
                        <dd>상품 반품접수가 반려 되었습니다.
                        <dt>반품완료</dt>
                        <dd>상품 반품이 완료처리 되었습니다.
                    </dl>
                </div>
                <div id="return_explan" style="display:none;">
                    <dl id="sod_fin_legend">
                        <dd><?=$od['od_return_admin_memo']?></dd>
                    </dl>
                </div>

            </div>

            <?php
            // 총계 = 주문상품금액합계 + 배송비 - 상품할인 - 결제할인 - 배송비할인
            $tot_price = $od['od_cart_price'] + $od['od_send_cost'] + $od['od_send_cost2']
                - $od['od_cart_coupon'] - $od['od_coupon'] - $od['od_send_coupon']
                - $od['od_cancel_price'];
            $tot_drug_price = $od_dt['od_total_drug_price'] + $od['od_send_cost'] + $od['od_send_cost2']
                - $od['od_cart_coupon'] - $od['od_coupon'] - $od['od_send_coupon']
                - $od['od_cancel_price'];

            ?>
            <div class="sod_ta_wr">
                <dl id="m_sod_bsk_tot">
                    <dt class="sod_bsk_dvr">주문총액</dt>
                    <dd class="sod_bsk_dvr"><strong><?php echo number_format($od['od_cart_price']); ?> 원</strong></dd>

                    <?php if($od['od_cart_coupon'] > 0) { ?>
                        <dt class="sod_bsk_dvr">상품할인</dt>
                        <dd class="sod_bsk_dvr"><strong><?php echo number_format($od['od_cart_coupon']); ?> 원</strong></dd>
                    <?php } ?>

                    <?php if($od['od_coupon'] > 0) { ?>
                        <dt class="sod_bsk_dvr">결제할인</dt>
                        <dd class="sod_bsk_dvr"><strong><?php echo number_format($od['od_coupon']); ?> 원</strong></dd>
                    <?php } ?>

                    <?php if ($od['od_send_cost'] > 0) { ?>
                        <dt class="sod_bsk_dvr">배송비</dt>
                        <dd class="sod_bsk_dvr"><strong><?php echo number_format($od['od_send_cost']); ?> 원</strong></dd>
                    <?php } ?>

                    <?php if($od['od_send_coupon'] > 0) { ?>
                        <dt class="sod_bsk_dvr">배송비할인</dt>
                        <dd class="sod_bsk_dvr"><strong><?php echo number_format($od['od_send_coupon']); ?> 원</strong></dd>
                    <?php } ?>

                    <?php if ($od['od_send_cost2'] > 0) { ?>
                        <dt class="sod_bsk_dvr">추가배송비</dt>
                        <dd class="sod_bsk_dvr"><strong><?php echo number_format($od['od_send_cost2']); ?> 원</strong></dd>
                    <?php } ?>

                    <?php if ($od['od_cancel_price'] > 0) { ?>
                        <dt class="sod_bsk_dvr">취소금액</dt>
                        <dd class="sod_bsk_dvr"><strong><?php echo number_format($od['od_cancel_price']); ?> 원</strong></dd>
                    <?php } ?>

                    <dt class="sod_bsk_point">적립포인트</dt>
                    <dd class="sod_bsk_point"><strong><?php echo number_format($tot_point); ?> 점</strong></dd>

                    <dt class="sod_bsk_cnt">총계</dt>
                    <dd class="sod_bsk_cnt"><strong><?php echo number_format($tot_price); ?> 원</strong></dd>
                    <?php if($member['mb_type']=='1' && $od['od_cancel_price'] == 0) {?>
                        <dd class="sod_bsk_point" style="float: right;">
                            <strong><span style="color: #ff006c;">(약국) </span><?php echo number_format($tot_drug_price); ?> 원</strong>
                        </dd>
                    <?php } ?>

                </dl>
            </div>
        </section>

        <div id="sod_fin_view">
            <h2>결제/배송 정보</h2>
            <?php
            $receipt_price = $od['od_receipt_price']
                + $od['od_receipt_point'];
            $cancel_price = $od['od_cancel_price'];

            $misu = true;
            $misu_price = $tot_price - $receipt_price - $cancel_price;

            if ($misu_price == 0 && ($od['od_cart_price'] > $od['od_cancel_price'])) {
                $wanbul = " (완불)";
                $misu = false; // 미수금 없음
            }
            else
            {
                $wanbul = display_price($receipt_price);
            }

            // 결제정보처리
            if($od['od_receipt_price'] > 0)
                $od_receipt_price = display_price($od['od_receipt_price']);
            else
                $od_receipt_price = '아직 입금되지 않았거나 입금정보를 입력하지 못하였습니다.';

            $app_no_subj = '';
            $disp_bank = true;
            $disp_receipt = false;
            $easy_pay_name = '';
            if($od['od_settle_case'] == '신용카드' || $od['od_settle_case'] == 'KAKAOPAY' || is_inicis_order_pay($od['od_settle_case']) ) {
                $app_no_subj = '승인번호';
                $app_no = $od['od_app_no'];
                $disp_bank = false;
                $disp_receipt = true;
            } else if($od['od_settle_case'] == '간편결제') {
                $app_no_subj = '승인번호';
                $app_no = $od['od_app_no'];
                $disp_bank = false;
                switch($od['od_pg']) {
                    case 'lg':
                        $easy_pay_name = 'PAYNOW';
                        break;
                    case 'inicis':
                        $easy_pay_name = 'KPAY';
                        break;
                    case 'kcp':
                        $easy_pay_name = 'PAYCO';
                        break;
                    default:
                        break;
                }
            } else if($od['od_settle_case'] == '휴대폰') {
                $app_no_subj = '휴대폰번호';
                $app_no = $od['od_bank_account'];
                $disp_bank = false;
                $disp_receipt = true;
            } else if($od['od_settle_case'] == '가상계좌' || $od['od_settle_case'] == '계좌이체') {
                $app_no_subj = '거래번호';
                $app_no = $od['od_tno'];
            }
            ?>

            <section id="sod_fin_pay">
                <h3>결제정보</h3>

                <div  class="odf_tbl">
                    <table>
                        <colgroup>
                            <col class="grid_2">
                            <col>
                        </colgroup>
                        <tbody>
                        <tr>
                            <th scope="row">주문번호</th>
                            <td><?php echo $od_id; ?></td>
                        </tr>
                        <tr>
                            <th scope="row">주문일시</th>
                            <td><?php echo $od['od_time']; ?></td>
                        </tr>
                        <?php if($member['mb_type'] != 1) { ?>
                            <tr>
                                <th scope="row">결제방식</th>
                                <td><?php echo ($easy_pay_name ? $easy_pay_name.'('.$od['od_settle_case'].')' : check_pay_name_replace($od['od_settle_case'])); ?></td>
                            </tr>
                            <tr>
                                <th scope="row">결제금액</th>
                                <td><?php echo $od_receipt_price; ?></td>
                            </tr>
                        <?php } ?>
                        <?php
                        if($od['od_receipt_price'] > 0)
                        {
                            ?>
                            <tr>
                                <th scope="row">결제일시</th>
                                <td><?php echo $od['od_receipt_time']; ?></td>
                            </tr>
                            <?php
                        }

                        // 승인번호, 휴대폰번호, 거래번호
                        if($app_no_subj)
                        {
                            ?>
                            <tr>
                                <th scope="row"><?php echo $app_no_subj; ?></th>
                                <td><?php echo $app_no; ?></td>
                            </tr>
                            <?php
                        }

                        // 계좌정보
                        if($disp_bank)
                        {
                            ?>
                            <tr>
                                <th scope="row">입금자명</th>
                                <td><?php echo get_text($od['od_deposit_name']); ?></td>
                            </tr>
                            <tr>
                                <th scope="row">입금계좌</th>
                                <td><?php echo get_text($od['od_bank_account']); ?></td>
                            </tr>
                            <tr>
                                <th scope="row">입금기한</th>
                                <td><?php echo get_text($od['od_bank_date']); ?></td>
                            </tr>
                            <?php
                        }

                        if($disp_receipt) {
                            ?>
                            <tr>
                                <th scope="row">영수증</th>
                                <td>
                                    <?php
                                    if($od['od_settle_case'] == '휴대폰')
                                    {
                                        if($od['od_pg'] == 'lg') {
                                            require_once G5_SHOP_PATH.'/settle_lg.inc.php';
                                            $LGD_TID      = $od['od_tno'];
                                            $LGD_MERTKEY  = $config['cf_lg_mert_key'];
                                            $LGD_HASHDATA = md5($LGD_MID.$LGD_TID.$LGD_MERTKEY);

                                            $hp_receipt_script = 'showReceiptByTID(\''.$LGD_MID.'\', \''.$LGD_TID.'\', \''.$LGD_HASHDATA.'\');';
                                        } else if($od['od_pg'] == 'inicis') {
                                            $hp_receipt_script = 'window.open(\'https://iniweb.inicis.com/DefaultWebApp/mall/cr/cm/mCmReceipt_head.jsp?noTid='.$od['od_tno'].'&noMethod=1\',\'receipt\',\'width=430,height=700\');';
                                        } else {
                                            $hp_receipt_script = 'window.open(\''.G5_BILL_RECEIPT_URL.'mcash_bill&tno='.$od['od_tno'].'&order_no='.$od['od_id'].'&trade_mony='.$od['od_receipt_price'].'\', \'winreceipt\', \'width=500,height=690,scrollbars=yes,resizable=yes\');';
                                        }
                                        ?>
                                        <a href="javascript:;" onclick="<?php echo $hp_receipt_script; ?>">영수증 출력</a>
                                        <?php
                                    }

                                    if($od['od_settle_case'] == '신용카드' || is_inicis_order_pay($od['od_settle_case']) )
                                    {
                                        if($od['od_pg'] == 'lg') {
                                            require_once G5_SHOP_PATH.'/settle_lg.inc.php';
                                            $LGD_TID      = $od['od_tno'];
                                            $LGD_MERTKEY  = $config['cf_lg_mert_key'];
                                            $LGD_HASHDATA = md5($LGD_MID.$LGD_TID.$LGD_MERTKEY);

                                            $card_receipt_script = 'showReceiptByTID(\''.$LGD_MID.'\', \''.$LGD_TID.'\', \''.$LGD_HASHDATA.'\');';
                                        } else if($od['od_pg'] == 'inicis') {
                                            $card_receipt_script = 'window.open(\'https://iniweb.inicis.com/DefaultWebApp/mall/cr/cm/mCmReceipt_head.jsp?noTid='.$od['od_tno'].'&noMethod=1\',\'receipt\',\'width=430,height=700\');';
                                        } else {
                                            $card_receipt_script = 'window.open(\''.G5_BILL_RECEIPT_URL.'card_bill&tno='.$od['od_tno'].'&order_no='.$od['od_id'].'&trade_mony='.$od['od_receipt_price'].'\', \'winreceipt\', \'width=470,height=815,scrollbars=yes,resizable=yes\');';
                                        }
                                        ?>
                                        <? if( $ReceiptHtml == "" ){ ?>
                                        <a href="javascript:;" onclick="<?php echo $card_receipt_script; ?>">영수증 출력</a>
                                    <? }else{ ?>
                                        <table>
                                            <?=$ReceiptHtml?>
                                        </table>
                                    <? } ?>
                                        <?php
                                    }

                                    if($od['od_settle_case'] == 'KAKAOPAY')
                                    {
                                        $card_receipt_script = 'window.open(\'https://mms.cnspay.co.kr/trans/retrieveIssueLoader.do?TID='.$od['od_tno'].'&type=0\', \'popupIssue\', \'toolbar=no,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=yes,width=420,height=540\');';
                                        ?>
                                        <a href="javascript:;" onclick="<?php echo $card_receipt_script; ?>">영수증 출력</a>
                                        <?php
                                    }
                                    ?>
                                </td>
                            </tr>
                            <?php
                        }

                        if ($od['od_receipt_point'] > 0)
                        {
                            ?>
                            <tr>
                                <th scope="row">포인트사용</th>
                                <td><?php echo display_point($od['od_receipt_point']); ?></td>
                            </tr>

                            <?php
                        }

                        if ($od['od_refund_price'] > 0)
                        {
                            ?>
                            <tr>
                                <th scope="row">환불 금액</th>
                                <td><?php echo display_price($od['od_refund_price']); ?></td>
                            </tr>
                            <?php
                        }

                        // 현금영수증 발급을 사용하는 경우에만
                        if ($default['de_taxsave_use']) {
                            // 미수금이 없고 현금일 경우에만 현금영수증을 발급 할 수 있습니다.
                            if ($misu_price == 0 && $od['od_receipt_price'] && ($od['od_settle_case'] == '무통장' || $od['od_settle_case'] == '계좌이체' || $od['od_settle_case'] == '가상계좌')) {
                                ?>
                                <tr>
                                    <th scope="row">현금영수증</th>
                                    <td>
                                        <?php
                                        if ($od['od_cash'])
                                        {
                                            if($od['od_pg'] == 'lg') {
                                                require_once G5_SHOP_PATH.'/settle_lg.inc.php';

                                                switch($od['od_settle_case']) {
                                                    case '계좌이체':
                                                        $trade_type = 'BANK';
                                                        break;
                                                    case '가상계좌':
                                                        $trade_type = 'CAS';
                                                        break;
                                                    default:
                                                        $trade_type = 'CR';
                                                        break;
                                                }
                                                $cash_receipt_script = 'javascript:showCashReceipts(\''.$LGD_MID.'\',\''.$od['od_id'].'\',\''.$od['od_casseqno'].'\',\''.$trade_type.'\',\''.$CST_PLATFORM.'\');';
                                            } else if($od['od_pg'] == 'inicis') {
                                                $cash = unserialize($od['od_cash_info']);
                                                $cash_receipt_script = 'window.open(\'https://iniweb.inicis.com/DefaultWebApp/mall/cr/cm/Cash_mCmReceipt.jsp?noTid='.$cash['TID'].'&clpaymethod=22\',\'showreceipt\',\'width=380,height=540,scrollbars=no,resizable=no\');';
                                            } else {
                                                require_once G5_SHOP_PATH.'/settle_kcp.inc.php';

                                                $cash = unserialize($od['od_cash_info']);
                                                $cash_receipt_script = 'window.open(\''.G5_CASH_RECEIPT_URL.$default['de_kcp_mid'].'&orderid='.$od_id.'&bill_yn=Y&authno='.$cash['receipt_no'].'\', \'taxsave_receipt\', \'width=360,height=647,scrollbars=0,menus=0\');';
                                            }
                                            ?>
                                            <a href="javascript:;" onclick="<?php echo $cash_receipt_script; ?>">현금영수증 확인하기</a>
                                            <?php
                                        }
                                        else
                                        {
                                            ?>
                                            <a href="javascript:;" onclick="window.open('<?php echo G5_SHOP_URL; ?>/taxsave.php?od_id=<?php echo $od_id; ?>', 'taxsave', 'width=550,height=400,scrollbars=1,menus=0');">현금영수증을 발급하시려면 클릭하십시오.</a>
                                        <?php } ?>
                                    </td>
                                </tr>
                                <?php
                            }
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </section>

            <section id="sod_fin_orderer">
                <h3>주문하신 분</h3>

                <div  class="odf_tbl">
                    <table>
                        <colgroup>
                            <col class="grid_2">
                            <col>
                        </colgroup>
                        <tbody>
                        <tr>
                            <th scope="row">이 름</th>
                            <td><?php echo get_text($od['od_name']); ?></td>
                        </tr>
                        <tr style="display:none;">
                            <th scope="row">전화번호</th>
                            <td><?php echo get_text($od['od_tel']); ?></td>
                        </tr>
                        <tr>
                            <th scope="row">핸드폰</th>
                            <td><?php echo get_text($od['od_hp']); ?></td>
                        </tr>
                        <tr style="display:none;">
                            <th scope="row">주 소</th>
                            <td><?php echo get_text(sprintf("(%s%s)", $od['od_zip1'], $od['od_zip2']).' '.print_address($od['od_addr1'], $od['od_addr2'], $od['od_addr3'], $od['od_addr_jibeon'])); ?></td>
                        </tr>
                        <tr style="display:none;">
                            <th scope="row">E-mail</th>
                            <td><?php echo get_text($od['od_email']); ?></td>
                        </tr>
                        </tbody>
                    </table>
                </div>

            </section>

            <?
            //다중배송이 아닐때.
            if( $od['od_multi_yn'] != "Y" ){ ?>

                <section id="sod_fin_receiver">
                    <h3>받으시는 분</h3>

                    <div  class="odf_tbl">
                        <table>
                            <colgroup>
                                <col class="grid_2">
                                <col>
                            </colgroup>
                            <tbody>
                            <tr>
                                <th scope="row">이 름</th>
                                <td><?php echo get_text($od['od_b_name']); ?></td>
                            </tr>
                            <tr>
                                <th scope="row">전화번호</th>
                                <td><?php echo get_text($od['od_b_tel']); ?></td>
                            </tr>
                            <tr>
                                <th scope="row">핸드폰</th>
                                <td><?php echo get_text($od['od_b_hp']); ?></td>
                            </tr>
                            <tr>
                                <th scope="row">주 소</th>
                                <td><?php echo get_text(sprintf("(%s%s)", $od['od_b_zip1'], $od['od_b_zip2']).' '.print_address($od['od_b_addr1'], $od['od_b_addr2'], $od['od_b_addr3'], $od['od_b_addr_jibeon'])); ?></td>
                            </tr>
                            <?php
                            // 희망배송일을 사용한다면
                            if ($default['de_hope_date_use'])
                            {
                                ?>
                                <tr>
                                    <th scope="row">희망배송일</th>
                                    <td><?php echo substr($od['od_hope_date'],0,10).' ('.get_yoil($od['od_hope_date']).')' ;?></td>
                                </tr>
                            <?php }
                            if ($od['od_memo'])
                            {
                                ?>
                                <tr style="display:none;">
                                    <th scope="row">전하실 말씀</th>
                                    <td><?php echo conv_content($od['od_memo'], 0); ?></td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </section>

                <!-- <section id="sod_fin_dvr">
            <h3>배송정보</h3>

            <div  class="odf_tbl">
                <table>
                <colgroup>
                    <col class="grid_2">
                    <col>
                </colgroup>
                <tbody>
                <?php
                if ($od['od_invoice'] && $od['od_delivery_company'])
                {
                    ?>
                <tr>
                    <th scope="row">배송회사</th>
                    <td><?php echo $od['od_delivery_company']; ?> <?php echo get_delivery_inquiry($od['od_delivery_company'], $od['od_invoice'], 'dvr_link'); ?></td>
                </tr>
                <tr>
                    <th scope="row">운송장번호</th>
                    <td><?php echo $od['od_invoice']; ?></td>
                </tr>
                <tr>
                    <th scope="row">배송일시</th>
                    <td><?php echo $od['od_invoice_time']; ?></td>
                </tr>
                <?php
                }
                else
                {
                    ?>
                <tr>
                    <td class="empty_table" colspan="2">아직 배송하지 않았거나 배송정보를 입력하지 못하였습니다.</td>
                </tr>
                <?php
                }
                ?>
                </tbody>
                </table>
            </div>
        </section> -->

            <? } ?>

            <?
            //다중배송일때.
            if( $od['od_multi_yn'] == "Y" ){

                $sql = " select a.comp_code, b.od_date3, a.od_send_yn , b.od_status , c.*
                            from tbl_shop_order_detail a, tbl_shop_order_receiver b, tbl_shop_order_multi_receiver c
                            where a.od_id = b.od_id and a.od_num= b.od_num and a.od_id = c.od_id and a.od_num= c.od_num and a.od_id = '$od_id' ";

                ?>

                <section id="sod_fin_receiver">
                    <h3>받으시는 분</h3>

                    <div class="tbl_head01 tbl_wrap">
                        <table border="1">
                            <tr>
                                <td scope="col" style="width:30px;" align="center">성명</td>
                                <td scope="col" style="width:40px;" align="center">HP</td>
                                <td scope="col" style="width:50px;" align="center">주소</td>
                                <td scope="col" style="width:50px;" align="center">배송회사</td>
                                <td scope="col" style="width:50px;" align="center">운송장번호</td>
                            </tr>
                            <tbody>
                            <?
                            $res = sql_query($sql);
                            for($k=0; $rs=sql_fetch_array($res); $k++) {
                                ?>
                                <tr align="center">
                                    <td><?=$rs["od_b_name"]?></td>
                                    <td><?=$rs["od_b_hp"]?></td>
                                    <td>
                                        <?=$rs["od_b_addr1"]?><br>
                                        <?=$rs["od_b_addr2"]?><br>
                                        <?=$rs["od_b_addr3"]?>
                                    </td>
                                    <td><?=$rs["od_delivery_company"]?></td>
                                    <td><?=$rs["od_invoice"]?></td>
                                </tr>
                                <?
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                    <div>
                        <center>
                            <button type="button" id="btn_frmline_1" class="btn_frmline_1" onclick="javascript:location.href='/shop/orderinquiry.php';">주문내역확인</button>&nbsp;
                            <button type="button" id="btn_frmline_3" class="btn_frmline_3" onclick="javascript:location.href='/';">메인으로가기</button>
                        </center>
                    </div>
                </section>

            <? } ?>

        </div>

        <section id="sod_fin_tot">
            <h2>결제합계</h2>

            <ul>
                <li>
                    총 구매액
                    <strong><?php echo display_price($tot_price); ?></strong>
                </li>
                <?php
                if ($misu_price > 0) {
                    echo '<li>';
                    echo '미결제액'.PHP_EOL;
                    echo '<strong>'.display_price($misu_price).'</strong>';
                    echo '</li>';
                }
                ?>
                <li id="alrdy">
                    결제액
                    <strong><?php echo $wanbul; ?></strong>
                    <?php if( $od['od_receipt_point'] ){    //포인트로 결제한 내용이 있으면 ?>
                        <div class="right">
                            <p><span class="title"><i class="fa fa-angle-right" aria-hidden="true"></i> 포인트 결제</span><?php echo number_format($od['od_receipt_point']); ?> 점</p>
                            <p><span class="title"><i class="fa fa-angle-right" aria-hidden="true"></i> 실결제</span><?php echo number_format($od['od_receipt_price']); ?> 원</p>
                        </div>
                    <?php } ?>
                </li>
            </ul>
        </section>

        <?

        //배송완료인 상태는 반품이 가능하게끔 처리해주기
        if( $order_receiver_cnt > 0 ){

            $custom_cancel = true;
            $button_name = "반품 신청하기";
            $submit_url = G5_SHOP_URL."/orderinquiryreturn.php";
            $txt_1 = "주문반품";
            $txt_2 = "반품사유";
            $txt_3 = "<option value='기타' data-pay_yn='Y'>기타</option>";
            $txt_4 = "";

        }else{

            $return_possible = "";
            $button_name = "주문 취소하기";
            $submit_url = G5_SHOP_URL."/orderinquirycancel.php";
            $txt_1 = "주문취소";
            $txt_2 = "취소사유";
            $txt_3 = "";
            $txt_4 = " display:none; ";

        }


        ?>

        <? if( $od['od_status'] != "반품접수" && $od['od_status'] != "반품완료" ){ ?>
            <section id="sod_fin_cancel">
                <h2><?=$txt_1?></h2>
                <?php
                // 취소한 내역이 없다면
                if ($cancel_price == 0) {
                    if ($custom_cancel) {
                        ?>

                        <? if( $return_possible == "" ){ ?>
                            <span>
                                <button type="button" onclick="document.getElementById('sod_fin_cancelfrm').style.display='block';"><?=$button_name?></button>
                            </span>
                        <? } ?>

                        <div style="margin-top:10px;">
                            <center>
                                <button type="button" id="btn_frmline_1" class="btn_frmline_1" onclick="javascript:location.href='/shop/orderinquiry.php';">주문내역확인</button>&nbsp;
                                <button type="button" id="btn_frmline_3" class="btn_frmline_3" onclick="javascript:location.href='/';">메인으로가기</button>
                            </center>
                        </div>
                        <div id="sod_fin_cancelfrm">
                            <form method="post" action="<?=$submit_url?>" onsubmit="return fcancel_check(this);">
                                <input type="hidden" name="od_id"  value="<?php echo $od['od_id']; ?>">
                                <input type="hidden" name="token"  value="<?php echo $token; ?>">
                                <input type="hidden" name="return_od_num_check" id="return_od_num_check" value=""/>

                                <label for="cancel_memo" class="sound_only"><?=$txt_2?></label>
                                <!-- <input type="text" name="cancel_memo" id="cancel_memo" required class="frm_input required" maxlength="100" placeholder="취소사유"> -->
                                <select name="cancel_memo" id="cancel_memo" class="frm_input" required>
                                    <option value="">--<?=$txt_2?>--</option>
                                    <option value="필요없어짐(단순변심)" data-pay_yn="Y">필요없어짐(단순변심)</option>
                                    <option value="색상/사이즈가 기대와 다름" data-pay_yn="Y">색상/사이즈가 기대와 다름</option>
                                    <option value="상품이 파손되어 배송됨" data-pay_yn="N">상품이 파손되어 배송됨</option>
                                    <option value="상품 결함/기능에 이상이 있음" data-pay_yn="N">상품 결함/기능에 이상이 있음</option>
                                    <option value="주문 상품과 다른 상품이 배송됨" data-pay_yn="N">주문 상품과 다른 상품이 배송됨</option>
                                    <option value="상품이 늦게 배송됨" data-pay_yn="Y">상품이 늦게 배송됨</option>
                                    <?=$txt_3?>
                                </select>
                                <span id="memo_space" style="display:none;">
                                    <input type="text" name="client_memo" id="client_memo" class="frm_input" size="40" maxlength="255" placeholder="반품기타사유">
                                </span>

                                <div id="return_text_info" style="border:1px solid #e7ebf1;padding:10px 10px 10px 10px;margin-bottom:5px;line-height:20px;width:auto;<?=$txt_4?>">
                                    <table>
                                        <tr>
                                            <td width="30"><input type="checkbox" name="return_check_yn" id="return_check_yn" value="Y"></td>
                                            <td align="left">단순 변심이나 개인적인 기호의 차이로 인하여 반품 및 교환 요청시, 배송료(왕복배송비)를 고객님께서 부담하셔야 합니다.</td>
                                        </tr>
                                    </table>
                                </div>
                                <div id="return_method_info" style="border:1px solid #e7ebf1;padding:10px 10px 10px 10px;margin-bottom:5px;line-height:20px;width:auto;<?=$txt_4?>">
                                    <table>
                                        <tr>
                                            <td width="30">&nbsp;</td>
                                            <td align="left"><strong>왕복 배송비 결제 방법 선택하기<strong></td>
                                        </tr>
                                        <tr height="70">
                                            <td width="30"><input type="radio" name="od_return_method" value="1"></td>
                                            <td align="left">계좌입금 : 국민은행<br>140101-01-008595 (주)오엔케이</td>
                                        </tr>
                                        <tr height="25">
                                            <td width="30"><input type="radio" name="od_return_method" value="2"></td>
                                            <td align="left">택배 박스에 동봉</td>
                                        </tr>
                                    </table>
                                </div>

                                <input type="submit" value="확인" class="btn_frmline">

                            </form>
                        </div>
                        <?php
                    }
                } else {
                    ?>
                    <p>주문 취소, 반품, 품절된 내역이 있습니다.</p>
                <?php } ?>
            </section>
        <? } ?>

        <?php if ($od['od_settle_case'] == '가상계좌' && $od['od_misu'] > 0 && $default['de_card_test'] && $is_admin && $od['od_pg'] == 'kcp') {
            preg_match("/\s{1}([^\s]+)\s?/", $od['od_bank_account'], $matchs);
            $deposit_no = trim($matchs[1]);
            ?>
            <p>관리자가 가상계좌 테스트를 한 경우에만 보입니다.</p>
            <div id="kcp_acc_test" class="odf_tbl">
                <form method="post" action="http://devadmin.kcp.co.kr/Modules/Noti/TEST_Vcnt_Noti_Proc.jsp" target="_blank">
                    <table>
                        <caption>모의입금처리</caption>
                        <colgroup>
                            <col class="grid_2">
                            <col>
                        </colgroup>
                        <tbody>
                        <tr>
                            <th scope="col"><label for="e_trade_no">KCP 거래번호</label></th>
                            <td><input type="text" name="e_trade_no" value="<?php echo $od['od_tno']; ?>"></td>
                        </tr>
                        <tr>
                            <th scope="col"><label for="deposit_no">입금계좌</label></th>
                            <td><input type="text" name="deposit_no" value="<?php echo $deposit_no; ?>"></td>
                        </tr>
                        <tr>
                            <th scope="col"><label for="req_name">입금자명</label></th>
                            <td><input type="text" name="req_name" value="<?php echo $od['od_deposit_name']; ?>"></td>
                        </tr>
                        <tr>
                            <th scope="col"><label for="noti_url">입금통보 URL</label></th>
                            <td><input type="text" name="noti_url" value="<?php echo G5_SHOP_URL; ?>/settle_kcp_common.php"></td>
                        </tr>
                        </tbody>
                    </table>
                    <div id="sod_fin_test" class="btn_confirm">
                        <input type="submit" value="입금통보 테스트" class="btn_submit">
                    </div>
                </form>
            </div>
        <?php } ?>

    </div>

    <script>


        $(function() {
            $("#sod_sts_explan_open").on("click", function() {
                var $explan = $("#sod_sts_explan");
                if($explan.is(":animated"))
                    return false;

                if($explan.is(":visible")) {
                    $explan.slideUp(200);
                    $("#sod_sts_explan_open").text("상태설명보기");
                } else {
                    $explan.slideDown(200);
                    $("#sod_sts_explan_open").text("상태설명닫기");
                }
            });

            $("#sod_sts_explan_close").on("click", function() {
                var $explan = $("#sod_sts_explan");
                if($explan.is(":animated"))
                    return false;

                $explan.slideUp(200);
                $("#sod_sts_explan_open").text("상태설명보기");
            });

            $("#return_explan_open").on("click", function() {
                var $explan = $("#return_explan");
                if($explan.is(":animated"))
                    return false;

                if($explan.is(":visible")) {
                    $explan.slideUp(200);
                    $("#return_explan_open").text("반품반려 이유보기");
                } else {
                    $explan.slideDown(200);
                    $("#return_explan_open").text("반품반려 이유닫기");
                }
            });

            $("#return_explan_close").on("click", function() {
                var $explan = $("#return_explan");
                if($explan.is(":animated"))
                    return false;

                $explan.slideUp(200);
                $("#return_explan_open").text("반품반려 이유보기");
            });

            <? if( $order_receiver_cnt > 0 ){ ?>

            $("#cancel_memo").change(function(){

                var selected = $(this).find('option:selected');
                var extra = selected.data('pay_yn');

                if ( extra == "Y" ){
                    $("#return_text_info").css("display","");
                    $("#return_method_info").css("display","");
                }else{
                    $("#return_text_info").css("display","none");
                    $("#return_method_info").css("display","none");
                    $("input:checkbox[id='return_check_yn']").prop("checked", false);
                    $("input:radio[name='od_return_method']").prop("checked", false);
                }

                if( $(this).val() == "기타" ){
                    $("#memo_space").css("display","");
                }else{
                    $("#memo_space").css("display","none");
                    $("#client_memo").val("");
                }
            });

            <? } ?>

        });

        function fcancel_check(f)
        {
            <?if( $order_receiver_cnt > 0 ){?>

            var $ct_chk = $("input[name^=return_od_num]");
            var chked_cnt = $ct_chk.filter(":checked").size();
            if( chked_cnt == 0 ){
                alert("반품하실 상품을 체크해주세요.");
                return false;
            }
            //체크한 상품 문자열 연결해서 넘기기
            var chk = $("input[name^=return_od_num]:checked").map(function() {
                return this.value;
            }).get().join("|");

            $("#return_od_num_check").val(chk);

            var selected = $("#cancel_memo").find('option:selected');
            var pay_yn = selected.data('pay_yn');

            var Msg_1 = "주문을 정말 반품하시겠습니까?";
            var Msg_2 = "반품사유를 입력해 주십시오.";

            if( f.return_check_yn.checked == false && pay_yn == "Y" ){
                alert("배송료 부담 확인여부를 체크해주세요.");
                return false;
            }
            if( pay_yn == "Y" && !document.getElementsByName("od_return_method")[0].checked && !document.getElementsByName("od_return_method")[1].checked ){
                alert("왕복 배송비 결제 방법을 선택해주세요.");
                return false;
            }
            <?}else{?>
            var Msg_1 = "주문을 정말 취소하시겠습니까?";
            var Msg_2 = "취소사유를 입력해 주십시오.";
            <?}?>
            if(!confirm(Msg_1))
                return false;

            var memo = f.cancel_memo.value;
            if(memo == "") {
                alert(Msg_2);
                return false;
            }

            return true;
        }
    </script>

<?php
include_once(G5_MSHOP_PATH.'/_tail.php');
?>
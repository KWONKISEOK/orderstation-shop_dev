<?php
include_once('./_common.php');

$comp = preg_replace("/[\<\>\'\"\\\'\\\"\%\=\(\)\s]/", "", $_REQUEST['comp']);
/*
if ($comp == 276)      $g5['title'] = '네이처인';
else if ($comp == 334) $g5['title'] = '조인메디칼(주)';
else if ($comp == 347) $g5['title'] = '(주)유니텍산업';
else if ($comp == 284) $g5['title'] = '포프리';
else if ($comp == 338) $g5['title'] = '발란스코드';
else if ($comp == 361) $g5['title'] = '(주)선해수산';
else if ($comp == 287) $g5['title'] = '아임닭';
else if ($comp == 394) $g5['title'] = '(주)오트리푸드빌리지';
else
    alert('상품유형이 아닙니다.');
*/

$sql3 = " select comp_name from {$g5['member_table']} where mb_type = '7' and comp_code = '{$comp}' limit 1 ";
$row3 = sql_fetch($sql3);

if( $row3["comp_name"] ){
	$g5['title'] = $row3["comp_name"];
}

if( empty($comp) ){
	alert('필수값이 없습니다.');
	exit;
}

include_once(G5_MSHOP_PATH.'/_head.php');

// 한페이지에 출력하는 이미지수 = $list_mod * $list_row
// 모바일에서는 계산된 이미지수가 중요함
$list_mod   = $default['de_mobile_listtype_list_mod'];   // 한줄에 이미지 몇개씩 출력? 단, 모바일환경에서는 사용되지 않음.
$list_row   = $default['de_mobile_listtype_list_row'];   // 한 페이지에 몇라인씩 출력?

$img_width  = $default['de_mobile_listtype_img_width'];  // 출력이미지 폭
$img_height = $default['de_mobile_listtype_img_height']; // 출력이미지 높이
?>
<div id="listtype"> 
<?php
// 상품 출력순서가 있다면
$order_by = ' it_order, it_id desc ';
if ($sort != '')
    $order_by = $sort.' '.$sortodr.' , it_order, it_id desc';
else
    $order_by = 'it_order, it_id desc';

if (!$skin)
    $skin = $default['de_listtype_list_skin'];
else
    $skin = preg_replace('#\.+[\\\/]#', '', $skin);

define('G5_SHOP_CSS_URL', G5_SHOP_SKIN_URL);
?>

<div class="nav">
	<ul class="grid3">
		<li <?php if($type==1) echo "class='color'";?>><a href="<?php echo G5_SHOP_URL;?>/listtype.php?type=1"><img src="<?php echo G5_THEME_URL.'/img/'.G5_MOBILE_DIR;?>/top_menu_icon_1.jpg" alt=""> 베스트상품</a></li>
		<li <?php if($type==3) echo "class='color'";?>><a href="<?php echo G5_SHOP_URL;?>/listtype.php?type=3"><img src="<?php echo G5_THEME_URL.'/img/'.G5_MOBILE_DIR;?>/top_menu_icon_2.jpg" alt=""> 신상품</a></li>
		<li <?php if($type==2) echo "class='color'";?>><a href="<?php echo G5_SHOP_URL;?>/listtype.php?type=2"><img src="<?php echo G5_THEME_URL.'/img/'.G5_MOBILE_DIR;?>/top_menu_icon_3.jpg" alt=""> MD추천상품</a></li>
				
		<li><a href="/shop/eventbanner.php"><img src="<?php echo G5_THEME_URL.'/img/'.G5_MOBILE_DIR;?>/top_menu_icon_6.jpg" alt="">이벤트</a></li>
		<li><a href="/shop/health.php"><img src="<?php echo G5_THEME_URL.'/img/'.G5_MOBILE_DIR;?>/top_menu_icon_4.jpg" alt="">건강정보</a></li>
		<li><a href="/shop/board.php"><img src="<?php echo G5_THEME_URL.'/img/'.G5_MOBILE_DIR;?>/top_menu_icon_5.jpg" alt="">고객센터</a></li>

	</ul>
</div>

<div id="titleArea" class="">
	<h2><?php echo $g5['title'];?></h2>
</div>

<?php
// 리스트 유형별로 출력
$list_file = G5_MSHOP_SKIN_PATH.'/'.$skin;
if (file_exists($list_file)) {
    // 총몇개 = 한줄에 몇개 * 몇줄
    $items = $list_mod * $list_row;
    // 페이지가 없으면 첫 페이지 (1 페이지)
    if ($page < 1) $page = 1;
    // 시작 레코드 구함
    $from_record = ($page - 1) * $items;

    $list = new item_list();
    $list->set_comp($comp);
    $list->set_list_skin($list_file);
    $list->set_list_mod($list_mod);
    $list->set_list_row($list_row);
    $list->set_img_size($img_width, $img_height);
    $list->set_is_page(true);
    $list->set_order_by($order_by);
	$list->set_mb_type($member['mb_type']);
    $list->set_from_record($from_record);
    $list->set_view('it_img', true);
    $list->set_view('it_id', false);
    $list->set_view('it_name', true);
    $list->set_view('it_cust_price', false);
    $list->set_view('it_price', true);
    $list->set_view('it_icon', true);
    $list->set_view('sns', false);
    $display = $list->run();

    // where 된 전체 상품수
    $total_count = $list->total_count;
    // 전체 페이지 계산
    $total_page  = ceil($total_count / $items);

	echo $display;
}
else
{
    echo '<div align="center">'.$skin.' 파일을 찾을 수 없습니다.<br>관리자에게 알려주시면 감사하겠습니다.</div>';
}
?>
</div>

<?php
$qstr .= '&amp;comp='.$comp.'&amp;sort='.$sort.'&amp;sortodr='.$sortodr;
echo get_paging($config['cf_write_pages'], $page, $total_page, "{$_SERVER['SCRIPT_NAME']}?$qstr&amp;page=");
?>

<?php
include_once(G5_MSHOP_PATH.'/_tail.php');
?>
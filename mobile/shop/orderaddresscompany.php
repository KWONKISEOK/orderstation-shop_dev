<?php
include_once('./_common.php');

if(!$is_member)
    alert_close('회원이시라면 회원로그인 후 이용해 주십시오.');
$it_company = $_REQUEST[it_company];
$TmpString=explode(",", $it_company);

$od_name = $_REQUEST[od_name];

$str = $it_company;
$num = preg_replace("/[^0-9]*/s", "", $str);

$sql = "select * from tbl_shop_order_address_company";
$result = @sql_query($sql);
$total_count = @sql_num_rows($result);

$cnt = 0;
for($i=0; $i < $total_count; $i++){
    if($TmpString[$i]){
        $cnt++;
    }
}

$g5['title'] = '사내 수령지 목록';
include_once(G5_PATH.'/head.sub.php');
?>

<style>
    table td {text-align: center;}
</style>

<div id="sod_addr" class="new_win">

    <h1 id="win_title" style="width: 390px"><i class="fa fa-address-book-o" aria-hidden="true"></i> 사내 수령지 목록<span id="order_qty"></span></h1>
    <button type="button" onclick="self.close();" class="btn_close_top"></button>
    <div class="new_win_con">
        <div class="tbl_head01 tbl_wrap">
            <table style="width: 350px">
                <thead>
                <tr>
                    <th scope="col">배송지명</th>
                    <th scope="col">선택</th>
                </tr>

                </thead>
                <tbody>
                <?php
                $sep = chr(30);
                for($i=0; $i < $cnt; $i++){
                    $sql = "select * from tbl_shop_order_address_company where it_company = '$TmpString[$i]'";
                    $row = sql_fetch($sql);
                    $addr = $row['ad_name'].$sep.$row['ad_tel'].$sep.$row['ad_hp'].$sep.$row['ad_zip1'].$sep.$row['ad_zip2'].$sep.$row['ad_addr1'].$sep.$row['ad_addr2'].$sep.$row['ad_addr3'].$sep.$row['ad_jibeon'].$sep.$row['ad_subject'];
                    $addr = get_text($addr);
                    ?>
                    <tr style="background-color: white">
                        <td class="td_sbj">
                            <span id="ad_subject<?php echo $i;?>"><?php echo str_empty_chk(get_text($row['ad_subject'])); ?></span>
                        </td>
                        <td class="td_sbj">
                            <input type="hidden" value="<?php echo $addr; ?>" class="hidden_addr">
                            <input type="hidden" value="<?php echo $row['ad_id']; ?>" class="hidden_aid">
                            <input type="hidden" value="<?php echo $od_name; ?>" id="od_name" class="hidden_aid">
                            <a href="#" class="btn_sel sel_address" style="line-height: 30px; margin: 0 25%; border-color: darkorange; color: darkorange">선택</a>
                        </td>
                    </tr>
                <?php }?>


                <script>
                    $(function() {
                        $(".sel_address").on("click", function() {
                            var addr = $(this).siblings("input").val().split(String.fromCharCode(30));
                            var f = window.opener.forderform;

                            f.od_b_name.value        = $("#od_name").val();
                            //f.od_b_tel.value         = addr[1];
                            f.od_b_hp.value          = addr[2];
                            f.od_b_zip.value         = addr[3] + addr[4];
                            f.od_b_addr1.value       = addr[5];
                            f.od_b_addr2.value       = addr[6];
                            f.od_b_addr3.value       = addr[7];
                            f.od_b_addr_jibeon.value = addr[8];
                            f.ad_subject.value       = addr[9];
                            var zip1 = addr[3].replace(/[^0-9]/g, "");
                            var zip2 = addr[4].replace(/[^0-9]/g, "");

                            if(zip1 != "" && zip2 != "") {
                                var code = String(zip1) + String(zip2);

                                if(window.opener.zipcode != code) {
                                    window.opener.zipcode = code;
                                }
                            }
                            f.ad_subject.readOnly = true;
                            f.od_b_hp.readOnly = true;
                            f.od_b_addr2.readOnly = true;
                            f.od_zip.disabled = true;

                            if(addr[0] == "태전약품전주" || addr[0] == "티제이팜평택"){
                                opener.alert("월요일 2시 이전 주문 시 화요일 수령 가능합니다.");
                            } else if (addr[0] == "오엔케이서울"){
                                opener.alert("수요일 2시 이전 주문 시 목요일 수령 가능합니다.");
                            }
                            $(opener.location).attr("href", "javascript:addr_company();");
                            self.close();
                        });
                    });

                </script>
<?php

?>
<div class="modal-popup modal-terms">
    <div class="modal-wrap">
        <div class="modal-body">
            <h1 class="modal-title">개인정보 수집 및 이용에 대한 안내</h1>
            <div class="terms-box">
                <?php echo $default['de_guest_privacy']; ?>
            </div>
            <button type="button" onclick="javascript:hideLayer();" class="btn-default"><span>확인</span></button>
        </div>
    </div>
</div>

<script>
    function terms () {
        $('.modal-popup').addClass('on');
    }
    function hideLayer() {
        $('.modal-popup').removeClass('on');
    }
</script>

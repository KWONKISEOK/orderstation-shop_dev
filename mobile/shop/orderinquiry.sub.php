<?php
if (!defined("_GNUBOARD_")) exit; // 개별 페이지 접근 불가

if (!defined("_ORDERINQUIRY_")) exit; // 개별 페이지 접근 불가

// 테마에 orderinquiry.sub.php 있으면 include
if(defined('G5_THEME_SHOP_PATH')) {
    $theme_inquiry_file = G5_THEME_MSHOP_PATH.'/orderinquiry.sub.php';
    if(is_file($theme_inquiry_file)) {
        include_once($theme_inquiry_file);
        return;
        unset($theme_inquiry_file);
    }
}
?>

<?php if (!$limit) { ?>총 <?php echo $cnt; ?> 건<?php } ?>

<link rel="stylesheet" href="//code.jquery.com/ui/1.8.18/themes/base/jquery-ui.css" />
<script src="//code.jquery.com/ui/1.8.18/jquery-ui.min.js"></script>

<form name="frm1" method="get" action="/shop/orderinquiry.php" style="margin:0">	

	<div id="search-box">
		<span class="btndate" id="today">오늘</span>&nbsp;
		<span class="btndate" id="bday15">15일</span>&nbsp;
		<span class="btndate" id="bmonth1">1개월</span>&nbsp;
		<span class="btndate" id="bmonth3">3개월</span>&nbsp;&nbsp;
		<br/><br/>
          <input type="text" name="sdate" id="sdate" style="width:90px; text-align:center" class="datepickerbutton" value="<?php echo $sdate;?>" readonly>
          ~
          <input type="text" name="edate" id="edate" style="width:90px; text-align:center" class="datepickerbutton" value="<?php echo $edate;?>" readonly>
          <button class="btn03">조회</button>
	</div>

</form>
<style>
.btn03 {
    display: inline-block;
    padding: 5px;
    border: 1px solid #3b3c3f;
    background: #4b545e;
    color: #fff;
    text-decoration: none;
    vertical-align: middle;
}

</style>
<script>
    $.datepicker.setDefaults({
        dateFormat: 'yy-mm-dd',
        prevText: '이전 달',
        nextText: '다음 달',
        monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
        monthNamesShort: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
        dayNames: ['일', '월', '화', '수', '목', '금', '토'],
        dayNamesShort: ['일', '월', '화', '수', '목', '금', '토'],
        dayNamesMin: ['일', '월', '화', '수', '목', '금', '토'],
        showMonthAfterYear: true,
        yearSuffix: '년'
    });

    $(function() {
        $(".datepickerbutton").datepicker();
    });

	function setDate(kind) {
		var today = '<?php echo date("Y-m-d"); ?>';
		var bday15 = '<?php echo date("Y-m-d", strtotime('-15 days')); ?>';
		var bmonth1 = '<?php echo date("Y-m-d", strtotime('-1 month')); ?>';
		var bmonth3 = '<?php echo date("Y-m-d", strtotime('-3 month')); ?>';

		if(kind==1) {
			$("#sdate").val(today);
			$("#edate").val(today);
		}
		if(kind==2) {
			$("#sdate").val(bday15);
			$("#edate").val(today);
		}
		if(kind==3) {
			$("#sdate").val(bmonth1);
			$("#edate").val(today);
		}
		if(kind==4) {
			$("#sdate").val(bmonth3);
			$("#edate").val(today);
		}

	}
	$( "#today" ).click(function() {
	  setDate(1);
	});
	$( "#bday15" ).click(function() {
	  setDate(2);
	});
	$( "#bmonth1" ).click(function() {
	  setDate(3);
	});
	$( "#bmonth3" ).click(function() {
	  setDate(4);
	});

</script>

<div id="sod_inquiry">
    <ul>
        <?php
        $sql = " select *,
                    (od_cart_coupon + od_coupon + od_send_coupon) as couponprice
                   $sql_common
                  order by od_id desc  $limit";
        $result = sql_query($sql);
        for ($i=0; $row=sql_fetch_array($result); $i++)
        {
            
            $uid = md5($row['od_id'].$row['od_time'].$row['od_ip']);
        ?>

        <li>
            <div class="inquiry_idtime">
                <a href="<?php echo G5_SHOP_URL; ?>/orderinquiryview2.php?od_id=<?php echo $row['od_id']; ?>&amp;uid=<?php echo $uid; ?>" class="idtime_link"><?php echo $row['od_id']; ?></a>
				&nbsp;결제금액 &nbsp;<?php echo number_format($row['od_cart_price']); ?>
                <span class="idtime_time"><?php echo substr($row['od_time'],2,8); ?></span>
            </div>
			
            <div class="inquiry_name">
				

				<?php
				// 주문상품
				$sql = " select a.it_name,  a.od_qty,a.od_total_drug_price, a.od_total_sale_price, a.od_total_incen, a.od_send_cost,
							a.od_option, b.od_status, b.od_pay_yn , b.od_invoice,b.od_delivery_company, b.od_b_name, b.od_b_addr1, b.od_b_addr2, b.od_b_addr3
							from {$g5['g5_shop_order_detail_table']} a, tbl_shop_order_receiver b
							where a.od_id = b.od_id and a.od_num = b.od_num and b.od_seq = 1 and a.od_id = '{$row['od_id']}'";
				$result2 = sql_query($sql);

				for ($i=0; $ct=sql_fetch_array($result2); $i++)
				{
					
					
					switch($ct['od_status']) {
						case '주문':
							$od_status = '<span class="status_01">입금확인중</span>';
							break;
						case '확정':
							$od_status = '<span class="status_01">입금확인중</span>';
							break;
						case '입금':
							$od_status = '<span class="status_02">입금완료</span>';
							break;
						case '준비':
							$od_status = '<span class="status_03">상품준비중</span>';
							break;
						case '배송':
							$od_status = '<span class="status_04">상품배송</span>';
							break;
						case '완료':
							$od_status = '<span class="status_05">배송완료</span>';
							break;
						case '반품접수':
							$od_status = '<span class="status_05">반품접수</span>';
							break;
						case '반품승인':
							$od_status = '<span class="status_05">반품접수</span>';
							break;
						case '반품반려':
							$od_status = '<span class="status_05">반품반려</span>';
							break;
						case '반품완료':
							$od_status = '<span class="status_05">반품완료</span>';
							break;
						default:
							$od_status = '<span class="status_06">주문취소</span>';
							break;
					}

					if( ($ct['od_status'] == "주문" || $ct['od_status'] == "확정") && $ct['od_pay_yn'] == "Y" ){
						$od_status = '<span class="status_02">입금완료</span>';
					}

					if($member['mb_type']=='1') {
						switch($row['od_gubun']) {
							case '1':
								$od_gubun = '<span class="status_01">고객주문</span>';
								break;
							case '2':
								$od_gubun = '<span class="status_02">약국주문</span>';
								break;
							case '3':
								$od_gubun = '<span class="status_03">추전고객</span>';
								break;							
						}
					}

					$od_invoice = '';
					if($ct['od_delivery_company'] && $ct['od_invoice']){

					    if($ct['od_delivery_company'] != "한진택배"){
                            $od_invoice = '<span class="inv_inv"><i class="fa fa-truck" aria-hidden="true"></i> <strong>'.get_text($ct['od_delivery_company']).'</strong> '.get_text($ct['od_invoice']).' &nbsp;&nbsp;'.get_delivery_inquiry_button($ct['od_delivery_company'], $ct['od_invoice'], 'delivery_search').'</span>';
                        }else{
                            $od_invoice = '<span class="inv_inv"><i class="fa fa-truck" aria-hidden="true"></i> <strong>'.get_text($ct['od_delivery_company']).'</strong> '.get_text($ct['od_invoice']).' &nbsp;&nbsp;'.get_delivery_inquiry_button_han($ct['od_delivery_company'], $ct['od_invoice'], 'delivery_search').'</span>';
                        }
                    }

					$ct_name = get_text($ct['it_name']).' '.get_text($ct['od_option']);
					$b_name = get_text($ct['od_b_name']);
					$b_addr1 = get_text($ct['od_b_addr1']);
					$b_addr2 = get_text($ct['od_b_addr2']).' '.get_text($ct['od_b_addr3']);


					?>
					<?php if($i>0) { ?>
					<table style="border-top: 1px solid #ccc;margin:6px 0 6px 0 ;">
					<?php } else { ?>
					<table>
					<?php } ?>
					<tr>
						<td width="30%">상품명</td>
						<td><?php echo $ct_name; ?></td>
					</tr>
                        <tr>
                            <td width="30%">배송지</td>
                            <td><?php echo $b_name; ?><br>
                            <?php echo $b_addr1?><br>
                            <?php echo $b_addr2?></td>
                        </tr>
					<tr>
						<td>주문금액</td>
						<td>  <?php echo $ct['od_qty']; ?>개 <?php ?>
						<?php if($member['mb_type']=='1') {
                            echo number_format($ct['od_total_drug_price']+$ct['od_sned_cost']);
						    ?>
						(<?php echo number_format($ct['od_total_incen']); ?>)
						<?php } else{
                            echo number_format($ct['od_total_sale_price']);
                        }?>
						</td>
					</tr>
					<!-- <tr>
						<td>배송상태</td>
						<td><?php echo $od_status; ?></td>
					</tr> -->
					<tr>
						<td colspan="2">
						<div class="inquiry_inv">
							<?php echo $od_invoice; ?>
							<span class="inv_status"><?php echo $od_status; ?></span>
						</div>
						</td>
					</tr>
					</table>
					


				<?php } ?>
				
            </div>

            <!-- <div class="inquiry_price">
                <?php echo display_price($row['od_receipt_price']); ?>
            </div>
            <div class="inquiry_inv">
                <?php echo $od_invoice; ?>
                <span class="inv_status"><?php echo $od_status; ?></span>
            </div> -->
        </li>

        <?php
        }

        if ($i == 0)
            echo '<li class="empty_list">주문 내역이 없습니다.</li>';
        ?>
    </ul>
</div>

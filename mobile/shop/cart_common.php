
<?
add_stylesheet('<link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Noto+Sans+KR:wght@100;300;400;500;700;900&display=swap">', 0);
add_stylesheet('<link rel="stylesheet" href="'.G5_THEME_CSS_URL.'/cart_mobile.css?date=20200707">', 0);
?>

<div class="sod_list notof">

	<?php
	$tot_point = 0;
	$tot_sell_price = 0;
	$tot_sendcost_price = 0; //배송비총계
	$tot_left_price = 0; //소비자가총계
	$tot_right_price = 0; //약국가총계
	$tot_point_price = 0; //포인트 할인금액 총계

	//장바구니 검사
	$result = c_mysqli_call('SP_ORDER_INFO_NEW_VERSION', " '".$s_cart_id."','CART','".$member['mb_type']."','".$period."','','".$get_it_id."' ");

	$cart_count = sql_num_rows($result);

	$i=0;
	$TmpString="";
	$LeftSum=0; // 소비자가 
	$RightSum=0; // 약국가
	$trans_memo = "";
	$TimeGoodsYn = "N"; //타임세일 제품이 있는지 여부
	$item_Soldout ="N"; //품절인지 체크
	$soldout_cnt = 0; //품절상품 갯수 체크 

	foreach($result as $row) {
		// 합계금액 계산
		$sql = " select SUM(IF(io_type = 1, (io_price * ct_qty), ((ct_price + io_price) * ct_qty))) as price,
						SUM(ct_point * ct_qty) as point,
						SUM((select it_incen * a.ct_qty from tbl_shop_item where it_id = a.it_id))  as it_incen, 
						SUM((select io_incen * a.ct_qty from tbl_shop_item_option where io_id = a.io_id limit 1) ) as io_incen, SUM(ct_qty) as qty,
						SUM(ct_qty) as qty
					from {$g5['g5_shop_cart_table']} a
					where it_id = '{$row['it_id']}'
					  and od_id = '$s_cart_id' ";
		$sum = sql_fetch($sql);

		if ($i==0) { // 계속쇼핑
			$continue_ca_id = $row['ca_id'];
		}

		$a1 = '<a href="./item.php?it_id='.$row['it_id'].'"><strong>';
		$a2 = '</strong></a>';
		$image_width = 80;
		$image_height = 80;
		$image = get_it_image($row['it_id'], $image_width, $image_height);

		$it_name = $a1 . stripslashes($row['it_name']) . $a2;
		$it_options = print_item_options_new($row['it_id'], $s_cart_id);
		$period_cnt	=0;
		$period_cnt	= $row['ct_period_cnt'];
		$ct_period_yn = $row['ct_period_yn'];

		if($it_options) {

			$TmpString=explode("|", $it_options);
			$LeftSum=$LeftSum+$TmpString[1];
			$RightSum=$RightSum+$TmpString[2];

			if($ct_period_yn == 'Y') {
				$mod_options = '<button type="button" id="mod_opt_'.$row['it_id'].'" class="mod_btn mod_options2">주문수정</button><button id="mod_opt_'.$row['it_id'].'"  type="button" class="mod_btn mod_options_auto2">정기주문수정</button>';
			} else {
				 $mod_options = '<button type="button" id="mod_opt_'.$row['it_id'].'" class="mod_btn mod_options2">주문수정</button>';
			}

		   $it_name2 = '<div class="sod_opt">'.$TmpString[0].'</div>';
		   // $it_name .= ;
		}

        // 소비자가
        $sql_price = "select it_id, it_cust_price, it_price, it_drug_price from tbl_shop_item where it_id = '$row[it_id]'";
        $re_price = sql_fetch($sql_price);

        $cust = '';
        if($re_price['it_cust_price']>0){ // 상품할인이 있는 경우
            $cust = $re_price['it_cust_price'] * $sum['qty'];
            $sale = ($cust - $LeftSum);
        } else{
            $sale = "0";
        }

		// 배송비
		/*
		switch($row['ct_send_cost'])
		{
			case 1:
				$ct_send_cost = '착불';
				break;
			case 2:
				$ct_send_cost = '무료';
				break;
			default:
				$ct_send_cost = '주문시결제';
				break;
		}
		*/
		if( $row['cal_it_sc_price'] == 0 ){
			 $ct_send_cost = '무료';
		}else{
			 $ct_send_cost = '주문시결제';			
		}

		$sendcost = $row['cal_it_sc_price'];

		//배송비업데이트
		if($sendcost>=0) {
			$sql = " update tbl_shop_cart set it_sc_cost = '$sendcost' where ct_id='{$row['ct_id']}' ";
			$rtncost = sql_fetch($sql);
		}

		$total_cost = $total_cost + $sendcost;

		//$point      = $sum['point'];
		//$sell_price = $sum['price'];

		//정기주문 예외처리 상품
		$optionprice = get_option_dis_price($row['it_id'], $row['io_id'],$row['io_type']);	
	
		if($optionprice > 0) {
			$sell_price = $sum['qty'] * ($row['ct_price']+$optionprice);
		} else {
			if($period_cnt > 0) {
				$sell_price = $sum['price'] * $period_cnt;
				$incen_sum = ($sum['it_incen']+$sum['io_incen']) * $period_cnt;
			} else {
				$sell_price = $sum['price'];
				$incen_sum = $sum['it_incen']+$sum['io_incen'];
			}
		}

        /* 상품별 배송비 정책 노출 */
        if($row['it_sc_type'] != 1){

            if($row['it_sc_type'] =='2') $trans_memo = number_format($row['it_sc_price']).'원 (동일 판매자 상품 '.number_format($row['it_sc_minimum']).'원 이상 구매 시 무료)';
            if($row['it_sc_type'] =='3') $trans_memo = number_format($row['it_sc_price']).'원 (동일 판매자 상품 묶음배송 가능)';
            if($row['it_sc_type'] =='4') $trans_memo = number_format($row['it_sc_price']).'원 <br>(X개 단위로 배송비 ooo원이 추가 부과)';

        }

        if( $row['it_trans_bundle'] == "1" ){
            $trans_memo .= "&nbsp;[묶음배송상품]";
        }

	?>

	<div class="padtet">
		<input type="hidden" name="it_id[<?php echo $i; ?>]"    value="<?php echo $row['it_id']; ?>">
		<input type="hidden" name="it_name[<?php echo $i; ?>]"  value="<?php echo get_text($row['it_name']); ?>">

		<? if( $row['row_num'] == "0" ){ ?>
		<table class="tit_tb">
			<tr>
				<td>
					<label class="container"> 
						<input type="checkbox"  class="ct_all" id="ct_all_<?=$row['comp_code']?>" value="1" data-comp="<?=$row['comp_code']?>" <? if( strpos($get_comp_code,$row['comp_code']) !== false  ){ ?> checked="checked" <?}?>  >
						<span class="checkmark"></span>
					</label>
				</td>
				<td>
					<a href="/shop/listcomp.php?comp=<?=$row['comp_code']?>" style="line-height: 1.4em;"><?=$row['comp_name']?></a>
				</td>
			</tr>
            <!--
			<? if( $row['comp_trans_use'] == "Y" && $row['comp_trans_compare'] > 0  ){ ?>
			<tr>
				<td colspan="2">
					<div style="font-size:10px;font-weight:bold;margin-top:-5px;margin-bottom:15px;text-align:left;">
						주문하시는 총 [묶음배송상품]의 합이 <?=number_format( $row['comp_trans_compare'] )?>원 이상시 묶음배송비 <?=number_format( $row['comp_trans_price'] )?>원 입니다.
					</div>			
				</td>
			</tr>
			<? } ?>
			-->
            <tr>
                <td colspan="2">
                    <div style="font-size:10px;font-weight:bold;margin-top:-5px;margin-bottom:15px;text-align:left;">
                        <?php echo $trans_memo; ?>
                    </div>
                </td>
            </tr>
		</table>
		<? } ?>
		<!-- 상품 -->
		<ul class="crt_con1" style="border:0px solid #000;position:relative;">
			<li class="inner_div"></li>
			<li>
				<ul class="cont_tit1">
					<li>
						<label class="container_2"> 
							<input type="checkbox" name="ct_chk[<?php echo $i; ?>]" value="1" id="ct_chk_<?php echo $i; ?>" <? if( strpos($get_it_id,$row['it_id']) !== false  ){ ?> checked="checked" <?}?> data-change_yn="<?=$row['price_change_yn_sum'];?>" data-it_id="<?=$row['it_id'];?>" data-comp="<?=$row['comp_code']?>">
							<span class="checkmark_2"></span>
						</label>
					</li>
					<li>
						<a><?php echo $it_name; ?></a>
						<span><?php echo $it_name2; ?></span>
                        <br><br>
                        <span> *수량 : <?php echo $sum['qty'];?></span>
						<?if( $row['price_change_yn_sum'] != 0 ){?>
							<div style='margin-top:5px;'><font color='red'>가격변동상품</font></div>
						<?}?>
						<? if( $row['it_type4'] == "1" && $time_sale_yn == "N" ){ ?>
							<div style='margin-top:5px;'><font color='blue'>타임세일상품</font></div>
						<? } ?>
						<? if( $row['it_soldout'] == "1" || $row['it_use'] =="0"){ ?>
							<div style='margin-top:5px;'><font color='blue'>품절 상품</font></div>
						<? } ?>
						<button type="button" onclick="return form_check('xdel','<?=$row['it_id']?>');">&#215;</button>
					</li>
				</ul>
			</li>

			<li>
				<div class="li_op_wr " style="border:0px solid #000;width:100%;">
					<div class="total_img"><?php echo $image; ?></div>
					<div class="nncartwr1">
						<div class="ncart_pli1">
							<div>
								<? if($member['mb_type']==1){ ?>
								<a>
									<span class="pli">
										<?php echo number_format($LeftSum); ?>원
									</span>
									<span class=smRLa1>!</span>
								</a>
								<div class="crtonbox" style="display:none;">
                                    <a>포인트</a><span>-<?php echo number_format($LeftSum-$RightSum); ?>원</span>
                                    <?php if($sale > 0 ){?>
                                    </br><a>상품할인</a><span>-<?php echo number_format($sale); ?>원</span>
                                    <?php }?>
                                </div>
								<p><?php echo number_format($RightSum); ?>원</p>
								<? }else{ ?>
                                <?php if($sale > 0 ){?>
                                <p style="text-decoration:line-through; color: #757575; font-size: medium"><?php echo number_format($LeftSum+$sale); ?>원</p>
                                <?php }?>
								<p style="margin-top:10px;"><?php echo number_format($LeftSum); ?>원</p>
								<a>
									<span class="pli_clone">&nbsp;</span>
									<span class="smRLa1_clone">&nbsp;</span>
								</a>
								<? } ?>
							</div>
							<?php echo $mod_options; ?>
						</div>
					</div>
				</div>
			</li>
		</ul>
		<!-- 상품 -->

	</div>

	<?
	if( $row['row_num'] == "0" ){
		$foot_qty		= 0; //총수량
		$foot_sendcost	= 0; //배송비
		$foot_LeftSum	= 0; //소비자가
		$foot_RightSum	= 0; //약국가
		$foot_PointSum	= 0; //포인트 할인금액
        $foot_Sale = 0; // 상품할인가
	}

	if( strpos($get_it_id,$row['it_id']) !== false ){
		$foot_qty += $sum['qty'];
		$foot_sendcost += $sendcost;
		$foot_LeftSum += $LeftSum;
		$foot_RightSum += $RightSum;
		$foot_PointSum += ( $LeftSum-$RightSum );
        $foot_Sale += $sale;

		$tot_sendcost_price += $sendcost; 
		$tot_left_price += $LeftSum; 
		$tot_right_price += $RightSum;
		$tot_point_price += ( $LeftSum-$RightSum );
		$tot_sell_price += $sell_price;
        $tot_sale += $sale;
        $tot_cust_price += $LeftSum + $sale;
	}		
	//총 리스트의 마지막 일때 처리
	if( $row['total_comp_it_cnt'] == $row['row_num'] ){
	?>
	<div class="chdgkq" style="margin-bottom:30px;line-height: 1.4em;" >
		<? if($member['mb_type']==1){ ?>
		약국공급가 <?=number_format($foot_RightSum)?>원 + 배송비 <?=number_format($foot_sendcost)?>원 = <?=number_format($foot_RightSum+$foot_sendcost)?>원
		<? }else{?>
            <?=number_format($foot_LeftSum+$foot_Sale)?>원
            <? if($foot_Sale > 0){?> - 상품할인 <?= number_format($foot_Sale)?>원<?}?>
            + 배송비 <?=number_format($foot_sendcost)?>원 = <?=number_format($foot_LeftSum+$foot_sendcost)?>원
		<? }
        if($row['it_sc_type'] =='2') { ?>
            <span style="font-size: 12px; text-align:left;">
            <br>(동일 판매자 상품) 최종 소비자가 <?=number_format($row['it_sc_minimum'])?> 원 이상 구매 시 배송비 무료
            <? if($member['mb_type']==1){ ?>
                <br>선택완료 최종소비자가 : <?=number_format($foot_LeftSum) ?> 원
             <span style="font-size: 10px">
                <br>* 최종소비자가 = 소비자가 - 상품할인가
             </span>
            <? } ?>
        </span>
        <?php } ?>
	</div>
	<?
	}	
	?>
	<?php
		
		$RightSum = 0;
		$LeftSum = 0;
		$incen_sum = 0;
		$trans_memo = "";

		//타임세일상품 체크여부
		if( $row['it_type4'] == "1" && $time_sale_yn == "N" ){ 
			$TimeGoodsYn = "Y";
		}
		if( $row['it_soldout'] == "1" || $row['it_use'] =="0"){ 
			$item_Soldout = "Y";
		}

		$i++;
	} // for 끝

	if ($i == 0) {
		echo '<li class="empty_list">장바구니에 담긴 상품이 없습니다.</li>';
	} else {
		// 배송비 계산
		//$send_cost = get_sendcost($s_cart_id, 0);
	}
	?>


	<?php
    if ($tot_left_price > 0 || $tot_sendcost_price > 0) {
    ?>

	<div class="padtet">
		<div class=" wnansgkrl">
			<div class="crte_titw">
				<h2>결제 예정 금액</h2> 
				<? if($member['mb_type']==1) { //약국회원일때 ?>
				<a><?=number_format($tot_right_price+$tot_sendcost_price);?>원</a>
				<?}else{?>
				<a><?=number_format($tot_left_price+$tot_sendcost_price);?>원</a>
				<? } ?>
				</div>
				<ul>
                    <li>
                        <div class=""><a>소비자가</a>
                            <p><?=number_format($tot_cust_price);?>원</p>
                        </div>
                    </li>
					<? if($member['mb_type']==1){ ?>
					<li>
						<div><span class="smRLa2">&#45;</span><a>할인</a>
							<p>-<?=number_format($tot_point_price+$tot_sale);?>원</p>
						</div>
					</li>
                    <li>
                        <div><span class="smRLa2">&#61;</span><a>약국공급가</a>
                            <p><?=number_format($tot_right_price);?>원</p>
                        </div>
                    </li>
					<? }else{ ?>
                        <li class="last_1">
                            <div><span class="smRLa2">&#45;</span><a>할인</a>
                                <p><?=number_format($tot_sale);?>원</p>
                            </div>
                        </li>
					<? } ?>
					<li class="last_1">
						<div><span class="smRLa2">&#43;</span><a>배송비</a>
							<p><?=number_format($tot_sendcost_price);?>원</p>
						</div>
					</li>
				</ul>
		</div>
		<div class="bt_c1">
			<button type="button" onclick="return form_check('seldelete','');" class="btn01">선택 삭제</button> 
			<button type="button" onclick="return form_check('buy','');" class="btn_submit">주문하기</button>
		</div>

	</div>

	<?php } ?>

</div>

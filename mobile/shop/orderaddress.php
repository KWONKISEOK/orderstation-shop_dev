<?php
include_once('./_common.php');

if(!$is_member)
    alert_close('회원이시라면 회원로그인 후 이용해 주십시오.');

$multi_gubun = $_REQUEST[multi_gubun]; //다중배송여부

$sql_common = " from {$g5['g5_shop_order_address_table']} where mb_id = '{$member['mb_id']}' ";

$sql = " select count(ad_id) as cnt " . $sql_common;
$row = sql_fetch($sql);
$total_count = $row['cnt'];

$g5['title'] = '배송지 목록';
include_once(G5_PATH.'/head.sub.php');
?>

<form name="forderaddress" method="post" action="<?php echo $order_action_url; ?>" autocomplete="off">
<div id="sod_addr" class="new_win">
    <h1 id="win_title"><i class="fa fa-address-book-o" aria-hidden="true"></i> 배송지 목록 <span id="order_qty"></span></h1>

    <div class="list_01" style="padding-bottom: 50px;">
        <ul>
            <?php
            $sep = chr(30);
            for($i=0; $row=sql_fetch_array($result); $i++) {
                $addr = $row['ad_name'].$sep.$row['ad_tel'].$sep.$row['ad_hp'].$sep.$row['ad_zip1'].$sep.$row['ad_zip2'].$sep.$row['ad_addr1'].$sep.$row['ad_addr2'].$sep.$row['ad_addr3'].$sep.$row['ad_jibeon'].$sep.$row['ad_subject'];
                $addr = get_text($addr);
            ?>
            <li>
                <div class="addr_title">
                    <input type="hidden" name="ad_id[<?php echo $i; ?>]" value="<?php echo $row['ad_id'];?>">
                    <i class="fa fa-caret-right" aria-hidden="true" style="position: absolute;top: 17px;left: 10px;"></i>
                    <label for="ad_subject<?php echo $i;?>" class="sound_only">배송지명</label>
                    <div style="line-height: 27px;">
                        <?php echo ($row['ad_subject']) ? $row['ad_name'].' ('.$row['ad_subject'].')' : $row['ad_name']; ?>
                        <?php if($row['ad_default']) { ?>
<!--                            <label for="" class="default_addr">기본배송지</label>-->
                            <span style="font-weight: 700;color: #ff8800;float: right;">기본배송지</span>
                        <?php } ?>
                    </div>
                </div>
                <div class="addr_info">
                    <div class="addr_name">
                        <? if( $multi_gubun == "Y" ){ ?>
                            <label for="chk_<?php echo $i;?>"><font size="1.8em">다중배송 여부 </font></label> <input type="checkbox" name="chk" id="chk_<?php echo $i;?>" class="chk">
                            <br>
                        <? } ?>
                    </div>
                    <div class="addr_addr"><?php echo print_address($row['ad_addr1'], $row['ad_addr2'], $row['ad_addr3'], $row['ad_jibeon']); ?></div>
                    <div class="addr_tel"><i class="fa fa-phone" aria-hidden="true"></i> <?php echo $row['ad_tel']; ?> / <i class="fa fa-mobile" aria-hidden="true"></i> <?php echo $row['ad_hp']; ?></div>
                </div>
                <div class="addr_btn">
                    <input type="hidden" value="<?php echo $addr; ?>" class="hidden_addr">
                    <input type="hidden" value="<?php echo $row['ad_id']; ?>" class="hidden_aid">
                    <?php if ($multi_gubun == "Y") { ?>
                    <button type="button" class="btn_sel sel_address2" onclick="RegMultiReceiverConfirm(this);">선택</button>
                    <?php } else { ?>
                    <button type="button" class="btn_sel sel_address">선택</button>
                    <?php } ?>
                    <a href="./orderaddresswrite.php?ad_id=<?php echo $row['ad_id']; ?>&multi_gubun=<?php echo $multi_gubun?>" class="mod_address">수정</a>
                    <a href="<?php echo $_SERVER['SCRIPT_NAME']; ?>?w=d&amp;ad_id=<?php echo $row['ad_id']; ?>" id="btn_del" class="del_address">삭제</a>
                    <input type="radio" name="ad_default" value="<?php echo $row['ad_id'];?>" id="ad_default<?php echo $i;?>" <?php if($row['ad_default']) echo 'checked="checked"';?>>
                </div>
            </li>
            <?php
            }
            ?>

            <?php if ($total_count == 0) { ?>
            <li style="padding: 150px 0 150px 0;text-align: center;font-weight: 700;color: #333;">
                등록된 배송지가 없습니다.
            </li>
            <?php } ?>
        </ul>
    </div>

    <div class="win_btn fixed">
        <a href="./orderaddresswrite.php?multi_gubun=<?php echo $multi_gubun?>" class="btn_add" style="width: 49%;line-height: 40px;">신규 등록</a>
        <? if ($multi_gubun == "Y") { ?>
            <button type="button" onclick="RegMultiReceiver();" class="btn_submit" style="width: 49%;">선택</button>
        <? } else { ?>
            <button type="button" onclick="self.close();" class="btn_close_new" style="width: 49%;margin: 0 0 5px;">닫기</button>
        <? } ?>
    </div>
    <button type="button" onclick="self.close();" class="btn_close_top"></button>
</div>
</form>

<?php echo get_paging($config['cf_mobile_pages'], $page, $total_page, "{$_SERVER['SCRIPT_NAME']}?$qstr&amp;page="); ?>

<script>
    $(function () {
        $(".sel_address").on("click", function () {
            var addr = $(this).siblings("input").val().split(String.fromCharCode(30));

            var f = window.opener.forderform;
            f.od_b_name.value = addr[0];
            //f.od_b_tel.value         = addr[1];
            f.od_b_hp.value = addr[2];
            f.od_b_zip.value = addr[3] + addr[4];
            f.od_b_addr1.value = addr[5];
            f.od_b_addr2.value = addr[6];
            f.od_b_addr3.value = addr[7];
            f.od_b_addr_jibeon.value = addr[8];
            f.ad_subject.value = addr[9];

            var zip1 = addr[3].replace(/[^0-9]/g, "");
            var zip2 = addr[4].replace(/[^0-9]/g, "");

            if (zip1 != "" && zip2 != "") {
                var code = String(zip1) + String(zip2);

                if (window.opener.zipcode != code) {
                    window.opener.zipcode = code;
                    window.opener.calculate_sendcost(code);
                }
            }

            f.ad_subject.readOnly = false;
            f.od_b_name.readOnly = false;
            f.od_b_hp.readOnly = false;
            f.od_b_addr2.readOnly = false;
            f.od_zip.disabled = false;
            window.close();
        });

        $(".del_address").on("click", function () {
            return confirm("배송지 목록을 삭제하시겠습니까?");
        });

        // 전체선택 부분
        $("#chk_all").on("click", function () {
            if ($(this).is(":checked")) {
                $("input[name^='chk[']").attr("checked", true);
            } else {
                $("input[name^='chk[']").attr("checked", false);
            }
        });

    });

    // 다중배송지 개별 선택
    function RegMultiReceiverConfirm(target){

        // 선택한 행의 전체 선택자
        var ts = $(target).closest('li');

        var TmpHtml = "";
        var MultiSum = 0;
        var order_cnt = parseInt($(opener.document).find("#order_qty").val());
        var aid = "";
        var aidChk = 0;

        var parent_sum = 0;
        var i=1;

        var addr = ts.find(".hidden_addr").val().split(String.fromCharCode(30));

        // 중복 주소지 추가 체크
        aid = ts.find(".hidden_aid").val();
        $(opener.document).find('input[name=multi_aid]').each(function (k, v) {
            if (v.value == aid) {
                aidChk++;
            }
        });

        if (ts.find(".multi_cnt").val() != '') {
            MultiSum = parseInt(ts.find(".multi_cnt").val());
        }

        TmpHtml += "<tr id='row_"+i+"'>";
        TmpHtml += " <td align='center' style='text-align:center;'>";
        TmpHtml += "    <input type='hidden' name='multi_aid' value='"+aid+"'/>";
        TmpHtml += "    <input type='checkbox' name='multi_chk'/>";
        TmpHtml += " </td>";
        TmpHtml += " <td align='center' style='text-align:center;'>";
        TmpHtml += " <input type='text' name='multi_cnt[]' class='multi_cnt' class='frm_input' style='width:30px;padding:0px;' onKeyPress=onlyNumber(this) maxlength='4' value='1'/> ";
        TmpHtml += " </td>";
        TmpHtml += " <td align='center'> ";
        TmpHtml += "    <table style='border-bottom: unset;'>";
        TmpHtml += "        <tr>";
        TmpHtml += "            <td align='left'>";
        TmpHtml += "	            <input type='hidden' name='multi_b_name[]' value='" + addr[0] + "'/> " + addr[0];
        TmpHtml += "            </td>";
        TmpHtml += "        </tr>";
        TmpHtml += "        <tr>";
        TmpHtml += "            <td align='left'>";
        TmpHtml += "	            <input type='hidden' name='multi_b_zip[]' value='" + addr[3] + addr[4] + "'/> " + addr[3] + addr[4];
        TmpHtml += "	            <input type='hidden' name='multi_b_addr1[]' value='" + addr[5] + "'/> " + addr[5];
        TmpHtml += "	            <input type='hidden' name='multi_b_addr2[]' value='" + addr[6] + "'/> " + addr[6];
        TmpHtml += "	            <input type='hidden' name='multi_b_addr3[]' value='" + addr[7] + "'/> " + addr[7];
        TmpHtml += "	            <input type='hidden' name='multi_b_addr_jibeon[]' value='" + addr[7] + "'/> ";
        TmpHtml += "	            <input type='hidden' name='multi_ad_subject[]' value='" + addr[7] + "'/> ";
        TmpHtml += "            </td>";
        TmpHtml += "        </tr>";
        TmpHtml += "        <tr>";
        TmpHtml += "            <td align='left'>";
        TmpHtml += "	            <input type='hidden' name='multi_b_hp[]' value='" + addr[2] + "'/> " + addr[2];
        TmpHtml += "            </td>";
        TmpHtml += "        </tr>";
        TmpHtml += "    </table>";
        TmpHtml += " </td>";
        TmpHtml += "</tr>";


        if (aidChk > 0) {
            alert("이미 추가된 배송지입니다. 주문서를 확인해주세요.");
            return;
        }

        $(opener.document).find('#multi_table > tbody:last').append(TmpHtml);
        $(opener.document).find("#multAdd").prop("type","button");
        $(opener.document).find(".multi_cnt").prop("type", "text");
        $(opener.document).find(".m_cnt").remove();
        $(opener.document).find("#multiChk").val("0");

        self.close();

    }

    // 다중배송지 리스트 체크 선택
    function RegMultiReceiver() {

        var addr = "";
        var TmpHtml = "";
        var MultiSum = 0;
        var ErrorChk = 0;
        var order_cnt = parseInt($(opener.document).find("#order_qty").val());

        var aid = "";
        var aidChk = 0;

        var parent_sum = 0;
        var i = 1;


        $("input[name=chk]:checkbox").each(function (index, item) {

            if ($("input:checkbox[name=chk]").eq(index).prop("checked")) {

                addr = $(document).find(".hidden_addr").eq(index).val().split(String.fromCharCode(30));

                // 중복 주소지 추가 체크
                aid = $(document).find(".hidden_aid").eq(index).val();
                $(opener.document).find('input[name=multi_aid]').each(function(k, v) {
                    if (v.value == aid) {
                        aidChk++;
                    }
                });


                TmpHtml += "<tr id='row_" + i + "' style='border-bottom: 1px solid #d3d3d3;'>";
                TmpHtml += " <td align'center' style='text-align:center;'>";
                TmpHtml += "    <input type='hidden' name='multi_aid' value='"+aid+"'/>";
                TmpHtml += "    <input type='checkbox' name='multi_chk'/>";
                TmpHtml += " </td>";
                TmpHtml += " <td align'center' style='text-align:center;'>";
                TmpHtml += "    <input type='text' name='multi_cnt[]' class='multi_cnt' class='frm_input' style='width:30px;padding:0px;' onKeyPress=onlyNumber(this) maxlength='4' value='1'/> ";
                TmpHtml += " </td>";
                TmpHtml += " <td align='center'> ";
                TmpHtml += "    <table style='border-bottom: unset;'>";
                TmpHtml += "        <tr>";
                TmpHtml += "            <td align='left'>";
                TmpHtml += "	            <input type='hidden' name='multi_b_name[]' value='" + addr[0] + "'/> " + addr[0];
                TmpHtml += "            </td>";
                TmpHtml += "        </tr>";
                TmpHtml += "        <tr>";
                TmpHtml += "            <td align='left'>";
                TmpHtml += "	            <input type='hidden' name='multi_b_zip[]' value='" + addr[3] + addr[4] + "'/> " + addr[3] + addr[4];
                TmpHtml += "	            <input type='hidden' name='multi_b_addr1[]' value='" + addr[5] + "'/> " + addr[5];
                TmpHtml += "	            <input type='hidden' name='multi_b_addr2[]' value='" + addr[6] + "'/> " + addr[6];
                TmpHtml += "	            <input type='hidden' name='multi_b_addr3[]' value='" + addr[7] + "'/> " + addr[7];
                TmpHtml += "	            <input type='hidden' name='multi_b_addr_jibeon[]' value='" + addr[7] + "'/> ";
                TmpHtml += "	            <input type='hidden' name='multi_ad_subject[]' value='" + addr[7] + "'/> ";
                TmpHtml += "            </td>";
                TmpHtml += "        </tr>";
                TmpHtml += "        <tr>";
                TmpHtml += "            <td align='left'>";
                TmpHtml += "	            <input type='hidden' name='multi_b_hp[]' value='" + addr[2] + "'/> " + addr[2];
                TmpHtml += "            </td>";
                TmpHtml += "        </tr>";
                TmpHtml += "    </table>";
                TmpHtml += " </td>";
                TmpHtml += "</tr>";

                i++;

                ErrorChk++;
            }

        });

        if (ErrorChk == 0) {
            alert("선택된 배송지가 없습니다. 배송지를 체크해주세요.");
            return;
        }

        if (aidChk > 0) {
            alert("이미 추가된 배송지가 있습니다. 주문서를 확인해주세요.");
            return;
        }

        //$(opener.document).find('#multi_table > tbody').empty();
        $(opener.document).find('#multi_table > tbody:last').append(TmpHtml);
        $(opener.document).find("#multAdd").prop("type","button");
        $(opener.document).find(".multi_cnt").prop("type", "text");
        $(opener.document).find(".m_cnt").remove();
        $(opener.document).find("#multiChk").val("0");

        self.close();

    }

    <? if( $multi_gubun == "Y" ){ ?>
    var order_qty = $(opener.document).find('#order_qty').val();
    $("#order_qty").html("(장바구니 담은갯수:" + order_qty + ")");
    <? } ?>

    $(function(){
        stickyFooter();

        $(window).scroll(stickyFooter).resize(stickyFooter);
    });


    function stickyFooter(){
        document_height = $(document).height(); // 문서 전체 높이
        document_scrollTop = $(document).scrollTop(); // 문서 전체 높이 중 스크롤 위치
        window_height = $(window).height(); // 창 높이
        footer_height = $(".win_btn .fixed").height();

        gap = document_height - footer_height - window_height;
        bottom = document_scrollTop - gap ;

        if(document_scrollTop > gap){
            $(".win_btn .fixed").css("bottom", bottom+"px");
        }else{
            $(".win_btn .fixed").css("bottom","0");
        }
    }

</script>

<?php
include_once(G5_PATH.'/tail.sub.php');
?>
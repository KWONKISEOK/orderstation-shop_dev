<?php
include_once('./_common.php');

// 테마에 orderinquiryview.php 있으면 include
if(defined('G5_THEME_SHOP_PATH')) {
    $theme_inquiryview_file = G5_THEME_MSHOP_PATH.'/orderinquiryview.php';
    if(is_file($theme_inquiryview_file)) {
        include_once($theme_inquiryview_file);
        return;
        unset($theme_inquiryview_file);
    }
}

$g5['title'] = '주문상세내역';
include_once(G5_MSHOP_PATH.'/_head.php');

//달콩 이벤트세트 구매한 회원 쿠폰 발행해주기;
//EventDalcongCoupon($member['mb_name'],$member['mb_id'],$member['mb_type']);

// LG 현금영수증 JS
if($od['od_pg'] == 'lg') {
    if($default['de_card_test']) {
    echo '<script language="JavaScript" src="http://pgweb.uplus.co.kr:7085/WEB_SERVER/js/receipt_link.js"></script>'.PHP_EOL;
    } else {
        echo '<script language="JavaScript" src="http://pgweb.uplus.co.kr/WEB_SERVER/js/receipt_link.js"></script>'.PHP_EOL;
    }
}

$sql = "select * from {$g5['g5_shop_order_table']} where od_id = '$od_id' {$service_type_and_query} ";
$od = sql_fetch($sql);

$sql = "select sum(od_total_drug_price) as od_total_drug_price from {$g5['g5_shop_order_detail_table']} where od_id = '$od_id' {$service_type_and_query} ";
$od_dt = sql_fetch($sql);

// 주문 취소 데이터 확인
$sql = "select * from {$g5['g5_shop_order_cancel_table']} where od_id = '$od_id'";
$od_cancel = sql_fetch($sql);

?>

<!-- 추가 0121 -->
<h2 class="title-section">주문완료</h2>
<div class="order-result-top">
    <p class="order-desc">고객님의 주문이 정상적으로 완료되었습니다.</p>
    <p class="order-number"><span class="label">주문번호</span><span class="desc"><?php echo $od_id; ?></span></p>
</div>
<!-- [D] 아코디언 기능이 들어가는 경우 .accordion-box, .accordion-title, .accordion-content 추가 -->
<div class="border-top-bottom-style accordion-box">
    <?php
    $st_count1 = $st_count2 = 0;
    $custom_cancel = false;

    $sql = " select it_id, it_name, od_send_cost, 0 it_sc_type
                    from {$g5['g5_shop_order_detail_table']}
                    where od_id = '$od_id' {$service_type_and_query}
                    group by it_id
                    order by od_id ";
    $result = sql_query($sql);
    $total_cnt = sql_num_rows($result);
    //debug2($total_cnt);
    ?>
    <h3 class="title-box accordion-title is-opened">
        <span class="left-side title-main">주문 상품<span>(<?php echo $total_cnt; ?>)</span></span>
        <span class="right-side"><i class="icon icon-accordion"></i></span>
    </h3>
    <div class="content-box accordion-content">
        <?php

        //완료가 아닌갯수 구하기
        $order_receiver_cnt = 0;
        $return_possible = ""; //2차옵션 상품일경우 반품을 막기위한 조치

        for ($i = 0; $row = sql_fetch_array($result); $i++) {
            $image_width = 80;
            $image_height = 80;
            $image = get_it_image($row['it_id'], 80, 80, '', '', $row['it_name']);

            $sql = " select a.od_id, a.it_name, a.od_option, a.od_qty qty, b.od_seq, b.od_qty, a.od_price, od_total_sale_price, od_total_drug_price, od_total_incen, b.od_status, b.od_period_date, a.od_period_cnt, a.od_send_cost,
				                b.od_delivery_company,b.od_invoice, a.od_num , b.od_bill_tno , a.io_id, b.od_pay_yn, b.od_return_confirm, b.od_pay_date
                            from {$g5['g5_shop_order_detail_table']} a, tbl_shop_order_receiver b
                            where a.od_id = b.od_id 
							  and a.od_num = b.od_num
							  and a.od_id = '$od_id'
                              and it_id = '{$row['it_id']}' {$service_type_and_query_a} ";


            $res = sql_query($sql);
            $rowspan = sql_num_rows($res) + 1;

            $ReceiptHtml = "";

            //완료가 아닌갯수 구하기
            $order_receiver_cnt = 0;
            $return_possible = ""; //2차옵션 상품일경우 반품을 막기위한 조치

            for($k=0; $opt=sql_fetch_array($res); $k++) {

                //2차옵션 상품이 끼어있으면 반품신청을 못한다.
                if( strpos( $opt['io_id'] , chr(30) ) !== false ){
                    $return_possible = "false";
                }

                if( !empty($opt['od_bill_tno']) ) {
                    $receipt_script = 'window.open(\'https://iniweb.inicis.com/DefaultWebApp/mall/cr/cm/mCmReceipt_head.jsp?noTid='.$opt['od_bill_tno'].'&noMethod=1\',\'receipt\',\'width=430,height=700\');';
                    $link_script = "&nbsp;<a href=\"javascript:;\" onclick=".$receipt_script.">[영수증 출력]</a>";
                    $ReceiptHtml .= "<tr><td>".get_text($opt['od_option'])."-".get_text($opt['od_period_date'])."&nbsp;".$link_script."</td></tr>";
                }

                $od_invoice = '';

                if($opt['od_delivery_company'] && $opt['od_invoice'])
                    $od_invoice = $opt['od_delivery_company'].' '.get_delivery_inquiry($opt['od_delivery_company'], $opt['od_invoice'], 'dvr_link');

                if($opt[od_period_cnt] > 0) {
                    $subprice= $opt['od_total_sale_price'] / $opt[od_period_cnt];
                    $subincen= $opt['od_total_incen'] / $opt[od_period_cnt];
                } else {
                    $subprice=$opt['od_total_sale_price'];
                    $subincen= $opt['od_total_incen'] ;
                }

                // 소비자가
                $sql_price = "select it_id, it_cust_price, it_price, it_drug_price from tbl_shop_item where it_id = '$row[it_id]'";
                $re_price = sql_fetch($sql_price);

                $cust = '';
                $sale_yn = '';
                $sale = '';
                if($re_price['it_cust_price']>0){ // 상품할인이 있는 경우
                    $sale_yn = 'y';
                    $cust = $re_price['it_cust_price'] * $opt['od_qty'];
                    $sale = ($cust - $subprice);

                }

                $tot_cust_price += ($sale + $subprice);
                $tot_sale += $sale;

        ?>
            <!-- 상품 START -->
            <div class="box-item">
                <div class="image-box">
                    <div class="item-image"><?php echo $image; ?></div>
                    <div class="item-desc">
                        <p class="product">
                            <?php
                            if ($opt['od_status'] == "완료" || $opt['od_status'] == "반품반려") {
                                $order_receiver_cnt++;
                                echo '<input type="checkbox" name="return_od_num[]" value="'.$opt['od_num'].'">';
                            }
                            ?>
                            <a href="./item.php?it_id=<?php echo $row['it_id']; ?>"><?php echo $row['it_name']; ?></a>

                            <!-- 주문상태 -->
                            <span class="btndate" style="margin-left: 5px;">
                                <?php
                                if ($opt['od_status'] == "주문" && $opt['od_pay_yn'] == "Y") {
                                    echo '입금';
                                } else {
                                    echo $opt['od_status'];
                                }
                                ?>
                            </span>
                        </p>
                        <ul>
                            <li class="desc">
                                옵션 : <?php echo get_text($opt['od_option']); ?>
                                <?php if ($opt['od_period_cnt'] > 0) { ?>
                                    <br> 배송일 : <?php echo get_text($opt['od_period_date']); ?> (<?php echo $opt['od_seq'] ?>회차)
                                <?php } ?>
                                <!--<span class="total-count">1</span>세트--></li>
                        </ul>
                        <p class="price-set">
                            <span class="pay-price">
                                <span class="price">
                                    <?php
                                    if($opt['od_period_cnt'] > 0) {
                                        $subprice= $opt['od_total_sale_price'] / $opt[od_period_cnt];
                                        $subincen= $opt['od_total_incen'] / $opt[od_period_cnt];
                                    } else {
                                        $subprice=$opt['od_total_sale_price'];
                                        $subincen= $opt['od_total_incen'] ;
                                    }
                                    echo number_format($subprice);
                                    ?>
                                </span>
                                <span class="unit">원</span>
                            </span>
                            <!--<span class="original-price"><del>12,500원</del></span>-->
                        </p>
                    </div>
                </div>
            </div>
            <div class="box-item">
                <span class="label">상품 금액</span>
                <span class="pricebox">
                    <span style="text-decoration:line-through; color: #757575; font-size: 13px;">
                    <?php if($cust != ''){ ?>
                        <?php echo number_format($cust/$opt['od_qty']).'<span class="unit">&nbsp;원</span>' ; //정가 ?>
                    <?php } else if($member['mb_type']=='1') {?>
                        <?php echo number_format($subprice/$opt['od_qty']).'<span class="unit">&nbsp;원</span>' ; //정가 ?>
                    <?php }?>
                     </span>
                     <?php
                     if($member['mb_type']=='1') {
                         if ($opt[od_period_cnt] > 0) {
                             $subprice = $opt['od_total_drug_price'] / $opt[od_period_cnt];
                             $subincen = $opt['od_total_incen'] / $opt[od_period_cnt];
                         } else {
                             $subprice = $opt['od_total_drug_price'];
                             $subincen = $opt['od_total_incen'];
                         }
                     }
                     ?>
                   <span class="price"><?php echo number_format($subprice/$opt['od_qty']); ?></span><span class="unit">&nbsp;원</span></span>
            </div>
                <div class="box-item">
                    <span class="label">수량</span>
                    <span class="pricebox"><span class="total-count"><?php echo  number_format($opt['od_qty']); ?></span><span class="unit">&nbsp;개</span></span>
                </div>
                <div class="box-item">
                    <span class="label">배송비</span>
                    <span class="pricebox">
                        <span class="total-count">
                             <?php if($od['od_company_yn'] == 'Y'){ ?>
                                 사내수령배송비할인
                             <?php } else{
                                 echo ($od['od_send_cost'] == 0) ? '무료' : number_format($od['od_send_cost']).'</span><span class="unit">&nbsp;원</span>';
                             }?>
                    </span>
                </div>
                <div class="box-item">
                    <span class="label">총금액</span>
                    <span class="pricebox"><span class="price"><?php
                            $t_price = ($subprice + $od['od_send_cost']);
                            echo number_format($t_price); ?></span><span class="unit">&nbsp;원</span></span>
                </div>
            <!-- 상품 END -->
        <?php
                $tot_point += $point;

                $st_count1++;
                if ($opt['od_status'] == '주문' || (($opt['od_status'] == '입금') && ($od['od_settle_case'] == '신용카드')))
                    $st_count2++;

                $od_pay_date = $opt['od_pay_date'];
            }
        }

        // 주문 상품의 상태가 모두 주문이면 고객 취소 가능
        if ($st_count1 > 0 && $st_count1 == $st_count2) $custom_cancel = true;
        ?>
        <div class="box-item blank-area"></div>
    </div>

    <script>
        $(document).on('click', '.accordion-title', function () {
        if ($(this).hasClass('is-opened')) {
            $(this).next('.accordion-content').slideUp(200);
            $(this).removeClass('is-opened');
        } else {
            $(this).next('.accordion-content').slideDown(200);
            $(this).addClass('is-opened');
        }
        });
    </script>
</div>

<?php
// 총계 = 주문상품금액합계 + 배송비 - 상품할인 - 결제할인 - 배송비할인
$tot_price = $od['od_cart_price'] + $od['od_send_cost'] + $od['od_send_cost2']
    - $od['od_cart_coupon'] - $od['od_coupon'] - $od['od_send_coupon']  - $od['od_receipt_point']
    - $od['od_cancel_price'];
$tot_drug_price = $od_dt['od_total_drug_price'] + $od['od_send_cost'] + $od['od_send_cost2']
    - $od['od_cart_coupon'] - $od['od_coupon'] - $od['od_send_coupon'] - $od['od_receipt_point']
    - $od['od_cancel_price'];

?>

<?php
$receipt_price = $od['od_receipt_price']
    + $od['od_receipt_point'];
$cancel_price = $od['od_cancel_price'];

$misu = true;
$misu_price = $tot_price - $receipt_price - $cancel_price;

if ($misu_price == 0 && ($od['od_cart_price'] > $od['od_cancel_price'])) {
    $wanbul = " (완불)";
    $misu = false; // 미수금 없음
}
else
{
    $wanbul = display_price($receipt_price);
}

// 결제정보처리
if($od['od_receipt_price'] > 0)
    $od_receipt_price = display_price($od['od_receipt_price']);
else
    $od_receipt_price = '입금 완료 후 결제일시가 표시됩니다.';

$app_no_subj = '';
$disp_bank = true;
$disp_receipt = false;
$easy_pay_name = '';
if($od['od_settle_case'] == '신용카드' || $od['od_settle_case'] == 'KAKAOPAY' || is_inicis_order_pay($od['od_settle_case']) ) {
    $app_no_subj = '승인번호';
    $app_no = $od['od_app_no'];
    $disp_bank = false;
    $disp_receipt = true;
} else if($od['od_settle_case'] == '간편결제') {
    $app_no_subj = '승인번호';
    $app_no = $od['od_app_no'];
    $disp_bank = false;
    switch($od['od_pg']) {
        case 'lg':
            $easy_pay_name = 'PAYNOW';
            break;
        case 'inicis':
            $easy_pay_name = 'KPAY';
            break;
        case 'kcp':
            $easy_pay_name = 'PAYCO';
            break;
        default:
            break;
    }
} else if($od['od_settle_case'] == '휴대폰') {
    $app_no_subj = '휴대폰번호';
    $app_no = $od['od_bank_account'];
    $disp_bank = false;
    $disp_receipt = true;
} else if($od['od_settle_case'] == '가상계좌' || $od['od_settle_case'] == '계좌이체') {
    $app_no_subj = '거래번호';
    $app_no = $od['od_tno'];
}

$od_data_check = true;
if ($od['od_status'] == '취소' || strpos($od['od_status'], '반품')) {
    if ($od_cancel['od_id']) {
        // 주문상태에 따른 값 처리
        $od_send_cost = $od_cancel['od_send_cost'];
        $od_send_cost2 = $od_cancel['od_send_cost2'];
        $tot_price = $od_cancel['od_receipt_price'];
        $od_coupon = $od_cancel['od_coupon'];
        $od_receipt_point = $od_cancel['od_receipt_point'];
    } else {
        // 과거 데이터는 취소 시 기존 데이터가 0 처리되어 확인이 불가
        $od_send_cost = $od['od_send_cost'];
        $od_send_cost2 = $od['od_send_cost2'];
        $tot_price = $od['od_cart_price'];
        $od_coupon = $od['od_coupon'];
        $od_receipt_point = $od['od_receipt_point'];
        $od_data_check = false;
    }
} else {
    $od_send_cost = $od['od_send_cost'];
    $od_send_cost2 = $od['od_send_cost2'];
    $od_coupon = $od['od_coupon'];
    $od_receipt_point = $od['od_receipt_point'];
}

?>

<?php
//배송완료인 상태는 반품이 가능하게끔 처리해주기
if ($order_receiver_cnt > 0) {

    $custom_cancel = true;
    $button_name = "반품 신청하기";
    $submit_url = G5_SHOP_URL."/orderinquiryreturn.php";
    $txt_1 = "주문반품";
    $txt_2 = "반품사유";
    $txt_3 = "<option value='기타' data-pay_yn='Y'>기타</option>";
    $txt_4 = "";

} else {

    $return_possible = "";
    $button_name = "주문 취소하기";
    $submit_url = G5_SHOP_URL."/orderinquirycancel.php";
    $txt_1 = "주문취소";
    $txt_2 = "취소사유";
    $txt_3 = "";
    $txt_4 = " display:none; ";

}
?>

<div class="border-top-style">
    <h3 class="title-box accordion-title">
        <span class="left-side title-main">배송지/결제 정보</span>
        <span class="right-side">
            <?php if ($cancel_price == 0 && $custom_cancel) { ?>
                <button type="button" class="btn-default btn-sm btn-white-border" onclick="fcancel_pop();"><?php echo $button_name; ?></button>
            <?php } else { ?>
                <p>주문 취소, 반품, 품절된 내역이 있습니다.</p>
            <?php } ?>
        </span>
    </h3>
    <div class="content-box ">
        <?php if ($od['od_multi_yn'] != "Y") { // 다중배송이 아닐때 ?>
        <div class="box-item row">
            <div class="tr">
                <span class="list-label">받으시는 분</span>
                <span class="list-desc"><?php echo get_text($od['od_b_name']); ?></span>
            </div>
            <div class="tr" style="display: none;">
                <span class="list-label">전화 번호</span>
                <span class="list-desc"><?php echo get_text($od['od_b_tel']); ?></span>
            </div>
            <div class="tr">
                <span class="list-label">휴대폰 번호</span>
                <span class="list-desc"><?php echo get_text($od['od_b_hp']); ?></span>
            </div>
            <div class="tr">
                <span class="list-label">배송 주소</span>
                <span class="list-desc">
                    <?php echo get_text(sprintf("(%s%s)", $od['od_b_zip1'], $od['od_b_zip2']).' '.print_address($od['od_b_addr1'], $od['od_b_addr2'], $od['od_b_addr3'], $od['od_b_addr_jibeon'])); ?>
                </span>
            </div>
            <div class="tr">
                <?php
                $sql = "select od_trans_memo from tbl_shop_order_receiver where od_id='{$od['od_id']}' limit 1";
                $fetch = sql_fetch($sql);
                $od_trans_memo = $fetch['od_trans_memo'];
                ?>
                <span class="list-label">배송 메시지</span>
                <span class="list-desc">
                    <?php echo get_text($od_trans_memo); ?>
                </span>
            </div>
        </div>
        <?php } else { // 다중배송일때
            $sql = " select a.comp_code, b.od_date3, a.od_send_yn , b.od_status, b.od_trans_memo, c.*
                            from tbl_shop_order_detail a, tbl_shop_order_receiver b, tbl_shop_order_multi_receiver c
                            where a.od_id = b.od_id and a.od_num= b.od_num and a.od_id = c.od_id and a.od_num= c.od_num and a.od_id = '$od_id' ";

            $res = sql_query($sql);
            for ($k = 0; $rs = sql_fetch_array($res); $k++) { ?>
            <div class="box-item row">
                <div class="tr">
                    <span class="list-label">받으시는 분</span>
                    <span class="list-desc"><?php echo get_text($rs['od_b_name']); ?></span>
                </div>
                <div class="tr">
                    <span class="list-label">휴대폰 번호</span>
                    <span class="list-desc"><?php echo get_text($rs['od_b_hp']); ?></span>
                </div>
                <div class="tr">
                    <span class="list-label">배송 주소</span>
                    <span class="list-desc">
                        <?php echo get_text(sprintf("(%s%s)", $rs['od_b_zip1'], $rs['od_b_zip2']).' '.print_address($rs['od_b_addr1'], $rs['od_b_addr2'], $rs['od_b_addr3'], $rs['od_b_addr_jibeon'])); ?>
                    </span>
                </div>
                <div class="tr">
                    <span class="list-label">배송 메시지</span>
                    <span class="list-desc">
                        <?php echo get_text($rs['od_trans_memo']); ?>
                    </span>
                </div>
                <!-- 모바일 배송정보 하단 버튼으로 이용하도록 숨김처리
                <div class="tr">
                    <span class="list-label">배송회사</span>
                    <span class="list-desc"><?php /*echo get_text($rs["od_delivery_company"]); */?></span>
                </div>
                <div class="tr">
                    <span class="list-label">운송장번호</span>
                    <span class="list-desc"><?php /*echo get_text($rs["od_invoice"]); */?></span>
                </div>
                -->
            </div>
            <?php }
        }
        ?>
        <div class="box-item row">
            <div class="tr">
                <span class="list-label">결제 수단</span>
                <span class="list-desc"><?php echo ($easy_pay_name ? $easy_pay_name.'('.$od['od_settle_case'].')' : check_pay_name_replace($od['od_settle_case'])); ?></span>
                <?php if ($od['od_settle_case'] == '신용카드') { ?>
                    <!--<span class="br">현대카드(1234-****-****-****)</span>-->
                    <!--<span class="br">일시불</span>-->
                <?php } ?>
            </div>
            <div class="tr">
                <span class="list-label">결제 일시</span>
                <span class="list-desc">
                    <?php
                    if ($od_pay_date != "" && $od_pay_date != "0000-00-00 00:00:00") {
                        $od_pay_date_str = $od_pay_date;
                    } else if ($od['od_receipt_time'] != "0000-00-00 00:00:00") {
                        $od_pay_date_str = $od['od_receipt_time'];
                    } else {
                        $od_pay_date_str = "입금확인시 노출됩니다.";
                    }

                    echo $od_pay_date_str;
                    ?>
                </span>
            </div>
            <?php if ($od['od_settle_case'] == '무통장' && $is_drug == false) { ?>
            <div class="tr">
                <span class="list-label">입금자명</span>
                <span class="list-desc"><?php echo $od['od_deposit_name']; ?></span>
            </div>
            <div class="tr">
                <span class="list-label">입금계좌</span>
                <span class="list-desc"><?php echo $od['od_bank_account']; ?></span>
            </div>
            <div class="tr">
                <span class="list-label">입금기한</span>
                <span class="list-desc"><?php echo $od['od_bank_date']; ?></span>
            </div>
            <?php } ?>
            <?php if ($disp_receipt) { // 영수증 출력 ?>
            <div class="tr">
                <span class="list-label">영수증</span>
                <span class="list-desc">
                    <?php
                    if($od['od_settle_case'] == '휴대폰')
                    {
                        if($od['od_pg'] == 'lg') {
                            require_once G5_SHOP_PATH.'/settle_lg.inc.php';
                            $LGD_TID      = $od['od_tno'];
                            $LGD_MERTKEY  = $config['cf_lg_mert_key'];
                            $LGD_HASHDATA = md5($LGD_MID.$LGD_TID.$LGD_MERTKEY);

                            $hp_receipt_script = 'showReceiptByTID(\''.$LGD_MID.'\', \''.$LGD_TID.'\', \''.$LGD_HASHDATA.'\');';
                        } else if($od['od_pg'] == 'inicis') {
                            $hp_receipt_script = 'window.open(\'https://iniweb.inicis.com/DefaultWebApp/mall/cr/cm/mCmReceipt_head.jsp?noTid='.$od['od_tno'].'&noMethod=1\',\'receipt\',\'width=430,height=700\');';
                        } else {
                            $hp_receipt_script = 'window.open(\''.G5_BILL_RECEIPT_URL.'mcash_bill&tno='.$od['od_tno'].'&order_no='.$od['od_id'].'&trade_mony='.$od['od_receipt_price'].'\', \'winreceipt\', \'width=500,height=690,scrollbars=yes,resizable=yes\');';
                        }
                        ?>
                        <a href="javascript:;" class="btn-sm btn-white-border" onclick="<?php echo $hp_receipt_script; ?>">영수증 출력</a>
                        <?php
                    }

                    if($od['od_settle_case'] == '신용카드' || is_inicis_order_pay($od['od_settle_case']) )
                    {
                        if($od['od_pg'] == 'lg') {
                            require_once G5_SHOP_PATH.'/settle_lg.inc.php';
                            $LGD_TID      = $od['od_tno'];
                            $LGD_MERTKEY  = $config['cf_lg_mert_key'];
                            $LGD_HASHDATA = md5($LGD_MID.$LGD_TID.$LGD_MERTKEY);

                            $card_receipt_script = 'showReceiptByTID(\''.$LGD_MID.'\', \''.$LGD_TID.'\', \''.$LGD_HASHDATA.'\');';
                        } else if($od['od_pg'] == 'inicis') {
                            $card_receipt_script = 'window.open(\'https://iniweb.inicis.com/DefaultWebApp/mall/cr/cm/mCmReceipt_head.jsp?noTid='.$od['od_tno'].'&noMethod=1\',\'receipt\',\'width=430,height=700\');';
                        } else {
                            $card_receipt_script = 'window.open(\''.G5_BILL_RECEIPT_URL.'card_bill&tno='.$od['od_tno'].'&order_no='.$od['od_id'].'&trade_mony='.$od['od_receipt_price'].'\', \'winreceipt\', \'width=470,height=815,scrollbars=yes,resizable=yes\');';
                        }
                        ?>
                        <? if( $ReceiptHtml == "" ){ ?>
                        <a href="javascript:;" class="btn-sm btn-white-border" onclick="<?php echo $card_receipt_script; ?>">영수증 출력</a>
                    <? }else{ ?>
                        <table>
                            <?=$ReceiptHtml?>
                        </table>
                    <? } ?>
                        <?php
                    }

                    if($od['od_settle_case'] == 'KAKAOPAY')
                    {
                        $card_receipt_script = 'window.open(\'https://mms.cnspay.co.kr/trans/retrieveIssueLoader.do?TID='.$od['od_tno'].'&type=0\', \'popupIssue\', \'toolbar=no,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=yes,width=420,height=540\');';
                        ?>
                        <a href="javascript:;" class="btn-sm btn-white-border" onclick="<?php echo $card_receipt_script; ?>">영수증 출력</a>
                        <?php
                    }
                    ?>
                </span>
            </div>
            <?php } ?>
        </div>
        <ul class="pay-result">
            <?php if ($od_data_check == true) { ?>
                <li>
                    <span class="label">소비자가</span>
                    <span class="pricebox">
                    <span class="price"><?php echo number_format($tot_cust_price); ?></span>
                    <span class="unit">원</span>
                </span>
                </li>
                <?php if ($tot_sale > 0) { ?>
                    <li>
                        <span class="label">상품 할인</span>
                        <span class="pricebox">
                <span class="price">- <?php echo number_format($tot_sale); ?></span>
                <span class="unit">원</span>
            </span>
                    </li>
                <?php } ?>
                <?php if ($od_coupon > 0) { ?>
                    <li>
                        <span class="label">쿠폰 할인</span>
                        <span class="pricebox">
                        <span class="price">- <?php echo number_format($od_coupon); ?></span>
                        <span class="unit">원</span>
                    </span>
                    </li>
                <?php } ?>
                <?php
                if($member['mb_referee'] == 4 || $member['mb_extra'] == 11){
                    $p_name = "적립금";
                }else{
                    $p_name = "포인트";
                }
                if ($od['od_receipt_point'] > 0) { ?>
                    <li>
                        <span class="label">사용 <?php echo $p_name ?></span>
                        <span class="pricebox">
                        <span class="price">- <?php echo number_format($od['od_receipt_point']); ?></span>
                        <span class="unit">원</span>
                    </span>
                    </li>
                <?php } ?>
                <?php if($member['mb_type']=='1') {?>
                    <li>
                        <span class="label">포인트 할인</span>
                        <span class="pricebox">
                            <span class="price">- <?php echo number_format($tot_price-$tot_drug_price); ?></span>
                            <span class="unit">원</span>
                        </span>
                    </li>
                <?php } ?>
            <?php } ?>
            <li>
                <span class="label">배송비</span>
                <span class="pricebox">
                    <span class="price">
                         <?php if($od['od_company_yn'] == 'Y'){ ?>
                             사내수령배송비할인
                         <?php } else{
                             echo ($od_send_cost == 0) ? '무료' : number_format($od_send_cost).'</span><span class="unit">&nbsp;원</span>';
                         }?>
                </span>
            </li>
            <?php if($member['mb_type']=='1' && $od['od_cancel_price'] == 0) {?>
                <li class="total">
                    <span class="label">총 약국 결제 금액</span>
                    <span class="pricebox">
                        <span class="price"><?php echo number_format($tot_drug_price); ?></span>
                        <span class="unit">원</span>
                    </span>
                </li>
            <?php } else { ?>
                <li class="total">
                    <span class="label">최종 결제 금액</span>
                    <span class="pricebox">
                    <span class="price"><?php echo number_format($tot_price); ?></span>
                    <span class="unit">원</span>
                </span>
                </li>
            <?php }  ?>
        </ul>
        <div class="btn-set btn-set-md">
            <button class="btn-default btn-white-border" type="button" onclick="location.href='/shop/orderinquiry.php';">주문 내역 조회</button>
            <button class="btn-default btn-orange" type="button" onclick="location.href='/';">쇼핑 계속하기</button>
        </div>
    </div>
</div>

<!-- 추가 0121 끝 -->

<script>

history.pushState(null, document.title, location.href);
window.addEventListener('popstate', function (event) {
    //history.pushState(null, document.title, '/shop');
    location.href = '<?php echo G5_SHOP_URL;?>'
});

$(function() {

    Modal_IframeClose();

    $("#sod_sts_explan_open").on("click", function() {
        var $explan = $("#sod_sts_explan");
        if($explan.is(":animated"))
            return false;

        if($explan.is(":visible")) {
            $explan.slideUp(200);
            $("#sod_sts_explan_open").text("상태설명보기");
        } else {
            $explan.slideDown(200);
            $("#sod_sts_explan_open").text("상태설명닫기");
        }
    });

    $("#sod_sts_explan_close").on("click", function() {
        var $explan = $("#sod_sts_explan");
        if($explan.is(":animated"))
            return false;

        $explan.slideUp(200);
        $("#sod_sts_explan_open").text("상태설명보기");
    });
    $("#return_explan_open").on("click", function() {
        var $explan = $("#return_explan");
        if($explan.is(":animated"))
            return false;

        if($explan.is(":visible")) {
            $explan.slideUp(200);
            $("#return_explan_open").text("반품반려 이유보기");
        } else {
            $explan.slideDown(200);
            $("#return_explan_open").text("반품반려 이유닫기");
        }
    });

    $("#return_explan_close").on("click", function() {
        var $explan = $("#return_explan");
        if($explan.is(":animated"))
            return false;

        $explan.slideUp(200);
        $("#return_explan_open").text("반품반려 이유보기");
    });

	<? if( $order_receiver_cnt > 0 ){ ?>

	$("#cancel_memo").change(function(){

		var selected = $(this).find('option:selected');
        var extra = selected.data('pay_yn'); 

		if ( extra == "Y" ){
			$("#return_text_info").css("display","");
			$("#return_method_info").css("display","");
		}else{
			$("#return_text_info").css("display","none");
			$("#return_method_info").css("display","none");		
			$("input:checkbox[id='return_check_yn']").prop("checked", false);
			$("input:radio[name='od_return_method']").prop("checked", false);
		}

		if( $(this).val() == "기타" ){
			$("#memo_space").css("display","");
		}else{
			$("#memo_space").css("display","none");		
			$("#client_memo").val("");
		}
	});

	<? } ?>

});

function fcancel_check(f)
{
	<?if( $order_receiver_cnt > 0 ){?>

        var $ct_chk = $("input[name^=return_od_num]");
        var chked_cnt = $ct_chk.filter(":checked").size();
		if( chked_cnt == 0 ){
			alert("반품하실 상품을 체크해주세요.");
			return false;
		}
		//체크한 상품 문자열 연결해서 넘기기
		var chk = $("input[name^=return_od_num]:checked").map(function() {
			return this.value;
		}).get().join("|");

		$("#return_od_num_check").val(chk);

		var selected = $("#cancel_memo").find('option:selected');
        var pay_yn = selected.data('pay_yn'); 

		var Msg_1 = "주문을 정말 반품하시겠습니까?";
		var Msg_2 = "반품사유를 입력해 주십시오.";

		if( f.return_check_yn.checked == false && pay_yn == "Y" ){
			alert("배송료 부담 확인여부를 체크해주세요.");
			return false;
		}
		if( pay_yn == "Y" && !document.getElementsByName("od_return_method")[0].checked && !document.getElementsByName("od_return_method")[1].checked ){
			alert("왕복 배송비 결제 방법을 선택해주세요.");
			return false;
		}
	<?}else{?>
		var Msg_1 = "주문을 정말 취소하시겠습니까?";
		var Msg_2 = "취소사유를 입력해 주십시오.";
	<?}?>
    if(!confirm(Msg_1))
        return false;

    var memo = f.cancel_memo.value;
    if(memo == "") {
        alert(Msg_2);
        return false;
    }

    return true;
}

function fcancel_pop() {
    var return_od_num_check = '';

    var order_receiver_cnt = 0;
    order_receiver_cnt = <?php echo ($order_receiver_cnt > 0) ? $order_receiver_cnt: 0; ?>;

    var od_status = '<?php echo $od['od_status']?>';

    if (order_receiver_cnt > 0) {
        var $ct_chk = $("input[name^=return_od_num]");
        var chked_cnt = $ct_chk.filter(":checked").size();
        if (chked_cnt == 0) {
            alert("반품하실 상품을 체크해주세요.");
            return false;
        }
        //체크한 상품 문자열 연결해서 넘기기
        var chk = $("input[name^=return_od_num]:checked").map(function () {
            return this.value;
        }).get().join("|");

        return_od_num_check = chk;
    } else {
        // 주문 취소의 경우
        if (od_status == '취소') {
            alert("이미 취소된 주문입니다.");
            return false;
        }
    }

    var params = "?od_id=<?php echo $od_id; ?>&order_receiver_cnt="+order_receiver_cnt+"&return_od_num_check="+return_od_num_check+"&return_url=orderinquiryview";

    <?php if (G5_IS_MOBILE) { ?>
    ModalPop('<?php echo G5_SHOP_URL; ?>/orderinquiryview.pop.php'+params,'','Y');
    <?php } else { ?>
    window.open('<?php echo G5_SHOP_URL; ?>/orderinquiryview.pop.php'+params, '주문취소', 'width=550,height=400,scrollbars=1,menus=0');
    <?php } ?>

}
</script>

<?php
include_once(G5_MSHOP_PATH.'/_tail.php');
?>
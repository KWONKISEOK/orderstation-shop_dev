<?php
include_once('./_common.php');

define("_MYUSER_", true);

//$od_pwd = get_encrypt_string($od_pwd);


$mb_name = $_POST['mb_name'];
$mb_id = $member['mb_id'];
// 회원인 경우
if ($is_member)
{
    $sql_common = " from {$g5['member_table']} where mb_recommend = '$mb_id ' ";
}
else // 그렇지 않다면 로그인으로 가기
{
    goto_url(G5_BBS_URL.'/login.php?url='.urlencode(G5_SHOP_URL.'/orderinquiry.php'));
}

if($mb_name) $sql_common .= " and mb_name like '%".$mb_name."%'";

// 테이블의 전체 레코드수만 얻음
$sql = " select count(*) as cnt " . $sql_common;
$row = sql_fetch($sql);
$total_count = $row['cnt'];

$rows = 10;//$config['cf_page_rows'];
$total_page  = ceil($total_count / $rows);  // 전체 페이지 계산
if ($page < 1) { $page = 1; } // 페이지가 없으면 첫 페이지 (1 페이지)
$from_record = ($page - 1) * $rows; // 시작 열을 구함

$g5['title'] = '고객관리';
include_once('./_head.php');

if($sdate == '') {
	$sdate = date( "Y-m" ).'-01';
	$edate = date( "Y-m-d" );
}


?>

<!-- 주문 내역 시작 { -->
<div id="sod_v">
    <p id="sod_v_info">고객정보를 조회 하실 수 있습니다.</p>

    <?php
    $limit = " limit $from_record, $rows ";
?>

<form name="frm1" method="post" action="/shop/myuserorder.php" style="margin:0">	
	<div style="margin:10px 0 10px 0;" ><a href="/shop/myuser.php" class=" btn02 ">고객조회</a> <a href="/shop/myuserorder.php" class=" btn02 ">고객주문조회</a></div>

	<div id="search-box">
		<span class="btndate" id="today">오늘</span>&nbsp;
		<span class="btndate" id="bday15">15일</span>&nbsp;
		<span class="btndate" id="bmonth1">1개월</span>&nbsp;
		<span class="btndate" id="bmonth3">3개월</span>&nbsp;&nbsp;
		<br/><br/>
          <input type="text" name="sdate" id="sdate" style="width:90px; text-align:center" class="datepickerbutton" value="<?php echo $sdate;?>" readonly>
          ~
          <input type="text" name="edate" id="edate" style="width:90px; text-align:center" class="datepickerbutton" value="<?php echo $edate;?>" readonly>
          <button class="btn03">조회</button>
	</div>

</form>

<style>
.btn03 {
    display: inline-block;
    padding: 5px;
    border: 1px solid #3b3c3f;
    background: #4b545e;
    color: #fff;
    text-decoration: none;
    vertical-align: middle;
}

.mypage .li_price_box {
    margin: 6px 6px;
    border-top: 1px solid #dcdcdc;
    padding: 10px 0 0;
	height:42px;
}
.mypage .prqty_price {
    border-right: 1px solid #dcdcdc;
	
}
.mypage .li_prqty_sp {
    float: left;
    width: 50%;
    display: block;
    line-height: 20px;
    padding: 0 7px;
    text-align: right;
}
.mypage .li_prqty_sp span {
    float: left;
}
</style>

<link rel="stylesheet" href="//code.jquery.com/ui/1.8.18/themes/base/jquery-ui.css" />
<script src="//code.jquery.com/ui/1.8.18/jquery-ui.min.js"></script>


<script>
    $.datepicker.setDefaults({
        dateFormat: 'yy-mm-dd',
        prevText: '이전 달',
        nextText: '다음 달',
        monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
        monthNamesShort: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
        dayNames: ['일', '월', '화', '수', '목', '금', '토'],
        dayNamesShort: ['일', '월', '화', '수', '목', '금', '토'],
        dayNamesMin: ['일', '월', '화', '수', '목', '금', '토'],
        showMonthAfterYear: true,
        yearSuffix: '년'
    });

    $(function() {
        $(".datepickerbutton").datepicker();
    });

	function setDate(kind) {
		var today = '<?php echo date("Y-m-d"); ?>';
		var bday15 = '<?php echo date("Y-m-d", strtotime('-15 days')); ?>';
		var bmonth1 = '<?php echo date("Y-m-d", strtotime('-1 month')); ?>';
		var bmonth3 = '<?php echo date("Y-m-d", strtotime('-3 month')); ?>';

		if(kind==1) {
			$("#sdate").val(today);
			$("#edate").val(today);
		}
		if(kind==2) {
			$("#sdate").val(bday15);
			$("#edate").val(today);
		}
		if(kind==3) {
			$("#sdate").val(bmonth1);
			$("#edate").val(today);
		}
		if(kind==4) {
			$("#sdate").val(bmonth3);
			$("#edate").val(today);
		}

	}
	$( "#today" ).click(function() {
	  setDate(1);
	});
	$( "#bday15" ).click(function() {
	  setDate(2);
	});
	$( "#bmonth1" ).click(function() {
	  setDate(3);
	});
	$( "#bmonth3" ).click(function() {
	  setDate(4);
	});

</script>

<div id="sod_inquiry">
    <ul class="mypage">
	<?php
    //$sql = " select * from tbl_member where mb_recommend='".$member['mb_id']."'";

	$sql = " select * $sql_common
              $limit ";

    $result = sql_query($sql);
    for ($i=0; $row=sql_fetch_array($result); $i++)
    {
        
	?>
    
    <li>

		<table style="margin:10px;">
	
		<tr>
			<td width="30%">고객명</td>
			<td><?php echo $row['mb_name']; ?></td>
		</tr>

		<tr>
			<td colspan="2">
			<div class="inquiry_inv">
				<?php echo $od_invoice; ?>
				<span class="inv_status"><?php echo $od_status; ?></span>
			</div>
			</td>
		</tr>
		</table>

		<div class="li_price_box">
			<span class="prqty_price li_prqty_sp"><span>주문건수 </span>0</span>
			<span class="li_prqty_sp"><span>주문금액 </span>0</span>
			<span class="prqty_price li_prqty_sp"><span>입금확인중 </span>0</span>
			<span class="li_prqty_sp"><span>결제완료 </span>0</span>
			

		</div>

       
    </li>
	<?php } ?>
  
   </ul>

</div>
<!-- } 주문 내역 목록 끝 -->

    <?php echo get_paging($config['cf_write_pages'], $page, $total_page, "{$_SERVER['SCRIPT_NAME']}?$qstr&amp;page="); ?>
</div>
<!-- } 주문 내역 끝 -->

<?php
include_once('./_tail.php');
?>

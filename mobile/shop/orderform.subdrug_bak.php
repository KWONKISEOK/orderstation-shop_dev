<?php
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가

$tablet_size = "1.0"; // 화면 사이즈 조정 - 기기화면에 맞게 수정(갤럭시탭,아이패드 - 1.85, 스마트폰 - 1.0)

// 개인결제번호제거
set_session('ss_personalpay_id', '');
set_session('ss_personalpay_hash', '');
?>

<div id="sod_approval_frm">
    <?php
    ob_start();
    ?>

    <ul class="sod_list">
        <?php

        $tot_point = 0;
        $tot_sell_price = 0;
        $tot_left_price = 0; //소비자가총계
        $tot_right_price = 0; //약국가총계

        $goods = $goods_it_id = "";
        $goods_count = -1;

        //장바구니 검사
        $result = c_mysqli_call('SP_ORDER_INFO_NEW_VERSION', " '".$s_cart_id."','ORDER','".$member['mb_type']."','".$period."','','' ");

        $good_info = '';
        $it_send_cost = 0;
        $it_cp_count = 0;

        $comm_tax_mny = 0; // 과세금액
        $comm_vat_mny = 0; // 부가세
        $comm_free_mny = 0; // 면세금액
        $tot_tax_mny = 0;
        $period='n';

        $price_change_yn = ""; //가격변동여부
        $TimeGoodsYn = "N"; //타임세일 제품이 있는지 여부

        $i=0;
        $TmpString="";
        $LeftSum=0; // 소비자가
        $RightSum=0; // 약국가
        $trans_memo = "";
        $multi_cnt = 0;

        foreach($result as $row) {

            //다중배송상품 계산
            if( $row[it_multisend] ){
                $multi_cnt++;
            }else{
                $multi_cnt--;
            }

            // 합계금액 계산
            $sql = " select SUM(IF(io_type = 1, (io_price * ct_qty), ((ct_price + io_price) * ct_qty))) as price,
                            SUM(ct_point * ct_qty) as point,
							SUM((select it_incen * a.ct_qty from tbl_shop_item where it_id = a.it_id))  as it_incen, 
                            SUM((select io_incen * a.ct_qty from tbl_shop_item_option where io_id = a.io_id limit 1) ) as io_incen, SUM(ct_qty) as qty,
                            SUM(ct_qty) as qty
                        from {$g5['g5_shop_cart_table']} a
                        where it_id = '{$row['it_id']}'
                          and od_id = '$s_cart_id' ";
            $sum = sql_fetch($sql);

            if (!$goods)
            {
                //$goods = addslashes($row[it_name]);
                //$goods = get_text($row[it_name]);
                $goods = preg_replace("/\?|\'|\"|\||\,|\&|\;/", "", $row['it_name']);
                $goods_it_id = $row['it_id'];
            }
            $goods_count++;

            // 에스크로 상품정보
            if($default['de_escrow_use']) {
                if ($i>0)
                    $good_info .= chr(30);
                $good_info .= "seq=".($i+1).chr(31);
                $good_info .= "ordr_numb={$od_id}_".sprintf("%04d", $i).chr(31);
                $good_info .= "good_name=".addslashes($row['it_name']).chr(31);
                $good_info .= "good_cntx=".$row['ct_qty'].chr(31);
                $good_info .= "good_amtx=".$row['ct_price'].chr(31);
            }

            $a1 = '<strong>';
            $a2 = '</strong>';
            $image_width = 80;
            $image_height = 80;
            $image = get_it_image($row['it_id'], $image_width, $image_height);

            $it_name = $a1 . stripslashes($row['it_name']) . $a2;
            $auto_order='';
            if($row['ct_period_yn'] == 'Y') {
                $auto_order= ' * 정기주문 시작:'.$row['ct_period_sdate'].'주기:'.$row['ct_period_week'].'횟수:'.$row['ct_period_cnt'];
                $period = 'y';//정기주문
                //정기주문 기간계산
                $sdate			= $row['ct_period_sdate'];
                $edate			= $row['ct_period_sdate'];
                $period_week	= $row['ct_period_week'];
                $period_cnt		= $row['ct_period_cnt'];

                for($i = 2; $i <= $period_cnt; $i++) {
                    $str_date = strtotime($edate);
                    if($period_week == '1') {
                        $str_date = strtotime($edate.'+7 days'); // +1 days로 하루를 더함
                    }
                    if($period_week == '2') {
                        $str_date = strtotime($edate.'+14 days');
                    }
                    if($period_week == '3') {
                        $str_date = strtotime($edate.'+21 days');
                    }
                    if($period_week == '4') {
                        $str_date = strtotime($edate.'+28 days');
                    }
                    if($period_week == '5') {
                        $str_date = strtotime("+".($i-1)." month",strtotime($row['ct_period_sdate']));
                    }
                    $edate =  date('Y-m-d',$str_date);
                    //echo $edate;
                }
                if($period_week == '5') {
                    $offerPeriod = str_replace('-','',$sdate) .'-'. str_replace('-','',$edate);
                    $auto_order= ' * 정기주문 : '. $offerPeriod.' <br> 주기:지정일마다 횟수:'.$row['ct_period_cnt'];
                }else{
                    $offerPeriod = str_replace('-','',$sdate) .'-'. str_replace('-','',$edate);
                    $auto_order= ' * 정기주문 : '. $offerPeriod.' <br> 주기:'.$row['ct_period_week'].'횟수:'.$row['ct_period_cnt'];
                }

            }

            $it_options = print_item_options_new($row['it_id'], $s_cart_id);
            if($it_options) {

                $TmpString=explode("|", $it_options);
                $LeftSum=$LeftSum+$TmpString[1];
                $RightSum=$RightSum+$TmpString[2];

                $it_name2 = '<div class="sod_opt">'.$TmpString[0].'</div>';
            }

            // 복합과세금액
            if($default['de_tax_flag_use']) {
                if($row['it_notax']) {
                    $comm_free_mny += $sum['price'];
                } else {
                    $tot_tax_mny += $sum['price'];
                }
            }

            //정기주문 예외처리 상품
            $optionprice = get_option_dis_price($row['it_id'], $row['io_id'],$row['io_type']);

            if($optionprice > 0) {
                $sell_price = $sum['qty'] * ($row['ct_price']+$optionprice);
            } else {
                if($period_cnt > 0) {
                    $sell_price = $sum['price'] * $period_cnt;
                    $incen_sum = ($sum['it_incen']+$sum['io_incen']) * $period_cnt;
                } else {
                    $sell_price = $sum['price'];
                    $incen_sum = $sum['it_incen']+$sum['io_incen'];
                }
            }








            // 쿠폰
            if($is_member) {
                $cp_button = '';
                $cp_count = 0;

                $sql = " select cp_id
                            from {$g5['g5_shop_coupon_table']}
                            where mb_id IN ( '{$member['mb_id']}', '약국회원' )
                              and cp_start <= '".G5_TIME_YMD."'
                              and cp_end >= '".G5_TIME_YMD."'
                              and cp_minimum <= '$sell_price'
                              and (
                                    ( cp_method = '0' and cp_target = '{$row['it_id']}' )
                                    OR
                                    ( cp_method = '1' and ( cp_target IN ( '{$row['ca_id']}', '{$row['ca_id2']}', '{$row['ca_id3']}' ) ) )
                                  ) and service_type is null ";

                $res = sql_query($sql);

                for($k=0; $cp=sql_fetch_array($res); $k++) {
                    if(is_used_coupon($member['mb_id'], $cp['cp_id']))
                        continue;

                    $cp_count++;
                }

                if($cp_count) {
                    $cp_button = '<div class="li_cp"><button type="button" class="cp_btn">쿠폰적용</button></div>';
                    $it_cp_count++;
                }
            }

            // 배송비
            /*
            switch($row['ct_send_cost'])
            {
                case 1:
                    $ct_send_cost = '착불';
                    break;
                case 2:
                    $ct_send_cost = '무료';
                    break;
                default:
                    $ct_send_cost = '주문시결제';
                    break;
            }
            */
            if( $row['cal_it_sc_price'] == 0 ){
                $ct_send_cost = '무료';
            }else{
                $ct_send_cost = '주문시결제';
            }

            $sendcost = $row['cal_it_sc_price'];

            //배송비업데이트
            if($sendcost>=0) {
                $sql = " update tbl_shop_cart set it_sc_cost = '$sendcost' where ct_id='{$row['ct_id']}' ";
                $rtncost = sql_fetch($sql);
            }
            $total_cost = $total_cost + $sendcost;




            ?>

            <li class="sod_li">
                <input type="hidden" name="it_id[<?php echo $i; ?>]"    value="<?php echo $row['it_id']; ?>">
                <input type="hidden" name="it_name[<?php echo $i; ?>]"  value="<?php echo get_text($row['it_name']); ?>">
                <input type="hidden" name="it_price[<?php echo $i; ?>]" value="<?php echo $sell_price; ?>">
                <?php if($default['de_tax_flag_use']) { ?>
                    <input type="hidden" name="it_notax[<?php echo $i; ?>]" value="<?php echo $row['it_notax']; ?>">
                <?php } ?>
                <input type="hidden" name="cp_id[<?php echo $i; ?>]" value="">
                <input type="hidden" name="cp_price[<?php echo $i; ?>]" value="0">
                <div class="li_name">
                    <?php echo $it_name; ?>
                </div>
                <div class="li_op_wr">
                    <span class="total_img"><?php echo $image; ?></span>
                    <div class="sod_opt"><?php echo $it_name2; ?></div>
                    <div class="li_mod" >
                        <?php echo $cp_button; ?>
                        <?if( $row['price_change_yn_sum'] != 0 ){?>
                            <div style='margin-top:5px;'><font color='red'>가격변동상품</font></div>
                        <?}?>
                        <? if( $row['it_type4'] == "1" && $time_sale_yn == "N" ){ ?>
                            <div style='margin-top:5px;'><font color='blue'>타임세일상품</font></div>
                        <? } ?>
                    </div>
                </div>

                <div class="li_prqty">
                    <span class="prqty_price li_prqty_sp"><span>소비자가 </span>
                        <?php if ($row['price_change_yn_sum'] != 0) { ?>
                        <strike>
                            <?php } ?>
                            <?php echo number_format($LeftSum); ?>
                            <?php if( $row['price_change_yn_sum'] != 0 ) { ?>
                        </strike>
                    <?php } ?>
                    </span>
                    <span class="prqty_qty li_prqty_sp"><span>수량 </span><?php echo number_format($sum['qty']); ?></span>
                    <span class="prqty_sc li_prqty_sp"><span>배송비 </span><?php echo $ct_send_cost; ?>(<?php echo number_format($sendcost); ?>)</span>
                    <span class="total_point li_prqty_sp">
                        <span>
                            <?php if($member['mb_type']==1) { ?>
                            적립포인트
                        </span>
                        <strong><?php echo number_format($sum['it_incen']+$sum['io_incen']); ?></strong>
                    </span>
                    <?php } ?>

                </div>
                <div class="total_price total_span" style="height: 52px;">
                    <span>주문금액</span>
                    <strong><?php echo number_format($LeftSum + $sendcost); ?></strong>
                    <?php if($member['mb_type']==1) { ?>
                        <br/>
                        <span style='font-size:0.98em;float: right;'><?php echo number_format($RightSum+$sendcost); ?></span>
                        <span style='font-size:0.98em;font-weight: bold;color:#ff006c;float: right;'>(약국)&nbsp;</span>
                    <?php } ?>
                </div>

            </li>

            <?php
            $tot_left_price += $LeftSum;
            $tot_right_price += $RightSum;
            $tot_point      += $point;
            $tot_sell_price += $sell_price;
            $RightSum = 0;
            $LeftSum = 0;
            $trans_memo = "";

            //가격변동 체크여부
            if( $row['price_change_yn_sum'] != 0 ){
                $price_change_yn = "Y";
            }
            //타임세일상품 체크여부
            if( $row['it_type4'] == "1" && $time_sale_yn == "N" ){
                $TimeGoodsYn = "Y";
            }

            $i++;
        } // for 끝

        if ($i == 0) {
            //echo '<li class="empty_li">장바구니에 담긴 상품이 없습니다.</li>';
            alert('장바구니가 비어 있습니다.', G5_SHOP_URL.'/cart.php');
        } else {
            // 배송비 계산
            //$send_cost = get_sendcost($s_cart_id);
        }

        // 복합과세처리
        if($default['de_tax_flag_use']) {
            $comm_tax_mny = round(($tot_tax_mny + $total_cost) / 1.1);
            $comm_vat_mny = ($tot_tax_mny + $total_cost) - $comm_tax_mny;
        }
        ?>
    </ul>

    <?php if ($goods_count) $goods .= ' 외 '.$goods_count.'건'; ?>


    <!-- 주문상품 합계 시작 { -->
    <div class="sod_ta_wr">
        <dl id="m_sod_bsk_tot">
            <dt class="sod_bsk_sell">소계</dt>
            <dd class="sod_bsk_sell"><strong><?php echo number_format($tot_left_price); ?> 원</strong></dd>
            <?php if($it_cp_count > 0) { ?>
                <dt class="sod_bsk_coupon">쿠폰</dt>
                <dd class="sod_bsk_coupon"><strong id="ct_tot_coupon">0 원</strong></dd>
            <?php } ?>
            <dt class="sod_bsk_dvr">배송비</dt>
            <dd class="sod_bsk_dvr"><strong><?php echo number_format($total_cost); ?> 원</strong></dd>
            <!--
            <dt class="sod_bsk_point">포인트</dt>
            <dd class="sod_bsk_point"><strong>
			<?php if($member['mb_type']==1) { ?>
				<?php echo number_format($incen_total); ?>
				<?php } ?>
			 </strong></dd>
			-->
            <dt class="sod_bsk_cnt">총계</dt>
            <dd class="sod_bsk_cnt">
                <?php
                $tot_price = $tot_sell_price + $total_cost;
                ?>
                <strong id="ct_tot_price"><?php echo number_format($tot_left_price+$total_cost); ?></strong> 원
                <?php if($member['mb_type']==1) { ?>
                    <br/>
                    <span style='font-size:0.98em;float: right;'><?php echo number_format($tot_right_price+$total_cost); ?> 원</span>
                    <span style='font-size:0.98em;font-weight: bold;color:#ff006c;float: right;'>(약국)&nbsp;</span>
                <?php } ?>
            </dd>
        </dl>
    </div>



    <!-- } 주문상품 합계 끝 -->

    <?php
    $content = ob_get_contents();
    ob_end_clean();

    ?>
</div>

<div id="sod_frm">
    <form name="forderform" method="post" action="<?php echo $order_action_url; ?>" autocomplete="off">
        <input type="hidden" name="od_price"    value="<?php echo $tot_sell_price; ?>">
        <input type="hidden" name="org_od_price"    value="<?php echo $tot_sell_price; ?>">
        <input type="hidden" name="od_send_cost" value="<?php echo $total_cost; ?>">
        <input type="hidden" name="od_send_cost2" value="0">
        <input type="hidden" name="item_coupon" value="0">
        <input type="hidden" name="od_coupon" value="0">
        <input type="hidden" name="od_send_coupon" value="0">

        <?php echo $content; ?>

        <section id="sod_frm_orderer" >
            <table>
                <tr>
                    <td width="150">
                        <h2>주문하시는 분</h2>
                    </td>
                    <?php if($member['mb_type']==1) { ?>
                        <td>
                            <!-- <a href="<?php echo G5_SHOP_URL;?>/orderuser.php" id="order_user" class="btn_frmline">고객주문</a> -->

                            <div id="radio" class="ui-buttonset">
                                <input type="radio" id="od_gubun1" name="od_gubun" onclick="myaddr(1);od_gubun_chg1();" value="1" ><label for="od_gubun1" ><span class="">고객주문</span></label>
                                <input type="radio" id="od_gubun2" name="od_gubun" value="2" onclick="od_gubun_chg2()" checked ><label for="od_gubun2" ><span class="">약국주문</span></label>

                            </div>
                        </td>
                    <?php } else { ?>
                        <td> <input type="hidden" name="od_gubun" value="3"> </td>
                    <?php }  ?>

                </tr>
            </table>


            <div class="odf_list">
                <div class = "info_table">
                    <dl>
                        <li>
                            <dt>
                                <label for="od_name">이름<strong class="sound_only"> 필수</strong></label>
                            </dt>
                            <dd>
                                <input type="text" name="od_name" value="<?php echo get_text($member['mb_name']); ?>" id="od_name" required class="frm_input required" maxlength="20">
                            </dd>
                        </li>

                        <?php if (!$is_member) { // 비회원이면 ?>
                            <li>
                                <label for="od_pwd">비밀번호<strong class="sound_only"> 필수</strong></label>

                                <input type="password" name="od_pwd" id="od_pwd" required class="frm_input required" maxlength="20">
                                영,숫자 3~20자 (주문서 조회시 필요)

                            </li>
                        <?php } ?>

                        <dt>
                            <label for="od_hp">핸드폰</label>
                        </dt>
                        <dd>
                            <input type="text" name="od_hp" value="<?php echo get_text($member['mb_hp']); ?>" id="od_hp" required class="frm_input required" maxlength="20">
                        </dd>
                        </li>
                        <li>
                            <dt><strong>주소</strong></dt>
                            <dd>
                                <span class="add_num"><label for="od_zip" class="sound_only">우편번호<strong class="sound_only"> 필수</strong></label>
                                    <input type="text" name="od_zip" value="<?php echo $member['mb_zip1'].$member['mb_zip2']; ?>" id="od_zip" required class="frm_input required" size="5" maxlength="6" readonly style="width:60%">
                                    <button type="button" class="btn_frmline btn_addsch" onclick="win_zip('forderform', 'od_zip', 'od_addr1', 'od_addr2', 'od_addr3', 'od_addr_jibeon');">주소검색</button></span>
                                <label for="od_addr1" class="sound_only">기본주소<strong class="sound_only"> 필수</strong></label>
                                <input type="text" name="od_addr1" value="<?php echo get_text($member['mb_addr1']) ?>" id="od_addr1" required class="frm_input frm_address required">
                                <label for="od_addr2" class="sound_only">상세주소</label>
                                <input type="text" name="od_addr2" value="<?php echo get_text($member['mb_addr2']) ?>" id="od_addr2" class="frm_input frm_address">
                                <label for="od_addr3" class="sound_only">참고항목</label>
                                <input type="text" name="od_addr3" value="<?php echo get_text($member['mb_addr3']) ?>" id="od_addr3" class="frm_input frm_address" readonly="readonly">
                                <input type="hidden" name="od_addr_jibeon" value="<?php echo get_text($member['mb_addr_jibeon']); ?>"><br>
                            </dd>
                        </li>
                        <li>
                            <dt>
                                <label for="od_email">E-mail<strong class="sound_only"> 필수</strong></label>
                            </dt>
                            <dd>
                                <input type="email" name="od_email" value="<?php echo $member['mb_email']; ?>" id="od_email" required class="frm_input required" maxlength="100">
                            </dd>
                        </li>

                        <?php if ($default['de_hope_date_use']) { // 배송희망일 사용 ?>
                            <li>
                                <label for="od_hope_date">희망배송일</label>
                                <!-- <select name="od_hope_date" id="od_hope_date">
							<option value="">선택하십시오.</option>
							<?php
                                for ($i=0; $i<7; $i++) {
                                    $sdate = date("Y-m-d", time()+86400*($default['de_hope_date_after']+$i));
                                    echo '<option value="'.$sdate.'">'.$sdate.' ('.get_yoil($sdate).')</option>'.PHP_EOL;
                                }
                                ?>
							</select> -->
                                <input type="text" name="od_hope_date" value="" id="od_hope_date" required class="frm_input required" size="11" maxlength="10" readonly> 이후로 배송 바랍니다.

                            </li>
                        <?php } ?>
                    </dl>
                </div>
            </div>
        </section>

        <section id="sod_frm_taker">
            <h2>받으시는 분
                <span style="margin-left: 15px; font-size: 0.9em;">
                    <?php if($member['mb_type']==1 && $multi_cnt == 1) { ?>
                    <input type="checkbox" name="multiYN" id="multiYN" value="Y" style="margin-right: 3px;"/>
                    <label for="multiYN">다중배송</label>
                    <span id="mylist_button" style="display:none;">
                        <button type="button" class="btn_frmline btn_addsch" onclick="javascript:myaddr2(2);">나의고객리스트</button>
                        <input type="hidden" name="order_qty" id="order_qty" value="<?=$sum['qty']?>"/>
                        <?php } ?>
                    </span>
            </h2>

            <div class="tbl_receiver_content_1" style="overflow:auto;">

                <span class="tbl_head03 tbl_wrap" id="multi_receiver_list" style="display:none;">
                    <table border="1" id="multi_table">
                        <thead>
                        <tr>
                            <th scope="col" style="width:30px;" align="Center"><input type="checkbox" name="all_check"/></th>
                            <th scope="col" style="width:68px;" align="Center">상품수량입력</th>
                            <th scope="col" style="text-align: center;">배송정보</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                    <br>
                    <center><input type="button" value="선택삭제" onclick="javascript:MultiDel();" class="btn_submit" style="width:70px;height:30px;"/></center>
                </span>

            </div>

            <div class="tbl_receiver_content_2">
                <div class="odf_list">
                    <div class="info_table">
                        <dl>
                            <?php
                            if($is_member) {
                                // 배송지 이력
                                $addr_list = '';
                                $sep = chr(30);

                                // 주문자와 동일
                                $addr_list .= '<input type="radio" name="ad_sel_addr" value="same" id="ad_sel_addr_same">'.PHP_EOL;
                                $addr_list .= '<label for="ad_sel_addr_same">주문자와 동일</label>'.PHP_EOL;

                                // 기본배송지
                                $sql = " select *
                                from {$g5['g5_shop_order_address_table']}
                                where mb_id = '{$member['mb_id']}'
                                  and ad_default = '1' ";
                                $row = sql_fetch($sql);
                                if($row['ad_id']) {
                                    $val1 = $row['ad_name'].$sep.$row['ad_tel'].$sep.$row['ad_hp'].$sep.$row['ad_zip1'].$sep.$row['ad_zip2'].$sep.$row['ad_addr1'].$sep.$row['ad_addr2'].$sep.$row['ad_addr3'].$sep.$row['ad_jibeon'].$sep.$row['ad_subject'];
                                    $addr_list .= '<br><input type="radio" name="ad_sel_addr" value="'.get_text($val1).'" id="ad_sel_addr_def">'.PHP_EOL;
                                    $addr_list .= '<label for="ad_sel_addr_def">기본배송지</label>'.PHP_EOL;
                                }

                                // 최근배송지
                                $sql = " select *
                                from {$g5['g5_shop_order_address_table']}
                                where mb_id = '{$member['mb_id']}'
                                  and ad_default = '0'
                                order by ad_id desc
                                limit 1 ";
                                $result = sql_query($sql);
                                for($i=0; $row=sql_fetch_array($result); $i++) {
                                    $val1 = $row['ad_name'].$sep.$row['ad_tel'].$sep.$row['ad_hp'].$sep.$row['ad_zip1'].$sep.$row['ad_zip2'].$sep.$row['ad_addr1'].$sep.$row['ad_addr2'].$sep.$row['ad_addr3'].$sep.$row['ad_jibeon'].$sep.$row['ad_subject'];
                                    $val2 = '<label for="ad_sel_addr_'.($i+1).'">최근배송지('.($row['ad_subject'] ? $row['ad_subject'] : $row['ad_name']).')</label>';
                                    $addr_list .= '<br><input type="radio" name="ad_sel_addr" value="'.get_text($val1).'" id="ad_sel_addr_'.($i+1).'"> '.PHP_EOL.$val2.PHP_EOL;
                                }

                                $addr_list .= '<br><input type="radio" name="ad_sel_addr" value="new" id="od_sel_addr_new">'.PHP_EOL;
                                $addr_list .= '<label for="od_sel_addr_new">신규배송지</label>'.PHP_EOL;

                                $addr_list .='<br><a href="javascript:myaddr(2);" id="order_address" class="btn_frmline" >나의고객리스트</a>';

                                //onclick="myaddr(1);
                            } else {
                                // 주문자와 동일
                                $addr_list .= '<input type="checkbox" name="ad_sel_addr" value="same" id="ad_sel_addr_same">'.PHP_EOL;
                                $addr_list .= '<label for="ad_sel_addr_same">주문자와 동일</label>'.PHP_EOL;
                            }
                            ?>
                            <li class="dlv_slt">
                                <div><?php echo $addr_list; ?></div>
                            </li>
                            <?php if($is_member) { ?>
                                <dt style="width:14%">
                                    <label for="ad_subject">배송지명</label>
                                </dt>
                                <dt> <input type="text" name="ad_subject" id="ad_subject" class="frm_input" maxlength="20"></dt>
                                <input type="checkbox" name="ad_default" id="ad_default" value="1"  style="margin-left:10px;">
                                <label for="ad_default" class="ad_default">기본배송지로 설정</label>


                                <?php
                            }
                            ?>
                            <li>
                                <dt>
                                    <label for="od_b_name">이름<strong class="sound_only"> 필수</strong></label>
                                </dt>
                                <dd>
                                    <input type="text" name="od_b_name" id="od_b_name" required class="frm_input required" maxlength="20">
                                </dd>
                            </li>

                            <li>
                                <dt>
                                    <label for="od_b_hp">핸드폰</label>
                                </dt>
                                <dd>
                                    <input type="text" name="od_b_hp" id="od_b_hp" required class="frm_input required" maxlength="20">
                                </dd>
                            </li>
                            <li>
                                <dt><strong>주소</strong></dt>
                                <dd>
                                    <label for="od_b_zip" class="sound_only">우편번호<strong class="sound_only"> 필수</strong></label>
                                    <span class="add_num"><input type="text" name="od_b_zip" id="od_b_zip" required class="frm_input required" size="5" maxlength="6" readonly style="width:60%">
                                        <button type="button" class="btn_frmline  btn_addsch" onclick="win_zip('forderform', 'od_b_zip', 'od_b_addr1', 'od_b_addr2', 'od_b_addr3', 'od_b_addr_jibeon');">주소 검색</button></span>
                                    <label for="od_b_addr1" class="sound_only">기본주소<strong class="sound_only"> 필수</strong></label>
                                    <input type="text" name="od_b_addr1" id="od_b_addr1" required class="frm_input frm_address required" readonly>
                                    <label for="od_b_addr2" class="sound_only">상세주소</label>
                                    <input type="text" name="od_b_addr2" id="od_b_addr2" class="frm_input frm_address">
                                    <label for="od_b_addr3" class="sound_only">참고항목</label>
                                    <input type="text" name="od_b_addr3" id="od_b_addr3" class="frm_input frm_address" readonly="readonly">
                                    <input type="hidden" name="od_b_addr_jibeon" value="">
                                </dd>
                            </li>
                            <li style="display:none;">
                                <label for="od_memo">전하실 말씀</label>
                                <textarea name="od_memo" id="od_memo"></textarea>
                            </li>
                    </div>
                    </dl>
                </div>
            </div>

        </section>




        <?php
        $oc_cnt = $sc_cnt = 0;
        if($is_member) {
            // 주문쿠폰
            $sql = " select cp_id
                    from {$g5['g5_shop_coupon_table']}
                    where mb_id IN ( '{$member['mb_id']}', '소비자회원' )
                      and cp_method = '2'
                      and cp_start <= '".G5_TIME_YMD."'
                      and cp_end >= '".G5_TIME_YMD."'
                      and cp_minimum <= '$tot_sell_price' and service_type is null ";
            $res = sql_query($sql);

            for($k=0; $cp=sql_fetch_array($res); $k++) {
                if(is_used_coupon($member['mb_id'], $cp['cp_id']))
                    continue;

                $oc_cnt++;
            }

            if($total_cost > 0) {
                // 배송비쿠폰
                $sql = " select cp_id
                        from {$g5['g5_shop_coupon_table']}
                        where mb_id IN ( '{$member['mb_id']}', '소비자회원' )
                          and cp_method = '3'
                          and cp_start <= '".G5_TIME_YMD."'
                          and cp_end >= '".G5_TIME_YMD."'
                          and cp_minimum <= '$tot_sell_price' and service_type is null ";
                $res = sql_query($sql);

                for($k=0; $cp=sql_fetch_array($res); $k++) {
                    if(is_used_coupon($member['mb_id'], $cp['cp_id']))
                        continue;

                    $sc_cnt++;
                }
            }
        }
        ?>

        <section id="sod_frm_pay">
            <h2>결제정보 입력</h2>


            <div class="odf_tbl">
                <table>
                    <tbody>
                    <?php if($oc_cnt > 0) { ?>
                        <tr>
                            <th scope="row">주문할인쿠폰</th>
                            <td>
                                <input type="hidden" name="od_cp_id" value="">
                                <button type="button" id="od_coupon_btn" class="cp_btn1">쿠폰적용</button>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">주문할인금액</th>
                            <td><span id="od_cp_price">0</span>원</td>
                        </tr>
                    <?php } ?>
                    <?php if($sc_cnt > 0) { ?>
                        <tr>
                            <th scope="row">배송비할인쿠폰</th>
                            <td>
                                <input type="hidden" name="sc_cp_id" value="">
                                <button type="button" id="sc_coupon_btn" class="cp_btn1">쿠폰적용</button>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">배송비할인금액</th>
                            <td><span id="sc_cp_price">0</span>원</td>
                        </tr>
                    <?php } ?>
                    <tr>
                        <th>총 주문금액</th>
                        <td>
                            <?php
                            if($period=='y') {
                                if($optionprice > 0) {
                                    $tot_price = $tot_sell_price + $total_cost;
                                } else {
                                    //$tot_price = ($tot_sell_price*$period_cnt) + $total_cost;
                                    $tot_price = ($tot_sell_price) + $total_cost;
                                }
                            }
                            ?>
                            <span id="od_tot_price" style="padding: 0;"><?php echo number_format($tot_price); ?></span> 원
                            <div style="margin-top:-10px;">
                                <span style="font-size:0.98em;font-weight: bold;color:#ff006c;">(약국)</span>
                                <span style="float:right;font-size:0.98em;">&nbsp;<? echo number_format($tot_right_price+$total_cost); ?> 원</span>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>추가배송비</th>
                        <td><span id="od_send_cost2">0</span>원 (지역에 따라 추가되는 도선료 등의 배송비입니다.)</td>
                    </tr>
                    </tbody>
                </table>
            </div>


            <?php

            $temp_point = 0;
            // 회원이면서 포인트사용이면
            if ($is_member && $config['cf_use_point'])
            {
                // 포인트 결제 사용 포인트보다 회원의 포인트가 크다면
                if ($member['mb_point'] >= $default['de_settle_min_point'])
                {
                    $temp_point = (int)$default['de_settle_max_point'];

                    if($temp_point > (int)$tot_sell_price)
                        $temp_point = (int)$tot_sell_price;

                    if($temp_point > (int)$member['mb_point'])
                        $temp_point = (int)$member['mb_point'];

                    $point_unit = (int)$default['de_settle_point_unit'];
                    $temp_point = (int)((int)($temp_point / $point_unit) * $point_unit);

                    echo '<div class="sod_frm_point" style="display:none;">';
                    echo '<div><input type="hidden" name="max_temp_point" value="'.$temp_point.'"><label for="od_temp_point">사용 포인트('.$point_unit.'점 단위)</label> <input type="text" id="od_temp_point" name="od_temp_point" value="0" size="10"> 점</div>';
                    echo '<div id="sod_frm_pt_info"><span><strong>보유포인트</strong>'.display_point($member['mb_point']).'</span><span class="max_point_box"><strong>최대사용가능포인트</strong><em id="use_max_point">'.display_point($temp_point).'</em></span></div>';
                    echo '</div>';
                    $multi_settle++;
                }
            }
            ?>

            <input type="hidden" id="od_settle_bank" name="od_settle_case" checked value="무통장">
            <input type="hidden" name="od_bank_account" value="약국주문">
            <input type="hidden" name="od_deposit_name" value="약국주문">

        </section>

        <?php if($default['de_tax_flag_use']) { ?>
            <input type="hidden" name="comm_tax_mny"      value="<?php echo $comm_tax_mny; ?>">         <!-- 과세금액    -->
            <input type="hidden" name="comm_vat_mny"      value="<?php echo $comm_vat_mny; ?>">         <!-- 부가세     -->
            <input type="hidden" name="comm_free_mny"     value="<?php echo $comm_free_mny; ?>">        <!-- 비과세 금액 -->
        <?php } ?>

        <?
        //가격변동이 없을때 주문할수 있음..
        if( $price_change_yn != "Y" ){
            ?>
            <div id="display_pay_button" class="btn_confirm">
                <span id="show_pay_btn"><input type="button" onClick="forderform_check();" value="주문하기" class="btn_submit"></span>
                <a href="<?php echo G5_SHOP_URL; ?>" class="btn_cancel">취소</a>
            </div>
        <?}else{

            if( $price_change_yn == "Y" ){
                $ViewMsg_1 = "alert('가격변동이 있는 상품이 있습니다.가격변동상품을 삭제후 다시 주문하십시요.');";
            }
            if( $TimeGoodsYn == "Y" ){
                $ViewMsg_2 = "alert('타임세일 상품은 세일 시간에만 구매 가능합니다.');";
            }

            ?>
            <div id="display_pay_button" class="btn_confirm">
                <span id="show_pay_btn"><input type="button" onClick="<?=$ViewMsg_1?><?=$ViewMsg_2?>" value="주문하기" class="btn_submit"></span>
                <a href="<?php echo G5_SHOP_URL; ?>" class="btn_cancel">취소</a>
            </div>
        <? } ?>


</div>

<script>
    var zipcode = "";

    $(function() {
        var $cp_btn_el;
        var $cp_row_el;

        $(".cp_btn").click(function() {
            $cp_btn_el = $(this);
            $cp_row_el = $(this).closest("li");
            $("#cp_frm").remove();
            var it_id = $cp_btn_el.closest("li").find("input[name^=it_id]").val();

            $.post(
                "./orderitemcoupon.php",
                { it_id: it_id,  sw_direct: "<?php echo $sw_direct; ?>" },
                function(data) {
                    $cp_btn_el.after(data);
                }
            );
        });

        $(document).on("click", ".cp_apply", function() {
            var $el = $(this).closest("tr");
            var cp_id = $el.find("input[name='f_cp_id[]']").val();
            var price = $el.find("input[name='f_cp_prc[]']").val();
            var subj = $el.find("input[name='f_cp_subj[]']").val();
            var sell_price;

            if(parseInt(price) == 0) {
                if(!confirm(subj+"쿠폰의 할인 금액은 "+price+"원입니다.\n쿠폰을 적용하시겠습니까?")) {
                    return false;
                }
            }

            // 이미 사용한 쿠폰이 있는지
            var cp_dup = false;
            var cp_dup_idx;
            var $cp_dup_el;
            $("input[name^=cp_id]").each(function(index) {
                var id = $(this).val();

                if(id == cp_id) {
                    cp_dup_idx = index;
                    cp_dup = true;
                    $cp_dup_el = $(this).closest("li");;

                    return false;
                }
            });

            if(cp_dup) {
                var it_name = $("input[name='it_name["+cp_dup_idx+"]']").val();
                if(!confirm(subj+ "쿠폰은 "+it_name+"에 사용되었습니다.\n"+it_name+"의 쿠폰을 취소한 후 적용하시겠습니까?")) {
                    return false;
                } else {
                    coupon_cancel($cp_dup_el);
                    $("#cp_frm").remove();
                    $cp_dup_el.find(".cp_btn").text("쿠폰적용").removeClass("cp_mod").focus();
                    $cp_dup_el.find(".cp_cancel").remove();
                }
            }

            var $s_el = $cp_row_el.find(".total_price strong");;
            sell_price = parseInt($cp_row_el.find("input[name^=it_price]").val());
            sell_price = sell_price - parseInt(price);
            if(sell_price < 0) {
                alert("쿠폰할인금액이 상품 주문금액보다 크므로 쿠폰을 적용할 수 없습니다.");
                return false;
            }
            $s_el.text(number_format(String(sell_price)));
            $cp_row_el.find("input[name^=cp_id]").val(cp_id);
            $cp_row_el.find("input[name^=cp_price]").val(price);

            calculate_total_price();
            $("#cp_frm").remove();
            $cp_btn_el.text("변경").addClass("cp_mod").focus();
            if(!$cp_row_el.find(".cp_cancel").size())
                $cp_btn_el.after("<button type=\"button\" class=\"cp_cancel\">취소</button>");
        });

        $(document).on("click", "#cp_close", function() {
            $("#cp_frm").remove();
            $cp_btn_el.focus();
        });

        $(document).on("click", ".cp_cancel", function() {
            coupon_cancel($(this).closest("li"));
            calculate_total_price();
            $("#cp_frm").remove();
            $(this).closest("li").find(".cp_btn").text("쿠폰적용").removeClass("cp_mod").focus();
            $(this).remove();
        });

        $("#od_coupon_btn").click(function() {
            $("#od_coupon_frm").remove();
            var $this = $(this);
            var price = parseInt($("input[name=org_od_price]").val()) - parseInt($("input[name=item_coupon]").val());
            if(price <= 0) {
                alert('상품금액이 0원이므로 쿠폰을 사용할 수 없습니다.');
                return false;
            }
            $.post(
                "./ordercoupon.php",
                { price: price },
                function(data) {
                    $this.after(data);
                }
            );
        });

        $(document).on("click", ".od_cp_apply", function() {
            var $el = $(this).closest("tr");
            var cp_id = $el.find("input[name='o_cp_id[]']").val();
            var price = parseInt($el.find("input[name='o_cp_prc[]']").val());
            var subj = $el.find("input[name='o_cp_subj[]']").val();
            var send_cost = $("input[name=od_send_cost]").val();
            var item_coupon = parseInt($("input[name=item_coupon]").val());
            var od_price = parseInt($("input[name=org_od_price]").val()) - item_coupon;

            if(price == 0) {
                if(!confirm(subj+"쿠폰의 할인 금액은 "+price+"원입니다.\n쿠폰을 적용하시겠습니까?")) {
                    return false;
                }
            }

            if(od_price - price <= 0) {
                alert("쿠폰할인금액이 주문금액보다 크므로 쿠폰을 적용할 수 없습니다.");
                return false;
            }

            $("input[name=sc_cp_id]").val("");
            $("#sc_coupon_btn").text("쿠폰적용");
            $("#sc_coupon_cancel").remove();

            $("input[name=od_price]").val(od_price - price);
            $("input[name=od_cp_id]").val(cp_id);
            $("input[name=od_coupon]").val(price);
            $("input[name=od_send_coupon]").val(0);
            $("#od_cp_price").text(number_format(String(price)));
            $("#sc_cp_price").text(0);
            calculate_order_price();
            $("#od_coupon_frm").remove();
            $("#od_coupon_btn").text("변경").focus();
            if(!$("#od_coupon_cancel").size())
                $("#od_coupon_btn").after("<button type=\"button\" id=\"od_coupon_cancel\" class=\"cp_cancel1\">취소</button>");
        });

        $(document).on("click", "#od_coupon_close", function() {
            $("#od_coupon_frm").remove();
            $("#od_coupon_btn").focus();
        });

        $(document).on("click", "#od_coupon_cancel", function() {
            var org_price = $("input[name=org_od_price]").val();
            var item_coupon = parseInt($("input[name=item_coupon]").val());
            $("input[name=od_price]").val(org_price - item_coupon);
            $("input[name=sc_cp_id]").val("");
            $("input[name=od_coupon]").val(0);
            $("input[name=od_send_coupon]").val(0);
            $("#od_cp_price").text(0);
            $("#sc_cp_price").text(0);
            calculate_order_price();
            $("#od_coupon_frm").remove();
            $("#od_coupon_btn").text("쿠폰적용").focus();
            $(this).remove();
            $("#sc_coupon_btn").text("쿠폰적용");
            $("#sc_coupon_cancel").remove();
        });

        $("#sc_coupon_btn").click(function() {
            $("#sc_coupon_frm").remove();
            var $this = $(this);
            var price = parseInt($("input[name=od_price]").val());
            var send_cost = parseInt($("input[name=od_send_cost]").val());
            $.post(
                "./ordersendcostcoupon.php",
                { price: price, send_cost: send_cost },
                function(data) {
                    $this.after(data);
                }
            );
        });

        $(document).on("click", ".sc_cp_apply", function() {
            var $el = $(this).closest("tr");
            var cp_id = $el.find("input[name='s_cp_id[]']").val();
            var price = parseInt($el.find("input[name='s_cp_prc[]']").val());
            var subj = $el.find("input[name='s_cp_subj[]']").val();
            var send_cost = parseInt($("input[name=od_send_cost]").val());

            if(parseInt(price) == 0) {
                if(!confirm(subj+"쿠폰의 할인 금액은 "+price+"원입니다.\n쿠폰을 적용하시겠습니까?")) {
                    return false;
                }
            }

            $("input[name=sc_cp_id]").val(cp_id);
            $("input[name=od_send_coupon]").val(price);
            $("#sc_cp_price").text(number_format(String(price)));
            calculate_order_price();
            $("#sc_coupon_frm").remove();
            $("#sc_coupon_btn").text("변경").focus();
            if(!$("#sc_coupon_cancel").size())
                $("#sc_coupon_btn").after("<button type=\"button\" id=\"sc_coupon_cancel\" class=\"cp_cancel1\">취소</button>");
        });

        $(document).on("click", "#sc_coupon_close", function() {
            $("#sc_coupon_frm").remove();
            $("#sc_coupon_btn").focus();
        });

        $(document).on("click", "#sc_coupon_cancel", function() {
            $("input[name=od_send_coupon]").val(0);
            $("#sc_cp_price").text(0);
            calculate_order_price();
            $("#sc_coupon_frm").remove();
            $("#sc_coupon_btn").text("쿠폰적용").focus();
            $(this).remove();
        });

        $("#od_b_addr2").focus(function() {
            var zip = $("#od_b_zip").val().replace(/[^0-9]/g, "");
            if(zip == "")
                return false;

            var code = String(zip);

            if(zipcode == code)
                return false;

            zipcode = code;
            calculate_sendcost(code);
        });

        $("#od_settle_bank").on("click", function() {
            $("[name=od_deposit_name]").val( $("[name=od_name]").val() );
            $("#settle_bank").show();
            $("#show_req_btn").css("display", "none");
            $("#show_pay_btn").css("display", "inline");
        });

        $("#od_settle_iche,#od_settle_card,#od_settle_vbank,#od_settle_hp,#od_settle_easy_pay,#od_settle_kakaopay,#od_settle_samsungpay").bind("click", function() {
            $("#settle_bank").hide();
            $("#show_req_btn").css("display", "inline");
            $("#show_pay_btn").css("display", "none");
        });

        // 배송지선택
        $("input[name=ad_sel_addr]").on("click", function() {
            var addr = $(this).val().split(String.fromCharCode(30));

            if (addr[0] == "same") {
                gumae2baesong();
            } else {
                if(addr[0] == "new") {
                    for(i=0; i<10; i++) {
                        addr[i] = "";
                    }
                }

                var f = document.forderform;
                f.od_b_name.value        = addr[0];
                //f.od_b_tel.value         = addr[1];
                f.od_b_hp.value          = addr[2];
                f.od_b_zip.value         = addr[3] + addr[4];
                f.od_b_addr1.value       = addr[5];
                f.od_b_addr2.value       = addr[6];
                f.od_b_addr3.value       = addr[7];
                f.od_b_addr_jibeon.value = addr[8];
                f.ad_subject.value       = addr[9];

                var zip1 = addr[3].replace(/[^0-9]/g, "");
                var zip2 = addr[4].replace(/[^0-9]/g, "");

                var code = String(zip1) + String(zip2);

                if(zipcode != code) {
                    calculate_sendcost(code);
                }
            }
        });

        // 모두선택
        $("input[name=all_check]").click(function() {
            if($(this).is(":checked"))
                $("input[name^=multi_chk]").attr("checked", true);
            else
                $("input[name^=multi_chk]").attr("checked", false);
        });


        $(document).on("click", "#multiYN", function() {

            if( $("input:checkbox[id='multiYN']").is(":checked") ){

                $("#multi_receiver_list").css("display","");
                $("#mylist_button").css("display","");
                $(".tbl_receiver_content_2").css("display","none");
                //$(".tbl_receiver_content_1").height(150);

            }else{

                $("#multi_receiver_list").css("display","none");
                $("#mylist_button").css("display","none");
                $(".tbl_receiver_content_2").css("display","");

            }

        });

    });

    function od_gubun_chg2(){
        var f = document.forderform;

        /*frm.ord_name.value 		= "인포약국";
        frm.ord_tel1.value 		= "02";
        frm.ord_tel2.value 		= "391";
        frm.ord_tel3.value 		= "0660";
        frm.ord_hp1.value 		= "010";
        frm.ord_hp2.value 		= "3605";
        frm.ord_hp3.value 		= "5879";
        frm.ord_zipcode.value 	= "122-829";
        frm.ord_addr1.value 	= "서울특별시 은평구 녹번동 77~91";
        frm.ord_addr2.value 	= "83-54번지 원기빌딩 202호1212";*/

        f.od_name.value        = "<?php echo get_text($member['mb_name']); ?>";
        //f.od_tel.value         = "<?php echo get_text($member['mb_tel']); ?>";
        f.od_hp.value          = "<?php echo get_text($member['mb_hp']); ?>";
        f.od_zip.value         = "<?php echo $member['mb_zip1'].$member['mb_zip2']; ?>";
        f.od_addr1.value       = "<?php echo get_text($member['mb_addr1']) ?>";
        f.od_addr2.value       = "<?php echo get_text($member['mb_addr2']) ?>";
        f.od_addr3.value       = "<?php echo get_text($member['mb_addr3']) ?>";
        f.od_addr_jibeon.value = "<?php echo get_text($member['mb_addr_jibeon']); ?>";
        f.od_email.value       = "<?php echo $member['mb_email']; ?>";

    }

    function od_gubun_chg1(){
        var f = document.forderform;

        f.od_name.value        = "";
        //f.od_tel.value         = "";
        f.od_hp.value          = "";
        f.od_zip.value         = "";
        f.od_addr1.value       = "";
        f.od_addr2.value       = "";
        f.od_addr3.value       = "";
        f.od_addr_jibeon.value = "";
        f.od_email.value       = "";

    }
    function myaddr(gubun) {
        var url = "<?php echo G5_SHOP_URL;?>/orderuser.php?gubun="+gubun ;
        window.open(url, "win_address", "left=100,top=100,width=800,height=600,scrollbars=1");
        return false;
    }
    function myaddr2(gubun) {
        var url = "<?php echo G5_SHOP_URL;?>/orderuser.php?gubun="+gubun+"&multi_gubun=Y" ;
        window.open(url, "win_address", "left=100,top=100,width=800,height=600,scrollbars=1");
        //return false;
        return;
    }


    function coupon_cancel($el)
    {
        var $dup_sell_el = $el.find(".total_price strong");
        var $dup_price_el = $el.find("input[name^=cp_price]");
        var org_sell_price = $el.find("input[name^=it_price]").val();

        $dup_sell_el.text(number_format(String(org_sell_price)));
        $dup_price_el.val(0);
        $el.find("input[name^=cp_id]").val("");
    }

    function calculate_total_price()
    {
        var $it_prc = $("input[name^=it_price]");
        var $cp_prc = $("input[name^=cp_price]");
        var tot_sell_price = sell_price = tot_cp_price = 0;
        var it_price, cp_price, it_notax;
        var tot_mny = comm_tax_mny = comm_vat_mny = comm_free_mny = tax_mny = vat_mny = 0;
        var send_cost = parseInt($("input[name=od_send_cost]").val());

        $it_prc.each(function(index) {
            it_price = parseInt($(this).val());
            cp_price = parseInt($cp_prc.eq(index).val());
            sell_price += it_price;
            tot_cp_price += cp_price;
        });

        tot_sell_price = sell_price - tot_cp_price + send_cost;

        $("#ct_tot_coupon").text(number_format(String(tot_cp_price))+" 원");
        $("#ct_tot_price").text(number_format(String(tot_sell_price)));

        $("input[name=good_mny]").val(tot_sell_price);
        $("input[name=od_price]").val(sell_price - tot_cp_price);
        $("input[name=item_coupon]").val(tot_cp_price);
        $("input[name=od_coupon]").val(0);
        $("input[name=od_send_coupon]").val(0);
        <?php if($oc_cnt > 0) { ?>
        $("input[name=od_cp_id]").val("");
        $("#od_cp_price").text(0);
        if($("#od_coupon_cancel").size()) {
            $("#od_coupon_btn").text("쿠폰적용");
            $("#od_coupon_cancel").remove();
        }
        <?php } ?>
        <?php if($sc_cnt > 0) { ?>
        $("input[name=sc_cp_id]").val("");
        $("#sc_cp_price").text(0);
        if($("#sc_coupon_cancel").size()) {
            $("#sc_coupon_btn").text("쿠폰적용");
            $("#sc_coupon_cancel").remove();
        }
        <?php } ?>
        $("input[name=od_temp_point]").val(0);
        <?php if($temp_point > 0 && $is_member) { ?>
        calculate_temp_point();
        <?php } ?>
        calculate_order_price();
    }

    function calculate_order_price()
    {
        var sell_price = parseInt($("input[name=od_price]").val());
        var send_cost = parseInt($("input[name=od_send_cost]").val());
        var send_cost2 = parseInt($("input[name=od_send_cost2]").val());
        var send_coupon = parseInt($("input[name=od_send_coupon]").val());
        var tot_price = sell_price + send_cost + send_cost2 - send_coupon;

        $("form[name=sm_form] input[name=good_mny]").val(tot_price);
        $("#od_tot_price").text(number_format(String(tot_price)));
        <?php if($temp_point > 0 && $is_member) { ?>
        calculate_temp_point();
        <?php } ?>
    }

    function calculate_temp_point()
    {
        var sell_price = parseInt($("input[name=od_price]").val());
        var mb_point = parseInt(<?php echo $member['mb_point']; ?>);
        var max_point = parseInt(<?php echo $default['de_settle_max_point']; ?>);
        var point_unit = parseInt(<?php echo $default['de_settle_point_unit']; ?>);
        var temp_point = max_point;

        if(temp_point > sell_price)
            temp_point = sell_price;

        if(temp_point > mb_point)
            temp_point = mb_point;

        temp_point = parseInt(temp_point / point_unit) * point_unit;

        $("#use_max_point").text(number_format(String(temp_point))+"점");
        $("input[name=max_temp_point]").val(temp_point);
    }

    function calculate_sendcost(code)
    {
        $.post(
            "./ordersendcost.php",
            { zipcode: code },
            function(data) {
                $("input[name=od_send_cost2]").val(data);
                $("#od_send_cost2").text(number_format(String(data)));

                zipcode = code;

                calculate_order_price();
            }
        );
    }

    function calculate_tax()
    {
        var $it_prc = $("input[name^=it_price]");
        var $cp_prc = $("input[name^=cp_price]");
        var sell_price = tot_cp_price = 0;
        var it_price, cp_price, it_notax;
        var tot_mny = comm_free_mny = tax_mny = vat_mny = 0;
        var send_cost = parseInt($("input[name=od_send_cost]").val());
        var send_cost2 = parseInt($("input[name=od_send_cost2]").val());
        var od_coupon = parseInt($("input[name=od_coupon]").val());
        var send_coupon = parseInt($("input[name=od_send_coupon]").val());
        var temp_point = 0;

        $it_prc.each(function(index) {
            it_price = parseInt($(this).val());
            cp_price = parseInt($cp_prc.eq(index).val());
            sell_price += it_price;
            tot_cp_price += cp_price;
            it_notax = $("input[name^=it_notax]").eq(index).val();
            if(it_notax == "1") {
                comm_free_mny += (it_price - cp_price);
            } else {
                tot_mny += (it_price - cp_price);
            }
        });

        if($("input[name=od_temp_point]").size())
            temp_point = parseInt($("input[name=od_temp_point]").val());

        tot_mny += (send_cost + send_cost2 - od_coupon - send_coupon - temp_point);
        if(tot_mny < 0) {
            comm_free_mny = comm_free_mny + tot_mny;
            tot_mny = 0;
        }

        tax_mny = Math.round(tot_mny / 1.1);
        vat_mny = tot_mny - tax_mny;
        $("input[name=comm_tax_mny]").val(tax_mny);
        $("input[name=comm_vat_mny]").val(vat_mny);
        $("input[name=comm_free_mny]").val(comm_free_mny);
    }

    /* 결제방법에 따른 처리 후 결제등록요청 실행 */
    var settle_method = "무통장";
    var temp_point = 0;


    function forderform_check()
    {
        var f = document.forderform;

        // 재고체크
        var stock_msg = order_stock_check();
        if(stock_msg != "") {
            alert(stock_msg);
            return false;
        }

        // 필드체크
        if(!orderfield_check(f))
            return false;

        // 금액체크
        if(!payment_check(f))
            return false;

        if(settle_method != "무통장" && f.res_cd.value != "0000") {
            alert("결제등록요청 후 주문해 주십시오.");
            return false;
        }

        //document.getElementById("display_pay_button").style.display = "none";
        //document.getElementById("show_progress").style.display = "block";

        //setTimeout(function() {
        f.submit();
        //}, 300);
    }

    // 주문폼 필드체크
    function orderfield_check(f)
    {
        errmsg = "";
        errfld = "";
        var deffld = "";

        check_field(f.od_name, "주문하시는 분 이름을 입력하십시오.");
        if (typeof(f.od_pwd) != 'undefined')
        {
            clear_field(f.od_pwd);
            if( (f.od_pwd.value.length<3) || (f.od_pwd.value.search(/([^A-Za-z0-9]+)/)!=-1) )
                error_field(f.od_pwd, "회원이 아니신 경우 주문서 조회시 필요한 비밀번호를 3자리 이상 입력해 주십시오.");
        }
        //check_field(f.od_tel, "주문하시는 분 전화번호를 입력하십시오.");
        check_field(f.od_addr1, "주소검색을 이용하여 주문하시는 분 주소를 입력하십시오.");
        //check_field(f.od_addr2, " 주문하시는 분의 상세주소를 입력하십시오.");
        check_field(f.od_zip, "");

        //clear_field(f.od_email);
        //if(f.od_email.value=='' || f.od_email.value.search(/(\S+)@(\S+)\.(\S+)/) == -1)
        //    error_field(f.od_email, "E-mail을 바르게 입력해 주십시오.");

        //if (typeof(f.od_hope_date) != "undefined")
        //{
        //    clear_field(f.od_hope_date);
        //    if (!f.od_hope_date.value)
        //        error_field(f.od_hope_date, "희망배송일을 선택하여 주십시오.");
        //}

        //다중배송일경우 총상품갯수와 분배갯수 체크
        if( $("input:checkbox[id='multiYN']").is(":checked") ){

            var multi_sum = 0;
            var order_cnt = 0;

            order_cnt = parseInt( $(document).find("#order_qty").val() );

            $("input[class=multi_cnt]").each(function(index, item) {
                if( $(document).find(".multi_cnt").eq(index).val() != "" ){
                    multi_sum += parseInt( $(document).find(".multi_cnt").eq(index).val() );
                }
            });

            if( multi_sum == 0 || ( order_cnt != multi_sum ) ){
                alert("다중배송시 상품수량입력이 잘못되었습니다.");
                return;
            }

        }else{

            check_field(f.od_b_name, "받으시는 분 이름을 입력하십시오.");
            //check_field(f.od_b_tel, "받으시는 분 전화번호를 입력하십시오.");
            check_field(f.od_b_addr1, "주소검색을 이용하여 받으시는 분 주소를 입력하십시오.");
            //check_field(f.od_b_addr2, "받으시는 분의 상세주소를 입력하십시오.");
            check_field(f.od_b_zip, "");

        }

        var od_settle_bank = document.getElementById("od_settle_bank");
        if (od_settle_bank) {
            if (od_settle_bank.checked) {
                check_field(f.od_bank_account, "계좌번호를 선택하세요.");
                check_field(f.od_deposit_name, "입금자명을 입력하세요.");
            }
        }

        // 배송비를 받지 않거나 더 받는 경우 아래식에 + 또는 - 로 대입
        f.od_send_cost.value = parseInt(f.od_send_cost.value);

        if (errmsg)
        {
            alert(errmsg);
            errfld.focus();
            return false;
        }

        var settle_case = document.getElementsByName("od_settle_case");
        settle_method = "무통장";

        return true;
    }

    // 결제체크
    function payment_check(f)
    {
        var max_point = 0;
        var od_price = parseInt(f.od_price.value);
        var send_cost = parseInt(f.od_send_cost.value);
        var send_cost2 = parseInt(f.od_send_cost2.value);
        var send_coupon = parseInt(f.od_send_coupon.value);
        temp_point = 0;

        if (typeof(f.max_temp_point) != "undefined")
            var max_point  = parseInt(f.max_temp_point.value);

        if (typeof(f.od_temp_point) != "undefined") {
            if (f.od_temp_point.value)
            {
                var point_unit = parseInt(<?php echo $default['de_settle_point_unit']; ?>);
                temp_point = parseInt(f.od_temp_point.value);

                if (temp_point < 0) {
                    alert("포인트를 0 이상 입력하세요.");
                    f.od_temp_point.select();
                    return false;
                }

                if (temp_point > od_price) {
                    alert("상품 주문금액(배송비 제외) 보다 많이 포인트결제할 수 없습니다.");
                    f.od_temp_point.select();
                    return false;
                }

                if (temp_point > <?php echo (int)$member['mb_point']; ?>) {
                    alert("회원님의 포인트보다 많이 결제할 수 없습니다.");
                    f.od_temp_point.select();
                    return false;
                }

                if (temp_point > max_point) {
                    alert(max_point + "점 이상 결제할 수 없습니다.");
                    f.od_temp_point.select();
                    return false;
                }

                if (parseInt(parseInt(temp_point / point_unit) * point_unit) != temp_point) {
                    alert("포인트를 "+String(point_unit)+"점 단위로 입력하세요.");
                    f.od_temp_point.select();
                    return false;
                }
            }
        }

        var tot_price = od_price + send_cost + send_cost2 - send_coupon - temp_point;

        if (document.getElementById("od_settle_iche")) {
            if (document.getElementById("od_settle_iche").checked) {
                if (tot_price < 150) {
                    alert("계좌이체는 150원 이상 결제가 가능합니다.");
                    return false;
                }
            }
        }

        if (document.getElementById("od_settle_card")) {
            if (document.getElementById("od_settle_card").checked) {
                if (tot_price < 1000) {
                    alert("신용카드는 1000원 이상 결제가 가능합니다.");
                    return false;
                }
            }
        }

        if (document.getElementById("od_settle_hp")) {
            if (document.getElementById("od_settle_hp").checked) {
                if (tot_price < 350) {
                    alert("휴대폰은 350원 이상 결제가 가능합니다.");
                    return false;
                }
            }
        }

        <?php if($default['de_tax_flag_use']) { ?>
        calculate_tax();
        <?php } ?>

        return true;
    }

    // 구매자 정보와 동일합니다.
    function gumae2baesong() {
        var f = document.forderform;

        f.od_b_name.value = f.od_name.value;
        //f.od_b_tel.value  = f.od_tel.value;
        f.od_b_hp.value   = f.od_hp.value;
        f.od_b_zip.value  = f.od_zip.value;
        f.od_b_addr1.value = f.od_addr1.value;
        f.od_b_addr2.value = f.od_addr2.value;
        f.od_b_addr3.value = f.od_addr3.value;
        f.od_b_addr_jibeon.value = f.od_addr_jibeon.value;

        calculate_sendcost(String(f.od_b_zip.value));
    }

    function MultiDel(){

        var ChekSum = 0;
        var TrId = "";

        $("input[name=multi_chk]:checkbox").each(function(index, item) {

            if( $("input:checkbox[name=multi_chk]").eq(index).prop("checked") ){

                ChekSum++;

            }

        });

        if( ChekSum == 0 ){
            alert("삭제목록을 체크해주세요.");
            return;
        }

        jQuery('input:checkbox[name=multi_chk]:checked').parents("tr").remove();

    }

</script>
<?php
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가

// add_stylesheet('css 구문', 출력순서); 숫자가 작을 수록 먼저 출력됨
add_stylesheet('<link rel="stylesheet" href="'.G5_MSHOP_SKIN_URL.'/style.css">', 0);
?>

<!-- 사용후기 쓰기 시작 { -->
<div id="sit_use_write" class="new_win">
    <h1 id="win_title">사용후기 쓰기</h1>

    <form name="fitemuse" method="post" action="./itemuseformupdate.php" onsubmit="return fitemuse_submit(this);" autocomplete="off" enctype="MULTIPART/FORM-DATA">
    <input type="hidden" name="w" value="<?php echo $w; ?>">
    <input type="hidden" name="it_id" value="<?php echo $it_id; ?>">
    <input type="hidden" name="is_id" value="<?php echo $is_id; ?>">
	 <input type="hidden" name="od_id" value="<?php echo $od_id; ?>">
    <input type="hidden" name="is_mobile_shop" value="1">

    <div class="form_01">

        <ul>
            <li>
                <label for="is_subject" class="sound_only">제목</label>
                <input type="text" name="is_subject" value="<?php echo get_text($use['is_subject']); ?>" id="is_subject" required class="required frm_input" minlength="2" maxlength="250" placeholder="제목">
            </li>
            <li>
                <div id="image_container"></div>
                <span class="sound_only">내용</span>
                <?php echo $editor_html; ?>
            </li>
            <li>
                <span class="sound_only">평가</span>
                <ul id="sit_use_write_star">
                    <li>
                        <input type="radio" name="is_score" value="5" id="is_score10" <?php echo ($is_score==5)?'checked="checked"':''; ?>>
                        <label for="is_score10">매우만족</label>
                        <img src="<?php echo G5_SHOP_URL; ?>/img/s_star5.png" width="90">
                    </li>
                    <li>
                        <input type="radio" name="is_score" value="4" id="is_score8" <?php echo ($is_score==4)?'checked="checked"':''; ?>>
                        <label for="is_score8">만족</label>
                        <img src="<?php echo G5_SHOP_URL; ?>/img/s_star4.png" width="90">
                    </li>
                    <li>
                        <input type="radio" name="is_score" value="3" id="is_score6" <?php echo ($is_score==3)?'checked="checked"':''; ?>>
                        <label for="is_score6">보통</label>
                        <img src="<?php echo G5_SHOP_URL; ?>/img/s_star3.png" width="90">
                    </li>
                    <li>
                        <input type="radio" name="is_score" value="2" id="is_score4" <?php echo ($is_score==2)?'checked="checked"':''; ?>>
                        <label for="is_score4">불만</label>
                        <img src="<?php echo G5_SHOP_URL; ?>/img/s_star2.png" width="90">
                    </li>
                    <li>
                        <input type="radio" name="is_score" value="1" id="is_score2" <?php echo ($is_score==1)?'checked="checked"':''; ?>>
                        <label for="is_score2">매우불만</label>
                        <img src="<?php echo G5_SHOP_URL; ?>/img/s_star1.png" width="90">
                    </li>
                    </ul>
                
            </li>
        </ul>
    </div>

    <div class="win_btn">
           <!-- <label class="btn_submit" for="isImage">사진첨부</label>
            <input type="file" class="btn_submit" accept="image/*" id="isImage" name="is_image" onchange="setThumbnail(event);">-->
        <input type="submit" id="submit" value="작성완료" class="btn_submit">
        <!--<button type="button" onclick="self.close();" class="btn_close">닫기</button>-->
		<button type="button" onclick="javascript:Modal_IframeClose();" class="btn_close">닫기</button>
    </div>

    </form>
</div>

<script type="text/javascript">
var clickbtn = false;
function fitemuse_submit(f)
{
    // 스마트에디터 value 변환
    oEditors.getById["is_content"].exec("UPDATE_CONTENTS_FIELD", []);

    var is_subject = $('input[name=is_subject]').val();
    var is_content = $("#is_content").val();

    // 모든 항목이 입력되었을때만 스크립트
    if(is_subject == "") {
        clickbtn = false;
        return false;
    }
    if(is_content == "<p><br></p>" || is_content == ""  || is_content == null || is_content == '&nbsp;' || is_content == '<p>&nbsp;</p>') {
        clickbtn = false;
        return false;
    }

	if(clickbtn == true){ //저장버튼 여러번 눌러 같은후기가 여러번 올라가는 것 방지
		alert("저장중입니다.");
		return false;
	}
	clickbtn = true;
    <?php echo $editor_js; ?>
	
	
    return true;
}

function setThumbnail(event) {

    var container = document.querySelector("#image_container");
    if(container){
        container.innerHTML = "";
    }

    var reader = new FileReader();
    reader.onload = function(event) {
        var img = document.createElement("img");
        img.setAttribute("src", event.target.result);
        document.querySelector("div#image_container").appendChild(img);
    };

    reader.readAsDataURL(event.target.files[0]);
}



</script>

<style>
    #image_container img{
        height: auto;
        width: 390px;
    }

    .win_btn input[type="file"] {
        /* 파일 필드 숨기기 */
        position: absolute;
        width: 1px;
        height: 1px;
        padding: 0;
        margin: -1px;
        overflow: hidden;
        clip:rect(0,0,0,0);
        border: 0;
    }

</style>
<!-- } 사용후기 쓰기 끝 -->
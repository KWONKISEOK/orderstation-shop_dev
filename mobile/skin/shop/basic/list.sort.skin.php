<?php
if (!defined("_GNUBOARD_")) exit; // 개별 페이지 접근 불가

$sct_sort_href = $_SERVER['SCRIPT_NAME'].'?';
if($ca_id)
    $sct_sort_href .= 'ca_id='.$ca_id;
else if($ev_id)
    $sct_sort_href .= 'ev_id='.$ev_id;
if($skin)
    $sct_sort_href .= '&amp;skin='.$skin;
$sct_sort_href .= '&amp;sort=';

// add_stylesheet('css 구문', 출력순서); 숫자가 작을 수록 먼저 출력됨
add_stylesheet('<link rel="stylesheet" href="'.G5_MSHOP_SKIN_URL.'/style.css">', 0);

// 상품 리스트에서 소비자가 최댓값
$max_sql = "SELECT MAX(it_price) as max_price FROM {$g5['g5_shop_event_item_table']} a, {$g5['g5_shop_item_table']} b WHERE a.it_id = b.it_id and a.ev_id = '$ev_id'";
$max_result = sql_fetch($max_sql);
?>
<style>
    ul#type li .active {
        color: #ff8800;
    }

</style>
<!-- 상품 정렬 선택 시작 { -->
<?php
if ($ev['ev_sort'] == 0 || $ev['ev_sort'] == 2) {?>
<div id="sct_sort1">
    <button type="button" class="btn_sort1"><i class="fa fa-arrows-v" aria-hidden="true"></i> 상품정렬</button>
    <ul id="type" >
        <li><a href="<?php echo $sct_sort_href; ?>it_sum_qty&amp;sortodr=desc&amp;maxPrice=<?php echo $_GET['maxPrice']?>&amp;minPrice=<?php echo $_GET['minPrice']?>" ><span class="<?php if($sort=='it_sum_qty' && $sortodr=='desc') echo 'active';?>">판매많은순</span></a></li>
        <li><a href="<?php echo $sct_sort_href; ?>it_price&amp;sortodr=asc&amp;maxPrice=<?php echo $_GET['maxPrice']?>&amp;minPrice=<?php echo $_GET['minPrice']?>" ><span class="<?php if($sort=='it_price' && $sortodr=='asc') echo 'active';?>">낮은가격순</span></a></li>
        <li><a href="<?php echo $sct_sort_href; ?>it_price&amp;sortodr=desc&amp;maxPrice=<?php echo $_GET['maxPrice']?>&amp;minPrice=<?php echo $_GET['minPrice']?>"><span class="<?php if($sort=='it_price' && $sortodr=='desc') echo 'active';?>">높은가격순</span></a></li>
        <li><a href="<?php echo $sct_sort_href; ?>it_use_avg&amp;sortodr=desc&amp;maxPrice=<?php echo $_GET['maxPrice']?>&amp;minPrice=<?php echo $_GET['minPrice']?>"><span class="<?php if($sort=='it_use_avg' && $sortodr=='desc') echo 'active';?>">평점높은순</span></a></li>
        <li><a href="<?php echo $sct_sort_href; ?>it_use_cnt&amp;sortodr=desc&amp;maxPrice=<?php echo $_GET['maxPrice']?>&amp;minPrice=<?php echo $_GET['minPrice']?>"><span class="<?php if($sort=='it_use_cnt' && $sortodr=='desc') echo 'active';?>">후기많은순</span></a></li>
        <li><a href="<?php echo $sct_sort_href; ?>it_update_time&amp;sortodr=desc&amp;maxPrice=<?php echo $_GET['maxPrice']?>&amp;minPrice=<?php echo $_GET['minPrice']?>"><span class="<?php if($sort=='it_update_time' && $sortodr=='desc') echo 'active';?>">최근등록순</span></a></li>
    </ul>
</div>
<?php } ?>

<?php
if ($ev['ev_sort'] == 1 || $ev['ev_sort'] == 2) { ?>
    <div id="sct_sort2" <?php if ($ev['ev_sort'] != 1) echo 'style="margin-top:-40px;margin-left: 80px;"'?>>
    <button type="button" class="btn_sort2"><i class="fa fa-arrows-v" aria-hidden="true"></i> 가격정렬</button>
    <ul id="type">
        <li><a href="<?php echo $sct_sort_href; ?>"><span class="<?php if($maxPrice=='') echo 'active';?>">전체</a></li>
        <li><a href="<?php echo $sct_sort_href; ?><?php echo $_GET['sort']?>&amp;sortodr=<?php echo $_GET['sortodr']?>&amp;maxPrice=10000&amp;minPrice=1" ><span class="<? if($maxPrice=='10000') echo 'active';?>">1만원 이하</span></a></li>
        <li><a href="<?php echo $sct_sort_href; ?><?php echo $_GET['sort']?>&amp;sortodr=<?php echo $_GET['sortodr']?>&amp;maxPrice=30000&amp;minPrice=10000" ><span class="<?php if($maxPrice=='30000') echo 'active';?>">1만원~3만원</span></a></li>
        <li><a href="<?php echo $sct_sort_href; ?><?php echo $_GET['sort']?>&amp;sortodr=<?php echo $_GET['sortodr']?>&amp;maxPrice=50000&amp;minPrice=30000" ><span class="<?php if($maxPrice=='50000') echo 'active';?>">3만원~5만원</span></a></li>
        <li><a href="<?php echo $sct_sort_href; ?><?php echo $_GET['sort']?>&amp;sortodr=<?php echo $_GET['sortodr']?>&amp;maxPrice=<?php echo $max_result['max_price']?>&amp;minPrice=50000" ><span class="<?php if($maxPrice==$max_result['max_price']) echo 'active';?>">5만원이상</span></a></li>
    </ul>
</div>
<?php } ?>
<!-- } 상품 정렬 선택 끝 -->

<script>
    $(".btn_sort1").click(function(){
        $("#sct_sort1 ul").show();
    });
    $(document).mouseup(function (e){
        var container = $("#sct_sort1 ul");
        if( container.has(e.target).length === 0)
            container.hide();
    });

    $(".btn_sort2").click(function(){
        $("#sct_sort2 ul").show();
    });
    $(document).mouseup(function (e){
        var container = $("#sct_sort2 ul");
        if( container.has(e.target).length === 0)
        container.hide();
    });
</script>
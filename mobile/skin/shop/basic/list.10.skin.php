<?php
include_once('./_common.php');
include_once(G5_THEME_SHOP_PATH.'/shop.head.php');
if (!defined("_GNUBOARD_")) exit; // 개별 페이지 접근 불가

// add_stylesheet('css 구문', 출력순서); 숫자가 작을 수록 먼저 출력됨
add_stylesheet('<link rel="stylesheet" href="'.G5_MSHOP_SKIN_URL.'/style.css">', 0);

global $member;
?>

<?php if(!defined('G5_IS_SHOP_AJAX_LIST') && $config['cf_kakao_js_apikey']) { ?>
<script src="https://developers.kakao.com/sdk/js/kakao.min.js"></script>
<script src="<?php echo G5_JS_URL; ?>/kakaolink.js"></script>
<script>
    // 사용할 앱의 Javascript 키를 설정해 주세요.
    Kakao.init("<?php echo $config['cf_kakao_js_apikey']; ?>");
</script>
<?php } ?>
<style>
.event_trans {
	font-size:10px;
	width:30px;
	height:30px;
	line-height:14px;
	display:inline;
	z-index:100;
	position:absolute;
	top:10px;
	right:44px;
	background:#ff8100;
	color:#fff;
	text-align:center;
	border-radius:50px;
	font-weight:bold;
	letter-spacing:1px;
	box-sizing:border-box;
	box-shadow:inset 0px 0px 2px 0px rgba(189,189,189,1);
}
</style>
<!-- 메인상품진열 10 시작 { -->
<?php
// is_drug=true 일 경우
// 포인트 보여짐, 오피스디포 카테고리 보여짐
$is_drug = false;
if($member['mb_type'] === '1' || $member['mb_type'] === '9') {
	$is_drug = true;
}else if($member['mb_type'] == ''){
    $ismember = "logout";
}

$is_gallery_list = ($this->ca_id && isset($_COOKIE['ck_itemlist'.$this->ca_id.'_type'])) ? $_COOKIE['ck_itemlist'.$this->ca_id.'_type'] : '';
if (!$is_gallery_list) {
    $is_gallery_list = 'gallery';
}
$li_width = ($is_gallery_list === 'gallery') ? intval(100 / $this->list_mod) : 100;
$li_width_style = ' style="width:'.$li_width.'%;"';
$ul_sct_class = ($is_gallery_list === 'gallery') ? 'sct_10' : 'sct_20';

for ($i = 0; $row = sql_fetch_array($result); $i++) {

    if ($row['it_cust_price'] > 0) {
        $is_sale = true;
    } else {
        $is_sale = false;
    }

    if ($i == 0) {
        if ($this->css) {
            echo "<ul id=\"sct_wrap\" class=\"{$this->css}  \">\n";
        } else {
            echo "<ul id=\"sct_wrap\" class=\"sct ".$ul_sct_class."\">\n";
        }
    }

    if ($i % $this->list_mod == 0)
        $li_clear = ' sct_clear';
    else
        $li_clear = '';
    if (get_item_auth_member($member['mb_id'], $row['it_id']) == false && get_item_auth_check($row['it_id'], 1) == true) { //회비관련 추가
        echo "<p class=\"sct_noitem\"></p>\n";
    } else {
        echo "<li class=\"sct_li{$li_clear}\"$li_width_style><div class=\"li_wr is_view_type_list ec-base-product\">\n";

        $array_t = array('S01754', 'S01753', 'S01752', 'S01751', 'S01750');
        if (in_array($row['it_id'], $array_t)) {
            echo " <span class=\"event_trans\">추가 할인</span> ";
        }
        if ($row['it_id'] == "S01755") {
            echo " <span class=\"event_trans\">타임세일</span> ";
        }


        if ($this->href) {
            echo "<div class=\"sct_img\"><a href=\"{$this->href}{$row['it_id']}\">\n";
        }

        if ($this->view_it_img) {
            echo get_it_image($row['it_id'], $this->img_width, $this->img_height, '', '', stripslashes($row['it_name']))."\n";
        }

        if ($this->href) {
            echo "</a></div>\n";
        }

        $whishlist_item_1 = get_wishlist_my_item_check($row['it_id']) ? 'is-active' : '';
        $whishlist_item_2 = get_wishlist_my_item_check($row['it_id']) ? 'd' : 'w';

        echo "<div class=\"thumbnail\">";
        echo "<button id='{$row['it_id']}' value='{$row['it_id']}' class='thumbnail icon icon-heart {$whishlist_item_1}' title='좋아요' type='button' onclick=\"item_wish(document.fitem,'{$row['it_id']}');\"></button>";
        echo "<input type='hidden' id='wishValue' value='{$whishlist_item_2}'>";
        echo "<input type='hidden' id='ismember' value='{$ismember}'>";
        echo "</div>";

        if ($this->view_it_brand) {

            echo "<div class=\"description\">";

            if($row['it_brand']){
                echo "<p class=\"sct_brand_name\">{$row['it_brand']}</p>";
            } else{
                echo "<p class=\"sct_brand_name\">ㅤㅤㅤㅤㅤㅤㅤㅤㅤㅤ</p>";
            }

        }

        if ($this->view_it_id) {
            echo "<div class=\"sct_id\">&lt;".stripslashes($row['it_id'])."&gt;</div>\n";
        }

        if ($this->href) {
            echo "<div class=\"sct_txt\"><a href=\"{$this->href}{$row['it_id']}\" class=\"sct_a\">\n";
        }

        if ($this->view_it_name) {
            echo stripslashes($row['it_name'])."\n";
        }

        if ($this->href) {
            echo "</a></div>\n";
        }

        if ($this->view_it_price) {
            $it_price = $row['it_cust_price'] > 0 ? number_format($row['it_cust_price']) : number_format($row['it_price']);
            $data_prod_price = $row['it_drug_price'] > 0 ? $row['it_drug_price'] : $row['it_drug_ori_price'];
            $data_prod_custom = $row['it_cust_price'] > 0 ? $row['it_cust_price'] : $row['it_price'];
            echo "<div class=\"sct_cost\">\n";

            // 공급자가 표시
            if ($is_drug) {
                echo "<span class='sct_price_title'>약국공급가</span>";
                echo "<div class='pay-area'>";
                    echo "<span class=\"sct_price\">".display_price($row['it_drug_price'])."</span><span class=\"sct_price_won\"> 원</span>\n";
                echo "<del class=\"sct_original_price\"><span class=\"price\">".$it_price."</span><span class=\"unit\">원</span></del>";
                echo "<span class='discount-percent' data-prod-price=\"{$data_prod_price}\" data-prod-custom=\"{$data_prod_custom}\"></span>";
                echo "</div>";

                echo "<span class=\"sct_incen\" style='color: #888;font-weight: 400;'>포인트 ".number_format($row['it_incen'])." | </span>\n";

            } else {
                echo "<div class='pay-area'>";
                     echo "<span class=\"sct_price\">".display_price($row['it_price'])."</span><span class=\"sct_price_won\"> 원</span>\n";
                if ($row['it_cust_price']) {
                    echo "<del class=\"sct_original_price\"><span class=\"price\">".number_format($row['it_cust_price'])."</span><span class=\"unit\">원</span></del>";
                    echo "<span class='discount-percent' data-prod-price='".$row['it_price']."' data-prod-custom='".$row['it_cust_price']."'></span>";
                }
                echo "</div>";

            }
            if($row['it_sc_type'] == "1"){
                echo "<span class=\"info\" style='color: #888;font-weight: 400;'>무료배송</span>";
            } else {
                echo "<span class=\"info\" style='color: #888;font-weight: 400;'>유료배송</span>";
            }



            echo "<div class='panel-set' style='margin-top: 10px;'>";
            if ($row['it_type3'] == 1) {
                echo "<span class='panel panel-new'>NEW</span>";
            }
            if ($row['it_type1'] == 1) {
                echo "<span class='panel panel-best'>BEST</span>";
            }
            if ($row['it_type2'] == 1) {
                echo "<span class='panel panel-reco'>추천상품</span>";
            }
            if ($row['it_supply_subject'] == 1) {
                echo "<span class='panel panel-gift'>추가증정</span>";
            }
            if ($row['it_company'] != 0 && ($member['mb_referee'] == 4 || $member['mb_type'] == 9)) {
                echo "<span class='panel panel-company'>사내수령</span>";
            }


            echo "</div>";
        }

        ?>
        <div class="icon">
            <div class="promotion">
                <?php if ($row['it_type1'] == 1) { ?>
                    <img src="<?php echo G5_THEME_URL; ?>/img/icon/icon_best.jpg" class="icon_img" alt="Best"/>
                <?php } ?>
                <?php if ($row['it_type2'] == 1) { ?>
                    <img src="<?php echo G5_THEME_URL; ?>/img/icon/icon_recommand.jpg" class="icon_img" alt="추천"/>
                <?php } ?>
                <?php if ($row['it_type3'] == 1) { ?>
                    <img src="<?php echo G5_THEME_URL; ?>/img/icon/icon_new.jpg" class="icon_img" alt="New"/>
                <?php } ?>
            </div>
        </div>
        <?php


        echo "</div></li>\n";
    }//회비관련 닫음
}

if ($i > 0) echo "</ul>\n";

if($i == 0) echo "<p class=\"sct_noitem\">등록된 상품이 없습니다.</p>\n";
?>
<!-- } 상품진열 10 끝 -->

<?php if( !defined('G5_IS_SHOP_AJAX_LIST') ) { ?>
<script>
jQuery(function($){
    var li_width = "<?php echo intval(100 / $this->list_mod); ?>",
        img_width = "<?php echo $this->img_width; ?>",
        img_height = "<?php echo $this->img_height; ?>",
        list_ca_id = "<?php echo $this->ca_id; ?>";

    function shop_list_type_fn(type){
        var $ul_sct = $("ul.sct");

        if(type == "gallery") {
            $ul_sct.removeClass("sct_20").addClass("sct_10")
            .find(".sct_li").attr({"style":"width:"+li_width+"%"});
        } else {
            $ul_sct.removeClass("sct_10").addClass("sct_20")
            .find(".sct_li").removeAttr("style");
        }

        if (typeof g5_cookie_domain != 'undefined') {
            set_cookie("ck_itemlist"+list_ca_id+"_type", type, 1, g5_cookie_domain);
        }
    }

    $("button.sct_lst_view").on("click", function() {
        var $ul_sct = $("ul.sct");

        if($(this).hasClass("sct_lst_gallery")) {
            shop_list_type_fn("gallery");
        } else {
            shop_list_type_fn("list");
        }
    });
});

// 할인율 표시
$('.discount-percent').each(function(){

    var custom_price = parseInt($(this).attr('data-prod-custom'));
    var prod_price = parseInt($(this).attr('data-prod-price'));
    var rate = 0;
    if (!isNaN(custom_price) && !isNaN(prod_price)) {
        rate = Math.round((custom_price - prod_price) / custom_price * 100);
    }
    $(this).text(rate + '%');

    if (rate <= 0){
        $(this).hide();
    }else{
        $(this).show();
    }
});

// 상품보관
function item_wish(f, it_id) {

    var f_data = $(f).serialize();
    $.ajax({
        type: "POST",
        data: f_data + "&it_id=" + it_id + "&mode=" + $('#wishValue').val(),
        url: g5_url+"/shop/ajax.wishupdate.php",
        cache: false,
        success: function(result) {
            if (result == 'w') {
                document.getElementById(it_id).className = 'icon icon-heart is-active';
                $('#wishValue').val('d');
                /* location.replace('/shop/wishlist.php');*/
            } else if (result == 'd') {
                document.getElementById(it_id).className = 'icon icon-heart';
                $('#wishValue').val('w');
            } else{
                if($('#ismember').val()){
                    alert('회원 전용 서비스 입니다.');
                    location.replace('/bbs/login.php');
                }
            }
        }
    });
}

</script>
<?php } ?>
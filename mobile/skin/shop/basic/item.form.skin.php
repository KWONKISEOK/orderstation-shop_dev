<?php
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가

// add_stylesheet('css 구문', 출력순서); 숫자가 작을 수록 먼저 출력됨
add_stylesheet('<link rel="stylesheet" href="'.G5_SHOP_CSS_URL.'/style.css">', 0);
?>
<!--  2017 리뉴얼한 테마 적용 스크립트입니다. 기존 스크립트를 오버라이드 합니다.   -->
<script src="<?php echo G5_JS_URL; ?>/shop.override.js?ver=<?php echo G5_JS_VER; ?>"></script>

<link rel="stylesheet" href="https://releases.flowplayer.org/7.2.7/skin/skin.css">

<script src="<?php echo G5_JS_URL; ?>/jquery.marquee.min.js"></script>
<script src="https://releases.flowplayer.org/7.2.7/flowplayer.min.js"></script>
<script src="https://www.youtube.com/iframe_api"></script>

<?php if($config['cf_kakao_js_apikey']) { ?>
<!-- <script src="https://developers.kakao.com/sdk/js/kakao.min.js"></script> -->
<script src="//developers.kakao.com/sdk/js/kakao.min.js"></script>
<!-- <script src="<?php echo G5_JS_URL; ?>/kakaolink.js"></script> -->

<?php } 
$c_status = 0;?>

<form name="fitem" action="<?php echo $action_url; ?>" method="post">
    <input type="hidden" name="it_id[]" value="<?php echo $it['it_id']; ?>">
    <input type="hidden" name="sw_direct">
    <input type="hidden" name="url">
    <input type="hidden" name="c_status" value="<?php echo $c_status ?>;">

    <input type="hidden" name="member_id" value="<?php echo $member['mb_id'] ?>">
    <!--

    ******   추가 내용 ******

     -->
    <!-- [D] 장바구니 클릭 시 : .open-option 추가 -->
    <div class="shop-box">
        <div class="btn-option-box">
            <!-- [D] .btn-toggle 추가시 옵션창 열고 닫힘 -->
            <button class="btn-option btn-toggle" type="button">
                <span class="sound_only">열기</span>
            </button>
        </div>
        <div class="option-area">
            <!-- 옵션 선택(상품) -->
            <!-- [D] 옵션이 펼쳐진 경우 .show 추가 (하단 스크립트 적용됨) -->
            <?php if ($option_item_new) { ?>
                <?php echo $option_item_new; // 선택옵션 ?>

                <script>
                    /* =====================================
                    * dropdown (임의 작성입니다. 작업시 지워주세요)
                    * =====================================*/
                    $('.option-dropdown').on({
                        click: function () {
                            $(this).find('.dropdown-menu').stop().toggleClass('show').slideToggle(200);
                            $(this).toggleClass('show');
                        }
                    })
                </script>
            <?php } ?>
            <!-- 선택한 상품 목록 (약사용) -->
            <!-- [D] .show 추가 시 보임 -->
            <?php if ($is_orderable) { ?>
                <div class="product-selected-group show" id="it_sel_option">
                    <!-- 선택 상품 옵션 VIEW (JS) -->
                    <?php
                    // 옵션이 없는 경우 선택 상품이 즉시 보이도록 처리
                    if (!$option_item) {
                        if (!$it['it_buy_min_qty'])
                            $it['it_buy_min_qty'] = 1;

                        ?>
                        <div class="product-selected-box is-opened">
                            <input type="hidden" name="io_type[<?php echo $it_id; ?>][]" value="0">
                            <input type="hidden" name="io_id[<?php echo $it_id; ?>][]" value="">
                            <input type="hidden" name="io_value[<?php echo $it_id; ?>][]" value="<?php echo $it['it_name']; ?>">
                            <input type="hidden" class="io_price" value="0">
                            <input type="hidden" class="io_stock" value="<?php echo $it['it_stock_qty']; ?>">
                            <span class="selected-item"><?php echo $it['it_name']; ?></span>
                            <!--<button type="button" class="icon icon-pop-close" title="닫기"></button>-->
                            <div class="input-count-box">
                                <button type="button" class="count-minus sit_qty_minus" title="빼기">
                                    <span class="sound_only">감소</span>
                                </button>
                                <span class="count-text">
                                    <input type="text" id="ct_qty_<?php echo $i; ?>" title="수량입력" name="ct_qty[<?php echo $it_id; ?>][]" value="<?php echo $it['it_buy_min_qty']; ?>" size="5" class="input-default input-count">
                                </span>
                                <button type="button" class="count-plus sit_qty_plus" title="더하기">
                                    <span class="sound_only">증가</span>
                                </button>
                            </div>
                            <span class="selected-price">
                               <span class="price"><?php
                                   if($is_drug == true){
                                       echo number_format($it['it_drug_price']*$it['it_buy_min_qty']);
                                   }else{
                                       echo number_format($it['it_price']*$it['it_buy_min_qty']);
                                   }
                                   ?></span><span class="unit">원</span>
                            </span>
                        </div>
                        <script>
                            $(function () {
                                price_calculate();
                            });
                        </script>
                    <?php } ?>
                </div>
            <?php } ?>
            <!-- 합계금액-->
            <div class="product-price-box">
                <p class="product-price-title">총 구매수량
                    <span class="count" id="it_total_cnt"><?php echo ($option_item) ? 0 : 1; ?></span>개
                </p>

                <p class="product-price-desc">합계
               <span class="total-price"><?php
                        if($is_drug == true){
                            echo number_format($it['it_drug_price']*$it['it_buy_min_qty']);
                        } else{
                            echo number_format($it['it_price']*$it['it_buy_min_qty']);
                        }?></span><span class="unit">원</span></p>

            </div>

        </div>
        <!-- 버튼목록 -->
        <div class="btn-set btn-set-lg">
            <!-- [D] .btn-open 추가시 옵션창 열림 -->
            <button class="btn-default btn-orange-border btn-open" value="장바구니" type="button" onclick="document.pressed=this.value; fitem_submit(document.fitem);">
                장바구니
            </button>
            <!-- [D] .btn-open 추가시 옵션창 열림 -->
            <button class="btn-default btn-orange btn-open" value="바로구매" type="button" onclick="document.pressed=this.value; fitem_submit(document.fitem);">
                바로구매
            </button>
            <button class="btn-default btn-orange-border btn-like <?php if (get_wishlist_my_item_check($it['it_id'])) echo 'is-active'; ?>" title="좋아요" type="button" onclick="item_wish(document.fitem, '<?php echo $it['it_id']; ?>');">
                <input type="hidden" id="wishValue" value="<?php echo (get_wishlist_my_item_check($it['it_id'])) ? 'd' : 'w'; ?>">
                <i class="icon icon-heart"></i>
            </button>
        </div>
    </div>

    <div id="sit_ov_wrap">
        <div class="product-box">
            <!-- 상품이미지  -->
            <div class="product-view-box">
                <h2 class="sound_only">상품 이미지 보기</h2>
                <div class="gallery-wrap">
                    <div class="gallery-slider">
                        <?php
                        $big_img_count = 0;
                        $thumbnails = array();
                        $thumb_img_w = 384; // 넓이
                        $thumb_img_h = 350; // 높이
                        for ($i = 1; $i <= 10; $i++) {
                            if (!$it['it_img'.$i])
                                continue;

                            $thumb = get_it_thumbnail($it['it_img'.$i], $thumb_img_w, $thumb_img_h);

                            if (!$thumb)
                                continue;

                            //echo '<img src="'.$thumb.'" alt="">';
                            echo '<div class="gallery">';
                            echo $thumb;
                            echo '</div>';
                        }

                        if (!$thumb) {
                            echo '<div class="no-image">';
                            echo '<img src="'.G5_SHOP_URL.'/img/no_image.png" alt="">';
                            echo '</div>';
                        }
                        ?>
                    </div>
                </div>

                <script>
                    $(document).ready( function () {
                        var sliderSettings = {
                            mode: 'horizontal',// 가로 방향 수평 슬라이드
                            speed: 500,        // 이동 속도를 설정
                            //pager: true,      // 현재 위치 페이징 표시 여부 설정
                            moveSlides: 1,     // 슬라이드 이동시 개수
                            controls: false,    // 이전 다음 버튼 노출 여부
                            adaptiveHeight: true,
                            pagerType: 'short',
                            onSliderLoad: function() {
                                $('.gallery-slider').css('visibility', 'visible');
                            }
                        };
                        var slider = $('.gallery-slider').bxSlider(sliderSettings);
                        $(window).resize(function(){
                            slider.reloadSlider();
                        }).trigger('resize');

                    });
                </script>

            </div>
            <!-- 상품 요약정보(정기주문)  -->
            <div class="product-detail-box">
                <a class="product-brand-link" href="/shop/listcomp.php?comp=<?php echo $it['comp_code']; ?>">
                    <span>브랜드관</span>
                    <i class="icon icon-brand-link">
                        <span class="sound_only">더보기</span>
                    </i>
                </a>
                <h2 class="product-title"><?php echo stripslashes($it['it_name']); ?>
                    <span class="sound_only">요약정보 및 구매</span>
                </h2>
                <div class="product-grade-box">
                    <div class="product-grade">
                        <div class="product-star-wrap">
                            <?php
                            // 평점을 소수점이하는 별도로 표시하기 위해 아래와 같이 변수 선언
                            $floor_score = floor($star_score);
                            $decimal_score = $star_score - $floor_score;
                            ?>

                            <?php for ($i=0; $i<$floor_score; $i++) { ?>
                                <span class="product-star">
                                    <!-- [D] inline 스타일로 퍼센트 수치 조정 -->
                                    <span class="product-star-value" style="width: 100%"></span>
                                </span>
                            <?php } ?>

                            <?php if ($decimal_score > 0) { ?>
                                <span class="product-star">
                                    <?php
                                    $decimal_score_per = $decimal_score * 100;
                                    ?>
                                    <!-- [D] inline 스타일로 퍼센트 수치 조정 -->
                                    <span class="product-star-value" style="width:<?php echo $decimal_score_per; ?>%"></span>
                                </span>
                            <?php } ?>
                        </div>
                        <span class="sound_only">별점 5점 중 <em><?php echo $star_score; ?></em>점</span>
                        <span class="grade"><?php echo $star_score; ?></span>
                        <span class="total-review">(
                            <span class="count"><?php echo $item_use_count; ?></span>
                            <span class="unit">건</span>
                            )
                        </span>
                    </div>
                    <div class="share-sns-box">
                        <a class="icon share-sns" href="javascript:;" title="SNS공유"></a>
                        <div class="sns-area">
                            <?php
                            $sns_title = get_text($it['it_name']).' | '.get_text($config['cf_title']);
                            $sns_url = G5_SHOP_URL.'/item.php?it_id='.$it['it_id'];
                            ?>
                            <?php echo get_sns_share_link('facebook', $sns_url, $sns_title, G5_MSHOP_SKIN_URL.'/img/facebook.png'); ?>
                            <?php echo get_sns_share_link('twitter', $sns_url, $sns_title, G5_MSHOP_SKIN_URL.'/img/twitter.png'); ?>
                            <?php echo get_sns_share_link('kakaotalk', $sns_url, $sns_title, G5_MSHOP_SKIN_URL.'/img/sns_kakao.png'); ?>
                            <a href="javascript:popup_item_recommend('<?php echo $it['it_id']; ?>');" id="sit_btn_rec">
                                <i class="fa fa-envelope-o" aria-hidden="true"></i>
                                <span class="sound_only">추천하기</span>
                            </a>
                        </div>
                    </div>
                    <script type='text/javascript'>
                        //<![CDATA[
                        // // 사용할 앱의 JavaScript 키를 설정해 주세요.
                        Kakao.init("<?php echo $config['cf_kakao_js_apikey']; ?>");
                        // // 카카오링크 버튼을 생성합니다. 처음 한번만 호출하면 됩니다.
                        Kakao.Link.createDefaultButton({
                            container: '#kakao-link-btn',
                            objectType: 'feed',
                            content: {
                                title: '<?php echo $it['it_name'];?>',
                                description: '',
                                imageUrl: '<?php echo get_it_imageurl($it['it_id']);?>',
                                link: {
                                    mobileWebUrl: '<?php echo $sns_url;?>',
                                    webUrl: '<?php echo $sns_url;?>'
                                }
                            },
                            /*social: {
                              likeCount: 286,
                              commentCount: 45,
                              sharedCount: 845
                            },*/
                            buttons: [{
                                title: '상품 보기',
                                link: {
                                    mobileWebUrl: '<?php echo $sns_url;?>',
                                    webUrl: '<?php echo $sns_url;?>'
                                }
                            }]
                        });
                        //]]>
                    </script>
                    <script>
                        /* =====================================
                       * sns 기존스크립트 임시 붙여놓음
                       * =====================================*/
                        $(".share-sns").click(function () {
                            $(".sns-area").show();
                        });
                        $(document).mouseup(function (e) {
                            var container = $(".sns-area");
                            if (container.has(e.target).length === 0)
                                container.hide();
                        });
                    </script>
                </div>
                <?php if ($is_drug == true) { ?>
                <p class="product-price-order">약국공급가</p>
                <?php } ?>
                <div class="product-price-set">
                    <!-- <div class="product-original-group"> -->
                    <span class="product-sale-price">
                            <?php if (!$it['it_use']) { // 판매가능이 아닐 경우 ?>
                                <span class="price"><?php echo display_price(get_price($it)); ?></span>
                                <span class="unit">원</span>
                            <?php } else if ($it['it_tel_inq']) { // 전화문의일 경우 ?>
                                <span class="price">전화문의</span>
                            <?php } else { // 전화문의가 아닐 경우 ?>
                                <?php if ($is_drug == true) { ?>
                                    <span class="price"><?php echo display_price($it['it_drug_price']); ?></span>
                                    <span class="unit">원</span>~
                                    <input type="hidden" id="it_drug_price" value="<?php echo $it['it_drug_price']; ?>">
                                <?php } else { ?>
                                    <span class="price"><?php echo display_price(get_price($it)); ?></span>
                                    <span class="unit">원</span>~
                                <?php } ?>
                                  <input type="hidden" id="it_price" value="<?php if($is_drug == true){echo $it['it_drug_price'];}else{echo get_price($it);} ?>">
                            <?php } ?>
                        </span>
                    <!-- </div> -->
                    <div class="product-original-group">
                        <span class="product-original-price">
                            <?php if ($is_drug == true) {
                                if($it['it_cust_price'] > 0 ){?>
                                    <span class="price"><?php echo display_price($it['it_cust_price']); ?></span>
                                <? }else{?>
                                    <span class="price"><?php echo display_price(get_price($it)); ?></span>
                                <? }?>
                                <span class="unit">원</span>
                            <?php } else { ?>
                                <?php if ($it['it_cust_price']) { ?>
                                    <span class="price"><?php echo display_price($it['it_cust_price']); ?></span>
                                    <span class="unit">원</span>
                                <?php } ?>
                            <?php } ?>
                        </span>
                        <?php if ($is_drug == true) { ?>
                            <!-- 할인정보 툴팁 / 툴팁 열릴때 .is-opened 추가(스크립트 하기 적용됨) -->
                            <div class="tooltip-wrapper">
                                <a class="open-tooltip icon icon-noti" href="javascript:;"></a>
                                <div class="tooltip-layer">
                                    <div class="tooltip-wrap">

                                        <h2 class="tooltip-title">결제 혜택 안내</h2>
                                        <div class="tooltip-cont" style="padding-top: 8px">
                                            <div class="tooltip-payment">
                                                <div class="divide">
                                                    <dl class="price-info price-info-type2" style="margin-bottom: 15px">
                                                        <dt class="title">소비자가</dt>
                                                        <dd class="desc">
                                                          <span class="price"><?php
                                                              if($it['it_cust_price'] > 0){
                                                                echo number_format($it['it_cust_price']);
                                                              }else{
                                                                echo display_price(get_price($it));
                                                              }?>
                                                          </span>
                                                          <span class="unit">원</span>
                                                        </dd>
                                                    </dl>
                                                </div>
                                                <div class="divide">
                                                    <?php if($it['it_cust_price']>0){?>
                                                    <dl class="price-info price-info-type2" id="item_discount" style="margin:8px 0px;">
                                                        <dt class="title">상품할인</dt>
                                                        <dd class="desc">
                                                <span class="price">
                                                    <input type="hidden" class="item_discount" value="<?php echo $it['it_cust_price'] ?>">
                                                  - <?php echo number_format($it['it_cust_price'] - $it['it_price']);?></span>
                                                            <span class="unit">원</span>
                                                        </dd>
                                                    </dl>
                                                    <?php } ?>
                                                    <dl class="price-info price-info-type2" style="margin:8px 0px;">
                                                        <dt class="title">포인트</dt>
                                                        <dd class="desc">
                                                            <span class="price">- <?php echo number_format($it['it_price'] - $it['it_drug_price']); ?></span>
                                                            <span class="unit">원</span>
                                                        </dd>
                                                    </dl>
                                                </div>
                                                <div class="divide" >
                                                    <dl class="price-info price-info-type2 price-info-result" style="margin-top: 15px">
                                                        <dt class="title">총 약국 결제 금액</dt>
                                                        <dd class="desc">
                                                            <span class="price"><?php echo number_format($it['it_drug_price']); ?></span>
                                                            <span class="unit">원</span>
                                                        </dd>
                                                    </dl>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="tooltip-foot">
                                            <button type="button" class="close-tooltip btn-tooltip">확인</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
                <div class="product-info-box">
                    <dl class="list-style">
                        <dt class="title">배송정보</dt>
                        <dd class="desc">
                            <?php
                            $ct_send_cost_label = '배송비';
                            $code_comp = $it['comp_code'];

                            if ($it['it_sc_type'] == 1) {
                                $sc_method = '무료배송';
                            } else {
                                $sc_method = '유료배송';

                                /**
                                 * 배송비 결제 방식 - 현재 선불 밖에 없으므로 주석 처리
                                 */
                                /*if ($it['it_sc_method'] == 1) {
                                    $sc_method = '수령후 지불';
                                } else if ($it['it_sc_method'] == 2) {
                                    $ct_send_cost_label = '<label for="ct_send_cost">배송비결제</label>';
                                    $sc_method = '<select name="ct_send_cost" id="ct_send_cost">
                                              <option value="0">주문시 결제</option>
                                              <option value="1">수령후 지불</option>
                                          </select>';
                                } else {
                                    $sc_method = '유료배송';
                                }*/

                                if ($it['it_sc_type'] == '2') $trans_memo = '(동일 판매자 상품 '.number_format($it['it_sc_minimum']).'원 이상 구매 시 무료)';
                                if ($it['it_sc_type'] == '3') $trans_memo = '(동일 판매자 상품 묶음배송 가능)';
                                if ($it['it_sc_type'] == '4') $trans_memo = '('.$it['it_sc_qty'].'개 단위로 배송비 '.number_format($it['it_sc_price']).'원 추가 부과)';


                            }
                            ?>
                            <span><?php echo $sc_method; ?></span>
                            <?php if ($it['it_sc_type'] != 1) { ?>
                                <span class="bar"></span>
                                <span><?php echo number_format($it['it_sc_price']); ?>원</span>
                                <span style="margin-top: 5px;"><?php echo $trans_memo; ?></span>
                            <?php } ?>

                        </dd>
                    </dl>
                    <?php if($it['it_company'] != 0 && ($member['mb_referee'] == 4 || $member['mb_type'] == 9)){ ?>
                        <dl class="list-style">
                            <dt class="title">사내수령가능지점</dt>
                            <dd class="desc">
                                <span><input type="checkbox" name="it_company" value="1" <? if (preg_match('/1/',$it['it_company'])){ ?>checked<? } ?> onclick="return false;" >태전약품 전주 &nbsp;</br></span>
                                <span><input type="checkbox" name="it_company" value="2" <? if (preg_match('/2/',$it['it_company'])){ ?>checked<? } ?> onclick="return false;" >티제이팜 평택 &nbsp;</br></span>
                                <span><input type="checkbox" name="it_company" value="3" <? if (preg_match('/3/',$it['it_company'])){ ?>checked<? } ?> onclick="return false;" >오엔케이 서울 &nbsp;</span>
                            </dd>
                        </dl>
                    <?php }?>
                    <dl class="list-style">
                        <dt class="title">카드혜택</dt>
                        <dd class="desc">무이자 할부 혜택
                            <!-- 툴팁 시작 -->
                            <!-- 툴팁 열릴때 .is-opened 추가(스크립트 하기 적용됨) -->
                            <div class="tooltip-wrapper">
                                <a class="open-tooltip icon icon-noti" href="#;"></a>
                                <div class="tooltip-layer">
                                    <div class="tooltip-wrap">
                                        <div class="tooltip-container">
                                            <h2 class="tooltip-title">무이자 할부 혜택 안내</h2>
                                            <div class="tooltip-cont">
                                                <?php echo CardInfoText3(); ?>
                                            </div>
                                            <div class="tooltip-foot">
                                                <button type="button" class="close-tooltip btn-tooltip">확인</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <script>
                                /* =====================================
                                * tooltip (임의 작성입니다. 작업시 지워주세요)
                                * =====================================*/
                                tooltipFunc();

                                function tooltipFunc() {
                                    var tooltipBottomPosition;
                                    $(document).on('click', '.open-tooltip', function () {
                                        $('.tooltip-wrapper').removeClass('is-opened');
                                        $(this).parent('.tooltip-wrapper').addClass('is-opened');
                                        $('body').css('overflow', 'hidden');
                                        tooltipBottomPosition = $(this).offset().top + 20 - $(document).scrollTop(); // 툴팁 위치
                                        $(this).parent('.tooltip-wrapper').find(".tooltip-wrap").css({top: tooltipBottomPosition + "px"}).fadeIn(500);
                                    });
                                    $(document).on('click', '.close-tooltip', function () {
                                        $('.tooltip-wrapper').removeClass('is-opened');
                                        $('body').css('overflow', 'auto');
                                    });
                                    $('body').click(function (e) {
                                        if ($('.tooltip-wrapper').hasClass('is-opened')) {
                                            if (!$('.tooltip-wrapper').has(e.target).length) {
                                                $('.tooltip-wrapper').removeClass('is-opened');
                                            }
                                        }
                                    });
                                }
                            </script>
                        </dd>
                    </dl>
                    <?php
                    $item_point = get_price_cash($it['it_point_type'], $it['it_price'], $member['mb_grade']);
                    if($it['it_point_type'] == 1 && ($member['mb_referee'] == 4 || $member['mb_extra'] == 11)) {
                        if(true){ ?>
                            <dl class="list-style">
                                <dt class="title">적립금</dt>
                                <dd class="desc"><?php echo $item_point?> 원
                                </dd>
                            </dl>
                        <?php }
                    }?>
                </div>

                <?php if (($it['it_period_yn'] == 'Y' || $it['it_period_yn'] == 'O')) { ?>
                    <link rel="stylesheet" href="//code.jquery.com/ui/1.8.18/themes/base/jquery-ui.css" />
                    <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
                    <script>
                        $.datepicker.setDefaults({
                            dateFormat: 'yy-mm-dd',
                            prevText: '이전 달',
                            nextText: '다음 달',
                            monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
                            monthNamesShort: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
                            dayNames: ['일', '월', '화', '수', '목', '금', '토'],
                            dayNamesShort: ['일', '월', '화', '수', '목', '금', '토'],
                            dayNamesMin: ['일', '월', '화', '수', '목', '금', '토'],
                            showMonthAfterYear: true,
                            yearSuffix: '년', beforeShowDay: function (date) {
                                var day = date.getDay();
                                //return [(day != 0 && day != 6)];
                                var every_day = date.getDate();
                                if (day == 0 || day == 6) {
                                    return [false, ''];
                                } else {
                                    return [true, ''];
                                }
                            }
                        });

                        $(function () {
                            $(".datepickerbutton").datepicker({minDate: 0});
                        });
                    </script>

                    <!-- 옵션 선택 (공통-정기주문) -->
                    <div class="product-option-group">
                        <!-- 상위 옵션 선택 -->
                        <div class="product-option-box option-main">
                            <dl class="list-style">
                                <dt class="title">정기배송</dt>
                                <dd class="desc">
                                    <div class="form-radio radio-sm">
                                        <input type="radio" name="ct_period_yn" id="ct_period_yny" value="Y">
                                        <label for="ct_period_yny">신청하기</label>
                                    </div>
                                    <input type="hidden" id="period_y" value="<?php echo $it['it_period_yn'] ?>">
                                    <?php if( $it['it_period_yn'] != 'O'){ ?>
                                    <div class="form-radio radio-sm">
                                        <input type="radio" name="ct_period_yn" id="ct_period_ynn" value="N" checked>
                                        <label for="ct_period_ynn">신청안함</label>
                                    </div>
                                    <?php } ?>
                                </dd>
                            </dl>
                        </div>
                        <!-- 하위 옵션 선택 -->
                        <div class="product-option-box option-sub" id="ct_period_area" style="display: none;">
                            <dl class="list-style">
                                <dt class="title"><?php echo ($it['it_id'] == "S02014") ? '결제날짜' : '배송날짜'; ?></dt>
                                <dd class="desc">
                                    <input type="text" class="input-default input-date datepickerbutton" name="ct_period_sdate" value="<?php echo date('Y-m-d'); ?>" readonly/>
                                </dd>
                            </dl>
                            <dl class="list-style">
                                <dt class="title"><?php echo ($it['it_id'] == "S02014") ? '결제주기' : '배송주기'; ?></dt>
                                <dd class="desc">
                                    <div class="form-radio radio-sm">
                                        <input type="radio" name="ct_period_week" value="1" id="delivery-1week" checked="checked">
                                        <label for="delivery-1week">1주 간격</label>
                                    </div>
                                    <div class="form-radio radio-sm">
                                        <input type="radio" name="ct_period_week" value="2" id="delivery-2week">
                                        <label for="delivery-2week">2주 간격</label>
                                    </div>
                                    <div class="form-radio radio-sm">
                                        <input type="radio" name="ct_period_week" value="3" id="delivery-3week">
                                        <label for="delivery-3week">3주 간격</label>
                                    </div>
                                    <div class="form-radio radio-sm">
                                        <input type="radio" name="ct_period_week" value="4" id="delivery-4week">
                                        <label for="delivery-4week">4주 간격</label>
                                    </div>
                                    <div class="form-radio radio-sm">
                                        <input type="radio" name="ct_period_week" value="5" id="delivery-select">
                                        <label for="delivery-select">날짜 지정</label>
                                    </div>
                                </dd>
                            </dl>
                            <?php if ($it['it_id'] == "S02014") { ?>
                                <dl class="list-style">
                                    <dt class="title">결제횟수</dt>
                                    <dd class="desc">
                                        <div class="input-count-box">
                                            <button type="button" class="count-minus" title="빼기" onclick="fnc_suselect3(0,'ct_period_cnt')"></button>
                                            <input type="text" name="ct_period_cnt" id="ct_period_cnt" title="수량입력" maxlength="2" size="3" onKeyPress="onlyNumber()" value="5" readonly class="input-default input-count">
                                            <button type="button" class="count-plus" title="더하기" onclick="fnc_suselect3(1,'ct_period_cnt')"></button>
                                        </div>
                                        <!--<span class="input-guide"><b>1주</b>마다 <b>1</b>박스씩 <b>1</b>회 배송됩니다.</span>-->
                                    </dd>
                                </dl>
                            <?php } else { ?>
                                <dl class="list-style">
                                    <dt class="title">배송횟수</dt>
                                    <dd class="desc">
                                        <div class="input-count-box">
                                            <input type="hidden" class="it_period_min" value="<?php echo $it['it_period_min'] ?>" >
                                            <button type="button" class="count-minus" title="빼기" onclick="fnc_suselect4(0,'ct_period_cnt')"></button>
                                            <span class="count-text">
                                                  <input type="text" name="ct_period_cnt" id="ct_period_cnt" title="수량입력" maxlength="2" size="3" onKeyPress="onlyNumber()" value="<?php echo round(($it['it_period_min']+$it['it_period_max'])/2) ?>" readonly class="input-default input-count">
                                             </span>
                                            <input type="hidden" class="it_period_max" value="<?php echo $it['it_period_max'] ?>" >
                                            <button type="button" class="count-plus" title="더하기" onclick="fnc_suselect4(1,'ct_period_cnt')"></button>
                                        </div>
                                        <!--<span class="input-guide"><b>1주</b>마다 <b>1</b>박스씩 <b>1</b>회 배송됩니다.</span>-->
                                    </dd>
                                </dl>
                                <dl class="list-style">
                                    <dt class="title">배송메모</dt>
                                    <dd class="desc">
                                        <div class="input-count-box">
                                            <input type="text"  style="padding-top:8px;"name="ct_period_memo" id="ct_period_memo" size="50" maxlength="50" value="" class="input-default">
                                        </div>
                                    </dd>
                                </dl>
                            <?php } ?>
                        </div>
                    </div>
                <?php } ?>

            </div>
        </div>
    </div>

    <style>
        .res-iframe {

            width: 100%;
            height: 100%;
            border: 0;

        }

        .flowplayer {

            height: auto;
            background-color: #fff;
            background-size: cover;
            background-image: url("../data/editor/player_thumb_m.jpg");

        }

        .sellingpoint img {
            text-align: center;

        }

        .sellingpoint table {
            text-align: center;
            width: 100%;
        }

        .sellingpoint tr td {
            padding: 6px;
            border: 1px solid #444444;
        }

        .sellingpoint th {
            background-color: #efefef;
        }

        .sellingpoint td {
            text-align: left;
        }

    </style>
    <?php
    $sql = "select a.it_id,b.it_player,b.it_player_you from {$g5['g5_shop_item_table']} a,{$g5['g5_shop_item_player_table']} b where a.it_id='$it_id' and b.it_id='$it_id' and a.it_only_mobile_display = 0";
    $it_p = sql_fetch($sql);

    ?>
    <div id="sit_tab">
        <ul class="tab_tit">
            <li>
                <button type="button" rel="#sit_inf" class="selected">상품정보</button>
            </li>
            <li>
                <button type="button" rel="#sit_use">사용후기</button>
            </li>
            <li>
                <button type="button" rel="#sit_qa">상품문의</button>
            </li>
            <li>
                <button type="button" rel="#sit_dvex">배송/교환</button>
            </li>
        </ul>
        <ul class="tab_con">

            <!-- 상품 정보 시작 { -->
            <li id="sit_inf">
                <h2 class="contents_tit">
                    <span>상품 정보</span>
                </h2>
                <?php if ($it_p['it_player_you'] == 0 and $it_p['it_player'] != "") {//유튜브아니면 ?>
                    <p align="center">
                    <div class="flowplayer">
                        <video autoplay>
                            <source type='video/mp4;codecs="avc1.42E01E, mp4a.40.2"' src="<?php echo $it_p['it_player']; ?>">
                        </video>
                    </div>
                    <?php if ($it_p['it_id'] == "307004") { ?>
                        <p align="center"></p>
                        <div>
                            <div class="flowplayer">
                                <video>
                                    <source type='video/mp4;codecs="avc1.42E01E, mp4a.40.2"' src="https://www.orderstation.co.kr/data/editor/playfile/5문5답_보이로 체온계_MH6.mp4">
                                </video>
                            </div>
                        </div>
                    <?php } ?>
                <?php }
                if ($it_p['it_player_you'] == 1 && $it_p['it_player'] != "") { ?>
                    <div>

                        <!--<p align="center">-->
                        <center>
                            <div id="video-placeholder" style="width:100%;"></div>
                        </center>


                        <!--<iframe id=it_players class="res-iframe" src ="<?php echo $it_p['it_player'].'?autoplay=0&rel=0'; ?>" gesture="media"></iframe>-->

                        <!--<iframe width="300" height="206" src="https://www.youtube.com/embed/cNfbDK9vIFI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        -->
                    </div>
                <?php } ?>

                <h3>상품 상세설명</h3>
                <? if (!empty($it['it_box_txt'])) { ?>
                    <!--모바일 버전 박스-->
                    <div class="choice_wrap_2" style="line-height: normal;">
                        <div class="choice_box1">
                            <div class="box1_left">
                                <span>선택</span>
                                <a>1</a>
                            </div>
                            <div class="box1_right">
                                <div class="br_left1">
                                    <span><?=$it['it_name']?></span>
                                    <?=$it["it_box_txt"]?>
                                </div>
                                <div class="br_right1">
                                    <img src="<?php echo G5_DATA_URL; ?>/item/<?php echo $it["it_box_img"]; ?>">
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--모바일 버전 박스-->
                <? } ?>

                <? if ($it['it_opt_box'] == "1") { ?>

                    <?
                    $sql2 = " select * from {$g5['g5_shop_item_option_table']} where io_type = '0' and it_id = '$it_id' and io_use = '1' order by io_no asc ";
                    $result2 = sql_query($sql2);

                    for ($i = 0; $row2 = sql_fetch_array($result2); $i++) {
                        ?>

                        <!--모바일 버전 박스-->
                        <div class="choice_wrap_2" style="line-height: normal;">
                            <div class="choice_box1">
                                <div class="box1_left">
                                    <span>선택</span>
                                    <a><?=$i + 1?></a>
                                </div>
                                <div class="box1_right">
                                    <div class="br_left1">
                                        <span><?=str_replace("선택".($i + 1).")", "", $row2["io_name"])?></span>
                                        <?=$row2["io_txt"]?>
                                    </div>
                                    <div class="br_right1">
                                        <img src="<?php echo G5_DATA_URL; ?>/option/<?php echo $row2["io_img"]; ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--모바일 버전 박스-->
                        <?
                    }

                }
                ?>

                <?php if ($it['it_mobile_explan']) { // 상품 상세설명 ?>

                    <div id="sit_inf_explan" class="sellingpoint">
                        <?
                        //결합이미지 사용일때.
                        if ($it['it_orderbox_yn'] == "Y") {

                            $changecode = "
							<div class=\"up_divwrap2\">
								<i class=\"fa  fa-plus-circle\"></i>
								<a class='marquee'> ".$it['it_orderbox_txt']." </a><img src=\"/img/over_img.png\" class='over_img'>
							</div>			
						";
                            $it['it_mobile_explan'] = str_replace("{결합이미지}", $changecode, $it['it_mobile_explan']);
                            $it['it_explan'] = str_replace("{결합이미지}", $changecode, $it['it_explan']);

                        } else {

                            $it['it_mobile_explan'] = str_replace("{결합이미지}", "", $it['it_mobile_explan']);
                            $it['it_explan'] = str_replace("{결합이미지}", "", $it['it_explan']);

                        }
                        ?>
                        <?php echo($it['it_mobile_explan'] ? conv_content($it['it_mobile_explan'], 1) : conv_content($it['it_explan'], 1)); ?>
                    </div>


                <?php } ?>

                <?php
                if ($it['it_info_value']) { // 상품 정보 고시
                    $info_data = unserialize(stripslashes($it['it_info_value']));
                    if (is_array($info_data)) {
                        $gubun = $it['it_info_gubun'];
                        $info_array = $item_info[$gubun]['article'];
                        ?>
                        <h3>상품 정보 고시</h3>
                        <table id="sit_inf_open">
                            <tbody>
                            <?php
                            foreach ($info_data as $key => $val) {
                                $ii_title = $info_array[$key][0];
                                $ii_value = $val;
                                ?>
                                <tr>
                                    <th scope="row"><?php echo $ii_title; ?></th>
                                    <td><?php echo $ii_value; ?></td>
                                </tr>
                            <?php } //foreach?>
                            </tbody>
                        </table>
                        <!-- 상품정보고시 end -->
                        <?php
                    } else {
                        if ($is_admin) {
                            echo '<p>상품 정보 고시 정보가 올바르게 저장되지 않았습니다.<br>config.php 파일의 G5_ESCAPE_FUNCTION 설정을 addslashes 로<br>변경하신 후 관리자 &gt; 상품정보 수정에서 상품 정보를 다시 저장해주세요. </p>';
                        }
                    }
                } //if
                ?>


            </li>
            <!-- 사용후기 시작 { -->
            <li id="sit_use">
                <h2>사용후기</h2>

                <div id="itemuse"><?php include_once(G5_SHOP_PATH.'/itemuse.php'); ?></div>
            </li>
            <!-- } 사용후기 끝 -->

            <!-- 상품문의 시작 { -->
            <li id="sit_qa">
                <h2>상품문의</h2>

                <div id="itemqa"><?php include_once(G5_SHOP_PATH.'/itemqa.php'); ?></div>
            </li>
            <!-- } 상품문의 끝 -->

            <?php if ($default['de_baesong_content']) { // 배송정보 내용이 있다면 ?>
            <!-- 배송정보 시작 { -->
            <li id="sit_dvex">
                <h2>배송/교환정보</h2>
                <div id="sit_dvr">
                    <h3>배송정보</h3>

                    <?php echo conv_content($default['de_baesong_content'], 1); ?>
                </div>
                <!-- } 배송정보 끝 -->
                <?php } ?>


                <?php if ($default['de_change_content']) { // 교환/반품 내용이 있다면 ?>
                    <!-- 교환/반품 시작 { -->
                    <div id="sit_ex">
                        <h3>교환/반품</h3>

                        <?php echo conv_content($default['de_change_content'], 1); ?>
                    </div>
                    <!-- } 교환/반품 끝 -->
                <?php } ?>
            </li>
        </ul>
    </div>

    <script>

        $('.marquee').marquee({
            //speed in milliseconds of the marquee
            duration: 8000,
            //gap in pixels between the tickers
            gap: 80,
            //time in milliseconds before the marquee will start animating
            delayBeforeStart: 1000,
            //'left' or 'right'
            direction: 'left',
            //true or false - should the marquee be duplicated to show an effect of continues flow
            duplicated: true,
            startVisible: true

        });

        $(function () {
            $(".tab_con>li").hide();
            $(".tab_con>li:first").show();
            $(".tab_tit li button").click(function () {
                $(".tab_tit li button").removeClass("selected");
                $(this).addClass("selected");
                $(".tab_con>li").hide();
                $($(this).attr("rel")).show();
            });

            /*// 결제 혜택 안내 팝업
            if($(".item_discount").val() > 0) {
                document.getElementById("item_discount").style.display="";
            }else{
                document.getElementById("item_discount").style.display="none";
            }*/
        });

        function onYouTubeIframeAPIReady() {

            var player;

            player = new YT.Player('video-placeholder', {
                height: 350,
                videoId: '<?php echo $it_p['it_player']?>',
                playerVars: {rel: 0} //추천영상 안보여주게 설정
            });
        }

        onYouTubeIframeAPIReady();
    </script>
</form>

<?php if($default['de_mobile_rel_list_use']) { ?>
    <!-- 관련상품 시작 { -->
    <section id="sit_rel">
        <h2>관련상품</h2>
        <div class="sct_wrap">
            <?php
            $rel_skin_file = $skin_dir.'/'.$default['de_mobile_rel_list_skin'];
            if(!is_file($rel_skin_file))
                $rel_skin_file = G5_MSHOP_SKIN_PATH.'/'.$default['de_mobile_rel_list_skin'];

            $sql = " select b.* from {$g5['g5_shop_item_relation_table']} a left join {$g5['g5_shop_item_table']} b on (a.it_id2=b.it_id) where a.it_id = '{$it['it_id']}' and b.it_use='1' ";
            $list = new item_list($rel_skin_file, $default['de_mobile_rel_list_mod'], 0, $default['de_mobile_rel_img_width'], $default['de_mobile_rel_img_height']);
            $list->set_query($sql);
            echo $list->run();
            ?>
        </div>
    </section>
    <!-- } 관련상품 끝 -->
<?php } ?>

<script>
    /* =====================================
    * [D] 퍼블 확인 용
    * =====================================*/
    $(document).on('click', '.shop-box .btn-toggle', function(e) {
        var $this = $(this);
        if ($this.parents('.shop-box').hasClass('open-option')) {
            $this.parents('.shop-box').removeClass('open-option');
            $this.parents('.shop-box').find('.option-area').slideUp(200);
        } else {
            $this.parents('.shop-box').addClass('open-option');
            $this.parents('.shop-box').find('.option-area').slideDown(200);
        }
    });
    $(document).on('click', '.shop-box .btn-open', function(e) {
        var $this = $(this);
        if ($this.parents('.shop-box').hasClass('open-option')) {

        } else {
            $this.parents('.shop-box').addClass('open-option');
            $this.parents('.shop-box').find('.option-area').slideDown(200);
        }
    });
    $("input[name=ct_period_yn]").click(function () {
        if ($(this).val() == "Y") {
            //$("#ct_period_table").show();
            $("#ct_period_area").show();
        } else {
            //$("#ct_period_table").hide();
            $("#ct_period_area").hide();
        }

    });
</script>

<script>
    function fnc_suselect2(str_gbn, obj) {
        if (str_gbn == 0) {
            if (parseInt(document.getElementById(obj).value) > 2) {
                document.getElementById(obj).value = parseInt(document.getElementById(obj).value) - 1;
            }
        } else {
            if (parseInt(document.getElementById(obj).value) < 5) {
                document.getElementById(obj).value = parseInt(document.getElementById(obj).value) + 1;
            }
        }
    }

    function fnc_suselect3(str_gbn, obj) {
        if (str_gbn == 0) {
            if (parseInt(document.getElementById(obj).value) > 2) {
                document.getElementById(obj).value = parseInt(document.getElementById(obj).value) - 1;
            }
        } else {
            if (parseInt(document.getElementById(obj).value) < 12) {
                document.getElementById(obj).value = parseInt(document.getElementById(obj).value) + 1;
            }
        }
    }

    // 배송횟수 설정
    function fnc_suselect4(str_gbn, obj) {

        var it_period_min = $(".it_period_min").val();
        var it_period_max = $(".it_period_max").val();

        if (str_gbn == 0) {
            if (parseInt(document.getElementById(obj).value) > it_period_min) {
                document.getElementById(obj).value = parseInt(document.getElementById(obj).value) - 1;
            }
        } else {
            if (parseInt(document.getElementById(obj).value) < it_period_max) {
                document.getElementById(obj).value = parseInt(document.getElementById(obj).value) + 1;
            }
        }
    }
</script>

<script>

    $(window).bind("pageshow", function(event) {
        if (event.originalEvent.persisted) {
            document.location.reload();
        }
    });

    $(function(){
        var period_y =  $("#period_y").val();
        if( period_y== 'O'){
            $("#ct_period_yny").attr("checked", true);
            $("#ct_period_area").show();
        }

        // 상품이미지 슬라이드
        var time = 500;
        var idx = idx2 = 0;
        var slide_width = $("#sit_pvi_slide").width();
        var slide_count = $("#sit_pvi_slide li").size();
        $("#sit_pvi_slide li:first").css("display", "block");
        if(slide_count > 1)
            $(".sit_pvi_btn").css("display", "inline");

        $("#sit_pvi_prev").click(function() {
            if(slide_count > 1) {
                idx2 = (idx - 1) % slide_count;
                if(idx2 < 0)
                    idx2 = slide_count - 1;
                $("#sit_pvi_slide li:hidden").css("left", "-"+slide_width+"px");
                $("#sit_pvi_slide li:eq("+idx+")").filter(":not(:animated)").animate({ left: "+="+slide_width+"px" }, time, function() {
                    $(this).css("display", "none").css("left", "-"+slide_width+"px");
                });
                $("#sit_pvi_slide li:eq("+idx2+")").css("display", "block").filter(":not(:animated)").animate({ left: "+="+slide_width+"px" }, time,
                    function() {
                        idx = idx2;
                    }
                );
            }
        });

        $("#sit_pvi_next").click(function() {
            if(slide_count > 1) {
                idx2 = (idx + 1) % slide_count;
                $("#sit_pvi_slide li:hidden").css("left", slide_width+"px");
                $("#sit_pvi_slide li:eq("+idx+")").filter(":not(:animated)").animate({ left: "-="+slide_width+"px" }, time, function() {
                    $(this).css("display", "none").css("left", slide_width+"px");
                });
                $("#sit_pvi_slide li:eq("+idx2+")").css("display", "block").filter(":not(:animated)").animate({ left: "-="+slide_width+"px" }, time,
                    function() {
                        idx = idx2;
                    }
                );
            }
        });

        // 상품이미지 크게보기
        $(".popup_item_image").click(function() {
            var url = $(this).attr("href");
            var top = 10;
            var left = 10;
            var opt = 'scrollbars=yes,top='+top+',left='+left;
            popup_window(url, "largeimage", opt);

            return false;
        });
        /*
        $("#CardInfoText").click(function(){

            $("#card_info2").css("display", "none");
            $("#card_info").slideToggle(300);

        });
        $("#CardInfoText2").click(function(){

            $("#card_info").css("display", "none");
            $("#card_info2").slideToggle(300);

        });
        */
        $("#CardInfoText3").click(function(){

            $("#card_info3").slideToggle(300);

        });
        $(".shopping").click(function(){
            $("#modalLayer").fadeOut("slow");
        });

        $(".gocart").click(function(){
            if($("input[name=ct_period_yn]:checked").val() != 'Y'){
                location.replace("<?php echo G5_SHOP_URL.'/cart.php'?>");
            }else
            {
                location.replace("<?php echo G5_SHOP_URL.'/cart.php?period=y'?>");
            }
        });
    });

    $(document).on("click", "#delivery-select", function(){
        alert("원하시는 배송 날짜를 아래 배송메모에 입력 부탁드립니다.");
    });


    // 상품보관
    function item_wish(f, it_id)
    {
        var f_data = $(f).serialize();
        $.ajax({
            type: "POST",
            data: f_data + "&it_id=" + it_id + "&mode=" + $('#wishValue').val(),
            url: g5_url+"/shop/ajax.wishupdate.php",
            cache: false,
            success: function(result) {
                if (result == 'w') {
                    $('.btn-like').addClass('is-active');
                    $('#wishValue').val('d');
                } else if (result == 'd') {
                    $('.btn-like').removeClass('is-active');
                    $('#wishValue').val('w');
                } else {
                    alert(result);
                }
            }
        });

    }

    // 추천메일
    function popup_item_recommend(it_id)
    {
        if (!g5_is_member)
        {
            if (confirm("회원만 추천하실 수 있습니다."))
                document.location.href = "<?php echo G5_BBS_URL; ?>/login.php?url=<?php echo urlencode(G5_SHOP_URL."/item.php?it_id=$it_id"); ?>";
        }
        else
        {
            url = "<?php echo G5_SHOP_URL; ?>/itemrecommend.php?it_id=" + it_id;
            opt = "scrollbars=yes,width=616,height=420,top=10,left=10";
            popup_window(url, "itemrecommend", opt);
        }
    }

    // 재입고SMS 알림
    function popup_stocksms(it_id)
    {
        url = "<?php echo G5_SHOP_URL; ?>/itemstocksms.php?it_id=" + it_id;
        opt = "scrollbars=yes,width=616,height=420,top=10,left=10";
        popup_window(url, "itemstocksms", opt);
    }

    function fsubmit_check(f)
    {
        // 판매가격이 0 보다 작다면
        if (document.getElementById("it_price").value < 0) {
            alert("전화로 문의해 주시면 감사하겠습니다.");
            return false;
        }

        if($(".product-selected-box").size() < 1) {
            alert("상품의 선택옵션을 선택해 주십시오.");
            return false;
        }

        var val, io_type, result = true;
        var sum_qty = 0;
        var min_qty = parseInt(<?php echo $it['it_buy_min_qty']; ?>);
        var max_qty = parseInt(<?php echo $it['it_buy_max_qty']; ?>);
        var $el_type = $("input[name^=io_type]");

        $("input[name^=ct_qty]").each(function(index) {
            val = $(this).val();

            if(val.length < 1) {
                alert("수량을 입력해 주십시오.");
                result = false;
                return false;
            }

            if(val.replace(/[0-9]/g, "").length > 0) {
                alert("수량은 숫자로 입력해 주십시오.");
                result = false;
                return false;
            }

            if(parseInt(val.replace(/[^0-9]/g, "")) < 1) {
                alert("수량은 1이상 입력해 주십시오.");
                result = false;
                return false;
            }

            io_type = $el_type.eq(index).val();
            if(io_type == "0")
                sum_qty += parseInt(val);
        });

        if(!result) {
            return false;
        }

        if(min_qty > 0 && sum_qty < min_qty) {
            alert("선택옵션 개수 총합 "+number_format(String(min_qty))+"개 이상 주문해 주십시오.");
            return false;
        }

        if(max_qty > 0 && sum_qty > max_qty) {
            alert("선택옵션 개수 총합 "+number_format(String(max_qty))+"개 이하로 주문해 주십시오.");
            return false;
        }

        return true;
    }

    // 바로구매, 장바구니 폼 전송
    function fitem_submit(f) {
        f.action = "<?php echo $action_url; ?>";
        f.target = "";

        var member = $("input[name=member_id]").val();
        var period = $("input[name=ct_period_yn]:checked").val();
        if (document.pressed == "장바구니") {
            f.sw_direct.value = 0;
        } else { // 바로구매
            f.sw_direct.value = 1;
            if (period == 'Y' && member =='') {
                alert("정기배송은 회원전용 서비스 입니다. \n로그인 후 이용하여 주십시오.");
            }
        }

        // 판매가격이 0 보다 작다면
        if (document.getElementById("it_price").value < 0) {
            alert("전화로 문의해 주시면 감사하겠습니다.");
            return false;
        }

        if($(".shop-box.open-option").length < 1) {
            return false;
        }

        if ($(".product-selected-box").size() < 1) {
            alert("상품의 선택옵션을 선택해 주십시오.");
            return false;
        }

        var val, io_type, result = true;
        var sum_qty = 0;
        var min_qty = parseInt(<?php echo $it['it_buy_min_qty']; ?>);
        var max_qty = parseInt(<?php echo $it['it_buy_max_qty']; ?>);
        var $el_type = $("input[name^=io_type]");

        $("input[name^=ct_qty]").each(function (index) {
            val = $(this).val();

            if (val.length < 1) {
                alert("수량을 입력해 주십시오.");
                result = false;
                return false;
            }

            if (val.replace(/[0-9]/g, "").length > 0) {
                alert("수량은 숫자로 입력해 주십시오.");
                result = false;
                return false;
            }

            if (parseInt(val.replace(/[^0-9]/g, "")) < 1) {
                alert("수량은 1이상 입력해 주십시오.");
                result = false;
                return false;
            }

            io_type = $el_type.eq(index).val();
            if (io_type == "0")
                sum_qty += parseInt(val);
        });
        $("input[name^=io_id]").each(function (index) {
            val = $(this).val();
            if (val == '188018') {
                if ($("input[name=ct_period_yn]:checked").val() == 'N') {
                    alert("정기주문상품입니다. 정기주문을 신청해주세요.");
                    result = false;
                    return false;
                }
            }
        });


        if (!result) {
            return false;
        }

        if (min_qty > 0 && sum_qty < min_qty) {
            alert("선택옵션 개수 총합 " + number_format(String(min_qty)) + "개 이상 주문해 주십시오.");
            return false;
        }

        if (max_qty > 0 && sum_qty > max_qty) {
            alert("선택옵션 개수 총합 " + number_format(String(max_qty)) + "개 이하로 주문해 주십시오.");
            return false;
        }

        if (document.pressed == "장바구니") {

            f.c_status.value = '1';
            $.ajax({
                type: "POST",
                url: "<?php echo G5_SHOP_URL; ?>/cartupdate.php",
                data: $(f).serialize(),
                error: function (error) {
                    alert("장바구니에 담기지 않았습니다");
                    return false;
                },
                success: function (data) {

                    var marginLeft = $(".modalContent").outerWidth() / 2;
                    var marginTop = $(".modalContent").outerHeight() / 2;
                    if (period == 'Y' && member =='') {
                        alert("정기배송은 회원전용 서비스 입니다. \n로그인 후 이용하여 주십시오.");
                        window.location.href = "<?php echo G5_BBS_URL?>/login.php?period="+period;
                    } else {
                        $("#modalLayer").fadeIn("slow");
                        $(".modalContent").css({"margin-top": -marginTop, "margin-left": -marginLeft});
                        $('#right_cart2').html(data);
                    }

                    return false;
                }
            });
            return false;
        } else {
            f.submit();
            return true;
        }

    }

</script>

<style>
    .modal{
        width:90%;
        height:90%;
        position:fixed;
        left:0;
        top:0;
        z-index:10000;
    }

    #modalLayer{
        display:none;
    }
    #modalLayer .modalContent{
        width:300px;
        height:150px;
        padding:10px 10px 10px 10px;
        border:1px solid #EAEAEA;
        position:fixed;
        left:50%;
        top:45%;
        z-index:99;
        background:#fff;
    }
    #modalLayer .modalContent button{
        border-radius:3px;
        padding:15px 15px;
        cursor:pointer;
    }
    #modalLayer .content {
        margin-top:10px;
        text-align:center;
    }
    #modalLayer .content p{
        font-weight:700;
        padding:20px;
    }
    #modalLayer .content .shopping {
        color:#fff;
        padding:10px 15px;
        border:1px solid #FF8224;
        background:#FF8224;
        margin-right:10px;
    }
    #modalLayer .content .gocart {
        border-radius:3px;
        border:1px solid #d9d9d9;
        color:black;
        padding:10px 15px;
        background:#d9d9d9;
    }

</style>
<div id="modalLayer">
    <div class="modal">
        <div class="modalContent">
            <div class="content">
                <p><strong>선택하신 상품을 장바구니에 담았습니다.</strong></p>
                <button type="button" id="shopping" class="shopping">계속 쇼핑하기</button>
                <button type="button" id="gocart" class="gocart">장바구니 바로가기</button>
            </div>
        </div>
    </div>
</div>
<?php
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가

// add_stylesheet('css 구문', 출력순서); 숫자가 작을 수록 먼저 출력됨
add_stylesheet('<link rel="stylesheet" href="'.$member_skin_url.'/style.css">', 0);
?>

<div id="mb_confirm" class="mbskin">
    <h1><?php echo $g5['title'] ?></h1>

    <p>
        <strong>비밀번호를 한번 더 입력해주세요.</strong>
        <?php if ($url == 'member_leave.php') { ?>
        비밀번호를 입력하시면 회원탈퇴가 완료됩니다.
        <?php }else{ ?>
        회원님의 정보를 안전하게 보호하기 위해 비밀번호를 한번 더 확인합니다.
        <?php }  ?>
    </p>

    <form name="fmemberconfirm" id="fmemberconfirm" action="<?php echo $url ?>" onsubmit="return false;" method="post">
        <input type="hidden" name="mb_id" value="<?php echo $member['mb_id'] ?>">
        <input type="hidden" name="w" value="u">

        <fieldset>
            회원아이디
            <span id="mb_confirm_id"><?php echo $member['mb_id'] ?></span>
            <input type="password" name="mb_password" id="mb_confirm_pw" placeholder="비밀번호(필수)" required class="frm_input" size="15" maxLength="20">
            <input type="button" value="확인" id="btn_submit" class="btn_submit" onclick="fmemberconfirm_submit()">
            <input type="button" value="취소" class="btn_submit" style="border: 1px solid #ccc;background: #fff;color: #000; margin-top: 0px;" onclick="history.back();">
        </fieldset>
    </form>
</div>

<script>
    $(function() {
        $("#mb_confirm_pw").keypress(function(e) {
            if (e.keyCode == 13) {
                fmemberconfirm_submit();
            }
        });
    });
    function fmemberconfirm_submit(f) {
        var mode = $('input[name=w]').val();
        var mb_id = $('input[name=mb_id]').val();
        var mb_password = $('input[name=mb_password]').val();

        $.post(
            "/bbs/ajax.mb_pw_check.php",
            {w: mode, mb_id: mb_id, mb_password: mb_password},
            function (data) {
                var result =  $.parseJSON(data);
                if (result.type == 'ok') {
                    document.getElementById("btn_submit").disabled = true;
                    document.fmemberconfirm.submit();
                } else if (result.type == 'error') {
                    alert(result.msg);
                }
          });
    }
</script>

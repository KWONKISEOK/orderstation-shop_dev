<?php
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가

// add_stylesheet('css 구문', 출력순서); 숫자가 작을 수록 먼저 출력됨
add_stylesheet('<link rel="stylesheet" href="'.$member_skin_url.'/style.css">', 0);

// 아이디 자동저장 
$ck_id_save = get_cookie("ck_id_save"); 

if ($ck_id_save) { 
$ch_id_save_chk = "checked"; 
} 

?>
<!-- jinhee 추가 -->
<div class="sub">
    <h2 class="title-section">로그인</h2>
    <div class="form-login">
        <!-- [D]활성화 상태일 경우 : .active 추가 -->
        <div id="tab-login" class="tab-button-set">
            <button type="button" class="tab-button active"><span>회원 로그인</span></button>
            <button type="button" class="tab-button"><span>비회원 주문하기</span></button><!-- [D]비회원 주문하기일 경우 워딩 : 비회원 주문하기 -->
        </div>
        <div id="tab-login-panel" class="tab-panel-set">
            <div class="tab-panel">
                <fieldset id="" class="form-group-login">
                    <legend>회원로그인</legend>
                    <label for="" class="sound_only">회원아이디<strong class="sound_only"> 필수</strong></label>
                    <input type="text" name="" id="" required class="input-default required" size="20" maxLength="20" placeholder="아이디" value=''>
                    <label for="" class="sound_only">비밀번호<strong class="sound_only"> 필수</strong></label>
                    <input type="password" name="" id="" required class="input-default required" size="20" maxLength="20" placeholder="비밀번호">
                    <input type="submit" value="로그인" class="btn-default btn-large btn-orange">
                    <div class="form-set">
                        <div class="form-checkbox">
                            <input type='checkbox' id='check1' name=''/>
                            <label for="check1">로그인 상태 유지</label>
                        </div>
                        <div class="form-checkbox">
                            <input type="checkbox" id="check2" name=""/>
                            <label for="check2">아이디 저장</label>
                        </div>
                    </div>
                    <div class="sns-wrap">
                        <a href="" class="icon-sns icon-kakao" title="카카오">
                            <span class="ico"></span>
                            <span class="brand">KAKAO</span>
                        </a>
                        <a href="" class="icon-sns icon-facebook" title="페이스북">
                            <span class="ico"></span>
                            <span class="brand">FACEBOOK</span>
                        </a>
                        <a href="" class="icon-sns icon-naver" title="네이버">
                            <span class="ico"></span>
                            <span class="brand">NAVER</span>
                        </a>
                    </div>
                    <ul class="foot-section">
                        <h2 class="sound_only">회원로그인 안내</h2>
                        <li><a href="">아이디 찾기</a></li>
                        <li><a href="">비밀번호 찾기</a></li>
                        <li><a href="">회원가입</a></li>
                    </ul>
                </fieldset>
            </div>
            <div class="tab-panel">
                <fieldset id="" class="form-group-login">
                    <form name="forderinquiry" method="post" action="" autocomplete="off">
                        <legend>비회원 주문조회</legend>
                        <label for="" class="sound_only">주문자명</label>
                        <input type="text" name="" id="" class="input-default" size="20" maxLength="20" placeholder="주문자명" value="<?php echo $od_id; ?>" >
                        <label for="" class="sound_only">주문 비밀번호</label>
                        <input type="password" name="" id="" class="input-default" size="20" maxLength="20" placeholder="주문 비밀번호">
                        <input type="submit" value="주문조회 하기" class="btn-default btn-large btn-orange">
                        <p class="info">주문 번호 및 비밀번호를 잊으셨다면 고객센터(1544-5462)로 문의 바랍니다.</p>
                        <a href="" class="btn-default btn-join">아직 계정이 없으신가요? 오더 스테이션 회원가입</a>
                    </form>
                </fieldset>
            </div>

            <!-- [D] 비회원 주문하기 일 경우 -->
            <!-- <div class="tab-panel">
                <fieldset id="login_fs" class="form-group-login">
                    <legend>비회원 주문하기</legend>
                    <p class="guide-join">오더 스테이션 회원이 되시면 <b>약사님과 1:1 건강 상담 및</b><br/>
                        <b>다양한 상품과 혜택</b>을 만나보실 수 있습니다.
                    </p>
                    <input type="submit" value="비회원 주문하기" class="btn-default btn-large btn-orange">
                    <p class="info">주문 번호 및 비밀번호를 잊으셨다면 고객센터(1544-5462)로 문의 바랍니다.</p>
                    <a href="./register.php" class="btn-default btn-join">아직 계정이 없으신가요? 오더 스테이션 회원가입</a>
                </fieldset>
            </div> -->

    </div>
</div>
<script>
    /* =====================================
    * tab
    * =====================================*/
    $(function() {
        tab('#tab-login',0); //로그인 탭
    });
    function tab(e, num){
        // console.log("e",e);
        var num = num || 0;
        var menu = $(e).children();
        var con = $(e+'-panel').children();
        var select = $(menu).eq(num);
        var i = num;
    
        select.addClass('active');
        con.eq(num).show();
        menu.click(function(){
            if(select!==null){
                select.removeClass("active");
                con.eq(i).hide();
            }
            select = $(this);	
            i = $(this).index();
            select.addClass('active');
            con.eq(i).show();
        });
    }
</script>


<div id="mb_login" class="mbskin">
    <h1><?php echo $g5['title'] ?></h1>

    <form name="flogin" action="<?php echo $login_action_url ?>" onsubmit="return flogin_submit(this);" method="post" id="flogin">
    <input type="hidden" name="url" value="<?php echo $login_url ?>">

    <div id="login_frm">
        <label for="login_id" class="sound_only">아이디<strong class="sound_only"> 필수</strong></label>
        <input type="text" name="mb_id" id="login_id" placeholder="아이디(필수)" required class="frm_input required" maxLength="20" value='<?=$ck_id_save?>'>
        <label for="login_pw" class="sound_only">비밀번호<strong class="sound_only"> 필수</strong></label>
        <input type="password" name="mb_password" id="login_pw" placeholder="비밀번호(필수)" required class="frm_input required" maxLength="20">
        <div>
			<input type='checkbox' id='id_save' name='id_save' <?=$ch_id_save_chk?>>아이디 저장 &nbsp
            <input type="checkbox" name="auto_login" id="login_auto_login">
            <label for="login_auto_login">자동로그인</label>
        </div>
       <input type="submit" value="로그인" class="btn_submit">
    </div>

    <?php
    // 소셜로그인 사용시 소셜로그인 버튼
    //@include_once(get_social_skin_path().'/social_login.skin.php');
    ?>

    <section class="mb_login_join">
        <h2>회원로그인 안내</h2>
        
        <div>
			<a href="<?php echo G5_BBS_URL ?>/password_lost.php">회원정보찾기</a>
            <a href="./register.php">회원 가입</a>
        </div>
		
		
	
	
    </section>
<br>
	<a href="../shop/index.php">홈으로가기</a>

    </form>


    <?php // 쇼핑몰 사용시 여기부터 ?>
    <?php if ($default['de_level_sell'] == 1) { // 상품구입 권한 ?>

    <!-- 주문하기, 신청하기 -->
    <?php if (preg_match("/orderform.php/", $url)) { ?>

    <section id="mb_login_notmb" class="mbskin">
        <h2>비회원 구매</h2>

        <p>
            비회원으로 주문하시는 경우 포인트는 지급하지 않습니다.
        </p>

        <div id="guest_privacy">
            <?php echo $default['de_guest_privacy']; ?>
        </div>

        <label for="agree">개인정보수집에 대한 내용을 읽었으며 이에 동의합니다.</label>
        <input type="checkbox" id="agree" value="1">

        <div class="btn_confirm">
            <a href="javascript:guest_submit(document.flogin);" class="btn_submit">비회원으로 구매하기</a>
        </div>

        <script>
            function guest_submit(f) {
                if (document.getElementById('agree')) {
                    if (!document.getElementById('agree').checked) {
                        alert("개인정보수집에 대한 내용을 읽고 이에 동의하셔야 합니다.");
                        return;
                    }
                }
                f.url.value = "<?php echo $url; ?>";
                f.action = "<?php echo $url; ?>";
                f.submit();
            }
        </script>
    </section>

    <?php } else if (preg_match("/orderinquiry.php$/", $url)) { ?>
    <div class="mbskin" id="mb_login_od_wr">
        <h2>비회원 주문조회 </h2>

        <fieldset id="mb_login_od">
            <legend>비회원 주문조회</legend>

            <form name="forderinquiry" method="post" action="<?php echo urldecode($url); ?>" autocomplete="off">

                <label for="od_id" class="od_id sound_only">주문번호<strong class="sound_only"> 필수</strong></label>
                <input type="text" name="od_id" value="<?php echo $od_id; ?>" id="od_id" required class="frm_input required" size="20" placeholder="주문번호">
                <label for="id_pwd" class="od_pwd sound_only">비밀번호<strong class="sound_only">
                        필수</strong></label>
                <input type="password" name="od_pwd" size="20" id="od_pwd" required class="frm_input required" placeholder="비밀번호">
                <input type="submit" value="확인" class="btn_submit">

            </form>
        </fieldset>

        <section id="mb_login_odinfo">
            <p>메일로 발송해드린 주문서의 <strong>주문번호</strong> 및 주문 시 입력하신 <strong>비밀번호</strong>를 정확히 입력해주십시오.</p>
        </section>

    </div>
    <?php } ?>

    <?php } ?>
    <?php // 쇼핑몰 사용시 여기까지 반드시 복사해 넣으세요 ?>

</div>

<script>
$(function(){
    $("#login_auto_login").click(function(){
        if (this.checked) {
            this.checked = confirm("자동로그인을 사용하시면 다음부터 회원아이디와 비밀번호를 입력하실 필요가 없습니다.\n\n공공장소에서는 개인정보가 유출될 수 있으니 사용을 자제하여 주십시오.\n\n자동로그인을 사용하시겠습니까?");
        }
    });
});

function flogin_submit(f)
{
	if( /Android/i.test(navigator.userAgent)) {
	    // Android
		window.Login.justDoIt(f.mb_id.value);
	} else if (/iPhone|iPad|iPod/i.test(navigator.userAgent)) {
	    // iOS
		webkit.messageHandlers.callbackHandler.postMessage(f.mb_id.value);
	}
	
	return true;
}


</script>

<?php
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가

// add_stylesheet('css 구문', 출력순서); 숫자가 작을 수록 먼저 출력됨
add_stylesheet('<link rel="stylesheet" href="'.$member_skin_url.'/style.css">', 0);

// 토크 생성 
$token = md5(uniqid(rand(), true)); 
set_session("ss_token", $token); 

?>
<style>

input[type=text], input[type=password] {
    /*height: 25px;*/
    height: 28px;
    line-height: 28px;
    padding: 2px 4px;
	width:40px;
	/*width:80px;*/
    border: 1px solid #d5d5d5;
    color: #353535;
    font-size: 12px;
}
.btn_wt_s {
    vertical-align: middle;
    padding: 0 7px;
    border: 1px solid #787878;
    color: #333;
    line-height: 28px;
    background: #ffffff;
    background: -moz-linear-gradient(top, #ffffff 0%, #f3f3f3 100%);
    background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#ffffff), color-stop(100%,#f3f3f3));
    background: -webkit-linear-gradient(top, #ffffff 0%,#f3f3f3 100%);
    background: -o-linear-gradient(top, #ffffff 0%,#f3f3f3 100%);
    background: -ms-linear-gradient(top, #ffffff 0%,#f3f3f3 100%);
    background: linear-gradient(to bottom, #ffffff 0%,#f3f3f3 100%);
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#f3f3f3',GradientType=0 );
}
.btn-base {
    display: inline-block;
    border-radius: 3px;
    /*line-height: 300%;*/
    font-size: 12px;
}
.btn_org {
    border: 1px solid #db7a0c;
    color: #fff;
	cursor:pointer;
    background: #f08f21;
    background: -moz-linear-gradient(top, #f08f21 0%, #dc7a0b 100%);
    background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#f08f21), color-stop(100%,#dc7a0b));
    background: -webkit-linear-gradient(top, #f08f21 0%,#dc7a0b 100%);
    background: -o-linear-gradient(top, #f08f21 0%,#dc7a0b 100%);
    background: -ms-linear-gradient(top, #f08f21 0%,#dc7a0b 100%);
    background: linear-gradient(to bottom, #f08f21 0%,#dc7a0b 100%);
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f08f21', endColorstr='#dc7a0b',GradientType=0 );
}
a.btn_wt {
    border: 1px solid #adadad;
    color: #333;
    background: #ffffff;
    background: -moz-linear-gradient(top, #ffffff 0%, #f3f3f3 100%);
    background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#ffffff), color-stop(100%,#f3f3f3));
    background: -webkit-linear-gradient(top, #ffffff 0%,#f3f3f3 100%);
    background: -o-linear-gradient(top, #ffffff 0%,#f3f3f3 100%);
    background: -ms-linear-gradient(top, #ffffff 0%,#f3f3f3 100%);
    background: linear-gradient(to bottom, #ffffff 0%,#f3f3f3 100%);
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#f3f3f3',GradientType=0 );
}
.btn_w {
    text-align: center;
    margin-top: 8px;
}
.btn_w a {
    width: 30%;
}
.btn_submit {
	padding:10px;
}
a.btn_cancel {
padding:10px;
}

select {
    -webkit-appearance: none;
    -moz-appearance: none;
    appearance: none;
    font-size: 12px;

    border: #C8CACC 1px solid;
    padding: 6px 25px 6px 10px;
}
select, input, img, button {
    vertical-align: middle;
}

#check_msg, #hp_send_msg, #hp_certify_msg {
	color:#ff8800;
}
#box {
	margin:10px 0 10px 0;
	font-size:1.2em;
	text-align:center;
	padding:10px;
	 border-radius: 3px;
	 background: #fff;
}
.box-content {
	padding:6px;
	background: #fff;
    border-radius: 3px;
}

.box-content table tr {
	height: 50px;
}
</style>

<!-- 쿠폰 내역 시작 { -->
<div id="scp_list" class="new_win">
    <h1 id="win_title"><i class="fa fa-newspaper-o" aria-hidden="true"></i> <?php echo $g5['title'] ?></h1>

    <div class="list_01">
        <form name="fpasswordlost" action="password_result.php" onsubmit="return form_submit(this);"  method="post" autocomplete="off">
		<input type="hidden" id="mb_hp_check" name="mb_hp_check" value="N">
        <div id="box">
			
			<p>
				<label><input type="radio" name="mb_kind" id="mb_sms1" value="id" onClick="fnChange(1);" checked /> 아이디 찿기</label>
				<label><input type="radio" name="mb_kind" id="mb_sms2" value="pw" onClick="fnChange(2);"/> 비밀번호 찾기</label>
			</p>
		</div>		
			<div class="box-content">
				
					<h3>회원정보에 등록한 휴대폰번호를 입력하세요.</h3>
					<br/>
					<table>
					<tr>
						<th scope="row" width="80"><span id="title_name">성명</span></th>
						<td>
							<p><input type="text" name="mb_id" id="mb_id"  class="inputTypeText" style="width:100px;" /></p>
						</td>
					</tr>
					<tr>
						<th scope="row">휴대폰</th>
						<td>
							<select name="mb_hp1" id="mb_hp1" class="inputTypeText" />
								<option value="">선택</option>
								<option value="010" >010</option>
								<option value="011" >011</option>
								<option value="016" >016</option>
								<option value="017" >017</option>
								<option value="018" >018</option>
								<option value="019" >019</option>
							</select>
							<input type="text" name="mb_hp2" id="mb_hp2" maxlength="4"  class="inputTypeText"  />
							<input type="text" name="mb_hp3" id="mb_hp3" maxlength="4" class="inputTypeText" />
							<input type=button value='인증번호 전송' class="btn-base btn_wt_s" onclick="hp_certify(this.form);"> 
							<br />
							<span id="hp_send_msg"></span>
						</td>
					</tr>
					<tr>
						<th scope="row">인증번호</th>
						<td>
							<input type="text" name="mb_hp_certify" id="mb_hp_certify" value=""  class="inputTypeText" style="width:100px;" />
							<input type=button value='확인' class="btn-base btn_wt_s" onclick="hp_certify_check(this.form);"> 
							<span id="hp_certify_msg"></span>							
						</td>
					</tr>
					</table>
				
				</div>
				<br/><br/>

			<div style="overflow: hidden;">
				<div style="float:left;">
					<!--<input type="button" onClick="javascript:history.back(-1);" value="뒤로" class="btn_submit">--> 
					<button onclick="history.back(-1);return false;" class="btn_submit">뒤로</button>
				</div>
				<div style="float:right;">
					<input type="submit" value="조회" class="btn_submit"> 
				</div>			
			</div>

			</form>
    </div>
</div>


<!-- 회원정보 찾기 시작 { -->
<!-- <div id="find_info" class="new_win">
    <h1 id="win_title">회원정보 찾기</h1>
    <div class="new_win_con">
        
</div> -->

<script>

var Msg_1 = "<?=$var_msg_1?>";
var Msg_2 = "<?=$var_msg_2?>";
var Msg_3 = "<?=$var_msg_3?>";
var Msg_4 = "<?=$var_msg_4?>";

function fnChange(kind) {
	if(kind==1) {
		$("#title_name").text('성명');
	} else {
		$("#title_name").text('아이디');
	}
}
// submit 최종 폼체크
function form_submit(f)
{
	var mb_kind = $('input[name="mb_kind"]:checked').val();
	if(mb_kind == 'id') {
		if(chkSpace(f.mb_id.value)){
			alert("\n성명을 입력하세요.");
			f.mb_id.focus();
			return false;
		}
	} else {
		if(chkSpace(f.mb_id.value)){
			alert("\n아이디를 입력하세요.");
			f.mb_id.focus();
			return false;
		}
	}

	if(chkSpace(f.mb_hp_certify.value)){
		alert("\n휴대폰 인증을 하셔야 됩니다.");
		//f.mb_hp_certify.focus();
		return false;
	}

	if (f.mb_hp_check.value=="N") {
		alert("\휴대폰 인증을 진행하세요.");
		return false;
	}

  

    return true;
}

$(function() {
    var sw = screen.width;
    var sh = screen.height;
    var cw = document.body.clientWidth;
    var ch = document.body.clientHeight;
    var top  = sh / 2 - ch / 2 - 100;
    var left = sw / 2 - cw / 2;
    moveTo(left, top);
});

function hp_certify(f) { 

	var GetYn = OrderMemSearch();

	if( GetYn == "N" ){
		alert(Msg_4);
		return;
	}else if( GetYn == "X" ){
		alert(Msg_3);
		return;
	}
	
	var mb_kind = $('input[name="mb_kind"]:checked').val();
	if(mb_kind == 'id') {
		if(chkSpace(f.mb_id.value)){
			alert("\n성명을 입력하세요.");
			f.mb_id.focus();
			return false;
		}
	} else {
		if(chkSpace(f.mb_id.value)){
			alert("\n아이디를 입력하세요.");
			f.mb_id.focus();
			return false;
		}
	}

	var mb_id = f.mb_id.value;

	if(f.mb_hp1.selectedIndex==0){
		alert("\n휴대폰을 선택해 주세요.");
		f.mb_hp1.focus();
		return false;
	}
	if(chkSpace(f.mb_hp2.value)){
		alert("\n휴대폰을 입력해 주세요.");
		f.mb_hp2.focus();
		return false;
	}
	if(chkSpace(f.mb_hp3.value)){
		alert("\n휴대폰을 입력해 주세요.");
		f.mb_hp3.focus();
		return false;
	}


	var pattern = /^01[0-9][-]{0,1}[0-9]{3,4}[-]{0,1}[0-9]{4}$/; 
	var mb_hp = f.mb_hp1.value + f.mb_hp2.value + f.mb_hp3.value;
		
	if(!pattern.test(mb_hp)){ 
		alert("핸드폰 번호가 입력되지 않았거나 번호가 틀립니다.\n\n핸드폰 번호를 *** 개인정보보호를 위한 휴대폰번호 노출방지 *** 또는 *** 개인정보보호를 위한 휴대폰번호 노출방지 *** 과 같이 입력해 주십시오."); 
		f.mb_hp2.select(); 
		f.mb_hp2.focus(); 
		return; 
	} else {

		$.post("/bbs/ajax.mb_hp_check.php",
			{mode:'search-send', mb_kind:mb_kind, mb_id:mb_id, mb_hp : mb_hp, token:'<?=$token?>'},
		function(data) {
			  var json =  $.parseJSON(data);
			  if(json.type == 'ok') {
				  $('#hp_send_msg').html(json.msg);
			  } else {
				  $('#hp_send_msg').html(json.msg);
			  }
		});
	}
}
function hp_certify_check(f) { 

	var GetYn = OrderMemSearch();

	if( GetYn == "N" ){
		alert(Msg_4);
		return;
	}else if( GetYn == "X" ){
		alert(Msg_3);
		return;
	}

	if(chkSpace(f.mb_hp_certify.value)){
		alert("\n인증번호를 입력해 주세요.");
		f.mb_hp_certify.focus();
		return false;
	}
	var mb_hp_certify = f.mb_hp_certify.value ;

	$.post("/bbs/ajax.mb_hp_check.php",
			{mode:'check', mb_hp_certify : mb_hp_certify},
		function(data) {
			  var json =  $.parseJSON(data);

			  if(json.type == 'ok') {
				  $('#mb_hp_check').val('Y');
				  $('#hp_certify_msg').html(json.msg);
			  } else {
				  $('#mb_hp_check').val('N');
				  $('#hp_certify_msg').html(json.msg);
			  }
		});

} 
// 입력값이 NULL 인지 체크
function chkSpace(strValue){
	var flag=true;
	if (strValue!=""){
		for (var i=0; i < strValue.length; i++){
			if (strValue.charAt(i) != " "){
				flag=false;
				break;
			}
		}
	}
	return flag;
}

function OrderMemSearch(){

	var frm = document.fpasswordlost;
	var SendYn_1 = "Y";
	var SendYn_2 = "Y";
	var SendYn_3 = "Y";

	var mb_hp1		= frm.mb_hp1.value;
	var mb_hp2		= frm.mb_hp2.value;
	var mb_hp3		= frm.mb_hp3.value;

	if( mb_hp1 == "" || mb_hp2 == "" || mb_hp3 == "" ){
		SendYn_1 = "N";
		SendYn_2 = "N";
		SendYn_3 = "N";
	}

	if( SendYn_1 == "Y" && SendYn_2 == "Y" && SendYn_3 == "Y" ){

		$.ajax({

			  url: '/bbs/order_mem_check.php',
			  type: 'post',
			  async: false,
			  data: {
					"mb_hp1"	:	mb_hp1,
					"mb_hp2"	:	mb_hp2,
					"mb_hp3"	:	mb_hp3
			  },
			  dataType: 'text',
			  success: function(data) {

				ReturnValue= data.trim();

			  }

		});

		return ReturnValue;

	}else{

		return "N";		

	}

}

</script>
<!-- } 회원정보 찾기 끝 -->
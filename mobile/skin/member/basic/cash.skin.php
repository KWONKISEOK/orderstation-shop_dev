<?php
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가

// add_stylesheet('css 구문', 출력순서); 숫자가 작을 수록 먼저 출력됨
add_stylesheet('<link rel="stylesheet" href="'.$member_skin_url.'/style.css">', 0);
?>

<div id="point" class="new_win">
    <h1 id="win_title"><i class="fa fa-database" aria-hidden="true"></i> <?php echo $g5['title'] ?></h1>
    <div class="list_01">
        <ul id="cash_ul">
            <?php
            $sum_cash1 = $sum_cash2 = $sum_cash3 = 0;

            $sql = " select * , ( ca_expire_date < LEFT( NOW() , 10 )  ) as expired_yn
                        {$sql_common}
                        {$sql_order}
                        limit {$from_record}, {$rows} ";
            $result = sql_query($sql);
            for ($i=0; $row=sql_fetch_array($result); $i++) {
                $cash1 = $cash2 = 0;
                if ($row['ca_cash'] > 0) {
                    $cash1 = '+' .number_format($row['ca_cash']);
                    $sum_cash1 += $row['ca_cash'];
                } else {
                    $cash2 = number_format($row['ca_cash']);
                    $sum_cash2 += $row['ca_cash'];
                }
                $ca_content = $row['ca_content'];

                $expr = '';
                //if($row['ca_expired'] == 1)
                if($row['expired_yn'] == 1 || $row['ca_expired'] == 1){
                    $expr = ' txt_expired';
                }
                ?>
                <li>
                    <div class="cash_wrap01">
                        <span class="cash_log"><?php echo $ca_content; ?></span>
                        <span class="cash_date"><i class="fa fa-clock-o" aria-hidden="true"></i> <?php echo $row['ca_datetime']; ?></span>
                    </div>
                    <div class="cash_wrap02">
                        <?php if($cash1){?>
                        <span class="cash_in"><?php echo $cash1?></span>
                        <?php }else {?>
                        <span class="cash_out"><?php echo $cash2?></span>
                        <?php }?>
                        <span class=" cash_date cash_expdate<?php echo $expr; ?>">
                    <?if ($row['expired_yn'] == 1 || $row['po_expired'] == 1) { ?>
                        <? if( $row['ca_cash'] < 0 ){ ?>
                            사용
                        <?}else{?>
                            만료 <?=substr(str_replace('-', '', $row['ca_expire_date']), 2); ?>
                        <? } ?>
                    <?}else{
                        echo $row['po_expire_date'] == '9999-12-31' ? '&nbsp;' : $row['ca_expire_date'];
                    }
                    ?>
                    </span>
                    </div>
                </li>
                <?php
            }

            if ($i == 0)
                echo '<li class="empty_list">자료가 없습니다.</li>';
            else {
                if ($sum_point1 > 0)
                    $sum_point1 = "+" . number_format($sum_point1);
                $sum_point2 = number_format($sum_point2);
            }
            ?>
        </ul>

        <div id="cash_sum" style="display:none;">
            <div class="sum_row">
                <span class="sum_tit">지급+사용</span>
                <b class="sum_val"><?php echo $common_point["po_cash_1"] ?></b>
            </div>
            <div class="sum_row">
                <span class="sum_tit">만료</span>
                <b class="sum_val"><?php echo $common_point["po_cash_2"] ?></b>
            </div>
            <div class="sum_row">
                <span class="sum_tit">보유</span>
                <b class="sum_val"><?php echo number_format($member['mb_point']); ?></b>
            </div>
        </div>

        <?php echo get_paging(G5_IS_MOBILE ? $config['cf_mobile_pages'] : $config['cf_write_pages'], $page, $total_page, $_SERVER['SCRIPT_NAME'].'?'.$qstr.'&amp;page='); ?>

        <div class="win_btn"><button type="button" onclick="javascript:window.close();" class="btn_close">창닫기</button></div>
    </div>
</div>
<?php
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가

// add_stylesheet('css 구문', 출력순서); 숫자가 작을 수록 먼저 출력됨
add_stylesheet('<link rel="stylesheet" href="'.$member_skin_url.'/style.css">', 0);

// 아이디 자동저장 
$ck_id_save = get_cookie("ck_id_save");

if ($ck_id_save) {
    $ch_id_save_chk = "checked";
}

// 탭 타이틀
if (preg_match("/orderform.php/", $url)) {
    $nouser_title = "비회원 주문하기";
} else if (preg_match("/orderinquiry.php$/", $url)) {
    $nouser_title = "비회원 주문조회";
} else {
    $nouser_title = "비회원 주문조회";
}

?>
<!-- jinhee 추가 -->
<div class="sub">
    <h2 class="title-section"><?php echo $g5['title'] ?></h2>
    <div class="form-login">
        <!-- [D]활성화 상태일 경우 : .active 추가 -->
        <div id="tab-login" class="tab-button-set">
            <button type="button" class="tab-button active">
                <span>회원 로그인</span>
            </button>
            <?php if($_GET['period'] != 'Y'){?>
            <button type="button" class="tab-button"><span><?php echo $nouser_title; ?></span></button><!-- [D]비회원 주문하기일 경우 워딩 : 비회원 주문하기 -->
            <?php } ?>
        </div>
        <div id="tab-login-panel" class="tab-panel-set">
            <div class="tab-panel">
                <form name="flogin" action="<?php echo $login_action_url ?>" onsubmit="return flogin_submit(this);" method="post">
                    <input type="hidden" name="url" value="<?php echo $login_url ?>">
                    <fieldset id="" class="form-group-login">
                        <legend>회원로그인</legend>
                        <label for="login_id" class="sound_only">회원아이디<strong class="sound_only"> 필수</strong></label>
                        <input type="text" name="mb_id" id="login_id" required class="input-default required" size="20" maxLength="20" placeholder="아이디" value='<?=$ck_id_save?>'>
                        <label for="login_pw" class="sound_only">비밀번호<strong class="sound_only"> 필수</strong></label>
                        <input type="password" name="mb_password" id="login_pw" required class="input-default required" size="20" maxLength="20" placeholder="비밀번호">
                        <input type="submit" value="로그인" class="btn-default btn-large btn-orange">
                        <div class="form-set">
                            <div class="form-checkbox">
                                <input type='checkbox' name="auto_login" id="login_auto_login"/>
                                <label for="login_auto_login">로그인 상태 유지</label>
                            </div>
                            <div class="form-checkbox">
                                <input type="checkbox" id='id_save' name='id_save' <?php echo $ch_id_save_chk; ?> />
                                <label for="id_save">아이디 저장</label>
                            </div>
                        </div>
                        <?php
                        // 소셜로그인 사용시 소셜로그인 버튼
                        // @include(get_social_skin_path().'/social_login.skin.php');
                        ?>
                        <ul class="foot-section">
                            <h2 class="sound_only">회원로그인 안내</h2>
                            <li>
                                <a href="<?php echo G5_BBS_URL ?>/password_lost.php">아이디 찾기</a>
                            </li>
                            <li>
                                <a href="<?php echo G5_BBS_URL ?>/password_lost.php">비밀번호 찾기</a>
                            </li>
                            <li>
                                <a href="<?php echo G5_BBS_URL ?>/register.php">회원가입</a>
                            </li>
                        </ul>
                    </fieldset>
                </form>
            </div>
            <div class="tab-panel">
                <?php if (preg_match("/orderform.php/", $url)) { ?>
                    <!-- 비회원 주문하기 일 경우 -->
                    <fieldset id="login_fs" class="form-group-login">
                        <legend>비회원 주문하기</legend>
                        <p class="guide-join">오더 스테이션 회원이 되시면 <b>약사님과 1:1 건강 상담 및</b><br/>
                            <b>다양한 상품과 혜택</b>을 만나보실 수 있습니다.
                        </p>
                        <input type="button" onclick="location.href='<?php echo $url; ?>'" value="비회원 주문하기" class="btn-default btn-large btn-orange">
                        <p class="table-info">주문 번호 및 비밀번호를 잊으셨다면 고객센터(1544-5462)로 문의 바랍니다.</p>
                        <?php
                        // 소셜로그인 사용시 소셜로그인 버튼
                        // @include(get_social_skin_path().'/social_login.skin.php');
                        ?>
                        <a href="<?php echo G5_BBS_URL ?>/register.php" class="btn-default btn-join">아직 계정이 없으신가요? 오더 스테이션 회원가입</a>
                    </fieldset>
                <?php } else if (preg_match("/orderinquiry.php$/", $url)) { ?>
                    <fieldset id="mb_login_od" class="form-group-login">
                        <form name="forderinquiry" method="post" action="<?php echo urldecode($url); ?>" autocomplete="off">
                            <legend>비회원 주문조회</legend>
                            <label for="od_id" class="sound_only">주문번호</label>
                            <input type="text" name="od_id" id="od_id" class="input-default" size="20" maxLength="20" placeholder="주문 번호" required>
                            <label for="od_pwd" class="sound_only">주문 비밀번호</label>
                            <input type="password" name="od_pwd" id="od_pwd" class="input-default" size="20" maxLength="20" placeholder="주문 비밀번호" required>
                            <input type="submit" value="주문조회 하기" class="btn-default btn-large btn-orange">
                            <p class="table-info">주문 번호 및 비밀번호를 잊으셨다면 고객센터(1544-5462)로 문의 바랍니다.</p>
                            <?php
                            // 소셜로그인 사용시 소셜로그인 버튼
                            // @include(get_social_skin_path().'/social_login.skin.php');
                            ?>
                            <a href="<?php echo G5_BBS_URL ?>/register.php" class="btn-default btn-join">아직 계정이 없으신가요? 오더 스테이션 회원가입</a>
                        </form>
                    </fieldset>
                <?php } else { ?>
                    <!-- 로그인 -> 비회원 주문조회 -->
                    <fieldset id="mb_login_od" class="form-group-login">
                        <form name="forderinquiry" method="post" action="/shop/orderinquiry.php" autocomplete="off">
                            <legend>비회원 주문조회</legend>
                            <label for="od_id" class="sound_only">주문번호</label>
                            <input type="text" name="od_id" id="od_id" class="input-default" size="20" maxLength="20" placeholder="주문 번호" required>
                            <label for="od_pwd" class="sound_only">주문 비밀번호</label>
                            <input type="password" name="od_pwd" id="od_pwd" class="input-default" size="20" maxLength="20" placeholder="주문 비밀번호" required>
                            <input type="submit" value="주문조회 하기" class="btn-default btn-large btn-orange">
                            <p class="table-info">주문 번호 및 비밀번호를 잊으셨다면 고객센터(1544-5462)로 문의 바랍니다.</p>
                            <?php
                            // 소셜로그인 사용시 소셜로그인 버튼
                            // include(get_social_skin_path().'/social_login.skin.php');
                            ?>
                            <a href="<?php echo G5_BBS_URL ?>/register.php" class="btn-default btn-join">아직 계정이 없으신가요? 오더 스테이션 회원가입</a>
                        </form>
                    </fieldset>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<script>

    $(function () {
        tab('#tab-login', 0); //로그인 탭

        $("#login_auto_login").click(function () {
            if (this.checked) {
                this.checked = confirm("자동로그인을 사용하시면 다음부터 회원아이디와 비밀번호를 입력하실 필요가 없습니다.\n\n공공장소에서는 개인정보가 유출될 수 있으니 사용을 자제하여 주십시오.\n\n자동로그인을 사용하시겠습니까?");
            }
        });

    });

    function tab(e, num) {
        // console.log("e",e);
        var num = num || 0;
        var menu = $(e).children();
        var con = $(e + '-panel').children();
        var select = $(menu).eq(num);
        var i = num;

        select.addClass('active');
        con.eq(num).show();
        menu.click(function () {
            if (select !== null) {
                select.removeClass("active");
                con.eq(i).hide();
            }
            select = $(this);
            i = $(this).index();
            select.addClass('active');
            con.eq(i).show();
        });
    }

    function flogin_submit(f) {
        if (/Android/i.test(navigator.userAgent)) {
            // Android
            window.Login.justDoIt(f.mb_id.value);
        } else if (/iPhone|iPad|iPod/i.test(navigator.userAgent)) {
            // iOS
            webkit.messageHandlers.callbackHandler.postMessage(f.mb_id.value);
        }

        return true;
    }
</script>


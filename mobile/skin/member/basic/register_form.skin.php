<?php 
if (!defined('_GNUBOARD_')) exit;   // 개별 페이지 접근 불가

// add_stylesheet('css 구문', 출력순서); 숫자가 작을 수록 먼저 출력됨
add_stylesheet('<link rel="stylesheet" href="'.$member_skin_url.'/style.css">', 0);

$mb_hp		= explode('-', $member['mb_hp']);
if (count($mb_hp) >= 2) {
	$mb_hp1 = $mb_hp[0];
	$mb_hp2 = $mb_hp[1];
	$mb_hp3 = $mb_hp[2];
}
$mb_birth		= explode('-', $member['mb_birth']);
if (count($mb_birth) >= 2) {
	$mb_birth1 = $mb_birth[0];
	$mb_birth2 = $mb_birth[1];
	$mb_birth3 = $mb_birth[2];
}
$mb_email		= explode('@', $member['mb_email']);
if (count($mb_email) >= 1) {
	$mb_email1 = $mb_email[0];
	$mb_email2 = $mb_email[1];
}



?>

<div>
    <script src="<?php echo G5_JS_URL ?>/jquery.register_form.js"></script>
    <?php if($config['cf_cert_use'] && ($config['cf_cert_ipin'] || $config['cf_cert_hp'])) { ?>
    <script src="<?php echo G5_JS_URL ?>/certify.js?v=<?php echo G5_JS_VER; ?>"></script>
    <?php } ?>

    <form name="fregisterform" id="fregisterform" action="<?php echo $register_action_url ?>" onsubmit="return fregisterform_submit(this, 0);" method="post" enctype="multipart/form-data" autocomplete="off">
    <input type="hidden" name="w" value="<?php echo $w ?>">
    <input type="hidden" name="url" value="<?php echo $urlencode ?>">
    <input type="hidden" name="agree" value="<?php echo $agree ?>">
    <input type="hidden" name="agree2" value="<?php echo $agree2 ?>">
    <input type="hidden" name="cert_type" value="<?php echo $member['mb_certify']; ?>">
    <input type="hidden" name="cert_no" value="">
	<input type="hidden" name="social_regist" value="N" /> 



<style>
/* 공통 */
.progress { overflow:hidden; height:37px; border-bottom:1px solid #ccc; text-align:center; }
.progress li { position:relative; float:left; padding:0 0 0 17px; width:35%; height:100%; line-height:37px; background-color:#ebebeb; box-sizing:border-box; list-style-type:none; }
.progress li:first-child { padding:0; width:30%; }
.progress li:first-child:before { display:none; }
.progress li:before { position:absolute; top:0; left:0; display:inline-block; content:""; width:17px; height:37px; background:url("//img.echosting.cafe24.com/skin/mobile/common/bg_process.png") no-repeat 0 0; background-size:67px auto; }
.progress li.done { background-color:#fafafa; }
.progress li.done + li:before { background-position:-25px 0; }
.progress li.done + li.done:before { background-position:-50px 0; }

.xans-member-join { margin:14px 7px 0; padding:0 0 29px; color:#63666e; font-size: 11px; }
.xans-member-join .titleArea { width:100%; margin:0; border:none; }
.xans-member-join .titleArea.accountArea { margin:-8px 0 0; }
.xans-member-join .titleArea h3 { height:30px; padding:13px 0 0 14px; border:1px solid #d5d5d5; font-size:13px; font-weight:bold; color:#4a5164; font-family:"맑은 고딕",Malgun Gothic,"돋움",Dotum; background:#f1f1f1; }
.xans-member-join .titleArea h3 span { float:right; padding:0 14px 0 0; color:#1b1b1b; font:11px normal; }
.xans-member-join .titleArea h3 span img { margin:0 3px 0 0; padding:4px 0 0; vertical-align:top; }
.xans-member-join p.required { margin:-25px 0 6px; color:#6292db; font-size:11px; text-align:right; }
.xans-member-join p.required img { vertical-align:middle; }
.xans-member-join div.grid { margin:7px 0 0; }
.xans-member-join p.grid { margin:0 0 7px 0; }
.xans-member-join ul.grid li { margin:5px 0 0; }
.xans-member-join p.txtDesc,
.xans-member-join ul.txtDesc li { position:relative; padding:0 0 0 9px; margin:3px 0 0; line-height:1.4; font-size:12px; }
.xans-member-join p.txtDesc:before,
.xans-member-join ul.txtDesc li:before { position:absolute; top:7px; left:0; display:block; content:""; width:4px; height:1px; background:currentColor; }
.xans-member-join .btnArea { margin: 14px auto 0; font-size: 0; text-align: center; }

.xans-member-join .boardWrite table tr:first-child th,
.xans-member-join .boardWrite table tr:first-child td { border-top:none; }
.xans-member-join .displaynone + .boardWrite table tr:first-child th,
.xans-member-join .displaynone + .boardWrite table tr:first-child td { border-top:1px solid #ccc; }

/* 정보입력 */
.xans-member-join .boardWrite { color:#1b1b1b; margin:0 0 7px 0; }
.xans-member-join .boardWrite table { font-size:13px; word-wrap:break-word; border:1px solid #ccc; }
.xans-member-join .boardWrite table tr:first-child th,
.xans-member-join .boardWrite table tr:first-child td { border-top:none; }
.xans-member-join .boardWrite table th { width:90px; padding:7px 0 7px 10px; border-top:1px solid #ececec; color:#757575; font-size:13px; font-weight:normal; text-align:left; vertical-align:middle; background:#fff; }
.xans-member-join .boardWrite table th > img { vertical-align:top; padding:4px 0 0; }
.xans-member-join .boardWrite table td { padding:7px 10px; border-top:1px solid #ececec; vertical-align:middle; background:#fff; }
.xans-member-join .boardWrite table .idCheck { font-size:11px; }
.xans-member-join .boardWrite table .idCheck th { vertical-align:top; padding-top:13px; }
.xans-member-join .boardWrite table .idCheck .error { color:#f00; }
.xans-member-join .boardWrite table td.thead { border-left:1px solid #ccc; }
.xans-member-join .boardWrite table td.thead label:last-child { margin-right:0; }
.xans-member-join .boardWrite table td #foreignerAuth label:last-child { margin-right:0; }
.xans-member-join .boardWrite table td.thead label input { vertical-align:middle; }
.xans-member-join .boardWrite table td.thead p { height:30px; padding:6px 0 0; color:#757575; font-size:13px; }
.xans-member-join .boardWrite table td label { margin:0 14px 0 0; }
.xans-member-join .boardWrite .interest label { display:inline-block; min-width:90px; margin:0 7px 14px 0; }
.xans-member-join .boardWrite .interest { font-size:11px; line-height:1.2em; }
.xans-member-join .boardWrite .interest input { margin:0 3px 0 0; }
.xans-member-join .boardWrite .interest span { display:inline-block; }
.xans-member-join .boardWrite select { margin-right:4px; }

.xans-member-join .boardWrite #authWrap #ipinWrap img,
.xans-member-join .boardWrite #authWrap #mobileWrap img,
.xans-member-join .boardWrite #authWrap img { width:106px; }

/* INPUT SIZE */
.xans-member-join input[type=text],
.xans-member-join input[type=password],
.xans-member-join input[type=number],
.xans-member-join input[type=tel],
.xans-member-join select { width:100%; }
.xans-member-join #foreigner_ssn { width:auto; }
.xans-member-join #user_passwd_confirm,
.xans-member-join #hint,
.xans-member-join #hint_answer { margin-bottom:7px; }
.xans-member-join #mb_id, #mb_hp_certify { width:113px; }
.xans-member-join #postcode1 { width:55px; text-align:center; }
.xans-member-join #addr1 { margin:5px 0 0; }
.xans-member-join #addr2 { margin:5px 0 7px; }

.xans-member-join #mb_hp1 { width:33%; }
.xans-member-join #mb_hp2,
.xans-member-join #mb_hp3 { width:25%; margin:0 4px; }
.xans-member-join #bssn1,
.xans-member-join #bssn2 { width:70px; }
.xans-member-join #mb_email1 { max-width:40%; margin:0 4px 7px 0; }
.xans-member-join #mb_email2 { max-width:45%; margin:0 4px 7px 4px; }
.xans-member-join #mb_email3 { height:29px; margin:0 0 7px; }

.xans-member-join #mb_birth1,
.xans-member-join #marry_year,
.xans-member-join #partner_year { width:30%; }
.xans-member-join #mb_birth2,
.xans-member-join #marry_month,
.xans-member-join #partner_month { width:30%;  }
.xans-member-join #mb_birth3,
.xans-member-join #marry_day,
.xans-member-join #partner_day { width:30%;  }


#popup div.wrap { background:#f1f1f1; }
.xans-member-join #agreeTerms { display:block; min-height:500px; height:auto; padding:7px; border:1px solid #ccc; border-radius:2px; color:#353535; font-size:12px; background:#fff; }

#popup div.wrap { background:#f1f1f1; }
.xans-member-join #privacyTerms { display:block; min-height:500px; height:auto; padding:7px; border:1px solid #ccc; border-radius:2px; color:#353535; font-size:12px; background:#fff; }

.xans-member-joincomplete { margin:14px 7px 0; padding:0 0 29px; font-size:13px; }
.xans-member-joincomplete > p { margin:10px 12px 0; }
.xans-member-joincomplete .welcome { padding:14px 14px 14px; border:1px solid #d5d5d5; font-size:13px; text-align:center; background:#fff; }
.xans-member-joincomplete .welcome p:last-child { margin:10px 0 0; }
.xans-member-joincomplete .welcome .complete { color:#ed5062; font-size:15px; font-weight:bold; }
.xans-member-joincomplete .welcome .xans-member-benefitjoin { margin:15px 0 0; text-align:left; }
.xans-member-joincomplete .welcome .xans-member-benefitjoin p { margin:0; font-size:11px; }
.xans-member-joincomplete ul { margin:7px 0px 0; padding:14px 14px 14px 14px; border:1px solid #d5d5d5; background:#fff; }
.xans-member-joincomplete li { margin:0 0 14px 0; }
.xans-member-joincomplete li:last-child { margin-bottom:0; }
.xans-member-joincomplete li span { display:inline-block; min-width:45px; color:#757575; }
.xans-member-joincomplete li strong { display:inline-block; margin:0 0 0 20px; color:#353535; font-weight:400; }
.xans-member-joincomplete li:before { overflow:hidden; content:""; position:relative; left:-10px; display:inline-block; width:3px; height:7px; margin-right:-3px; }
.xans-member-joincomplete .btnArea.type2 { margin:14px auto 0; }
.xans-member-joincomplete .memberEmail { color:#333; font-weight:bold; }

/* 추가 */
.progress { overflow:hidden; height:37px; border-bottom:1px solid #ccc; text-align:center; }
.progress li { position:relative; float:left; padding:0 0 0 17px; width:35%; height:100%; line-height:37px; background-color:#ebebeb; box-sizing:border-box; list-style-type:none; }
.progress li:first-child { padding:0; width:30%; }
.progress li:first-child:before { display:none; }
.progress li:before { position:absolute; top:0; left:0; display:inline-block; content:""; width:17px; height:37px; background:url("//img.echosting.cafe24.com/skin/mobile/common/bg_process.png") no-repeat 0 0; background-size:67px auto; }
.progress li.done { background-color:#fafafa; }
.progress li.done + li:before { background-position:-25px 0; }
.progress li.done + li.done:before { background-position:-50px 0; }

.xans-member-join { margin:14px 7px 0px; padding:0 0 29px; color:#63666e; font-size: 11px; }
.xans-member-join .agreeArea { margin:0 0 7px; border:1px solid #ccc; background:#fff; }
.xans-member-join .title { position:relative; line-height:36px; color:#333; border-bottom:1px solid #ccc; }
.xans-member-join .title h3 { padding:0 0 0 14px; font-size:13px; font-weight:normal; }
.xans-member-join .title h3 label { display:none; }
.xans-member-join .title h3 label.check { display:inline; }
.xans-member-join .title a.btnMore { position:absolute; top:0px; right:14px; font-size:11px; text-decoration:underline; }
.xans-member-join .contents { overflow:hidden; height:90px; margin:14px; color:#757575; font-size:11px; }
.xans-member-join .btnArea.type2 { margin:14px auto 0; }

/* 추가 */
.progress { overflow:hidden; height:37px; border-bottom:1px solid #ccc; text-align:center; }
.progress li { position:relative; float:left; padding:0 0 0 17px; width:35%; height:100%; line-height:37px; background-color:#ebebeb; box-sizing:border-box; list-style-type:none; }
.progress li:first-child { padding:0; width:30%; }
.progress li:first-child:before { display:none; }
.progress li:before { position:absolute; top:0; left:0; display:inline-block; content:""; width:17px; height:37px; background:url("//img.echosting.cafe24.com/skin/mobile/common/bg_process.png") no-repeat 0 0; background-size:67px auto; }
.progress li.done { background-color:#fafafa; }
.progress li.done + li:before { background-position:-25px 0; }
.progress li.done + li.done:before { background-position:-50px 0; }
 
.agreeAll { padding:0 0 20px; }
.agreeAll p { font-size:13px; }
.agreeAll h3 { margin:0 0 14px; color:#1b1b1b; }
.agreeAll span.ec-base-chk { width:32px; height:32px; }
.agreeAll span.ec-base-chk .checkbox { background-position:0 0; }
.agreeAll span.ec-base-chk input:checked + .checkbox { background-position:-34px 0; }
.agreeAll span.ec-base-chk + label { width:calc(100% - 46px); width:-webkit-calc(100% - 46px); color:#538aeb; }
  
span.ec-base-chk { position:relative; display:inline-block; margin:0 7px 0 0; width:20px; height:20px; vertical-align:middle; cursor:pointer; }
span.ec-base-chk input { z-index:1; position:absolute; top:0; left:0; width:100%; height:100%; opacity:0; cursor:pointer; }
span.ec-base-chk .checkbox { position:absolute; top:0; left:0; width:100%; height:100%; background:url("//img.echosting.cafe24.com/skin/mobile/common/bg_join_check.png") no-repeat 0 -50px; background-size:65px auto; }
span.ec-base-chk input:checked + .checkbox { background-position:-25px -50px; }
span.ec-base-chk + label { display:inline-block; width:80%; width:calc(100% - 34px); width:-webkit-calc(100% - 34px); min-height:0; vertical-align:middle; line-height:20px; }
input[type="checkbox"].ec-base-chk { margin:0 7px 0 0; width:20px; height:20px; border:0; -webkit-appearance:none; background-image:url("//img.echosting.cafe24.com/skin/mobile/common/bg_join_check.png"); background-repeat:no-repeat; background-position:0 -50px; background-size: 65px auto; }
input[type="checkbox"].ec-base-chk:checked { background-position:-25px -50px; -webkit-appearance:none; }

.xans-member-join .additional { display:flex; display:-webkit-flex; padding:0 14px; border-top:1px solid #ccc; background:#fafafa; }
.xans-member-join .additional li { flex:1; }
.xans-member-join .additional li label { height:35px; line-height:35px; vertical-align:top; background:#fafafa; }

.xans-member-join .joinConfirm { display:none; z-index:300; position:fixed; top:0; bottom:0; left:0; min-width:320px; background:#fff; }
.xans-member-join .joinConfirm .header { position:relative; }
.xans-member-join .joinConfirm .header h2 { height:36px; padding:0 38px 0 7px; font-size:15px; color:#fff; line-height:36px; background:#777777; }
.xans-member-join .joinConfirm .header .btnClose { position:absolute; right:0; top:0; z-index:110; width:36px; height:36px; border:0; padding:0; font-size:0; text-indent:100%; background:url("http://img.echosting.cafe24.com/skin/mobile_ko_KR/layout/ico_close_white.png") center center no-repeat; background-size:14px 14px; }

.xans-member-join .joinConfirm .content { overflow-y:auto; padding:0 10px; height:100%; -webkit-overflow-scrolling:touch; }
.xans-member-join .joinConfirm .content p { margin:20px 0 0;font-size:11px; line-height:1.4; }
.xans-member-join .joinConfirm .content .boardList { margin:10px 0 50px; padding:0 0 60px; }
.xans-member-join .joinConfirm .boardList table { width:100%; table-layout:fixed; border:0; border-collapse:collapse; border-spacing:0; }
.xans-member-join .joinConfirm .boardList th,
.xans-member-join .joinConfirm .boardList td { padding:8px 10px 7px; font-size:12px; line-height:1.4; border:1px solid #d7d5d5; word-wrap:break-word; }
.xans-member-join .joinConfirm .boardList th { font-weight:normal; text-align:left; background-color:#fafafa; }
.xans-member-join .joinConfirm .btnArea.type2 { position:fixed; bottom:0; margin:0; padding:6px 14px 7px; width:100%; max-width:none; border-top:1px solid #ccc; border-radius:0 0 5px 5px; background:#f1f1f1; }

#popup div.wrap { background:#f1f1f1; }
.xans-member-join #consignmentTerms { display:block; min-height:500px; height:auto; padding:7px; border:1px solid #ccc; border-radius:2px; color:#353535; font-size:12px; background:#fff; }


#popup div.wrap { background:#f1f1f1; }
.xans-member-join #informationTerms { display:block; min-height:500px; height:auto; padding:7px; border:1px solid #ccc; border-radius:2px; color:#353535; font-size:12px; background:#fff; }

select {
    max-width: 100%;
    height: 29px;
    margin: 0;
    padding: 0 35px 0 3px;
    color: #353535;
    word-break: break-all;
    font-weight: inherit;
    border: 1px solid #ccc;
    background: #fff url(http://img.echosting.cafe24.com/skin/mobile_ko_KR/layout/bg_selectbox.gif) no-repeat right center;
    background-size: 32px 27px;
    -webkit-border-radius: 3px;
    -webkit-appearance: none;
}

</style>



<?php if($w=='') { ?>
					<input type="hidden" id="mb_id_check" name="mb_id_check" value="N">
					<input type="hidden" id="mb_hp_check" name="mb_hp_check" value="N">
					<?php } else { ?>
					<input type="hidden" id="mb_id_check" name="mb_id_check" value="Y">
					<input type="hidden" id="mb_hp_check" name="mb_hp_check" value="Y">
					<?php } ?>


<div class="xans-member-join">
	<div id="titleArea">
	<?php if($w=='') { ?>
		<h2>회원가입</h2>
	<?php } else { ?>
		 <h2>회원정보수정</h2>
	<?php } ?>

    <span class="xans-element- xans-layout xans-layout-mobileaction "><a href="#none" onclick="history.go(-1);return false;"><img src="//img.echosting.cafe24.com/skin/mobile_ko_KR/layout/btn_back.gif" width="33" alt="뒤로가기"></a>
</span>
</div>


<div class="boardWrite ">
	<table border="1" summary="">
	<caption>회원 기본정보</caption>
	<tbody>
	<tr class="idCheck">
		<th scope="row">아이디 </th>
			<td>
			<?php if($w=='') { ?>
			<input id="mb_id" name="mb_id" fw-filter="isFill&amp;isFill&amp;isMin[4]&amp;isMax[16]&amp;isIdentity" fw-label="아이디" fw-msg="" class="inputTypeText" placeholder="" value="" type="text"> 
			 <button type="button" class="btnBasic" name="id_check" id="id_check" >중복확인</button>
				<p id="check_msg"></p>
				<?php } else { ?>
					<?php echo $member['mb_id']; ?>
					<input type="hidden" name="mb_id" id="mb_id" value="<?php echo $member['mb_id']; ?>"/>
				<?php } ?>

			</td>
		</tr>
	<tr>
	<th scope="row">비밀번호 </th>
			<td><input id="mb_passwd1" name="mb_passwd1" fw-filter="isFill&amp;isMin[4]&amp;isMax[16]" fw-label="비밀번호" fw-msg="" autocomplete="off" maxlength="16" 0="disabled" value="" type="password"></td>
		</tr>
	<tr>
		<th scope="row" class="pwConfirm">비밀번호<br>확인 </th>
			<td><input id="mb_passwd2" name="mb_passwd2" fw-filter="isFill&amp;isMatch[passwd]" fw-label="비밀번호 확인" fw-msg="비밀번호가 일치하지 않습니다." autocomplete="off" maxlength="16" 0="disabled" value="" type="password"> <span id="pwConfirmMsg"></span>
		</td>
	</tr>
	<tr class="idCheck">
		<th scope="row">성명 </th>
			<td>
			<input id="mb_name" name="mb_name" value="<?php echo $member['mb_name']; ?>" type="text"> 
			
			</td>
		</tr>
	<tr>
	<tr class="emailCheck">
	<th scope="row">이메일 </th>
						<td><input id="mb_email1" name="mb_email1" fw-filter="isFill" fw-label="이메일" fw-alone="N" fw-msg="" class="mailId" value="<?php echo $mb_email1; ?>" type="text">@<input id="mb_email2" name="mb_email2" fw-filter="isFill" fw-label="이메일" fw-alone="N" fw-msg="" class="mailAddress"  value="<?php echo $mb_email2; ?>" type="text">
						<select id="mb_email3" fw-filter="isFill" fw-label="이메일" fw-alone="N" fw-msg="" onChange="fnc_semail1(this.value);">
			<option value="" selected="selected">- 이메일 선택 -</option>
			<option value="naver.com">naver.com</option>
			<option value="daum.net">daum.net</option>
			<option value="nate.com">nate.com</option>
			<option value="hotmail.com">hotmail.com</option>
			<option value="yahoo.com">yahoo.com</option>
			<option value="empas.com">empas.com</option>
			<option value="korea.com">korea.com</option>
			<option value="dreamwiz.com">dreamwiz.com</option>
			<option value="gmail.com">gmail.com</option>
			<option value="">직접입력</option>
			</select> 
			</td>
		</tr>
	
	
		
	<tr class="">
	<th scope="row">휴대전화 </th>
		<td>
		<?php if($w=='') { ?>
			<select id="mb_hp1" name="mb_hp1" fw-filter="isNumber" fw-label="휴대전화" fw-alone="N" fw-msg="">
			<option value="">선택</option>
			<option value="010">010</option>
			<option value="011">011</option>
			<option value="016">016</option>
			<option value="017">017</option>
			<option value="018">018</option>
			<option value="019">019</option>
			</select>-<input id="mb_hp2" name="mb_hp2" maxlength="4" fw-filter="isNumber" fw-label="휴대전화" fw-alone="N" fw-msg="" value="" type="tel">-<input id="mb_hp3" name="mb_hp3" maxlength="4" fw-filter="isNumber" fw-label="휴대전화" fw-alone="N" fw-msg="" value="" type="tel">

			<input type=button value='인증번호 전송' class="btnBasic"  onclick="hp_certify(this.form);"> 
					<span id="hp_send_msg"></span>
				<br/>

					<input type="text" name="mb_hp_certify" id="mb_hp_certify" value=""  class="inputTypeText" />
					<input type=button value='확인' class="btnBasic" onclick="hp_certify_check(this.form);"> 
					<span id="hp_certify_msg"></span>
			
				<?php } else { ?>

					<select name="mb_hp1" id="mb_hp1" class="inputTypeText" />
						<option value="">선택</option>
						<option value="010" <?php if($mb_hp1=='010') echo 'selected';?> >010</option>
						<option value="011" <?php if($mb_hp1=='011') echo 'selected';?> >011</option>
						<option value="016" <?php if($mb_hp1=='016') echo 'selected';?> >016</option>
						<option value="017" <?php if($mb_hp1=='017') echo 'selected';?> >017</option>
						<option value="018" <?php if($mb_hp1=='018') echo 'selected';?> >018</option>
						<option value="019" <?php if($mb_hp1=='019') echo 'selected';?> >019</option>
					</select>
					<input type="tel" name="mb_hp2" id="mb_hp2" maxlength="4" style="ime-mode:inactive" class="inputTypeText" value="<?php echo $mb_hp2; ?>"  />
					<input type="tel" name="mb_hp3" id="mb_hp3" maxlength="4" style="ime-mode:inactive"  class="inputTypeText" value="<?php echo $mb_hp3; ?>"  />

				<?php }  ?>

		</td>
	</tr>

	<tr>
		<th scope="row">성별</th>
		<td>
			
				<label><input type="radio" name="mb_sex" id="mb_sex1" value="M" <?php if($member['mb_sex'] =='M') echo 'checked';?> /> 남</label>&nbsp;
				<label><input type="radio" name="mb_sex" id="mb_sex2" value="W" <?php if($member['mb_sex'] =='W') echo 'checked';?> /> 여</label>
			
		</td>
	</tr>
	<tr>
		<th scope="row">생년월일</th>
		<td>
			
			<select name="mb_birth1" id="mb_birth1" >
				<option value="">년</option>
				<?php 
				$currentYear = date('Y');
				$startYear = ($currentYear - 100);
				foreach (range($currentYear, $startYear) as $year) { 
					$selected = "";
					if($year == $mb_birth1) { $selected = " selected"; }
					echo '<option' . $selected . '>' . $year . '</option>';
				 }
				?>								
			</select>
			<select name="mb_birth2" id="mb_birth2">
				<option value="">월</option>
				
				<?php 
				foreach (range(1, 12) as $month) { 
					if($month < 10) $month = '0'.$month;
					$selected = "";
					if($month == $mb_birth2) { $selected = " selected"; }
					echo '<option' . $selected . '>' . $month . '</option>';
				 }
				?>				
															
			</select>
			<select name="mb_birth3" id="mb_birth3" >
				<option value="">일</option>
				
				<?php 
				foreach (range(1, 31) as $day) { 
					if($day < 10) $day = '0'.$day;
					$selected = "";
					if($day == $mb_birth3) { $selected = " selected"; }
					echo '<option' . $selected . '>' . $day . '</option>';
				 }
				?>			
			</select>
			
		</td>
	</tr>

	
	</tbody>
	</table>



		<br>
					<h2>오더스테이션 모바일의 이벤트, 쿠폰, 할인특가 등의 다양한 정보를 이메일, 문자,푸쉬알람으로 받으시겠습니까?</h2>
	
					<table border="1" summary="">
					<caption>회원 기본정보</caption>
							<colgroup>
					<col style="width:150px;"/>
					<col style="width:auto;"/>
						<tr>
							<th scope="row">SMS수신</th>
							<td>
								<p>
									<label><input type="radio" name="mb_sms" id="mb_sms1" value="1" <?php if($member['mb_sms'] =='1') echo 'checked';?>/> 동의</label>
									<label><input type="radio" name="mb_sms" id="mb_sms2" value="0" <?php if($member['mb_sms'] =='0') echo 'checked';?>/> 동의 안함</label>
								</p>
							</td>
						</tr>
						<tr>
							<th scope="row">E-mail수신</th>
							<td>
								<p>
									<label><input type="radio" name="mb_mailling" id="mb_mailling1" value="1" <?php if($member['mb_mailling'] =='1') echo 'checked';?>/> 동의</label>
									<label><input type="radio" name="mb_mailling" id="mb_mailling2" value="0" <?php if($member['mb_mailling'] =='0') echo 'checked';?> /> 동의 안함</label>
								</p>
							</td>
						</tr>
						<tr>
							<th scope="row">스마트폰 푸쉬알림</th>
							<td>
								<p>
									<label><input type="radio" name="mb_push" id="mb_push1" value="1" <?php if($member['mb_push'] =='1') echo 'checked';?>/> 동의</label>
									<label><input type="radio" name="mb_push" id="mb_push2" value="0" <?php if($member['mb_push'] =='0') echo 'checked';?> /> 동의 안함</label>
								</p>
							</td>
						</tr>
					</table>
					<!-- <p class="btn_w">
						<a href="javascript:Save_Click();" class="btn btn_org">추천약사선택</a>
						 <a href="/main02.asp" class="btn-base btn_wt">취소</a> 
					</p> -->
					<br>
						<h2 class="sub_h2_tit mt15">나의 추천 약사 등록하기</h2>
						<table border="1" summary="">
					<caption>회원 기본정보</caption>
							<colgroup>
					<col style="width:auto;"/>
							<tbody><tr>
								<td class="td_bg">
									<ul class="listtype01">
										<li>고객님의 동네 약국의 약사님을 등록해 주세요.</li>
										<li>선택하신 약사님이 고객님의 추천 약사로 지정됩니다.	</li>
										<li>지정된 추천 약사가 고객님의 건강을 컨설팅 해드립니다.</li>
									</ul>
								</td>
							</tr>
							<!--
							<tr>
								<td>
									<p><select name="" id="" class="selc w100"><option value="">약국을 선택해주세요.</option></select></p>
								</td>
							</tr>
							//-->
							<tr>
								<td>
									<p>

										<!--<a href="javascript:npopupLayer('pop_pharmacy.asp?obj1=frm&obj2=str_tuserid&obj3=str_tname',380,428)" class="btn btn_wt_s">검색</a>//-->
										<div id="sod_bsk_list"> <strong >추천약사 검색해서 지정하기</strong></div>
									</p>

									<iframe src="./register_pharmacy.php" frameborder="0" width="100%" height="240" marginwidth="0" marginheight="0"></iframe>



								</td>
							</tr>
						</tbody>
						</table>
						<table class="t_medi mt10">
							<colgroup>
								<col width="30%">
								<col width="70%">
							</colgroup>
							<tbody>
								<tr>
									<th>추천 약사</th>
									<td > 
									<?php if($w=='') { ?>
		
									<input type="hidden" name="mb_recommend" id="mb_recommend" >
									<span id="mb_recommend_name"></span>
									<?php } else { 
										$rmb = get_member($member['mb_recommend']);
									?>
									<input type="hidden" name="mb_recommend" id="mb_recommend" value="<?php echo $member['mb_recommend'];?>">
									<span id="mb_recommend_name"><?php echo $rmb['pharm_name'];?> <?php echo $rmb['mb_name'];?></span>
									<?php } ?>
									
									</td>
								</tr>
							</tbody>
						</table>


	</div>
</div>




	<div class="btn_confirm">
		<a href="<?php echo G5_URL ?>" class="btnBasic ">취소</a>
		<input type="submit" value="<?php echo $w==''?'회원가입':'정보수정'; ?>" id="btn_submit" class="btnSubmit" accesskey="s">
	</div>
</div>
</form>

    
</div>

<script> 

var Msg_1 = "<?=$var_msg_1?>";
var Msg_2 = "<?=$var_msg_2?>";
var Msg_3 = "<?=$var_msg_3?>";
var Msg_4 = "<?=$var_msg_4?>";

$('#id_check').on('click', function(e){
	var mb_id = $('#mb_id').val();
	if( mb_id=='') {
		alert('사용자 ID를 입력하세요.');
		$('#mb_id').focus();
		return false;
	}

	if (mb_id.length<6 || mb_id.length>18) {
		alert("아이디는 6-18byte 입니다.");
		$('#mb_id').focus();
		return;
	}
	if(isnum(mb_id)) {
		alert("\n아이디는 영문과 혼용하셔야 합니다.");
		$('#mb_id').focus();
		return;
	}

	$.post("/bbs/ajax.mb_check.php",
			{mode:'idcheck', mb_id : mb_id},
		function(data) {
			  var json =  $.parseJSON(data);

			  if(json.count > 0) {
				  $('#mb_id_check').val('N');
				  $('#check_msg').html('이미 등록된 아이디 입니다.');
			  } else {
				  $('#mb_id_check').val('Y');
				  $('#check_msg').html('사용 가능합니다.');
			  }
		});
	return false;
});
function hp_certify(f) { 

	var GetYn = OrderMemSearch();

	if( GetYn == "B" ){
		alert(Msg_1);
		return false;
	}else if( GetYn == "O" ){
		alert(Msg_2);
		return false;	
	}else if( GetYn == "N" ){
		alert(Msg_4);
		return;
	}

	if(f.mb_hp1.selectedIndex==0){
		alert("\n휴대폰을 선택해 주세요.");
		f.mb_hp1.focus();
		return false;
	}
	if(chkSpace(f.mb_hp2.value)){
		alert("\n휴대폰을 입력해 주세요.");
		f.mb_hp2.focus();
		return false;
	}
	if(chkSpace(f.mb_hp3.value)){
		alert("\n휴대폰을 입력해 주세요.");
		f.mb_hp3.focus();
		return false;
	}


	var pattern = /^01[0-9][-]{0,1}[0-9]{3,4}[-]{0,1}[0-9]{4}$/; 
	var mb_hp = f.mb_hp1.value + f.mb_hp2.value + f.mb_hp3.value;
		
	if(!pattern.test(mb_hp)){ 
		alert("핸드폰 번호가 입력되지 않았거나 번호가 틀립니다.\n\n핸드폰 번호를 *** 개인정보보호를 위한 휴대폰번호 노출방지 *** 또는 *** 개인정보보호를 위한 휴대폰번호 노출방지 *** 과 같이 입력해 주십시오."); 
		f.mb_hp2.select(); 
		f.mb_hp2.focus(); 
		return; 
	} else {

		$.post("/bbs/ajax.mb_hp_check.php",
			{mode:'send-register', mb_hp : mb_hp, token:'<?=$token?>'},
		function(data) {

			  var json =  $.parseJSON(data);

			  if(json.type == 'ok') {
				  //$('#hp_send_msg').html('전송하였습니다.' + json.certify);
                  $('#hp_send_msg').html(json.msg);
			  } else {
				  //$('#hp_send_msg').html('실패하였습니다.');
                  $('#hp_send_msg').html(json.msg);
			  }
		});
	}

	//win_open("/bbs/hp_certify.php?hp="+mb_hp+"&token=<?=$token?>", "hiddenframe"); 
} 
function win_open(url, name, option) 
{ 
    var popup = window.open(url, name, option); 
    popup.focus(); 

}
function hp_certify_check(f) { 

	var GetYn = OrderMemSearch();

	if( GetYn == "B" ){
		alert(Msg_1);
		return false;
	}else if( GetYn == "O" ){
		alert(Msg_2);
		return false;	
	}else if( GetYn == "N" ){
		alert(Msg_4);
		return;
	}

	if(chkSpace(f.mb_hp_certify.value)){
		alert("\n인증번호를 입력해 주세요.");
		f.mb_hp_certify.focus();
		return false;
	}
	var mb_hp_certify = f.mb_hp_certify.value ;

	//win_open("/bbs/hp_certify_check.php?mb_hp_certify="+mb_hp_certify, "hiddenframe"); 
	$.post("/bbs/ajax.mb_hp_check.php",
			{mode:'check', mb_hp_certify : mb_hp_certify},
		function(data) {
			  var json =  $.parseJSON(data);

			  if(json.type == 'ok') {
				  $('#mb_hp_check').val('Y');
				  //$('#hp_certify_msg').html('인증되었습니다.');
                  $('#hp_certify_msg').html(json.msg);
			  } else {
				  $('#mb_hp_check').val('N');
				  //$('#hp_certify_msg').html('인증번호가 틀립니다. 다시입력해주세요.' + json.msg);
                  $('#hp_certify_msg').html(json.msg);
			  }
		});

} 

//숫자체크
function isnum(NUM) {
	for(var i=0;i<NUM.length;i++){
		achar = NUM.substring(i,i+1);
		if( achar < "0" || achar > "9" ){
			return false;
		}
	}
	return true;
}

// 입력값이 NULL 인지 체크
function chkSpace(strValue){
	var flag=true;
	if (strValue!=""){
		for (var i=0; i < strValue.length; i++){
			if (strValue.charAt(i) != " "){
				flag=false;
				break;
			}
		}
	}
	return flag;
}

</script> 


    <script>

	// 선택사항수정
    $(".mod_options").click(function() {
      
        var $this = $(this);
        close_btn_idx = $(".mod_options").index($(this));

        $.post(
            "./register_pharmacy.php",
            function(data) {
                $("#mod_option_frm").remove();
                $this.after("<div id=\"mod_option_frm\"></div>");
                $("#mod_option_frm").html(data);
            }
        );
    });

	 // 옵션수정 닫기
    $(document).on("click", "#mod_option_close", function() {
        $("#mod_option_frm").remove();
        $(".mod_options").eq(close_btn_idx).focus();
    });
	// 옵션수정 닫기
    function closeLayer() {
        $("#mod_option_frm").remove();
        $(".mod_options").eq(close_btn_idx).focus();
    }


    // 추천약사
    $("#mb_recommend_pop").click(function() {

		if(!fregisterform_submit(fregisterform, 1)) {
			return ;
		}

        
        var $this = $(this);
        close_btn_idx = $("#mb_recommend_pop").index($(this));

        $.post(
             "./register_pharmacy.php",
            function(data) {
                $("#mod_option_frm").remove();
                $this.after("<div id=\"mod_option_frm\"></div>");
                $("#mod_option_frm").html(data);
            }
        );
    });



	function onOnlyAlphaNumber(obj) {
		str = obj.value; 
		len = str.length; 
		ch = str.charAt(0);
		for(i = 0; i < len; i++) { 
			ch = str.charAt(i); 
			if( (ch >= '0' && ch <= '9') || (ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z') ) { 
			continue; 
			} else { 
			alert("영문과 숫자만 입력이 가능합니다.");
			obj.value="";
			obj.focus();
			return false; 
		} 
		}
		return true; 
	}

    // submit 최종 폼체크
    function fregisterform_submit(f, kind)
    {
		if(chkSpace(f.mb_id.value)){
        	alert("\n아이디를 입력해 주세요..");
            f.mb_id.focus();
            return false;
	   	}

	   	if(f.mb_id_check.value=='N'){
	       	alert("\n아이디 중복체크를 하여주세요");
	        return false;
	   	}
		<?php if($w=='') { ?>
		if(chkSpace(f.mb_passwd1.value)){
        	alert("\n비밀번호를 입력해 주세요..");
            f.mb_passwd1.focus();
            return false;
		}
		if(chkSpace(f.mb_passwd2.value)){
        	alert("\n비밀번호를 입력해 주세요..");
            f.mb_passwd2.focus();
            return false;
	    }
	    if(mb_passwd_check()==false) {
            f.mb_passwd1.focus();
        	return false;
		}
		<?php } else { ?>
			if(f.mb_passwd1.value != ''){
				if(mb_passwd_check()==false) {
					f.mb_passwd1.focus();
					return false;
				}
			}
			
		<?php }  ?>

		if(chkSpace(f.mb_name.value)){
        	alert("\n성명을 입력해 주세요..");
            f.mb_name.focus();
            return false;
	    }
		
		if (f.mb_email1.value == "" ) {
			alert("\n이메일를 입력해 주세요..");
			f.mb_email1.focus();
			return false;
		}
		if(chkSpace(f.mb_email2.value)){
			alert("\n이메일를 입력해 주세요..");
			f.mb_email2.focus();
			return false;
		}
		if(mb_email_check(f.mb_email1.value+"@"+f.mb_email2.value)==false) {
			alert("\n정확한 이메일 주소를 입력하세요.");
			f.mb_email1.focus();
			return false;
		}
		
		if(f.mb_hp1.selectedIndex==0){
        	alert("\n휴대폰을 선택해 주세요.");
            f.mb_hp1.focus();
            return false;
		}
		if(chkSpace(f.mb_hp2.value)){
        	alert("\n휴대폰을 입력해 주세요.");
            f.mb_hp2.focus();
            return false;
		}
		if(chkSpace(f.mb_hp3.value)){
        	alert("\n휴대폰을 입력해 주세요.");
            f.mb_hp3.focus();
            return false;
		}
		
		if (f.mb_hp_check.value=="N") {
			alert("\휴대폰 인증을 진행하세요.");
			return false;
		}
		
		if(f.mb_sex[0].checked==false&&f.mb_sex[1].checked==false){
	      	alert("\n성별을 선택 주세요.");
	        f.mb_sex[0].focus();
	      	return false;
		}
		if(f.mb_birth1.selectedIndex==0){
        	alert("\n생년월일을 선택해 주세요.");
            f.mb_birth1.focus();
            return false;
		}
		if(f.mb_birth2.selectedIndex==0){
        	alert("\n생년월일을 선택해 주세요.");
            f.mb_birth2.focus();
            return false;
		}
		if(f.mb_birth3.selectedIndex==0){
        	alert("\n생년월일을 선택해 주세요.");
            f.mb_birth3.focus();
            return false;
		}
		if(f.mb_sms[0].checked==false&&f.mb_sms[1].checked==false){
	      	alert("\nSMS 수신여부를 선택해 주세요.");
	        f.mb_sms[0].focus();
	      	return false;
		}
		if(f.mb_mailling[0].checked==false&&f.mb_mailling[1].checked==false){
	      	alert("\n이메일 수신여부를 선택해 주세요.");
	        f.mb_mailling[0].focus();
	      	return false;
		}
		if(f.mb_push[0].checked==false&&f.mb_push[1].checked==false){
	      	alert("\n스마트폰 푸쉬알림 수신여부를 선택해 주세요.");
	        f.mb_push[0].focus();
	      	return false;
		}
		<? if( $member['mb_type'] != "1" ){ ?>
		if (f.mb_recommend.value=="") {
			alert("\추천약사를 지정하세요.");
			return false;
		}
		<? } ?>

        return true;
    }
	// 비밀번호
	function mb_passwd_check()
	{
	    //valid check
		if ( document.fregisterform.mb_passwd1.value.length <= 0 )
		{
			alert("\n비밀번호를 입력하세요.");
			return false;
		}
		
		var isID = /^(?=.*[a-zA-Z])(?=.*\d).{6,20}$/;
		if( !isID.test(document.fregisterform.mb_passwd1.value) ) {
			alert("비밀번호는 6~20자의 최소 1개의 숫자, 영문자 를 포함해야 합니다."); 
			//str.focus();
			return false; 
		}
		
		if ( document.fregisterform.mb_passwd2.value.length <= 0 )
		{
			alert("\n비밀번호를 한번 더 입력하세요.");
			document.fregisterform.mb_passwd2.value="";
			return false;
		}

			if( document.fregisterform.mb_passwd1.value != document.fregisterform.mb_passwd2.value)
		{
			alert("\n비밀번호를 정확히 입력하세요.");
			document.fregisterform.mb_passwd1.value="";
			document.fregisterform.mb_passwd2.value="";
			return false;
		}

		//if ( _valid_length_check(2) ) return false;
		return true;
	}
	function mb_name_check() {
	   	if ( _valid_value_check() )    return false;
		if (! isKorean(document.fregisterform.mb_name)) {
			alert("\n이름은 한글만 입력하세요.");
			fregisterform.mb_name.value="";
	        	return false;
		}
		return true;
	}

	function mb_email_check(mb_email) {
	   	if(!isValidEmail(mb_email)){
			return false;
		}
		return true;
	}
	function _valid_value_check() {
	    	str = document.fregisterform.str_name.value;
		var strbyte = 0;
		for (i = 0; i < str.length; i++) {
			var code = str.charCodeAt(i);
			var ch   = str.substr(i,1).toUpperCase();
			code     = parseInt(code);
			if ( code >= 0 && code  <= 47 ) {
				document.fregisterform.str_name.value="";
				alert("특수문자는 사용하실 수 없습니다.");
				return 1;
			}
		}
		return 0;
	}

	// 입력값이 한글로 되어있는지 체크 (추가)
	function isKorean(input) {
		for(i=0; i<input.value.length; i++){
			var CodeNum = input.value.charCodeAt(i);
			if (CodeNum < 128){
				flag = false;
			} else {
				//alert("한글 아님");
				flag = true;
			}
		}
		return flag;
	}
	// 입력값이 이메일 형식인지 체크
	function isValidEmail(input) {
	//    var format = /^(\S+)@(\S+)\.([A-Za-z]+)$/;
	    var format = /^((\w|[\-\.])+)@((\w|[\-\.])+)\.([A-Za-z]+)$/;
	    return isValidFormat(input,format);
	}
	// 입력값이 사용자가 정의한 포맷 형식인지 체크
	function isValidFormat(input,format) {
		  if (input.search(format) != -1) {
	        return true; //올바른 포맷 형식
	    }
	    return false;
	}
	function fnc_semail1(str_value) {
		if (str_value == "") {
			document.fregisterform.mb_email2.value = "";
		} else {
			document.fregisterform.mb_email2.value = str_value;
		}
	}

	function OrderMemSearch(){
	
		var frm = document.fregisterform;
		var SendYn_1 = "Y";
		var SendYn_2 = "Y";
		var SendYn_3 = "Y";

		var mb_hp1		= frm.mb_hp1.value;
		var mb_hp2		= frm.mb_hp2.value;
		var mb_hp3		= frm.mb_hp3.value;

		if( mb_hp1 == "" || mb_hp2 == "" || mb_hp3 == "" ){
			SendYn_1 = "N";
			SendYn_2 = "N";
			SendYn_3 = "N";
		}

		if( SendYn_1 == "Y" && SendYn_2 == "Y" && SendYn_3 == "Y" ){

			$.ajax({

				  url: '/bbs/order_mem_check.php',
				  type: 'post',
				  async: false,
				  data: {
						"mb_hp1"	:	mb_hp1,
						"mb_hp2"	:	mb_hp2,
						"mb_hp3"	:	mb_hp3
				  },
				  dataType: 'text',
				  success: function(data) {

					ReturnValue= data.trim();

				  }

			});

			return ReturnValue;

		}else{

			return "N";		

		}
	
	}

    </script>
	<style>
	#mod_option_frm .btn_confirm .btn_close {
top:-346px;
}
#mod_option_frm {
    overflow-y: auto;
</style>
<?php
if (!defined("_GNUBOARD_")) exit; // 개별 페이지 접근 불가

if (!defined('_SHOP_')) {
    $pop_division = 'comm';
} else {
    $pop_division = 'shop';
}

$sql = " select * from {$g5['new_win_table']}
          where '".G5_TIME_YMDHIS."' between nw_begin_time and nw_end_time
            and nw_device IN ( 'both', 'mobile' ) and nw_division IN ( 'both', '".$pop_division."' ) and nw_service_view_1 = 'Y'
          order by nw_id asc ";
$result = sql_query($sql, false);

$cnt = sql_num_rows($result);
?>

<style>


    /*
    * ------------------------------
    * popup
    * ------------------------------
    */

    .modal-popup {
        position: fixed;
        width: 100%;
        height: 100%;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        background-color: rgba(0, 0, 0, 0.3);
        opacity: 0;
        transition: all 0.3s;
        z-index: 9999;
        display: none;
        margin-left: auto;
        margin-right: auto;
        -webkit-align-items: center;
        align-items: center;
        -webkit-justify-content: center;
        justify-content: center;
    }

    .modal-popup.on {
        display: -webkit-flex;
        display: flex;
        opacity: 1;
    }
    .modal-popup .modal-wrap {
        display: flex;
        flex-direction: column;
        position: relative;
        max-height: 100%;
        height: initial;
        overflow: hidden;
    }
    /* event */
    .modal-popup.modal-event .modal-wrap {
        width: 85%;
        max-width: 480px;
    }


    /*
    * ------------------------------
    * popup - header
    * ------------------------------
    */

    /*
    * ------------------------------
    * popup - content
    * ------------------------------
    */
    .modal-popup.modal-event .modal-body {
        position: relative;
        flex: 0 1 auto;
        overflow: hidden !important;
    }
    .event-slider-wrap {
        position: relative;
    }
    .event-slider-wrap .frame-guide {
        position: relative;
        width: 100%;
    }
    /* .event-slider-wrap {
        display: block;
        width: 40px;
        height: 40px;
        margin-top: -20px;
        background-image: url(/theme/basic/img/mobile/frame.png);
        background-size: 12px 25px;
        background-repeat: no-repeat;
        background-position: center;
    } */
    /*
    * ------------------------------
    * popup - button
    * ------------------------------
    */
    .btn-right {
        display: flex;
        justify-content: flex-end;
        padding: 10px 20px;
        /*border-top: 1px solid #ccc;*/
        /*background: #fff;*/
    }
    .modal-btn {
        border: 0;
        background: none;
        color: #252525;
        font-size: 14px;
        font-weight: bold;
    }
    .modal-popup.modal-event .bx-wrapper {
        position: absolute;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
    }
    .modal-popup.modal-event .bx-wrapper .bx-pager {
        padding-bottom: 0;
    }
    .modal-popup.modal-event .bx-wrapper .bx-pager.bx-default-pager a {
        width: 8px;
        height: 8px;
        margin: 0 4px;
        border-radius: 50%;
        background: #878787;
        border: none;
    }
    
    .modal-popup.modal-event .bx-wrapper .bx-pager.bx-default-pager a.active, 
    .modal-popup.modal-event .bx-wrapper .bx-pager.bx-default-pager a:focus, 
    .modal-popup.modal-event .bx-wrapper .bx-pager.bx-default-pager a:hover {
        background: #fff;
    }
    /*
    * ------------------------------
    * popup - button
    * ------------------------------
    */
    .btn-right {
        display: flex;
        justify-content: flex-end;
        padding: 10px 20px;
        border-top: 1px solid #ccc;
        background: #fff;
    }
    .event-btn {
        display: flex;
        align-items: center;
        justify-content: center;
        position: relative;
        width: 100%;
        flex: 0 1 auto;
        height: 55px;
        background: rgb(42, 42, 42);
        border: 0;
        color: #fff;
        font-size: 15px;
        letter-spacing: -0.19px;
        
    }
    .event-btn + .event-btn::before {
        position: absolute;
        left: 0;
        top: 50%;
        margin-top: -6px;
        content: '';
        display: block;
        width: 1px;
        height: 15px;
        background: rgba(255, 255, 255, 0.4);
    }
    .btn-box {
        display: flex;
        width: 100%;
    }
    /*
    * ------------------------------
    * bxslider - 오버라이드
    * ------------------------------
    */
    .modal-popup.modal-event .bx-wrapper .bx-viewport {
        position: absolute !important;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        width: 100%;
        height: 100% !important;

    }
    .modal-popup.modal-event .bx-wrapper {
        /*margin-bottom: 20px;*/
        box-shadow: none;
        border: none;
    }
    .bx-wrapper .bx-controls-direction a.bx-prev {
        left: 12px;
        display: block;
        width: 40px;
        height: 40px;
        margin-top: -20px;
        background-image: url(/theme/basic/img/mobile/slide-prev.png);
        background-size: 12px 25px;
        background-repeat: no-repeat;
        background-position: center;
    }
    .bx-wrapper .bx-controls-direction a.bx-next {
        right: 12px;
        display: block;
        width: 40px;
        height: 40px;
        margin-top: -20px;
        background-image: url(/theme/basic/img/mobile/slide-next.png);
        background-size: 12px 25px;
        background-repeat: no-repeat;
        background-position: center;

    }
    .modal-popup.modal-event .bx-wrapper .bx-pager {
        padding-top: 0;
    }
    .modal-popup.modal-event .bx-wrapper .bx-controls-auto .bx-controls-auto-item,
    .modal-popup.modal-event .bx-wrapper .bx-pager-item {
        vertical-align: middle;
    }
    .modal-popup.modal-event .bx-wrapper .bx-pager {
        font-size: 10px;
    }
    .modal-popup.modal-event .bx-wrapper .bx-controls-auto,
    .modal-popup.modal-event .bx-wrapper .bx-pager {
        bottom: 30px;
    }

    .event-slider {}
    .event-slider .slider { width: 100%; ;}
    .event-slider .slider img{ width: 100%;  }

</style>

<!-- 팝업레이어 시작 { -->
<?php if (!$_COOKIE["os_main_pop"] && $cnt > 0) { ?>
<!-- 모바일 팝업  -->
<!-- [D] 팝업열기 방법1 : .on 클래스 추가시 팝업 view -->
<!-- [D] 팝업열기 방법2 : data-pop(팝업창) 의 이름과 data-pop-btn(팝업여는 버튼) 의 이름이 같아야 함 -->
<section class="modal-popup modal-event on" data-pop="eventPop">
    <div class="modal-wrap" id="event_pop" style="visibility: hidden;">
        <div class="modal-body">
            <div class="event-slider-wrap">
                <img class="frame-guide" src="/theme/basic/img/mobile/frame.png" alt=""/>
                <div class="event-slider">
                    <?php
                    for ($i = 0; $nw = sql_fetch_array($result); $i++) {
                        $pop_view = "false";

                        // 이미 체크 되었다면 Continue
                        if ($_COOKIE["os_main_pop"])
                            continue;

                        //전체
                        if ($nw['nw_type'] == "A" || $nw['nw_type'] == "") {
                            $pop_view = "true";
                        }
                        //일반
                        if ($nw['nw_type'] == "B" && $member["mb_type"] == "0") {
                            $pop_view = "true";
                        }
                        //약국
                        if ($nw['nw_type'] == "C" && $member["mb_type"] == "1") {
                            $pop_view = "true";
                        }
                        ?>
                    <a href="<?php echo $nw['nw_href']?>" target="_self">
                        <?php if ($pop_view == "true") { ?>
                            <div class="slider">
                                <?php echo conv_content($nw['nw_content'], 1); ?>
                            </div>
                        <?php } ?>
                    <?php } ?>
                </div>
            </div>
        </div>
        <div class="modal-foot">
            <div class="btn-box">
                <button type="button" id="main_pop_reject" class="event-btn modal-close" data-pop-btn="eventPop" data-close-time="24" >오늘은 그만보기</button>
                <button type="button" data-pop-btn="eventPop" class="event-btn modal-close">닫기</button>
            </div>
        </div>
    </div>
</section>
<?php } ?>

<script>
    $(document).ready(function() {
        $('.event-slider').bxSlider( {
            mode: 'horizontal',// 가로 방향 수평 슬라이드 (fade)
            speed: 500,        // 이동 속도를 설정
//            pager: true,      // 현재 위치 페이징 표시 여부 설정
            moveSlides: 1,     // 슬라이드 이동시 개수
            // slideWidth: 100,   // 슬라이드 너비
            // minSlides: 4,      // 최소 노출 개수
            // maxSlides: 4,      // 최대 노출 개수
            // slideMargin: 5,    // 슬라이드간의 간격
            // auto: true,        // 자동 실행 여부
            // autoHover: true,   // 마우스 호버시 정지 여부
            controls: true,    // 이전 다음 버튼 노출 여부
            onSliderLoad: function(){
                $('#event_pop').css('visibility','visible');
            }
        });

        popupShowHide(); //팝업
       

        $("#main_pop_reject").click(function () {
            var exp_time = $(this).data('close-time');
            set_cookie('os_main_pop', 1, exp_time, g5_cookie_domain);
            $('.modal-close').click();
        });
    });

    /* =====================================
    * 레이어 팝업
    * =====================================*/
    function popupShowHide() {
        var openBtn = '[data-pop-btn]',
            closeBtn = '.modal-close';
        if($('.modal-popup').hasClass('on')) {
            $('body').css('overflow', 'hidden');
        } 
        function getTarget(t) {
            return $(t).attr('data-pop-btn');
        }
        function open(t) { //팝업열기
            var showTarget = $('[data-pop="' + t + '"]');
            showTarget.addClass('on').attr('aria-modal', true).find('.modal-close').focus();
            showTarget.find('.modal-close').data('activeTarget', t);
        }
        function close(t) { //팝업닫기
            var activeTarget = $('[data-pop="' + t + '"]');
            activeTarget.removeClass('on').removeAttr('aria-modal'); //팝업닫기
            $('[data-pop-btn="' + t + '"]').focus(); //버튼으로 포커스 가도록
        }
        $(document).on('click', openBtn, function (e) {
            e.preventDefault();
            open(getTarget(this));
        })
        .on('click', closeBtn, function (e) {
            e.preventDefault();
            close($(this).data('activeTarget'));
            $('body').css('overflow', 'auto');
        });
       
    }
</script>
<!-- } 팝업레이어 끝 -->
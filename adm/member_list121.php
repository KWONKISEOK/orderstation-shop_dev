<?php
include_once('./_common.php');

$mode	= $_POST['mode'];

$list_date = $fr_year . $fr_month;

$sql  = "select * , bb.pharm_bank_agree ";
$sql .= "from ( ";
$sql .= "  select ";
$sql .= "      pharm_custno, od_calculate_yn, sum(case when od_period_yn = 'Y' then od_total_incen / od_period_cnt ELSE od_total_incen end) incen ";
$sql .= "  from tbl_shop_order a ";
$sql .= "  inner join tbl_shop_order_detail b ";
$sql .= "      on a.od_id = b.od_id ";
$sql .= "  inner join tbl_shop_order_receiver c ";
$sql .= " 	    on a.od_id = c.od_id and b.od_num = c.od_num ";
$sql .= "  where ";
$sql .= "      c.od_pay_yn = 'Y' and a.od_gubun = '3' and date_format(c.od_date1, '%Y%m') = '" . $list_date . "' and c.od_status != '취소'  and a.pharm_custno != '' and b.it_id not in('S00603','S00604') ";
$sql .= "  group by pharm_custno, od_calculate_yn ";
$sql .= ") aa ";
$sql .= "inner join tbl_member bb ";
$sql .= "  on aa.pharm_custno = bb.pharm_custno and mb_type = '1' ";
if($stx != '') {
	$sql .= "  and pharm_name like '%$stx%' ";
}
$sql .= "order by aa.pharm_custno desc ";

//echo $sql;

$res = sql_query($sql);
if(!$res) exit('Cannot run query.');

switch($mode){
	
	case 'excel1':
		$ExcelTitle = urlencode( "개인소득지급관리_" . date( "YmdHis" ) );
		header( "Content-type: application/vnd.ms-excel" );
		header( "Content-type: application/vnd.ms-excel; charset=utf-8" );
		header( "Content-Disposition: attachment; filename = " . $ExcelTitle . ".xls" );
		header( "Content-Description: PHP4 Generated Data" );
		print( "<meta http-equiv=\"Content-Type\" content=\"application/vnd.ms-excel; charset=utf-8\">" );
		echo "<table border=1  >";
		echo "<tr bgcolor='#E7E7E7'>";
		echo "<th>약국명</th>";
		echo "<th>약사님</th>";
		echo "<th>은행명</th>";
		echo "<th>계좌번호</th>";
		echo "<th>예금주</th>";
		echo "<th>수수료</th>";
		echo "<th>개인소득(3%)</th>";
		echo "<th>주민세(10%)</th>";
		echo "<th>실지급금</th>";
		echo "<th>지급여부</th>";		
		echo "<th>동의여부</th>";		
		echo "</tr>";
		while($row = mysqli_fetch_array($res,MYSQL_ASSOC)) {


			$mb_id           = $row["mb_id"];
			$mb_name         = $row["mb_name"];
			$mb_tel          = $row["mb_tel"];
			$pharm_name   = $row["pharm_name"];
			$pharm_custno  = $row["pharm_custno"];
			

			$sales_name       = $row["sales_name"];
			$mb_status       = $row["pharm_status"];
		
			$pharm_bank_agree         = $row["pharm_bank_agree"];
			$pharm_bank_agree_date         = $row["pharm_bank_agree_date"];
			$pharm_bank_user         = $row["pharm_bank_user"];
			$pharm_bank_num          = $row["pharm_bank_num"];
			$pharm_bank_code          = $row["pharm_bank_code"];
			$od_calculate_yn        = $row["od_calculate_yn"];
			$incen        = $row["incen"];
			$org_incen           = $incen;

			if ( $pharm_bank_agree == '' ) {
				$mem_bank_agree = "N";
			}

			$pharm_bank_name = ViewBank( $pharm_bank_code );
			if (  $incen > 30000 ) {
				$incen2 = ( $incen * 0.003 ) * 10;
				$incen3 = ( $incen2 * 0.01 ) * 10;
				if ( $incen2 >= 1000 ) {
					$incen =  $incen  - ( $incen2  +  $incen3  );
				} else {
					$incen2 = 0;
					$incen3 = 0;
				}
			} else {
				$incen2 = 0;
				$incen3 = 0;
			}
			$incen = floor($incen);
			?>
			<tr>
			<td align="center" style="mso-number-format:\@"><?php echo $pharm_name; ?></td>
			<td align="center" style="mso-number-format:\@"><?php echo $mb_name; ?></td>		
			<td align="center" style="mso-number-format:\@"><?php echo $pharm_bank_name ; ?></td>
			<td align="center" style="mso-number-format:\@"><?php echo $pharm_bank_num; ?></td>		
			<td align="center" style="mso-number-format:\@"><?php echo $pharm_bank_user; ?></td>
			<td align="center"><?php echo number_format( $org_incen ); ?></td>
			<td align="center"><?php echo number_format( floor($incen2 / 10) * 10 ); ?></td>
			<td align="center"><?php echo number_format( floor($incen3 / 10) * 10 ); ?></td>
			<td align="center"><?php echo number_format( $incen ); ?></td>
			<td align="center" style="mso-number-format:\@"><?php echo $od_calculate_yn; ?></td>
			<td align="center" style="mso-number-format:\@"><?php echo $pharm_bank_agree; ?></td>
			<?php
		}
		echo "</table>";
		break;
	case 'excel2':
		$ExcelTitle = urlencode( "개인소득지급관리수동업로드용_" . date( "YmdHis" ) );
		header( "Content-type: application/vnd.ms-excel" );
		header( "Content-type: application/vnd.ms-excel; charset=utf-8" );
		header( "Content-Disposition: attachment; filename = " . $ExcelTitle . ".xls" );
		header( "Content-Description: PHP4 Generated Data" );
		print( "<meta http-equiv=\"Content-Type\" content=\"application/vnd.ms-excel; charset=utf-8\">" );
		echo "<table border=1  >";
		echo "<tr bgcolor='#E7E7E7'>";
		echo "<th>지급일</th>";
		echo "<th>판매자ID</th>";
		echo "<th>사업자번호</th>";
		echo "<th>지급액</th>";
		echo "<th>예금주</th>";
		echo "<th>은행코드</th>";
		echo "<th>계좌번호</th>";	
		echo "</tr>";
		while($row = mysqli_fetch_array($res,MYSQL_ASSOC)) {


			$mb_id           = $row["mb_id"];
			$mb_name         = $row["mb_name"];
			$mb_tel          = $row["mb_tel"];
			$pharm_name   = $row["pharm_name"];
			$pharm_custno  = $row["pharm_custno"];
			$pharm_number  = $row["pharm_number"];

			$sales_name       = $row["sales_name"];
			$mb_status       = $row["pharm_status"];
		
			$pharm_bank_agree         = $row["pharm_bank_agree"];
			$pharm_bank_agree_date         = $row["pharm_bank_agree_date"];
			$pharm_bank_user         = $row["pharm_bank_user"];
			$pharm_bank_num          = $row["pharm_bank_num"];
			$pharm_bank_code          = $row["pharm_bank_code"];
			$od_calculate_yn        = $row["od_calculate_yn"];
			$incen        = $row["incen"];
			$org_incen           = $incen;

			if ( $pharm_bank_agree == '' ) {
				$mem_bank_agree = "N";
			}

			$pharm_bank_name = ViewBank( $pharm_bank_code );
			if (  $incen > 30000 ) {
				$incen2 = ( $incen * 0.003 ) * 10;
				$incen3 = ( $incen2 * 0.01 ) * 10;
				if ( $incen2 >= 1000 ) {
					$incen =  $incen  - ( $incen2  +  $incen3  );
				} else {
					$incen2 = 0;
					$incen3 = 0;
				}
			} else {
				$incen2 = 0;
				$incen3 = 0;
			}
			$incen = floor($incen);

			$timestamp = strtotime("+1 days");
			if($pharm_bank_agree == 'Y') {
			?>
			<tr>
			<td align="center" style="mso-number-format:\@"><?php echo date("Y-m-d", $timestamp); ?></td>
			<td align="center" style="mso-number-format:\@"><?php echo $mb_id; ?></td>		
			<td align="center" style="mso-number-format:\@"><?php echo $pharm_number ; ?></td>
			<td align="center"><?php echo number_format( $incen ); ?></td>
			<td align="center" style="mso-number-format:\@"><?php echo $pharm_bank_user; ?></td>
			<td align="center" style="mso-number-format:\@"><?php echo $pharm_bank_code; ?></td>
			<td align="center" style="mso-number-format:\@"><?php echo $pharm_bank_num; ?></td>					
			<?php
			}
		}
		echo "</table>";
		break;	
}
?>
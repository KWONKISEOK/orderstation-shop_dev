<?php
include_once('./_common.php');

$mode	= $_POST['mode'];

$fr_year = $_POST['fr_year'];
$fr_month = $_POST['fr_month'];
$to_year = $_POST['to_year'];
$to_month = $_POST['to_month'];

//od_gubun
if ($od_gubun) {
	$sql_search = " and a.od_gubun = '$od_gubun' ";
}

$sql_search .= " and instr( c.od_status , '반품' ) <= 0 and not ( a.od_settle_case = '가상계좌' and c.od_pay_yn = 'N' ) and b.it_id not in('S00603','S00604')	";

switch($mode){
	case 'read':
		
		
		$sql = " select case od_gubun when '1' then 2 when '2' then 1 when '3' then 3 else 9 end od_gubun, 
       case sosok when '002001' then 1 when '002002' then 2 else 3 end sosok, 1 seq
						  ,sum(case right(odr_date, 2) when '01' then 1 else 0 end) month1
						  ,sum(case right(odr_date, 2) when '02' then 1 else 0 end) month2
						  ,sum(case right(odr_date, 2) when '03' then 1 else 0 end) month3
						  ,sum(case right(odr_date, 2) when '04' then 1 else 0 end) month4
						  ,sum(case right(odr_date, 2) when '05' then 1 else 0 end) month5
						  ,sum(case right(odr_date, 2) when '06' then 1 else 0 end) month6
						  ,sum(case right(odr_date, 2) when '07' then 1 else 0 end) month7
						  ,sum(case right(odr_date, 2) when '08' then 1 else 0 end) month8
						  ,sum(case right(odr_date, 2) when '09' then 1 else 0 end) month9
						  ,sum(case right(odr_date, 2) when '10' then 1 else 0 end) month10
						  ,sum(case right(odr_date, 2) when '11' then 1 else 0 end) month11
						  ,sum(case right(odr_date, 2) when '12' then 1 else 0 end) month12
						  ,count(*) tot_cnt
					from( 
					select distinct left(od_date1, 7) as odr_date, left(pharm_manager_dept, 6) sosok, a.od_gubun , a.od_id
						from tbl_shop_order a, tbl_shop_order_detail b, tbl_shop_order_receiver c  
						where a.od_id = b.od_id and a.od_id = c.od_id and b.od_num = c.od_num 
						and c.od_status != '취소' and (DATE_FORMAT(a.od_time,'%Y%m') between '$fr_year$fr_month' and '$to_year$to_month') ".$sql_search ."
					) aa 
					group by  case od_gubun when '1' then 2 when '2' then 1 when '3' then 3 else 9 end, 
					          case sosok when '002001' then 1 when '002002' then 2 else 3 end
	union all
	select case od_gubun when '1' then 2 when '2' then 1 when '3' then 3 else 9 end od_gubun, 
       case sosok when '002001' then 1 when '002002' then 2 else 3 end sosok, 2 seq
						  ,sum(case right(odr_date, 2) when '01' then od_total_drug_price else 0 end) month1
						  ,sum(case right(odr_date, 2) when '02' then od_total_drug_price else 0 end) month2
						  ,sum(case right(odr_date, 2) when '03' then od_total_drug_price else 0 end) month3
						  ,sum(case right(odr_date, 2) when '04' then od_total_drug_price else 0 end) month4
						  ,sum(case right(odr_date, 2) when '05' then od_total_drug_price else 0 end) month5
						  ,sum(case right(odr_date, 2) when '06' then od_total_drug_price else 0 end) month6
						  ,sum(case right(odr_date, 2) when '07' then od_total_drug_price else 0 end) month7
						  ,sum(case right(odr_date, 2) when '08' then od_total_drug_price else 0 end) month8
						  ,sum(case right(odr_date, 2) when '09' then od_total_drug_price else 0 end) month9
						  ,sum(case right(odr_date, 2) when '10' then od_total_drug_price else 0 end) month10
						  ,sum(case right(odr_date, 2) when '11' then od_total_drug_price else 0 end) month11
						  ,sum(case right(odr_date, 2) when '12' then od_total_drug_price else 0 end) month12
						  ,sum(od_total_drug_price) tot_cnt
					from( 
					select left(od_date1, 7) as odr_date, left(pharm_manager_dept, 6) sosok, a.od_gubun , b.od_total_drug_price
						from tbl_shop_order a, tbl_shop_order_detail b, tbl_shop_order_receiver c  
						where a.od_id = b.od_id and a.od_id = c.od_id and b.od_num = c.od_num and c.od_seq = 1
						and c.od_status != '취소' and (DATE_FORMAT(a.od_time,'%Y%m') between '$fr_year$fr_month' and '$to_year$to_month') ".$sql_search ."
					) aa 
					group by  case od_gubun when '1' then 2 when '2' then 1 when '3' then 3 else 9 end, 
					          case sosok when '002001' then 1 when '002002' then 2 else 3 end	
	  union all
	   select 1 od_gubun, 
       case sosok when '002001' then 1 when '002002' then 2 else 3 end sosok, 4 seq
						  ,sum(case right(yymm, 2) when '01' then mem_cnt else 0 end) month1
						  ,sum(case right(yymm, 2) when '02' then mem_cnt else 0 end) month2
						  ,sum(case right(yymm, 2) when '03' then mem_cnt else 0 end) month3
						  ,sum(case right(yymm, 2) when '04' then mem_cnt else 0 end) month4
						  ,sum(case right(yymm, 2) when '05' then mem_cnt else 0 end) month5
						  ,sum(case right(yymm, 2) when '06' then mem_cnt else 0 end) month6
						  ,sum(case right(yymm, 2) when '07' then mem_cnt else 0 end) month7
						  ,sum(case right(yymm, 2) when '08' then mem_cnt else 0 end) month8
						  ,sum(case right(yymm, 2) when '09' then mem_cnt else 0 end) month9
						  ,sum(case right(yymm, 2) when '10' then mem_cnt else 0 end) month10
						  ,sum(case right(yymm, 2) when '11' then mem_cnt else 0 end) month11
						  ,sum(case right(yymm, 2) when '12' then mem_cnt else 0 end) month12
						  ,sum(mem_cnt) tot_cnt
  from tbl_member_mover where yymm between '201801' and '201812'
  group by case sosok when '002001' then 1 when '002002' then 2 else 3 end
   union all
   select case od_gubun when '1' then 2 when '2' then 1 when '3' then 3 else 9 end od_gubun, 
       case sosok when '002001' then 1 when '002002' then 2 else 3 end sosok, 5 seq
						  ,sum(case right(odr_date, 2) when '01' then 1 else 0 end) month1
						  ,sum(case right(odr_date, 2) when '02' then 1 else 0 end) month2
						  ,sum(case right(odr_date, 2) when '03' then 1 else 0 end) month3
						  ,sum(case right(odr_date, 2) when '04' then 1 else 0 end) month4
						  ,sum(case right(odr_date, 2) when '05' then 1 else 0 end) month5
						  ,sum(case right(odr_date, 2) when '06' then 1 else 0 end) month6
						  ,sum(case right(odr_date, 2) when '07' then 1 else 0 end) month7
						  ,sum(case right(odr_date, 2) when '08' then 1 else 0 end) month8
						  ,sum(case right(odr_date, 2) when '09' then 1 else 0 end) month9
						  ,sum(case right(odr_date, 2) when '10' then 1 else 0 end) month10
						  ,sum(case right(odr_date, 2) when '11' then 1 else 0 end) month11
						  ,sum(case right(odr_date, 2) when '12' then 1 else 0 end) month12
						  ,count(*) tot_cnt
					from( 
					select distinct left(od_date1, 7) as odr_date, left(pharm_manager_dept, 6) sosok, a.od_gubun , a.mb_id
						from tbl_shop_order a, tbl_shop_order_detail b, tbl_shop_order_receiver c  
						where a.od_id = b.od_id and a.od_id = c.od_id and b.od_num = c.od_num 
						and a.od_gubun = '2' and c.od_status != '취소' and (DATE_FORMAT(a.od_time,'%Y%m') between '$fr_year$fr_month' and '$to_year$to_month')".$sql_search ."
					) aa 
					group by  case od_gubun when '1' then 2 when '2' then 1 when '3' then 3 else 9 end, 
					          case sosok when '002001' then 1 when '002002' then 2 else 3 end

				  order by od_gubun, sosok, seq";

		$res = sql_query($sql);
		
		if(!$res) exit('Cannot run query.');

		$response->page = 1;
		$response->total = 1;
		$response->records = 10;
		$i = 0;
		while($row = mysqli_fetch_array($res,MYSQL_ASSOC)) {

			switch($row['od_gubun']) {
				case '1':
					$od_gubun = '약국';
					break;
				case '2':
					$od_gubun = '고객';
					break;
				case '3':
					$od_gubun = '일반';
					break;
			}
			switch ( $row["sosok"] ) {
				case "1" :
					$sosok = "태전";
					break;
				case "2" :
					$sosok = "티제이";
					break;
				case "3" :
					$sosok = "기타";
					break;
			
			}
			switch ( $row["seq"] ) {
				case "1" :
					$data_type = "주문건수";
					break;
				case "2" :
					$data_type = "매출";
					break;
				case "4" :
					$data_type = "총거래처수";
					break;
				case "5" :
					$data_type = "가동처수";
					break;
			}

		    if ($dept) {
				if($dept == $row["sosok"]) {
					$response->rows[$i]['od_gubun']		= $od_gubun ;	
					$response->rows[$i]['sosok']		= $sosok;	
					$response->rows[$i]['data_type']	= $data_type;			
					$response->rows[$i]['month1']		= $row["month1"];
					$response->rows[$i]['month2']		= $row["month2"];
					$response->rows[$i]['month3']		= $row["month3"];
					$response->rows[$i]['month4']		= $row["month4"];
					$response->rows[$i]['month5']		= $row["month5"];
					$response->rows[$i]['month6']		= $row["month6"];
					$response->rows[$i]['month7']		= $row["month7"];
					$response->rows[$i]['month8']		= $row["month8"];
					$response->rows[$i]['month9']		= $row["month9"];
					$response->rows[$i]['month10']		= $row["month10"];
					$response->rows[$i]['month11']		= $row["month11"];
					$response->rows[$i]['month12']		= $row["month12"];
					$response->rows[$i]['tot_cnt']		= $row["tot_cnt"];				
				}
			} else {
				if($row['od_gubun'] == '9'){
					break;
				}
				$response->rows[$i]['od_gubun']		= $od_gubun ;	
				$response->rows[$i]['sosok']		= $sosok;	
				$response->rows[$i]['data_type']	= $data_type;			
				$response->rows[$i]['month1']		= $row["month1"];
				$response->rows[$i]['month2']		= $row["month2"];
				$response->rows[$i]['month3']		= $row["month3"];
				$response->rows[$i]['month4']		= $row["month4"];
				$response->rows[$i]['month5']		= $row["month5"];
				$response->rows[$i]['month6']		= $row["month6"];
				$response->rows[$i]['month7']		= $row["month7"];
				$response->rows[$i]['month8']		= $row["month8"];
				$response->rows[$i]['month9']		= $row["month9"];
				$response->rows[$i]['month10']		= $row["month10"];
				$response->rows[$i]['month11']		= $row["month11"];
				$response->rows[$i]['month12']		= $row["month12"];
				$response->rows[$i]['tot_cnt']		= $row["tot_cnt"];

			}

			$i++;
		} 

		echo json_encode($response);
		//echo $sql;
		break;
	
}



	

?>
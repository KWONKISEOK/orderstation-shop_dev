<?php
$sub_menu = '500400';
include_once('./_common.php');

auth_check($auth[$sub_menu], "r");

$g5['title'] = '주매출 요약';
include_once(G5_ADMIN_PATH.'/admin.head.php');
include_once(G5_PLUGIN_PATH.'/jquery-ui/datepicker.php');

//if(! preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $fr_date) ) $fr_date = '';
//if(! preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $to_date) ) $to_date = '';
//if($fr_date =='') $fr_date = date("Y-m-d", strtotime('-1 month'));
//if($to_date =='') $to_date = date("Y-m-d");



$year =date( "Y" );
$month =date( "m" );

?>

<link rel="stylesheet" type="text/css" media="screen" href="/adm/jqGrid/css/ui.jqgrid.css" />  


<form name="forderlist" id="forderlist" method="post" >
<input type="hidden" id="mode" name="mode"  value="read" />

<div class="local_sch03  tbl_wrap" style="padding:10px;">
    <table>
   
    <tbody>
    <tr>
        <th scope="row">
			<select name="od_date" id="od_date" >
				<option value="1" <?php echo get_selected($od_date, '1'); ?>>주문일</option>
				<option value="2" <?php echo get_selected($od_date, '2'); ?>>확정일</option>
				<option value="3" <?php echo get_selected($od_date, '3'); ?>>배송일</option>
				<option value="4" <?php echo get_selected($od_date, '4'); ?>>완료일</option>
			</select>
		</th>
        <td colspan=9>
			<select name="fr_year">
				<?php
				for ( $x = $year; $x > 2014; $x-- ) {?>
					<option value="<?php echo $x; ?>"><?php echo $x; ?>년</option>
				<?php } ?>
			</select>
			<select name="fr_month">
				<?php
				for ( $x = 1; $x <= 12; $x ++ ) { 
						if($x < 10) { $xstr = '0'.$x;} else {$xstr = $x;}
					?>
					<option value="<?php echo $xstr; ?>"><?php echo $x; ?>월</option>
				<?php } ?>
			</select>
			~
			<select name="to_year">
				<?php
				for ( $x = $year; $x > 2014; $x-- ) {?>
					<option value="<?php echo $x; ?>"><?php echo $x; ?>년</option>
				<?php } ?>
			</select>
			<select name="to_month">
				<?php
				for ( $x = 1; $x <= 12; $x ++ ) { 
						if($x < 10) { $xstr = '0'.$x;} else {$xstr = $x;}
						?>
					<option value="<?php echo $xstr; ?>" <?php if($month==$xstr) echo 'selected';?> ><?php echo $x; ?>월</option>
				<?php } ?>
			</select>
			
        </td>
		<td rowspan="2">
			 <input type="button" value="검색" class="get_theme_confc btn btn_01" style="cursor:pointer" onclick="formSubmit(1);">    
			 <!-- <input type="button" value="엑셀 다운로드" class="get_theme_confc btn btn_02" style="cursor:pointer" onclick="formSubmit(2);">  -->

		</td>
		
    </tr>
	<tr>
        <th scope="row">처리상태</th>
        <td>
			<select name="od_status" id="od_status"  >
			    <option value="">==전체==</option>
				<option value="주문" <?php echo get_selected($od_status, '주문'); ?>>주문</option>
				<option value="확정" <?php echo get_selected($od_status, '확정'); ?>>확정</option>
				<option value="배송" <?php echo get_selected($od_status, '배송'); ?>>배송</option>
				<option value="완료" <?php echo get_selected($od_status, '완료'); ?>>완료</option>
				<option value="취소" <?php echo get_selected($od_status, '취소'); ?>>취소</option>   
			</select>
			
        </td>

		<th scope="row">업체선택</th>
        <td>
			<select name="comp_code" id="comp_code" >
				<option value="">==업체선택==</option>
				<?php
				$sql2 = " select comp_code, comp_name from tbl_member where mb_type='7' order by comp_name ";
				$result2 = sql_query($sql2);
				for ($i=0; $row2=sql_fetch_array($result2); $i++) {
					if($comp_code ==$row2['comp_code']) 
						$checked='selected';
					else 
						$checked ='';

					echo '<option value="'.$row2['comp_code'].'" '.$checked.' >'.$row2['comp_name'].'</option>'.PHP_EOL;
				}
				?>
			</select>
        </td>
		<th scope="row">소속구분</th>
        <td>
			<select name="manager_dept" id="manager_dept" >
				<option value="">==전체==</option>
				<option value="002001" <?php echo get_selected($od_status, '002001'); ?>>태전약품</option>
				<option value="002002" <?php echo get_selected($od_status, '002002'); ?>>티제이팜</option>
				<option value="002003" <?php echo get_selected($od_status, '002003'); ?>>오엔케이</option>
				<option value=" " <?php echo get_selected($od_status, ' '); ?>>기타</option>
			</select>
        </td>
		<th scope="row">사원검색</th>
        <td>
			<select name="pharm_manager" id="pharm_manager" >
				<option value="">==전체==</option>
				<?php
				$sql2 = " select mb_id, mb_name from tbl_member where mb_type='5' order by comp_name ";
				$result2 = sql_query($sql2);
				for ($i=0; $row2=sql_fetch_array($result2); $i++) {
					
					echo '<option value="'.$row2['mb_id'].'" '.$checked.' >'.$row2['mb_name'].'</option>'.PHP_EOL;
				}
				?>
			</select>
        </td>
    </tr>
	
    </tbody>
    </table>
</div>


<div class=" tbl_wrap1">
<div id="preloader">
	 <img src="/adm/jqGrid/waiting.gif" align="absmiddle"> Loading...		
</div>
<table id="grid-table"></table>
<div id="grid-pager" ></div>


</div>   
</form>

<script>


$(function(){
    $("#fr_date, #to_date").datepicker({ changeMonth: true, changeYear: true, dateFormat: "yy-mm-dd", showButtonPanel: true, yearRange: "c-99:c+99", maxDate: "+0d" });

   
});

function set_date(today)
{
    <?php
    $date_term = date('w', G5_SERVER_TIME);
    $week_term = $date_term + 7;
    $last_term = strtotime(date('Y-m-d', G5_SERVER_TIME));
    ?>
    if (today == "오늘") {
        document.getElementById("fr_date").value = "<?php echo G5_TIME_YMD; ?>";
        document.getElementById("to_date").value = "<?php echo G5_TIME_YMD; ?>";
    } else if (today == "15일") {
        document.getElementById("fr_date").value = "<?php echo date('Y-m-d', strtotime('-13 Day', $last_term)); ?>";
        document.getElementById("to_date").value = "<?php echo date('Y-m-d', G5_SERVER_TIME); ?>";
    } else if (today == "1개월") {
        document.getElementById("fr_date").value = "<?php echo date('Y-m-d', strtotime('-1 Month', $last_term)); ?>";
        document.getElementById("to_date").value = "<?php echo date('Y-m-d', G5_SERVER_TIME); ?>";
    } else if (today == "2개월") {
       document.getElementById("fr_date").value = "<?php echo date('Y-m-d', strtotime('-2 Month', $last_term)); ?>";
        document.getElementById("to_date").value = "<?php echo date('Y-m-d', G5_SERVER_TIME); ?>";
	} else if (today == "3개월") {
        document.getElementById("fr_date").value = "<?php echo date('Y-m-d', strtotime('-3 Month', $last_term)); ?>";
        document.getElementById("to_date").value = "<?php echo date('Y-m-d', G5_SERVER_TIME); ?>";
    } 
}
</script>

<script src="/adm/jqGrid/js/i18n/grid.locale-ko.js"   type="text/javascript"></script>  
<script src="/adm/jqGrid/js/jquery.jqGrid.min.js"    type="text/javascript"></script> 
<script type="text/javascript"> 
	var grid_selector = "#grid-table";
	var pager_selector = "#grid-pager";

	$(document).ready(function () {
		var parent_column = $(grid_selector).closest('[class*="col-"]');
		//resize to fit page size
		$(window).on('resize.jqGrid', function () {
			$(grid_selector).jqGrid( 'setGridWidth', parent_column.width() );
		})
		var chkcell={cellId:undefined, chkval:undefined}; //cell rowspan 중복 체크

		jQuery(grid_selector).jqGrid({
			datatype :  fnSearch,
			colNames: ['년월','주차', '소속', '약국주문','고객주문','일반주문','합계'],	
		colModel: [
				   { name: 'od_date',      index: 'od_date',      width: 100, align: "center", sorttype: "data" ,cellattr:jsFormatterCell},    
				   { name: 'week_cnt',      index: 'week_cnt',      width: 100, align: "center", sorttype: "text" },  
				   { name: 'dept_name', index: 'dept_name',   width: 100, align: "center", sorttype: "text" }, 				   
				   { name: 'price1',      index: 'price1',      width: 100, align: "right",  formatter:'currency', formatoptions:{thousandsSeparator:",", decimalPlaces: 0} },
				   { name: 'price2',      index: 'price2',      width: 100, align: "right", formatter:'currency', formatoptions:{thousandsSeparator:",", decimalPlaces: 0} },
				   { name: 'price3',      index: 'price3',      width: 100, align: "right",  formatter:'currency', formatoptions:{thousandsSeparator:",", decimalPlaces: 0} },
				   { name: 'price4',      index: 'price4',      width: 100, align: "right", formatter:'currency', formatoptions:{thousandsSeparator:",", decimalPlaces: 0} },
				  ],
			viewrecords : true,
			rowNum:1000,
			rowList:[1000],
			pager : pager_selector,
			altRows: true,
			sortname: 'od_id',
			sortorder: "desc",
			height:510,
			multiboxonly: true,			
			autowidth: true,
            shrinkToFit: true,
			gridComplete: function() {  /** 데이터 로딩시 함수 **/
                var grid = this;
                 
                $('td[name="cellRowspan"]', grid).each(function() {
                    var spans = $('td[rowspanid="'+this.id+'"]',grid).length+1;
                    if(spans>1){
                     $(this).attr('rowspan',spans);
                    }
                });    
            }
		});
		function jsFormatterCell(rowid, val, rowObject, cm, rdata){
			var result = "";
			 
			if(chkcell.chkval != val){ //check 값이랑 비교값이 다른 경우
				var cellId = this.id + '_row_'+rowid+'-'+cm.name;
				result = ' rowspan="1" id ="'+cellId+'" + name="cellRowspan"';
			
				chkcell = {cellId:cellId, chkval:val};
			}else{
				result = 'style="display:none"  rowspanid="'+chkcell.cellId+'"'; //같을 경우 display none 처리
				
			}
			return result;
		}

	});

	function formSubmit(type) {
		if(type==1) {
			$('#mode').val('read');
			jQuery(grid_selector).trigger("reloadGrid");
		} else {


			var form = "<form action='calculate1_01.php' method='post'>"; 
			form += "<input type='hidden' name='mode' value='excel' />"; 
			form += "<input type='hidden' name='od_date' value='"+$('#od_date').val()+"' />"; 
			form += "<input type='hidden' name='fr_date' value='"+$('#fr_date').val()+"' />"; 
			form += "<input type='hidden' name='to_date' value='"+$('#to_date').val()+"' />"; 

			form += "<input type='hidden' name='od_status' value='"+$('#od_status option:selected').val()+"' />"; 
			form += "<input type='hidden' name='od_pay_yn' value='"+$('#od_pay_yn option:selected').val()+"' />"; 
			form += "<input type='hidden' name='od_gubun' value='"+$('#od_gubun option:selected').val()+"' />"; 
			form += "<input type='hidden' name='od_settle_case' value='"+$('#od_settle_case option:selected').val()+"' />"; 
			form += "<input type='hidden' name='comp_code' value='"+$('#comp_code option:selected').val()+"' />"; 
			form += "<input type='hidden' name='pharm_manager' value='"+$('#pharm_manager option:selected').val()+"' />"; 


			form += "</form>"; 
			jQuery(form).appendTo("body").submit().remove(); 


		}
	}
	function fnSearch(pdata) {
		$("#preloader").show();
		var jqGridParam = '';
		for (var i in pdata) {
			jqGridParam += '&'+i+'='+pdata[i];
		}

		$.ajax({
			url:'calculate4_01.php',
			type:'POST',
			data:$('#forderlist').serialize()+jqGridParam,
			success: function(data) {
	//alert(data);
				var thegrid = jQuery(grid_selector)[0];
				thegrid.addJSONData(eval("("+data+")"));
			},

			complete: function(jsondata,stat) {
				$("#preloader").fadeOut("fast");

			}
		});
	}
	

</script>


<?php
include_once (G5_ADMIN_PATH.'/admin.tail.php');
?>

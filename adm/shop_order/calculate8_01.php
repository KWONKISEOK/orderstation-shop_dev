<?php
include_once('./_common.php');

$mode	= $_POST['mode'];

//조회조건
//날자구분
switch($od_date) {
	case '1':
		$where[] = " c.od_date1 between '$fr_date 00:00:00' and '$to_date 23:59:59' ";
		break;
	case '2':
		$where[] = " c.od_date2 between '$fr_date 00:00:00' and '$to_date 23:59:59' ";
		break;
	case '3':
		$where[] = " c.od_date3 between '$fr_date 00:00:00' and '$to_date 23:59:59' ";
		break;
	case '4':
		$where[] = " c.od_date4 between '$fr_date 00:00:00' and '$to_date 23:59:59' ";
		break;
	default:
		$where[] = " c.od_date1 between '$fr_date 00:00:00' and '$to_date 23:59:59' ";
		break;
}
//상태
if ($od_status) {
	$where[] = " c.od_status = '$od_status' ";
}
//결제구분
/*if ($od_settle_case) {
	 switch($od_settle_case) {
		case '빌링결제':
			$where[] = " a.od_billkey != '' ";
			break;
		default:
			$where[] = " a.od_settle_case = '$od_settle_case' ";
			break;
	}
}*/
//입금여부
/*if ($od_pay_yn) {
	$where[] = " c.od_pay_yn = '$od_pay_yn' ";
}*/
//공급사코드
if ($comp_code) {
	$where[] = " b.comp_code = '$comp_code' ";
}
//주문구분
if ($od_gubun) {
	$where[] = " a.od_gubun = '$od_gubun' ";
}

//사원구분
/*if ($pharm_manager) {
	$where[] = " a.pharm_manager = '$pharm_manager' ";
}*/
//카테고리 구분
if ($ca_id) {
	$where[] = "ca_id like '$ca_id%'";
}
switch($sel_field) {
	case 'od_id':
		$where[] = " a.od_id = '$search' ";
		break;
	case 'od_name':
		$where[] = " od_name = '$search'";
		break;
	case 'od_b_name':
		$where[] = " c.od_b_name = '$search' ";
		break;
	case 'it_name':
		$where[] = " b.it_name like '%$search%' ";
		break;
	case 'mb_id':
		$where[] = " a.mb_id = '$search' ";
		break;
	case 'pharm_name':
		$where[] = " a.pharm_name like '%$search%'";
		break;
	default:
		
		break;
}

if ($where) {
	$sql_search = implode(' and ', $where);
}

$sql_search .= " and instr( c.od_status , '반품' ) <= 0 and not ( a.od_settle_case = '가상계좌' and c.od_pay_yn = 'N' ) and b.it_id not in('S00603','S00604')	";

switch($mode){
	case 'read':
		

		$page		= $_POST['page'];
		$limit		= $_POST['rows'];
		$sidx		= $_POST['sidx'];
		$sord		= $_POST['sord'];
		$orderby	= $sidx." ".$sord;

	
		$sql = " select count(*) cnt
				   from tbl_shop_order a
					inner join tbl_shop_order_detail b on a.od_id = b.od_id
					inner join tbl_shop_order_receiver c on a.od_id = c.od_id and b.od_num = c.od_num
					left join tbl_member f on b.comp_code = f.comp_code
				   left join tbl_shop_item e on b.it_id = e.it_id
				where c.od_status in ('확정','배송','완료') and ".$sql_search ;

		$res = sql_fetch($sql);
		if(!$res) exit('Cannot run query.');

		$count = $res['cnt'];

		if( $count > 0 && $limit > 0) { 
			$total_pages = ceil($count/$limit); 
		} else { 
			$total_pages = 0; 
		}
		if ($page > $total_pages) $page=$total_pages;
		$start = $limit*$page - $limit;
		if($start <0) $start = 0;
		
		$sql = " select date_format(a.od_time, '%Y-%m-%d') as od_date
			 , a.od_gubun
			 , b.it_name,b.od_option, c.od_qty, od_total_drug_price, od_total_incen, od_sum_supply_price, od_sum_dodome_price, b.od_total_sale_price,od_sum_drug_price,od_sum_sale_price
			 , od_itm_drug_price, c.od_status,  e.it_notax
			 , (select pharm_div from tbl_member where a.mb_id = mb_id) pharm_div
			 
			 , od_date1, od_date2, od_date3, od_date4
			 , b.it_id, b.od_option
			 ,SUM(c.od_qty)AS tot_qty
			 , (
				select ca_name	
				from tbl_shop_category
				where ca_id = e.ca_id
			   ) as ca_name1
			 , (	
				select ca_name	
					from tbl_shop_category
					where ca_id = e.ca_id2
			  ) as ca_name2
			 , b.od_price , f.comp_name ,SUM(c.od_qty)*od_sum_drug_price AS totalsum
			from tbl_shop_order a
				inner join tbl_shop_order_detail b on a.od_id = b.od_id
				inner join tbl_shop_order_receiver c on a.od_id = c.od_id and b.od_num = c.od_num
				left join tbl_member f on b.comp_code = f.comp_code
			   left join tbl_shop_item e on b.it_id = e.it_id
			where c.od_status != '취소' and ".$sql_search ." 
			group by od_option ORDER BY ".$orderby." LIMIT ".$start.",".$limit;


		/*$sql = "SELECT *
				 FROM tbl_member
				 WHERE mb_type=0 ".$sql_search." 
				 ORDER BY ".$orderby." LIMIT ".$start.",".$limit;*/
		
		$res = sql_query($sql);
		if(!$res) exit('Cannot run query.');

		$response->page = $page;
		$response->total = $total_pages;
		$response->records = $count;
		$i = 0;
		while($row = mysqli_fetch_array($res,MYSQL_ASSOC)) {

			switch($row['od_gubun']) {
				case '1':
					$od_gubun = '고객주문';
					break;
				case '2':
					$od_gubun = '약국주문';
					break;
				case '3':
					$od_gubun = '일반주문';
					break;
			}
			switch ( $row["pharm_div"] ) {
				case "1" :
					$pharm_div = "태전약품";
					break;
				case "2" :
					$pharm_div = "티제이팜";
					break;
			}

			switch ( $row["it_notax"] ) {
				case "0" :
					$it_notax = "과세";
					break;
				case "1" :
					$it_notax = "비과세";
					break;
				case "2" :
					$it_notax = "면세";
					break;
			}

			if (  $row["od_sum_dodome_price"] > 0 ) {
				$dodome_margin = (  $row["od_sum_dodome_price"]  -  $row["od_sum_supply_price"]  ) /  $row["od_sum_dodome_price"] ;
			} else {
				$dodome_margin = 0;
			}

			if (  $row["od_total_drug_price"]  > 0 ) {
				$drug_margin = (  $row["od_total_drug_price"]  -  $row["od_sum_dodome_price"]  ) /  $row["od_total_drug_price"];
			} else {
				$drug_margin = 0;
			}

			if (  $row["od_total_sale_price"]  > 0 ) {
				$sale_margin = (  $row["od_total_sale_price"]  -  $row["od_total_drug_price"]  ) /  $row["od_total_sale_price"] ;
			} else {
				$sale_margin = 0;
			}

			//$odr_total_2 = ( mycdbl( $row["odd_total_price"] ) * mycdbl( $row["odr_goods_cnt"] ) ) + mycdbl( $row["odd_trans_price"] );
			$odr_total_2 =  $row["od_total_sale_price"] +  $row["od_send_cost"];

			
			$response->rows[$i]['od_date']		= $row[od_date];
			$response->rows[$i]['od_id']		= $row[od_id];
			$response->rows[$i]['mb_id']		= $row[mb_id];
			$response->rows[$i]['od_name']		= $row[od_name];
			$response->rows[$i]['it_name']		= $row[it_name];
			$response->rows[$i]['tot_qty']		= $row[tot_qty];
			$response->rows[$i]['totalsum']		= $row[totalsum];
			$response->rows[$i]['od_total_incen']		= $row[od_total_incen];
			$response->rows[$i]['od_itm_drug_price']		= $row[od_itm_drug_price];
			$response->rows[$i]['od_status']		= $row[od_status];
			$response->rows[$i]['od_gubun']			= $od_gubun;
			$response->rows[$i]['it_notax']			= $it_notax;
			$response->rows[$i]['pharm_div']		= $pharm_div;
			$response->rows[$i]['sales_name']		= $row[sales_name];
			$response->rows[$i]['od_sum_supply_price']		= $row[od_sum_supply_price];
			$response->rows[$i]['od_sum_dodome_price']		= $row[od_sum_dodome_price];
			$response->rows[$i]['od_sum_drug_price']		= $row[od_sum_drug_price];
			$response->rows[$i]['od_sum_sale_price']		= $row[od_sum_sale_price];
			

			$response->rows[$i]['od_date1']		= DateFormat($row[od_date1]);
			$response->rows[$i]['od_date2']		= DateFormat($row[od_date2]);
			$response->rows[$i]['od_date3']		= DateFormat($row[od_date3]);
			$response->rows[$i]['od_date4']		= DateFormat($row[od_date4]);
			$response->rows[$i]['it_id']		= $row[it_id];
			$response->rows[$i]['od_option']		= $row[od_option];
			$response->rows[$i]['od_receipt_point']		= $row[od_receipt_point];
			$response->rows[$i]['od_coupon']		= $row[od_coupon];
			$response->rows[$i]['od_invoice']		= $row[od_invoice];
			$response->rows[$i]['od_delivery_company']		= $row[od_delivery_company];
			$response->rows[$i]['od_b_name']		= $row[od_b_name];
			$response->rows[$i]['ca_name1']		= $row[ca_name1];
			$response->rows[$i]['ca_name2']		= $row[ca_name2];
			$response->rows[$i]['od_price']		= $row[od_price];
			$response->rows[$i]['od_send_cost']		= $row[od_send_cost];
			$response->rows[$i]['comp_name']		= $row[comp_name];
			$response->rows[$i]['od_total_sale_price']		= $row[od_total_sale_price];

			$response->rows[$i]['dodome_margin']		= round($dodome_margin,2);
			$response->rows[$i]['drug_margin']			= round($drug_margin,2);
			$response->rows[$i]['sale_margin']			= round($sale_margin,2);
			$response->rows[$i]['odr_total_2']			= $odr_total_2;

			$i++;
		} 

		echo json_encode($response);
		//echo $sql;
		break;
	case 'excel':

		$ExcelTitle = urlencode( "상품별매출_" . date( "YmdHis" ) );
		header( "Content-type: application/vnd.ms-excel" );
		header( "Content-type: application/vnd.ms-excel; charset=utf-8" );
		header( "Content-Disposition: attachment; filename = " . $ExcelTitle . ".xls" );
		header( "Content-Description: PHP4 Generated Data" );
		print( "<meta http-equiv=\"Content-Type\" content=\"application/vnd.ms-excel; charset=utf-8\">" );
		echo "<table border=1  >";
		echo "<tr bgcolor='#E7E7E7'>";	
		echo "<th>주문구분</th>";
		echo "<th>업체명</th>";
		echo "<th>카테고리1</th>";
		echo "<th>카테고리2</th>";
		echo "<th>상품명</th>";
		echo "<th>옵션명</th>";
		echo "<th>합계</th>";
		echo "<th>약국출하가</th>";	
		echo "<th>판매수량</th>";
		echo "</tr>";
		
		$orderby = 'totalsum desc';

		$sql = " select date_format(a.od_time, '%Y-%m-%d') as od_date
			 , a.od_gubun
			 , b.it_name,b.od_option, c.od_qty, od_total_drug_price, od_total_incen, od_sum_supply_price, od_sum_dodome_price, b.od_total_sale_price,od_sum_drug_price,od_sum_sale_price
			 , od_itm_drug_price, c.od_status,  e.it_notax
			 , (select pharm_div from tbl_member where a.mb_id = mb_id) pharm_div
			 
			 , od_date1, od_date2, od_date3, od_date4
			 , b.it_id, b.od_option
			 ,SUM(c.od_qty)AS tot_qty
			 , (
				select ca_name	
				from tbl_shop_category
				where ca_id = e.ca_id
			   ) as ca_name1
			 , (	
				select ca_name	
					from tbl_shop_category
					where ca_id = e.ca_id2
			  ) as ca_name2
			 , b.od_price , f.comp_name, SUM(c.od_qty)*od_sum_drug_price AS totalsum
			from tbl_shop_order a
				inner join tbl_shop_order_detail b on a.od_id = b.od_id
				inner join tbl_shop_order_receiver c on a.od_id = c.od_id and b.od_num = c.od_num
				left join tbl_member f on b.comp_code = f.comp_code
			   left join tbl_shop_item e on b.it_id = e.it_id
			where c.od_status != '취소' and ".$sql_search ." 
			group by od_option ORDER BY ".$orderby;

		$res = sql_query($sql);
		if(!$res) exit('Cannot run query.');

		$i = 0;
		while($row = mysqli_fetch_array($res,MYSQL_ASSOC)) {
		

			switch($row['od_gubun']) {
				case '1':
					$od_gubun = '고객주문';
					break;
				case '2':
					$od_gubun = '약국주문';
					break;
				case '3':
					$od_gubun = '일반주문';
					break;
			}


			if (  $row["od_sum_dodome_price"] > 0 ) {
				$dodome_margin = (  $row["od_sum_dodome_price"]  -  $row["od_sum_supply_price"]  ) /  $row["od_sum_dodome_price"] ;
			} else {
				$dodome_margin = 0;
			}

			if (  $row["od_total_drug_price"]  > 0 ) {
				$drug_margin = (  $row["od_total_drug_price"]  -  $row["od_sum_dodome_price"]  ) /  $row["od_total_drug_price"];
			} else {
				$drug_margin = 0;
			}

			if (  $row["od_total_sale_price"]  > 0 ) {
				$sale_margin = (  $row["od_total_sale_price"]  -  $row["od_total_drug_price"]  ) /  $row["od_total_sale_price"] ;
			} else {
				$sale_margin = 0;
			}

			//$odr_total_2 = ( mycdbl( $row["odd_total_price"] ) * mycdbl( $row["odr_goods_cnt"] ) ) + mycdbl( $row["odd_trans_price"] );
			$odr_total_2 =  $row["od_total_sale_price"] +  $row["od_send_cost"];

			echo "<tr>";
			
			echo "<td>" . $od_gubun . "</td>";
			echo "<td>" . $row["comp_name"] . "</td>";
			echo "<td>" . $row["ca_name1"] . "</td>";
			echo "<td>" . $row["ca_name2"] . "</td>";
			echo "<td style=\"mso-number-format:'\@';\">" . $row["it_name"] . "</td>";
			echo "<td style=\"mso-number-format:'\@';\">" . $row["od_option"] . "</td>";
			echo "<td>" . $row["totalsum"] . "</td>";
			echo "<td>" . $row["od_total_drug_price"] . "</td>";
		
			echo "<td>" . $row["tot_qty"] . "</td>";
			echo "</tr>";

			$i++;
		} 
		break;
	
}



	

?>
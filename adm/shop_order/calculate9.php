<?php
$sub_menu = '500210';
include_once('./_common.php');

auth_check($auth[$sub_menu], "r");

$g5['title'] = '담당자별매출';
include_once(G5_ADMIN_PATH.'/admin.head.php');
include_once(G5_PLUGIN_PATH.'/jquery-ui/datepicker.php');

if(! preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $fr_date) ) $fr_date = '';
if(! preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $to_date) ) $to_date = '';
if($fr_date =='') $fr_date = date("Y-m-d", strtotime('-1 month'));
if($to_date =='') $to_date = date("Y-m-d");
?>

<link rel="stylesheet" type="text/css" media="screen" href="/adm/jqGrid/css/ui.jqgrid.css" />  


<form name="forderlist" id="forderlist" method="post" >
<input type="hidden" id="mode" name="mode"  value="read" />

<div class="local_sch03  tbl_wrap" style="padding:10px;">
    <table>
   
    <tbody>
    <tr>
        <th scope="row">
			<select name="od_date" id="od_date" >
				<option value="1" <?php echo get_selected($od_date, '1'); ?>>주문일</option>
				<option value="2" <?php echo get_selected($od_date, '2'); ?>>확정일</option>
				<option value="3" <?php echo get_selected($od_date, '3'); ?>>배송일</option>
				<option value="4" <?php echo get_selected($od_date, '4'); ?>>완료일</option>
			</select>
		</th>
        <td colspan=5>
            <input type="text" id="fr_date"  name="fr_date" value="<?php echo $fr_date; ?>" class="frm_input" size="10" maxlength="10"> ~
			<input type="text" id="to_date"  name="to_date" value="<?php echo $to_date; ?>" class="frm_input" size="10" maxlength="10">
			<button type="button" onclick="javascript:set_date('오늘');">오늘</button>
			<button type="button" onclick="javascript:set_date('15일');">15일</button>
			<button type="button" onclick="javascript:set_date('1개월');">1개월</button>
			<button type="button" onclick="javascript:set_date('2개월');">2개월</button>
			<button type="button" onclick="javascript:set_date('12개월');">3개월</button>
        </td>
		<td rowspan="3">
			 <input type="button" value="검색" class="get_theme_confc btn btn_01" style="cursor:pointer" onclick="formSubmit(1);">    
			 <!-- <input type="button" value="엑셀 다운로드" class="get_theme_confc btn btn_02" style="cursor:pointer" onclick="formSubmit(2);">     -->

		</td>
		
    </tr>
	<tr>
        <th scope="row">처리상태</th>
        <td>
			<select name="od_status" id="od_status"  >
			    <option value="">==전체==</option>
				<option value="주문" <?php echo get_selected($od_status, '주문'); ?>>주문</option>
				<option value="확정" <?php echo get_selected($od_status, '확정'); ?>>확정</option>
				<option value="배송" <?php echo get_selected($od_status, '배송'); ?>>배송</option>
				<option value="완료" <?php echo get_selected($od_status, '완료'); ?>>완료</option>
				<option value="취소" <?php echo get_selected($od_status, '취소'); ?>>취소</option>   
			</select>
			
        </td>
		
		<th scope="row">주문구분</th>
        <td>
           <select name="od_gubun" id="od_gubun" >
				<option value="">==전체==</option>
				<option value="1" <?php echo get_selected($od_gubun, '1'); ?>>고객주문</option>
				<option value="2" <?php echo get_selected($od_gubun, '2'); ?>>약국주문</option>
				<option value="3" <?php echo get_selected($od_gubun, '3'); ?>>일반주문</option>
			</select>
        </td>
    </tr>
	<tr>

        
		<th scope="row">업체선택</th>
        <td>
			<select name="comp_code" id="comp_code" >
				<option value="">==업체선택==</option>
				<?php
				$sql2 = " select comp_code, comp_name from tbl_member where mb_type='7' order by comp_name ";
				$result2 = sql_query($sql2);
				for ($i=0; $row2=sql_fetch_array($result2); $i++) {
					if($comp_code ==$row2['comp_code']) 
						$checked='selected';
					else 
						$checked ='';

					echo '<option value="'.$row2['comp_code'].'" '.$checked.' >'.$row2['comp_name'].'</option>'.PHP_EOL;
				}
				?>
			</select>
        </td>
		<th scope="row">사원검색</th>
        <td>
			<select name="pharm_manager" id="pharm_manager" >
				<option value="">==사원구분==</option>
				<?php
				$sql2 = " select mb_id, mb_name from tbl_member where mb_type='5' order by comp_name ";
				$result2 = sql_query($sql2);
				for ($i=0; $row2=sql_fetch_array($result2); $i++) {
					
					echo '<option value="'.$row2['mb_id'].'" '.$checked.' >'.$row2['mb_name'].'</option>'.PHP_EOL;
				}
				?>
			</select>
        </td>
		<th scope="row">검색</th>
        <td>
            <select name="sel_field" id="sel_field">
				<option value="od_id" >주문번호</option>			
				<option value="od_name">주문자</option>
				<option value="od_b_name" >받는분</option>
				<option value="it_name" >상품명</option>
				<option value="mb_id" >아이디</option>
				<option value="pharm_name" >약국명</option>   
			</select>
			<label for="search" class="sound_only">검색어<strong class="sound_only"> 필수</strong></label>
			<input type="text" name="search" id="search" class=" frm_input" autocomplete="off">
        </td>
    </tr>
	
    </tbody>
    </table>
</div>


<div class=" tbl_wrap1">
<div id="preloader">
	 <img src="/adm/jqGrid/waiting.gif" align="absmiddle"> Loading...		
</div>
<table id="grid-table"></table>
<div id="grid-pager" ></div>


</div>   
</form>
<?php

?>

<?php //$qstr ='comp_code='.$_GET['comp_code'].'&'.$qstr;
echo get_paging(G5_IS_MOBILE ? $config['cf_mobile_pages'] : $config['cf_write_pages'], $page, $total_page, "{$_SERVER['SCRIPT_NAME']}?$qstr&amp;page="); ?>

<script>


$(function(){
    $("#fr_date, #to_date").datepicker({ changeMonth: true, changeYear: true, dateFormat: "yy-mm-dd", showButtonPanel: true, yearRange: "c-99:c+99", maxDate: "+0d" });

   
});

function set_date(today)
{
    <?php
    $date_term = date('w', G5_SERVER_TIME);
    $week_term = $date_term + 7;
    $last_term = strtotime(date('Y-m-d', G5_SERVER_TIME));
    ?>
    if (today == "오늘") {
        document.getElementById("fr_date").value = "<?php echo G5_TIME_YMD; ?>";
        document.getElementById("to_date").value = "<?php echo G5_TIME_YMD; ?>";
    } else if (today == "15일") {
        document.getElementById("fr_date").value = "<?php echo date('Y-m-d', strtotime('-13 Day', $last_term)); ?>";
        document.getElementById("to_date").value = "<?php echo date('Y-m-d', G5_SERVER_TIME); ?>";
    } else if (today == "1개월") {
        document.getElementById("fr_date").value = "<?php echo date('Y-m-d', strtotime('-1 Month', $last_term)); ?>";
        document.getElementById("to_date").value = "<?php echo date('Y-m-d', G5_SERVER_TIME); ?>";
    } else if (today == "2개월") {
       document.getElementById("fr_date").value = "<?php echo date('Y-m-d', strtotime('-2 Month', $last_term)); ?>";
        document.getElementById("to_date").value = "<?php echo date('Y-m-d', G5_SERVER_TIME); ?>";
	} else if (today == "3개월") {
        document.getElementById("fr_date").value = "<?php echo date('Y-m-d', strtotime('-3 Month', $last_term)); ?>";
        document.getElementById("to_date").value = "<?php echo date('Y-m-d', G5_SERVER_TIME); ?>";
    } 
}
</script>

<script src="/adm/jqGrid/js/i18n/grid.locale-ko.js"   type="text/javascript"></script>  
<script src="/adm/jqGrid/js/jquery.jqGrid.min.js"    type="text/javascript"></script> 
<script type="text/javascript"> 
	var grid_selector = "#grid-table";
	var pager_selector = "#grid-pager";

	$(document).ready(function () {
		var parent_column = $(grid_selector).closest('[class*="col-"]');
		//resize to fit page size
		$(window).on('resize.jqGrid', function () {
			$(grid_selector).jqGrid( 'setGridWidth', parent_column.width() );
		})

		jQuery(grid_selector).jqGrid({
			datatype :  fnSearch,
			colNames: ['주문구분','소속','담당자','판매금액','판매수량'],	
		colModel: [
				   { name: 'od_gubun',      index: 'od_gubun',      width: 55, align: "center" },    				  		 
				   { name: 'dept',      index: 'dept',      width: 80, align: "center", sorttype: "text" },    
				   { name: 'od_sales_name',      index: 'od_sales_name',      width: 80, align: "center", sorttype: "text" },    				  
				   { name: 'od_total_price',      index: 'od_total_price',      width: 40, align: "right",  formatter:'currency', formatoptions:{thousandsSeparator:",", decimalPlaces: 0} },	   
				   { name: 'od_goods_cnt',      index: 'od_goods_cnt',      width: 50, align: "right", sorttype: "int" }, 			
				  ],
			viewrecords : true,
			rowNum:1000,
			rowList:[1000],
			pager : pager_selector,
			altRows: true,
			sortname: 'od_id',
			sortorder: "desc",
			height:510,
			multiboxonly: true,			
			autowidth: true,
            shrinkToFit: true
		});
	});

	function formSubmit(type) {
		
			$('#mode').val('read');
			jQuery(grid_selector).trigger("reloadGrid");
	
	}
	function fnSearch(pdata) {
		$("#preloader").show();
		var jqGridParam = '';
		for (var i in pdata) {
			jqGridParam += '&'+i+'='+pdata[i];
		}

		$.ajax({
			url:'calculate9_01.php',
			type:'POST',
			data:$('#forderlist').serialize()+jqGridParam,
			success: function(data) {

				var thegrid = jQuery(grid_selector)[0];
				thegrid.addJSONData(eval("("+data+")"));
			},

			complete: function(jsondata,stat) {
				$("#preloader").fadeOut("fast");
				//var thegrid = jQuery(grid_selector)[0];
				//updatePagerIcons(thegrid);			
			}
		});
	}
	function updatePagerIcons(table) {
		var replacement = 
		{
			'ui-icon-seek-first' : 'fa fa-angle-double-left bigger-140',
			'ui-icon-seek-prev' : 'fa fa-angle-left bigger-140',
			'ui-icon-seek-next' : 'fa fa-angle-right bigger-140',
			'ui-icon-seek-end' : 'fa fa-angle-double-right bigger-140'
		};
		$('.ui-pg-table:not(.navtable) > tbody > tr > .ui-pg-button > .ui-icon').each(function(){
			var icon = $(this);
			var $class = $.trim(icon.attr('class').replace('ui-icon', ''));
			
			if($class in replacement) icon.attr('class', 'ui-icon '+replacement[$class]);
		})
	}

</script>


<div id="Excel_Select" title="엑셀 출력 선택">
	<form name="ExcelFrm" id="ExcelFrm" action="calculate1_01.php?<?php echo $qstr; ?>" method="post" target="iFrm">
		<table class="table_style1">
			<tr>
				<td width="150"></td>
				<td><input type="radio" name="ptype" value="1" id="ExcelParamSelect1"><label for="ExcelParamSelect1">전체</label>
				</td>
			</tr>
			
		</table>
	</form>
</div>
<script>
	$("#Excel_Select").dialog({
		autoOpen: false,
		resizable: false,
		modal: true,
		width: 800,
		buttons: {
			"엑셀다운로드": function () {
				$("#ExcelFrm").submit();
				$(this).dialog("close");
			},
			Cancel: function () {
				$(this).dialog("close");
			}
		}
	});

	$("#ExcelDownButton").click(function () {
		$("#Excel_Select").dialog("open");
	});

</script>
<!-- (s)hidden Frame -->
<iframe name="iFrm" style="display: none;" ></iframe>


<?php
include_once (G5_ADMIN_PATH.'/admin.tail.php');
?>

<?php
include_once('./_common.php');

$mode	= $_POST['mode'];

//조회조건
//날자구분
switch($od_date) {
	case '1':
		$where[] = " c.od_date1 between '$fr_date 00:00:00' and '$to_date 23:59:59' ";
		break;
	case '2':
		$where[] = " c.od_date2 between '$fr_date 00:00:00' and '$to_date 23:59:59' ";
		break;
	case '3':
		$where[] = " c.od_date3 between '$fr_date 00:00:00' and '$to_date 23:59:59' ";
		break;
	case '4':
		$where[] = " c.od_date4 between '$fr_date 00:00:00' and '$to_date 23:59:59' ";
		break;
	default:
		$where[] = " c.od_date1 between '$fr_date 00:00:00' and '$to_date 23:59:59' ";
		break;
}
//상태
if ($od_status) {
	$where[] = " c.od_status = '$od_status' ";
}
//결제구분
if ($od_settle_case) {
	 switch($od_settle_case) {
		case '빌링결제':
			$where[] = " a.od_billkey != '' ";
			break;
		default:
			$where[] = " a.od_settle_case = '$od_settle_case' ";
			break;
	}
}
//입금여부
if ($od_pay_yn) {
	$where[] = " c.od_pay_yn = '$od_pay_yn' ";
}
//공급사코드
if ($comp_code) {
	$where[] = " b.comp_code = '$comp_code' ";
}
//주문구분
if ($od_gubun) {
	$where[] = " a.od_gubun = '$od_gubun' ";
}

//사원구분
if ($pharm_manager) {
	$where[] = " a.pharm_manager = '$pharm_manager' ";
}

//반품이 아닌것들만 가지고 오기
$where[] = " instr( c.od_status , '반품' ) <= 0  ";

if ($where) {
	$sql_search = implode(' and ', $where);
}

switch($mode){
	case 'read':

		
		$sql = " select od_gubun, pharm_manager_dept, pharm_manager, sum(od_total_drug_price) as od_total_price
					, sum(od_qty) as od_goods_cnt
					, (select code_nm from tbl_code where major_cd='A03' and minor_cd = left(aa.pharm_manager_dept,6)) as od_sales_sosok_title 
					, (select code_nm from tbl_code where major_cd='A03' and minor_cd = aa.pharm_manager_dept) as od_sales_sosok_name 
					, (select mb_name from tbl_member where mb_id = aa.pharm_manager) as od_sales_name
					 from(
					select od_total_drug_price, c.od_qty , a.od_gubun, a.pharm_manager_dept, a.pharm_manager
						from tbl_shop_order a
							inner join tbl_shop_order_detail b on a.od_id = b.od_id
							inner join tbl_shop_order_receiver c on a.od_id = c.od_id and b.od_num = c.od_num
							inner join tbl_member f on b.comp_code = f.comp_code
						where c.od_status != '취소' and ".$sql_search ."
					) as aa
					group by pharm_manager_dept, pharm_manager, od_gubun
					order by od_gubun, pharm_manager_dept, pharm_manager desc ";

		$res = sql_query($sql);
		if(!$res) exit('Cannot run query.');

		$response->page = 1;
		$response->total = 1;
		$response->records = 1000;
		$i = 0;
		while($row = mysqli_fetch_array($res,MYSQL_ASSOC)) {

			switch($row['od_gubun']) {
				case '1':
					$od_gubun = '고객주문';
					break;
				case '2':
					$od_gubun = '약국주문';
					break;
				case '3':
					$od_gubun = '일반주문';
					break;
			}
			
			$response->rows[$i]['od_gubun']			= $od_gubun;
			$response->rows[$i]['dept']				= $row[od_sales_sosok_title].' '.$row[od_sales_sosok_name];
			$response->rows[$i]['od_sales_name']	= $row[od_sales_name];
			$response->rows[$i]['od_total_price']	= $row[od_total_price];			
			$response->rows[$i]['od_goods_cnt']		= $row[od_goods_cnt];
			
			$i++;
		} 

		echo json_encode($response);
		break;
	
}



	

?>
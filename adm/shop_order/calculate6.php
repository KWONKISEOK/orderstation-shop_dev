<?php
$sub_menu = '500310';
include_once('./_common.php');

auth_check($auth[$sub_menu], "r");

$g5['title'] = '가동처 현황';
include_once(G5_ADMIN_PATH.'/admin.head.php');
include_once(G5_PLUGIN_PATH.'/jquery-ui/datepicker.php');

if(! preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $fr_date) ) $fr_date = '';
if(! preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $to_date) ) $to_date = '';
if($fr_date =='') $fr_date = date("Y-m-d", strtotime('-1 month'));
if($to_date =='') $to_date = date("Y-m-d");



$year =date( "Y" );
$month =date( "m" );

?>

<link rel="stylesheet" type="text/css" media="screen" href="/adm/jqGrid/css/ui.jqgrid.css" />  


<form name="forderlist" id="forderlist" method="post" >
<input type="hidden" id="mode" name="mode"  value="read" />

<div class="local_sch03  tbl_wrap" style="padding:10px;">
    <table>
   
    <tbody>
    <tr>
        <th scope="row">
			년도
		</th>
        <td >
			<select name="fr_year" id="fr_year">
				<?php
				for ( $x = $year; $x > 2014; $x-- ) {?>
					<option value="<?php echo $x; ?>"><?php echo $x; ?>년</option>
				<?php } ?>
			</select>
			
			
        </td>
		
		<td >
			 <input type="button" value="검색" class="get_theme_confc btn btn_01" style="cursor:pointer" onclick="formSubmit(1);">    
		
		</td>
		
    </tr>

    </tbody>
    </table>
</div>


<div class=" tbl_wrap1">
<div id="preloader">
	 <img src="/adm/jqGrid/waiting.gif" align="absmiddle"> Loading...		
</div>
<table id="grid-table"></table>
<div id="grid-pager" ></div>


</div>   
</form>

<script>


$(function(){
    $("#fr_date, #to_date").datepicker({ changeMonth: true, changeYear: true, dateFormat: "yy-mm-dd", showButtonPanel: true, yearRange: "c-99:c+99", maxDate: "+0d" });

   
});

function set_date(today)
{
    <?php
    $date_term = date('w', G5_SERVER_TIME);
    $week_term = $date_term + 7;
    $last_term = strtotime(date('Y-m-d', G5_SERVER_TIME));
    ?>
    if (today == "오늘") {
        document.getElementById("fr_date").value = "<?php echo G5_TIME_YMD; ?>";
        document.getElementById("to_date").value = "<?php echo G5_TIME_YMD; ?>";
    } else if (today == "15일") {
        document.getElementById("fr_date").value = "<?php echo date('Y-m-d', strtotime('-13 Day', $last_term)); ?>";
        document.getElementById("to_date").value = "<?php echo date('Y-m-d', G5_SERVER_TIME); ?>";
    } else if (today == "1개월") {
        document.getElementById("fr_date").value = "<?php echo date('Y-m-d', strtotime('-1 Month', $last_term)); ?>";
        document.getElementById("to_date").value = "<?php echo date('Y-m-d', G5_SERVER_TIME); ?>";
    } else if (today == "2개월") {
       document.getElementById("fr_date").value = "<?php echo date('Y-m-d', strtotime('-2 Month', $last_term)); ?>";
        document.getElementById("to_date").value = "<?php echo date('Y-m-d', G5_SERVER_TIME); ?>";
	} else if (today == "3개월") {
        document.getElementById("fr_date").value = "<?php echo date('Y-m-d', strtotime('-3 Month', $last_term)); ?>";
        document.getElementById("to_date").value = "<?php echo date('Y-m-d', G5_SERVER_TIME); ?>";
    } 
}
</script>

<script src="/adm/jqGrid/js/i18n/grid.locale-ko.js"   type="text/javascript"></script>  
<script src="/adm/jqGrid/js/jquery.jqGrid.min.js"    type="text/javascript"></script> 
<script type="text/javascript"> 
	var grid_selector = "#grid-table";
	var pager_selector = "#grid-pager";

	$(document).ready(function () {
		var parent_column = $(grid_selector).closest('[class*="col-"]');
		//resize to fit page size
		$(window).on('resize.jqGrid', function () {
			$(grid_selector).jqGrid( 'setGridWidth', parent_column.width() );
		})
		var chkcell={cellId:undefined, chkval:undefined}; //cell rowspan 중복 체크

		jQuery(grid_selector).jqGrid({
			datatype :  fnSearch,
			colNames: ['구분','1월', '2월', '3월','4월','5월','6월','7월', '8월', '9월','10월','11월','12월'],	
		colModel: [
				   { name: 'dept_name',      index: 'dept_name',      width: 100, align: "center", sorttype: "data" },    			   
				   { name: 'month1',      index: 'month1',      width: 100, align: "right",  formatter:'currency', formatoptions:{thousandsSeparator:",", decimalPlaces: 0} },
				   { name: 'month2',      index: 'month2',      width: 100, align: "right", formatter:'currency', formatoptions:{thousandsSeparator:",", decimalPlaces: 0} },
				   { name: 'month3',      index: 'month3',      width: 100, align: "right",  formatter:'currency', formatoptions:{thousandsSeparator:",", decimalPlaces: 0} },
				   { name: 'month4',      index: 'month4',      width: 100, align: "right", formatter:'currency', formatoptions:{thousandsSeparator:",", decimalPlaces: 0} },
					{ name: 'month5',      index: 'month5',      width: 100, align: "right", formatter:'currency', formatoptions:{thousandsSeparator:",", decimalPlaces: 0} },
					{ name: 'month6',      index: 'month6',      width: 100, align: "right", formatter:'currency', formatoptions:{thousandsSeparator:",", decimalPlaces: 0} },
					{ name: 'month7',      index: 'month7',      width: 100, align: "right", formatter:'currency', formatoptions:{thousandsSeparator:",", decimalPlaces: 0} },
					{ name: 'month8',      index: 'month8',      width: 100, align: "right", formatter:'currency', formatoptions:{thousandsSeparator:",", decimalPlaces: 0} },
					{ name: 'month9',      index: 'month9',      width: 100, align: "right", formatter:'currency', formatoptions:{thousandsSeparator:",", decimalPlaces: 0} },
					{ name: 'month10',      index: 'month10',      width: 100, align: "right", formatter:'currency', formatoptions:{thousandsSeparator:",", decimalPlaces: 0} },
					{ name: 'month11',      index: 'month11',      width: 100, align: "right", formatter:'currency', formatoptions:{thousandsSeparator:",", decimalPlaces: 0} },
					{ name: 'month12',      index: 'month12',      width: 100, align: "right", formatter:'currency', formatoptions:{thousandsSeparator:",", decimalPlaces: 0} },
				  ],
			viewrecords : true,
			rowNum:1000,
			rowList:[1000],
			pager : pager_selector,
			altRows: true,
			sortname: 'od_id',
			sortorder: "desc",
			height:500,
			multiboxonly: true,			
			autowidth: true,
            shrinkToFit: true
		});
	
	});

	function formSubmit(type) {
	
			jQuery(grid_selector).trigger("reloadGrid");
	
	}
	function fnSearch(pdata) {
		$("#preloader").show();
		var jqGridParam = '';
		for (var i in pdata) {
			jqGridParam += '&'+i+'='+pdata[i];
		}

		$.ajax({
			url:'calculate6_01.php',
			type:'POST',
			data:$('#forderlist').serialize()+jqGridParam,
			success: function(data) {
	
				var thegrid = jQuery(grid_selector)[0];
				thegrid.addJSONData(eval("("+data+")"));
			},

			complete: function(jsondata,stat) {
				$("#preloader").fadeOut("fast");

			}
		});
	}
	

</script>


<?php
include_once (G5_ADMIN_PATH.'/admin.tail.php');
?>

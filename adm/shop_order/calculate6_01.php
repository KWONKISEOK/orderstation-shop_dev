<?php
include_once('./_common.php');

$mode	= $_POST['mode'];

$fr_year = $_POST['fr_year'];

switch($mode){
	case 'read':
		
		
		$sql = " select IFNULL(sosok,'') sosok
						  ,sum(case right(odr_date, 2) when '01' then 1 else 0 end) month1
						  ,sum(case right(odr_date, 2) when '02' then 1 else 0 end) month2
						  ,sum(case right(odr_date, 2) when '03' then 1 else 0 end) month3
						  ,sum(case right(odr_date, 2) when '04' then 1 else 0 end) month4
						  ,sum(case right(odr_date, 2) when '05' then 1 else 0 end) month5
						  ,sum(case right(odr_date, 2) when '06' then 1 else 0 end) month6
						  ,sum(case right(odr_date, 2) when '07' then 1 else 0 end) month7
						  ,sum(case right(odr_date, 2) when '08' then 1 else 0 end) month8
						  ,sum(case right(odr_date, 2) when '09' then 1 else 0 end) month9
						  ,sum(case right(odr_date, 2) when '10' then 1 else 0 end) month10
						  ,sum(case right(odr_date, 2) when '11' then 1 else 0 end) month11
						  ,sum(case right(odr_date, 2) when '12' then 1 else 0 end) month12
					from( 
					select distinct left(od_date1, 7) as odr_date, left(pharm_manager_dept, 6) sosok, a.mb_id 
						from tbl_shop_order a, tbl_shop_order_detail b, tbl_shop_order_receiver c  
						where a.od_id = b.od_id and a.od_id = c.od_id and b.od_num = c.od_num 
						and od_gubun in('1','2') and c.od_status != '취소' and a.od_time > '$fr_year-01-01' and instr( c.od_status , '반품' ) <= 0
						and not ( a.od_settle_case = '가상계좌' and c.od_pay_yn = 'N' ) and b.it_id not in('S00603','S00604')
						union all 
						select distinct left(od_date1, 7) as odr_date, left(pharm_manager_dept, 6) sosok, a.pharm_custno  mb_id
						from tbl_shop_order a, tbl_shop_order_detail b, tbl_shop_order_receiver c
						where a.od_id = b.od_id and a.od_id = c.od_id and b.od_num = c.od_num 
						and od_gubun in('3') and c.od_status != '취소' and a.od_time > '$fr_year-01-01' and instr( c.od_status , '반품' ) <= 0
						and not ( a.od_settle_case = '가상계좌' and c.od_pay_yn = 'N' ) and b.it_id not in('S00603','S00604')
						
					) aa 
					group by IFNULL(sosok,'')
					order by sosok desc, odr_date ";

		$res = sql_query($sql);
		if(!$res) exit('Cannot run query.');

		$response->page = 1;
		$response->total = 1;
		$response->records = 10;
		$i = 0;
		while($row = mysqli_fetch_array($res,MYSQL_ASSOC)) {

			$sosok = $row["sosok"];
			$dept_name = '기타지역';
			if($sosok == '002001') $dept_name = '태전약품';
			if($sosok == '002002') $dept_name = '티제이팜';
		
			$response->rows[$i]['dept_name']	= $dept_name;			
			$response->rows[$i]['month1']		= $row["month1"];
			$response->rows[$i]['month2']		= $row["month2"];
			$response->rows[$i]['month3']		= $row["month3"];
			$response->rows[$i]['month4']		= $row["month4"];
			$response->rows[$i]['month5']		= $row["month5"];
			$response->rows[$i]['month6']		= $row["month6"];
			$response->rows[$i]['month7']		= $row["month7"];
			$response->rows[$i]['month8']		= $row["month8"];
			$response->rows[$i]['month9']		= $row["month9"];
			$response->rows[$i]['month10']		= $row["month10"];
			$response->rows[$i]['month11']		= $row["month11"];
			$response->rows[$i]['month12']		= $row["month12"];

			$i++;
		} 

		echo json_encode($response);
		break;
	
}



	

?>
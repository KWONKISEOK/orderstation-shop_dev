<?php
$sub_menu = '500110';
include_once('./_common.php');

auth_check($auth[$sub_menu], "r");

$g5['title'] = '정산';
include_once(G5_ADMIN_PATH.'/admin.head.php');
include_once(G5_PLUGIN_PATH.'/jquery-ui/datepicker.php');

if(! preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $fr_date) ) $fr_date = '';
if(! preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $to_date) ) $to_date = '';
if($fr_date =='') $fr_date = date("Y-m-d", strtotime('-1 month'));
if($to_date =='') $to_date = date("Y-m-d");
?>

<link rel="stylesheet" type="text/css" media="screen" href="/adm/jqGrid/css/ui.jqgrid.css" />  


<form name="forderlist" id="forderlist" method="post" >
<input type="hidden" id="mode" name="mode"  value="read" />

<div class="local_sch03  tbl_wrap" style="padding:10px;">
    <table>
   
    <tbody>
    <tr>
        <th scope="row">
			<select name="od_date" id="od_date" >
				<option value="1" <?php echo get_selected($od_date, '1'); ?>>주문일</option>
				<option value="2" <?php echo get_selected($od_date, '2'); ?>>확정일</option>
				<option value="3" <?php echo get_selected($od_date, '3'); ?>>배송일</option>
				<option value="4" <?php echo get_selected($od_date, '4'); ?>>완료일</option>
				<option value="5" <?php echo get_selected($od_date, '5'); ?>>반품접수일</option>
				<option value="6" <?php echo get_selected($od_date, '6'); ?>>반품반려일</option>
				<option value="7" <?php echo get_selected($od_date, '7'); ?>>반품완료일</option>
			</select>
		</th>
        <td colspan=5>
            <input type="text" id="fr_date"  name="fr_date" value="<?php echo $fr_date; ?>" class="frm_input" size="10" maxlength="10"> ~
			<input type="text" id="to_date"  name="to_date" value="<?php echo $to_date; ?>" class="frm_input" size="10" maxlength="10">
			<button type="button" onclick="javascript:set_date('오늘');">오늘</button>
			<button type="button" onclick="javascript:set_date('15일');">15일</button>
			<button type="button" onclick="javascript:set_date('1개월');">1개월</button>
			<button type="button" onclick="javascript:set_date('2개월');">2개월</button>
			<button type="button" onclick="javascript:set_date('12개월');">3개월</button>
        </td>
		<td rowspan="3">
			 <input type="button" value="검색" class="get_theme_confc btn btn_01" style="cursor:pointer" onclick="formSubmit(1);">    
			 <input type="button" value="엑셀 다운로드" class="get_theme_confc btn btn_02" style="cursor:pointer" onclick="formSubmit(2);">    

		</td>
		
    </tr>
	<tr>
        <th scope="row">처리상태</th>
        <td>
			<select name="od_status" id="od_status"  >
			    <option value="">==전체==</option>
				<option value="주문" <?php echo get_selected($od_status, '주문'); ?>>주문</option>
				<option value="확정" <?php echo get_selected($od_status, '확정'); ?>>확정</option>
				<option value="배송" <?php echo get_selected($od_status, '배송'); ?>>배송</option>
				<option value="완료" <?php echo get_selected($od_status, '완료'); ?>>완료</option>
				<option value="취소" <?php echo get_selected($od_status, '취소'); ?>>취소</option>   
				<option value="반품접수" <?php echo get_selected($od_status, '반품접수'); ?>>반품접수</option>   
				<option value="반품반려" <?php echo get_selected($od_status, '반품반려'); ?>>반품반려</option>   
				<option value="반품완료" <?php echo get_selected($od_status, '반품완료'); ?>>반품완료</option>  
			</select>
			
        </td>
		<th scope="row">입금여부</th>
        <td>
            <select name="od_pay_yn" id="od_pay_yn" >
				<option value="">==전체==</option>
				<option value="N" <?php echo get_selected($od_pay_yn, 'N'); ?>>미입금</option>
				<option value="Y" <?php echo get_selected($od_pay_yn, 'Y'); ?>>입금</option>
			</select>
        </td>
		<th scope="row">주문구분</th>
        <td>
           <select name="od_gubun" id="od_gubun" >
				<option value="">==전체==</option>
				<option value="1" <?php echo get_selected($od_gubun, '1'); ?>>고객주문</option>
				<option value="2" <?php echo get_selected($od_gubun, '2'); ?>>약국주문</option>
				<option value="3" <?php echo get_selected($od_gubun, '3'); ?>>일반주문</option>
			</select>
        </td>
    </tr>
	<tr>

        <th scope="row">결제구분</th>
        <td>
			<select name="od_settle_case" id="od_settle_case" >
				<option value="">==전체==</option>
				<option value="무통장" <?php echo get_selected($od_settle_case, '무통장'); ?>>미입금</option>
				<option value="가상계좌" <?php echo get_selected($od_settle_case, '가상계좌'); ?>>가상계좌</option>
				<option value="계좌이체" <?php echo get_selected($od_settle_case, '계좌이체'); ?>>계좌이체</option>
				<option value="휴대폰" <?php echo get_selected($od_settle_case, '휴대폰'); ?>>휴대폰</option>
				<option value="신용카드" <?php echo get_selected($od_settle_case, '신용카드'); ?>>신용카드</option>
				<option value="빌링결제" <?php echo get_selected($od_settle_case, '빌링결제'); ?>>빌링결제</option>
				<option value="KAKAOPAY" <?php echo get_selected($od_settle_case, 'KAKAOPAY'); ?>>KAKAOPAY</option>

			</select>
        </td>
		<th scope="row">업체선택</th>
        <td>
			<select name="comp_code" id="comp_code" style="width:200px;">
				<option value="">==업체선택==</option>
				<?php
				$sql2 = " select comp_code, comp_name from tbl_member where mb_type='7' order by comp_name ";
				$result2 = sql_query($sql2);
				for ($i=0; $row2=sql_fetch_array($result2); $i++) {
					if($comp_code ==$row2['comp_code']) 
						$checked='selected';
					else 
						$checked ='';

					echo '<option value="'.$row2['comp_code'].'" '.$checked.' >'.$row2['comp_name'].'</option>'.PHP_EOL;
				}
				?>
			</select>
        </td>
		<th scope="row">사원검색</th>
        <td>
			<select name="pharm_manager" id="pharm_manager" >
				<option value="">==사원구분==</option>
				<?php
				$sql2 = " select mb_id, mb_name from tbl_member where mb_type='5' order by comp_name ";
				$result2 = sql_query($sql2);
				for ($i=0; $row2=sql_fetch_array($result2); $i++) {
					
					echo '<option value="'.$row2['mb_id'].'" '.$checked.' >'.$row2['mb_name'].'</option>'.PHP_EOL;
				}
				?>
			</select>
        </td>
    </tr>
	
    </tbody>
    </table>
</div>


<div class=" tbl_wrap1">
<div id="preloader">
	 <img src="/adm/jqGrid/waiting.gif" align="absmiddle"> Loading...		
</div>
<table id="grid-table"></table>
<div id="grid-pager" ></div>


</div>   
</form>
<?php

?>

<?php //$qstr ='comp_code='.$_GET['comp_code'].'&'.$qstr;
echo get_paging(G5_IS_MOBILE ? $config['cf_mobile_pages'] : $config['cf_write_pages'], $page, $total_page, "{$_SERVER['SCRIPT_NAME']}?$qstr&amp;page="); ?>

<script>


$(function(){
    $("#fr_date, #to_date").datepicker({ changeMonth: true, changeYear: true, dateFormat: "yy-mm-dd", showButtonPanel: true, yearRange: "c-99:c+99", maxDate: "+0d" });

   
});

function set_date(today)
{
    <?php
    $date_term = date('w', G5_SERVER_TIME);
    $week_term = $date_term + 7;
    $last_term = strtotime(date('Y-m-d', G5_SERVER_TIME));
    ?>
    if (today == "오늘") {
        document.getElementById("fr_date").value = "<?php echo G5_TIME_YMD; ?>";
        document.getElementById("to_date").value = "<?php echo G5_TIME_YMD; ?>";
    } else if (today == "15일") {
        document.getElementById("fr_date").value = "<?php echo date('Y-m-d', strtotime('-13 Day', $last_term)); ?>";
        document.getElementById("to_date").value = "<?php echo date('Y-m-d', G5_SERVER_TIME); ?>";
    } else if (today == "1개월") {
        document.getElementById("fr_date").value = "<?php echo date('Y-m-d', strtotime('-1 Month', $last_term)); ?>";
        document.getElementById("to_date").value = "<?php echo date('Y-m-d', G5_SERVER_TIME); ?>";
    } else if (today == "2개월") {
       document.getElementById("fr_date").value = "<?php echo date('Y-m-d', strtotime('-2 Month', $last_term)); ?>";
        document.getElementById("to_date").value = "<?php echo date('Y-m-d', G5_SERVER_TIME); ?>";
	} else if (today == "3개월") {
        document.getElementById("fr_date").value = "<?php echo date('Y-m-d', strtotime('-3 Month', $last_term)); ?>";
        document.getElementById("to_date").value = "<?php echo date('Y-m-d', G5_SERVER_TIME); ?>";
    } 
}
</script>

<script src="/adm/jqGrid/js/i18n/grid.locale-ko.js"   type="text/javascript"></script>  
<script src="/adm/jqGrid/js/jquery.jqGrid.min.js"    type="text/javascript"></script> 
<script type="text/javascript"> 
	var grid_selector = "#grid-table";
	var pager_selector = "#grid-pager";

	$(document).ready(function () {
		var parent_column = $(grid_selector).closest('[class*="col-"]');
		//resize to fit page size
		$(window).on('resize.jqGrid', function () {
			$(grid_selector).jqGrid( 'setGridWidth', parent_column.width() );
		})
		
		jQuery(grid_selector).jqGrid({
			datatype :  fnSearch,
			colNames: ['주문일자','주문번호','회사구분','담당자','주문자','수령인','카테고리1','카테고리2','업체명','상품명','옵션명','과세','수량','사입가','도매가마진','도매가','약국출하가 마진','약국출하가','소비자가 마진','소비자가','택배비','소비자가합계','주문상태'],	
		colModel: [
				   { name: 'od_date',      index: 'od_date',      width: 55, align: "center", sorttype: "data" },    
				   { name: 'od_id',      index: 'od_id',      width: 75, align: "center", sorttype: "text" },    
				   { name: 'pharm_div', index: 'pharm_div',   width: 38, align: "center", sorttype: "text" }, 
				   { name: 'sales_name',  index: 'sales_name',  width: 35, align: "center", sorttype: "text" }, 
				   { name: 'od_name',      index: 'od_name',      width: 35, align: "center", sorttype: "text" },    
				   { name: 'od_b_name',      index: 'od_b_name',      width: 35, align: "center", sorttype: "text" },    
				   { name: 'ca_name1',      index: 'ca_name1',      width: 80, align: "center", sorttype: "text" },    
				   { name: 'ca_name2',      index: 'ca_name2',      width: 80, align: "center", sorttype: "text" }, 
				   { name: 'comp_name',      index: 'comp_name',      width: 80, align: "center", sorttype: "text" },    
				   { name: 'it_name',      index: 'it_name',   width: 150,    align: "left", sorttype: "text" },   
				   { name: 'od_option',      index: 'od_option',    width: 150,   align: "left", sorttype: "text" },   
				   { name: 'it_notax',      index: 'gds_tax',      width: 30, align: "center", sorttype: "text" },    
				   { name: 'od_qty',      index: 'odr_goods_cnt',      width: 25, align: "right", sorttype: "int" },
				   { name: 'od_sum_supply_price',      index: 'od_sum_supply_price',      width: 40, align: "right", sorttype: "int" },      				   
				   { name: 'dodome_margin',      index: 'dodome_margin',      width: 50, align: "right", sorttype: "int" },      				   
				   { name: 'od_sum_dodome_price',      index: 'od_sum_dodome_price',      width: 40, align: "right", sorttype: "int" },      				   
				   { name: 'drug_margin',      index: 'drug_margin',      width: 70, align: "right", sorttype: "int" },      				   
				   { name: 'od_total_drug_price',      index: 'od_total_drug_price',      width: 50, align: "right", sorttype: "int" },      				   
				   { name: 'sale_margin',      index: 'sale_margin',      width: 60, align: "right", sorttype: "int" },      				   
				   { name: 'od_total_sale_price',      index: 'od_total_sale_price',      width: 40, align: "right", sorttype: "int" },      				   
				   { name: 'od_send_cost',      index: 'od_send_cost',      width: 30, align: "right", sorttype: "int" },      				   
				   { name: 'odr_total_2',      index: 'odr_total_2',      width: 60, align: "right", sorttype: "int" },  
				   { name: 'od_status',      index: 'od_status',      width: 40, align: "center", sorttype: "text" },
				  ],
			viewrecords : true,
			rowNum:20,
			rowList:[20,100, 1000],
			pager : pager_selector,
			altRows: true,
			sortname: 'a.od_id',
			sortorder: "desc",
			height:510,
			autowidth: true,
            shrinkToFit: true,
			footerrow: true,
			userDataOnFooter : true,
			gridComplete: function () {

				var $self = $(this);
				var od_sum_supply_price = $self.jqGrid("getCol", "od_sum_supply_price", false, "sum");
				var od_sum_dodome_price = $self.jqGrid("getCol", "od_sum_dodome_price", false, "sum");
				var od_total_drug_price = $self.jqGrid("getCol", "od_total_drug_price", false, "sum");
				var od_total_sale_price = $self.jqGrid("getCol", "od_total_sale_price", false, "sum");
				var od_send_cost = $self.jqGrid("getCol", "od_send_cost", false, "sum");
				var odr_total_2 = $self.jqGrid("getCol", "odr_total_2", false, "sum");
				var od_qty = $self.jqGrid("getCol", "od_qty", false, "sum");

				var dodome_margin = (od_sum_dodome_price-od_sum_supply_price)/od_sum_dodome_price;
				var drug_margin = (od_total_drug_price-od_sum_dodome_price)/od_total_drug_price;
				var sale_margin = (od_total_sale_price-od_total_drug_price)/od_total_sale_price;

				console.log(od_sum_supply_price);
				$self.jqGrid("footerData","set",{od_date:"Total:",od_sum_supply_price:od_sum_supply_price, od_sum_dodome_price: od_sum_dodome_price,od_total_drug_price:od_total_drug_price,od_total_sale_price:od_total_sale_price,od_send_cost:od_send_cost,odr_total_2:odr_total_2,od_qty:od_qty,dodome_margin:dodome_margin.toFixed(2)+"%",drug_margin:drug_margin.toFixed(2)+"%",sale_margin:sale_margin.toFixed(2)+"%"});
			}
		});
	});

	function formSubmit(type) {
		if(type==1) {
			$('#mode').val('read');
			jQuery(grid_selector).trigger("reloadGrid");
		} else {


			var form = "<form action='calculate1_01.php' method='post'>"; 
			form += "<input type='hidden' name='mode' value='excel' />"; 
			form += "<input type='hidden' name='od_date' value='"+$('#od_date').val()+"' />"; 
			form += "<input type='hidden' name='fr_date' value='"+$('#fr_date').val()+"' />"; 
			form += "<input type='hidden' name='to_date' value='"+$('#to_date').val()+"' />"; 

			form += "<input type='hidden' name='od_status' value='"+$('#od_status option:selected').val()+"' />"; 
			form += "<input type='hidden' name='od_pay_yn' value='"+$('#od_pay_yn option:selected').val()+"' />"; 
			form += "<input type='hidden' name='od_gubun' value='"+$('#od_gubun option:selected').val()+"' />"; 
			form += "<input type='hidden' name='od_settle_case' value='"+$('#od_settle_case option:selected').val()+"' />"; 
			form += "<input type='hidden' name='comp_code' value='"+$('#comp_code option:selected').val()+"' />"; 
			form += "<input type='hidden' name='pharm_manager' value='"+$('#pharm_manager option:selected').val()+"' />"; 


			form += "</form>"; 
			jQuery(form).appendTo("body").submit().remove(); 


		}
	}
	function fnSearch(pdata) {
		$("#preloader").show();
		var jqGridParam = '';
		for (var i in pdata) {
			jqGridParam += '&'+i+'='+pdata[i];
		}

		$.ajax({
			url:'calculate1_01.php',
			type:'POST',
			data:$('#forderlist').serialize()+jqGridParam+"&page_gubun=1",
			success: function(data) {

				var thegrid = jQuery(grid_selector)[0];
				thegrid.addJSONData(eval("("+data+")"));
			},

			complete: function(jsondata,stat) {
				$("#preloader").fadeOut("fast");
				//var thegrid = jQuery(grid_selector)[0];
				//updatePagerIcons(thegrid);			
			}
		});
	}
	function updatePagerIcons(table) {
		var replacement = 
		{
			'ui-icon-seek-first' : 'fa fa-angle-double-left bigger-140',
			'ui-icon-seek-prev' : 'fa fa-angle-left bigger-140',
			'ui-icon-seek-next' : 'fa fa-angle-right bigger-140',
			'ui-icon-seek-end' : 'fa fa-angle-double-right bigger-140'
		};
		$('.ui-pg-table:not(.navtable) > tbody > tr > .ui-pg-button > .ui-icon').each(function(){
			var icon = $(this);
			var $class = $.trim(icon.attr('class').replace('ui-icon', ''));
			
			if($class in replacement) icon.attr('class', 'ui-icon '+replacement[$class]);
		})
	}

</script>


<div id="Excel_Select" title="엑셀 출력 선택">
	<form name="ExcelFrm" id="ExcelFrm" action="calculate1_01.php?<?php echo $qstr; ?>" method="post" target="iFrm">
		<table class="table_style1">
			<tr>
				<td width="150"></td>
				<td><input type="radio" name="ptype" value="1" id="ExcelParamSelect1"><label for="ExcelParamSelect1">전체</label>
				</td>
			</tr>
			
		</table>
	</form>
</div>
<script>
	$("#Excel_Select").dialog({
		autoOpen: false,
		resizable: false,
		modal: true,
		width: 800,
		buttons: {
			"엑셀다운로드": function () {
				$("#ExcelFrm").submit();
				$(this).dialog("close");
			},
			Cancel: function () {
				$(this).dialog("close");
			}
		}
	});

	$("#ExcelDownButton").click(function () {
		$("#Excel_Select").dialog("open");
	});

</script>
<!-- (s)hidden Frame -->
<iframe name="iFrm" style="display: none;" ></iframe>

<!--업체선택 검색기능-->

<link href="../select2.css" rel="stylesheet"/>
<script type="text/javascript" src="../select2.js"></script>
<script>
$(document).ready(function () {
	$("#comp_code").select2();
});
</script>

<?php
include_once (G5_ADMIN_PATH.'/admin.tail.php');
?>

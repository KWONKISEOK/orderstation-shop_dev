<?php
$sub_menu = '500110';
include_once('./_common.php');

auth_check($auth[$sub_menu], "r");

$g5['title'] = '정산관리';
include_once(G5_ADMIN_PATH.'/admin.head.php');
include_once(G5_PLUGIN_PATH.'/jquery-ui/datepicker.php');

if(! preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $fr_date) ) $fr_date = '';
if(! preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $to_date) ) $to_date = '';
if($fr_date =='') $fr_date = date("Y-m-d", strtotime('-1 month'));
if($to_date =='') $to_date = date("Y-m-d");
?>

<link rel="stylesheet" type="text/css" media="screen" href="/adm/jqGrid/css/ui.jqgrid.css" />  


<form name="forderlist" id="forderlist" method="post" >
<input type="hidden" id="mode" name="mode"  value="read" />
<input type="hidden" id="od_date" name="od_date"  value="3" />
<input type="hidden" id="comp_code" name="comp_code"  value="<?php echo $member['comp_code'];?>" />

<div class="local_sch03  tbl_wrap" style="padding:10px;">
    <table>
   
    <tbody>
    <tr>
        <th scope="row">
			정산일자
		</th>
        <td colspan=5>
            <input type="text" id="fr_date"  name="fr_date" value="<?php echo $fr_date; ?>" class="frm_input" size="10" maxlength="10"> ~
			<input type="text" id="to_date"  name="to_date" value="<?php echo $to_date; ?>" class="frm_input" size="10" maxlength="10">
			<button type="button" onclick="javascript:set_date('오늘');">오늘</button>
			<button type="button" onclick="javascript:set_date('15일');">15일</button>
			<button type="button" onclick="javascript:set_date('1개월');">1개월</button>
			<button type="button" onclick="javascript:set_date('2개월');">2개월</button>
			<button type="button" onclick="javascript:set_date('12개월');">3개월</button>
        </td>
		<td rowspan="2">
			 <input type="button" value="검색" class="get_theme_confc btn btn_01" style="cursor:pointer" onclick="formSubmit(1);">    
			 <input type="button" value="엑셀 다운로드" class="get_theme_confc btn btn_02" style="cursor:pointer" onclick="formSubmit(2);">    
		</td>
		
    </tr>
	<tr>

		<th scope="row">상품명검색</th>
        <td>
            <label for="search" class="sound_only">검색어<strong class="sound_only"> 필수</strong></label>
			<input type="text" name="search" value="<?php echo $search; ?>" id="search" class=" frm_input" autocomplete="off">
        </td>
		<th scope="row">주문구분</th>
        <td>
           <select name="od_gubun" id="od_gubun" >
				<option value="">==전체==</option>
				<option value="1" <?php echo get_selected($od_gubun, '1'); ?>>고객주문</option>
				<option value="2" <?php echo get_selected($od_gubun, '2'); ?>>약국주문</option>
				<option value="3" <?php echo get_selected($od_gubun, '3'); ?>>일반주문</option>
			</select>
        </td>
    </tr>
    </tbody>
    </table>
</div>

<div style="margin-bottom:10px;font-weight:bold;">배송일기준정산 총합계금액: <strong><span id="NewSumPrice"></span></strong></div>

<div class=" tbl_wrap1">
<div id="preloader">
	 <img src="/adm/jqGrid/waiting.gif" align="absmiddle"> Loading...		
</div>
<table id="grid-table"></table>
<div id="grid-pager" ></div>


</div>   
</form>
<?php

?>

<?php //$qstr ='comp_code='.$_GET['comp_code'].'&'.$qstr;
echo get_paging(G5_IS_MOBILE ? $config['cf_mobile_pages'] : $config['cf_write_pages'], $page, $total_page, "{$_SERVER['SCRIPT_NAME']}?$qstr&amp;page="); ?>

<script>


$(function(){
    $("#fr_date, #to_date").datepicker({ changeMonth: true, changeYear: true, dateFormat: "yy-mm-dd", showButtonPanel: true, yearRange: "c-99:c+99", maxDate: "+0d" });

   
});

function set_date(today)
{
    <?php
    $date_term = date('w', G5_SERVER_TIME);
    $week_term = $date_term + 7;
    $last_term = strtotime(date('Y-m-d', G5_SERVER_TIME));
    ?>
    if (today == "오늘") {
        document.getElementById("fr_date").value = "<?php echo G5_TIME_YMD; ?>";
        document.getElementById("to_date").value = "<?php echo G5_TIME_YMD; ?>";
    } else if (today == "15일") {
        document.getElementById("fr_date").value = "<?php echo date('Y-m-d', strtotime('-13 Day', $last_term)); ?>";
        document.getElementById("to_date").value = "<?php echo date('Y-m-d', G5_SERVER_TIME); ?>";
    } else if (today == "1개월") {
        document.getElementById("fr_date").value = "<?php echo date('Y-m-d', strtotime('-1 Month', $last_term)); ?>";
        document.getElementById("to_date").value = "<?php echo date('Y-m-d', G5_SERVER_TIME); ?>";
    } else if (today == "2개월") {
       document.getElementById("fr_date").value = "<?php echo date('Y-m-d', strtotime('-2 Month', $last_term)); ?>";
        document.getElementById("to_date").value = "<?php echo date('Y-m-d', G5_SERVER_TIME); ?>";
	} else if (today == "3개월") {
        document.getElementById("fr_date").value = "<?php echo date('Y-m-d', strtotime('-3 Month', $last_term)); ?>";
        document.getElementById("to_date").value = "<?php echo date('Y-m-d', G5_SERVER_TIME); ?>";
    } 
}
</script>

<script src="/adm/jqGrid/js/i18n/grid.locale-ko.js"   type="text/javascript"></script>  
<script src="/adm/jqGrid/js/jquery.jqGrid.min.js"    type="text/javascript"></script> 
<script type="text/javascript"> 
	var grid_selector = "#grid-table";
	var pager_selector = "#grid-pager";

	$(document).ready(function () {
		var parent_column = $(grid_selector).closest('[class*="col-"]');
		//resize to fit page size
		$(window).on('resize.jqGrid', function () {
			$(grid_selector).jqGrid( 'setGridWidth', parent_column.width() );
		})
		
		jQuery(grid_selector).jqGrid({
			datatype :  fnSearch,
			colNames: ['주문일자','배송일자','주문번호','주문자','수령인','카테고리1','카테고리2','상품명','옵션명','과세','수량','공급가','공급가합계','소비자가','택배비','소비자가합계','주문상태'],
		colModel: [
				   { name: 'od_date',      index: 'od_date',      width: 55, align: "center", sorttype: "data" },
				   { name: 'od_date3',      index: 'od_date3',      width: 55, align: "center", sorttype: "text" },
				   { name: 'od_id',      index: 'od_id',      width: 75, align: "center", sorttype: "text" },
				   { name: 'od_name',      index: 'od_name',      width: 35, align: "center", sorttype: "text" },
				   { name: 'od_b_name',      index: 'od_b_name',      width: 35, align: "center", sorttype: "text" },    
				   { name: 'ca_name1',      index: 'ca_name1',      width: 80, align: "center", sorttype: "text" },    
				   { name: 'ca_name2',      index: 'ca_name2',      width: 80, align: "center", sorttype: "text" },    
				   { name: 'it_name',      index: 'it_name',   width: 150,    align: "left", sorttype: "text" },   
				   { name: 'od_option',      index: 'od_option',    width: 150,   align: "left", sorttype: "text" },   
				   { name: 'it_notax',      index: 'gds_tax',      width: 30, align: "center", sorttype: "text" },    
				   { name: 'od_qty',      index: 'odr_goods_cnt',      width: 25, align: "right", sorttype: "int" },
				   { name: 'od_sum_supply_price',      index: 'od_sum_supply_price',      width: 40, align: "right", sorttype: "int" },
				   { name: 'od_sum_supply_price_qty',      index: 'od_sum_supply_price_qty',      width: 60, align: "right", sorttype: "int" },
				   { name: 'od_total_sale_price',      index: 'od_total_sale_price',      width: 40, align: "right", sorttype: "int" },      				   
				   { name: 'od_send_cost',      index: 'od_send_cost',      width: 30, align: "right", sorttype: "int" },      				   
				   { name: 'odr_total_2',      index: 'odr_total_2',      width: 60, align: "right", sorttype: "int" },  
				   { name: 'od_status',      index: 'od_status',      width: 40, align: "center", sorttype: "text" },
				  ],
			viewrecords : true,
			rowNum:100,
			rowList:[100,100, 1000],
			pager : pager_selector,
			altRows: true,
			sortname: 'a.od_id',
			sortorder: "desc",
			height:510,
			autowidth: true,
            shrinkToFit: true,
			footerrow: true,
			userDataOnFooter : true,
			gridComplete: function () {

				var $self = $(this);
				var od_sum_supply_price = $self.jqGrid("getCol", "od_sum_supply_price", false, "sum");
				
				var od_total_sale_price = $self.jqGrid("getCol", "od_total_sale_price", false, "sum");
				var od_send_cost = $self.jqGrid("getCol", "od_send_cost", false, "sum");
				var odr_total_2 = $self.jqGrid("getCol", "odr_total_2", false, "sum");
				var od_qty = $self.jqGrid("getCol", "od_qty", false, "sum");
				var od_sum_supply_price_qty = $self.jqGrid("getCol", "od_sum_supply_price_qty", false, "sum");

				$(".ui-paging-info").prev('span.TmpFoot').remove().end();
				$(".ui-paging-info").before("<span class='TmpFoot' style='margin-right:50px;'>해당기간 정산금액 : "+(od_sum_supply_price_qty+od_send_cost)+"원(vat포함)</span>");

				$self.jqGrid("footerData","set",
					{od_date:"Total:",od_sum_supply_price:od_sum_supply_price, od_sum_supply_price_qty:od_sum_supply_price_qty , od_total_sale_price:od_total_sale_price,od_send_cost:od_send_cost,odr_total_2:odr_total_2,od_qty:od_qty}
				);
			}
		});
	});

	function formSubmit(type) {
		if(type==1) {
			$('#mode').val('read');
			jQuery(grid_selector).trigger("reloadGrid");
		} else {
			var form = "<form action='vcalculate1_01.php' method='post'>"; 
			form += "<input type='hidden' name='mode' value='excel2' />"; 
			form += "<input type='hidden' name='od_date' value='"+$('#od_date').val()+"' />"; 
			form += "<input type='hidden' name='fr_date' value='"+$('#fr_date').val()+"' />"; 
			form += "<input type='hidden' name='to_date' value='"+$('#to_date').val()+"' />"; 
			form += "<input type='hidden' name='od_gubun' value='"+$('#od_gubun option:selected').val()+"' />"; 
			form += "<input type='hidden' name='comp_code' value='"+$('#comp_code').val()+"' />"; 
			form += "<input type='hidden' name='search' value='"+$('#search').val()+"' />"; 

			form += "</form>"; 
			jQuery(form).appendTo("body").submit().remove();
		}
	}
	function fnSearch(pdata) {
		$("#preloader").show();
		var jqGridParam = '';
		for (var i in pdata) {
			jqGridParam += '&'+i+'='+pdata[i];
		}

		$.ajax({
			url:'vcalculate1_01.php',
			type:'POST',
			data:$('#forderlist').serialize()+jqGridParam,
			success: function(data) {

				var obj = JSON.parse(data);
				//console.log(obj.NewSumPrice);

				//공급가 * 수량 + 배송비
				$('#NewSumPrice').html( obj.NewSumPrice );

				var thegrid = jQuery(grid_selector)[0];
				thegrid.addJSONData(eval("("+data+")"));
			},

			complete: function(jsondata,stat) {
				$("#preloader").fadeOut("fast");
				//var thegrid = jQuery(grid_selector)[0];
				//updatePagerIcons(thegrid);			
			}
		});
	}
	function updatePagerIcons(table) {
		var replacement = 
		{
			'ui-icon-seek-first' : 'fa fa-angle-double-left bigger-140',
			'ui-icon-seek-prev' : 'fa fa-angle-left bigger-140',
			'ui-icon-seek-next' : 'fa fa-angle-right bigger-140',
			'ui-icon-seek-end' : 'fa fa-angle-double-right bigger-140'
		};
		$('.ui-pg-table:not(.navtable) > tbody > tr > .ui-pg-button > .ui-icon').each(function(){
			var icon = $(this);
			var $class = $.trim(icon.attr('class').replace('ui-icon', ''));
			
			if($class in replacement) icon.attr('class', 'ui-icon '+replacement[$class]);
		})
	}

</script>


<?php
include_once (G5_ADMIN_PATH.'/admin.tail.php');
?>

<?php
include_once('./_common.php');

$mode	= $_POST['mode'];


$fr_date = $fr_year . $fr_month;
$to_date = $to_year . $to_month;

//조회조건
//날자구분
switch($od_date) {
	case '1':
		$where[] = " DATE_FORMAT(c.od_date1, '%Y%m')  between '$fr_date' and '$to_date' ";
		break;
	case '2':
		$where[] = " DATE_FORMAT(c.od_date2, '%Y%m')  between '$fr_date' and '$to_date' ";
		break;
	case '3':
		$where[] = " DATE_FORMAT(c.od_date3, '%Y%m')  between '$fr_date' and '$to_date' ";
		break;
	case '4':
		$where[] = " DATE_FORMAT(c.od_date4, '%Y%m')  between '$fr_date' and '$to_date' ";
		break;
	default:
		$where[] = " DATE_FORMAT(c.od_date1, '%Y%m')  between '$fr_date' and '$to_date' ";
		break;
}

//print_r( $where);
//상태
if ($od_status) {
	$where[] = " c.od_status = '$od_status' ";
}
/*
//결제구분
if ($od_settle_case) {
	 switch($od_settle_case) {
		case '빌링결제':
			$where[] = " a.od_billkey != '' ";
			break;
		default:
			$where[] = " a.od_settle_case = '$od_settle_case' ";
			break;
	}
}
*/

//입금여부
if ($od_pay_yn) {
	$where[] = " c.od_pay_yn = '$od_pay_yn' ";
}
//공급사코드
if ($comp_code) {
	$where[] = " b.comp_code = '$comp_code' ";
}
//소속구분
//if ($manager_dept) {
//	$where[] = " left(a.pharm_manager_dept, 6) = '$manager_dept' ";
//}

//사원구분
if ($pharm_manager) {
	$where[] = " a.pharm_manager = '$pharm_manager' ";
}

if ($where) {
	$sql_search = implode(' and ', $where);
}

$sql_search .= " and instr( c.od_status , '반품' ) <= 0 and not ( a.od_settle_case = '가상계좌' and c.od_pay_yn = 'N' ) and b.it_id not in('S00603','S00604')	";

switch($mode){
	case 'read':
		
		$sql = " select
					 od_date1, ifnull(sum(sosok_price1_gubun1),0) as sosok_price1_gubun1 
					 , max(aa.pharm_manager_dept) as pharm_manager_dept
				   , ifnull(sum(sosok_price2_gubun1),0) as sosok_price2_gubun1
				   , ifnull(sum(sosok_price3_gubun1),0) as sosok_price3_gubun1
				   , ifnull(sum(sosok_price4_gubun1),0) as sosok_price4_gubun1
				   , ifnull(sum(sosok_price1_gubun2),0) as sosok_price1_gubun2
				   , ifnull(sum(sosok_price2_gubun2),0) as sosok_price2_gubun2
				   , ifnull(sum(sosok_price3_gubun2),0) as sosok_price3_gubun2
				   , ifnull(sum(sosok_price4_gubun2),0) as sosok_price4_gubun2
				   , ifnull(sum(sosok_price1_gubun3),0) as sosok_price1_gubun3
				   , ifnull(sum(sosok_price2_gubun3),0) as sosok_price2_gubun3
				   , ifnull(sum(sosok_price3_gubun3),0) as sosok_price3_gubun3
				   , ifnull(sum(sosok_price4_gubun3),0) as sosok_price4_gubun3
				from (
				select 
					   case when left(a.pharm_manager_dept, 6) = '002001' and od_gubun = '2' then od_sum_drug_price * c.od_qty end as sosok_price1_gubun1
					 , case when left(a.pharm_manager_dept, 6) = '002002' and od_gubun = '2' then od_sum_drug_price * c.od_qty end as sosok_price2_gubun1
					 , case when left(a.pharm_manager_dept, 6) = '002003' and od_gubun = '2' then od_sum_drug_price * c.od_qty end as sosok_price3_gubun1
					 , case when left(a.pharm_manager_dept, 6) not in('002001','002002','002003') and od_gubun = '2' then od_sum_drug_price * c.od_qty end as sosok_price4_gubun1
					 , case when left(a.pharm_manager_dept, 6) = '002001' and od_gubun = '1' then od_sum_drug_price * c.od_qty end as sosok_price1_gubun2
					 , case when left(a.pharm_manager_dept, 6) = '002002' and od_gubun = '1' then od_sum_drug_price * c.od_qty end as sosok_price2_gubun2
					 , case when left(a.pharm_manager_dept, 6) = '002003' and od_gubun = '1' then od_sum_drug_price * c.od_qty end as sosok_price3_gubun2
					 , case when left(a.pharm_manager_dept, 6) not in('002001','002002','002003') and od_gubun = '1' then od_sum_drug_price * c.od_qty end as sosok_price4_gubun2
					 , case when left(a.pharm_manager_dept, 6) = '002001' and od_gubun = '3' then od_sum_drug_price * c.od_qty end as sosok_price1_gubun3
					 , case when left(a.pharm_manager_dept, 6) = '002002' and od_gubun = '3' then od_sum_drug_price * c.od_qty end as sosok_price2_gubun3
					 , case when left(a.pharm_manager_dept, 6) = '002003' and od_gubun = '3' then od_sum_drug_price * c.od_qty end as sosok_price3_gubun3
					 , case when left(a.pharm_manager_dept, 6) not in('002001','002002','002003') and od_gubun = '3' then od_sum_drug_price * c.od_qty end as sosok_price4_gubun3
					 , a.pharm_manager_dept
					 , d.comp_name
					 , left(c.od_date1, 7) as od_date1
					 , left(a.pharm_manager_dept, 6) as od_dept
					 , a.od_gubun
				   from tbl_shop_order a, tbl_shop_order_detail b, tbl_shop_order_receiver c , tbl_member d
				   where a.od_id = b.od_id and a.od_id = c.od_id and b.od_num = c.od_num and b.comp_code = d.comp_code
					   and c.od_status != '취소' and ".$sql_search ."
				) as aa
				group by od_date1
				order by od_date1 desc";

		$res = sql_query($sql);
		if(!$res) exit('Cannot run query.');

		$response->page = 1;
		$response->total = 1;
		$response->records = 1000;
		$i = 0;
		while($row = mysqli_fetch_array($res,MYSQL_ASSOC)) {

			$date1 = $row["od_date1"];

			$sosok_price3_gubun1 = $row["sosok_price3_gubun1"];
			$sosok_price3_gubun2 = $row["sosok_price3_gubun2"];
			$sosok_price3_gubun3 = $row["sosok_price3_gubun3"];


			$sosok_price1_gubun1 = $row["sosok_price1_gubun1"];
			$sosok_price1_gubun2 = $row["sosok_price1_gubun2"];
			$sosok_price1_gubun3 = $row["sosok_price1_gubun3"];


			$sosok_price2_gubun1 = $row["sosok_price2_gubun1"];
			$sosok_price2_gubun2 = $row["sosok_price2_gubun2"];
			$sosok_price2_gubun3 = $row["sosok_price2_gubun3"];
			

			$sosok_price4_gubun1 = $row["sosok_price4_gubun1"];
			$sosok_price4_gubun2 = $row["sosok_price4_gubun2"];
			$sosok_price4_gubun3 = $row["sosok_price4_gubun3"];
			$dateweek            = $row["dateweek"];
			$date_cnt            = $row["date_cnt"];

			if ($manager_dept == '002003' or $manager_dept =='') {
				$response->rows[$i]['od_date']		= $date1;				
				$response->rows[$i]['dept_name']	= '오엔케이';
				$response->rows[$i]['price1']		= $sosok_price3_gubun1;
				$response->rows[$i]['price2']		= $sosok_price3_gubun2;
				$response->rows[$i]['price3']		= $sosok_price3_gubun3;
				$response->rows[$i]['price4']		= $sosok_price3_gubun1+$sosok_price3_gubun2+$sosok_price3_gubun3;
				
				$i++;
			}
			if ($manager_dept == '002001' or $manager_dept =='') {
				$response->rows[$i]['od_date']		= $date1;				
				$response->rows[$i]['dept_name']	= '태전약품';
				$response->rows[$i]['price1']		= $sosok_price1_gubun1;
				$response->rows[$i]['price2']		= $sosok_price1_gubun2;
				$response->rows[$i]['price3']		= $sosok_price1_gubun3;
				$response->rows[$i]['price4']		= $sosok_price1_gubun1+$sosok_price1_gubun2+$sosok_price1_gubun3;
				$i++;
			}
			if ($manager_dept == '002002' or $manager_dept =='') {
				$response->rows[$i]['od_date']		= $date1;				
				$response->rows[$i]['dept_name']	= '티제이팜';
				$response->rows[$i]['price1']		= $sosok_price2_gubun1;
				$response->rows[$i]['price2']		= $sosok_price2_gubun2;
				$response->rows[$i]['price3']		= $sosok_price2_gubun3;
				$response->rows[$i]['price4']		= $sosok_price2_gubun1+$sosok_price2_gubun2+$sosok_price2_gubun3;
				$i++;
			}
			if ($manager_dept == ' ' or $manager_dept =='') {
				$response->rows[$i]['od_date']		= $date1;
				
				$response->rows[$i]['dept_name']	= '기타';
				$response->rows[$i]['price1']		= $sosok_price4_gubun1;
				$response->rows[$i]['price2']		= $sosok_price4_gubun2;
				$response->rows[$i]['price3']		= $sosok_price4_gubun3;
				$response->rows[$i]['price4']		= $sosok_price4_gubun1+$sosok_price4_gubun2+$sosok_price4_gubun3;
				$i++;
			}
		} 

		echo json_encode($response);
		//echo $sql;
		break;
	case 'excel':

		$ExcelTitle = urlencode( "월매출_요약" . date( "YmdHis" ) );
		header( "Content-type: application/vnd.ms-excel" );
		header( "Content-type: application/vnd.ms-excel; charset=utf-8" );
		header( "Content-Disposition: attachment; filename = " . $ExcelTitle . ".xls" );
		header( "Content-Description: PHP4 Generated Data" );
		print( "<meta http-equiv=\"Content-Type\" content=\"application/vnd.ms-excel; charset=utf-8\">" );
		echo "<table border=1  >";
		echo "<tr bgcolor='#E7E7E7'>";
		echo "<th>일자</th>";
		echo "<th>소속</th>";
		echo "<th>약국주문</th>";
		echo "<th>고객주문</th>";
		echo "<th>일반주문</th>";
		echo "<th>합계</th>";	
		echo "</tr>";
		
		//$orderby = 'a.od_id desc';

		$sql = " select
					 od_date1, ifnull(sum(sosok_price1_gubun1),0) as sosok_price1_gubun1 
					 , max(aa.pharm_manager_dept) as pharm_manager_dept
				   , ifnull(sum(sosok_price2_gubun1),0) as sosok_price2_gubun1
				   , ifnull(sum(sosok_price3_gubun1),0) as sosok_price3_gubun1
				   , ifnull(sum(sosok_price4_gubun1),0) as sosok_price4_gubun1
				   , ifnull(sum(sosok_price1_gubun2),0) as sosok_price1_gubun2
				   , ifnull(sum(sosok_price2_gubun2),0) as sosok_price2_gubun2
				   , ifnull(sum(sosok_price3_gubun2),0) as sosok_price3_gubun2
				   , ifnull(sum(sosok_price4_gubun2),0) as sosok_price4_gubun2
				   , ifnull(sum(sosok_price1_gubun3),0) as sosok_price1_gubun3
				   , ifnull(sum(sosok_price2_gubun3),0) as sosok_price2_gubun3
				   , ifnull(sum(sosok_price3_gubun3),0) as sosok_price3_gubun3
				   , ifnull(sum(sosok_price4_gubun3),0) as sosok_price4_gubun3
				   
				from (
				select 
					   case when left(a.pharm_manager_dept, 6) = '002001' and od_gubun = '2' then od_sum_drug_price * c.od_qty end as sosok_price1_gubun1
					 , case when left(a.pharm_manager_dept, 6) = '002002' and od_gubun = '2' then od_sum_drug_price * c.od_qty end as sosok_price2_gubun1
					 , case when left(a.pharm_manager_dept, 6) = '002003' and od_gubun = '2' then od_sum_drug_price * c.od_qty end as sosok_price3_gubun1
					 , case when left(a.pharm_manager_dept, 6) not in('002001','002002','002003') and od_gubun = '2' then od_sum_drug_price * c.od_qty end as sosok_price4_gubun1
					 , case when left(a.pharm_manager_dept, 6) = '002001' and od_gubun = '1' then od_sum_drug_price * c.od_qty end as sosok_price1_gubun2
					 , case when left(a.pharm_manager_dept, 6) = '002002' and od_gubun = '1' then od_sum_drug_price * c.od_qty end as sosok_price2_gubun2
					 , case when left(a.pharm_manager_dept, 6) = '002003' and od_gubun = '1' then od_sum_drug_price * c.od_qty end as sosok_price3_gubun2
					 , case when left(a.pharm_manager_dept, 6) not in('002001','002002','002003') and od_gubun = '1' then od_sum_drug_price * c.od_qty end as sosok_price4_gubun2
					 , case when left(a.pharm_manager_dept, 6) = '002001' and od_gubun = '3' then od_sum_drug_price * c.od_qty end as sosok_price1_gubun3
					 , case when left(a.pharm_manager_dept, 6) = '002002' and od_gubun = '3' then od_sum_drug_price * c.od_qty end as sosok_price2_gubun3
					 , case when left(a.pharm_manager_dept, 6) = '002003' and od_gubun = '3' then od_sum_drug_price * c.od_qty end as sosok_price3_gubun3
					 , case when left(a.pharm_manager_dept, 6) not in('002001','002002','002003') and od_gubun = '3' then od_sum_drug_price * c.od_qty end as sosok_price4_gubun3
					 , a.pharm_manager_dept
					 , d.comp_name
					 , left(c.od_date1, 7) as od_date1
					 , left(a.pharm_manager_dept, 6) as od_dept
					 , a.od_gubun
				   from tbl_shop_order a, tbl_shop_order_detail b, tbl_shop_order_receiver c , tbl_member d
				   where a.od_id = b.od_id and a.od_id = c.od_id and b.od_num = c.od_num and b.comp_code = d.comp_code
					   and c.od_status != '취소' and ".$sql_search ."
				) as aa
				group by od_date1
				order by od_date1 desc";


		$res = sql_query($sql);
		if(!$res) exit('Cannot run query.');

		$i = 0;
		//$sosok_price3_gubun1_tol=0;
	
	
		while($row = mysqli_fetch_array($res,MYSQL_ASSOC)) {

		
			$date1 = $row["od_date1"];
			
			$sosok_price3_gubun1 = $row["sosok_price3_gubun1"];
			$sosok_price3_gubun2 = $row["sosok_price3_gubun2"];
			$sosok_price3_gubun3 = $row["sosok_price3_gubun3"];
			$sosok_price3_gubun1_tot1 = $sosok_price3_gubun1_tot1 + $row["sosok_price3_gubun1"];
			$sosok_price3_gubun2_tot2 = $sosok_price3_gubun2_tot2 + $row["sosok_price3_gubun2"];
			$sosok_price3_gubun3_tot3 = $sosok_price3_gubun3_tot3 + $row["sosok_price3_gubun3"]; 

			$sosok_price1_gubun1 = $row["sosok_price1_gubun1"];
			$sosok_price1_gubun2 = $row["sosok_price1_gubun2"];
			$sosok_price1_gubun3 = $row["sosok_price1_gubun3"];
			$sosok_price1_gubun1_tot1 = $sosok_price1_gubun1_tot1 + $row["sosok_price1_gubun1"];
			$sosok_price1_gubun2_tot2 = $sosok_price1_gubun2_tot2 + $row["sosok_price1_gubun2"];
			$sosok_price1_gubun3_tot3 = $sosok_price1_gubun3_tot3 + $row["sosok_price1_gubun3"];

			$sosok_price2_gubun1 = $row["sosok_price2_gubun1"];
			$sosok_price2_gubun2 = $row["sosok_price2_gubun2"];
			$sosok_price2_gubun3 = $row["sosok_price2_gubun3"];
			$sosok_price2_gubun1_tot1 = $sosok_price2_gubun1_tot1 + $row["sosok_price2_gubun1"];
			$sosok_price2_gubun2_tot2 = $sosok_price2_gubun2_tot2 + $row["sosok_price2_gubun2"];
			$sosok_price2_gubun3_tot3 = $sosok_price2_gubun3_tot3 + $row["sosok_price2_gubun3"];

			$sosok_price4_gubun1 = $row["sosok_price4_gubun1"];
			$sosok_price4_gubun2 = $row["sosok_price4_gubun2"];
			$sosok_price4_gubun3 = $row["sosok_price4_gubun3"];
			$sosok_price4_gubun1_tot1 = $sosok_price4_gubun1_tot1 + $row["sosok_price4_gubun1"];
			$sosok_price4_gubun2_tot2 = $sosok_price4_gubun2_tot2 + $row["sosok_price4_gubun2"];
			$sosok_price4_gubun3_tot3 = $sosok_price4_gubun3_tot3 + $row["sosok_price4_gubun3"];
			//$dateweek            = $row["dateweek"];
			//$date_cnt            = $row["date_cnt"];

			if ($manager_dept == '002003' or $manager_dept =='') {
				echo "<tr>";
				if($manager_dept == '002003') {
					echo "<td style=\"mso-number-format:'\@';\">" . $date1 . " </td>";
				} else {
					echo "<td rowspan=4 style=\"mso-number-format:'\@';\">" . $date1 . " </td>";
				}
				echo "<td >오엔케이 </td>";
				echo "<td style=\"mso-number-format:'\@';\">" . $sosok_price3_gubun1 . " </td>";
				echo "<td style=\"mso-number-format:'\@';\">" . $sosok_price3_gubun2 . " </td>";
				echo "<td style=\"mso-number-format:'\@';\">" . $sosok_price3_gubun3 . " </td>";
				echo "<td style=\"mso-number-format:'\@';\">" . ($sosok_price3_gubun1+$sosok_price3_gubun2+$sosok_price3_gubun3) . " </td>";				
				
				echo "</tr>";		

			}
			if ($manager_dept == '002001' or $manager_dept =='') {
				echo "<tr>";
				if($manager_dept == '002001') {
					echo "<td style=\"mso-number-format:'\@';\">" . $date1 . " </td>";
				} 
				echo "<td >태전약품 </td>";
				echo "<td style=\"mso-number-format:'\@';\">" . $sosok_price1_gubun1 . " </td>";
				echo "<td style=\"mso-number-format:'\@';\">" . $sosok_price1_gubun2 . " </td>";
				echo "<td style=\"mso-number-format:'\@';\">" . $sosok_price1_gubun3 . " </td>";
				echo "<td style=\"mso-number-format:'\@';\">" . ($sosok_price1_gubun1+$sosok_price1_gubun2+$sosok_price1_gubun3) . " </td>";				
				echo "</tr>";
			
			}
			if ($manager_dept == '002002' or $manager_dept =='') {
				echo "<tr>";
				if($manager_dept == '002002') {
					echo "<td style=\"mso-number-format:'\@';\">" . $date1 . " </td>";
				} 
				echo "<td >티제이팜 </td>";
				echo "<td style=\"mso-number-format:'\@';\">" . $sosok_price2_gubun1 . " </td>";
				echo "<td style=\"mso-number-format:'\@';\">" . $sosok_price2_gubun2 . " </td>";
				echo "<td style=\"mso-number-format:'\@';\">" . $sosok_price2_gubun3 . " </td>";
				echo "<td style=\"mso-number-format:'\@';\">" . ($sosok_price2_gubun1+$sosok_price2_gubun2+$sosok_price2_gubun3) . " </td>";				
				echo "</tr>";
		
			}
			if ($manager_dept == ' ' or $manager_dept =='') {
				echo "<tr>";
				if($manager_dept == ' ') {
					echo "<td style=\"mso-number-format:'\@';\">" . $date1 . " </td>";
				} 
				echo "<td >기타 </td>";
				echo "<td style=\"mso-number-format:'\@';\">" . $sosok_price4_gubun1 . " </td>";
				echo "<td style=\"mso-number-format:'\@';\">" . $sosok_price4_gubun2 . " </td>";
				echo "<td style=\"mso-number-format:'\@';\">" . $sosok_price4_gubun3 . " </td>";
				echo "<td style=\"mso-number-format:'\@';\">" . ($sosok_price4_gubun1+$sosok_price4_gubun2+$sosok_price4_gubun3) . " </td>";				
				echo "</tr>";
			
			}
		} 
		echo "<td colspan=2 bgcolor='#E7E7E7' style=\"mso-number-format:'\@';\">" . '총 합계'. " </td>";
		echo "<td style=\"mso-number-format:'\@';\">" . ($sosok_price3_gubun1_tot1+$sosok_price1_gubun1_tot1+$sosok_price2_gubun1_tot1+$sosok_price4_gubun1_tot1) . " </td>";
		echo "<td style=\"mso-number-format:'\@';\">" . ($sosok_price3_gubun2_tot2+$sosok_price1_gubun2_tot2+$sosok_price2_gubun2_tot2+$sosok_price4_gubun2_tot2) . " </td>";
		echo "<td style=\"mso-number-format:'\@';\">" . ($sosok_price3_gubun3_tot3+$sosok_price1_gubun3_tot3+$sosok_price2_gubun3_tot3+$sosok_price4_gubun3_tot3) . " </td>";
		echo "<td style=\"mso-number-format:'\@';\">" . ($$sosok_price3_gubun1_tot1+$sosok_price3_gubun2_tot2+$sosok_price3_gubun3_tot3
		+$sosok_price1_gubun1_tot1+$sosok_price1_gubun2_tot2+$sosok_price1_gubun3_tot3+$sosok_price2_gubun1_tot1+$sosok_price2_gubun2_tot2+$sosok_price2_gubun3_tot3
		+$sosok_price4_gubun1_tot1+$sosok_price4_gubun2_tot2+$sosok_price4_gubun3_tot3
		) . " </td>";
		
		echo "<tr>";
		echo "<td rowspan=4 >합계 </td>";
		echo "<td >오엔케이 </td>";
				echo "<td style=\"mso-number-format:'\@';\">" . $sosok_price3_gubun1_tot1 . " </td>";
				echo "<td style=\"mso-number-format:'\@';\">" . $sosok_price3_gubun2_tot2 . " </td>";
				echo "<td style=\"mso-number-format:'\@';\">" . $sosok_price3_gubun3_tot3 . " </td>";
				echo "<td style=\"mso-number-format:'\@';\">" . ($sosok_price3_gubun1_tot1+$sosok_price3_gubun2_tot2+$sosok_price3_gubun3_tot3) . " </td>";	//수정			
				echo "</tr>";	
		
		echo "<tr>";
		echo "<td>태전약품 </td>";
				echo "<td style=\"mso-number-format:'\@';\">" . $sosok_price1_gubun1_tot1 . " </td>";
				echo "<td style=\"mso-number-format:'\@';\">" . $sosok_price1_gubun2_tot2 . " </td>";
				echo "<td style=\"mso-number-format:'\@';\">" . $sosok_price1_gubun3_tot3 . " </td>";
				echo "<td style=\"mso-number-format:'\@';\">" . ($sosok_price1_gubun1_tot1+$sosok_price1_gubun2_tot2+$sosok_price1_gubun3_tot3) . " </td>";	//수정			
		echo "</tr>";	
		
		echo "<tr>";
		echo "<td>티제이팜 </td>";
				echo "<td style=\"mso-number-format:'\@';\">" . $sosok_price2_gubun1_tot1 . " </td>";
				echo "<td style=\"mso-number-format:'\@';\">" . $sosok_price2_gubun2_tot2 . " </td>";
				echo "<td style=\"mso-number-format:'\@';\">" . $sosok_price2_gubun3_tot3 . " </td>";
				echo "<td style=\"mso-number-format:'\@';\">" . ($sosok_price2_gubun1_tot1+$sosok_price2_gubun2_tot2+$sosok_price2_gubun3_tot3) . " </td>";	//수정			
		echo "</tr>";	
		echo "<tr>";
		echo "<td>기타 </td>";
				echo "<td style=\"mso-number-format:'\@';\">" . $sosok_price4_gubun1_tot1 . " </td>";
				echo "<td style=\"mso-number-format:'\@';\">" . $sosok_price4_gubun2_tot2 . " </td>";
				echo "<td style=\"mso-number-format:'\@';\">" . $sosok_price4_gubun3_tot3 . " </td>";
				echo "<td style=\"mso-number-format:'\@';\">" . ($sosok_price4_gubun1_tot1+$sosok_price4_gubun2_tot2+$sosok_price4_gubun3_tot3) . " </td>";	//수정			
		echo "</tr>";			

		break;
	
	
}
?>
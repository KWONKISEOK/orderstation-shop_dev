<?php
$sub_menu = '200300';
include_once('./_common.php');

check_demo();

auth_check($auth[$sub_menu], 'd');

check_admin_token();

$count = count($_POST['chk']);

if(!$count)
    alert('삭제할 메일목록을 1개이상 선택해 주세요.');

for($i=0; $i<$count; $i++) {
    $ma_id = $_POST['chk'][$i];
    $al_data_old = sql_fetch("select * from {$g5['mail_table']} where ma_id = '{$ma_id}'");
    $sql = " delete from {$g5['mail_table']} where ma_id = '$ma_id' ";
    sql_query($sql);
    
    /************* 관리자 로그 처리 START *************/
    insert_admin_log(200,200300, '메일 삭제', '', $ma_id, '', $_SERVER['REQUEST_URI'], $al_data, $al_data_old);
}

goto_url('./mail_list.php');
?>
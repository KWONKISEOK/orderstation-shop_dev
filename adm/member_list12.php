<?php
$sub_menu = "200102";
include_once('./_common.php');

auth_check($auth[$sub_menu], 'r');

$g5['title'] = '개인소득지급관리';
include_once('./admin.head.php');
include_once(G5_PLUGIN_PATH.'/jquery-ui/datepicker.php');

$now = date( "Ymd" );
if ( $list_year == "" ) {
	$list_year = year( $now );
}
if ( $list_month == "" ) {
	$list_month = month($now);
}


$list_startdate = $list_year . "-" . $list_month . "-" . "01";
$list_enddate = $list_year . "-" . $list_month . "-" . date( 't', strtotime( $list_startdate ) );

$sql  = "select count(aa.pharm_custno) cnt , sum(incen)  incen  ";
$sql .= "from ( ";
$sql .= "  select ";
$sql .= "      pharm_custno, od_calculate_yn, sum(case when od_period_yn = 'Y' then od_total_incen / od_period_cnt ELSE od_total_incen end) incen ";
$sql .= "  from tbl_shop_order a ";
$sql .= "  inner join tbl_shop_order_detail b ";
$sql .= "      on a.od_id = b.od_id ";
$sql .= "  inner join tbl_shop_order_receiver c ";
$sql .= " 	    on a.od_id = c.od_id and b.od_num = c.od_num ";
$sql .= "  where ";
$sql .= "      c.od_pay_yn = 'Y' and a.od_gubun = '3' and date_format(c.od_date1, '%Y%m') = '" . $list_year . addstring( $list_month ) . "' and c.od_status != '취소'  and a.pharm_custno != '' and b.it_id not in('S00603','S00604')  ";
$sql .= "  group by pharm_custno, od_calculate_yn ";
$sql .= ") aa ";
$sql .= "inner join tbl_member bb ";
$sql .= "  on aa.pharm_custno = bb.pharm_custno and mb_type = '1' ";
if($sfl=='pharm_name') {
	$sql .= "  and pharm_name like '%$stx%' ";
}

$row = sql_fetch($sql);
$total_count = $row['cnt'];
$total_incen = $row['incen'];

$rows = $config['cf_page_rows'];
$total_page  = ceil($total_count / $rows);  // 전체 페이지 계산
if ($page < 1) $page = 1; // 페이지가 없으면 첫 페이지 (1 페이지)
$from_record = ($page - 1) * $rows; // 시작 열을 구함


$qstr1 = "list_year=$list_year&amp;list_month=$list_month&amp;sel_field=$sel_field&amp;search=$search&amp;save_search=$search";
$qstr = "$qstr1&amp;sort1=$sort1&amp;sort2=$sort2&amp;page=$page";

$listall = '<a href="'.$_SERVER['SCRIPT_NAME'].'" class="ov_listall">전체목록</a>';

/*

$sql  = "select sum(incen)  incen";
$sql .= " from ( ";
$sql .= "  select ";
$sql .= "      pharm_custno, od_calculate_yn, sum(od_total_incen) incen ";
$sql .= "  from tbl_shop_order a ";
$sql .= "  inner join tbl_shop_order_detail b ";
$sql .= "      on a.od_id = b.od_id ";
$sql .= "  inner join tbl_shop_order_receiver c ";
$sql .= " 	    on a.od_id = c.od_id and b.od_num = c.od_num ";
$sql .= "  where ";
$sql .= "      c.od_pay_yn = 'Y' and a.od_gubun = '3' and date_format(c.od_date1, '%Y%m') = '" . $list_year . addstring( $list_month ) . "' and c.od_status != '취소'  and a.pharm_custno != ''";
$sql .= "  group by pharm_custno, od_calculate_yn ";
$sql .= ") aa ";
$sql .= "inner join tbl_member bb ";
$sql .= "  on aa.pharm_custno = bb.pharm_custno and mb_type = '1' ";
if($sfl=='pharm_name') {
	$sql .= "  and pharm_name like '%$stx%' ";
}

$row2 = sql_fetch($sql);
$total_incen = $row2['incen'];
*/

$colspan = 10;
?>

<div class="local_ov01 local_ov">
    <?php echo $listall ?>
    <span class="btn_ov01"><span class="ov_txt">총 건수 </span><span class="ov_num"> <?php echo number_format($total_count) ?> 건 </span></span>
	<span class="btn_ov01"><span class="ov_txt">총 금액 </span><span class="ov_num"> <?php echo number_format($total_incen) ?> 원 </span></span>
   
</div>

<form id="fsearch" name="fsearch" class="local_sch01 local_sch" method="get">


<div class="local_sch03  tbl_wrap" style="padding:10px;">
    <table>
   
    <tbody>
	
	<tr>
        <th scope="row">결제구분</th>
        <td>
			<select name="list_year" id="list_year">
				<?php
				for ( $x = 2015; $x <= year( $now ) + 1; $x ++ ) {
					?>
					<option value="<?php echo $x; ?>"<?php if (  $list_year == $x ) {
						echo "selected";
					} ?>><?php echo $x; ?>년
					</option>
					<?php
				}
				?>
			</select>
			<select name="list_month" id="list_month">
				<?php
				for ( $x = 1; $x <= 12; $x ++ ) {
					if($x < 10) { $xstr = '0'.$x;} else {$xstr = $x;}
					?>
					<option value="<?php echo $xstr; ?>"<?php if (  $list_month  == $x ) {
						echo "selected";
					} ?>><?php echo $x; ?>월
					</option>
					<?php
				}
				?>
			</select>			
        </td>
		<th scope="row">검색</th>
        <td>
            <select name="sfl" id="sfl" >
			<option value="pharm_name" <?php echo get_selected($sfl, 'pharm_name'); ?>>약국명</option>
			
			</select>
			<label for="stx" class="sound_only">검색어</label>
			<input type="text" name="stx" value="<?php echo $stx ?>" id="stx" class=" frm_input">
        </td>	
		<td align="right" >
			 <input type="submit" value="검색" class="get_theme_confc btn btn_01">
			 <input type="button" value="엑셀 다운로드" class="get_theme_confc btn btn_02" style="cursor:pointer" onclick="formSubmit(2);">
			 <input type="button" value="수동업로드용 엑셀 다운로드" class="get_theme_confc btn btn_02" style="cursor:pointer" onclick="formSubmit(3);">    

		</td>
    </tr>
	
    </tbody>
    </table>
</div>



</form>

<form name="fmemberlist" id="fmemberlist" action="" onsubmit="return fmemberlist_submit(this);" method="post">
<input type="hidden" name="sst" value="<?php echo $sst ?>">
<input type="hidden" name="sod" value="<?php echo $sod ?>">
<input type="hidden" name="sfl" value="<?php echo $sfl ?>">
<input type="hidden" name="stx" value="<?php echo $stx ?>">
<input type="hidden" name="yearmonth"  id="yearmonth" value="">
<input type="hidden" name="list_year" value="<?php echo $list_year ?>">
<input type="hidden" name="list_month" value="<?php echo $list_month ?>">
<input type="hidden" name="sel_field" value="<?php echo $sel_field ?>">
<input type="hidden" name="search" value="<?php echo $search ?>">
<input type="hidden" name="save_search" value="<?php echo $search ?>">

<input type="hidden" name="page" value="<?php echo $page ?>">
<input type="hidden" name="token" value="">

<div class="tbl_head01 tbl_wrap">
    <table>
    <caption><?php echo $g5['title']; ?> 목록</caption>
    <thead>
    <tr>
        <th scope="col" id="mb_list_chk" rowspan="2" >
            <label for="chkall" class="sound_only">회원 전체</label>
            <input type="checkbox" name="chkall" value="1" id="chkall" onclick="check_all(this.form)">
        </th>
        <th scope="col" id="mb_list_id" >약국명</th>
		<th scope="col" id="mb_list_name">약사님</th>
        <th scope="col" id="mb_list_nick">은행명</th>
		<th scope="col" id="mb_list_auth">계좌번호</th>
		<th scope="col" id="mb_list_auth">예금주</th>
		<th scope="col" id="mb_list_auth">수수료</th>
		<th scope="col" id="mb_list_auth">개인소득(3%)</th>
		<th scope="col" id="mb_list_auth">주민세(10%)</th>
		<th scope="col" id="mb_list_auth">실지급금</th>
		<th scope="col" id="mb_list_auth">지급여부</th>
		<th scope="col" id="mb_list_auth">동의여부</th>
        
    </tr>
    </thead>
    <tbody>
    <?php
	$sql  = "select aa.pharm_custno, aa.od_calculate_yn, aa.incen ,  ";
	$sql .= "		bb.mb_id , bb.mb_name , bb.mb_tel , bb.pharm_name , bb.pharm_status , bb.pharm_bank_agree ,  ";
	$sql .= "		bb.pharm_bank_agree_date, bb.pharm_bank_user, bb.pharm_bank_num, bb.pharm_bank_code, bb.mb_nick " ;
	$sql .= "from ( ";
	$sql .= "  select ";
	$sql .= "      pharm_custno, od_calculate_yn, sum(case when od_period_yn = 'Y' then od_total_incen / od_period_cnt ELSE od_total_incen end) incen ";
	$sql .= "  from tbl_shop_order a ";
	$sql .= "  inner join tbl_shop_order_detail b ";
	$sql .= "      on a.od_id = b.od_id ";
	$sql .= "  inner join tbl_shop_order_receiver c ";
	$sql .= " 	    on a.od_id = c.od_id and b.od_num = c.od_num ";
	$sql .= "  where ";
	$sql .= "      c.od_pay_yn = 'Y' and a.od_gubun = '3' and date_format(c.od_date1, '%Y%m') = '" . $list_year . addstring( $list_month ) . "' and c.od_status != '취소'  and a.pharm_custno != ''  and b.it_id not in('S00603','S00604') ";
	$sql .= "  group by pharm_custno, od_calculate_yn ";
	$sql .= ") aa ";
	$sql .= "inner join tbl_member bb ";
	$sql .= "  on aa.pharm_custno = bb.pharm_custno and mb_type = '1' ";
	if($sfl=='pharm_name') {
		$sql .= "  and pharm_name like '%$stx%' ";
	}
	$sql .= "order by aa.pharm_custno desc ";
	$sql .= " limit {$from_record}, {$rows} ";

	$result = sql_query($sql);

    for ($i=0; $row=sql_fetch_array($result); $i++) {

       
        $mb_id = $row['mb_id'];
       

        //$bg = 'bg'.($i%2);
		
		$mb_id           = $row["mb_id"];
		//$mb_pass         = $row["mb_pass"];
		$mb_name         = $row["mb_name"];
		$mb_tel          = $row["mb_tel"];
		$pharm_name   = $row["pharm_name"];
		$pharm_custno  = $row["pharm_custno"];

		//$sales_name       = $row["sales_name"];
		$mb_status       = $row["pharm_status"];
	
		$pharm_bank_agree         = $row["pharm_bank_agree"];
		$pharm_bank_agree_date         = $row["pharm_bank_agree_date"];
		$pharm_bank_user         = $row["pharm_bank_user"];
		$pharm_bank_num          = $row["pharm_bank_num"];
		$pharm_bank_code          = $row["pharm_bank_code"];
		$od_calculate_yn        = $row["od_calculate_yn"];
		$incen        = $row["incen"];
		$org_incen           = $incen;

		if ( $pharm_bank_agree == '' ) {
			$mem_bank_agree = "N";
		}

		$pharm_bank_name = ViewBank( $pharm_bank_code );
		if (  $incen > 30000 ) {
			$incen2 = ( $incen * 0.003 ) * 10;
			$incen3 = ( $incen2 * 0.01 ) * 10;
			if ( $incen2 >= 1000 ) {
				$incen =  $incen  - ( $incen2  +  $incen3  );
			} else {
				$incen2 = 0;
				$incen3 = 0;
			}
		} else {
			$incen2 = 0;
			$incen3 = 0;
		}
		$incen = floor($incen);

		$check_disabled = "";
		if ( $od_calculate_yn != 'N' or  $pharm_bank_agree !='Y' ) {
			$check_disabled = "disabled";
		}

		?>
    <tr >
        
        <td headers="mb_list_chk" class="td_chk" >
            <input type="hidden" name="mb_id[<?php echo $i ?>]" value="<?php echo $row['mb_id'] ?>" id="mb_id_<?php echo $i ?>">
            <label for="chk_<?php echo $i; ?>" class="sound_only"><?php echo get_text($row['mb_name']); ?> <?php echo get_text($row['mb_nick']); ?>님</label>
            <input type="checkbox" name="chk[]" value="<?php echo $i ?>" id="chk_<?php echo $i ?>" <?php echo $check_disabled;?>>
        </td>
		<td align="center"><?php echo $pharm_name; ?></td>
		<td align="center"><?php echo $mb_name; ?></td>
		
		<td align="center"><?php echo $pharm_bank_name ; ?></td>
		<td align="center"><?php echo $pharm_bank_num; ?></td>		
		<td align="center"><?php echo $pharm_bank_user; ?></td>
		<td align="right">
			<a href="./shop_admin/orderlist.php?od_date=1&fr_date=<?php echo $list_startdate ; ?>&to_date=<?php echo $list_enddate ; ?>&od_gubun=3&sel_field=pharm_custno&search=<?php echo $pharm_custno; ?>"><?php echo number_format( $org_incen ); ?></a>
		</td>
		<td align="right"><?php echo number_format( floor($incen2 / 10) * 10 ); ?></td>
		<td align="right"><?php echo number_format( floor($incen3 / 10) * 10 ); ?></td>
		<td align="right"><?php echo number_format( $incen ); ?></td>
		<td align="center"><?php echo $od_calculate_yn; ?></td>
		<td align="center"><?php echo $pharm_bank_agree; ?></td>
		
	</tr>

	


    <?php
    }
    if ($i == 0)
        echo "<tr><td colspan=\"".$colspan."\" class=\"empty_table\">자료가 없습니다.</td></tr>";
    ?>

	<tr>
		<td align="left" colspan=12>

			
			<div class="local_cmd01 " style="text-align:left;">
			<input type="text" id="fr_date"  name="fr_date" value="" class="frm_input" size="10" maxlength="10" readonly >
			<input type="button" value="선택지급요청" class="btn_submit" onclick="flist_submit(1);">   
			* 지급요청시 이니시스 영업일로만 선택해주시기 바랍니다.
			</div>

		</td>
	 
	</tr>

    </tbody>
    </table>
</div>


</form>

<?php 
echo get_paging(G5_IS_MOBILE ? $config['cf_mobile_pages'] : $config['cf_write_pages'], $page, $total_page, "{$_SERVER['SCRIPT_NAME']}?$qstr&amp;page="); ?>
<script>

$(function(){
    $("#fr_date").datepicker({ changeMonth: true, changeYear: true, dateFormat: "yy-mm-dd", showButtonPanel: true, yearRange: "c-99:c+99", maxDate: "+10d" });
});


function formSubmit(type) {
	if(type==2) {
		var form = "<form action='member_list121.php' method='post'>"; 
		form += "<input type='hidden' name='mode' value='excel1' />"; 
		form += "<input type='hidden' name='fr_year' value='"+$('#list_year option:selected').val()+"' />"; 
		form += "<input type='hidden' name='fr_month' value='"+$('#list_month option:selected').val()+"' />"; 
		form += "<input type='hidden' name='stx' value='"+$('#stx').val()+"' />"; 
		form += "</form>"; 
		jQuery(form).appendTo("body").submit().remove(); 
	}
	if(type==3) {
		var form = "<form action='member_list121.php' method='post'>"; 
		form += "<input type='hidden' name='mode' value='excel2' />"; 		
		form += "<input type='hidden' name='fr_year' value='"+$('#list_year option:selected').val()+"' />"; 
		form += "<input type='hidden' name='fr_month' value='"+$('#list_month option:selected').val()+"' />"; 
		form += "<input type='hidden' name='stx' value='"+$('#stx').val()+"' />"; 
		form += "</form>"; 
		jQuery(form).appendTo("body").submit().remove(); 

	}
}
/*
function fmemberlist_submit(f)
{
    if (!is_checked("chk[]")) {
        alert(document.pressed+" 하실 항목을 하나 이상 선택하세요.");
        return false;
    }

    if(document.pressed == "선택삭제") {
        if(!confirm("선택한 자료를 정말 삭제하시겠습니까?")) {
            return false;
        }
    }

    return true;
}
*/
function flist_submit(action)
{
	

	var f = document.fmemberlist;
	
	if(action ==1) {
		if(f.fr_date.value == '') {
			alert("지급일을 입력하세요.");
			return false;
		}
		if (!is_checked("chk[]")) {
			alert("정산할 지급건을 체크 해주세요.");
			return false;
		}
		if (!confirm("선택한 정산을 지급요청하시겠습니까?"))
			return false;
		

		var year = $('#list_year option:selected').val();
		var month = $('#list_month option:selected').val();
		$('#yearmonth').val(year+month);		

	}
	
    f.action = "./member_list122.php";
	f.submit();
    return true;
}
</script>

<?php
include_once ('./admin.tail.php');
?>

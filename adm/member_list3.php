<?php
$sub_menu = "200120";
include_once('./_common.php');

auth_check($auth[$sub_menu], 'r');



if(! preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $fr_date) ) $fr_date = '';
if(! preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $to_date) ) $to_date = '';
if($fr_date =='') $fr_date = date("Y-m-d", strtotime('-5 year'));
if($to_date =='') $to_date = date("Y-m-d");
 $today_date = date("Y-m-d");
$sql_common = " from {$g5['member_table']} a ";

$sql_search = " where (1) ";
if ($stx) {
    $sql_search .= " and ( ";
    switch ($sfl) {
        
        case 'mb_name' :
        case 'mb_id' :
            $sql_search .= " ({$sfl} like '%{$stx}%') ";
            break;
		case 'pharm_name' :
            $sql_search .= " ({$sfl} like '%{$stx}%') ";
            break;
		case 'mb_hp' :
            $sql_search .= " ({$sfl} like '%{$stx}%') ";
            break;
        default :
            $sql_search .= " ({$sfl} like '{$stx}%') ";
            break;
    }
    $sql_search .= " ) ";
}

    $sql_search .= " and mb_type = '0' ";

$sql_search .= " and mb_datetime between '$fr_date 00:00:00' and '$to_date 23:59:59' ";


/* 회원구분 */
if ( $mb_referee != "" ) {
    $sql_search .= " and mb_referee = '$mb_referee' ";
}


//상태
if ($pharm_status) {
    $sql_search .= " and pharm_status = '$pharm_status' ";
}

/* 추천약국구분 */
if ( $mb_referee2 != "" ) {

	 $sql_search .= " and mb_referee2 = '$mb_referee2' ";		

}


if ($pharm_div) {
    $sql_search .= " and pharm_div = '$pharm_div' ";
}
if ($pharm_search) {
    $sql_search .= " and pharm_search = '$pharm_search' ";
}

if ($pharm_sido) {
    $sql_search .= " and pharm_sido2 = '$pharm_sido' ";
}
if ($pharm_gugun) {
    $sql_search .= " and pharm_gugun2 = '$pharm_gugun' ";
}
if ($pharm_dong) {
    $sql_search .= " and pharm_dong2 = '$pharm_dong' ";
}


if ($sales_id) {
    $sql_search .= " and pharm_manager = '$sales_id' ";
}




if ($pharm_id) {
    $sql_search .= " and mb_recommend = '$pharm_id' ";
}





if (!$sst) {
    $sst = "mb_datetime";
    $sod = "desc";
}

$sql_order = " order by {$sst} {$sod} ";

/*
$sql = " select count(*) as cnt from ( 
			select *, (select mb_referee from tbl_member where mb_id = a.mb_recommend) mb_referee2 
					, (select pharm_sido from tbl_member where a.mb_recommend = mb_id) as  pharm_sido2
					, (select pharm_gugun from tbl_member where a.mb_recommend = mb_id) as  pharm_gugun2
					, (select pharm_dong from tbl_member where a.mb_recommend = mb_id) as  pharm_dong2			
			{$sql_common} 
		) as x {$sql_search} {$sql_order} ";
*/
//속도개선 요청에의해 수정
$sql = " select count(mb_id) as cnt from ( 
			select *
			{$sql_common} 
		) as x {$sql_search} {$sql_order} ";

$row = sql_fetch($sql);
$total_count = $row['cnt'];

$rows = $config['cf_page_rows'];
$total_page  = ceil($total_count / $rows);  // 전체 페이지 계산
if ($page < 1) $page = 1; // 페이지가 없으면 첫 페이지 (1 페이지)
$from_record = ($page - 1) * $rows; // 시작 열을 구함


$listall = '<a href="'.$_SERVER['SCRIPT_NAME'].'" class="ov_listall">전체목록</a>';

$g5['title'] = '소비자회원관리';
include_once('./admin.head.php');
include_once(G5_PLUGIN_PATH.'/jquery-ui/datepicker.php');
/*
$sql = " select * from( select a.*
                ,(select pharm_name from tbl_member where mb_id = a.mb_recommend) pharm_name2
				,(select pharm_custno from tbl_member where mb_id = a.mb_recommend) pharm_custno2
				,(select sum(cp_price) from tbl_shop_coupon where mb_id = a.mb_id) cp_price2
				,(select mb_referee from tbl_member where mb_id = a.mb_recommend) mb_referee2
				, (select pharm_sido from tbl_member where a.mb_recommend = mb_id) as  pharm_sido2
				, (select pharm_gugun from tbl_member where a.mb_recommend = mb_id) as  pharm_gugun2
				, (select pharm_dong from tbl_member where a.mb_recommend = mb_id) as  pharm_dong2
		{$sql_common} ) as x {$sql_search} {$sql_order} limit {$from_record}, {$rows} ";
//echo "<pre>".$sql;
*/

//속도개선 요청에의해 수정
$sql = "
		select * FROM( 
				 SELECT   a.mb_id
						, a.mb_recommend
						, a.mb_name
						, a.mb_hp
						, a.mb_point
						, a.pharm_status
						, a.mb_datetime
						, a.mb_nick
						, a.pharm_manager
						, a.pharm_search
						, a.pharm_div
						, a.mb_referee
						, a.mb_type
						, b.pharm_name AS pharm_name2
						, b.pharm_custno AS pharm_custno2
						, c.cp_price as cp_price2
						, d.cp_price as cp_price3
						, b.mb_referee AS mb_referee2
						, b.pharm_sido AS pharm_sido2
						, b.pharm_gugun AS pharm_gugun2
						, b.pharm_dong AS pharm_dong2
						, a.service_type
				 from {$g5['member_table']} as a left JOIN {$g5['member_table']} as b ON a.mb_recommend = b.mb_id 
												 left JOIN (
												
													SELECT mb_id , sum(cp_price) AS cp_price FROM tbl_shop_coupon where service_type is null and '$today_date' <= cp_end GROUP BY mb_id
																		  
												) c ON a.mb_id = c.mb_id
												 left JOIN (
												
													SELECT mb_id , sum(cp_price) AS cp_price FROM tbl_shop_coupon where service_type ='B' and '$today_date' <= cp_end GROUP BY mb_id
																		  
												) d ON a.mb_id = d.mb_id
		) as x {$sql_search} {$sql_order} limit {$from_record}, {$rows} 
	";


$result = sql_query($sql);

$colspan = 17;
?>

<div class="local_ov01 local_ov">
    <?php echo $listall ?>
    <span class="btn_ov01"><span class="ov_txt">총회원수 </span><span class="ov_num"> <?php echo number_format($total_count) ?>명 </span></span>
  
</div>
<style>
.btn1 {
    height: 24px;
    border: 0;
    border-radius: 5px;
    padding: 0 10px;
    font-weight: bold;
    font-size: 1.09em;
    vertical-align: middle;
	margin:2px;
}
</style>
<form id="fsearch" name="fsearch" class="local_sch01 local_sch" method="get">


<div class="local_sch03  tbl_wrap" style="padding:10px;">
    <table>
   
    <tbody>
	<tr>
        <th scope="row">등록일</th>
		<td colspan=5>
            <input type="text" id="fr_date"  name="fr_date" value="<?php echo $fr_date; ?>" class="frm_input" size="10" maxlength="10"> ~
			<input type="text" id="to_date"  name="to_date" value="<?php echo $to_date; ?>" class="frm_input" size="10" maxlength="10">
			<button type="button" onclick="javascript:set_date('오늘');">오늘</button>
			<button type="button" onclick="javascript:set_date('어제');">어제</button>
			<button type="button" onclick="javascript:set_date('이번주');">이번주</button>
			<button type="button" onclick="javascript:set_date('이번달');">이번달</button>
			<button type="button" onclick="javascript:set_date('지난주');">지난주</button>
			<button type="button" onclick="javascript:set_date('지난달');">지난달</button>
			<button type="button" onclick="javascript:set_date('전체');">전체</button>
        </td>
		<th scope="row">지역</th>
        <td >
            <select name="pharm_sido" id="pharm_sido" onChange="onChangeAddr(1)">
				<option value="">=시도=</option>
				<?php
				$sql = "SELECT distinct sido ";
				$sql .= "FROM tbl_addr ";
				$sql .= "ORDER BY sido ";
				$res = sql_query($sql);
				for ($i=0; $row=sql_fetch_array($res); $i++) {
					$sido = $row["sido"];
					?>
					<option value="<?php echo $sido; ?>" <?php if ( $pharm_sido == $sido ) {
						echo "selected";
					} ?>><?php echo $sido; ?></option>
					<?php
				}
				?>
			</select>
			<select name="pharm_gugun" id="pharm_gugun" onchange="onChangeAddr(2,2)">
				<option value="">==전체==</option>
				<?php
				$sql = "SELECT distinct gugun ";
				$sql .= "FROM tbl_addr ";
				$sql .= " where sido =  '".$pharm_sido. "'";
				$sql .= "ORDER BY gugun ";
				$res = sql_query($sql);
				for ($i=0; $row=sql_fetch_array($res); $i++) {
					$gugun = $row["gugun"];
					?>
					<option value="<?php echo $gugun; ?>" <?php if ( $pharm_gugun == $gugun ) {
						echo "selected";
					} ?>><?php echo $gugun; ?></option>
					<?php
				}
				?>
			</select>
			<select name="pharm_dong" id="pharm_dong" >
				<option value="">==전체==</option>
				<?php
				$sql = "SELECT distinct dong ";
				$sql .= "FROM tbl_addr ";
				$sql .= " where gugun =  '".$pharm_gugun. "'";
				$sql .= "ORDER BY dong ";
				$res = sql_query($sql);
				for ($i=0; $row=sql_fetch_array($res); $i++) {
					$dong = $row["dong"];
					?>
					<option value="<?php echo $dong; ?>" <?php if ( $pharm_dong == $dong ) {
						echo "selected";
					} ?>><?php echo $dong; ?></option>
					<?php
				}
				?>
			</select>
        </td>
		
		<td rowspan="2"  width="160">
			 <input type="submit" value="검색" class="get_theme_confc btn btn_01">
			 <input type="button" value="엑셀 다운로드" class="get_theme_confc btn btn_02" style="cursor:pointer" onclick="formSubmit(1);">    

		</td>
		<td rowspan="2"  width="150">			
			 <input type="button" value="엑셀 다운로드(포인트)" class="get_theme_confc btn1 btn_02" style="cursor:pointer" onclick="formSubmit(2);"> 
			 <input type="button" value="엑셀 다운로드(쿠폰)" class="get_theme_confc btn1 btn_02" style="cursor:pointer" onclick="formSubmit(3);">    
		</td>
    </tr>
	<tr>
        <th scope="row">회원구분</th>
        <td>
			<select name="mb_referee" id="mb_referee"  >
			    <option value="">==전체==</option>
				<option value="0" <?php echo get_selected($mb_referee, '0'); ?>>일반회원</option>
				<option value="1" <?php echo get_selected($mb_referee, '1'); ?>>OS추천회원</option>
				<option value="2" <?php echo get_selected($mb_referee, '2'); ?>>모니터링회원</option>
				<option value="3" <?php echo get_selected($mb_referee, '3'); ?>>OS회원</option>
				<option value="4" <?php echo get_selected($mb_referee, '4'); ?>>OS직원</option>
			</select>
						
        </td>
        <th scope="row">추천약국구분</th>
        <td>
			<select name="mb_referee2" id="mb_referee2"  >
			    <option value="">==전체==</option>
				<option value="0" <?php echo get_selected($mb_referee2, '0'); ?>>일반회원</option>
				<option value="3" <?php echo get_selected($mb_referee2, '3'); ?>>os회원</option>
				<option value="5" <?php echo get_selected($mb_referee2, '5'); ?>>얼라이언스약국</option>
			</select>
						
        </td>
		    
		<th scope="row">사용여부</th>
        <td>
           <select name="pharm_status" id="pharm_status" >
				<option value="">==전체==</option>				
				<option value="1" <?php echo get_selected($pharm_status, '1'); ?>>사용대기</option>
				<option value="2" <?php echo get_selected($pharm_status, '2'); ?>>사용(승인)</option>
				<option value="3" <?php echo get_selected($pharm_status, '3'); ?>>사용안함</option>
				<option value="D" <?php echo get_selected($pharm_status, 'D'); ?>>휴면</option>
			</select>
        </td>
		<th scope="row">검색</th>
        <td>
            <select name="sfl" id="sfl" >
			
			<option value="mb_name" <?php echo get_selected($sfl, 'mb_name'); ?>>이름</option>
			<option value="mb_id" <?php echo get_selected($sfl, 'mb_id'); ?>>아이디</option>
			<option value="mb_hp" <?php echo get_selected($sfl, 'mb_hp'); ?>>핸드폰번호</option>
			
			</select>
			<label for="stx" class="sound_only">검색어</label>
			<input type="text" name="stx" value="<?php echo $stx ?>" id="stx" class=" frm_input">
			<!-- <input type="submit" class="btn_submit" value="검색"> -->
        </td>
		
    </tr>
	
    </tbody>
    </table>
</div>



</form>

<!-- <div class="local_desc01 local_desc">
    <p>
        회원자료 삭제 시 다른 회원이 기존 회원아이디를 사용하지 못하도록 회원아이디, 이름, 닉네임은 삭제하지 않고 영구 보관합니다.
    </p>
</div> -->


<form name="fmemberlist" id="fmemberlist" action=""  method="post">
<input type="hidden" name="sst" value="<?php echo $sst ?>">
<input type="hidden" name="sod" value="<?php echo $sod ?>">
<input type="hidden" name="sfl" value="<?php echo $sfl ?>">
<input type="hidden" name="stx" value="<?php echo $stx ?>">
<input type="hidden" name="page" value="<?php echo $page ?>">
<input type="hidden" name="token" value="">

<div class="tbl_head01 tbl_wrap">
    <table>
    <caption><?php echo $g5['title']; ?> 목록</caption>
    <thead>
    <tr>
        <th scope="col" id="mb_list_chk" >
            <label for="chkall" class="sound_only">회원 전체</label>
            <input type="checkbox" name="chkall" value="1" id="chkall" onclick="check_all(this.form)">
        </th>
		<th scope="col" >서비스명</th>        
        <th scope="col" id="mb_list_id" >아이디</th>
		<th scope="col" id="mb_list_name">이름</th>
        <th scope="col" id="mb_list_nick">연락처</th>
		<th scope="col" id="mb_list_auth">포인트</th>
		<th scope="col" id="mb_list_auth">오더스테이션쿠폰</th>
		<th scope="col" id="mb_list_auth">이즈브레앱쿠폰</th>
		<th scope="col" id="mb_list_auth">관리약국</th>
		<th scope="col" id="mb_list_auth">담당영업사원</th>
        <th scope="col" id="mb_list_auth">상태</th>
        <th scope="col" id="mb_list_mobile">회원구분</th>
        <th scope="col" id="mb_list_mobile">추천약국구분</th>
        <th scope="col" id="mb_list_lastcall">가입일</a></th>
		<th scope="col" id="mb_list_mng">관리</th>
    </tr>
    </thead>
    <tbody>
    <?php
    for ($i=0; $row=sql_fetch_array($result); $i++) {

       
        $s_mod = '<a href="./member_form.php?'.$qstr.'&amp;w=u&amp;m=3&amp;mb_id='.$row['mb_id'].'" class="btn btn_03">수정</a>';
        
		// 담당영업
        $sql3 = " select a.mb_name from tbl_member a, tbl_member b  where a.mb_id = b.pharm_manager and b.mb_id  = '{$row['mb_recommend']}' ";

        $row3 = sql_fetch($sql3);
		$mb_sales = $row3['mb_name'];

      
        $mb_id = $row['mb_id'];
       
    
        $bg = 'bg'.($i%2);

		$mb_id           = $row["mb_id"];
		$mb_name         = $row["mb_name"];
		$mb_hp          = $row["mb_hp"];
		$mb_point          = $row["mb_point"];
		$pharm_name2          = $row["pharm_name2"];
		$pharm_custno2          = $row["pharm_custno2"];
		$cp_price2          = $row["cp_price2"];
		$cp_price3          = $row["cp_price3"];

		$pharm_status          = $row["pharm_status"];

		$mb_referee          = $row["mb_referee"];
		$mb_referee2          = $row["mb_referee2"];
		$mb_datetime          = substr($row["mb_datetime"], 0, 10);
		/*
		switch ( $mb_referee ) {
			case "0" :
				$mb_referee_str = "일반회원";
				break;
			case "1" :
				$mb_referee_str = "OS추천회원";
				break;
			case "2" :
				$mb_referee_str = "모니터링회원";
				break;
			case "3" :
				$mb_referee_str = "OS서팩2회원";
				break;
			case "4" :
				$mb_referee_str = "OS직원";
				break;
			case "5" :
				$mb_referee_str = "얼라이언스약국";
				break;
		}
		*/
		
		$option='';$option1='';
		
    ?>

    <tr class="<?php echo $bg; ?>">
        
        <td headers="mb_list_chk" class="td_chk" >
            <input type="hidden" name="mb_id[<?php echo $i ?>]" value="<?php echo $row['mb_id'] ?>" id="mb_id_<?php echo $i ?>">
            <label for="chk_<?php echo $i; ?>" class="sound_only"><?php echo get_text($row['mb_name']); ?> <?php echo get_text($row['mb_nick']); ?>님</label>
            <input type="checkbox" name="chk[]" value="<?php echo $i ?>" id="chk_<?php echo $i ?>">
        </td>
		<td align="center"  >
		<?=TransServiceName($row['service_type']);?>
		</td>
		<td align="center"><?php echo $mb_id; ?></td>
		<td align="center"><?php echo $mb_name; ?></td>
		<td align="center"><?php echo $mb_hp; ?></td>
		
		<td align="center"><?php echo $mb_point; ?></td>
		<td align="center"><?php echo $cp_price2; ?></td>
		<td align="center"><?php echo $cp_price3; ?></td>
		<td align="center"><?php echo $pharm_name2; ?>(<?php echo $pharm_custno2;?>)</td>
		<td align="center"><?php echo $mb_sales; ?></td>

		<td align="center">
			<select name="pharm_status[<?php echo $i; ?>]">
				<?php
					$option .= '<option value="2" '.get_selected($pharm_status, '2').'>사용(승인)</option>'.PHP_EOL;
					$option .= '<option value="3" '.get_selected($pharm_status, '3').'>사용안함</option>'.PHP_EOL;
					echo $option;
				?>
			</select>

		</td>
		<td align="center">
			<select name="mb_referee[<?php echo $i; ?>]">
				<?php
					$option1 .= '<option value="0" '.get_selected($mb_referee, '0').'>일반회원</option>'.PHP_EOL;
					$option1 .= '<option value="1" '.get_selected($mb_referee, '1').'>OS추천회원</option>'.PHP_EOL;
					$option1 .= '<option value="2" '.get_selected($mb_referee, '2').'>모니터링회원</option>'.PHP_EOL;
					$option1 .= '<option value="3" '.get_selected($mb_referee, '3').'>OS회원</option>'.PHP_EOL;
					$option1 .= '<option value="4" '.get_selected($mb_referee, '4').'>OS직원</option>'.PHP_EOL;
					echo $option1;
				?>
			</select>
		</td>
		<td align="center">
			<?
				if( $mb_referee2 == "0" ){
					echo "일반회원";
				}else if( $mb_referee2 == "3" ){			
					#echo "OS서팩2회원";
				}else if( $mb_referee2 == "5" ){			
					echo "얼라이언스약국";
				}
			?>		
		</td>
		<td align="center"><?php echo $mb_datetime; ?></td>
		
		<td width="150">
			<?php echo $s_mod ?>
			
		</td>

	</tr>

    <?php
    }
	?>
	<tr>
	  <td colspan="13" align="left">
	  	
	  </td>

	  <td>
		
		<div class="local_cmd01 ">
		<input type="button" value="일괄수정" class="btn_submit" onclick="flist_submit();">   
		</div>
	
	  </td>
	</tr>
	<?php
    if ($i == 0)
        echo "<tr><td colspan=\"".$colspan."\" class=\"empty_table\">자료가 없습니다.</td></tr>";
    ?>
    </tbody>
    </table>
</div>


</form>

<?php 
$qstr ='mb_referee='.$_GET['mb_referee'].'&mb_referee2='.$_GET['mb_referee2'].'&pharm_id='.$_GET['pharm_id'].'&'.$qstr;
echo get_paging(G5_IS_MOBILE ? $config['cf_mobile_pages'] : $config['cf_write_pages'], $page, $total_page, '?'.$qstr.'&amp;page='); ?>

<script>

$(function(){
    $("#fr_date, #to_date").datepicker({ changeMonth: true, changeYear: true, dateFormat: "yy-mm-dd", showButtonPanel: true, yearRange: "c-99:c+99", maxDate: "+0d" });
});

//onChangeAddr(1,'');
//setTimeout(function(){ onChangeAddr(2,'');}, 100); 
function onChangeAddr(step, val){
	var sido = $("#pharm_sido");
	var gugun = $("#pharm_gugun");
	var dong = $("#pharm_dong");
	var obj = "";

	if(step == 1){
		obj = gugun;
		gugun.empty().data('option');
		gugun.append("<option value=''>시/군/구</option>");
	}
	if(step == 2){
		obj = dong; 
	}
	dong.empty().data('option');
	dong.append("<option value=''>동/면/읍</option>");
	$.ajax({
		type:"post",
		dataType:"xml",
		url: "/addr.php",
		data:{"mem_sido":sido.val(),"mem_gugun":gugun.val(), "step":step},
		success:function(data){
			var row= $(data).find("ROW").find("CELL");    
			var size=row.length;
			for(var i=0;i<size;i++){
				var cell =row.eq(i);
				$("<option></option>").text(cell.find("NAME").text()).attr("value",cell.find("NAME").text()).appendTo(obj);
			}

			if(step==1){
				gugun.val(val);
			}
			if(step==2){
				dong.val(val);
			}
		},
		error:function(request, status, errorThrown){
			//alert(errorThrown);
		}
	});
}

function formSubmit(type) {
		var form = "<form action='member_list3_01.php' method='post'>"; 
		if(type==1) {
			form += "<input type='hidden' name='mode' value='excel1' />"; 
		}
		if(type==2) {
			form += "<input type='hidden' name='mode' value='excel2' />"; 
		}
		if(type==3) {
			form += "<input type='hidden' name='mode' value='excel3' />"; 
		}

		form += "<input type='hidden' name='fr_date' value='"+$('#fr_date').val()+"' />"; 
		form += "<input type='hidden' name='to_date' value='"+$('#to_date').val()+"' />"; 
		form += "<input type='hidden' name='mb_referee' value='"+$('#mb_referee option:selected').val()+"' />"; 
		form += "<input type='hidden' name='mb_referee2' value='"+$('#mb_referee2 option:selected').val()+"' />"; 
		form += "<input type='hidden' name='pharm_status' value='"+$('#pharm_status option:selected').val()+"' />"; 
		form += "<input type='hidden' name='pharm_sido' value='"+$('#pharm_sido option:selected').val()+"' />"; 
	   form += "<input type='hidden' name='pharm_gugun' value='"+$('#pharm_gugun option:selected').val()+"' />"; 
	   form += "<input type='hidden' name='pharm_dong' value='"+$('#pharm_dong option:selected').val()+"' />"; 

		form += "<input type='hidden' name='sfl' value='"+$('#sfl option:selected').val()+"' />"; 
		form += "<input type='hidden' name='stx' value='"+$('#stx').val()+"' />"; 
		form += "</form>"; 
		jQuery(form).appendTo("body").submit().remove(); 
	}
function flist_submit()
{
	
	
	var f = document.fmemberlist;

	if (!is_checked("chk[]")) {
        alert("변경 하실 항목을 하나 이상 선택하세요.");
        return false;
    }
	
		
	//if (!confirm("일괄 변경하시겠습니까?"))
	//	return false;

    f.action = "./member_list3_update.php";
	f.submit();
    return true;
}

function set_date(today)
{
    <?php
    $date_term = date('w', G5_SERVER_TIME);
    $week_term = $date_term + 7;
    $last_term = strtotime(date('Y-m-01', G5_SERVER_TIME));
    ?>
    if (today == "오늘") {
        document.getElementById("fr_date").value = "<?php echo G5_TIME_YMD; ?>";
        document.getElementById("to_date").value = "<?php echo G5_TIME_YMD; ?>";
    } else if (today == "어제") {
        document.getElementById("fr_date").value = "<?php echo date('Y-m-d', G5_SERVER_TIME - 86400); ?>";
        document.getElementById("to_date").value = "<?php echo date('Y-m-d', G5_SERVER_TIME - 86400); ?>";
    } else if (today == "이번주") {
        document.getElementById("fr_date").value = "<?php echo date('Y-m-d', strtotime('-'.$date_term.' days', G5_SERVER_TIME)); ?>";
        document.getElementById("to_date").value = "<?php echo date('Y-m-d', G5_SERVER_TIME); ?>";
    } else if (today == "이번달") {
        document.getElementById("fr_date").value = "<?php echo date('Y-m-01', G5_SERVER_TIME); ?>";
        document.getElementById("to_date").value = "<?php echo date('Y-m-d', G5_SERVER_TIME); ?>";
    } else if (today == "지난주") {
        document.getElementById("fr_date").value = "<?php echo date('Y-m-d', strtotime('-'.$week_term.' days', G5_SERVER_TIME)); ?>";
        document.getElementById("to_date").value = "<?php echo date('Y-m-d', strtotime('-'.($week_term - 6).' days', G5_SERVER_TIME)); ?>";
    } else if (today == "지난달") {
        document.getElementById("fr_date").value = "<?php echo date('Y-m-01', strtotime('-1 Month', $last_term)); ?>";
        document.getElementById("to_date").value = "<?php echo date('Y-m-t', strtotime('-1 Month', $last_term)); ?>";
	} else if (today == "12개월") {
        document.getElementById("fr_date").value = "<?php echo date('Y-m-01', strtotime('-1 Year', $last_term)); ?>";
        document.getElementById("to_date").value = "<?php echo date('Y-m-d', G5_SERVER_TIME); ?>";
    } else if (today == "전체") {
        document.getElementById("fr_date").value = "";
        document.getElementById("to_date").value = "";
    }
}
</script>

<?php
include_once ('./admin.tail.php');
?>

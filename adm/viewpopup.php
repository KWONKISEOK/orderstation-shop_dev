<?php
include_once('./_common.php');

$comp_code = $member['comp_code'];

$sql = "SELECT * FROM (
			 SELECT distinct a.wr_id , a.wr_subject , a.wr_datetime , a.wr_1 , a.wr_2 , a.wr_3 , a.wr_4 , b.wr_comp_code , 
					  IFNULL( b.wr_confirm_yn , 'N' ) AS wr_confirm_yn , b.wr_del_yn , b.wr_confirm_time
			 FROM 
			 tbl_write_vender a left join tbl_write_vender_choice b ON a.wr_id = b.wr_id AND b.wr_comp_code = '".$comp_code."'
								  left join tbl_member c ON b.wr_comp_code = c.comp_code 
		 ) AS X WHERE (X.wr_4 = 'A' and X.wr_confirm_yn != 'Y') 
				OR ( X.wr_4 = 'C' and X.wr_comp_code = '".$comp_code."' AND X.wr_confirm_yn != 'Y' )
		order by x.wr_id desc
		";
$result = sql_query($sql);

?>

<link rel="stylesheet" href="/adm/css/admin.css">
<link rel="stylesheet" href="/js/font-awesome/css/font-awesome.min.css">

<div id="wrapper">

<div class="container_wr" style="margin-top:40px;">
	<div class="tbl_head01 tbl_wrap">
		<table>
		<thead>
		<tr>
			<th scope="col">제목</th>
			<th scope="col" width="150">등록일</th>
			<th scope="col" width="100">보기</th>
		</tr>
		</thead>
		<tbody>
		<? for($i=0; $row=sql_fetch_array($result); $i++) { ?>
		<tr class="bg0">
			<td class="td_left"><?php echo htmlspecialchars2($row['wr_subject']); ?></td>
			<td class="td_mng"><?php echo htmlspecialchars2($row['wr_datetime']); ?></td>
			<td class="td_mng"><input type="button" value="확인하기" onclick="javascript:parent.location.href='/adm/venderview.php?wr_id=<?=$row['wr_id']?>';"></td>
		</tr>
		<? }?>
		</tbody>
		</table>
	</div>
</div>

</div>
<?php
$sub_menu = "200101";
include_once('./_common.php');

auth_check($auth[$sub_menu], 'r');

$sql_common = " from {$g5['member_table']} a ";

$sql_search = " where (1) ";
if ($stx) {
    $sql_search .= " and ( ";
    switch ($sfl) {
        
        case 'mb_name' :
        case 'pharm_custno' :
            $sql_search .= " ({$sfl} like '%{$stx}') ";
            break;
		case 'pharm_name' :
            $sql_search .= " ({$sfl} like '%{$stx}%') ";
            break;
        default :
            $sql_search .= " ({$sfl} like '{$stx}%') ";
            break;
    }
    $sql_search .= " ) ";
}

$sql_search .= " and mb_type = '1' and pharm_bank_agree ='Y' ";

//상태
if ($pharm_status) {
    $sql_search .= " and pharm_status = '$pharm_status' ";
}
if ($mb_referee) {
    $sql_search .= " and mb_referee = '$mb_referee' ";
}
if ($pharm_div) {
    $sql_search .= " and pharm_div = '$pharm_div' ";
}
if ($pharm_search) {
    $sql_search .= " and pharm_search = '$pharm_search' ";
}



if ($pharm_control) {
    $sql_search .= " and pharm_control = '$pharm_control' ";
}
if ($pharm_look) {
    $sql_search .= " and pharm_look = '$pharm_look' ";
}
if ($mb_pay_yn) {
    $sql_search .= " and mb_pay_yn = '$mb_pay_yn' ";
}
if ($pharm_status) {
    $sql_search .= " and pharm_status = '$pharm_status' ";
}
if ($pharm_map) {    
	if ( $pharm_map == 'Y' ) {
		$sql_search .= " and pharm_map_lat != '' ";
	} else {
		$sql_search .= " and pharm_map_lat = '' ";
	}
}
if ($pharm_sido) {
    $sql_search .= " and pharm_sido = '$pharm_sido' ";
}

if ($sales_id) {
    $sql_search .= " and pharm_manager = '$sales_id' ";
}





if (!$sst) {
    $sst = "mb_datetime";
    $sod = "desc";
}

$sql_order = " order by {$sst} {$sod} ";

$sql = " select count(*) as cnt {$sql_common} {$sql_search} {$sql_order} ";
$row = sql_fetch($sql);
$total_count = $row['cnt'];

$rows = $config['cf_page_rows'];
$total_page  = ceil($total_count / $rows);  // 전체 페이지 계산
if ($page < 1) $page = 1; // 페이지가 없으면 첫 페이지 (1 페이지)
$from_record = ($page - 1) * $rows; // 시작 열을 구함
/*
// 탈퇴회원수
$sql = " select count(*) as cnt {$sql_common} {$sql_search} and mb_leave_date <> '' {$sql_order} ";
$row = sql_fetch($sql);
$leave_count = $row['cnt'];

// 차단회원수
$sql = " select count(*) as cnt {$sql_common} {$sql_search} and mb_intercept_date <> '' {$sql_order} ";
$row = sql_fetch($sql);
$intercept_count = $row['cnt'];
*/
$listall = '<a href="'.$_SERVER['SCRIPT_NAME'].'" class="ov_listall">전체목록</a>';

$g5['title'] = '개인소득정보관리';
include_once('./admin.head.php');

$sql = " select a.* 
		{$sql_common} {$sql_search} {$sql_order} limit {$from_record}, {$rows} ";
//echo $sql;

$result = sql_query($sql);

$colspan = 16;
?>

<div class="local_ov01 local_ov">
    <?php echo $listall ?>
    <span class="btn_ov01"><span class="ov_txt">총회원수 </span><span class="ov_num"> <?php echo number_format($total_count) ?>명 </span></span>
    <!-- <a href="?sst=mb_intercept_date&amp;sod=desc&amp;sfl=<?php echo $sfl ?>&amp;stx=<?php echo $stx ?>" class="btn_ov01"> <span class="ov_txt">차단 </span><span class="ov_num"><?php echo number_format($intercept_count) ?>명</span></a>
    <a href="?sst=mb_leave_date&amp;sod=desc&amp;sfl=<?php echo $sfl ?>&amp;stx=<?php echo $stx ?>" class="btn_ov01"> <span class="ov_txt">탈퇴  </span><span class="ov_num"><?php echo number_format($leave_count) ?>명</span></a> -->
</div>

<form id="fsearch" name="fsearch" class="local_sch01 local_sch" method="get">


<div class="local_sch03  tbl_wrap" style="padding:10px;">
    <table>
   
    <tbody>
	
	<tr>
        <th scope="row">결제구분</th>
        <td>
			<select name="mb_pay_yn" id="mb_pay_yn"  >
			    <option value="">==전체==</option>
				<option value="Y" <?php echo get_selected($mb_pay_yn, 'Y'); ?>>오더스테이션결제</option>
				<option value="N" <?php echo get_selected($mb_pay_yn, 'N'); ?>>직거래</option>
			</select>
			
        </td>
		
		<th scope="row">거래구분</th>
        <td>
            <select name="pharm_div" id="pharm_div" >
				<option value="">==전체==</option>
				<option value="1" <?php echo get_selected($pharm_div, '1'); ?>>태전약품</option>
				<option value="2" <?php echo get_selected($pharm_div, '2'); ?>>티제이팜</option>
			</select>
        </td>
	
		<th scope="row">사용여부</th>
        <td>
           <select name="pharm_status" id="pharm_status" >
				<option value="">==전체==</option>
				<option value="N" <?php echo get_selected($pharm_status, 'N'); ?>>신규가입</option>
				<option value="L" <?php echo get_selected($pharm_status, 'L'); ?>>ERP등록중</option>
				<option value="1" <?php echo get_selected($pharm_status, '1'); ?>>사용대기</option>
				<option value="2" <?php echo get_selected($pharm_status, '2'); ?>>사용(승인)</option>
				<option value="3" <?php echo get_selected($pharm_status, '3'); ?>>사용안함</option>
			</select>
        </td>

		<th scope="row">좌표유무</th>
        <td>
           <select name="pharm_map" id="pharm_map" >
				<option value="">==전체==</option>
				<option value="Y" <?php echo get_selected($pharm_map, 'Y'); ?>>등록</option>
				<option value="N" <?php echo get_selected($pharm_map, 'N'); ?>>미등록</option>
			</select>
        </td>
		<td rowspan="2">
			 <input type="submit" value="검색" class="get_theme_confc btn btn_01">

		</td>
    </tr>
	<tr>
		<th scope="row">추천구분</th>
        <td>
           <select name="pharm_look" id="pharm_look" >
				<option value="">==전체==</option>
				<option value="0" <?php echo get_selected($pharm_look, '0'); ?>>미선택</option>
				<option value="1" <?php echo get_selected($pharm_look, '1'); ?>>지역</option>
				<option value="2" <?php echo get_selected($pharm_look, '2'); ?>>전체</option>
			</select>
        </td>
     
		<th scope="row">시도</th>
        <td>
            <select name="pharm_sido" id="pharm_sido" onChange="onChangeAddr(1)">
				<option value="">=시도=</option>
				<?php
				$sql = "SELECT distinct sido ";
				$sql .= "FROM tbl_addr ";
				$sql .= "ORDER BY sido ";
				$res = sql_query($sql);
				for ($i=0; $row=sql_fetch_array($res); $i++) {
					$sido = $row["sido"];
					?>
					<option value="<?php echo $sido; ?>" <?php if ( $pharm_sido == $sido ) {
						echo "selected";
					} ?>><?php echo $sido; ?></option>
					<?php
				}
				?>
			</select>
        </td>
		<th scope="row">시구군</th>
        <td>
           <select name="pharm_gugun" id="pharm_gugun" >
				<option value="">==전체==</option>
			</select>
        </td>
		<th scope="row">검색</th>
        <td>
            <select name="sfl" id="sfl" >
			<option value="pharm_name" <?php echo get_selected($sfl, 'pharm_name'); ?>>약국명</option>
			<option value="mb_name" <?php echo get_selected($sfl, 'mb_name'); ?>>이름</option>
			<option value="mb_id" <?php echo get_selected($sfl, 'mb_id'); ?>>아이디</option>
			<option value="pharm_custno" <?php echo get_selected($sfl, 'pharm_custno'); ?>>약국코드</option>
			
			</select>
			<label for="stx" class="sound_only">검색어</label>
			<input type="text" name="stx" value="<?php echo $stx ?>" id="stx" class=" frm_input">
			<!-- <input type="submit" class="btn_submit" value="검색"> -->
        </td>
		
    </tr>
	
    </tbody>
    </table>
</div>



</form>

<!-- <div class="local_desc01 local_desc">
    <p>
        회원자료 삭제 시 다른 회원이 기존 회원아이디를 사용하지 못하도록 회원아이디, 이름, 닉네임은 삭제하지 않고 영구 보관합니다.
    </p>
</div> -->


<form name="fmemberlist" id="fmemberlist" action="" onsubmit="return fmemberlist_submit(this);" method="post">
<input type="hidden" name="sst" value="<?php echo $sst ?>">
<input type="hidden" name="sod" value="<?php echo $sod ?>">
<input type="hidden" name="sfl" value="<?php echo $sfl ?>">
<input type="hidden" name="stx" value="<?php echo $stx ?>">


<input type="hidden" name="page" value="<?php echo $page ?>">
<input type="hidden" name="token" value="">

<div class="tbl_head01 tbl_wrap">
    <table>
    <caption><?php echo $g5['title']; ?> 목록</caption>
    <thead>
    <tr>
        <th width="10"></th>
        <th scope="col" id="mb_list_id" >약국명</th>
		<th scope="col" id="mb_list_name">약사님</th>
        <th scope="col" id="mb_list_nick">은행명</th>
		<th scope="col" id="mb_list_auth">계좌번호</th>
		<th scope="col" id="mb_list_auth">예금주</th>
		<th scope="col" id="mb_list_auth">동의일</th>
        
    </tr>
    </thead>
    <tbody>
    <?php
    for ($i=0; $row=sql_fetch_array($result); $i++) {

       
        $mb_id = $row['mb_id'];
       

        $bg = 'bg'.($i%2);
		
		$mb_id           = $row["mb_id"];
		//$mb_pass         = $row["mb_pass"];
		$mb_name         = $row["mb_name"];
		$mb_tel          = $row["mb_tel"];
		$pharm_name   = $row["pharm_name"];

		$sales_name       = $row["sales_name"];
		$mb_status       = $row["pharm_status"];
	
		$pharm_bank_agree_date         = $row["pharm_bank_agree_date"];
		$pharm_bank_user         = $row["pharm_bank_user"];
		$pharm_bank_num          = $row["pharm_bank_num"];
		$pharm_bank_code          = $row["pharm_bank_code"];

		$trstyle = "";

		if ( $mb_name != $pharm_bank_user ) {
			$trstyle = "#99CCFF";
		}
		?>
    <tr  bgcolor="<?php echo $trstyle; ?>">
        
        <td ></td>
		<td align="center"><?php echo $pharm_name; ?></td>
		<td align="center"><?php echo $mb_name; ?></td>
		
		<td align="center"><?php echo ViewBank( $pharm_bank_code ); ?></td>
		<td align="center"><?php echo $pharm_bank_num; ?></td>		
		<td align="center"><?php echo $pharm_bank_user; ?></td>
		<td align="center"><?php echo $pharm_bank_agree_date; ?></td>
		
	</tr>

    <?php
    }
    if ($i == 0)
        echo "<tr><td colspan=\"".$colspan."\" class=\"empty_table\">자료가 없습니다.</td></tr>";
    ?>
    </tbody>
    </table>
</div>


</form>

<?php 
$qstr ='m=1&sales_id='.$_GET['sales_id'].'&'.$qstr;
echo get_paging(G5_IS_MOBILE ? $config['cf_mobile_pages'] : $config['cf_write_pages'], $page, $total_page, "{$_SERVER['SCRIPT_NAME']}?$qstr&amp;page="); ?>
<script>
function fmemberlist_submit(f)
{
    if (!is_checked("chk[]")) {
        alert(document.pressed+" 하실 항목을 하나 이상 선택하세요.");
        return false;
    }

    if(document.pressed == "선택삭제") {
        if(!confirm("선택한 자료를 정말 삭제하시겠습니까?")) {
            return false;
        }
    }

    return true;
}
function fnLogin(id) {
	if(confirm(id+" 로 로그인하시겠습니까?")) {
		location.href='/bbs/login_check_admin.php?mb_id=' + id;
	}
}
</script>

<?php
include_once ('./admin.tail.php');
?>

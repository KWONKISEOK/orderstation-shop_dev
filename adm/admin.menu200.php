<?php
$sql = " select count(*) as cnt from tbl_member 
		 where comp_change_status=0 and  comp_charge_datetime_1 is not null ";
$row = sql_fetch($sql);
//반품신청건수
$return_cnt_view = "(" .$row['cnt']. ")";

$menu['menu200'] = array (

	array('200000', '회원관리', G5_ADMIN_URL.'/member_list.php', 'member'),
	array('200100', '약국회원관리', G5_ADMIN_URL.'/member_list1.php', 'member1'),

	array('200101', '개인소득정보관리', G5_ADMIN_URL.'/member_list11.php', 'member11'),
	array('200102', '개인소득지급관리', G5_ADMIN_URL.'/member_list12.php', 'member12'),

    array('200110', '약국소비자회원관리', G5_ADMIN_URL.'/member_list2.php', 'member2'),
	array('200120', '소비자회원관리', G5_ADMIN_URL.'/member_list3.php', 'member3'),
	array('200130', '영업사원관리', G5_ADMIN_URL.'/member_list4.php', 'member4'),
	array('200140', '공급사관리'.$return_cnt_view, G5_ADMIN_URL.'/member_list5.php', 'member5'),
	array('200150', '관리자관리', G5_ADMIN_URL.'/member_list6.php', 'member6'),

    array('200300', '회원메일발송', G5_ADMIN_URL.'/mail_list.php', 'mb_mail'),
    //array('200800', '접속자집계', G5_ADMIN_URL.'/visit_list.php', 'mb_visit', 1),
    //array('200810', '접속자검색', G5_ADMIN_URL.'/visit_search.php', 'mb_search', 1),
    //array('200820', '접속자로그삭제', G5_ADMIN_URL.'/visit_delete.php', 'mb_delete', 1),
    array('200200', '포인트관리', G5_ADMIN_URL.'/point_list.php', 'mb_point'),
    array('200400', '적립금관리', G5_ADMIN_URL.'/cash_list.php', 'mb_cash'),
    //array('200900', '투표관리', G5_ADMIN_URL.'/poll_list.php', 'mb_poll')
);
?>
<?php
$sub_menu = '100111';
include_once('./_common.php');
include_once(G5_EDITOR_LIB);

auth_check($auth[$sub_menu], 'r');

$g5['title'] = '약관 관리';
include_once('./admin.head.php');

$sql = "select * from tbl_stplat ";
$result = sql_query($sql);

$cnt = "select count(*) from tbl_stplat ";
$result_cnt = sql_fetch($cnt);;
?>

<form name="stplatform" id="stplatform" method="post" action="./stplat_form_update.php">
    <input type="hidden" value="<?php $result_cnt['count(*)']?>">
    <?php for ($i = 0; $row = sql_fetch_array($result); $i++) { ?>
        <section id="anc_cf_basic">
            <div class="tbl_frm01 tbl_wrap">
                <table>
                    <colgroup>
                        <col class="grid_4">
                        <col>
                    </colgroup>
                    <tbody>
                    <tr>
                        <th scope="row" style="text-align: center"><?php echo $row['st_title'] ?></th>
                        <td><?php echo editor_html('st_' . $i, get_text($row['st_content'], 0)); ?></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </section>
    <?php } ?>
    <div class="btn_fixed_top">
        <input type="button" onclick="form_submit();" value="확인" class="btn_submit btn">
    </div>
</form>

<script>
    function form_submit() {
        <?php
        for ($i=0;$i<$result_cnt['count(*)'];$i++) {
            echo get_editor_js('st_'.$i);
        }
        ?>
        $('#stplatform').submit();
    }
</script>
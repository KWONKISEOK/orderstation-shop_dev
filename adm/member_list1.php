<?php
$sub_menu = "200100";
include_once('./_common.php');

auth_check($auth[$sub_menu], 'r');

$sql_common = " from {$g5['member_table']} a ";

$sql_search = " where (1) ";
if ($stx) {
    $sql_search .= " and ( ";
    switch ($sfl) {
        
        case 'mb_name' :
        case 'pharm_custno' :
            $sql_search .= " ({$sfl} like '%{$stx}') ";
            break;
		case 'pharm_name' :
            $sql_search .= " ({$sfl} like '%{$stx}%') ";
            break;
		case 'sales_name' :
            $sql_search .= " ({$sfl} like '%{$stx}%') ";
            break;
		case 'mb_hp' :
            $sql_search .= " ({$sfl} like '%{$stx}%') ";
            break;
        default :
            $sql_search .= " ({$sfl} like '{$stx}%') ";
            break;
    }
    $sql_search .= " ) ";
}
if ($is_admin != 'super')
    $sql_search .= " and mb_level <= '{$member['mb_level']}' ";

if ($is_admin == 'sales')
    $sql_search .= " and mb_manager = '{$member['mb_id']}' ";

//if ($_GET['mb_type'] != '')
    $sql_search .= " and mb_type = '1' ";

//상태
if ($pharm_status) {
    $sql_search .= " and pharm_status = '$pharm_status' ";
}
if ($mb_referee) {
    $sql_search .= " and mb_referee = '$mb_referee' ";
}
if ($pharm_div) {
    $sql_search .= " and pharm_div = '$pharm_div' ";
}
if ($pharm_search) {
    $sql_search .= " and pharm_search = '$pharm_search' ";
}



if ($pharm_control) {
    $sql_search .= " and pharm_control = '$pharm_control' ";
}
if ($pharm_look) {
    $sql_search .= " and pharm_look = '$pharm_look' ";
}
if ($mb_pay_yn) {
    $sql_search .= " and mb_pay_yn = '$mb_pay_yn' ";
}
if ($pharm_status) {
    $sql_search .= " and pharm_status = '$pharm_status' ";
}
if ($pharm_map) {    
	if ( $pharm_map == 'Y' ) {
		$sql_search .= " and pharm_map_lat != '' ";
	} else {
		$sql_search .= " and pharm_map_lat = '' ";
	}
}
if ($pharm_sido) {
    $sql_search .= " and pharm_sido = '$pharm_sido' ";
}
if ($pharm_gugun) {
    $sql_search .= " and pharm_gugun = '$pharm_gugun' ";
}
if ($pharm_dong) {
    $sql_search .= " and pharm_dong = '$pharm_dong' ";
}

if ($sales_id) {
    $sql_search .= " and pharm_manager = '$sales_id' ";
}





if (!$sst) {
    $sst = "mb_datetime";
    $sod = "desc";
}

$sql_order = " order by {$sst} {$sod} ";

/*
$sql = " 
		select count(*) as cnt from(
			select a.* ,
			(select mb_name from tbl_member where mb_id = a.pharm_manager and mb_type=5 limit 1)  as sales_name
			{$sql_common} 
		) as x  {$sql_search} {$sql_order} ";
*/

//속도개선 요청에의해 수정
$sql = " 
		select count(mb_id) as cnt from(
			select a.mb_id , a.mb_name , a.mb_tel , a.pharm_name ,  a.pharm_status , a.pharm_custno , a.pharm_div , a.mb_pay_yn , a.mb_hp,
				   a.pharm_sido , a.pharm_gugun , a.pharm_dong , a.pharm_look , a.pharm_map_lat , a.pharm_map_lng , a.pharm_control , 
				   a.mb_nick , a.mb_sort , a.mb_type , a.mb_level , a.mb_referee,  a.pharm_search , a.mb_datetime , a.pharm_manager,
				   (select mb_name from tbl_member where mb_id = a.pharm_manager and mb_type=5 limit 1)  as sales_name
			{$sql_common} 
		) as x  {$sql_search} {$sql_order} ";

$row = sql_fetch($sql);
$total_count = $row['cnt'];

if( empty($view_cnt) ){
	$rows = $config['cf_page_rows'];
}else{
	$rows = $view_cnt;
}
$total_page  = ceil($total_count / $rows);  // 전체 페이지 계산
if ($page < 1) $page = 1; // 페이지가 없으면 첫 페이지 (1 페이지)
$from_record = ($page - 1) * $rows; // 시작 열을 구함
/*
// 탈퇴회원수
$sql = " select count(*) as cnt {$sql_common} {$sql_search} and mb_leave_date <> '' {$sql_order} ";
$row = sql_fetch($sql);
$leave_count = $row['cnt'];

// 차단회원수
$sql = " select count(*) as cnt {$sql_common} {$sql_search} and mb_intercept_date <> '' {$sql_order} ";
$row = sql_fetch($sql);
$intercept_count = $row['cnt'];
*/
$listall = '<a href="'.$_SERVER['SCRIPT_NAME'].'" class="ov_listall">전체목록</a>';

$g5['title'] = '약국회원관리';
include_once('./admin.head.php');

$qstr = "pharm_search=$pharm_search&amp;pharm_look=$pharm_look&amp;mb_referee=$mb_referee&amp;pharm_status=$pharm_status&amp;sales_id=$sales_id&amp;pharm_control=$pharm_control&amp;pharm_sido=$pharm_sido&amp;pharm_gugun=$pharm_gugun&amp;pharm_dong=$pharm_dong&amp;sfl=$sfl&amp;stx=$stx&amp;view_cnt=$view_cnt";

/*
$sql = " 
		select * from(
			select a.* 
					,(select count(*) from tbl_member_user where mb_id = a.mb_id) as user_cnt
					,(select count(*) from tbl_member where mb_recommend = a.mb_id) as user_cnt2
					,(select mb_name from tbl_member where mb_id = a.pharm_manager and mb_type=5 limit 1)  as sales_name
			{$sql_common} 
		) as x {$sql_search} {$sql_order} limit {$from_record}, {$rows} ";
*/

//속도개선 요청에의해 수정
$sql = " 
	SELECT x.* FROM( 
		select a.mb_id , a.mb_name , a.mb_tel , a.pharm_name ,  a.pharm_status , a.pharm_custno , a.pharm_div , a.mb_pay_yn , a.mb_hp,
			   a.pharm_sido , a.pharm_gugun , a.pharm_dong , a.pharm_look , a.pharm_map_lat , a.pharm_map_lng , a.pharm_control , 
			   a.mb_nick , a.mb_sort , a.mb_type , a.mb_level , a.mb_referee,  a.pharm_search , a.mb_datetime , a.pharm_manager , 
			   ifnull(b.user_cnt,0) as user_cnt , ifnull(c.user_cnt2,0) as user_cnt2 , d.mb_name AS sales_name 
		from tbl_member a LEFT JOIN (
									SELECT COUNT(mb_id) AS user_cnt , mb_id FROM tbl_member_user GROUP BY mb_id
								) b ON a.mb_id = b.mb_id
							LEFT JOIN (
									SELECT COUNT(mb_recommend) AS user_cnt2 , mb_recommend FROM tbl_member GROUP BY mb_recommend
								) c ON a.mb_id = c.mb_recommend
							LEFT JOIN (
									SELECT max(mb_name) AS mb_name, mb_id from tbl_member where mb_type=5 GROUP BY mb_id 
								) d ON a.pharm_manager = d.mb_id
	) as x {$sql_search} {$sql_order} limit {$from_record}, {$rows} 
";

//echo $sql;

$result = sql_query($sql);

$colspan = 16;
?>
<div class="local_ov01 local_ov">
    <?php echo $listall ?>
    <span class="btn_ov01"><span class="ov_txt">총회원수 </span><span class="ov_num"> <?php echo number_format($total_count) ?>명 </span></span>
    <!-- <a href="?sst=mb_intercept_date&amp;sod=desc&amp;sfl=<?php echo $sfl ?>&amp;stx=<?php echo $stx ?>" class="btn_ov01"> <span class="ov_txt">차단 </span><span class="ov_num"><?php echo number_format($intercept_count) ?>명</span></a>
    <a href="?sst=mb_leave_date&amp;sod=desc&amp;sfl=<?php echo $sfl ?>&amp;stx=<?php echo $stx ?>" class="btn_ov01"> <span class="ov_txt">탈퇴  </span><span class="ov_num"><?php echo number_format($leave_count) ?>명</span></a> -->
</div>

<form id="fsearch" name="fsearch" class="local_sch01 local_sch" method="get">


<div class="local_sch03  tbl_wrap" style="padding:10px;">
    <table>
   
    <tbody>
	<tr>
        <th scope="row">추천회원구분</th>
        <td>
			<select name="mb_referee" id="mb_referee"  >
			    <option value="">==전체==</option>
				<option value="0" <?php echo get_selected($mb_referee, '0'); ?>>일반회원</option>
				<option value="3" <?php echo get_selected($mb_referee, '3'); ?>>OS회원</option>
				<option value="5" <?php echo get_selected($mb_referee, '5'); ?>>얼라이언스약국</option>
			</select>
			
        </td>
		<th scope="row">출고통제</th>
        <td>
            <select name="pharm_control" id="pharm_control" >
				<option value="">==전체==</option>
				<option value="Y" <?php echo get_selected($pharm_control, 'Y'); ?>>예</option>
				<option value="N" <?php echo get_selected($pharm_control, 'N'); ?>>아니오</option>
			</select>
        </td>
		
		<th scope="row">추천구분</th>
        <td>
           <select name="pharm_look" id="pharm_look" >
				<option value="">==전체==</option>
				<option value="0" <?php echo get_selected($pharm_look, '0'); ?>>미선택</option>
				<option value="1" <?php echo get_selected($pharm_look, '1'); ?>>지역</option>
				<option value="2" <?php echo get_selected($pharm_look, '2'); ?>>전체</option>
			</select>
        </td>
	
		<th scope="row">검색노출여부</th>
        <td>
           <select name="pharm_search" id="pharm_search" >
				<option value="">==전체==</option>
				<option value="Y" <?php echo get_selected($pharm_search, 'Y'); ?>>노출</option>
				<option value="N" <?php echo get_selected($pharm_search, 'N'); ?>>비노출</option>
			</select>
        </td>
		<td rowspan="3">
			 <input type="submit" value="검색" class="get_theme_confc btn btn_01">
			 <input type="button" value="엑셀 다운로드" class="get_theme_confc btn btn_02" style="cursor:pointer" onclick="formSubmit(1);">

		</td>
    </tr>
	<tr>
        <th scope="row">결제구분</th>
        <td>
			<select name="mb_pay_yn" id="mb_pay_yn"  >
			    <option value="">==전체==</option>
				<option value="Y" <?php echo get_selected($mb_pay_yn, 'Y'); ?>>오더스테이션결제</option>
				<option value="N" <?php echo get_selected($mb_pay_yn, 'N'); ?>>직거래</option>
			</select>
			
        </td>
		
		<th scope="row">거래구분</th>
        <td>
            <select name="pharm_div" id="pharm_div" >
				<option value="">==전체==</option>
				<option value="1" <?php echo get_selected($pharm_div, '1'); ?>>태전약품</option>
				<option value="2" <?php echo get_selected($pharm_div, '2'); ?>>티제이팜</option>
			</select>
        </td>
	
		<th scope="row">사용여부</th>
        <td>
           <select name="pharm_status" id="pharm_status" >
				<option value="">==전체==</option>
				<option value="N" <?php echo get_selected($pharm_status, 'N'); ?>>신규가입</option>
				<option value="L" <?php echo get_selected($pharm_status, 'L'); ?>>ERP등록중</option>
				<option value="1" <?php echo get_selected($pharm_status, '1'); ?>>사용대기</option>
				<option value="2" <?php echo get_selected($pharm_status, '2'); ?>>사용(승인)</option>
				<option value="3" <?php echo get_selected($pharm_status, '3'); ?>>사용안함</option>
			</select>
        </td>

		<th scope="row">좌표유무</th>
        <td>
           <select name="pharm_map" id="pharm_map" >
				<option value="">==전체==</option>
				<option value="Y" <?php echo get_selected($pharm_map, 'Y'); ?>>등록</option>
				<option value="N" <?php echo get_selected($pharm_map, 'N'); ?>>미등록</option>
			</select>
        </td>
    </tr>
	<tr>

     
		<th scope="row">지역</th>
        <td colspan='3'>
            <select name="pharm_sido" id="pharm_sido" onChange="onChangeAddr(1)">
				<option value="">=시도=</option>
				<?php
				$sql = "SELECT distinct sido ";
				$sql .= "FROM tbl_addr ";
				$sql .= "ORDER BY sido ";
				$res = sql_query($sql);
				for ($i=0; $row=sql_fetch_array($res); $i++) {
					$sido = $row["sido"];
					?>
					<option value="<?php echo $sido; ?>" <?php if ( $pharm_sido == $sido ) {
						echo "selected";
					} ?>><?php echo $sido; ?></option>
					<?php
				}
				?>
			</select>
			<select name="pharm_gugun" id="pharm_gugun" onchange="onChangeAddr(2,2)">
				<option value="">==전체==</option>
				<?php
				$sql = "SELECT distinct gugun ";
				$sql .= "FROM tbl_addr ";
				$sql .= " where sido =  '".$pharm_sido. "'";
				$sql .= "ORDER BY gugun ";
				$res = sql_query($sql);
				for ($i=0; $row=sql_fetch_array($res); $i++) {
					$gugun = $row["gugun"];
					?>
					<option value="<?php echo $gugun; ?>" <?php if ( $pharm_gugun == $gugun ) {
						echo "selected";
					} ?>><?php echo $gugun; ?></option>
					<?php
				}
				?>
			</select>
			<select name="pharm_dong" id="pharm_dong" >
				<option value="">==전체==</option>
				<?php
				$sql = "SELECT distinct dong ";
				$sql .= "FROM tbl_addr ";
				$sql .= " where gugun =  '".$pharm_gugun. "'";
				$sql .= "ORDER BY dong ";
				$res = sql_query($sql);
				for ($i=0; $row=sql_fetch_array($res); $i++) {
					$dong = $row["dong"];
					?>
					<option value="<?php echo $dong; ?>" <?php if ( $pharm_dong == $dong ) {
						echo "selected";
					} ?>><?php echo $dong; ?></option>
					<?php
				}
				?>
			</select>
        </td>
		
		<th scope="row">검색</th>
        <td>
            <select name="sfl" id="sfl" >
			<option value="pharm_name" <?php echo get_selected($sfl, 'pharm_name'); ?>>약국명</option>
			<option value="mb_name" <?php echo get_selected($sfl, 'mb_name'); ?>>이름</option>
			<option value="mb_id" <?php echo get_selected($sfl, 'mb_id'); ?>>아이디</option>
			<option value="pharm_custno" <?php echo get_selected($sfl, 'pharm_custno'); ?>>약국코드</option>
			<option value="sales_name" <?php echo get_selected($sfl, 'sales_name'); ?>>관리영업사원</option>
			<option value="mb_hp" <?php echo get_selected($sfl, 'mb_hp'); ?>>핸드폰번호</option>
			</select>
			<label for="stx" class="sound_only">검색어</label>
			<input type="text" name="stx" value="<?php echo $stx ?>" id="stx" class=" frm_input">
			<!-- <input type="submit" class="btn_submit" value="검색"> -->
        </td>
		
		<th scope="row">리스트갯수</th>
        <td>
            <select name="view_cnt" id="view_cnt" >
			<option value="">==선택==</option>
			<option value="200" <?php echo get_selected($view_cnt, '200'); ?>>200</option>
			<option value="300" <?php echo get_selected($view_cnt, '300'); ?>>300</option>
			</select>
		</td>

    </tr>
	
    </tbody>
    </table>
</div>



</form>

<!-- <div class="local_desc01 local_desc">
    <p>
        회원자료 삭제 시 다른 회원이 기존 회원아이디를 사용하지 못하도록 회원아이디, 이름, 닉네임은 삭제하지 않고 영구 보관합니다.
    </p>
</div> -->


<form name="fmemberlist" id="fmemberlist" action="" onsubmit="return fmemberlist_submit(this);" method="post">
<input type="hidden" name="sst" value="<?php echo $sst ?>">
<input type="hidden" name="sod" value="<?php echo $sod ?>">
<input type="hidden" name="sfl" value="<?php echo $sfl ?>">
<input type="hidden" name="stx" value="<?php echo $stx ?>">


<input type="hidden" name="page" value="<?php echo $page ?>">
<input type="hidden" name="token" value="">

<div class="tbl_head01 tbl_wrap">
    <table>
    <caption><?php echo $g5['title']; ?> 목록</caption>
    <thead>
    <tr>
        <th scope="col" id="mb_list_chk" >
            <label for="chkall" class="sound_only">회원 전체</label>
            <input type="checkbox" name="chkall" value="1" id="chkall" onclick="check_all(this.form)">
        </th>
		<th scope="col" id="mb_list_id" >정렬</th>
        <th scope="col" id="mb_list_id" >관리영업사원</th>
		<th scope="col" id="mb_list_name">추천여부</th>
        <th scope="col" id="mb_list_nick">지역</th>
		<th scope="col" id="mb_list_nick">약국코드</th>
		<th scope="col" id="mb_list_auth">약국명</th>
		<th scope="col" id="mb_list_auth">핸드폰번호</th>
		<th scope="col" id="mb_list_auth">약사님</th>
		<th scope="col" id="mb_list_auth">결제구분</th>
        <th scope="col" id="mb_list_auth">거래구분</th>
        <th scope="col" id="mb_list_mobile">지도좌표</th>
        <th scope="col" id="mb_list_lastcall">소비자수</a></th>
		<th scope="col" id="mb_list_sms">상태</a></th>		
		<th scope="col" id="mb_list_mng"><a href="/bbs/login_check_readmin.php?mb_id=admin&$qstr">
		<span class="get_theme_confc btn btn_01" style="padding:4px;">관리자재로그인</span></a></th>
    </tr>
    </thead>
    <tbody>
    <?php
    for ($i=0; $row=sql_fetch_array($result); $i++) {

        // 접근가능한 그룹수
        $sql2 = " select count(*) as cnt from {$g5['group_member_table']} where mb_id = '{$row['mb_id']}' ";
        $row2 = sql_fetch($sql2);
        $group = '';
        if ($row2['cnt'])
            $group = '<a href="./boardgroupmember_form.php?mb_id='.$row['mb_id'].'">'.$row2['cnt'].'</a>';

        if ($is_admin == 'group') {
            $s_mod = '';
        } else {
            $s_mod = '<a href="./member_form.php?'.$qstr.'&amp;w=u&amp;mb_id='.$row['mb_id'].'" class="btn btn_03">수정</a>';
        }


        $bg = 'bg'.($i%2);
		
		$mb_id           = $row["mb_id"];
		$mb_name         = $row["mb_name"];
		$mb_tel          = $row["mb_tel"];
		$pharm_name   = $row["pharm_name"];
		$mb_hp   = $row["mb_hp"];
		$sales_name       = $row["sales_name"];
		$mb_status       = $row["pharm_status"];
		$user_cnt         = $row["user_cnt"];
		$user_cnt2        = $row["user_cnt2"];
		$pharm_custno       = $row["pharm_custno"];
		$pharm_div          = $row["pharm_div"];
		$mb_pay_yn          = $row["mb_pay_yn"];
		$pharm_sido         = $row["pharm_sido"];
		$pharm_gugun        = $row["pharm_gugun"];
		$pharm_dong         = $row["pharm_dong"];
		$pharm_look         = $row["pharm_look"];
		$pharm_lat          = $row["pharm_lat"];
		$pharm_lng          = $row["pharm_lng"];

		$mb_div_str      = "";

		switch ( $pharm_div ) {
			case "1":
				$pharm_div_str = "태전약품";
				break;
			case "2":
				$pharm_div_str = "티제이팜";
				break;
		}

		$mb_pay_yn_str = "";

		switch ( $mb_pay_yn ) {
			case "Y" :
				$mb_pay_yn_str = "오더스테이션결제";
				break;
			case "N" :
				$mb_pay_yn_str = "직거래";
				break;
		}

		$mb_status_str = "";

		switch ( $mb_status ) {
			case "1" :
				$mb_status_str = "사용(대기)";
				break;
			case "2" :
				$mb_status_str = "사용(승인)";
				break;
			case "3" :
				$mb_status_str = "사용안함";
				break;
			case "n" :
				$mb_status_str = "신규가입";
				break;
			case "l" :
				$mb_status_str = "erp등록중";
				break;
		}

		//$mb_tel = callnumbercheck( $mb_tel );

		switch ( $pharm_look ) {
			case "0" :
				$pharm_look_str = "미선택";
				break;
			case "1" :
				$pharm_look_str = "지역";
				break;
			case "2" :
				$pharm_look_str = "전체";
				break;
		}

		$map_str = "등록";

		if ( $map_lat == "" ) {
			$map_str = "미등록";
		}

		$mb_control = $row["pharm_control"];
    ?>

    <tr class="<?php echo $bg; ?>">
        
        <td headers="mb_list_chk" class="td_chk" >
            <input type="hidden" name="mb_id[<?php echo $i ?>]" value="<?php echo $row['mb_id'] ?>" id="mb_id_<?php echo $i ?>">
            <label for="chk_<?php echo $i; ?>" class="sound_only"><?php echo get_text($row['mb_name']); ?> <?php echo get_text($row['mb_nick']); ?>님</label>
            <input type="checkbox" name="chk[]" value="<?php echo $i ?>" id="chk_<?php echo $i ?>">
        </td>
		<td align="center" width="50">
			<input name="mb_sort[<?php echo $i; ?>]" value="<?php echo $row['mb_sort'] ?>" style="width:50px" />
		</td>

		<td align="center"><?php echo $sales_name; ?></td>
		<td align="center"><?php echo $pharm_look_str; ?></td>
		<td align="center"><?php echo $pharm_sido; ?></td>
		<td align="center"
			<?php if ( $mb_control == "Y" ) { ?> style="color:#f00" <?php } ?>><?php echo $pharm_custno; ?></td>
		<td align="center"
			<?php if ( $mb_control == "Y" ) { ?> style="color:#f00" <?php } ?>><?php echo $pharm_name; ?></td>
		<td align="center"><?php echo $mb_hp; ?></td>
		<td align="center"><?php echo $mb_name; ?></td>
		<td align="center"><?php echo $mb_pay_yn_str; ?></td>
		<td align="center"><?php echo $pharm_div_str; ?></td>
		<td align="center"><?php echo $map_str; ?></td>
		<td align="center">웹(<a
					href="./member_list2.php?pharm_id=<?php echo $mb_id; ?>"> <?php echo $user_cnt; ?> </a>)/앱(<a
					href="./member_list3.php?pharm_id=<?php echo $mb_id; ?>"> <?php echo $user_cnt2; ?> </a>)
		</td>
		<td align="center"><?php echo $mb_status_str; ?></td>
        
		<td width="150">
			<?php echo $s_mod ?>
			<?php if($row['mb_type'] > 0) {?>
			<a href="javascript:fnLogin('<?php echo $row['mb_id'];?>');" class="btn btn_02">Login</a>
			<?php } ?>
		</td>

	</tr>

   <?php
    }
	?>
	<tr>

	  <td colspan=13 style="text-align: left;">
		
		<div class="local_cmd01 ">
		<input type="button" value="정렬저장" class="btn_submit" onclick="flist_submit();">   
		<input type="button" value="일괄승인" class="btn_submit" onclick="confirm_submit();"> 
		<input type="button" value="일괄사용안함" class="btn_submit" onclick="confirm_submit2();">   
		</div>
	
	  </td>
	</tr>
	<?php
    if ($i == 0)
        echo "<tr><td colspan=\"".$colspan."\" class=\"empty_table\">자료가 없습니다.</td></tr>";
    ?>
    </tbody>
    </table>
</div>


</form>

<?php 

echo get_paging(G5_IS_MOBILE ? $config['cf_mobile_pages'] : $config['cf_write_pages'], $page, $total_page, "{$_SERVER['SCRIPT_NAME']}?$qstr&amp;page="); ?>
<script>

//onChangeAddr(1,'');
//setTimeout(function(){ onChangeAddr(2,'');}, 100); 
function onChangeAddr(step, val){
	var sido = $("#pharm_sido");
	var gugun = $("#pharm_gugun");
	var dong = $("#pharm_dong");
	var obj = "";

	if(step == 1){
		obj = gugun;
		gugun.empty().data('option');
		gugun.append("<option value=''>시/군/구</option>");
	}
	if(step == 2){
		obj = dong; 
	}
	dong.empty().data('option');
	dong.append("<option value=''>동/면/읍</option>");
	$.ajax({
		type:"post",
		dataType:"xml",
		url: "/addr.php",
		data:{"mem_sido":sido.val(),"mem_gugun":gugun.val(), "step":step},
		success:function(data){
			var row= $(data).find("ROW").find("CELL");    
			var size=row.length;
			for(var i=0;i<size;i++){
				var cell =row.eq(i);
				$("<option></option>").text(cell.find("NAME").text()).attr("value",cell.find("NAME").text()).appendTo(obj);
			}

			if(step==1){
				gugun.val(val);
			}
			if(step==2){
				dong.val(val);
			}
		},
		error:function(request, status, errorThrown){
			//alert(errorThrown);
		}
	});
}



function formSubmit(type) {
	var form = "<form action='member_list1_01.php' method='post'>"; 
		form += "<input type='hidden' name='mode' value='excel1' />"; 
	form += "<input type='hidden' name='mb_referee' value='"+$('#mb_referee option:selected').val()+"' />"; 
	form += "<input type='hidden' name='pharm_control' value='"+$('#pharm_control option:selected').val()+"' />"; 
	form += "<input type='hidden' name='pharm_look' value='"+$('#pharm_look option:selected').val()+"' />"; 
	form += "<input type='hidden' name='pharm_search' value='"+$('#pharm_search option:selected').val()+"' />"; 
	form += "<input type='hidden' name='mb_pay_yn' value='"+$('#mb_pay_yn option:selected').val()+"' />"; 

	form += "<input type='hidden' name='pharm_div' value='"+$('#pharm_div option:selected').val()+"' />"; 
	form += "<input type='hidden' name='pharm_map' value='"+$('#pharm_map option:selected').val()+"' />"; 	
	form += "<input type='hidden' name='pharm_status' value='"+$('#pharm_status option:selected').val()+"' />"; 

	form += "<input type='hidden' name='pharm_sido' value='"+$('#pharm_sido option:selected').val()+"' />"; 
	form += "<input type='hidden' name='pharm_gugun' value='"+$('#pharm_gugun option:selected').val()+"' />"; 
	form += "<input type='hidden' name='pharm_dong' value='"+$('#pharm_dong option:selected').val()+"' />"; 

	form += "<input type='hidden' name='sfl' value='"+$('#sfl option:selected').val()+"' />"; 
	form += "<input type='hidden' name='stx' value='"+$('#stx').val()+"' />"; 
	form += "</form>"; 
	jQuery(form).appendTo("body").submit().remove(); 
}

function flist_submit()
{
    var f = document.fmemberlist;

	if (!is_checked("chk[]")) {
        alert("변경 하실 항목을 하나 이상 선택하세요.");
        return false;
    }

	f.target = "ifrm";
    f.action = "./member_list1_update.php";
	f.submit();
    return true;
}
function confirm_submit()
{
    var f = document.fmemberlist;

	if (!is_checked("chk[]")) {
        alert("승인 하실 항목을 하나 이상 선택하세요.");
        return false;
    }

	f.target = "ifrm";
    f.action = "./member_list1_status_update.php";
	f.submit();
    return true;
}
function confirm_submit2()
{
    var f = document.fmemberlist;

	if (!is_checked("chk[]")) {
        alert("사용안함 항목을 하나 이상 선택하세요.");
        return false;
    }

	f.target = "ifrm";
    f.action = "./member_list1_status_update2.php";
	f.submit();
    return true;
}
function fnLogin(id) {
	if(confirm(id+" 로 로그인하시겠습니까?")) {
		//location.href='/bbs/login_check_admin.php?mb_id=' + id;
		var url ='/bbs/login_check_admin.php?mb_id=' + id;
		window.open(url);
	}
}
</script>

<iframe name="ifrm" id="ifrm" frameborder="0" height="0" width="100%" hspace="0" marginheight="0" marginwidth="0" scrolling="no" src="" vspace="0"></iframe>

<?php
include_once ('./admin.tail.php');
?>

<?php
if (!defined('G5_USE_SHOP') || !G5_USE_SHOP) return;

$menu['menu400'] = array (
    array('400000', '쇼핑몰관리', G5_ADMIN_URL.'/shop_admin/', 'shop_config'),
    array('400100', '쇼핑몰설정', G5_ADMIN_URL.'/shop_admin/configform.php', 'scf_config'),
    array('400400', '전체주문현황', G5_ADMIN_URL.'/shop_admin/orderlist.php', 'scf_order', 1),
	array('400401', '주문접수', G5_ADMIN_URL.'/shop_admin/orderlist.php?od_status=주문', 'scf_order', 1),
	array('400402', '주문확정', G5_ADMIN_URL.'/shop_admin/orderlist.php?od_status=확정', 'scf_order', 1),
	array('400403', '배송중', G5_ADMIN_URL.'/shop_admin/orderlist.php?od_status=배송', 'scf_order', 1),
	array('400404', '배송완료', G5_ADMIN_URL.'/shop_admin/orderlist.php?od_status=완료', 'scf_order', 1),
	//array('400405', '결제확인', G5_ADMIN_URL.'/shop_admin/orderlistp.php', 'scf_order', 1),
	array('400406', '주문취소', G5_ADMIN_URL.'/shop_admin/orderlist.php?od_status=취소', 'scf_order', 1),
	array('400411', '전체반품현황', G5_ADMIN_URL.'/shop_admin/orderlist.php?od_status=반품전체', 'scf_order', 1),
	array('400412', '반품승인', G5_ADMIN_URL.'/shop_admin/orderlist.php?od_status=반품승인', 'scf_order', 1),
	/*array('400408', '반품접수', G5_ADMIN_URL.'/shop_admin/orderlist.php?od_status=반품접수', 'scf_order', 1),*/
	array('400409', '반품반려', G5_ADMIN_URL.'/shop_admin/orderlist.php?od_status=반품반려', 'scf_order', 1),
	array('400410', '반품완료', G5_ADMIN_URL.'/shop_admin/orderlist.php?od_status=반품완료', 'scf_order', 1),
	//array('400407', '사내주문', G5_ADMIN_URL.'/shop_admin/orderlistp1.php', 'scf_order', 1),

    //array('400440', '개인결제관리', G5_ADMIN_URL.'/shop_admin/personalpaylist.php', 'scf_personalpay', 1),
    /*array('400200', '분류관리', G5_ADMIN_URL.'/shop_admin/categorylist.php', 'scf_cate'),
	array('400250', '상품승인처리', G5_ADMIN_URL.'/shop_admin/itemlist_reg.php', 'scf_item_reg'),
    array('400300', '상품관리', G5_ADMIN_URL.'/shop_admin/itemlist.php', 'scf_item'),
    array('400660', '상품문의', G5_ADMIN_URL.'/shop_admin/itemqalist.php', 'scf_item_qna'),
    array('400650', '사용후기', G5_ADMIN_URL.'/shop_admin/itemuselist.php', 'scf_ps'),
    array('400620', '상품재고관리', G5_ADMIN_URL.'/shop_admin/itemstocklist.php', 'scf_item_stock'),
    array('400610', '상품유형관리', G5_ADMIN_URL.'/shop_admin/itemtypelist.php', 'scf_item_type'),
    array('400500', '상품옵션재고관리', G5_ADMIN_URL.'/shop_admin/optionstocklist.php', 'scf_item_option'),*/
    array('400800', '쿠폰관리', G5_ADMIN_URL.'/shop_admin/coupon_login_ok.php', 'scf_coupon'),
	array('400801', '페이퍼쿠폰관리', G5_ADMIN_URL.'/shop_admin/rand_coupon_login_ok.php', 'scf_coupon2'),
	//array('400900', '프로모션 쿠폰관리', G5_ADMIN_URL.'/shop_admin/event_couponlist.php', 'event_coupon'),
	//array('400800', '쿠폰관리', G5_ADMIN_URL.'/shop_admin/couponlist.php', 'scf_coupon'),
    //array('400810', '쿠폰존관리', G5_ADMIN_URL.'/shop_admin/couponzonelist.php', 'scf_coupon_zone'),
    //array('400750', '추가배송비관리', G5_ADMIN_URL.'/shop_admin/sendcostlist.php', 'scf_sendcost', 1),
    //array('400410', '미완료주문', G5_ADMIN_URL.'/shop_admin/inorderlist.php', 'scf_inorder', 1),
);

//최고관리자일때 만 사내주문 메뉴 보이게 
if( $member['mb_id'] == "admin" ){

	array_push($menu['menu400'], array('400407', '사내주문', G5_ADMIN_URL.'/shop_admin/orderlistp1.php', 'scf_order', 1) );
	array_push($menu['menu400'], array('400413', '정기결제 불가일자 설정', G5_ADMIN_URL.'/shop_admin/NoDaySetting.php', 'scf_order', 1) );
	array_push($menu['menu400'], array('400414', '정기주문 미결제 오류', G5_ADMIN_URL.'/shop_admin/bill_error.php', 'scf_order', 1) );

}
?>
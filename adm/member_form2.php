<?php
$sub_menu = "200100";
include_once('./_common.php');


$sql = "select * from tbl_member_user where ad_id = '$ad_id'";
$mb = sql_fetch($sql);

$required_mb_id = 'readonly';
$required_mb_password = '';
$html_title = '수정';


$g5['title'] .= '소비자회원관리 ';
include_once('./admin.head.php');

// add_javascript('js 구문', 출력순서); 숫자가 작을 수록 먼저 출력됨
add_javascript(G5_POSTCODE_JS, 0);    //다음 주소 js
?>
<form name="fmember" id="fmember" action="./member_form2_update.php" onsubmit="return fmember_submit(this);" method="post">
<input type="hidden" name="w" value="<?php echo $w ?>">
<input type="hidden" name="sfl" value="<?php echo $sfl ?>">
<input type="hidden" name="stx" value="<?php echo $stx ?>">
<input type="hidden" name="sst" value="<?php echo $sst ?>">
<input type="hidden" name="sod" value="<?php echo $sod ?>">
<input type="hidden" name="page" value="<?php echo $page ?>">
<input type="hidden" name="ad_id" value="<?php echo $mb['ad_id'] ?>">
<input type="hidden" name="token" value="">

<div class="tbl_frm01 tbl_wrap">
    <table>
    <caption><?php echo $g5['title']; ?></caption>
    <colgroup>
        <col class="grid_4">
        <col>
        <col class="grid_4">
        <col>
    </colgroup>
    <tbody>
    
    <tr>
        <th scope="row"><label for="ad_name">이름<strong class="sound_only">필수</strong></label></th>
        <td><input type="text" name="ad_name" value="<?php echo $mb['ad_name'] ?>" id="ad_name" required class="required frm_input" size="15"  maxlength="20"></td>

		<th scope="row"><label for="mb_id">담당약국ID<strong class="sound_only">필수</strong></label></th>
        <td><input type="text" name="mb_id" value="<?php echo $mb['mb_id'] ?>" id="mb_id" required class="required frm_input" size="15"  maxlength="20"></td>
       
    </tr>

    <tr>
        <th scope="row"><label for="ad_hp">휴대폰번호</label></th>
        <td><input type="text" name="ad_hp" value="<?php echo $mb['ad_hp'] ?>" id="ad_hp" class="frm_input" size="15" maxlength="20"></td>
        <th scope="row"><label for="ad_tel">전화번호</label></th>
        <td><input type="text" name="ad_tel" value="<?php echo $mb['ad_tel'] ?>" id="ad_tel" class="frm_input" size="15" maxlength="20"></td>
    </tr>
    <tr>
        <th scope="row">주소</th>
        <td colspan="3" class="td_addr_line">
            <label for="ad_zip" class="sound_only">우편번호</label>
            <input type="text" name="ad_zip" value="<?php echo $mb['ad_zip1'].$mb['ad_zip2']; ?>" id="ad_zip" class="frm_input readonly" size="5" maxlength="6">
            <button type="button" class="btn_frmline" onclick="win_zip('fmember', 'ad_zip', 'ad_addr1', 'ad_addr2', 'ad_addr3', 'ad_jibeon');">주소 검색</button><br>
            <input type="text" name="ad_addr1" value="<?php echo $mb['ad_addr1'] ?>" id="ad_addr1" class="frm_input readonly" size="60">
            <label for="ad_addr1">기본주소</label><br>
            <input type="text" name="ad_addr2" value="<?php echo $mb['ad_addr2'] ?>" id="ad_addr2" class="frm_input" size="60">
            <label for="ad_addr2">상세주소</label>
            <br>
            <input type="text" name="ad_addr3" value="<?php echo $mb['ad_addr3'] ?>" id="ad_addr3" class="frm_input" size="60">
            <label for="ad_addr3">참고항목</label>
            <input type="hidden" name="ad_jibeon" value="<?php echo $mb['ad_jibeon']; ?>"><br>
        </td>
    </tr>
    
   
    <tr>
        <th scope="row"><label for="ad_memo">메모</label></th>
        <td colspan="3"><textarea name="ad_memo" id="ad_memo"><?php echo $mb['ad_memo'] ?></textarea></td>
    </tr>

    <tr>
        <th scope="row">가입일</th>
        <td><?php echo $mb['ad_datetime'] ?></td>

    </tr>

  

    </tbody>
    </table>
	
	<div id="wrap_list">
											
	</div>

</div>

<div class="btn_fixed_top">

    <a href="./member_list2.php?<?php echo $qstr ?>" class="btn btn_02">목록</a>
	
    <input type="submit" value="확인" class="btn_submit btn" accesskey='s'> 
</div>
</form>


<script>

function fmember_submit(f)
{
    

    return true;
}

</script>
<?php
include_once('./admin.tail.php');
?>

<?php
$sub_menu = "200140";
include_once('./_common.php');

auth_check($auth[$sub_menu], 'r');



if(! preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $fr_date) ) $fr_date = '';
if(! preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $to_date) ) $to_date = '';
if($fr_date =='') $fr_date = date("Y-m-d", strtotime('-1 year'));
if($to_date =='') $to_date = date("Y-m-d");



$sql_common = " from {$g5['member_table']} a ";

$sql_search = " where (1) ";
if ($stx) {
    $sql_search .= " and ( ";
    switch ($sfl) {
        
        case 'mb_name' :
        case 'mb_id' :
            $sql_search .= " ({$sfl} like '%{$stx}%') ";
            break;
		case 'comp_name' :
		case 'comp_md_name' :
            $sql_search .= " ({$sfl} like '%{$stx}%') ";
            break;
        default :
            $sql_search .= " ({$sfl} like '{$stx}%') ";
            break;
    }
    $sql_search .= " ) ";
}
if ($is_admin != 'super')
    $sql_search .= " and mb_level <= '{$member['mb_level']}' ";

if ($pharm_status)
    $sql_search .= " and pharm_status = '$pharm_status' ";

//if ($_GET['mb_type'] != '')
    $sql_search .= " and mb_type = '7' ";

if (!$sst) {
    $sst = "mb_datetime";
    $sod = "desc";
}

$sql_order = " order by comp_charge_datetime_1 desc , {$sst} {$sod} ";

$sql = " select count(*) as cnt {$sql_common} {$sql_search} {$sql_order} ";

$row = sql_fetch($sql);
$total_count = $row['cnt'];

$rows = $config['cf_page_rows'];
$total_page  = ceil($total_count / $rows);  // 전체 페이지 계산
if ($page < 1) $page = 1; // 페이지가 없으면 첫 페이지 (1 페이지)
$from_record = ($page - 1) * $rows; // 시작 열을 구함


$listall = '<a href="'.$_SERVER['SCRIPT_NAME'].'" class="ov_listall">전체목록</a>';

$g5['title'] = '공급사관리';
include_once('./admin.head.php');

$sql = " select a.*
                ,(select count(*) from tbl_shop_item where comp_code = a.comp_code) item_count
		{$sql_common} {$sql_search} {$sql_order} limit {$from_record}, {$rows} ";
//echo $sql;

$result = sql_query($sql);

$colspan = 16;
?>

<div class="local_ov01 local_ov">
    <?php echo $listall ?>
    <span class="btn_ov01"><span class="ov_txt">총회원수 </span><span class="ov_num"> <?php echo number_format($total_count) ?>명 </span></span>
   
</div>

<form id="fsearch" name="fsearch" class="local_sch01 local_sch" method="get">


<div class="local_sch03  tbl_wrap" style="padding:10px;">
    <table>
   
    <tbody>
	
	<tr>
        
		<th scope="row">사용여부</th>
        <td>
           <select name="pharm_status" id="pharm_status" >
				<option value="">==전체==</option>
				
				<option value="2" <?php echo get_selected($pharm_status, '2'); ?>>사용</option>
				<option value="3" <?php echo get_selected($pharm_status, '3'); ?>>사용안함</option>
			</select>
        </td>
   
		<th scope="row">검색</th>
        <td>
            <select name="sfl" id="sfl" >
			<option value="comp_name" <?php echo get_selected($sfl, 'comp_name'); ?>>공급사명</option>
			<option value="comp_md_name" <?php echo get_selected($sfl, 'comp_md_name'); ?>>담당자명</option>
			<option value="mb_id" <?php echo get_selected($sfl, 'mb_id'); ?>>아이디</option>
			
			
			</select>
			<label for="stx" class="sound_only">검색어</label>
			<input type="text" name="stx" value="<?php echo $stx ?>" id="stx" class=" frm_input">
        </td>
		<td >
			 <input type="submit" value="검색" class="get_theme_confc btn btn_01">
			 <input type="button" value="엑셀 다운로드" class="get_theme_confc btn btn_02" style="cursor:pointer" onclick="formSubmit(1);">

		</td>
		
    </tr>
	
    </tbody>
    </table>
</div>



</form>

<!-- <div class="local_desc01 local_desc">
    <p>
        회원자료 삭제 시 다른 회원이 기존 회원아이디를 사용하지 못하도록 회원아이디, 이름, 닉네임은 삭제하지 않고 영구 보관합니다.
    </p>
</div> -->


<form name="fmemberlist" id="fmemberlist" action="" onsubmit="return fmemberlist_submit(this);" method="post">
<input type="hidden" name="sst" value="<?php echo $sst ?>">
<input type="hidden" name="sod" value="<?php echo $sod ?>">
<input type="hidden" name="sfl" value="<?php echo $sfl ?>">
<input type="hidden" name="stx" value="<?php echo $stx ?>">
<input type="hidden" name="page" value="<?php echo $page ?>">
<input type="hidden" name="token" value="">

<div class="tbl_head01 tbl_wrap">
    <table>
    <caption><?php echo $g5['title']; ?> 목록</caption>
    <thead>
    <tr>
        <th width="10"></th>
        <th scope="col" id="mb_list_id" >공급사명</th>
		<th scope="col" id="mb_list_id" >아이디</th>
		<th scope="col" id="mb_list_name">대표자명</th>
        <th scope="col" id="mb_list_nick">담당자명</th>
		<th scope="col" id="mb_list_nick">담당연락처</th>		
		<th scope="col" id="mb_list_auth">상품수</th>		

        <th scope="col" id="mb_list_lastcall">가입일</a></th>
		<th scope="col" id="mb_list_mng">관리</th>
		<th scope="col" id="mb_list_mng">승인요청 여부</th>
    </tr>
    </thead>
    <tbody>
    <?php
    for ($i=0; $row=sql_fetch_array($result); $i++) {

        // 접근가능한 그룹수
        $sql2 = " select count(*) as cnt from {$g5['group_member_table']} where mb_id = '{$row['mb_id']}' ";
        $row2 = sql_fetch($sql2);
        $group = '';
        if ($row2['cnt'])
            $group = '<a href="./boardgroupmember_form.php?mb_id='.$row['mb_id'].'">'.$row2['cnt'].'</a>';

        if ($is_admin == 'group') {
            $s_mod = '';
        } else {
            $s_mod = '<a href="./member_form.php?'.$qstr.'&amp;w=u&amp;mb_id='.$row['mb_id'].'" class="btn btn_03">수정</a>';
        }
        $s_grp = '<a href="./boardgroupmember_form.php?mb_id='.$row['mb_id'].'" class="btn btn_02">그룹</a>';

        $leave_date = $row['mb_leave_date'] ? $row['mb_leave_date'] : date('Ymd', G5_SERVER_TIME);
        $intercept_date = $row['mb_intercept_date'] ? $row['mb_intercept_date'] : date('Ymd', G5_SERVER_TIME);

        $leave_msg = '';
        $intercept_msg = '';
        $intercept_title = '';
        if ($row['mb_leave_date']) {
            $mb_id = $mb_id;
            $leave_msg = '<span class="mb_leave_msg">탈퇴함</span>';
        }
        else if ($row['mb_intercept_date']) {
            $mb_id = $mb_id;
            $intercept_msg = '<span class="mb_intercept_msg">차단됨</span>';
            $intercept_title = '차단해제';
        }
        if ($intercept_title == '')
            $intercept_title = '차단하기';

    
        $bg = 'bg'.($i%2);

		$mb_id           = $row["mb_id"];
		$comp_ceo_name         = $row["comp_ceo_name"];
		$comp_md_name          = $row["comp_md_name"];
		$comp_md_tel          = $row["comp_md_tel"];
		$comp_name          = $row["comp_name"];
		$comp_code          = $row["comp_code"];
	
		$item_count          = $row["item_count"];
		$mb_datetime          = substr($row["mb_datetime"], 0, 10);
		
    ?>

    <tr class="<?php echo $bg; ?>">
        
        <td ></td>
		<td align="center"><?php echo $comp_name; ?>(<?php echo $comp_code; ?>)</td>
		<td align="center"><?php echo $mb_id; ?></td>
		<td align="center"><?php echo $comp_ceo_name; ?></td>
		<td align="center"><?php echo $comp_md_name; ?></td>
		<td align="center"><?php echo $comp_md_tel; ?></td>
		
		
		<td align="center">
			<a href="/adm/shop_admin/itemlist.php?comp_code=<?php echo $comp_code; ?>"><?php echo $item_count; ?></a>
		</td>

		<td align="center"><?php echo $mb_datetime; ?></td>
		
		<td width="150">
			<?php echo $s_mod ?>
			<?php if($row['mb_type'] > 0) {?>
			<a href="javascript:fnLogin('<?php echo $row['mb_id'];?>');" class="btn btn_02">Login</a>
			<?php } ?>
		</td>
		<td width="120">
			<?php if( $row['comp_change_status'] == '0' && ( $row['comp_charge_datetime_1'] > $row['comp_charge_datetime_2'] ) ) {?>
			 <font color='red'>승인요청</font>
			<?php } ?>			
		</td>
	</tr>

    <?php
    }
    if ($i == 0)
        echo "<tr><td colspan=\"".$colspan."\" class=\"empty_table\">자료가 없습니다.</td></tr>";
    ?>
    </tbody>
    </table>
</div>


</form>

<?php 
//$qstr ='mb_type='.$_GET['mb_type'].'&'.$qstr;
echo get_paging(G5_IS_MOBILE ? $config['cf_mobile_pages'] : $config['cf_write_pages'], $page, $total_page, '?'.$qstr.'&amp;page='); ?>

<script>

function formSubmit(type) {
	var form = "<form action='member_list5_01.php' method='post'>"; 
		form += "<input type='hidden' name='mode' value='excel' />"; 
		form += "<input type='hidden' name='pharm_status' value='"+$('#pharm_status option:selected').val()+"' />"; 
		form += "<input type='hidden' name='sfl' value='"+$('#sfl option:selected').val()+"' />"; 
		form += "<input type='hidden' name='stx' value='"+$('#stx').val()+"' />"; 
		form += "</form>"; 
	jQuery(form).appendTo("body").submit().remove(); 
}

function fnLogin(id) {
	if(confirm(id+" 로 로그인하시겠습니까?")) {
		location.href='/bbs/login_check_admin.php?mb_id=' + id;
	}
}
</script>

<?php
include_once ('./admin.tail.php');
?>

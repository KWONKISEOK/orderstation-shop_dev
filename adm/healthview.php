<?php
$sub_menu = '300840';
include_once('./_common.php');
include_once(G5_EDITOR_LIB);

auth_check($auth[$sub_menu], "w");



$html_title = "공급사정보공유";
$g5['title'] = $html_title.' 내용보기';


    $sql = " select * from tbl_write_news where wr_id = '$wr_id' ";
    $wr = sql_fetch($sql);
    if (!$wr['wr_id'])
        alert('등록된 자료가 없습니다.');


include_once (G5_ADMIN_PATH.'/admin.head.php');
?>

<form name="frmcontentform" action="./healthformupdate.php" onsubmit="return frmcontentform_check(this);" method="post" enctype="MULTIPART/FORM-DATA" >

<div class="tbl_frm01 tbl_wrap">
    <table>
    <caption><?php echo $g5['title']; ?> </caption>
    <colgroup>
        <col class="grid_4">
        <col>
    </colgroup>
    <tbody>

    <tr>
        <th scope="row"><label for="wr_subject">제목</label></th>
        <td><?php echo htmlspecialchars2($wr['wr_subject']); ?></td>
    </tr>
    <tr>
        <th scope="row">내용</th>
        <td><?php echo  nl2br($wr['wr_content']); ?></td>
    </tr>
   
    </tbody>
    </table>
</div>

<div class="btn_fixed_top">
    <a href="./healthlist.php" class="btn btn_02">목록</a>
</div>

</form>

<?php
// [KVE-2018-2089] 취약점 으로 인해 파일 경로 수정시에만 자동등록방지 코드 사용
?>
<script>


<?php
include_once (G5_ADMIN_PATH.'/admin.tail.php');
?>

<?php
include_once('./_common.php');

$mode	= $_POST['mode'];

$sql_common = " from tbl_member_user a, tbl_member b ";

$sql_search = " where (1) ";
if ($stx) {
    $sql_search .= " and ( ";
    switch ($sfl) {
        
        case 'mb_name' :
        case 'pharm_custno' :
            $sql_search .= " ({$sfl} like '%{$stx}') ";
            break;
		case 'pharm_name' :
            $sql_search .= " ({$sfl} like '%{$stx}%') ";
            break;
        default :
            $sql_search .= " ({$sfl} like '{$stx}%') ";
            break;
    }
    $sql_search .= " ) ";
}

$sql_search .= " and a.mb_id = b.mb_id   ";

//if ($pharm_id) {
//    $sql_search .= " and a.mb_id = '$pharm_id' ";
//}


switch($mode){
	
	case 'excel':
		$ExcelTitle = urlencode( "약국소비자회원" . date( "YmdHis" ) );
		header( "Content-type: application/vnd.ms-excel" );
		header( "Content-type: application/vnd.ms-excel; charset=utf-8" );
		header( "Content-Disposition: attachment; filename = " . $ExcelTitle . ".xls" );
		header( "Content-Description: PHP4 Generated Data" );
		print( "<meta http-equiv=\"Content-Type\" content=\"application/vnd.ms-excel; charset=utf-8\">" ); ?>
		<table border="1">
		<tr bgcolor="F7F7F7" height="30">
			<th>담당약국</th>
			<th>약국코드</th>
			<th>이름</th>
			<th>전화번호</th>
			<th>핸드폰번호</th>
			<th>우편번호</th>
			<th>주소1</th>
			<th>주소2</th>
			<th>약국시도</th>
			<th>약국구군</th>
			<th>약국동읍면</th>
			<th>등록일</th>
		</tr>
		<?php		
		$sql = " select a.* 
					, b.pharm_name
					, b.pharm_custno
					, b.pharm_sido
					, b.pharm_gugun
					, b.pharm_dong
				$sql_common  ".$sql_search ." order by ad_datetime desc";

		$res = sql_query($sql);
		if(!$res) exit('Cannot run query.');

		$i = 0;
		while($row = mysqli_fetch_array($res,MYSQL_ASSOC)) {

			?>
			<tr bgcolor="">
				<td style="mso-number-format:\@"><?php echo $row["pharm_name"];?></td>
				<td style="mso-number-format:\@"><?php echo $row["pharm_custno"];?></td>
				<td style="mso-number-format:\@"><?php echo $row["ad_name"];?></td>
				
				<td style="mso-number-format:\@"><?php echo $row["ad_tel"];?></td>
				<td style="mso-number-format:\@"><?php echo $row["ad_hp"];?></td>

				<td style="mso-number-format:\@"><?php echo $row["ad_zip1"];?> <?php echo $row["ad_zip2"];?> </td>
				<td style="mso-number-format:\@"><?php echo $row["ad_addr1"];?></td>
				<td style="mso-number-format:\@"><?php echo $row["ad_addr2"];?></td>
				<td style="mso-number-format:\@"><?php echo $row["pharm_sido"];?></td>
				<td style="mso-number-format:\@"><?php echo $row["pharm_gugun"];?></td>
				<td style="mso-number-format:\@"><?php echo $row["pharm_dong"];?></td>
				
				<td style="mso-number-format:\@"><?php echo substr($row["ad_datetime"], 0, 10);?></td>
			</tr>
		<?php			
		} 
		break;
	
}



	

?>
<?php
$sub_menu = '600650';
include_once('./_common.php');

check_demo();

check_admin_token();

if (!count($_POST['chk'])) {
    alert($_POST['act_button']." 하실 항목을 하나 이상 체크하세요.");
}

if ($_POST['act_button'] == "선택수정") {
    auth_check($auth[$sub_menu], 'w');
} else if ($_POST['act_button'] == "선택삭제") {
    auth_check($auth[$sub_menu], 'd');
} else {
    alert("선택수정이나 선택삭제 작업이 아닙니다.");
}

/*************** 트랜잭션 관련 ****************/
$error_cnt = 0;
mysqli_autocommit($g5['connect_db'], false);
/*************** 트랜잭션 관련 ****************/

for ($i=0; $i<count($_POST['chk']); $i++)
{
    $k = $_POST['chk'][$i]; // 실제 번호를 넘김

    if ($_POST['act_button'] == "선택수정")
    {
        $sql = "update {$g5['g5_shop_item_use_table']}
                   set is_score   = '{$_POST['is_score'][$k]}',
                       is_confirm = '{$_POST['is_confirm'][$k]}'
                 where is_id      = '{$_POST['is_id'][$k]}' ";
        sql_query($sql);
		/*************** 트랜잭션 관련 ****************/
		if( mysqli_errno($g5['connect_db']) ){
			$error_cnt += 1;
		}
		/*************** 트랜잭션 관련 ****************/
    }
    else if ($_POST['act_button'] == "선택삭제")
    {
        $sql = "delete from {$g5['g5_shop_item_use_table']} where is_id = '{$_POST['is_id'][$k]}' ";
        sql_query($sql);
		/*************** 트랜잭션 관련 ****************/
		if( mysqli_errno($g5['connect_db']) ){
			$error_cnt += 1;
		}
		/*************** 트랜잭션 관련 ****************/
    }

    update_use_cnt($_POST['it_id'][$k]);
    update_use_avg($_POST['it_id'][$k]);
}

/*************** 트랜잭션 관련 ****************/
if ($error_cnt > 0) {
	mysqli_rollback($g5['connect_db']);
	mysqli_close($g5['connect_db']);
	echo "
	<script>
		alert('데이터베이스의 에러로 인해 롤백되었습니다.');
		history.back();
	</script>
	";
	exit;
} else {
	mysqli_commit($g5['connect_db']);
} 	
/*************** 트랜잭션 관련 ****************/

goto_url("./itemuselist.php?sca=$sca&amp;sst=$sst&amp;sod=$sod&amp;sfl=$sfl&amp;stx=$stx&amp;page=$page");
?>

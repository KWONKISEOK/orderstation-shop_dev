<?php
$sub_menu = '600300';
include_once('./_common.php');

auth_check($auth[$sub_menu], "r");

$g5['title'] = '상품관리';
include_once (G5_ADMIN_PATH.'/admin.head.php');
include_once(G5_PLUGIN_PATH.'/jquery-ui/datepicker.php');

// 분류
/*
$ca_list  = '<option value="">선택</option>'.PHP_EOL;
$sql = " select * from {$g5['g5_shop_category_table']} ";
if ($is_admin != 'super')
    $sql .= " where ca_mb_id = '{$member['mb_id']}' ";
$sql .= " order by ca_order, ca_id ";
$result = sql_query($sql);
for ($i=0; $row=sql_fetch_array($result); $i++)
{
    $len = strlen($row['ca_id']) / 2 - 1;
    $nbsp = '';
    for ($i=0; $i<$len; $i++) {
        $nbsp .= '&nbsp;&nbsp;&nbsp;';
    }
    $ca_list .= '<option value="'.$row['ca_id'].'">'.$nbsp.$row['ca_name'].'</option>'.PHP_EOL;
}
*/

$where = " and ";
$sql_search = "";
if ($stx != "") {
    if ($sfl != "it_id") {
        $sql_search .= " $where $sfl like '%$stx%' ";
        $where = " and ";
    }
	else if ($sfl == "it_id") { 

    if ($sfl != "") {
		
		$sql_search .=" $where instr('$stx',$sfl)";
		/*$add1="'%";
		$add2="%'";
		$stx1 = str_replace(" ","%' or it_id like '%",$stx); 
		$stx2 = $add1.$stx1.$add2.")";
        $sql_search .= " $where ( $sfl like $stx2 ";*/
        $where = " and ";
    }
}
    if ($save_stx != $stx)
        $page = 1;
}


if ($sca != "") {
    $sql_search .= " $where (a.ca_id like '$sca%' or a.ca_id2 like '$sca%' or a.ca_id3 like '$sca%') ";
}

if ($sfl == "")  $sfl = "a.it_name";

$sql_common = " from {$g5['g5_shop_item_table']} a 
                     left join {$g5['g5_shop_category_table']} b on b.ca_id = a.ca_id
					 left join tbl_member c on c.comp_code = a.comp_code
               where c.mb_type = 7";

if( $s_comp_code != '' ){
	$comp_code = $s_comp_code;
}
if ($comp_code != '')
$sql_common .= " and a.comp_code = '$comp_code' ";

if ($it_use != '')
$sql_common .= " and a.it_use ='$it_use' ";

$sql_common .= " and a.it_status > 2 ";

if ($it_soldout != '') 
$sql_common .= " and a.it_soldout ='$it_soldout' ";

if( $s_it_main1 != "" ){
	$sql_common .= " and a.it_main1 = '1' ";
}
if( $s_it_main2 != "" ){
	$sql_common .= " and a.it_main2 = '1' ";
}
if( $s_it_main3 != "" ){
	$sql_common .= " and a.it_main3 = '1' ";
}
if( $s_it_type1 != "" ){
	$sql_common .= " and a.it_type1 = '1' ";
}
if( $s_it_type2 != "" ){
	$sql_common .= " and a.it_type2 = '1' ";
}
if( $s_it_type3 != "" ){
	$sql_common .= " and a.it_type3 = '1' ";
}
if( $s_it_type4 != "" ){
	$sql_common .= " and a.it_type4 = '1' ";
}
if( $s_it_service_view_1 != "" ){
	$sql_common .= " and a.it_service_view_1 = '1' ";
}
if( $s_it_service_view_2 != "" ){
	$sql_common .= " and a.it_service_view_2 = '1' ";
}
if( $s_it_time != "" ){
	$sql_common .= " and DATE(a.it_time) >= '".$s_it_time."' ";
}
if( $s_it_crm_yn != "" ){
	$sql_common .= " and a.it_crm_yn = '".$s_it_crm_yn."' ";
}

$sql_common .= $sql_search;

// 테이블의 전체 레코드수만 얻음
$sql = " select count(a.it_id) as cnt " . $sql_common;
$row = sql_fetch($sql);
$total_count = $row['cnt'];

$rows = $config['cf_page_rows'];
$total_page  = ceil($total_count / $rows);  // 전체 페이지 계산
if ($page < 1) { $page = 1; } // 페이지가 없으면 첫 페이지 (1 페이지)
$from_record = ($page - 1) * $rows; // 시작 열을 구함

if (!$sst) {
    $sst  = "a.it_time";
    $sod = "desc";
}
$sql_order = "order by $sst $sod";

$sql  = " select a.it_id , a.it_name , c.comp_name , a.it_use , a.it_soldout , a.it_main1 , a.it_main2 , a.it_main3 , 
				 a.it_type1 , a.it_type2 , a.it_type3 , a.it_type4 , a.it_service_view_1, a.it_service_view_2 , 
				 a.it_crm_yn , a.it_price , a.it_drug_price , a.it_incen , a.it_cust_price , a.it_stock_qty , a.ca_id
           $sql_common
           $sql_order
           limit $from_record, $rows ";
$result = sql_query($sql);
//echo $sql;

/*
echo "<pre>".$sql."</pre>";
exit;
*/	


//$qstr  = $qstr.'&amp;sca='.$sca.'&amp;page='.$page;
$qstr  = $qstr.'&amp;s_comp_code='.$comp_code.'&amp;sca='.$sca.'&amp;page='.$page.'&amp;save_stx='.$stx;
$qstr  = $qstr.'&amp;s_it_main1='.$s_it_main1.'&amp;s_it_main2='.$s_it_main2.'&amp;s_it_main3='.$s_it_main3.'&amp;s_it_crm_yn='.$s_it_crm_yn;
$qstr  = $qstr.'&amp;s_it_type1='.$s_it_type1.'&amp;s_it_type2='.$s_it_type2.'&amp;s_it_type3='.$s_it_type3.'&amp;s_it_type4='.$s_it_type4.'&amp;s_it_time='.$s_it_time.'&amp;it_use='.$it_use.'&amp;it_soldout='.$it_soldout.'&amp;it_service_view_1='.$it_service_view_1.'&amp;it_service_view_2='.$it_service_view_2;

$listall = '<a href="'.$_SERVER['SCRIPT_NAME'].'" class="ov_listall">전체목록</a>';
?>

<div class="local_ov01 local_ov">
    <?php echo $listall; ?>
    <span class="btn_ov01"><span class="ov_txt">등록된 상품</span><span class="ov_num"> <?php echo $total_count; ?>건</span></span>
</div>

<form name="flist" class="local_sch01 local_sch">
<input type="hidden" name="page" value="<?php echo $page; ?>">
<input type="hidden" name="save_stx" value="<?php echo $stx; ?>">


<div class="local_sch03  tbl_wrap" style="padding:10px;">
    <table>
   
    <tbody>
	<tr>
        <th scope="row">업체선택</th>
        <td>
			<select name="comp_code" id="comp_code" style="width:200px;" onchange="this.form.submit()">
				<option value="">==업체선택==</option>
				<?php
				$sql2 = " select comp_code, comp_name from tbl_member where mb_type='7' order by comp_name ";
				$result2 = sql_query($sql2);
				for ($i=0; $row2=sql_fetch_array($result2); $i++) {
				   if($comp_code ==$row2['comp_code']) 
						$checked='selected';
					else 
						$checked ='';

					echo '<option value="'.$row2['comp_code'].'" '.$checked.' >'.$row2['comp_name'].'</option>'.PHP_EOL;
				}
				?>
			</select>
			
        </td>
		<th scope="row">판매여부</th>
        <td>
            <select name="it_use" id="it_use" onchange="this.form.submit()" >
				<option value="">==전체==</option>
				<option value="1" <?php echo get_selected($it_use, '1'); ?>>예</option>
				<option value="0" <?php echo get_selected($it_use, '0'); ?>>아니오</option>
			</select>
        </td>

		<th scope="row">상품분류</th>
        <td>
           <select name="sca" id="sca">
				<option value="">전체분류</option>
				<?php
				$sql1 = " select ca_id, ca_name from {$g5['g5_shop_category_table']} order by ca_order, ca_id ";
				$result1 = sql_query($sql1);
				for ($i=0; $row1=sql_fetch_array($result1); $i++) {
					$len = strlen($row1['ca_id']) / 2 - 1;
					$nbsp = '';
					for ($i=0; $i<$len; $i++) $nbsp .= '&nbsp;&nbsp;&nbsp;';
					echo '<option value="'.$row1['ca_id'].'" '.get_selected($sca, $row1['ca_id']).'>'.$nbsp.$row1['ca_name'].'</option>'.PHP_EOL;
				}
				?>
			</select>
        </td>

		<th scope="row">메인화면노출</th>
        <td>
          <input type="checkbox" name="s_it_main1" id="s_it_main1" <?php echo ($s_it_main1 ? 'checked' : ''); ?> value="1"/>베스트
          <input type="checkbox" name="s_it_main2" id="s_it_main2" <?php echo ($s_it_main2 ? 'checked' : ''); ?> value="1"/>추천상품
          <input type="checkbox" name="s_it_main3" id="s_it_main3" <?php echo ($s_it_main3 ? 'checked' : ''); ?> value="1"/>신상품
        </td>
		
		<th scope="row">CRM 재고관리<br>대상제품</th>
        <td>
          <input type="text" name="s_it_crm_yn" id="s_it_crm_yn" value="<?=$s_it_crm_yn?>" class="frm_input" maxlength="1" style="width:80px; margin-right:35px;"/>
        </td>


		<td>
			 <input type="submit" value="검색" class="get_theme_confc btn btn_01">
			 <input type="button" value="엑셀 다운로드" class="get_theme_confc btn btn_02" style="cursor:pointer" onclick="formSubmit(1, 1);">
		</td>

    </tr>
	<tr>
		<th scope="row">상품등록일자<br>(검색일 이후 등록상품)</th>
        <td>
          <input type="text" name="s_it_time" id="s_it_time" value="<?php echo $s_it_time; ?>" class="frm_input" size="23" maxlength="10">
        </td>
		
		<th scope="row">품절여부</th>
        <td>
            <select name="it_soldout" id="it_soldout" onchange="this.form.submit()" >
				<option value="">==전체==</option>
				<option value="1" <?php echo get_selected($it_soldout, '1'); ?>>예</option>
				<option value="0" <?php echo get_selected($it_soldout, '0'); ?>>아니오</option>
			</select>
        </td>
		<th scope="row">검색</th>
        <td>
            <select name="sfl" id="sfl">
				<option value="it_name" <?php echo get_selected($sfl, 'it_name'); ?>>상품명</option>
				<option value="it_id" <?php echo get_selected($sfl, 'it_id'); ?>>상품코드</option>
				<option value="it_maker" <?php echo get_selected($sfl, 'it_maker'); ?>>제조사</option>
			</select>
			<label for="stx" class="sound_only">검색어</label>
			<input type="text" name="stx" value="<?php echo $stx ?>" id="stx" class=" frm_input" style="width:300px";>
        </td>
		
		<th scope="row">뱃지달기</th>
        <td>
          <input type="checkbox" name="s_it_type1" id="s_it_type1" <?php echo ($s_it_type1 ? 'checked' : ''); ?> value="1"/>베스트
          <input type="checkbox" name="s_it_type2" id="s_it_type2" <?php echo ($s_it_type2 ? 'checked' : ''); ?> value="1"/>추천상품
          <input type="checkbox" name="s_it_type3" id="s_it_type3" <?php echo ($s_it_type3 ? 'checked' : ''); ?> value="1"/>신상품
          <input type="checkbox" name="s_it_type4" id="s_it_type4" <?php echo ($s_it_type4 ? 'checked' : ''); ?> value="1"/>타임세일
        </td>
		<th scope="row">이즈브레앱노출</th>
        <td>
          <input type="checkbox" name="s_it_service_view_1" id="s_it_service_view_1" <?php echo ($s_it_service_view_1 ? 'checked' : ''); ?> value="1"/>메인
          <input type="checkbox" name="s_it_service_view_2" id="s_it_service_view_2" <?php echo ($s_it_service_view_2 ? 'checked' : ''); ?> value="1"/>전체상품
        </td>		
		

	</tr>
	
    </tbody>
    </table>
</div>

</form>

<form name="fitemlistupdate" method="post" action="./itemlistupdate.php" onsubmit="return fitemlist_submit(this);" autocomplete="off" id="fitemlistupdate">
<input type="hidden" name="sca" value="<?php echo $sca; ?>">
<input type="hidden" name="sst" value="<?php echo $sst; ?>">
<input type="hidden" name="sod" value="<?php echo $sod; ?>">
<input type="hidden" name="sfl" value="<?php echo $sfl; ?>">
<input type="hidden" name="stx" value="<?php echo $stx; ?>">
<input type="hidden" name="page" value="<?php echo $page; ?>">
<input type="hidden" name="s_it_main1" value="<?php echo $s_it_main1; ?>">
<input type="hidden" name="s_it_main2" value="<?php echo $s_it_main2; ?>">
<input type="hidden" name="s_it_main3" value="<?php echo $s_it_main3; ?>">
<input type="hidden" name="s_it_type1" value="<?php echo $s_it_type1; ?>">
<input type="hidden" name="s_it_type2" value="<?php echo $s_it_type2; ?>">
<input type="hidden" name="s_it_type3" value="<?php echo $s_it_type3; ?>">
<input type="hidden" name="s_it_type4" value="<?php echo $s_it_type4; ?>">
<input type="hidden" name="s_it_service_view_1" value="<?php echo $s_it_service_view_1; ?>">
<input type="hidden" name="s_it_service_view_2" value="<?php echo $s_it_service_view_2; ?>">
<input type="hidden" name="s_it_time" value="<?php echo $s_it_time; ?>">
<input type="hidden" name="s_it_crm_yn" value="<?php echo $s_it_crm_yn; ?>">
<input type="hidden" name="s_comp_code" value="<?php echo $comp_code; ?>">

<div class="tbl_head01 tbl_wrap">
    <table>
    <caption><?php echo $g5['title']; ?> 목록</caption>
    <thead>
    <tr>
        <th scope="col">
            <label for="chkall" class="sound_only">상품 전체</label>
            <input type="checkbox" name="chkall" value="1" id="chkall" onclick="check_all(this.form)">
        </th>
        <th scope="col"><?php echo subject_sort_link('it_id', 'sca='.$sca); ?>상품코드</a></th>
		<th scope="col">공급사명</th>
        <th scope="col" ><?php echo subject_sort_link('it_use', 'sca='.$sca, 1); ?>판매</a></th>
        <th scope="col"><?php echo subject_sort_link('it_soldout', 'sca='.$sca, 1); ?>품절</a></th>
		<th scope="col">메인화면노출</a></th>
		<th scope="col">뱃지달기</a></th>
		<th scope="col">이즈브레앱노출</a></th>
		<th scope="col" width="70">CRM<br>재고관리<br>대상</a></th>
		<!-- <th scope="col">MD추천상품</a></th> -->
   
        <th scope="col" id="th_img">이미지</th>
        <th scope="col" id="th_pc_title"><?php echo subject_sort_link('it_name', 'sca='.$sca); ?>상품명</a></th>
        <th scope="col" id="th_amt"><?php echo subject_sort_link('it_price', 'sca='.$sca); ?>판매가</a></th>
        <!-- <th scope="col" id="th_camt"><?php echo subject_sort_link('it_cust_price', 'sca='.$sca); ?>시중가격</a></th> -->

        <!-- <th scope="col" id="th_pt"><?php echo subject_sort_link('it_point', 'sca='.$sca); ?>포인트</a></th> -->
        <th scope="col" id="th_qty"><?php echo subject_sort_link('it_stock_qty', 'sca='.$sca); ?>재고</a></th>

		<th scope="col">관리</th>
        
    </tr>
    </thead>
    <tbody>
    <?php
    for ($i=0; $row=sql_fetch_array($result); $i++)
    {
        $href = G5_SHOP_URL.'/item.php?it_id='.$row['it_id'];
        $bg = 'bg'.($i%2);


    ?>
    <tr class="<?php echo $bg; ?>">
        <td class="td_chk">
            <label for="chk_<?php echo $i; ?>" class="sound_only"><?php echo get_text($row['it_name']); ?></label>
            <input type="checkbox" name="chk[]" value="<?php echo $i ?>" id="chk_<?php echo $i; ?>">
        </td>
        <td class="td_num">
            <input type="hidden" name="it_id[<?php echo $i; ?>]" value="<?php echo $row['it_id']; ?>">
            <?php echo $row['it_id']; ?>
        </td>
      
        <td>
           <?php echo $row['comp_name']; ?>
        </td>
        <td>
            <label for="use_<?php echo $i; ?>" class="sound_only">판매여부</label>
            <input type="checkbox" name="it_use[<?php echo $i; ?>]" <?php echo ($row['it_use'] ? 'checked' : ''); ?> value="1" id="use_<?php echo $i; ?>">


        </td>
        <td>
            <label for="soldout_<?php echo $i; ?>" class="sound_only">품절</label>
            <input type="checkbox" name="it_soldout[<?php echo $i; ?>]" <?php echo ($row['it_soldout'] ? 'checked' : ''); ?> value="1" id="soldout_<?php echo $i; ?>">
        </td>
		<td style="text-align:left;">
            <input type="checkbox" name="it_main1[<?php echo $i; ?>]" <?php echo ($row['it_main1'] ? 'checked' : ''); ?> value="1" id="main1_<?php echo $i; ?>"><label for="main1_<?php echo $i; ?>">베스트</label><br/>
            <input type="checkbox" name="it_main2[<?php echo $i; ?>]" <?php echo ($row['it_main2'] ? 'checked' : ''); ?> value="1" id="main2_<?php echo $i; ?>"><label for="main2_<?php echo $i; ?>">추천상품</label><br/>
            <input type="checkbox" name="it_main3[<?php echo $i; ?>]" <?php echo ($row['it_main3'] ? 'checked' : ''); ?> value="1" id="main3_<?php echo $i; ?>"><label for="main3_<?php echo $i; ?>">신상품</label>
        </td>
		<td style="text-align:left;">
            <input type="checkbox" name="it_type1[<?php echo $i; ?>]" <?php echo ($row['it_type1'] ? 'checked' : ''); ?> value="1" id="type1_<?php echo $i; ?>"><label for="type1_<?php echo $i; ?>">베스트</label><br/>
            <input type="checkbox" name="it_type2[<?php echo $i; ?>]" <?php echo ($row['it_type2'] ? 'checked' : ''); ?> value="1" id="type2_<?php echo $i; ?>"><label for="type2_<?php echo $i; ?>">추천상품</label><br/>
            <input type="checkbox" name="it_type3[<?php echo $i; ?>]" <?php echo ($row['it_type3'] ? 'checked' : ''); ?> value="1" id="type3_<?php echo $i; ?>"><label for="type3_<?php echo $i; ?>">신상품</label><br/>
			<input type="checkbox" name="it_type4[<?php echo $i; ?>]" <?php echo ($row['it_type4'] ? 'checked' : ''); ?> value="1" id="type4_<?php echo $i; ?>"><label for="type4_<?php echo $i; ?>">타임세일</label>
        </td>
		<td style="text-align:left;">
            <input type="checkbox" name="it_service_view_1[<?php echo $i; ?>]" <?php echo ($row['it_service_view_1'] ? 'checked' : ''); ?> value="1" id="type1_<?php echo $i; ?>"><label for="it_service_view_1_<?php echo $i; ?>">메인</label><br/>
            <input type="checkbox" name="it_service_view_2[<?php echo $i; ?>]" <?php echo ($row['it_service_view_2'] ? 'checked' : ''); ?> value="1" id="type2_<?php echo $i; ?>"><label for="it_service_view_2_<?php echo $i; ?>">전체상품</label><br/>
        </td>   
		<td>
            <input type="text" name="it_crm_yn[<?php echo $i; ?>]" value="<?=$row['it_crm_yn']?>" id="crm_yn_<?php echo $i; ?>" class="frm_input" maxlength="1"  style="width:35px;"><label for="crm_yn_<?php echo $i; ?>"></label>
        </td>         
       
    
        <td class="td_img"><a href="<?php echo $href; ?>"><?php echo get_it_image($row['it_id'], 50, 50); ?></a></td>
        <td headers="th_pc_title" style="text-align:left;">           
            <?php echo htmlspecialchars2(cut_str($row['it_name'],250, "")); ?>
        </td>
        <td headers="th_amt" class="td_numbig td_input">
            <?php echo number_format($row['it_price']); ?><br>
			<font color="red"><?php echo number_format($row['it_drug_price']); ?></font><br>
			<?php echo number_format($row['it_incen']); ?>
        </td>
        <!-- <td headers="th_camt" class="td_numbig td_input">
            <label for="cust_price_<?php echo $i; ?>" class="sound_only">시중가격</label>
            <input type="text" name="it_cust_price[<?php echo $i; ?>]" value="<?php echo $row['it_cust_price']; ?>" id="cust_price_<?php echo $i; ?>" class="tbl_input sit_camt" size="7">
        </td> -->

        <!-- <td headers="th_pt" class="td_numbig td_input"><?php echo $it_point; ?></td> -->
        <td headers="th_qty" class="td_numbig td_input">

            <?php echo $row['it_stock_qty']; ?>
        </td>

<!-- <td class="td_mng td_mng_s"> -->
		 <td width="200">
            <a href="./itemform.php?w=u&amp;it_id=<?php echo $row['it_id']; ?>&amp;ca_id=<?php echo $row['ca_id']; ?>&amp;<?php echo $qstr; ?>" class="btn btn_03"><span class="sound_only"><?php echo htmlspecialchars2(cut_str($row['it_name'],250, "")); ?> </span>수정</a>
            <a href="./itemcopy.php?it_id=<?php echo $row['it_id']; ?>&amp;ca_id=<?php echo $row['ca_id']; ?>" class="itemcopy btn btn_02" target="_blank"><span class="sound_only"><?php echo htmlspecialchars2(cut_str($row['it_name'],250, "")); ?> </span>복사</a>
            <a href="<?php echo $href; ?>" class="btn btn_02"><span class="sound_only"><?php echo htmlspecialchars2(cut_str($row['it_name'],250, "")); ?> </span>보기</a>
        </td>

    </tr>
    <?php
    }
    if ($i == 0)
        echo '<tr><td colspan="13" class="empty_table">자료가 한건도 없습니다.</td></tr>';
    ?>
    </tbody>
    </table>
</div>

<div class="btn_fixed_top">

    <a href="./itemform.php" class="btn btn_01">상품등록</a>
    <!-- <a href="./itemexcel.php" onclick="return excelform(this.href);" target="_blank" class="btn btn_02">상품일괄등록</a> -->
	<input type="submit" name="act_button" value="옵션상품으로등록" onclick="document.pressed=this.value" class="btn btn_02">
    <input type="submit" name="act_button" value="선택수정" onclick="document.pressed=this.value" class="btn btn_02">
    <?php if ($is_admin == 'super') { ?>
    <input type="submit" name="act_button" value="선택삭제" onclick="document.pressed=this.value" class="btn btn_02">
    <?php } ?>
    <input type="button" value="검색결과 수정폼 다운로드" class="get_theme_confc btn btn_02 color_04" style="cursor:pointer" onclick="formSubmit(1, 2);">
</div>
<!-- <div class="btn_confirm01 btn_confirm">
    <input type="submit" value="일괄수정" class="btn_submit" accesskey="s">
</div> -->
</form>

<?php 
echo get_paging(G5_IS_MOBILE ? $config['cf_mobile_pages'] : $config['cf_write_pages'], $page, $total_page, "{$_SERVER['SCRIPT_NAME']}?$qstr&amp;page="); ?>


<script src="<?php echo G5_ADMIN_URL; ?>/jqueryfileDownload.js"></script>
<script>

function formSubmit(type, action) {

	var s_it_main1 = "";
	var s_it_main2 = "";
	var s_it_main3 = "";
	var s_it_type1 = "";
	var s_it_type2 = "";
	var s_it_type3 = "";
	var s_it_type4 = "";
	var s_it_crm_yn = "";
	var s_it_service_view_1 = "";
	var s_it_service_view_2 = "";

	if( $('input:checkbox[name="s_it_main1"]').is(":checked") ){ 
		var s_it_main1 = "1";
	}
	if( $('input:checkbox[name="s_it_main2"]').is(":checked") ){ 
		var s_it_main2 = "1";
	}
	if( $('input:checkbox[name="s_it_main3"]').is(":checked") ){ 
		var s_it_main3 = "1";
	}
	if( $('input:checkbox[name="s_it_type1"]').is(":checked") ){ 
		var s_it_type1 = "1";
	}
	if( $('input:checkbox[name="s_it_type2"]').is(":checked") ){ 
		var s_it_type2 = "1";
	}
	if( $('input:checkbox[name="s_it_type3"]').is(":checked") ){ 
		var s_it_type3 = "1";
	}
	if( $('input:checkbox[name="s_it_type4"]').is(":checked") ){ 
		var s_it_type4 = "1";
	}
	if( $('input:checkbox[name="s_it_service_view_1"]').is(":checked") ){ 
		var s_it_service_view_1 = "1";
	}
	if( $('input:checkbox[name="s_it_service_view_2"]').is(":checked") ){ 
		var s_it_service_view_2 = "1";
	}

	if (action == 1) {
        action = 'itemlist_01.php';
        var ov_num = $('.ov_num').text();
        var confirm_txt = '현재 조회 된 상품 ' + ov_num + '을 엑셀로 다운합니다. 검색 결과가 많으면 로딩 시간이 길어 질 수 있습니다. 다운받으시겠습니까?';

        if (!confirm(confirm_txt)) {
            return false;
        }
    } else if (action == 2) {
        action = 'itemlist_excel.php';
        var ov_num = $('.ov_num').text();
        var confirm_txt = '현재 조회 된 상품 ' + ov_num + '을 엑셀로 다운합니다. 검색 결과가 많으면 로딩 시간이 길어 질 수 있습니다. 다운받으시겠습니까?';

        if (!confirm(confirm_txt)) {
            return false;
        }
    }

	var form = "<form name='pform' id='pform' action='"+action+"' method='post'>";
		form += "<input type='hidden' name='mode' value='excel' />"; 
		form += "<input type='hidden' name='comp_code' value='"+$('#comp_code option:selected').val()+"' />"; 
		form += "<input type='hidden' name='it_use' value='"+$('#it_use option:selected').val()+"' />"; 
		form += "<input type='hidden' name='it_soldout' value='"+$('#it_soldout option:selected').val()+"' />"; 
		form += "<input type='hidden' name='sca' value='"+$('#sca option:selected').val()+"' />"; 	
		form += "<input type='hidden' name='sfl' value='"+$('#sfl option:selected').val()+"' />"; 
		form += "<input type='hidden' name='stx' value='"+$('#stx').val()+"' />"; 
		form += "<input type='hidden' name='s_it_main1' value='"+s_it_main1+"' />"; 
		form += "<input type='hidden' name='s_it_main2' value='"+s_it_main2+"' />"; 
		form += "<input type='hidden' name='s_it_main3' value='"+s_it_main3+"' />"; 
		form += "<input type='hidden' name='s_it_type1' value='"+s_it_type1+"' />"; 
		form += "<input type='hidden' name='s_it_type2' value='"+s_it_type2+"' />"; 
		form += "<input type='hidden' name='s_it_type3' value='"+s_it_type3+"' />"; 
		form += "<input type='hidden' name='s_it_type4' value='"+s_it_type4+"' />"; 
		form += "<input type='hidden' name='s_it_service_view_1' value='"+s_it_service_view_1+"' />"; 
		form += "<input type='hidden' name='s_it_service_view_2' value='"+s_it_service_view_2+"' />"; 
		form += "<input type='hidden' name='s_comp_code' value='"+$('#comp_code option:selected').val()+"' />"; 
		form += "<input type='hidden' name='s_it_time' value='"+$('#s_it_time').val()+"' />"; 
		form += "<input type='hidden' name='s_it_crm_yn' value='"+$('#s_it_crm_yn').val()+"' />"; 
		form += "</form>";

    jQuery(form).appendTo("body");

    fileSubmit();
}

function fileSubmit() {
    $.fileDownload($("#pform").prop('action'),{
        httpMethod:"POST",
        data:$("#pform").serialize(),
        successCallback: function(url){
            $('#loading').hide();
        },
        failCallback: function(responseHtml,url){
            $('#loading').hide();
        },
        prepareCallback: function() {
            $('#loading').show();
        }
    });
}

function fitemlist_submit(f)
{
    if (!is_checked("chk[]")) {
        alert(document.pressed+" 하실 항목을 하나 이상 선택하세요.");
        return false;
    }

    if(document.pressed == "선택삭제") {
        if(!confirm("선택한 자료를 정말 삭제하시겠습니까?")) {
            return false;
        }
    }

    return true;
}

$(function() {
    $(".itemcopy").click(function() {
        var href = $(this).attr("href");
        window.open(href, "copywin", "left=100, top=100, width=300, height=200, scrollbars=0");
        return false;
    });
});

function excelform(url)
{
    var opt = "width=600,height=450,left=10,top=10";
    window.open(url, "win_excel", opt);
    return false;
}

$(function(){
    $("#s_it_time").datepicker({ changeMonth: true, changeYear: true, dateFormat: "yy-mm-dd", showButtonPanel: true, yearRange: "c-99:c+99"});
});

</script>

<!--업체선택 검색기능-->

<link href="../select2.css" rel="stylesheet"/>
<script type="text/javascript" src="../select2.js"></script>
<script>
$(document).ready(function () {
	$("#comp_code").select2();
});
</script>

<?php
include_once (G5_ADMIN_PATH.'/admin.tail.php');
?>

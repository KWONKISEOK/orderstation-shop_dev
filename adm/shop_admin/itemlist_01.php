<?php
include_once('./_common.php');
include_once(G5_LIB_PATH.'/iteminfo.lib.php');

$mode = $_POST['mode'];

$where = " and ";
$sql_search = "";


if ($stx != "") {
    if ($sfl != "it_id") {
        $sql_search .= " $where $sfl like '%$stx%' ";
        $where = " and ";

    } else if ($sfl == "it_id") {

        if ($sfl != "") {
            $sfl = "a.".$sfl;
            $sql_search .= " $where instr('$stx',$sfl)";
            /*$add1="'%";
            $add2="%'";
            $sfl="a.".$sfl;
            $stx1 = str_replace(" ","%' or a.it_id like '%",$stx);
            $stx2 = $add1.$stx1.$add2;
            $sql_search .= " $where $sfl like $stx2 ";
            */
            $where = " and ";
        }
    }
    if ($save_stx != $stx)
        $page = 1;
}

if ($sca != "") {
    $sql_search .= " $where (a.ca_id like '$sca%' or a.ca_id2 like '$sca%' or a.ca_id3 like '$sca%') ";
}

if ($sfl == "") $sfl = "it_name";

$sql_common = " from {$g5['g5_shop_item_table']} a 
                     left join {$g5['g5_shop_category_table']} b on b.ca_id = a.ca_id
					 left join tbl_member c on c.comp_code = a.comp_code
					 left join erp_code d on a.it_id = d.onk_code
					 left join tbl_shop_item_option e ON a.it_id = e.it_id
               where c.mb_type = 7";

if ($s_comp_code != '') {
    $comp_code = $s_comp_code;
}
if ($comp_code != '')
    $sql_common .= " and a.comp_code = '$comp_code' ";

if ($it_use != '')
    $sql_common .= " and a.it_use ='$it_use' ";

$sql_common .= " and a.it_status > 2 ";

if ($it_soldout != '')
    $sql_common .= " and a.it_soldout ='$it_soldout' ";

if ($s_it_main1 != "") {
    $sql_common .= " and a.it_main1 = '1' ";
}
if ($s_it_main2 != "") {
    $sql_common .= " and a.it_main2 = '1' ";
}
if ($s_it_main3 != "") {
    $sql_common .= " and a.it_main3 = '1' ";
}
if ($s_it_type1 != "") {
    $sql_common .= " and a.it_type1 = '1' ";
}
if ($s_it_type2 != "") {
    $sql_common .= " and a.it_type2 = '1' ";
}
if ($s_it_type3 != "") {
    $sql_common .= " and a.it_type3 = '1' ";
}
if ($s_it_time != "") {
    $sql_common .= " and DATE(a.it_time) >= '".$s_it_time."' ";
}
if ($s_it_crm_yn != "") {
    $sql_common .= " and a.it_crm_yn = '1' ";
}
$sql_common .= $sql_search;


if (!$sst) {
    $sst = "it_time";
    $sod = "desc";
}
$sql_order = "order by a.it_id asc ";

switch ($mode) {

    case 'excel':
        $ExcelTitle = urlencode("상품리스트".date("YmdHis"));

        header("Content-type: application/vnd.ms-excel");
        header("Content-type: application/vnd.ms-excel; charset=utf-8");
        header("Content-Disposition: attachment; filename = ".$ExcelTitle.".xls");
        header("Content-Description: PHP4 Generated Data");
        header('Set-Cookie: fileDownload=true; path=/');
        print("<meta http-equiv=\"Content-Type\" content=\"application/vnd.ms-excel; charset=utf-8\">");

        ?>
        <table border="1">
            <tr bgcolor="F7F7F7" height="30">
                <th>상품코드</th>
                <th>태전ERP코드</th>
                <th colspan="3">1차 카테고리</th>
                <th colspan="3">2차 카테고리</th>
                <th>상품명</th>
                <th>공급업체</th>
                <th>규격</th>
                <th>제조사/제조국</th>
                <th>원산지</th>
                <th>과세</th>
                <th>매입가</th>
                <th>도매가</th>
                <th>일반판매가</th>
                <th>약국판매가</th>
                <th>수수료</th>
                <th>약국판매가(앱)</th>
                <th>앱수수료</th>
                <th>검색어</th>
                <th>재고량</th>
                <th>최소주문수량</th>
                <th>최대주문수량</th>
                <th>판매여부</th>
                <th>품절여부</th>
                <th>옵션판매여부</th>
                <th>메인화면노출</th>
                <th>뺏지달기</th>
                <th>상품등록일자</th>
                <th>배송비</th>
                <th>묶음배송</th>
                <th>마감시간</th>
                <th>출고일</th>
                <th>정기주문가능여부</th>
                <th>배송비 유형</th>
                <th>배송비 결제</th>
                <th>기본배송비</th>
                <th>배송비 상세조건</th>
                <th>묶음배송 여부</th>
                <th>상품군</th>
                <th>CRM 재고관리 대상제품</th>
                <th>노출구분</th>
                <th>옵션코드</th>
                <th>옵션명</th>
            </tr>
            <tr bgcolor="F7F7F7">
                <th></th>
                <th></th>
                <th>Depth.1</th>
                <th>Depth.2</th>
                <th>Depth.3</th>
                <th>Depth.1</th>
                <th>Depth.2</th>
                <th>Depth.3</th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
            </tr>
            <?php
            $sql = " 
			SELECT X.* 
				,CASE @vjob WHEN X.it_id THEN @rownum:=@rownum+1 ELSE @rownum:=1 END as ranking
				,@vjob:=X.it_id as vjob
				,IFNULL( case when @rownum = 1 then Y.io_cnt ELSE 0 END , -1 ) AS row_span_cnt
			FROM(
						select a.* , e.io_id , e.io_name , c.comp_name
							,(select ca_name from tbl_shop_category where length(ca_id) = '2' and ca_id = left(a.ca_id, 2)) ca_name1
							,(select ca_name from tbl_shop_category where length(ca_id) = '4' and  ca_id = left(a.ca_id, 4)) ca_name2
							,(select ca_name from tbl_shop_category where ca_id = a.ca_id ) ca_name3
							,(select ca_name from tbl_shop_category where length(ca_id) = '2' and ca_id = left(a.ca_id2, 2)) ca_name4
							,(select ca_name from tbl_shop_category where length(ca_id) = '4' and  ca_id = left(a.ca_id2, 4)) ca_name5
							,(select ca_name from tbl_shop_category where ca_id = a.ca_id2 ) ca_name6
							,(select io_use from tbl_shop_item_option where io_id = a.it_id limit 1) io_use
							, case when d.erp_code > '0' then d.erp_code else a.it_id end as erp_code
			   $sql_common
			   $sql_order 
			) AS X LEFT JOIN(
				SELECT COUNT(it_id) AS io_cnt , it_id FROM tbl_shop_item_option GROUP BY it_id
			) AS Y ON X.it_id = Y.it_id	, ( SELECT @vjob:='', @rownum:=0 ) as Z  ";

            /*
            echo "<pre>".$sql."</pre>";
            exit;
            */


            $res = sql_query($sql);
            if (!$res) exit('Cannot run query.');

            $i = 0;
            while ($row = sql_fetch_array($res)) {

                $it_id = $row["it_id"];

                switch ($row["it_notax"]) {
                    case "0" :
                        $it_notax = "과세";
                        break;
                    case "1" :
                        $it_notax = "비과세";
                        break;
                    case "2" :
                        $it_notax = "면세";
                        break;
                }
                $it_sc_type == '';
                switch ($row["it_sc_type"]) {
                    case "1" :
                        $it_sc_type = "무료배송";
                        break;
                    case "2" :
                        $it_sc_type = "조건부 무료배송";
                        break;
                    case "3" :
                        $it_sc_type = "유료배송";
                        break;
                }
                $it_trans_bundle = '';
                switch ($row["it_trans_bundle"]) {
                    case "1" :
                        $it_trans_bundle = "<font color='blue'>묶음배송</font>";
                        break;
                    case "0" :
                        $it_trans_bundle = "개별배송";
                        break;
                }
                switch ($row["it_period_yn"]) {
                    case "Y" :
                        $it_period_yn = "가능";
                        break;
                    case "N" :
                        $it_period_yn = "불가능";
                        break;
                }
                $it_use = '';
                $io_use = '';
                switch ($row["it_use"]) {
                    case "1" :
                        $it_use = "<font color='blue'>판매</font>";
                        break;
                    case "0" :
                        $it_use = "<font color='red'>사용안함</font>";
                        break;
                }
                $it_soldout = '';
                switch ($row["it_soldout"]) {
                    case "1" :
                        $it_soldout = "<font color='red'>품절</font>";
                        break;
                    case "0" :
                        $it_soldout = "<font color='blue'>판매</font>";
                        break;
                }
                switch ($row["io_use"]) {
                    case "1" :
                        $io_use = "<font color='blue'>판매</font>";
                        break;
                    case "0" :
                        $io_use = "<font color='red'>사용안함</font>";
                        break;
                }

                /*$ca_name1 ='';
                if($row["ca_name1"] != '') $ca_name1 = $row["ca_name1"] ;
                if($row["ca_name2"] != '') $ca_name1 = $ca_name1 . ' > '.$row["ca_name2"] ;
                if($row["ca_name3"] != '') $ca_name1 = $ca_name1 . ' > '.$row["ca_name3"] ;
                $ca_name2 ='';
                if($row["ca_name4"] != '') $ca_name2 = $row["ca_name4"] ;
                if($row["ca_name5"] != '') $ca_name2 = $ca_name2 . ' > '.$row["ca_name5"] ;
                if($row["ca_name6"] != '') $ca_name2 = $ca_name2 . ' > '.$row["ca_name6"] ;
                */
                ?>
                <tr bgcolor="">
                    <? if ($row["row_span_cnt"] > 0) { ?> <!-- 0보다 크면 -->
                        <td style="mso-number-format:'\@';" rowspan="<?= $row["row_span_cnt"] ?>"><?php echo $row["it_id"]; ?></td>
                        <td style="mso-number-format:'\@';" rowspan="<?= $row["row_span_cnt"] ?>"> <?php echo $row["erp_code"]; ?></td>
                        <td rowspan="<?= $row["row_span_cnt"] ?>"><?php echo $row["ca_name1"]; ?></td>
                        <td rowspan="<?= $row["row_span_cnt"] ?>"><?php echo $row["ca_name2"]; ?></td>
                        <td rowspan="<?= $row["row_span_cnt"] ?>"><?php echo $row["ca_name3"]; ?></td>

                        <td rowspan="<?= $row["row_span_cnt"] ?>"><?php echo $row["ca_name4"]; ?></td>
                        <td rowspan="<?= $row["row_span_cnt"] ?>"><?php echo $row["ca_name5"]; ?></td>
                        <td rowspan="<?= $row["row_span_cnt"] ?>"><?php echo $row["ca_name6"]; ?></td>

                        <td rowspan="<?= $row["row_span_cnt"] ?>"><?php echo $row["it_name"]; ?></td>
                        <td rowspan="<?= $row["row_span_cnt"] ?>"><?php echo $row["comp_name"]; ?></td>
                        <td rowspan="<?= $row["row_span_cnt"] ?>"><?php echo $row["it_model"]; ?></td>
                        <td rowspan="<?= $row["row_span_cnt"] ?>"><?php echo $row["it_maker"]; ?></td>
                        <td rowspan="<?= $row["row_span_cnt"] ?>"><?php echo $row["it_origin"]; ?></td>
                        <td rowspan="<?= $row["row_span_cnt"] ?>"><?php echo $it_notax; ?></td>

                        <td rowspan="<?= $row["row_span_cnt"] ?>"><?php echo $row["it_supply_price"]; ?></td>
                        <td rowspan="<?= $row["row_span_cnt"] ?>"><?php echo $row["it_dodome_price"]; ?></td>
                        <td rowspan="<?= $row["row_span_cnt"] ?>"><?php echo $row["it_price"]; ?></td>
                        <td rowspan="<?= $row["row_span_cnt"] ?>"><?php echo $row["it_drug_price"]; ?></td>
                        <td rowspan="<?= $row["row_span_cnt"] ?>"><?php echo $row["it_incen"]; ?></td>

                        <td rowspan="<?= $row["row_span_cnt"] ?>"><?php echo $row["it_app_drug_price"]; ?></td>
                        <td rowspan="<?= $row["row_span_cnt"] ?>"><?php echo $row["it_app_incen"]; ?></td>
                        <td rowspan="<?= $row["row_span_cnt"] ?>"><?php echo $row["it_search"]; ?></td>
                        <td rowspan="<?= $row["row_span_cnt"] ?>"><?php echo $row["it_stock_qty"]; ?></td>
                        <td rowspan="<?= $row["row_span_cnt"] ?>"><?php echo $row["it_buy_min_qty"]; ?></td>
                        <td rowspan="<?= $row["row_span_cnt"] ?>"><?php echo $row["it_buy_max_qty"]; ?></td>
                        <td rowspan="<?= $row["row_span_cnt"] ?>"><?php echo $it_use; ?></td>
                        <td rowspan="<?= $row["row_span_cnt"] ?>"><?php echo $it_soldout; ?></td>
                        <td rowspan="<?= $row["row_span_cnt"] ?>"><?php echo $io_use; ?></td>
                        <td rowspan="<?= $row["row_span_cnt"] ?>">
                            <?
                            $tot_it_main = "";
                            if ($row["it_main1"] == 1) {
                                $tot_it_main .= "베스트";
                            }
                            if ($row["it_main2"] == 1) {
                                if ($tot_it_main != "") {
                                    $tot_it_main .= ",";
                                }
                                $tot_it_main .= "추천상품";
                            }
                            if ($row["it_main3"] == 1) {
                                if ($tot_it_main != "") {
                                    $tot_it_main .= ",";
                                }
                                $tot_it_main .= "신상품";
                            }
                            echo $tot_it_main;
                            ?>
                        </td>
                        <td rowspan="<?= $row["row_span_cnt"] ?>">
                            <?
                            $tot_it_type = "";
                            if ($row["it_type1"] == 1) {
                                $tot_it_type .= "베스트";
                            }
                            if ($row["it_type2"] == 1) {
                                if ($tot_it_type != "") {
                                    $tot_it_type .= ",";
                                }
                                $tot_it_type .= "추천상품";
                            }
                            if ($row["it_type3"] == 1) {
                                if ($tot_it_type != "") {
                                    $tot_it_type .= ",";
                                }
                                $tot_it_type .= "신상품";
                            }
                            echo $tot_it_type;
                            ?>
                        </td>
                        <td rowspan="<?= $row["row_span_cnt"] ?>"><?php echo $row["it_time"]; ?></td>
                        <td rowspan="<?= $row["row_span_cnt"] ?>"><?php echo $it_sc_type; ?></td>
                        <td rowspan="<?= $row["row_span_cnt"] ?>"><?php echo $it_trans_bundle; ?></td>
                        <td rowspan="<?= $row["row_span_cnt"] ?>"><?php echo $row["it_order_close_time"]; ?></td>
                        <td rowspan="<?= $row["row_span_cnt"] ?>"><?php echo $row["it_release_day"]; ?></td>
                        <td rowspan="<?= $row["row_span_cnt"] ?>"><?php echo $it_period_yn; ?></td>
                        <td rowspan="<?= $row["row_span_cnt"] ?>">
                            <?
                            if ($row["it_sc_type"] == "0") {
                                echo "쇼핑몰 기본설정";
                            } else if ($row["it_sc_type"] == "1") {
                                echo "무료배송";
                            } else if ($row["it_sc_type"] == "2") {
                                echo "조건부 무료배송";
                            } else if ($row["it_sc_type"] == "3") {
                                echo "유료배송";
                            } else if ($row["it_sc_type"] == "4") {
                                echo "수량별부과";
                            }
                            ?>
                        </td>
                        <td rowspan="<?= $row["row_span_cnt"] ?>">
                            <?
                            if ($row["it_sc_method"] == "0") {
                                echo "선불";
                            } else if ($row["it_sc_method"] == "1") {
                                echo "착불";
                            } else if ($row["it_sc_method"] == "2") {
                                echo "사용자선택";
                            }
                            ?>
                        </td>
                        <td rowspan="<?= $row["row_span_cnt"] ?>"><?= $row["it_sc_price"] ?></td>
                        <td rowspan="<?= $row["row_span_cnt"] ?>"><? if ($row["it_sc_type"] == "2") { ?><?= $row["it_sc_minimum"] ?><? } ?></td>
                        <td rowspan="<?= $row["row_span_cnt"] ?>">
                            <?
                            if ($row["it_trans_bundle"] == "1") {
                                echo "묶음배송";
                            }
                            ?>
                        </td>
                        <td rowspan="<?= $row["row_span_cnt"] ?>">
                            <?
                            foreach ($item_info as $key => $value) {

                                if ($row["it_info_gubun"] == $key) {
                                    echo $value['title'];
                                    break;
                                }

                            }
                            ?>
                        </td>
                        <td rowspan="<?= $row["row_span_cnt"] ?>">
                            <?
                            if ($row["it_crm_yn"] == "1") {
                                echo "Y";
                            } else {
                                echo "N";
                            }
                            ?>
                        </td>
                        <td rowspan="<?= $row["row_span_cnt"] ?>">
                            <?
                            if ($row["it_web_view"] == "1" && $row["it_app_view"] != "1") {
                                echo "약국회원";
                            }
                            if ($row["it_web_view"] != "1" && $row["it_app_view"] == "1") {
                                echo "일반회원";
                            }
                            if ($row["it_web_view"] == "1" && $row["it_app_view"] == "1") {
                                echo "약국회원/일반회원";
                            }
                            ?>
                        </td>
                    <? } ?>

                    <? if ($row["row_span_cnt"] < 0) { ?> <!-- 0보다 작으면 -->
                        <td style="mso-number-format:'\@';"><?php echo $row["it_id"]; ?></td>
                        <td style="mso-number-format:'\@';"><?php echo $row["erp_code"]; ?></td>
                        <td><?php echo $row["ca_name1"]; ?></td>
                        <td><?php echo $row["ca_name2"]; ?></td>
                        <td><?php echo $row["ca_name3"]; ?></td>

                        <td><?php echo $row["ca_name4"]; ?></td>
                        <td><?php echo $row["ca_name5"]; ?></td>
                        <td><?php echo $row["ca_name6"]; ?></td>

                        <td><?php echo $row["it_name"]; ?></td>
                        <td><?php echo $row["comp_name"]; ?></td>
                        <td><?php echo $row["it_model"]; ?></td>
                        <td><?php echo $row["it_maker"]; ?></td>
                        <td><?php echo $row["it_origin"]; ?></td>
                        <td><?php echo $it_notax; ?></td>

                        <td><?php echo $row["it_supply_price"]; ?></td>
                        <td><?php echo $row["it_dodome_price"]; ?></td>
                        <td><?php echo $row["it_price"]; ?></td>
                        <td><?php echo $row["it_drug_price"]; ?></td>
                        <td><?php echo $row["it_incen"]; ?></td>

                        <td><?php echo $row["it_app_drug_price"]; ?></td>
                        <td><?php echo $row["it_app_incen"]; ?></td>
                        <td><?php echo $row["it_search"]; ?></td>
                        <td><?php echo $row["it_stock_qty"]; ?></td>
                        <td><?php echo $row["it_buy_min_qty"]; ?></td>
                        <td><?php echo $row["it_buy_max_qty"]; ?></td>
                        <td><?php echo $it_use; ?></td>
                        <td><?php echo $it_soldout; ?></td>
                        <td><?php echo $io_use; ?></td>
                        <td>
                            <?
                            $tot_it_main = "";
                            if ($row["it_main1"] == 1) {
                                $tot_it_main .= "베스트";
                            }
                            if ($row["it_main2"] == 1) {
                                if ($tot_it_main != "") {
                                    $tot_it_main .= ",";
                                }
                                $tot_it_main .= "추천상품";
                            }
                            if ($row["it_main3"] == 1) {
                                if ($tot_it_main != "") {
                                    $tot_it_main .= ",";
                                }
                                $tot_it_main .= "신상품";
                            }
                            echo $tot_it_main;
                            ?>
                        </td>
                        <td>
                            <?
                            $tot_it_type = "";
                            if ($row["it_type1"] == 1) {
                                $tot_it_type .= "베스트";
                            }
                            if ($row["it_type2"] == 1) {
                                if ($tot_it_type != "") {
                                    $tot_it_type .= ",";
                                }
                                $tot_it_type .= "추천상품";
                            }
                            if ($row["it_type3"] == 1) {
                                if ($tot_it_type != "") {
                                    $tot_it_type .= ",";
                                }
                                $tot_it_type .= "신상품";
                            }
                            echo $tot_it_type;
                            ?>
                        </td>
                        <td><?php echo $row["it_time"]; ?></td>
                        <td><?php echo $it_sc_type; ?></td>
                        <td><?php echo $it_trans_bundle; ?></td>
                        <td><?php echo $row["it_order_close_time"]; ?></td>
                        <td><?php echo $row["it_release_day"]; ?></td>
                        <td><?php echo $it_period_yn; ?></td>
                        <td>
                            <?
                            if ($row["it_sc_type"] == "0") {
                                echo "쇼핑몰 기본설정";
                            } else if ($row["it_sc_type"] == "1") {
                                echo "무료배송";
                            } else if ($row["it_sc_type"] == "2") {
                                echo "조건부 무료배송";
                            } else if ($row["it_sc_type"] == "3") {
                                echo "유료배송";
                            } else if ($row["it_sc_type"] == "4") {
                                echo "수량별부과";
                            }
                            ?>
                        </td>
                        <td>
                            <?
                            if ($row["it_sc_method"] == "0") {
                                echo "선불";
                            } else if ($row["it_sc_method"] == "1") {
                                echo "착불";
                            } else if ($row["it_sc_method"] == "2") {
                                echo "사용자선택";
                            }
                            ?>
                        </td>
                        <td><?= $row["it_sc_price"] ?></td>
                        <td><? if ($row["it_sc_type"] == "2") { ?><?= $row["it_sc_minimum"] ?><? } ?></td>
                        <td>
                            <?
                            if ($row["it_trans_bundle"] == "1") {
                                echo "묶음배송";
                            }
                            ?>
                        </td>
                        <td>
                            <?
                            foreach ($item_info as $key => $value) {

                                if ($row["it_info_gubun"] == $key) {
                                    echo $value['title'];
                                    break;
                                }

                            }
                            ?>
                        </td>
                        <td>
                            <?
                            if ($row["it_crm_yn"] == "1") {
                                echo "Y";
                            } else {
                                echo "N";
                            }
                            ?>
                        </td>
                        <td>
                            <?
                            if ($row["it_web_view"] == "1" && $row["it_app_view"] != "1") {
                                echo "약국회원";
                            }
                            if ($row["it_web_view"] != "1" && $row["it_app_view"] == "1") {
                                echo "일반회원";
                            }
                            if ($row["it_web_view"] == "1" && $row["it_app_view"] == "1") {
                                echo "약국회원/일반회원";
                            }
                            ?>
                        </td>

                    <? } ?>
                    <td><?php echo $row["io_id"] ?></td>
                    <td><?php echo $row["io_name"] ?></td>
                </tr>
                <?php
                $i++;
            }
            ?>

        </table>
        <?php
        break;

}


?>
<?php
$sub_menu = '700600';
include_once('./_common.php');

auth_check($auth[$sub_menu], "w");

$bn_id = preg_replace('/[^0-9]/', '', $bn_id);

$html_title = '요즘은 이런게 대세 ';
$g5['title'] = $html_title.'관리';

$w = "u";

$html_title .= ' 수정';
$sql = " select * from tbl_shop_banner_trend limit 3";
$result = sql_query($sql);

include_once(G5_ADMIN_PATH.'/admin.head.php');
?>

<form name="fbanner" action="./trendbannerformupdate.php" method="post" enctype="multipart/form-data">
    <input type="hidden" name="w" value="<?php echo $w; ?>">
    <input type="hidden" name="bn_id" value="<?php echo $bn_id; ?>">

    <div class="tbl_frm01 tbl_wrap">
        <table>
            <caption><?php echo $g5['title']; ?></caption>
            <colgroup>
                <col class="grid_4">
                <col>
            </colgroup>
            <tbody>
            <?php
            for ($i = 1; $row = sql_fetch_array($result); $i++) { ?>
                <tr>
                    <th><?php echo $i; ?>번탭</th>
                    <td>
                        <table>
                            <tr>
                                <th scope="row" style="background: #d6dce7;"><label for="bn_alt">이벤트 id</label></th>
                                <td>
                                    <input type="text" name="bn_event_id_<?php echo $i; ?>" value="<?php echo $row['bn_event_id']; ?>" id="bn_id_<?php echo $i; ?>" class="frm_input" size="40">
                                </td>
                            </tr>
                            <tr>
                                <th scope="row" style="background: #d6dce7;"><label for="bn_alt">이벤트 설명</label></th>
                                <td>
                                    <input type="hidden" name="bn_id_<?php echo $i; ?>" value="<?php echo $row['bn_id'] ?>">
                                    <input type="text" name="bn_event_name_<?php echo $i; ?>" value="<?php echo $row['bn_event_name']; ?>" id="bn_event_name_<?php echo $i; ?>" class="frm_input" size="40">
                                </td>
                            </tr>
                            <tr>
                                <th scope="row" style="background: #d6dce7;"><label for="bn_alt">탭 이미지</label></th>
                                <td>
                                    <input type="file" name="bn_tabimg_<?php echo $i; ?>">
                                    <?php
                                    $bimg_str = "";
                                    $bimg = G5_DATA_PATH."/banner_trend/{$row['bn_tabimg']}";
                                    if (file_exists($bimg) && $row['bn_tabimg']) {
                                        $size = @getimagesize($bimg);
                                        if ($size[0] && $size[0] > 100)
                                            $width = 100;
                                        else
                                            $width = $size[0];

                                        //echo '<input type="checkbox" name="bn_bimg_del" value="1" id="bn_bimg_del"> <label for="bn_bimg_del">삭제</label>';
                                        $bimg_str = '<img src="'.G5_DATA_URL.'/banner_trend/'.$row['bn_tabimg'].'" width="'.$width.'">';
                                    }
                                    if ($bimg_str) {
                                        echo '<div class="banner_or_img">';
                                        echo $bimg_str;
                                        echo '</div>';
                                    }
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row" style="background: #d6dce7;"><label for="bn_alt">PC 배너 이미지</label></th>
                                <td>
                                    <input type="file" name="bn_pcimg_<?php echo $i; ?>">
                                    <?php
                                    $bimg_str = "";
                                    $bimg = G5_DATA_PATH."/banner_trend/{$row['bn_pcimg']}";
                                    if (file_exists($bimg) && $row['bn_pcimg']) {
                                        $size = @getimagesize($bimg);
                                        if ($size[0] && $size[0] > 100)
                                            $width = 100;
                                        else
                                            $width = $size[0];

                                        //echo '<input type="checkbox" name="bn_bimg_del" value="1" id="bn_bimg_del"> <label for="bn_bimg_del">삭제</label>';
                                        $bimg_str = '<img src="'.G5_DATA_URL.'/banner_trend/'.$row['bn_pcimg'].'" width="'.$width.'">';
                                    }
                                    if ($bimg_str) {
                                        echo '<div class="banner_or_img">';
                                        echo $bimg_str;
                                        echo '</div>';
                                    }
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row" style="background: #d6dce7;"><label for="bn_alt">Mobile 배너 이미지</label>
                                </th>
                                <td>
                                    <input type="file" name="bn_mimg_<?php echo $i; ?>">
                                    <?php
                                    $bimg_str = "";
                                    $bimg = G5_DATA_PATH."/banner_trend/{$row['bn_mimg']}";
                                    if (file_exists($bimg) && $row['bn_mimg']) {
                                        $size = @getimagesize($bimg);
                                        if ($size[0] && $size[0] > 100)
                                            $width = 100;
                                        else
                                            $width = $size[0];

                                        //echo '<input type="checkbox" name="bn_bimg_del" value="1" id="bn_bimg_del"> <label for="bn_bimg_del">삭제</label>';
                                        $bimg_str = '<img src="'.G5_DATA_URL.'/banner_trend/'.$row['bn_mimg'].'" width="'.$width.'">';
                                    }
                                    if ($bimg_str) {
                                        echo '<div class="banner_or_img">';
                                        echo $bimg_str;
                                        echo '</div>';
                                    }
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row"><label for="bn_order">사용 유무</label></th>
                                <td>
                                    <input type="radio" name="bn_use_<?php echo $i; ?>" value="Y" <?php echo get_checked($row['bn_use'], "Y"); ?>/>&nbsp;Y&nbsp;
                                    <input type="radio" name="bn_use_<?php echo $i; ?>" value="N" <?php echo get_checked($row['bn_use'], "N"); ?>/>&nbsp;N&nbsp;
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>

    <div class="btn_fixed_top">
        <input type="submit" value="저장" class="btn_submit btn" accesskey="s">
    </div>

</form>

<?php
include_once(G5_ADMIN_PATH.'/admin.tail.php');
?>

<?php
$sub_menu = '600670';
include_once('./_common.php');

auth_check($auth[$sub_menu], "r");

$g5['title'] = '대표상품 엑셀업로드';
include_once (G5_ADMIN_PATH.'/admin.head.php');


// 테이블의 전체 레코드수만 얻음
$sql = " select count(*) as cnt from item_excel_fileinfo ";
$row = sql_fetch($sql);
$total_count = $row['cnt'];

$rows = $config['cf_page_rows'];
$total_page  = ceil($total_count / $rows);  // 전체 페이지 계산
if ($page < 1) { $page = 1; } // 페이지가 없으면 첫 페이지 (1 페이지)
$from_record = ($page - 1) * $rows; // 시작 열을 구함

$sql  = "		  
		  SELECT a.* , ifnull(b.item_cnt,0) AS item_cnt , ifnull(c.item_success_cnt,0) AS item_success_cnt , d.comp_name
		  from item_excel_fileinfo AS a 
		  left join(
		  		SELECT COUNT(f_idx) AS item_cnt , f_idx FROM tbl_shop_item_excel GROUP BY f_idx
		  ) AS b ON a.f_idx = b.f_idx
		  left join(
		  		SELECT COUNT(f_idx) AS item_success_cnt , f_idx from tbl_shop_item WHERE f_idx > 0 GROUP BY f_idx
		  ) AS c ON a.f_idx = c.f_idx
		  left join(
		  		select comp_code, comp_name from tbl_member where mb_type='7' 
		  ) AS d ON a.f_comp_code = d.comp_code
		  order by a.f_idx desc limit $from_record, $rows  
		";
$result = sql_query($sql);

$qstr .= ($qstr ? '&amp;' : '').'sca='.$sca.'&amp;save_stx='.$stx;

?>

<style>
    .td_item {
        background: #3f51b5;
        color: #fff;
    }
</style>

<form name="frm" method="post" enctype="MULTIPART/FORM-DATA">
    <input type="hidden" name="page" value="<?php echo $page; ?>">
    <div class="tbl_head01 tbl_wrap">
        <div>
            <a href="<?php echo G5_URL; ?>/<?php echo G5_LIB_DIR; ?>/Excel/item_excel.xlsx" class="btn btn_02">상품일괄등록용 엑셀양식 다운로드</a>&nbsp;&nbsp;
            <a href="<?php echo G5_URL; ?>/<?php echo G5_LIB_DIR; ?>/Excel/item_excel_update.xlsx" class="btn btn_01">상품일괄수정용 엑셀양식 다운로드</a>
        </div>
        <br>
        <table>
            <tr>
                <td class="td_item" scope="col" width="100">공급사선택</a></td>
                <td class="td_center" width="200">
                    <select name="f_comp_code" id="f_comp_code" style="width:200px;">
                        <option value="">====업체선택====</option>
                        <?php
                        $sql2 = " select comp_code, comp_name from tbl_member where mb_type='7' order by comp_name ";
                        $result2 = sql_query($sql2);
                        for ($i = 0; $row2 = sql_fetch_array($result2); $i++) {
                            if ($comp_code == $row2['comp_code'])
                                $checked = 'selected';
                            else
                                $checked = '';
                            echo '<option value="'.$row2['comp_code'].'" '.$checked.' >'.$row2['comp_name'].'</option>'.PHP_EOL;
                        }
                        ?>
                    </select>
                </td>
                <td class="td_item" scope="col" width="100">엑셀파일</td>
                <td class="td_center" width="250">
                    <input name="excel_file" type="file" size="50">
                </td>
                <td class="td_item" scope="col" width="100">처리유형</td>
                <td scope="td_center" width="150">
                    <input type="radio" name="f_gubun" id="f_gubun1" value="w" checked> <label for="f_gubun1">등록</label>
                    <input type="radio" name="f_gubun" id="f_gubun2" value="u"> <label for="f_gubun2">수정</label>
                </td>
                <td class="td_item" scope="col" width="80">메모</a></td>
                <td class="td_left">
                    <input type="text" name="f_memo" class="frm_input" size="60" maxlength="1000"/>
                    <a href="javascript:SendFrm();" class="btn btn_03">엑셀 임시저장하기</a>
                </td>
            </tr>
            <tr>
                <td class="td_item">Error 행번호:</td>
                <td colspan="7">
                    <div id="ErrorView" style="float:left;font-weight:bold;color:red;"></div>
                </td>
            </tr>
        </table>

        <div>&nbsp;</div>

        <table>
            <caption><?php echo $g5['title']; ?> 목록</caption>
            <thead>
            <tr>
                <th scope="col" width="200">선택공급사</a></th>
                <th scope="col" width="300">파일명</a></th>
                <th scope="col" width="120">등록아이디</a></th>
                <th scope="col" width="120">등록아이피</a></th>
                <th scope="col" width="150">등록일</a></th>
                <th scope="col">메모</a></th>
                <th scope="col" width="170">데이터이동</th>
                <th scope="col" width="70">처리유형</th>
            </tr>
            </thead>
            <tbody>
            <?php
            for ($i = 0; $row = sql_fetch_array($result); $i++) {

                $bg = 'bg'.($i % 2);
                ?>

                <tr class="<?php echo $bg; ?>">
                    <td class="td_left"><?= $row["comp_name"] ?></td>
                    <td class="td_center">
                        <!--<a href="<?= G5_URL; ?>/<?= G5_DATA_DIR; ?>/upload_excel/<?= $row["f_name"] ?>"><?= $row["f_name"] ?></a>-->
                        <a href="./item_exceldownload.php?f_idx=<?= $row["f_idx"]; ?>"><?= $row["f_name"] ?></a>
                    </td>
                    <td class="td_center"><?= $row["f_regid"] ?></td>
                    <td class="td_center"><?= $row["f_regip"] ?></td>
                    <td class="td_center"><?= $row["f_regdate"] ?></td>
                    <td class="td_center"><?= $row["f_memo"] ?></td>
                    <td class="td_center">
                        <? if ($row['item_cnt'] > 0) { ?>
                            <? if ($row['f_gubun'] == 'w') { ?>
                                <a href="javascript:MoveItem('<?= $row["f_idx"] ?>', 'w');" class="btn btn_01">
                                    (<?= $row['item_cnt'] ?>)개 상품이동
                                </a>
                            <? } else { ?>
                                <a href="javascript:MoveItem('<?= $row["f_idx"] ?>', 'u');" class="btn btn_01">
                                    (<?= $row['item_cnt'] ?>)개 상품수정
                                </a>
                            <? } ?>
                            <br><br>
                            <a href="javascript:TempView(<?= $row["f_idx"] ?>);" class="btn btn_03">임시저장보기</a><br><br>
                            <a href="javascript:DelItem(<?= $row["f_idx"] ?>);" class="btn btn_02">임시저장삭제</a>
                        <? } ?>
                        <? if ($row['item_success_cnt'] > 0) { ?>
                            (<?= $row['item_success_cnt'] ?>)개 업로드 완료
                        <? } ?>
                    </td>
                    <td>
                        <? if ($row['f_gubun'] == 'w') { ?>
                            <font color="red">등록</font>
                        <? } else { ?>
                            <font color="blue">수정</font>
                        <? } ?>
                    </td>
                </tr>
                <?php
            }

            if ($i == 0) {
                echo '<tr><td colspan="7" class="empty_table">자료가 없습니다.</td></tr>';
            }
            ?>
            </tbody>
        </table>
    </div>
</form>

<form name="frm2">
	<input type="hidden" name="f_idx"/>
</form>

<!-- (s)hidden Frame -->
<iframe name="iFrm" style="display: none;" ></iframe>

<style>
	#popupLayer {display:none;border:5px solid #cccccc;margin-top:50px;padding:5px;background-color:#ffffff;z-index:5;}
	#popupLayer .b-close {position:absolute;top:10px;right:25px;color:#f37a20;font-weight:bold;cursor:hand;}
	#popupLayer .popupContent {margin:0;padding:0;text-align:center;border:0;}
	#popupLayer .popupContent iframe {width:1500px;height:800px;border:0;padding:0px;margin:0;z-index:10;}
</style>

<div id="popupLayer">
	<div class="popupContent"></div>
	<div class="b-close">
		<button type="button" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button"><span class="ui-button-text">&nbsp;X&nbsp;</span></button>
	</div>
</div>

<script src="/js/jquery.bpopup.min.js"></script>


<?php echo get_paging(G5_IS_MOBILE ? $config['cf_mobile_pages'] : $config['cf_write_pages'], $page, $total_page, "{$_SERVER['SCRIPT_NAME']}?$qstr&amp;page="); ?>

<!--업체선택 검색기능-->
<link href="../select2.css" rel="stylesheet"/>
<script type="text/javascript" src="../select2.js"></script>
<script>

$(document).ready(function () {
	$("#f_comp_code").select2();
});

function SendFrm(){
	var frm = document.frm;

	if(frm.f_comp_code.value == ""){
		alert("공급사를 선택해주세요");
		return;
	}

	if(frm.excel_file.value == ""){
		alert("엑셀파일을 선택해주세요");
		return;
	}
	frm.target = "iFrm";
	if ($("input:radio[name=f_gubun]:checked").val() == "w") {
        frm.action = "item_excelupload_proc.php"; // 등록
    } else {
        frm.action = "item_excelupload_proc2.php"; // 수정
    }
	frm.submit();
}

function MoveItem(GetId, type){

	var frm2 = document.frm2;

	if( GetId == "" ){
		alert("파일idx 값이 없습니다.");
		return
	}

	if (type == 'w') {
        if( !confirm("상품을 이동하시겠습니까?\n(임시저장보기로 이상이없는지 체크후 이동해주십시요.)") ){
            return;
        }
        frm2.action = "./item_exceldata_move.php";
    } else {
        if( !confirm("상품을 수정하시겠습니까?\n(임시저장보기로 이상이없는지 체크후 이동해주십시요.)") ){
            return;
        }
        frm2.action = "./item_exceldata_move2.php";
    }

	frm2.f_idx.value = GetId;
	frm2.target = "iFrm";
	frm2.method = "post";

	frm2.submit();
}

function DelItem(GetId){

	var frm2 = document.frm2;

	if( GetId == "" ){
		alert("파일idx 값이 없습니다.");
		return
	}

	if( !confirm("임시저장데이터를 삭제하시겠습니까?") ){
		return;
	}

	frm2.f_idx.value = GetId;
	frm2.target = "iFrm";
	frm2.method = "post";
	frm2.action = "./item_exceldata_del.php";
	frm2.submit();

}

function TempView(){

	$('#popupLayer').bPopup({

		modalClose: false,
		content:'iframe', //'ajax', 'iframe' or 'image'
		iframeAttr:'frameborder=auto',
		iframeAttr:'frameborder=0',
		contentContainer:'.popupContent',
		loadUrl:'./item_excelview.php'

	});

}

</script>

<?php
include_once (G5_ADMIN_PATH.'/admin.tail.php');
?>

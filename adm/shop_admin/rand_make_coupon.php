<?php
$sub_menu = '400801';
include_once('./_common.php');

auth_check($auth[$sub_menu], "r");

include_once(G5_PATH.'/head.sub.php');

if( empty($cp_no) ){
	echo "필수값이 없습니다.";
	exit;
}

?>
<div class="tbl_head01 tbl_wrap" style="padding:10px;margin-top:35px;">

<form name="frm">
	<input type="hidden" name="cp_no" value="<?=$cp_no?>"/>
    <table>
    <thead>
	<tr>
        <th scope="col">쿠폰번호생성</th>
	</tr>
	</thead>
	<tbody>
		<tr>
			<td>
				생성갯수
				<input type="text" id="cp_make_cnt" name="cp_make_cnt" style="width:100px;" autocomplete="off"/>&nbsp;
				<input type="button" value="생성하기" onclick="javascript:FrmSend();"/>
			</td>
		</tr>
	</tbody>
	</table>
	
	<div id="view_msg" style="color:red;width:100%;text-align:center;"></div>

</form>

<iframe src="" id="iFrm" name="iFrm" frameborder="0"/></iframe>

</div>

<script>

function FrmSend(){

	var frm = document.frm;

	if( frm.cp_make_cnt.value == "" ){
		alert("생성갯수를 입력해주세요");
		return;
	}

	frm.target = "iFrm";
	frm.method = "post";
	frm.action = "./rand_make_coupon_proc.php";
	frm.submit();

}


function ClosePop(){

	parent.location.reload();

}

</script>
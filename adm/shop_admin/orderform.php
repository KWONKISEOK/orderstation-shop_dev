<?php
$sub_menu = '400400';
include_once('./_common.php');
include_once('./admin.shop.lib.php');

if( $od_status == "" ){
	$sub_menu = '400400';
	$TopTitle = "주문";
}else if( $od_status == "주문" ){
	$sub_menu = '400401';
	$TopTitle = $od_status;
}else if( $od_status == "확정" ){
	$sub_menu = '400402';
	$TopTitle = $od_status;
}else if( $od_status == "배송" ){
	$sub_menu = '400403';
	$TopTitle = $od_status;
}else if( $od_status == "완료" ){
	$sub_menu = '400404';
	$TopTitle = $od_status;
}else if( $od_status == "취소" ){
	$sub_menu = '400406';
	$TopTitle = $od_status;
}else if( $od_status == "반품접수" ){
	$sub_menu = '400411';
	$TopTitle = $od_status;
}else if( $od_status == "반품반려" ){
	$sub_menu = '400409';
	$TopTitle = $od_status;
}else if( $od_status == "반품완료" ){
	$sub_menu = '400410';
	$TopTitle = $od_status;
}else if( $od_status == "반품전체" ){
	$sub_menu = '400411';
	$TopTitle = $od_status;
}else if( $od_status == "반품승인" ){
	$sub_menu = '400412';
	$TopTitle = $od_status;
}

$cart_title3 = '주문번호';
$cart_title4 = '배송완료';

auth_check($auth[$sub_menu], "w");

$g5['title'] = $TopTitle." 내역 수정";
include_once(G5_ADMIN_PATH.'/admin.head.php');


if( $od_status != "반품접수" && $od_status != "반품반려" && $od_status != "반품완료" && $od_status != "반품전체" ){
	// 완료된 주문에 포인트를 적립한다.
	save_order_point("완료");
}

//반품일경우 조건 쿼리
if( strpos($od_status,"반품") !== false ){
	$add_sql = " and instr( a.od_status , '반품' ) > 0  ";
}



//------------------------------------------------------------------------------
// 주문서 정보
//------------------------------------------------------------------------------
$sql = " select * from {$g5['g5_shop_order_table']} where od_id = '$od_id' ";
$od = sql_fetch($sql);
//debug2($od);
if (!$od['od_id']) {
    alert("해당 주문번호로 주문서가 존재하지 않습니다.");
}

$od['mb_id'] = $od['mb_id'] ? $od['mb_id'] : "비회원";
//------------------------------------------------------------------------------

// 결제방법
$s_receipt_way = $od['od_settle_case'];

if($od['od_settle_case'] == '간편결제') {
	switch($od['od_pg']) {
		case 'lg':
			$s_receipt_way = 'PAYNOW';
			break;
		case 'inicis':
			$s_receipt_way = 'KPAY';
			break;
		case 'kcp':
			$s_receipt_way = 'PAYCO';
			break;
		default:
			$s_receipt_way = $row['od_settle_case'];
			break;
	}
}

switch($od['od_gubun']) {
	case '1':
		$od_gubun1 = '고객주문';
		break;
	case '2':
		$od_gubun1 = '약국주문';
		break;
	case '3':
		$od_gubun1 = '일반주문';
		break;
}

$pg_anchor = '<ul class="anchor">
<li><a href="#anc_sodr_list">주문상품 목록</a></li>
<li><a href="#anc_sodr_pay">주문결제 내역</a></li>
<li><a href="#anc_sodr_chk">결제상세정보 확인</a></li>
<li><a href="#anc_sodr_paymo">결제상세정보 수정</a></li>
<li><a href="#anc_sodr_memo">상점메모</a></li>
<li><a href="#anc_sodr_orderer">주문하신 분</a></li>
<li><a href="#anc_sodr_taker">받으시는 분</a></li>
</ul>';

$html_receipt_chk = '<input type="checkbox" id="od_receipt_chk" value="'.$od['od_misu'].'" onclick="chk_receipt_price()">
<label for="od_receipt_chk">결제금액 입력</label><br>';
/*
$qstr1 = "od_status=".urlencode($od_status)."&amp;od_settle_case=".urlencode($od_settle_case)."&amp;od_misu=$od_misu&amp;od_cancel_price=$od_cancel_price&amp;od_refund_price=$od_refund_price&amp;od_receipt_point=$od_receipt_point&amp;od_coupon=$od_coupon&amp;fr_date=$fr_date&amp;to_date=$to_date&amp;sel_field=$sel_field&amp;search=$search&amp;save_search=$search";
*/
$qstr1 = "od_status=".urlencode($od_status)."&amp;od_settle_case=".urlencode($od_settle_case)."&amp;od_date=$od_date&amp;comp_code=$comp_code&amp;od_pay_yn=$od_pay_yn&amp;od_gubun=$od_gubun&amp;od_period_yn=$od_period_yn&amp;fr_date=$fr_date&amp;to_date=$to_date&amp;sel_field=$sel_field&amp;search=$search&amp;save_search=$search";

$qstr = "$qstr1&amp;sort1=$sort1&amp;sort2=$sort2&amp;page=$page";

// 상품목록
if ($is_admin == 'vender') {
	$sql = " select a.od_num
					 ,b.it_name
					 ,b.it_id
					 ,b.od_send_cost
					 ,b.od_option
					 ,b.io_id
					 ,b.od_period_yn
					 ,b.od_period_cnt
					 ,b.od_sum_drug_price
					 ,b.od_sum_incen
					 ,b.od_total_drug_price
					 ,b.od_total_incen					 
					 ,a.od_b_name
					 ,a.od_b_tel
					 ,a.od_b_hp
					 ,a.od_b_zip1
					 ,a.od_b_zip2
					 ,a.od_b_addr1
					 ,a.od_b_addr2
					 ,a.od_b_addr3
					 ,a.od_b_addr_jibeon
					 ,a.od_seq
					 ,a.od_date1
					 ,a.od_date2
					 ,a.od_date3
					 ,a.od_date4
					 ,a.od_qty
					 ,a.od_period_date
					 ,a.od_status
					 ,a.od_pay_yn
					 ,a.od_delivery_company
					 ,a.od_invoice
					 ,a.od_trans_memo	     
					 ,(select comp_name from tbl_member where comp_code = b.comp_code) comp_name
					 ,a.od_return_date_1
					 ,a.od_return_date_2
					 ,a.od_return_date_3
					 ,a.od_return_method
					 ,a.od_return_confirm, a.od_return_delivery_company , a.od_return_delivery_url
			   from tbl_shop_order_receiver a,  tbl_shop_order_detail b
			  where a.od_id = b.od_id
			    and a.od_num = b.od_num
			    and a.od_id = '{$od['od_id']}'
				and comp_code = '{$member['comp_code']}' {$add_sql}
			  order by comp_code, od_seq ";
} else {
	$sql = " select  a.od_num,c.od_gubun
					 ,b.it_name
					 ,b.it_id
					 ,b.od_send_cost
					 ,b.od_option
					 ,b.io_id
					 ,b.od_period_yn
					 ,b.od_period_cnt
					 ,b.od_sum_drug_price
					 ,b.od_sum_incen
					 ,b.od_total_drug_price
					 ,b.od_total_incen
			         ,b.od_total_sale_price
					 ,b.od_sum_sale_price
					 ,a.od_b_name
					 ,a.od_b_tel
					 ,a.od_b_hp
					 ,a.od_b_zip1
					 ,a.od_b_zip2
					 ,a.od_b_addr1
					 ,a.od_b_addr2
					 ,a.od_b_addr3
					 ,a.od_b_addr_jibeon
					 ,a.od_seq
					 ,a.od_date1
					 ,a.od_date2
					 ,a.od_date3
					 ,a.od_date4
					 ,a.od_qty
					 ,a.od_period_date
					 ,a.od_status
					 ,a.od_pay_yn
					 ,a.od_delivery_company
					 ,a.od_invoice
					 ,a.od_trans_memo	               					
					, (select comp_name from tbl_member where comp_code = b.comp_code) comp_name
					 ,a.od_return_date_1
					 ,a.od_return_date_2
					 ,a.od_return_date_3
					 ,a.od_return_method
					 ,a.od_return_confirm, a.od_return_delivery_company , a.od_return_delivery_url
					 ,a.service_type
					 ,c.pharm_name
                     ,c.mb_id 
					 ,(select pharm_custno from tbl_member where mb_id = c.mb_id) pharm_custno
					 from tbl_shop_order c inner join tbl_shop_order_detail b on c.od_id = b.od_id 
						  inner join tbl_shop_order_receiver a on b.od_id = a.od_id and b.od_num = a.od_num
				where a.od_id = '{$od['od_id']}' {$add_sql}
			  order by comp_code, od_seq ";

		
}

$result = sql_query($sql);


// add_javascript('js 구문', 출력순서); 숫자가 작을 수록 먼저 출력됨
add_javascript(G5_POSTCODE_JS, 0);    //다음 주소 js
?>


<section id="anc_sodr_list">
    <h2 class="h2_frm">주문상품 목록</h2>
    <!-- <?php echo $pg_anchor; ?> -->
    <div class="local_desc02 local_desc">
        <p>

            주문일시 <strong><?php echo substr($od['od_time'],0,16); ?> (<?php echo get_yoil($od['od_time']); ?>)</strong>&nbsp;&nbsp;&nbsp;
			주문번호 <strong><?php echo $od['od_id'];?></strong>&nbsp;&nbsp;
            <!--|
            주문총액 <strong><?php echo number_format($od['od_cart_price'] + $od['od_send_cost'] + $od['od_send_cost2']); ?></strong>원
			-->
			주문구분 : <strong><?php echo $od_gubun1; ?></strong>&nbsp;&nbsp;&nbsp;
			결제방법 : <strong><?php echo $s_receipt_way; ?></strong>&nbsp;&nbsp;&nbsp;
            <?php if ($od['pharm_name']) { ?>
            약국명(약국코드) : <strong><?php echo $od['pharm_name']; ?>(<?php echo $od['pharm_custno']; ?>)</strong>&nbsp;&nbsp;&nbsp;
            <?php } ?>
			메시지 : <strong><?php echo $od['od_memo']; ?></strong>

        </p>
        <?php if ($default['de_hope_date_use']) { ?><p>희망배송일은 <?php echo $od['od_hope_date']; ?> (<?php echo get_yoil($od['od_hope_date']); ?>) 입니다.</p><?php } ?>
        <?php if($od['od_mobile']) { ?>
        <p>모바일 쇼핑몰의 주문입니다.</p>
        <?php } ?>
    </div>

    <form name="frmorderform" method="post" action="./orderformdetailupdate.php" onsubmit="return form_submit(this);">
    <input type="hidden" name="od_id" value="<?php echo $od_id; ?>">
    <input type="hidden" name="mb_id" value="<?php echo $od['mb_id']; ?>">
    <input type="hidden" name="od_email" value="<?php echo $od['od_email']; ?>">
    <input type="hidden" name="sort1" value="<?php echo $sort1; ?>">
    <input type="hidden" name="sort2" value="<?php echo $sort2; ?>">
    <input type="hidden" name="sel_field" value="<?php echo $sel_field; ?>">
    <input type="hidden" name="search" value="<?php echo $search; ?>">
    <input type="hidden" name="page" value="<?php echo $page;?>">
    <input type="hidden" name="pg_cancel" value="0">
	<input type="hidden" name="return_gubun" value=""/><!--1:반품반려,2:반품완료-->

    <div class="tbl_head01 tbl_wrap">
        <table>
        <caption>주문 상품 목록</caption>
        <thead>
        <tr>
			<?php if ($is_admin == 'super') {?>
			<th scope="col">공급사명</th>
			<?php } ?>
			<th scope="col">
			<? if( strpos($_GET["od_status"],"반품") === false ){ ?>
                <label for="sit_select_all" class="sound_only">주문 상품 전체</label>
                <input type="checkbox" id="sit_select_all">
			<? } ?>
            </th>
            <th scope="col" width="50">이미지</th>   
			<th scope="col">상품명</th>  
			
			<th scope="col">정기주문</th>

            <th scope="col">상태</th>
            <th scope="col">수량</th>
			<?php if ($is_admin == 'super') {?>
            <th scope="col">개별판매가<br/>(개별옵션가)</th>
            <th scope="col">합계<br/>(+옵션합계)</th>
			<?php } ?>
            <th scope="col">개별 배송비</th>
			

        </tr>
        </thead>
        <tbody>
        <?php

        $chk_cnt = 0;
		$click_cnt_1 = 0;
		$click_cnt_2 = 0;
		$druc_sum = 0;
        for($i=0; $row=sql_fetch_array($result); $i++) {

            // 상품이미지
            $image = get_it_image($row['it_id'], 50, 50);

            $sc_price = "select it_sc_price from tbl_shop_item where it_id = '{$row['it_id']}'";
            $re_sc = sql_fetch($sc_price);
            ?>
            <tr>
				<?php if ($is_admin == 'super') {?>
                <td><?php echo $row['comp_name'];?></td>
				<?php } ?>


				<td class="">
					<? 
					//순번으로 넘어가기때문에 안보이는 상황은 display:none 처리로 대신해준다.
					if( ( $row['od_return_confirm'] == "N" && $row['od_status'] == "반품접수" && $is_admin == 'vender' ) || ( $row['od_return_confirm'] == "Y" && $row['od_status'] == "반품접수" && $is_admin == 'super' ) || strpos($row['od_status'],"반품") === false ){ ?>
                    <label for="ct_chk_<?php echo $chk_cnt; ?>" class="sound_only"><?php echo get_text($row['od_option']); ?></label>
                    <input type="checkbox" name="ct_chk[]" id="ct_chk_<?php echo $chk_cnt; ?>" value="<?php echo $chk_cnt; ?>" class="sct_sel_<?php echo $i; ?>">
                    <input type="hidden" name="it_id[<?php echo $chk_cnt; ?>]" value="<?php echo $row['it_id']; ?>">
					<input type="hidden" name="od_num[<?php echo $chk_cnt; ?>]" value="<?php echo $row['od_num']; ?>">
					<input type="hidden" name="od_seq[<?php echo $chk_cnt; ?>]" value="<?php echo $row['od_seq']; ?>">
                    <input type="hidden" name="od_pay_yn[<?php echo $i ?>]" value="<?php echo $row['od_pay_yn']; ?>" />
					<?}else{?>
					<div style="display:none;">
						<label for="ct_chk_<?php echo $chk_cnt; ?>" class="sound_only"><?php echo get_text($row['od_option']); ?></label>
						<input type="checkbox" name="ct_chk[<?php echo $chk_cnt; ?>]" id="ct_chk_<?php echo $chk_cnt; ?>" value="<?php echo $chk_cnt; ?>" class="sct_sel_<?php echo $i; ?>">
						<input type="hidden" name="it_id[<?php echo $chk_cnt; ?>]" value="<?php echo $row['it_id']; ?>">
						<input type="hidden" name="od_num[<?php echo $chk_cnt; ?>]" value="<?php echo $row['od_num']; ?>">
						<input type="hidden" name="od_seq[<?php echo $chk_cnt; ?>]" value="<?php echo $row['od_seq']; ?>">
                        <input type="hidden" name="od_pay_yn[<?php echo $i ?>]" value="<?php echo $row['od_pay_yn']; ?>" />
					</div>
					<? } ?>
                </td>

                <td  class="td_left">
                    <a href="./itemform.php?w=u&amp;it_id=<?php echo $row['it_id']; ?>"><?php echo $image; ?> </a>

			
                </td>
				 <td  class="td_left">
					<?php echo stripslashes($row['it_name']); ?> <br/><?php echo stripslashes($row['od_option']); ?> <?php echo substr( $row['io_id'],7,30 ); ?>
					
					<? if( ( $is_admin == 'vender' || $is_admin == 'super' ) && ( $row['od_return_method'] == "1" || $row['od_return_method'] == "2" ) ){ ?>
						<br><br>
						<font color='blue'>
						<strong>반품 배송비 결제 방법</strong> : 
						<? if( $row['od_return_method'] == "1" ){ ?>
							계좌입금
						<? }else if( $row['od_return_method'] == "2" ){ ?>
							택배 박스에 동봉
						<? } ?>
						</font>
						<br><br>
					<? } ?>
                </td>
				
				<td class=" ">	
					<table style="width:55%;margin:0 auto;">
					<tr>
						<td style="border:1px solid #fff;">
							<?php if($row['od_period_yn'] == 'Y') { ?>
							정기 <?php echo $row['od_seq']  ;?> 회차	<br/>
							<?php echo $row['od_period_date']  ;?>
							<?php } ?>
						</td>
						<? if( $is_admin == 'super' ){ ?>
						<td style="border:1px solid #fff;"> 
							<?php if($row['od_period_yn'] == 'Y') { ?>
							<input type="button" value="일자변경" onclick="javascript:ChangeDate('<?=$od_id?>','<?=$row['od_seq']?>');"/>
							<?php } ?>
						</td>
						<? } ?>
					</tr>
					</table>
				</td>
				
                
                <td class="" width="150">
				 <input type="hidden" name="current_status2[<?php echo $i ?>]" value="<?php echo $row['od_status']; ?>" />
					 <?
						if( $row['od_return_confirm'] == "Y" && $row['od_status'] == "반품접수" ){
							echo "<font color='red'>반품승인</font>";
						}else{
							echo "<strong>".$row['od_status']."</strong>";
						}					
					 ?>	
				</td>
                <td class="td_num">
                    <?php echo number_format($row['od_qty']); ?>
                </td>
				<?php if ($is_admin == 'super') {?>
				<td class="td_left ">
				 <?php if($row['service_type'] == 'B' && $row['pharm_name'] == ''){?>
					상품 : <?php echo number_format($row['od_sum_sale_price']); ?><br/>
					수수료 : <?php echo number_format($row['od_sum_incen']); ?>
				 <?php }else{ ?>
					상품 : <?php echo number_format($row['od_sum_drug_price']); ?><br/>
					수수료 : <?php echo number_format($row['od_sum_incen']); ?>
				 <?php } ?>
				</td>
                <td class="td_left ">
					상품 : <?php
					if($row['service_type'] == 'B' && $row['pharm_name'] == ''){
						echo number_format($row['od_total_sale_price']); 
					}else{
						echo number_format($row['od_total_drug_price']); 
					}?><br/>
					<?php if($row['od_period_yn'] == 'Y') { 
						$druc_sum=$row['od_total_drug_price'];
					}else {
						$druc_sum=$druc_sum+$row['od_total_drug_price'];
					}
					?>
					수수료 : <?php echo number_format($row['od_total_incen']); ?>
				</td>

				<?php } ?>
                <!-- <td class="td_num_right ">0</td>
				<td class="td_num_right ">0</td> -->
				

                <td class=" td_num_right">
                    <?php if($od['od_multi_yn'] == "N"){
                        echo number_format($row['od_send_cost']);
                    } else if($od['od_send_cost'] > 0){
                        echo number_format($re_sc['it_sc_price']);
                    } else{
                       "0";
                    } ?></td>
            </tr>
        <?php
			$chk_cnt++;

			//공급사가 승인하지 않은 반품접수 갯수구하기
			if( $row['od_return_confirm'] == "N" && $row['od_status'] == "반품접수" && $is_admin == 'vender' ){
				$click_cnt_1++;
			}

			//공급사가 승인한 반품승인건 갯수 구하기 
			if( $row['od_return_confirm'] == "Y" && $row['od_status'] == "반품접수" && $is_admin == 'super' ){
				$click_cnt_2++;			
			}

        }
        ?>
        </tbody>
        </table>
    </div>

        <?  if(strpos($auth[$sub_menu], 'b') == false) {?>
	 <div class="btn_list02 btn_list">
        <p>
            <input type="hidden" name="chk_cnt" value="<?php echo $chk_cnt; ?>">
            <input type="hidden" name="od_gubun" value="<?php echo $od['od_gubun']; ?>" />
			<? if(strpos($od_status,"반품") === false){ ?>
            <strong>주문 상태 변경</strong>           
            <input type="submit" name="od_status" value="확정" onclick="document.pressed=this.value" class="btn_02 color_03">
            <input type="submit" name="od_status" value="취소" onclick="document.pressed=this.value" class="btn_02 color_06"> 
			<? }else if( $od_status != "반품완료" && ( $od_status == "반품접수" || $od_status == "반품승인" || $od_status == "반품전체" ) ){ ?>

					<? if( $is_admin == 'vender' && $click_cnt_1 > 0 ){ ?>
					<strong>반품 상태 변경</strong>
					<input type="text" name="admin_memo" id="admin_memo" required class="frm_input required" size="40" maxlength="255" placeholder="반품반려사유" value="<?=$od['od_return_admin_memo']?>">
					<input type="button" name="od_status" value="반품반려" onclick="javascript:ReturnSendProc('1');" class="btn_02 color_03">
					<? } ?>
					<? if( $is_admin == 'super' && $click_cnt_2 > 0 ){ ?>
					<strong>반품 상태 변경</strong>
					<input type="button" name="od_status" value="반품완료" onclick="javascript:ReturnSendProc('2');" class="btn_02 color_03"> 
					<? } ?>
					<br><br>

			<? } ?>         
        </p>
		<?php if ($is_admin == 'super') {?>
		<a href="./orderlist.php?<?php echo $qstr; ?>" class="btn btn_02">목록</a>
		<?php } ?>
		<?php if ($is_admin == 'vender') {?>
		<a href="./orderlist_vender.php?<?php echo $qstr; ?>" class="btn btn_02">목록</a>
		<?php } ?>
		<?php if ($is_admin == 'sales') {?>
		<a href="./orderlist_sales.php?<?php echo $qstr; ?>" class="btn btn_02">목록</a>
		<?php } ?>
    </div>

<? } ?>
    
    </form>

</section>

<?php if($od['od_test']) { ?>
<div class="od_test_caution">주의) 이 주문은 테스트용으로 실제 결제가 이루어지지 않았으므로 절대 배송하시면 안됩니다.</div>
<?php } ?>



	<style>
ul.delivery {
    list-style:none;
    margin:0;
    padding:0;
}

ul.delivery li {
    margin: 2px;
    padding: 2px;
    border : 0;
    float: left;
	color:red;
}
</style>



<section id="anc_sodr_list">
    <h2 class="h2_frm">배송정보</h2>
    <!-- <?php echo $pg_anchor; ?> -->

    <form name="frmorderform2" method="post" action="./orderformtransupdate.php" onsubmit="return form_submit2(this);">
	<input type="hidden" name="od_status" value="<?php echo $_GET["od_status"]; ?>">
    <input type="hidden" name="od_id" value="<?php echo $od_id; ?>">
    <input type="hidden" name="mb_id" value="<?php echo $od['mb_id']; ?>">
    <input type="hidden" name="od_email" value="<?php echo $od['od_email']; ?>">
    <input type="hidden" name="sort1" value="<?php echo $sort1; ?>">
    <input type="hidden" name="sort2" value="<?php echo $sort2; ?>">
    <input type="hidden" name="sel_field" value="<?php echo $sel_field; ?>">
    <input type="hidden" name="search" value="<?php echo $search; ?>">
    <input type="hidden" name="page" value="<?php echo $page;?>">
    <input type="hidden" name="pg_cancel" value="0">
	<input type="hidden" name="od_multi_yn" value="<?=$od['od_multi_yn']?>"/>

    <div class="tbl_head01 tbl_wrap">
        <table>
        <caption>주문 상품 목록</caption>
        <thead>
        <tr>
			<?php if ($is_admin == 'super') {?>
			<th scope="col">공급사명</th>
			<?php } ?>
			<th scope="col">
				<? if( strpos($_GET["od_status"],"반품") === false ){ ?>
                <label for="sit_select_all_trans" class="sound_only">주문 상품 전체</label>
                <input type="checkbox" id="sit_select_all_trans">
				<? } ?>
            </th>
            <th scope="col" width="50">이미지</th>   
			<th scope="col">상품명</th>   
            <th scope="col">배송상태</th>
            <th scope="col">택배/운송장번호</th>

        </tr>
        </thead>
        <tbody>
        <?php
		$result = sql_query($sql);
        $chk_cnt = 0;
        for($i=0; $row=sql_fetch_array($result); $i++) {
            // 상품이미지
            $image = get_it_image($row['it_id'], 50, 50);
			
			

			$od_status = $row['od_status'];

			$invoice_time = is_null_time($row['od_invoice_time']) ? G5_TIME_YMDHIS : $row['od_invoice_time'];
			$delivery_company = $row['od_delivery_company'] ? $row['od_delivery_company'] : $default['de_delivery_company'];
            ?>
            <tr>
				<?php if ($is_admin == 'super') {?>
                <td><?php echo $row['comp_name'];?></td>
				<?php } ?>


				<td class="">
					<? 
					$od_check_disabled = "";
					if ($row['od_status'] != "확정" && $row['od_status'] != "주문" && $row['od_status'] !="배송") {
						$od_check_disabled = "disabled";
					}

					if( $row['od_pay_yn'] == "N" && $row['od_period_yn'] == 'Y' && $row['od_gubun'] == '3'){
						$od_check_disabled = "disabled";			
					}

					if ( $row['od_status'] == "반품접수" ) {
						$od_check_disabled = "";
					}

					//순번으로 넘어가기때문에 안보이는 상황은 display:none 처리로 대신해준다.
					if( ( $row['od_return_confirm'] == "N" && $row['od_status'] == "반품접수" ) || strpos($row['od_status'],"반품") === false ){ ?>
                    <label for="trans_chk_<?php echo $chk_cnt; ?>" class="sound_only"><?php echo get_text($row['od_option']); ?></label>
                    <input type="checkbox" name="trans_chk[]" id="trans_chk_<?php echo $chk_cnt; ?>" value="<?php echo $chk_cnt; ?>" class="sct_sel_<?php echo $i; ?>" <?php echo $od_check_disabled;?>>
                    <input type="hidden" name="it_id[<?php echo $chk_cnt; ?>]" value="<?php echo $row['it_id']; ?>">
					<input type="hidden" name="od_num[<?php echo $chk_cnt; ?>]" value="<?php echo $row['od_num']; ?>">
					<input type="hidden" name="od_seq[<?php echo $chk_cnt; ?>]" value="<?php echo $row['od_seq']; ?>">
					<? }else{ ?>
					<div style="display:none;">
                    <label for="trans_chk_<?php echo $chk_cnt; ?>" class="sound_only"><?php echo get_text($row['od_option']); ?></label>
                    <input type="checkbox" name="trans_chk[]" id="trans_chk_<?php echo $chk_cnt; ?>" value="<?php echo $chk_cnt; ?>" class="sct_sel_<?php echo $i; ?>" <?php echo $od_check_disabled;?>>
                    <input type="hidden" name="it_id[<?php echo $chk_cnt; ?>]" value="<?php echo $row['it_id']; ?>">
					<input type="hidden" name="od_num[<?php echo $chk_cnt; ?>]" value="<?php echo $row['od_num']; ?>">
					<input type="hidden" name="od_seq[<?php echo $chk_cnt; ?>]" value="<?php echo $row['od_seq']; ?>">
					</div>
					<? } ?>

                </td>

                <td  class="td_left">
                    <a href="./itemform.php?w=u&amp;it_id=<?php echo $row['it_id']; ?>"><?php echo $image; ?> </a>		
                </td>
				<td  class="td_left" width="30%">
					<?php echo stripslashes($row['it_name']); ?> 
					<?php if($row['od_period_yn'] == 'Y') { ?>
					 - <?php echo $row['od_period_date']  ;?>
					<?php } ?>
					<br/><?php echo stripslashes($row['od_option']); ?> <?php echo substr( $row['io_id'],7,30 ); ?>
                </td>
                
                <td  class="td_left" width="20%">
				       <input type="hidden" name="current_status[<?php echo $i ?>]" value="<?php echo $od_status; ?>" />
						주문 : <?php echo $row['od_date1'] ?><br/>
						확정 : <?php echo $row['od_date2'] ?><br/>
						배송 : <?php echo $row['od_date3'] ?><br/>
						완료 : <?php echo $row['od_date4'] ?><br/>
						<? if(strpos($row['od_status'],"반품") !== false){ ?>
						
						반품접수 : <?if( $row['od_return_date_1'] != "0000-00-00 00:00:00" ){ echo $row['od_return_date_1']; } ?><br/>
						반품반려 : <?if( $row['od_return_date_2'] != "0000-00-00 00:00:00" ){ echo $row['od_return_date_2']; } ?><br/>
						반품완료 : <?if( $row['od_return_date_3'] != "0000-00-00 00:00:00" ){ echo $row['od_return_date_3']; } ?><br/>
						<? } ?>
				</td>
               <td  class="td_left" width="30%">
				<? 
				// 다중배송이 아닐경우
				if( $od['od_multi_yn'] == "N" ){ 
				?>
                    <ul class='delivery'>
					<li>
					<?php if ($row['od_status'] != '주문') { ?>
						<select name="od_delivery_company[<?php echo $i; ?>]">
							<?php echo get_delivery_company($delivery_company); ?>
						</select>
					<?php } else {
						echo ($row['od_delivery_company'] ? $row['od_delivery_company'] : '');
					} ?>
					</li>

					<li>
				  <?php if ($row['od_status'] != '주문') { ?>
						<input type="text" name="od_invoice[<?php echo $i; ?>]" value="<?php echo $row['od_invoice']; ?>" class="frm_input2" size="20">
					<?php } else {
						echo ($row['od_invoice'] ? $row['od_invoice'] : '');
					} ?>
					</li>
					</ul>
					<br/>
					<?php
                    if($row['od_delivery_company'] != "한진택배"){
                        echo get_delivery_inquiry($row['od_delivery_company'], $row['od_invoice'], 'dvr_link');
                    }else{
                        echo get_delivery_inquiry_han($row['od_delivery_company'], $row['od_invoice'], 'dvr_link');
                    }
                    ?>
				<? } ?>
                </td>
	
            </tr>

            <tr>
			<? 
			// 다중배송이 아닐경우
			if( $od['od_multi_yn'] == "N" ){ ?>
			<td colspan="6">
                <style>
                    /*#MultiAddressList {table-layout: fixed; white-space: nowrap;}*/
                    #AddressList .frm_input {width:100%;}
                    #AddressList .delivery select {width:100%;}
                    #AddressList .delivery input {width:100%;}
                </style>
                <table id="AddressList">
                    <tr>
                        <th width="50">수량</th>
                        <th width="100">수령인</th>
                        <th width="120">핸드폰</th>
                        <th width="80">우편번호</th>
                        <th width="250">기본주소</th>
                        <th width="200">상세주소</th>
                        <th width="150">주소참고</th>
                        <th width="150">배송주의사항</th>
                        <th width="300">반품택배사/URL</th>
                    </tr>
                    <tr>
                        <td><?php echo $row['od_qty'];?></td>
                        <td class="td_left">
                            <input type="text" name="od_b_name[<?php echo $i; ?>]" value="<?php echo get_text($row['od_b_name']); ?>" id="od_b_name"  class="frm_input required">
                        </td>
                        <td>
                            <input type="text" name="od_b_hp[<?php echo $i; ?>]" value="<?php echo get_text($row['od_b_hp']); ?>" id="od_b_hp" class="frm_input required">
                        </td>
                        <td>
                            <input type="text" name="od_b_zip[<?php echo $i; ?>]" value="<?php echo $row['od_b_zip1'].$row['od_b_zip2']; ?>" id="od_b_zip"  class="frm_input required">
                        </td>
                        <td>
                            <input type="text" name="od_b_addr1[<?php echo $i; ?>]" value="<?php echo get_text($row['od_b_addr1']); ?>" id="od_b_addr1"  class="frm_input required">
                        </td>
                        <td>
                            <input type="text" name="od_b_addr2[<?php echo $i; ?>]" value="<?php echo get_text($row['od_b_addr2']); ?>" id="od_b_addr2" class="frm_input">
                        </td>
                        <td>
                            <input type="text" name="od_b_addr3[<?php echo $i; ?>]" value="<?php echo get_text($row['od_b_addr3']); ?>" id="od_b_addr3" class="frm_input">
                            <input type="hidden" name="od_b_addr_jibeon" value="<?php echo get_text($row['od_b_addr_jibeon']); ?>">
                        </td>
                        <td>
                            <input type="text" name="od_trans_memo[<?php echo $i; ?>]" value="<?php echo get_text($row['od_trans_memo']); ?>" id="od_trans_memo" class="frm_input">
                        </td>
                        <td>
                            <? if( $row['od_return_confirm'] == "Y" ){ ?>
                                <font color='red'>
                                    반품택배사 : <?=$row['od_return_delivery_company']?><br>
                                    반품택배사url : <?=$row['od_return_delivery_url']?>
                                </font>
                            <? } ?>
                        </td>
                    </tr>
                </table>
            </td>
			<? 
			}else if( $od['od_multi_yn'] == "Y" ){ 
									
				$multi_sql = " select * from tbl_shop_order_multi_receiver where od_id = '". $od_id ."' ";
				$result = sql_query($multi_sql);
			?>
			<td colspan="6" style="overflow-x: auto;">
                <h3 align="left" style="margin-bottom: 5px;">다중배송지 리스트</h3>
                <style>
                    /*#MultiAddressList {table-layout: fixed; white-space: nowrap;}*/
                    #MultiAddressList .frm_input {width:100%;}
                    #MultiAddressList .delivery select {width:100%;}
                    #MultiAddressList .delivery input {width:100%;}
                </style>
                <table id="MultiAddressList">
                    <tr>
                        <th width="50">수량</th>
                        <th width="100">수령인</th>
                        <th width="120">핸드폰</th>
                        <th width="80">우편번호</th>
                        <th width="250">기본주소</th>
                        <th width="200">상세주소</th>
                        <th width="150">주소참고</th>
                        <th width="150">배송주의사항</th>
                        <th width="300">택배/운송장번호</th>
                    </tr>
                    <? for($i=0; $row=sql_fetch_array($result); $i++) {

                        $invoice_time = is_null_time($row['od_invoice_time']) ? G5_TIME_YMDHIS : $row['od_invoice_time'];
                        $delivery_company = $row['od_delivery_company'] ? $row['od_delivery_company'] : $default['de_delivery_company'];

                    ?>
                    <tr>
                        <td><?php echo $row['od_qty'];?></td>
                        <td class="td_left">
                            <input type="hidden" name="od_num[<?php echo $i; ?>]" value="<?php echo get_text($row['od_num']); ?>"/>
                            <input type="hidden" name="od_seq[<?php echo $i; ?>]" value="<?php echo get_text($row['od_seq']); ?>"/>
                            <input type="text" name="od_b_name[<?php echo $i; ?>]" value="<?php echo get_text($row['od_b_name']); ?>" id="od_b_name"  class="frm_input required">
                        </td>
                        <td>
                            <input type="text" name="od_b_hp[<?php echo $i; ?>]" value="<?php echo get_text($row['od_b_hp']); ?>" id="od_b_hp" class="frm_input required">
                        </td>
                        <td>
                            <input type="text" name="od_b_zip[<?php echo $i; ?>]" value="<?php echo $row['od_b_zip1'].$row['od_b_zip2']; ?>" id="od_b_zip"  class="frm_input required">
                        </td>
                        <td>
                            <input type="text" name="od_b_addr1[<?php echo $i; ?>]" value="<?php echo get_text($row['od_b_addr1']); ?>" id="od_b_addr1"  class="frm_input required">
                        </td>
                        <td>
                            <input type="text" name="od_b_addr2[<?php echo $i; ?>]" value="<?php echo get_text($row['od_b_addr2']); ?>" id="od_b_addr2" class="frm_input">
                        </td>
                        <td>
                            <input type="text" name="od_b_addr3[<?php echo $i; ?>]" value="<?php echo get_text($row['od_b_addr3']); ?>" id="od_b_addr3" class="frm_input">
                            <input type="hidden" name="od_b_addr_jibeon" value="<?php echo get_text($row['od_b_addr_jibeon']); ?>">
                        </td>
                        <td>
                            <input type="text" name="od_trans_memo[<?php echo $i; ?>]" value="<?php echo get_text($row['od_trans_memo']); ?>" id="od_trans_memo" class="frm_input">
                        </td>
                        <td>
                            <ul class='delivery'>
                                <li>
                                    <?php if ($od_status != '주문') { ?>
                                        <select name="od_delivery_company[<?php echo $i; ?>]">
                                            <?php echo get_delivery_company($delivery_company); ?>
                                        </select>
                                    <?php } else {
                                        echo ($row['od_delivery_company'] ? $row['od_delivery_company'] : '');
                                    } ?>
                                </li>

                                <li>
                                    <?php if ($od_status != '주문') { ?>
                                        <input type="text" name="od_invoice[<?php echo $i; ?>]" value="<?php echo $row['od_invoice']; ?>" class="frm_input2" size="20">
                                    <?php } else {
                                        echo ($row['od_invoice'] ? $row['od_invoice'] : '');
                                    } ?>
                                </li>
                                <li>
                                    <?php echo get_delivery_inquiry($row['od_delivery_company'], $row['od_invoice'], 'dvr_link'); ?>
                                </li>
                            </ul>
                        </td>
                    </tr>
                    <? } ?>
                </table>
            </td>
			<? } ?>
			</tr>
        <?php
		$chk_cnt++;
        }
        ?>
        </tbody>
        </table>
    </div>

        <?  if(strpos($auth[$sub_menu], 'b') == false) {?>
    <div class="btn_list02 btn_list">
        <p>
            <input type="hidden" name="chk_cnt" value="<?php echo $chk_cnt; ?>">
			<? if(strpos($od_status,"반품") === false){ ?>
            <strong>주문 상태 변경</strong>           
           
             <input type="submit" name="od_status" value="배송" onclick="document.pressed=this.value" class="btn_02 color_04">
            <!-- <input type="submit" name="od_status" value="완료" onclick="document.pressed=this.value" class="btn_02 color_05"> -->
			<input type="submit" name="od_status" value="수정내역저장" onclick="document.pressed=this.value" class="btn_02 color_06">
			<? } ?>

        </p>
		<?php if ($is_admin == 'super') {?>
		<a href="./orderlist.php?<?php echo $qstr; ?>" class="btn btn_02">목록</a>
		<?php } ?>
		<?php if ($is_admin == 'vender' && strpos($_GET["od_status"],"반품접수") !== false) {?>
		<!--<a href="./orderlist_vender.php?<?php echo $qstr; ?>" class="btn btn_02">목록</a>-->
			<input type="button" name="od_return_confirm" value="승인" class="btn_02 color_04" onclick="javascript:ReturnConfirm();">
			<select name="od_return_delivery_company_select" id="od_return_delivery_company_select">
				<option value="">--반품택배사선택--</option>
				<option value="www.kdexp.com" data-company_name="경동택배">경동택배</option>
				<option value="home.daesinlogistics.co.kr" data-company_name="대신택배">대신택배</option>
				<option value="www.dongbups.com" data-company_name="동부택배">동부택배</option>
				<option value="www.ilogen.com" data-company_name="로젠택배">로젠택배</option>
				<option value="service.epost.go.kr" data-company_name="우체국">우체국</option>
				<option value="www.innogis.co.kr" data-company_name="이노지스택배">이노지스택배</option>
				<option value="www.hanjin.co.kr" data-company_name="한진택배">한진택배</option>
				<option value="www.lotteglogis.com" data-company_name="롯데택배">롯데택배</option>
				<option value="www.doortodoor.co.kr" data-company_name="CJ대한통운">CJ대한통운</option>
				<option value="was.cvsnet.co.kr" data-company_name="CVSnet편의점택배">CVSnet편의점택배</option>
				<option value="www.yellowcap.co.kr" data-company_name="KG옐로우캡택배">KG옐로우캡택배</option>
				<option value="www.kgbls.co.kr" data-company_name="KGB택배">KGB택배</option>
				<option value="www.kglogis.co.kr" data-company_name="KG로지스">KG로지스</option>
				<option value="www.kunyoung.com" data-company_name="건영택배">건영택배</option>
				<option value="www.honamlogis.co.kr" data-company_name="호남택배">호남택배</option>
			</select>
			<input type="hidden" name="od_return_delivery_company" id="od_return_delivery_company"/>
			<input type="text" name="od_return_delivery_url" id="od_return_delivery_url" class="frm_input" size="50">
			<input type="checkbox" name="od_return_confirm_check" id="od_return_confirm_check" value="Y"/>상품회수신청
		<?php } ?>
		<?php if ($is_admin == 'sales') {?>
		<a href="./orderlist_sales.php?<?php echo $qstr; ?>" class="btn btn_02">목록</a>
		<?php } ?>
    </div>
        <?php } ?>

    <!-- <div class="local_desc01 local_desc">
        <p>주문, 입금, 확정, 배송, 완료는 장바구니와 주문서 상태를 모두 변경하지만, 취소, 반품, 품절은 장바구니의 상태만 변경하며, 주문서 상태는 변경하지 않습니다.</p>
        <p>개별적인(이곳에서의) 상태 변경은 모든 작업을 수동으로 처리합니다. 예를 들어 주문에서 입금으로 상태 변경시 입금액(결제금액)을 포함한 모든 정보는 수동 입력으로 처리하셔야 합니다.</p>
    </div> -->

    </form>

</section>

	

<?php if ($is_admin == 'super') { ?>
 <section id="anc_sodr_pay">
    <h2 class="h2_frm">주문결제 내역</h2>
    

    <?php
    // 주문금액 = 상품구입금액 + 배송비 + 추가배송비
    $amount['order'] = $od['od_cart_price'] + $od['od_send_cost'] + $od['od_send_cost2'];

    // 입금액 = 결제금액 + 포인트
    $amount['receipt'] = $od['od_receipt_price'] + $od['od_receipt_point'];

    // 쿠폰금액
    $amount['coupon'] = $od['od_cart_coupon'] + $od['od_coupon'] + $od['od_send_coupon'];

    // 취소금액
    $amount['cancel'] = $od['od_cancel_price'];

    // 미수금 = 주문금액 - 취소금액 - 입금금액 - 쿠폰금액
    //$amount['미수'] = $amount['order'] - $amount['receipt'] - $amount['coupon'];

    

    if ($od['od_receipt_point'] > 0)
        $s_receipt_way .= "+포인트";
    ?>

    <div class="tbl_head01 tbl_wrap">
        <!-- <strong class="sodr_nonpay">미수금 <?php echo display_price($od['od_misu']); ?></strong> -->

        <table>
        <caption>주문결제 내역</caption>
        <thead>
        <tr>
            <th scope="col">주문번호</th>
            <th scope="col">결제방법</th>
            <th scope="col">주문총액</th>
			<?php
			if($od['od_gubun']=='2'){
				?>
			<th scope="col">약국가총액</th>
			<?php
			}
			?>
            <th scope="col">배송비</th>
            <th scope="col">상품가격</th>
            <th scope="col">포인트결제</th>
            <th scope="col">총결제액</th>
            <th scope="col">쿠폰</th>
            <th scope="col">주문취소</th>
        </tr>
        </thead> 
        <tbody>
        <tr>
            <td><?php echo $od['od_id']; ?></td>
            <td class="td_paybybig"><?php echo $s_receipt_way; ?></td>
            <td class="td_numbig td_numsum"><?php echo display_price($amount['order']-$amount['coupon']); ?></td>
			<?php
			// $od_gubun1 = '약국주문'
			if($od['od_gubun']=='2'){
				?>
			<td class="td_numbig td_numsum"><?php echo number_format($druc_sum); ?></td>
			<?php
			}
			?>
            <td class="td_numbig"><?php echo display_price($od['od_send_cost'] + $od['od_send_cost2']); ?></td>
            <td class="td_numbig"><?php echo display_price($amount['order'] - ($od['od_send_cost'] + $od['od_send_cost2']) ); ?></td>
            <td class="td_numbig"><?php echo display_point($od['od_receipt_point']); ?></td>
            <td class="td_numbig td_numincome"><?php echo number_format($amount['receipt']); ?>원</td>
            <td class="td_numbig td_numcoupon"><?php echo display_price($amount['coupon']); ?></td>
            <td class="td_numbig td_numcancel"><?php echo number_format($amount['cancel']); ?>원</td>
        </tr>
        </tbody>
        </table>
    </div>
</section>
<?php if($s_receipt_way != '무통장') { ?>
 <section class="">
    <h2 class="h2_frm">결제상세정보</h2>
   

    <form name="frmorderreceiptform" action="./orderformreceiptupdate.php" method="post" autocomplete="off">
    <input type="hidden" name="od_id" value="<?php echo $od_id; ?>">
    <input type="hidden" name="sort1" value="<?php echo $sort1; ?>">
    <input type="hidden" name="sort2" value="<?php echo $sort2; ?>">
    <input type="hidden" name="sel_field" value="<?php echo $sel_field; ?>">
    <input type="hidden" name="search" value="<?php echo $search; ?>">
    <input type="hidden" name="page" value="<?php echo $page; ?>">
    <input type="hidden" name="od_name" value="<?php echo $od['od_name']; ?>">
    <input type="hidden" name="od_hp" value="<?php echo $od['od_hp']; ?>">
    <input type="hidden" name="od_tno" value="<?php echo $od['od_tno']; ?>">
    <input type="hidden" name="od_escrow" value="<?php echo $od['od_escrow']; ?>">
    <input type="hidden" name="od_pg" value="<?php echo $od['od_pg']; ?>">

    <div class="compare_wrap">

        <section id="anc_sodr_chk" class="compare_left">
            <h3>결제상세정보 확인</h3>

            <div class="tbl_frm01">
                <table>
                <caption>결제상세정보</caption>
                <colgroup>
                    <col class="grid_3">
                    <col>
                </colgroup>
                <tbody>
                <?php if ($od['od_settle_case'] == '무통장' || $od['od_settle_case'] == '가상계좌' || $od['od_settle_case'] == '계좌이체') { ?>
                <?php if ($od['od_settle_case'] == '무통장' || $od['od_settle_case'] == '가상계좌') { ?>
                <tr>
                    <th scope="row">계좌번호</th>
                    <td><?php echo get_text($od['od_bank_account']); ?></td>
                </tr>
                <?php } ?>
                <tr>
                    <th scope="row"><?php echo $od['od_settle_case']; ?> 입금액</th>
                    <td><?php echo display_price($od['od_receipt_price']); ?></td>
                </tr>
                <tr>
                    <th scope="row">입금자</th>
                    <td><?php echo get_text($od['od_deposit_name']); ?></td>
                </tr>
                <tr>
                    <th scope="row">입금확인일시</th>
                    <td>
                        <?php if ($od['od_receipt_time'] == 0) { ?>입금 확인일시를 체크해 주세요.
                        <?php } else { ?><?php echo $od['od_receipt_time']; ?> (<?php echo get_yoil($od['od_receipt_time']); ?>)
                        <?php } ?>
                    </td>
                </tr>
                <?php } ?>

                <?php if ($od['od_settle_case'] == '휴대폰') { ?>
                <tr>
                    <th scope="row">휴대폰번호</th>
                    <td><?php echo get_text($od['od_bank_account']); ?></td>
                    </tr>
                <tr>
                    <th scope="row"><?php echo $od['od_settle_case']; ?> 결제액</th>
                    <td><?php echo display_price($od['od_receipt_price']); ?></td>
                </tr>
                <tr>
                    <th scope="row">결제 확인일시</th>
                    <td>
                        <?php if ($od['od_receipt_time'] == 0) { ?>결제 확인일시를 체크해 주세요.
                        <?php } else { ?><?php echo $od['od_receipt_time']; ?> (<?php echo get_yoil($od['od_receipt_time']); ?>)
                        <?php } ?>
                    </td>
                </tr>
                <?php } ?>

                <?php if ($od['od_settle_case'] == '신용카드') { ?>
                <tr>
                    <th scope="row" class="sodr_sppay">신용카드 결제금액</th>
                    <td>
                        <?php if ($od['od_receipt_time'] == "0000-00-00 00:00:00") {?>0원
                        <?php } else { ?><?php echo display_price($od['od_receipt_price']); ?>
                        <?php } ?>
                    </td>
                </tr>
                <tr>
                    <th scope="row" class="sodr_sppay">카드 승인일시</th>
                    <td>
                        <?php if ($od['od_receipt_time'] == "0000-00-00 00:00:00") {?>신용카드 결제 일시 정보가 없습니다.
                        <?php } else { ?><?php echo substr($od['od_receipt_time'], 0, 20); ?>
                        <?php } ?>
                    </td>
                </tr>
                <?php } ?>

                <?php if ($od['od_settle_case'] == 'KAKAOPAY') { ?>
                <tr>
                    <th scope="row" class="sodr_sppay">KAKOPAY 결제금액</th>
                    <td>
                        <?php if ($od['od_receipt_time'] == "0000-00-00 00:00:00") {?>0원
                        <?php } else { ?><?php echo display_price($od['od_receipt_price']); ?>
                        <?php } ?>
                    </td>
                </tr>
                <tr>
                    <th scope="row" class="sodr_sppay">KAKAOPAY 승인일시</th>
                    <td>
                        <?php if ($od['od_receipt_time'] == "0000-00-00 00:00:00") {?>신용카드 결제 일시 정보가 없습니다.
                        <?php } else { ?><?php echo substr($od['od_receipt_time'], 0, 20); ?>
                        <?php } ?>
                    </td>
                </tr>
                <?php } ?>

                <?php if ($od['od_settle_case'] == '간편결제' || ($od['od_pg'] == 'inicis' && is_inicis_order_pay($od['od_settle_case']) ) ) { ?>
                <tr>
                    <th scope="row" class="sodr_sppay"><?php echo $s_receipt_way; ?> 결제금액</th>
                    <td>
                        <?php if ($od['od_receipt_time'] == "0000-00-00 00:00:00") {?>0원
                        <?php } else { ?><?php echo display_price($od['od_receipt_price']); ?>
                        <?php } ?>
                    </td>
                </tr>
                <tr>
                    <th scope="row" class="sodr_sppay"><?php echo $s_receipt_way; ?> 승인일시</th>
                    <td>
                        <?php if ($od['od_receipt_time'] == "0000-00-00 00:00:00") { echo $s_receipt_way; ?> 결제 일시 정보가 없습니다.
                        <?php } else { ?><?php echo substr($od['od_receipt_time'], 0, 20); ?>
                        <?php } ?>
                    </td>
                </tr>
                <?php } ?>

                <?php if ($od['od_settle_case'] != '무통장') { ?>
                <tr>
                    <th scope="row">결제대행사 링크</th>
                    <td>
                        <?php
                        if ($od['od_settle_case'] != '무통장') {
                            switch($od['od_pg']) {
                                case 'lg':
                                    $pg_url  = 'http://pgweb.uplus.co.kr';
                                    $pg_test = 'LG유플러스';
                                    if ($default['de_card_test']) {
                                        $pg_url = 'http://pgweb.uplus.co.kr/tmert';
                                        $pg_test .= ' 테스트 ';
                                    }
                                    break;
                                case 'inicis':
                                    $pg_url  = 'https://iniweb.inicis.com/';
                                    $pg_test = 'KG이니시스';
                                    break;
                                case 'KAKAOPAY':
                                    $pg_url  = 'https://mms.cnspay.co.kr';
                                    $pg_test = 'KAKAOPAY';
                                    break;
                                default:
                                    $pg_url  = 'http://admin8.kcp.co.kr';
                                    $pg_test = 'KCP';
                                    if ($default['de_card_test']) {
                                        // 로그인 아이디 / 비번
                                        // 일반 : test1234 / test12345
                                        // 에스크로 : escrow / escrow913
                                        $pg_url = 'http://testadmin8.kcp.co.kr';
                                        $pg_test .= ' 테스트 ';
                                    }

                                }
                            echo "<a href=\"{$pg_url}\" target=\"_blank\">{$pg_test}바로가기</a><br>";
                        }
                        //------------------------------------------------------------------------------
                        ?>
                    </td>
                </tr>
                <?php } ?>

                <?php if($od['od_tax_flag']) { ?>
                <tr>
                    <th scope="row">과세공급가액</th>
                    <td><?php echo display_price($od['od_tax_mny']); ?></td>
                </tr>
                <tr>
                    <th scope="row">과세부가세액</th>
                    <td><?php echo display_price($od['od_vat_mny']); ?></td>
                </tr>
                <tr>
                    <th scope="row">비과세공급가액</th>
                    <td><?php echo display_price($od['od_free_mny']); ?></td>
                </tr>
                <?php } ?>
                <tr>
                    <th scope="row">주문금액할인</th>
                    <td><?php echo display_price($od['od_coupon']); ?></td>
                </tr>
                <tr>
                    <th scope="row">포인트</th>
                    <td><?php echo display_point($od['od_receipt_point']); ?></td>
                </tr>
                <tr>
                    <th scope="row">결제취소/환불액</th>
                    <td><?php echo display_price($od['od_refund_price']); ?></td>
                </tr>
                <?php if ($od['od_invoice']) { ?>
                <!-- <tr>
                    <th scope="row">배송회사</th>
                    <td><?php echo $od['od_delivery_company']; ?> <?php echo get_delivery_inquiry($od['od_delivery_company'], $od['od_invoice'], 'dvr_link'); ?></td>
                </tr>
                <tr>
                    <th scope="row">운송장번호</th>
                    <td><?php echo $od['od_invoice']; ?></td>
                </tr>
                <tr>
                    <th scope="row">배송일시</th>
                    <td><?php echo is_null_time($od['od_invoice_time']) ? "" : $od['od_invoice_time']; ?></td>
                </tr> -->
                <?php } ?>
                <tr>
                    <th scope="row"><label for="od_send_cost">배송비</label></th>
                    <td>
                        <input type="text" name="od_send_cost" value="<?php echo $od['od_send_cost']; ?>" id="od_send_cost" class="frm_input" size="10"> 원
                    </td>
                </tr>
                <?php if($od['od_send_coupon']) { ?>
                <tr>
                    <th scope="row">배송비할인</th>
                    <td><?php echo display_price($od['od_send_coupon']); ?></td>
                </tr>
                <?php } ?>
                <tr>
                    <th scope="row"><label for="od_send_cost2">추가배송비</label></th>
                    <td>
                        <input type="text" name="od_send_cost2" value="<?php echo $od['od_send_cost2']; ?>" id="od_send_cost2" class="frm_input" size="10"> 원
                    </td>
                </tr>
                <?php
                if ($od['od_misu'] == 0 && $od['od_receipt_price'] && ($od['od_settle_case'] == '무통장' || $od['od_settle_case'] == '가상계좌' || $od['od_settle_case'] == '계좌이체')) {
                ?>
                <tr>
                    <th scope="row">현금영수증</th>
                    <td>
                    <?php
                    if ($od['od_cash']) {
                        if($od['od_pg'] == 'lg') {
                            require G5_SHOP_PATH.'/settle_lg.inc.php';

                            switch($od['od_settle_case']) {
                                case '계좌이체':
                                    $trade_type = 'BANK';
                                    break;
                                case '가상계좌':
                                    $trade_type = 'CAS';
                                    break;
                                default:
                                    $trade_type = 'CR';
                                    break;
                            }
                            $cash_receipt_script = 'javascript:showCashReceipts(\''.$LGD_MID.'\',\''.$od['od_id'].'\',\''.$od['od_casseqno'].'\',\''.$trade_type.'\',\''.$CST_PLATFORM.'\');';
                        } else if($od['od_pg'] == 'inicis') {
                            $cash = unserialize($od['od_cash_info']);
                            $cash_receipt_script = 'window.open(\'https://iniweb.inicis.com/DefaultWebApp/mall/cr/cm/Cash_mCmReceipt.jsp?noTid='.$cash['TID'].'&clpaymethod=22\',\'showreceipt\',\'width=380,height=540,scrollbars=no,resizable=no\');';
                        } else {
                            require G5_SHOP_PATH.'/settle_kcp.inc.php';

                            $cash = unserialize($od['od_cash_info']);
                            $cash_receipt_script = 'window.open(\''.G5_CASH_RECEIPT_URL.$default['de_kcp_mid'].'&orderid='.$od_id.'&bill_yn=Y&authno='.$cash['receipt_no'].'\', \'taxsave_receipt\', \'width=360,height=647,scrollbars=0,menus=0\');';
                        }
                    ?>
                        <a href="javascript:;" onclick="<?php echo $cash_receipt_script; ?>">현금영수증 확인</a>
                    <?php } else { ?>
                        <a href="javascript:;" onclick="window.open('<?php echo G5_SHOP_URL; ?>/taxsave.php?od_id=<?php echo $od_id; ?>', 'taxsave', 'width=550,height=400,scrollbars=1,menus=0');">현금영수증 발급</a>
                    <?php } ?>
                    </td>
                </tr>
                <?php
                }
                ?>
                </tbody>
                </table>
            </div>
        </section>

        <section id="anc_sodr_paymo" class="compare_right">
            <h3>결제상세정보 수정</h3>

            <div class="tbl_frm01">
                <table>
                <caption>결제상세정보 수정</caption>
                <colgroup>
                    <col class="grid_3">
                    <col>
                </colgroup>
                <tbody>
                <?php if ($od['od_settle_case'] == '무통장' || $od['od_settle_case'] == '가상계좌' || $od['od_settle_case'] == '계좌이체') { ########## 시작?>
                <?php
                if ($od['od_settle_case'] == '무통장')
                {
                    // 은행계좌를 배열로 만든후
                    $str = explode("\n", $default['de_bank_account']);
                    $bank_account .= '<select name="od_bank_account" id="od_bank_account">'.PHP_EOL;
                    $bank_account .= '<option value="">선택하십시오</option>'.PHP_EOL;
                    for ($i=0; $i<count($str); $i++) {
                        $str[$i] = str_replace("\r", "", $str[$i]);
                        $bank_account .= '<option value="'.$str[$i].'" '.get_selected($od['od_bank_account'], $str[$i]).'>'.$str[$i].'</option>'.PHP_EOL;
                    }
                    $bank_account .= '</select> ';
                }
                else if ($od['od_settle_case'] == '가상계좌')
                    $bank_account = $od['od_bank_account'].'<input type="hidden" name="od_bank_account" value="'.$od['od_bank_account'].'">';
                else if ($od['od_settle_case'] == '계좌이체')
                    $bank_account = $od['od_settle_case'];
                ?>

                <?php if ($od['od_settle_case'] == '무통장' || $od['od_settle_case'] == '가상계좌') { ?>
                <tr>
                    <th scope="row"><label for="od_bank_account">계좌번호</label></th>
                    <td><?php echo $bank_account; ?></td>
                </tr>
                <?php } ?>

                <tr>
                    <th scope="row"><label for="od_receipt_price"><?php echo $od['od_settle_case']; ?> 입금액</label></th>
                    <td>
                        <?php echo $html_receipt_chk; ?>
                        <input type="text" name="od_receipt_price" value="<?php echo $od['od_receipt_price']; ?>" id="od_receipt_price" class="frm_input"> 원
                    </td>
                </tr>
                <tr>
                    <th scope="row"><label for="od_deposit_name">입금자명</label></th>
                    <td>
                        <?php if ($config['cf_sms_use'] && $default['de_sms_use4']) { ?>
                        <input type="checkbox" name="od_sms_ipgum_check" id="od_sms_ipgum_check">
                        <label for="od_sms_ipgum_check">SMS 입금 문자전송</label>
                        <br>
                        <?php } ?>
                        <input type="text" name="od_deposit_name" value="<?php echo get_text($od['od_deposit_name']); ?>" id="od_deposit_name" class="frm_input">
                    </td>
                </tr>
                <tr>
                    <th scope="row"><label for="od_receipt_time">입금 확인일시</label></th>
                    <td>
                        <input type="checkbox" name="od_bank_chk" id="od_bank_chk" value="<?php echo date("Y-m-d H:i:s", G5_SERVER_TIME); ?>" onclick="if (this.checked == true) this.form.od_receipt_time.value=this.form.od_bank_chk.value; else this.form.od_receipt_time.value = this.form.od_receipt_time.defaultValue;">
                        <label for="od_bank_chk">현재 시간으로 설정</label><br>
                        <input type="text" name="od_receipt_time" value="<?php echo is_null_time($od['od_receipt_time']) ? "" : $od['od_receipt_time']; ?>" id="od_receipt_time" class="frm_input" maxlength="19">
                    </td>
                </tr>
                <?php } ?>

                <?php if ($od['od_settle_case'] == '휴대폰') { ?>
                <tr>
                    <th scope="row">휴대폰번호</th>
                    <td><?php echo get_text($od['od_bank_account']); ?></td>
                </tr>
                <tr>
                    <th scope="row"><label for="od_receipt_price"><?php echo $od['od_settle_case']; ?> 결제액</label></th>
                    <td>
                        <?php echo $html_receipt_chk; ?>
                        <input type="text" name="od_receipt_price" value="<?php echo $od['od_receipt_price']; ?>" id="od_receipt_price" class="frm_input"> 원
                    </td>
                </tr>
                <tr>
                    <th scope="row"><label for="op_receipt_time">휴대폰 결제일시</label></th>
                    <td>
                        <input type="checkbox" name="od_hp_chk" id="od_hp_chk" value="<?php echo date("Y-m-d H:i:s", G5_SERVER_TIME); ?>" onclick="if (this.checked == true) this.form.od_receipt_time.value=this.form.od_hp_chk.value; else this.form.od_receipt_time.value = this.form.od_receipt_time.defaultValue;">
                        <label for="od_hp_chk">현재 시간으로 설정</label><br>
                        <input type="text" name="od_receipt_time" value="<?php echo is_null_time($od['od_receipt_time']) ? "" : $od['od_receipt_time']; ?>" id="op_receipt_time" class="frm_input" size="19" maxlength="19">
                    </td>
                </tr>
                <?php } ?>

                <?php if ($od['od_settle_case'] == '신용카드') { ?>
                <tr>
                    <th scope="row" class="sodr_sppay"><label for="od_receipt_price">신용카드 결제금액</label></th>
                    <td>
                        <?php echo $html_receipt_chk; ?>
                        <input type="text" name="od_receipt_price" id="od_receipt_price" value="<?php echo $od['od_receipt_price']; ?>" class="frm_input" size="10"> 원
                    </td>
                </tr>
                <tr>
                    <th scope="row" class="sodr_sppay"><label for="od_receipt_time">카드 승인일시</label></th>
                    <td>
                        <input type="checkbox" name="od_card_chk" id="od_card_chk" value="<?php echo date("Y-m-d H:i:s", G5_SERVER_TIME); ?>" onclick="if (this.checked == true) this.form.od_receipt_time.value=this.form.od_card_chk.value; else this.form.od_receipt_time.value = this.form.od_receipt_time.defaultValue;">
                        <label for="od_card_chk">현재 시간으로 설정</label><br>
                        <input type="text" name="od_receipt_time" value="<?php echo is_null_time($od['od_receipt_time']) ? "" : $od['od_receipt_time']; ?>" id="od_receipt_time" class="frm_input" size="19" maxlength="19">
                    </td>
                </tr>
                <?php } ?>

                <?php if ($od['od_settle_case'] == 'KAKAOPAY') { ?>
                <tr>
                    <th scope="row" class="sodr_sppay"><label for="od_receipt_price">KAKAOPAY 결제금액</label></th>
                    <td>
                        <?php echo $html_receipt_chk; ?>
                        <input type="text" name="od_receipt_price" id="od_receipt_price" value="<?php echo $od['od_receipt_price']; ?>" class="frm_input" size="10"> 원
                    </td>
                </tr>
                <tr>
                    <th scope="row" class="sodr_sppay"><label for="od_receipt_time">KAKAOPAY 승인일시</label></th>
                    <td>
                        <input type="checkbox" name="od_card_chk" id="od_card_chk" value="<?php echo date("Y-m-d H:i:s", G5_SERVER_TIME); ?>" onclick="if (this.checked == true) this.form.od_receipt_time.value=this.form.od_card_chk.value; else this.form.od_receipt_time.value = this.form.od_receipt_time.defaultValue;">
                        <label for="od_card_chk">현재 시간으로 설정</label><br>
                        <input type="text" name="od_receipt_time" value="<?php echo is_null_time($od['od_receipt_time']) ? "" : $od['od_receipt_time']; ?>" id="od_receipt_time" class="frm_input" size="19" maxlength="19">
                    </td>
                </tr>
                <?php } ?>

                <?php if ($od['od_settle_case'] == '간편결제' || ($od['od_pg'] == 'inicis' && is_inicis_order_pay($od['od_settle_case']) )) { ?>
                <tr>
                    <th scope="row" class="sodr_sppay"><label for="od_receipt_price"><?php echo $s_receipt_way; ?> 결제금액</label></th>
                    <td>
                        <?php echo $html_receipt_chk; ?>
                        <input type="text" name="od_receipt_price" id="od_receipt_price" value="<?php echo $od['od_receipt_price']; ?>" class="frm_input" size="10"> 원
                    </td>
                </tr>
                <tr>
                    <th scope="row" class="sodr_sppay"><label for="od_receipt_time"><?php echo $s_receipt_way; ?> 승인일시</label></th>
                    <td>
                        <input type="checkbox" name="od_card_chk" id="od_card_chk" value="<?php echo date("Y-m-d H:i:s", G5_SERVER_TIME); ?>" onclick="if (this.checked == true) this.form.od_receipt_time.value=this.form.od_card_chk.value; else this.form.od_receipt_time.value = this.form.od_receipt_time.defaultValue;">
                        <label for="od_card_chk">현재 시간으로 설정</label><br>
                        <input type="text" name="od_receipt_time" value="<?php echo is_null_time($od['od_receipt_time']) ? "" : $od['od_receipt_time']; ?>" id="od_receipt_time" class="frm_input" size="19" maxlength="19">
                    </td>
                </tr>
                <?php } ?>

                <tr>
                    <th scope="row"><label for="od_receipt_point">포인트 결제액</label></th>
                    <td><input type="text" name="od_receipt_point" value="<?php echo $od['od_receipt_point']; ?>" id="od_receipt_point" class="frm_input" size="10"> 점</td>
                </tr>
                <tr>
                    <th scope="row"><label for="od_refund_price">결제취소/환불 금액</label></th>
                    <td>
                        <input type="text" name="od_refund_price" value="<?php echo $od['od_refund_price']; ?>" id="od_refund_price" class="frm_input" size="10"> 원
                    </td>
                </tr>
             

                </tbody>
                </table>
            </div>
        </section>

    </div>

    <div class="btn_confirm01 btn_confirm">
	<? if(strpos($od_status,"반품") === false){ ?>
        <input type="submit" value="결제/배송내역 수정" class="btn_submit btn">
        <?php if($od_status == '주문' && $od['od_misu'] > 0) { ?>
        <a href="./personalpayform.php?popup=yes&amp;od_id=<?php echo $od_id; ?>" id="personalpay_add" class="btn btn_02">개인결제추가</a>
        <?php } ?>
        <!-- <?php if($od['od_misu'] < 0 && ($od['od_receipt_price'] - $od['od_refund_price']) > 0 && ($od['od_settle_case'] == '신용카드' || $od['od_settle_case'] == '계좌이체' || $od['od_settle_case'] == 'KAKAOPAY')) { ?>
        <a href="./orderpartcancel.php?od_id=<?php echo $od_id; ?>" id="orderpartcancel" class="btn btn_02"><?php echo $od['od_settle_case']; ?> 부분취소</a>
        <?php } ?> -->
	<? } ?>
        <a href="./orderlist.php?<?php echo $qstr; ?>" class="btn btn_02">목록</a>
    </div>
    </form>
</section> 
<?php } ?>
<?php } ?>

 <section id="anc_sodr_memo">
    <h2 class="h2_frm">상점메모</h2>
   
     <div class="local_desc02 local_desc">
         <p>
             고객님 배송메모.<br>
         </p>
     </div>
     <?php
     $sql = "select ct_period_memo from tbl_shop_cart where od_id='$od_id'";
     $cart = sql_fetch($sql);
     ?>
     <textarea name="od_shop_memo" id="od_shop_memo" rows="8" readonly><?php echo stripslashes($cart['ct_period_memo']); ?></textarea>
    <div class="local_desc02 local_desc">
        <p>
            현재 열람 중인 주문에 대한 내용을 메모하는곳입니다.<br>
        </p>
    </div>

    <form name="frmorderform3" action="./orderformupdate.php" method="post">
	<input type="hidden" name="od_status" value="<?php echo $_GET["od_status"]; ?>">
    <input type="hidden" name="od_id" value="<?php echo $od_id; ?>">
    <input type="hidden" name="sort1" value="<?php echo $sort1; ?>">
    <input type="hidden" name="sort2" value="<?php echo $sort2; ?>">
    <input type="hidden" name="sel_field" value="<?php echo $sel_field; ?>">
    <input type="hidden" name="search" value="<?php echo $search; ?>">
    <input type="hidden" name="page" value="<?php echo $page; ?>">
    <input type="hidden" name="mod_type" value="memo">

    <div class="tbl_wrap">
        <label for="od_shop_memo" class="sound_only">상점메모</label>
        <textarea name="od_shop_memo" id="od_shop_memo" rows="8"><?php echo stripslashes($od['od_shop_memo']); ?></textarea>
    </div>

        <?  if(strpos($auth[$sub_menu], 'b') == false) { ?>
    <div class="btn_confirm01 btn_confirm">
        <input type="submit" value="메모 수정" class="btn_submit btn">
    </div>

<? } ?>
    </form>
</section>

<section id="anc_sodr_memo">
    <h2 class="h2_frm">주문상태이력</h2>

    <div class="local_desc02 local_desc">
        <p>
            <?php echo stripslashes($od['od_mod_history']); ?>
        </p>
    </div>
</section>

<section>
    <h2 class="h2_frm">주문자 정보</h2>
    

    <form name="frmorderform3" action="./orderformupdate.php" method="post">
    <input type="hidden" name="od_id" value="<?php echo $od_id; ?>">
    <input type="hidden" name="sort1" value="<?php echo $sort1; ?>">
    <input type="hidden" name="sort2" value="<?php echo $sort2; ?>">
    <input type="hidden" name="sel_field" value="<?php echo $sel_field; ?>">
    <input type="hidden" name="search" value="<?php echo $search; ?>">
    <input type="hidden" name="page" value="<?php echo $page; ?>">
    <input type="hidden" name="mod_type" value="info">

    <div class="compare_wrap">

        <section id="anc_sodr_orderer" class="compare_left">
           <h3>주문하신 분</h3>

            <div class="tbl_frm01">
                <table>
                <caption>주문정보</caption>
                <colgroup>
                    <col class="grid_4">
                    <col>
                </colgroup>
                <tbody>
                <tr>
                    <th scope="row"><label for="od_name"><span class="sound_only">주문하신 분 </span>이름</label></th>
                    <td><input type="text" name="od_name" value="<?php echo get_text($od['od_name']); ?>" id="od_name" required class="frm_input required"></td>
                </tr>
				<!--
                <tr>
                    <th scope="row"><label for="od_tel"><span class="sound_only">주문하신 분 </span>전화번호</label></th>
                    <td><input type="text" name="od_tel" value="<?php echo get_text($od['od_tel']); ?>" id="od_tel"  class="frm_input "></td>
                </tr>
                -->
                <tr>
                    <th scope="row"><label for="od_hp"><span class="sound_only">주문하신 분 </span>핸드폰</label></th>
                    <td><input type="text" name="od_hp" value="<?php echo get_text($od['od_hp']); ?>" id="od_hp" class="frm_input"></td>
                </tr>
                <!--
                <tr>
                    <th scope="row"><span class="sound_only">주문하시는 분 </span>주소</th>
                    <td>
                        <label for="od_zip" class="sound_only">우편번호</label>
                        <input type="text" name="od_zip" value="<?php echo $od['od_zip1'].$od['od_zip2']; ?>" id="od_zip" required class="frm_input required" size="5">
                        <button type="button" class="btn_frmline" onclick="win_zip('frmorderform3', 'od_zip', 'od_addr1', 'od_addr2', 'od_addr3', 'od_addr_jibeon');">주소 검색</button><br>
                        <span id="od_win_zip" style="display:block"></span>
                        <input type="text" name="od_addr1" value="<?php echo get_text($od['od_addr1']); ?>" id="od_addr1" required class="frm_input required" size="35">
                        <label for="od_addr1">기본주소</label><br>
                        <input type="text" name="od_addr2" value="<?php echo get_text($od['od_addr2']); ?>" id="od_addr2" class="frm_input" size="35">
                        <label for="od_addr2">상세주소</label>
                        <br>
                        <input type="text" name="od_addr3" value="<?php echo get_text($od['od_addr3']); ?>" id="od_addr3" class="frm_input" size="35">
                        <label for="od_addr3">참고항목</label>
                        <input type="hidden" name="od_addr_jibeon" value="<?php echo get_text($od['od_addr_jibeon']); ?>"><br>
                    </td>
                </tr>
                -->
                <?php if($od['mb_id'] == "비회원" ){?>
                <tr>
                    <th scope="row"><label for=""><span class="sound_only">주문하신 분 </span>주문 비밀번호</label></th>
                    <td><input type="button" value="비밀번호 재발급" id="od_password" class="btn btn_03" onclick="order_password_resend('<?=$od_id?>');"></td>
                </tr>
             <?php }?>
                <tr>
                    <th scope="row"><label for="od_email"><span class="sound_only">주문하신 분 </span>E-mail</label></th>
                    <td><input type="text" name="od_email" value="<?php echo $od['od_email']; ?>" id="od_email"  class="frm_input email " size="30"></td>
                </tr>
                <tr>
                    <th scope="row"><span class="sound_only">주문하신 분 </span>IP Address</th>
                    <td><?php echo $od['od_ip']; ?></td>
                </tr>
                <?php if ($s_receipt_way == "무통장" && $od['od_deposit_name'] != "약국주문" ) { ?>
                <tr>
                    <th scope="row"><label for="od_name">입금자명</label></th>
                    <td><?php echo get_text($od['od_deposit_name']); ?></td>
                </tr>
                <tr>
                    <th scope="row"><label for="od_name">입금계좌</label></th>
                    <td><?php echo get_text($od['od_bank_account']); ?></td>
                </tr>
                <tr>
                    <th scope="row"><label for="od_name">입금기한</label></th>
                    <td><?php echo get_text($od['od_bank_date']); ?></td>
                </tr>
                    <?php if ($od['od_receipt_price'] > 0 && $od['od_misu'] == 0) { ?>
                    <tr>
                        <th scope="row"><label for="od_name">입금확인일</label></th>
                        <td><?php echo get_text($od['od_receipt_time']); ?></td>
                    </tr>
                    <?php } ?>
                <?php } ?>
                </tbody>
                </table>
            </div>
        </section>

        <section id="anc_sodr_taker" class="compare_right">
             
        </section>

    </div>

	<? if(strpos($od_status,"반품") === false){ ?>
        <?  if(strpos($auth[$sub_menu], 'b') == false) { ?>
	<div class="btn_confirm01 btn_confirm">
        <input type="submit" value="주문자 정보 수정" class="btn_submit btn ">
        <!-- <a href="./orderlist.php?<?php echo $qstr; ?>" class="btn">목록</a> -->
    </div> 
	    <? } ?>
	<? } ?>

    </form>
</section>

<script src="/js/jquery.bpopup.min.js"></script>

<script>
$(function() {
    // 전체 옵션선택
    $("#sit_select_all").click(function() {
        if($(this).is(":checked")) {
            $("input[name='it_sel[]']").attr("checked", true);
            $("input[name^=ct_chk]").attr("checked", true);
        } else {
            $("input[name='it_sel[]']").attr("checked", false);
            $("input[name^=ct_chk]").attr("checked", false);
        }
    });

	 $("#sit_select_all_trans").click(function() {
        if($(this).is(":checked")) {
            $("input[name='it_sel[]']").attr("checked", true);
            $("input[name^=trans_chk]").attr("checked", true);
        } else {
            $("input[name='it_sel[]']").attr("checked", false);
            $("input[name^=trans_chk]").attr("checked", false);
        }
    });


    // 상품의 옵션선택
    $("input[name='it_sel[]']").click(function() {
        var cls = $(this).attr("id").replace("sit_", "sct_");
        var $chk = $("input[name^=ct_chk]."+cls);
        if($(this).is(":checked"))
            $chk.attr("checked", true);
        else
            $chk.attr("checked", false);
    });

    // 개인결제추가
    $("#personalpay_add").on("click", function() {
        var href = this.href;
        window.open(href, "personalpaywin", "left=100, top=100, width=700, height=560, scrollbars=yes");
        return false;
    });

    // 부분취소창
    $("#orderpartcancel").on("click", function() {
        var href = this.href;
        window.open(href, "partcancelwin", "left=100, top=100, width=600, height=350, scrollbars=yes");
        return false;
    });
});

function form_submit(f)
{
    var check = false;
    var status = document.pressed;
    var od_settle_case = '<?php echo $od['od_settle_case']; ?>';

    for (i=0; i<f.chk_cnt.value; i++) {
        if (document.getElementById('ct_chk_'+i).checked == true)
            check = true;
    }

    if (check == false) {
        alert("처리할 자료를 하나 이상 선택해 주십시오.");
        return false;
    }
		var chk2 = document.getElementsByName("ct_chk[]"); 

	//console.log(chk2.length+'개');
	
	for (var i=0; i<chk2.length; i++)
    {
		
        if (chk2[i].checked)
        {
            var k = chk2[i].value;
		
            var current_status = f.elements['current_status2['+k+']'].value;
            var od_gubun = f.elements['od_gubun'].value;
            var od_pay_yn = f.elements['od_pay_yn['+k+']'].value;
			//console.log(chk2[i].value+'체크된 값');			
            switch (status)
            {
                case "확정" :
                    // 일반주문이며 무통장 미입금인 주문은 확정 처리 불가
                    if (od_gubun == 3 && od_settle_case == '무통장' && od_pay_yn == 'N') {
                        alert('미입금 주문건은 입금 확인 후 확정 가능합니다');
                        return false;
                    }

                    if (current_status == "배송") {
                       alert("이미 배송처리된 상품입니다.");
                       return false;
                    }
                    break;
					
            }
        }
    }

    var msg = "";

    <?php if($od['od_settle_case'] == '신용카드' || $od['od_settle_case'] == 'KAKAOPAY' || $od['od_settle_case'] == '간편결제' || ($od['od_pg'] == 'inicis' && is_inicis_order_pay($od['od_settle_case']) )) { ?>
    if(status == "취소" || status == "반품" || status == "품절") {
        var $ct_chk = $("input[name^=ct_chk]");
        var chk_cnt = $ct_chk.size();
        var chked_cnt = $ct_chk.filter(":checked").size();
        
		<?php if( $od['od_billkey'] != "" ){ ?>
			if(chk_cnt != chked_cnt && status == "취소") {
				alert("정기주문 취소시 erp와 연동문제로 인해 체크박스를 모두선택하고 전체취소를 해야합니다.");
				return false;
			}
		<? } ?>
        <?php if($od['od_pg'] == 'KAKAOPAY') { ?>
        var cancel_pg = "카카오페이";
        <?php } else { ?>
        var cancel_pg = "PG사의 <?php echo $od['od_settle_case']; ?>";
        <?php } ?>

        if(chk_cnt == chked_cnt) {
            if(confirm(cancel_pg+" 결제를 함께 취소하시겠습니까?\n\n한번 취소한 결제는 다시 복구할 수 없습니다.")) {
                f.pg_cancel.value = 1;
                msg = cancel_pg+" 결제 취소와 함께 ";
            } else {
                f.pg_cancel.value = 0;
                msg = "";
            }
        }
    }
    <?php } ?>

    if (confirm(msg+"\'" + status + "\' 상태를 선택하셨습니다.\n\n선택하신대로 처리하시겠습니까?")) {
        return true;
    } else {
        return false;
    }
}
function form_submit2(f)
{
	/*
    
	*/


	var check = false;
    var status = document.pressed;

    for (i=0; i<f.chk_cnt.value; i++) {
        if (document.getElementById('trans_chk_'+i).checked == true)
            check = true;
    }

    if (check == false) {
        alert("처리할 자료를 하나 이상 선택해 주십시오.");
        return false;
    }

	var chk = document.getElementsByName("trans_chk[]");
	
	for (var i=0; i<chk.length; i++)
    {
        if (chk[i].checked)
        {
            var k = chk[i].value;
            //var current_settle_case = f.elements['current_settle_case['+k+']'].value;
            var current_status = f.elements['current_status['+k+']'].value;
            switch (status)
            {
                case "배송" :
                    if (current_status != "확정") {
                        alert("'확정' 상태의 주문만 '배송'으로 변경이 가능합니다.");
                        return false;
                    }
                    var invoice      = f.elements['od_invoice['+k+']'];
                   
                    var delivery_company = f.elements['od_delivery_company['+k+']'];

                
                    if ($.trim(delivery_company.value) == '') {
                        alert("배송업체를 입력하시기 바랍니다.");
                        delivery_company.focus();
                        return false;
                    }

                    if ($.trim(invoice.value) == '') {
                        alert("운송장번호를 입력하시기 바랍니다.");
                        invoice.focus();
                        return false;
                    }
                    break;
            }
        }
    }
    if (confirm( status + " 상태를 선택하셨습니다.\n\n선택하신대로 처리하시겠습니까?")) {
        return true;
    } else {
        return false;
    }
}

function del_confirm()
{
    if(confirm("주문서를 삭제하시겠습니까?")) {
        return true;
    } else {
        return false;
    }
}

// 기본 배송회사로 설정
function chk_delivery_company()
{
    var chk = document.getElementById("od_delivery_chk");
    var company = document.getElementById("od_delivery_company");
    company.value = chk.checked ? chk.value : company.defaultValue;
}

// 현재 시간으로 배송일시 설정
function chk_invoice_time()
{
    var chk = document.getElementById("od_invoice_chk");
    var time = document.getElementById("od_invoice_time");
    time.value = chk.checked ? chk.value : time.defaultValue;
}

// 결제금액 수동 설정
function chk_receipt_price()
{
    var chk = document.getElementById("od_receipt_chk");
    var price = document.getElementById("od_receipt_price");
    price.value = chk.checked ? (parseInt(chk.value) + parseInt(price.defaultValue)) : price.defaultValue;
}


//반품처리를 기존로직과 안전하게 분리해서 처리한다.
function ReturnSendProc(GetParam){
	
	var frm = document.frmorderform;
	var Msg = "";

	if( GetParam == "1" && frm.admin_memo.value == "" ){
		alert("반품반려사유를 입력해주세요");
		return;
	}

	if( GetParam == "1" ){
		Msg = "반품반려 처리 하시겠습니까?";
	}else if( GetParam == "2" ){
		Msg = "반품완료 처리 하시겠습니까?";	
	}

	frm.return_gubun.value = GetParam;

	if(confirm(Msg)) {
		frm.action = "./orderformreturnupdate.php";
		frm.submit();
	}

}

$("#od_return_delivery_company_select").change(function(){
	var selected = $(this).find('option:selected');
	var extra = selected.data('company_name'); 

	$("#od_return_delivery_company").val(extra);
	$("#od_return_delivery_url").val(this.value);
});


function ReturnConfirm(){
	
	var check = false;
	var f = document.frmorderform;

	for (i=0; i<f.chk_cnt.value; i++) {
		if (document.getElementById('trans_chk_'+i).checked == true)
			check = true;
	}

	if (check == false) {
		alert("반품승인할 상품을 하나이상 체크해주세요.");
		return;
	}

	if( document.getElementById("od_return_delivery_company_select").value == "" ){
		alert("반품택배사를 선택해주세요");
		return;
	}
	if( document.getElementById("od_return_delivery_url").value == "" ){
		alert("반품택배사url을 입력해주세요");
		return;
	}
	if( document.getElementById("od_return_confirm_check").checked == false ){
		alert("상품회수신청을 체크해주세요");
		return;				
	}

	document.frmorderform2.action = "./orderformreturnupdate2.php";
	document.frmorderform2.method = "post";
	document.frmorderform2.submit();

}

function ChangeDate(GetId,GetSeq){

	$('#popupLayer').bPopup({

		modalClose: false,
		content:'iframe', //'ajax', 'iframe' or 'image'
		iframeAttr:'frameborder=auto',
		iframeAttr:'frameborder=0',
		contentContainer:'.popupContent',
		loadUrl:'./ChangeDate.php?od_id='+GetId+'&od_seq='+GetSeq

	});

}

function order_password_resend (od_id) {
    var conf = "주문 비밀번호를 재발급 하시겠습니까?\r비밀번호는 주문자 핸드폰 번호로 알림톡 전송됩니다.";

    if (confirm(conf)) {
        $.ajax({
            url: 'orderpassword_resend.php',
            type: 'POST',
            data: {
                'od_id': od_id,
                'token': '<?php echo get_admin_token();?>'
            },
            success: function(res) {
                if (res == true) {
                    alert('재발급되었습니다.');
                } else {
                    alert('처리 중 오류가 발생했습니다. 시스템 관리자에게 문의 해주세요.');
                }
            },
            error: function() {
                alert('처리 중 오류가 발생했습니다. 시스템 관리자에게 문의 해주세요.');
            }

        })
    }
}

</script>

<style>
	#popupLayer {display:none;border:5px solid #cccccc;margin-top:50px;padding:5px;background-color:#ffffff;z-index:5;}
	#popupLayer .b-close {position:absolute;top:10px;right:25px;color:#f37a20;font-weight:bold;cursor:hand;}
	#popupLayer .popupContent {margin:0;padding:0;text-align:center;border:0;}
	#popupLayer .popupContent iframe {width:500px;height:400px;border:0;padding:0px;margin:0;z-index:10;}
</style>

<div id="popupLayer">
	<div class="popupContent"></div>
	<div class="b-close">
		<button type="button" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button"><span class="ui-button-text">&nbsp;X&nbsp;</span></button>
	</div>
</div>

<?php
include_once(G5_ADMIN_PATH.'/admin.tail.php');
?>
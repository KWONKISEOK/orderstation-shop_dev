<?php
$sub_menu = '400400';
include_once('./_common.php');


$ct_chk_count = count($_POST['ct_chk']);
if(!$ct_chk_count){
    alert('반품처리할 자료를 하나 이상 선택해 주십시오.');
}

if( empty($return_gubun) ){
	alert('반품구분값이 없습니다.');
	exit;
}

if( $return_gubun == "1" ){
	$ct_status_value = "반품반려";
	$add_sql = " , od_return_date_2 = '".G5_TIME_YMDHIS."' , od_return_confirm = 'N', od_return_delivery_company = '' , od_return_delivery_url = '' ";
}else if( $return_gubun == "2" ){
	$ct_status_value = "반품완료";
	$add_sql = " , od_return_date_3 = '".G5_TIME_YMDHIS."' ";
}

$od = sql_fetch(" select * from {$g5['g5_shop_order_table']} where od_id = '$od_id' ");

if (!$od['od_id']) {
    alert("존재하는 주문이 아닙니다.");
	exit;
}

/*************** 트랜잭션 관련 ****************/
$error_cnt = 0;
mysqli_autocommit($g5['connect_db'], false);
/*************** 트랜잭션 관련 ****************/

//반품반려시...
if( $return_gubun == "1" ){

	$sql = " update {$g5['g5_shop_order_table']} set od_shop_memo = concat(od_shop_memo,\"\\n관리자 반품반려 - ".G5_TIME_YMDHIS." (이유 : {$admin_memo})\")
				where od_id = '$od_id' ";
	sql_query($sql);
	/*************** 트랜잭션 관련 ****************/
	if( mysqli_errno($g5['connect_db']) ){
		$error_cnt += 1;
	}
	/*************** 트랜잭션 관련 ****************/

}

$cnt = count($_POST['od_num']);

for ($i=0; $i<$cnt; $i++){

     $k = $_POST['ct_chk'][$i];
     $od_num = $_POST['od_num'][$k];
	 $od_seq = $_POST['od_seq'][$k];
	 $it_id = $_POST['it_id'][$k];

	 $sql2 = "SELECT a.mb_id, b.mb_referee, b.mb_extra from tbl_shop_order a, tbl_member b where od_id = '$od_id' AND a.mb_id = b.mb_id;";
	 $re2 = sql_fetch($sql2);
	 if( !empty($od_num) && !empty($od_seq) ){

		 //주문상세정보
		 $sql = " update tbl_shop_order_receiver set od_status = '{$ct_status_value}' , od_return_admin_memo = '{$admin_memo}' ".$add_sql." where od_id = '$od_id' 
		 and od_num = '{$od_num}' and od_seq = '{$od_seq}' ";
		 sql_query($sql);
		 /*************** 트랜잭션 관련 ****************/
		 if( mysqli_errno($g5['connect_db']) ){
			$error_cnt += 1;
		 }
		 /*************** 트랜잭션 관련 ****************/

	 }
	 if($return_gubun == "2" ){ // 반품완료
	     if($re2['mb_referee'] == 4 || $re2['mb_extra'] == 11 ){
             // 구매 포인트 적립
             $p_sql = "select * from tbl_shop_order_detail where od_id = '$od_id' and it_id = '$it_id'";
             $p_result = sql_fetch($p_sql);
             if($p_result['od_cash'] > 0){
                 insert_cash($re2['mb_id'],$p_result['od_cash']*(-1),"{$p_result['it_name']} 반품완료");
             }
             // 구매 포인트 적립 끝
         }
     }
}


/*************** 트랜잭션 관련 ****************/
if ($error_cnt > 0) {
	mysqli_rollback($g5['connect_db']);
	mysqli_close($g5['connect_db']);
	echo "
	<script>
		alert('데이터베이스의 에러로 인해 롤백되었습니다.');
		history.back();
	</script>
	";
	exit;
} else {
	mysqli_commit($g5['connect_db']);
} 	
/*************** 트랜잭션 관련 ****************/

$qstr  = "sort1=$sort1&amp;sort2=$sort2&amp;sel_field=$sel_field&amp;search=$search";
$qstr .= "&amp;od_status=$ct_status_value";
$qstr .= "&amp;od_settle_case=$od_settle_case";
$qstr .= "&amp;od_pay_yn=$od_pay_yn";
$qstr .= "&amp;comp_code=$comp_code";
$qstr .= "&amp;od_date=$od_date";
//$qstr .= "&amp;od_misu=$od_misu";
//$qstr .= "&amp;od_cancel_price=$od_cancel_price";
//$qstr .= "&amp;od_receipt_price=$od_receipt_price";
//$qstr .= "&amp;od_receipt_point=$od_receipt_point";
//$qstr .= "&amp;od_receipt_coupon=$od_receipt_coupon";
$qstr .= "&amp;page=$page";

goto_url("./orderlist.php?$qstr");

?>
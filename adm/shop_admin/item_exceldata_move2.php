<?php
$sub_menu = '600670';
include_once('./_common.php');

if (empty($f_idx)) {
    echo "<script>
			alert('파일idx 고유값이 없습니다.');
			parent.location.reload();
		  </script>";
    exit;
}

$backup_data = array();

$sql = "SELECT 
          tbl_shop_item.*,
          tbl_shop_item_excel.idx,
          tbl_shop_item_excel.it_price as change_it_price,
          tbl_shop_item_excel.it_drug_price as change_it_drug_price,
          tbl_shop_item_excel.it_incen as change_it_incen,
          tbl_shop_item_excel.it_dodome_price as change_it_dodome_price,
          tbl_shop_item_excel.it_supply_price as change_it_supply_price
        FROM tbl_shop_item, tbl_shop_item_excel 
        WHERE tbl_shop_item.it_id = tbl_shop_item_excel.it_id";
$result = sql_fetch_result($sql);

/*************** 트랜잭션 관련 ****************/
$error_cnt = 0;
$ErrorMsg = "";
mysqli_autocommit($g5['connect_db'], false);
/*************** 트랜잭션 관련 ****************/

// 단가변동이 있는지 먼저 선체크 후 변동이 있다면 해당 필드 UPDATE -> it_send_yn
foreach ($result as $key => $row) {
//    debug2($row['it_id']);
    $it_id = $row['it_id'];
    $idx = $row['idx'];

    $new = array();
    // 기존 값
    $new['it_price'] = $it_price = $row['it_price'];
    $new['it_drug_price'] = $it_drug_price = $row['it_drug_price'];
    $new['it_incen'] = $it_incen = $row['it_incen'];
    $new['it_dodome_price'] = $it_dodome_price = $row['it_dodome_price'];
    $new['it_supply_price'] = $it_supply_price = $row['it_supply_price'];

    // 변경 값
    $new['change_it_price'] = $change_it_price = $row['change_it_price'];
    $new['change_it_drug_price'] = $change_it_drug_price = $row['change_it_drug_price'];
    $new['change_it_incen'] = $change_it_incen = $row['change_it_incen'];
    $new['change_it_dodome_price'] = $change_it_dodome_price = $row['change_it_dodome_price'];
    $new['change_it_supply_price'] = $change_it_supply_price = $row['change_it_supply_price'];

//    debug2($new);

    $price_change_cnt = 0;
    //소비자가 체크
    if (($change_it_price != $it_price) && $change_it_price > 0) {
        $price_change_cnt++;
    }
    //약국공급가 체크
    if (($change_it_drug_price != $it_drug_price) && $change_it_drug_price > 0) {
        $price_change_cnt++;
    }
    //약국마진 체크
    if (($change_it_incen != $it_incen) && $change_it_incen > 0) {
        $price_change_cnt++;
    }
    //도도매가 체크
    if (($change_it_dodome_price != $it_dodome_price) && $change_it_dodome_price > 0) {
        $price_change_cnt++;
    }
    //매입가 체크
    if (($change_it_supply_price != $it_supply_price) && $change_it_supply_price > 0) {
        $price_change_cnt++;
    }
//    debug2('change_cnt : '.$price_change_cnt);

    //가격변동여부가 있다면 it_send_yn 컬럼 U 로 업데이트 (ERP 연동을 위함)
    if ($price_change_cnt > 0) {
        $it_send_yn = 'U';
    } else {
        $it_send_yn = 'N';
    }

    $sql = "UPDATE tbl_shop_item_excel SET it_send_yn='$it_send_yn' WHERE it_id='$it_id' and idx='$idx'";
    sql_query($sql);
    /*************** 트랜잭션 관련 ****************/
    if (mysqli_errno($g5['connect_db'])) {
        $error_cnt += 1;
    }
    /*************** 트랜잭션 관련 ****************/

    // 백업데이터 배열 저장
    $row['it_info_value'] = unserialize(stripslashes($row['it_info_value'])); // json_encode 때문에 별도 처리
    $backup_data[] = $row;

}


$sql = "UPDATE tbl_shop_item, tbl_shop_item_excel 

        SET 
          tbl_shop_item.ca_id = tbl_shop_item_excel.ca_id
          , tbl_shop_item.ca_id2 = tbl_shop_item_excel.ca_id2
          , tbl_shop_item.ca_id3 = tbl_shop_item_excel.ca_id3
          
          , tbl_shop_item.it_name = tbl_shop_item_excel.it_name
          , tbl_shop_item.it_model = tbl_shop_item_excel.it_model
          , tbl_shop_item.it_maker = tbl_shop_item_excel.it_maker
          , tbl_shop_item.it_origin = tbl_shop_item_excel.it_origin
          , tbl_shop_item.it_brand = tbl_shop_item_excel.it_brand
          , tbl_shop_item.it_notax = tbl_shop_item_excel.it_notax
          
          , tbl_shop_item.it_type1 = tbl_shop_item_excel.it_type1
          , tbl_shop_item.it_type2 = tbl_shop_item_excel.it_type2
          , tbl_shop_item.it_type3 = tbl_shop_item_excel.it_type3
          , tbl_shop_item.it_type4 = tbl_shop_item_excel.it_type4
          
          /* 가격 관련 필드 */
          , tbl_shop_item.it_cust_price = tbl_shop_item_excel.it_cust_price
          , tbl_shop_item.it_price = tbl_shop_item_excel.it_price
          , tbl_shop_item.it_supply_price = tbl_shop_item_excel.it_supply_price
          , tbl_shop_item.it_dodome_price = tbl_shop_item_excel.it_dodome_price
          , tbl_shop_item.it_drug_price = tbl_shop_item_excel.it_drug_price
          , tbl_shop_item.it_incen = tbl_shop_item_excel.it_incen
          , tbl_shop_item.it_drug_ori_price = tbl_shop_item_excel.it_drug_ori_price
          , tbl_shop_item.it_app_norm_price = tbl_shop_item_excel.it_app_norm_price
          , tbl_shop_item.it_app_drug_price = tbl_shop_item_excel.it_app_drug_price
          , tbl_shop_item.it_app_sale_price = tbl_shop_item_excel.it_app_sale_price
          , tbl_shop_item.it_app_incen = tbl_shop_item_excel.it_app_incen
          
          /* 단가 변동 시 ERP 연동 필드 추가 */
          , tbl_shop_item.it_send_yn = tbl_shop_item_excel.it_send_yn
          
          , tbl_shop_item.it_search = tbl_shop_item_excel.it_search
          , tbl_shop_item.it_stock_qty = tbl_shop_item_excel.it_stock_qty
          , tbl_shop_item.it_buy_min_qty = tbl_shop_item_excel.it_buy_min_qty
          , tbl_shop_item.it_buy_max_qty = tbl_shop_item_excel.it_buy_max_qty
          , tbl_shop_item.it_use = tbl_shop_item_excel.it_use
          , tbl_shop_item.it_soldout = tbl_shop_item_excel.it_soldout
          , tbl_shop_item.it_trans_bundle = tbl_shop_item_excel.it_trans_bundle
          , tbl_shop_item.it_order_close_time = tbl_shop_item_excel.it_order_close_time
          , tbl_shop_item.it_release_day = tbl_shop_item_excel.it_release_day
          , tbl_shop_item.it_period_yn = tbl_shop_item_excel.it_period_yn
          , tbl_shop_item.it_sc_type = tbl_shop_item_excel.it_sc_type
          , tbl_shop_item.it_sc_method = tbl_shop_item_excel.it_sc_method
          , tbl_shop_item.it_sc_price = tbl_shop_item_excel.it_sc_price
          , tbl_shop_item.it_sc_minimum = tbl_shop_item_excel.it_sc_minimum
          , tbl_shop_item.it_crm_yn = tbl_shop_item_excel.it_crm_yn
          
          /* 엑셀업로드 테이블 idx 값 */
          , tbl_shop_item.f_idx = tbl_shop_item_excel.f_idx
        WHERE tbl_shop_item.it_id = tbl_shop_item_excel.it_id";
sql_query($sql);
/*************** 트랜잭션 관련 ****************/
if (mysqli_errno($g5['connect_db'])) {
    $error_cnt += 1;
}
/*************** 트랜잭션 관련 ****************/

/*************** 트랜잭션 관련 ****************/
if ($error_cnt > 0) {
    mysqli_rollback($g5['connect_db']);
    mysqli_close($g5['connect_db']);
    echo "
		<script>
			alert('데이터베이스의 에러로 인해 롤백되었습니다.');
			parent.document.getElementById('ErrorView').innerHTML='[".$ErrorMsg."]';
		</script>
		";
    exit;
} else {
    mysqli_commit($g5['connect_db']);
}
/*************** 트랜잭션 관련 ****************/

/************* 관리자 로그 처리 START *************/
insert_admin_log(600,600670, '상품 수정', '', $f_idx, '', $_SERVER['REQUEST_URI'], '', $backup_data);
/************* 관리자 로그 처리 END *************/

//상품이동이 완료되었다면 해당 idx값의데이터를 지워준다.
$sql = " delete from tbl_shop_item_excel where f_idx = {$f_idx} ";
sql_query($sql);
/*************** 트랜잭션 관련 ****************/
if (mysqli_errno($g5['connect_db'])) {
    $error_cnt += 1;
}
/*************** 트랜잭션 관련 ****************/


/*************** 트랜잭션 관련 ****************/
if ($error_cnt > 0) {
    mysqli_rollback($g5['connect_db']);
    mysqli_close($g5['connect_db']);
    echo "
	<script>
		alert('데이터베이스의 에러로 인해 롤백되었습니다.');
		parent.location.reload();
	</script>
	";
    exit;
} else {
    mysqli_commit($g5['connect_db']);
}
/*************** 트랜잭션 관련 ****************/

echo "<script>
		alert('상품수정이 완료되었습니다.');
		parent.location.reload();
	  </script>";
exit;

?>
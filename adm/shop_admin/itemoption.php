<?php
include_once('./_common.php');

$po_run = false;

if($it['it_id']) {
    $opt_subject = explode(',', $it['it_option_subject']);
    $opt1_subject = $opt_subject[0];
    $opt2_subject = $opt_subject[1];
    $opt3_subject = $opt_subject[2];

    $sql = " select * from {$g5['g5_shop_item_option_table']} where io_type = '0' and it_id = '{$it['it_id']}' order by io_no asc ";
    $result = sql_query($sql);
    if(sql_num_rows($result))
        $po_run = true;
} else if(!empty($_POST)) {
    $opt1_subject = preg_replace(G5_OPTION_ID_FILTER, '', trim(stripslashes($_POST['opt1_subject'])));
    $opt2_subject = preg_replace(G5_OPTION_ID_FILTER, '', trim(stripslashes($_POST['opt2_subject'])));
    $opt3_subject = preg_replace(G5_OPTION_ID_FILTER, '', trim(stripslashes($_POST['opt3_subject'])));

    $opt1_val = preg_replace(G5_OPTION_ID_FILTER, '', trim(stripslashes($_POST['opt1'])));
    $opt2_val = preg_replace(G5_OPTION_ID_FILTER, '', trim(stripslashes($_POST['opt2'])));
    $opt3_val = preg_replace(G5_OPTION_ID_FILTER, '', trim(stripslashes($_POST['opt3'])));

    if(!$opt1_subject || !$opt1_val) {
        echo '옵션1과 옵션1 항목을 입력해 주십시오.';
        exit;
    }

    $po_run = true;

    $opt1_count = $opt2_count = $opt3_count = 0;

    if($opt1_val) {
        $opt1 = explode(',', $opt1_val);
        $opt1_count = count($opt1);
    }

    if($opt2_val) {
        $opt2 = explode(',', $opt2_val);
        $opt2_count = count($opt2);
    }

    if($opt3_val) {
        $opt3 = explode(',', $opt3_val);
        $opt3_count = count($opt3);
    }
}

if($po_run) {
?>

<div class="sit_option_frm_wrapper">
    <table>
    <caption>옵션 목록</caption>
    <thead>
    <tr>
        <th scope="col" rowspan="2">
            <label for="opt_chk_all" class="sound_only">전체 옵션</label>
            <input type="checkbox" name="opt_chk_all" value="1" id="opt_chk_all">
        </th>
		<th scope="col" rowspan="2">상품코드</th>
		<th scope="col" rowspan="2">옵션명</th>
        <th width="160">공급가</th>
        <th width="160">일반판매가</th>
        <th width="160">약국판매가</th>
        <th width="160">수수료</th>
		<th width="160">할인율(%)</th>

		<th scope="col" rowspan="2">재고수량</th>
        <th scope="col" rowspan="2">사용여부</th>


    </tr>
	<tr>
        <th width="160">도도매가</th>
        <th width="160">앱판매가</th>
        <th width="160">앱판매가(약국)</th>
        <th width="160">앱수수료</th>
		<th width="160">수량</th>
      </tr>

    </thead>
    <tbody>
    <?php
    if($it['it_id']) {
        for($i=0; $row=sql_fetch_array($result); $i++) {
            $opt_id = $row['io_id'];
            $opt_val = explode(chr(30), $opt_id);
            $opt_1 = $opt_val[0];
            $opt_2 = $opt_val[1];
            $opt_3 = $opt_val[2];
            $opt_2_len = strlen($opt_2);
            $opt_3_len = strlen($opt_3);
            $opt_price = $row['io_price'];
            $opt_stock_qty = $row['io_stock_qty'];
            $opt_noti_qty = $row['io_noti_qty'];
            $opt_use = $row['io_use'];

		
			$price_sql = " select * from {$g5['g5_shop_item_table']} where it_id = '$opt_1' ";
			$it_origin = sql_fetch($price_sql);

			if(!$it_origin){
				
				$it_origin_supply_price = 0;
				$it_origin_dodome_price = 0;
				$it_origin_price = 0;
				$it_origin_app_sale_price = 0;
				$it_origin_drug_price = 0;
				$it_origin_app_drug_price = 0;
				$it_origin_incen = 0;
				$it_origin_app_incen = 0;

			}else{
			
				$it_origin_supply_price = $it_origin["it_supply_price"];
				$it_origin_dodome_price = $it_origin["it_dodome_price"];
				$it_origin_price = $it_origin["it_price"];
				$it_origin_app_sale_price = $it_origin["it_app_sale_price"];
				$it_origin_drug_price = $it_origin["it_drug_price"];
				$it_origin_app_drug_price = $it_origin["it_app_drug_price"];
				$it_origin_incen = $it_origin["it_incen"];
				$it_origin_app_incen = $it_origin["it_app_incen"];

			}

    ?>
    <tr>
        <td class="td_chk">
            <input type="hidden" name="opt_id[]" value="<?php echo $opt_id; ?>">
            <label for="opt_chk_<?php echo $i; ?>" class="sound_only"></label>
            <input type="checkbox" name="opt_chk[]" id="opt_chk_<?php echo $i; ?>" value="1">
        </td>

        <td class="opt-cell"><?php echo $opt_1; if ($opt_2_len) echo ' <small>&gt;</small> '.$opt_2; if ($opt_3_len) echo ' <small>&gt;</small> '.$opt_3; ?></td>
		<td class="opt-cell">
			<div style="float:left;">
					<input type="text" name="opt_name[]" value="<?php echo $row['io_name']; ?>" id="opt_name<?php echo $i; ?>" class="frm_input" size="40"><br>
					<input type="text" name="opt_txt[]" value="<?=$row['io_txt']?>" class="frm_input" size="40"/><br>
					<input type="file" name="opt_img[]" />
					<input type="hidden" name="opt_img_origin[]" value="<?=$row['io_img']?>"/>
			</div>
			<div style="float:left;">
					<?
						$opt_img = G5_DATA_PATH.'/option/'.$row['io_img'];
						$TmpView = " style='display:none' ";
						if(is_file($opt_img) && $row['io_img']) {
							$size = @getimagesize($opt_img);
							$thumb = get_it_thumbnail2($row['io_img'], 80, 80);	
							$TmpView = "  ";
							echo $thumb;
						}
					?>
					<span <?=$TmpView?> >
					파일삭제<input type="checkbox" name="opt_img_del[<?=$i?>]" value="1"><br>
					</span>
			</div>
		</td>
        <td class="td_numsmall">

            <input type="text" name="opt_supply_price[]" value="<?php echo $row['io_supply_price']; ?>" id="opt_supply_price<?php echo $i; ?>" class="frm_input" size="9" data-origin_price="<?=$it_origin_supply_price?>" data-compare_price="<?=$it['it_supply_price']?>">
			<input type="text" name="opt_dodome_price[]" value="<?php echo $row['io_dodome_price']; ?>" id="opt_dodome_price<?php echo $i; ?>" class="frm_input" size="9" data-origin_price="<?=$it_origin_dodome_price?>" data-compare_price="<?=$it['it_dodome_price']?>">
        </td>
		 <td class="td_numsmall">
       
            <input type="text" name="opt_price[]" value="<?php echo $row['io_price']; ?>" id="opt_price<?php echo $i; ?>" class="frm_input" size="9"  data-origin_price="<?=$it_origin_price?>" data-compare_price="<?=$it['it_price']?>">
			<input type="text" name="opt_app_sale_price[]" value="<?php echo $row['io_app_sale_price']; ?>" id="opt_app_sale_price<?php echo $i; ?>" class="frm_input" size="9" data-origin_price="<?=$it_origin_app_sale_price?>" data-compare_price="<?=$it['it_app_sale_price']?>">
        </td>
		 <td class="td_numsmall">

            <input type="text" name="opt_drug_price[]" value="<?php echo $row['io_drug_price']; ?>" id="opt_drug_price<?php echo $i; ?>" class="frm_input" size="9" data-origin_price="<?=$it_origin_drug_price?>" data-compare_price="<?=$it['it_drug_price']?>">
			<input type="text" name="opt_app_drug_price[]" value="<?php echo $row['io_app_drug_price']; ?>" id="opt_app_drug_price<?php echo $i; ?>" class="frm_input" size="9" data-origin_price="<?=$it_origin_app_drug_price?>" data-compare_price="<?=$it['it_app_drug_price']?>">
        </td>
		 <td class="td_numsmall">
            <input type="text" name="opt_incen[]" value="<?php echo $row['io_incen']; ?>" id="opt_incen<?php echo $i; ?>" class="frm_input" size="9" data-origin_price="<?=$it_origin_incen?>" data-compare_price="<?=$it['it_incen']?>">
			<input type="text" name="opt_app_incen[]" value="<?php echo $row['io_app_incen']; ?>" id="opt_app_incen<?php echo $i; ?>" class="frm_input" size="9" data-origin_price="<?=$it_origin_app_incen?>" data-compare_price="<?=$it['it_app_incen']?>">
        </td>
		 <td class="td_numsmall">
            <input type="text" name="opt_period_rate1[]" value="<?php echo $row['io_period_rate1']; ?>" id="opt_period_rate1<?php echo $i; ?>" class="frm_input" size="9">
			<input type="text" name="opt_period_cnt1[]" value="<?php echo $row['io_period_cnt1']; ?>" id="opt_period_cnt1<?php echo $i; ?>" class="frm_input" size="9">
        </td>

        <td class="td_num">
            <label for="opt_stock_qty_<?php echo $i; ?>" class="sound_only"></label>
            <input type="text" name="opt_stock_qty[]" value="<?php echo $opt_stock_qty; ?>" id="op_stock_qty_<?php echo $i; ?>" class="frm_input" size="5">
        </td>

        <td class="td_mng">
            <label for="opt_use_<?php echo $i; ?>" class="sound_only"></label>
            <select name="opt_use[]" id="opt_use_<?php echo $i; ?>">
                <option value="1" <?php echo get_selected('1', $opt_use); ?>>사용함</option>
                <option value="0" <?php echo get_selected('0', $opt_use); ?>>사용안함</option>
            </select>
        </td>
    </tr>
    <?php
        } // for
    } else {
        for($i=0; $i<$opt1_count; $i++) {
            $j = 0;
            do {
                $k = 0;
                do {
                    $opt_1 = strip_tags(trim($opt1[$i]));
                    $opt_2 = strip_tags(trim($opt2[$j]));
                    $opt_3 = strip_tags(trim($opt3[$k]));

                    $opt_2_len = strlen($opt_2);
                    $opt_3_len = strlen($opt_3);

                    $opt_id = $opt_1;
                    if($opt_2_len)
                        $opt_id .= chr(30).$opt_2;
                    if($opt_3_len)
                        $opt_id .= chr(30).$opt_3;
                    $opt_price = 0;
                    $opt_stock_qty = 9999;
                    $opt_noti_qty = 100;
                    $opt_use = 1;

					//
					if( strpos($opt_id, chr(30)) !== false ){
						$split_opt_id = explode(chr(30), $opt_id);
						$split_opt_id = $split_opt_id[0];
					}else{
						$split_opt_id = $opt_id;
					}

                    // 기존에 설정된 값이 있는지 체크
                    if($_POST['w'] == 'u') {
                        $sql = " select *
                                    from {$g5['g5_shop_item_option_table']}
                                    where it_id = '{$_POST['it_id']}'
                                      and io_id = '$split_opt_id'
                                      and io_type = '0' ";
                        $row = sql_fetch($sql);

                        if($row) {
                            $opt_price = (int)$row['io_price'];
                            $opt_stock_qty = (int)$row['io_stock_qty'];
                            $opt_noti_qty = (int)$row['io_noti_qty'];
                            $opt_use = (int)$row['io_use'];
                        }
                    }

			//if($row['io_name'] =='') {
				$sql = " select it_name from {$g5['g5_shop_item_table']} where it_id = '$split_opt_id' ";
				$item = sql_fetch($sql);
				$io_name = $item['it_name'];
			//}

    ?>
    <tr>
        <td class="td_chk">
            <input type="hidden" name="opt_id[]" value="<?php echo $opt_id; ?>">
            <label for="opt_chk_<?php echo $i; ?>" class="sound_only"></label>
            <input type="checkbox" name="opt_chk[]" id="opt_chk_<?php echo $i; ?>" value="1">
        </td>
        <td class="opt1-cell"><?php echo $opt_1; if ($opt_2_len) echo ' <small>&gt;</small> '.$opt_2; if ($opt_3_len) echo ' <small>&gt;</small> '.$opt_3; ?></td>
		<td class="opt-cell"><input type="text" name="opt_name[]" value="<?php echo $io_name; ?>" id="opt_name<?php echo $i; ?>" class="frm_input" size="40"></td>
        <td class="td_numsmall">

            <input type="text" name="opt_supply_price[]" value="0" id="opt_supply_price<?php echo $i; ?>" class="frm_input" size="9">
			<input type="text" name="opt_dodome_price[]" value="0" id="opt_dodome_price<?php echo $i; ?>" class="frm_input" size="9">
        </td>
		 <td class="td_numsmall">
       
            <input type="text" name="opt_price[]" value="0" id="opt_price<?php echo $i; ?>" class="frm_input" size="9">
			<input type="text" name="opt_app_sale_price[]" value="0" id="opt_app_sale_price<?php echo $i; ?>" class="frm_input" size="9">
        </td>
		 <td class="td_numsmall">

            <input type="text" name="opt_drug_price[]" value="0" id="opt_drug_price<?php echo $i; ?>" class="frm_input" size="9">
			<input type="text" name="opt_app_drug_price[]" value="0" id="opt_app_drug_price<?php echo $i; ?>" class="frm_input" size="9">
        </td>
		 <td class="td_numsmall">

            <input type="text" name="opt_incen[]" value="0" id="opt_incen<?php echo $i; ?>" class="frm_input" size="9">
			<input type="text" name="opt_app_incen[]" value="0" id="opt_app_incen<?php echo $i; ?>" class="frm_input" size="9">
        </td>
		 <td class="td_numsmall">
            <input type="text" name="opt_period_rate1[]" value="0" id="opt_period_rate1<?php echo $i; ?>" class="frm_input" size="9">
			<input type="text" name="opt_period_cnt1[]" value="0" id="opt_period_cnt1<?php echo $i; ?>" class="frm_input" size="9">
        </td>

        <td class="td_num">
            <label for="opt_stock_qty_<?php echo $i; ?>" class="sound_only"></label>
            <input type="text" name="opt_stock_qty[]" value="<?php echo $opt_stock_qty; ?>" id="opt_stock_qty_<?php echo $i; ?>" class="frm_input" size="5">
        </td>
        
        <td class="td_mng">
            <label for="opt_use_<?php echo $i; ?>" class="sound_only"></label>
            <select name="opt_use[]" id="opt_use_<?php echo $i; ?>">
                <option value="1" <?php echo get_selected('1', $opt_use); ?>>사용함</option>
                <option value="0" <?php echo get_selected('0', $opt_use); ?>>사용안함</option>
            </select>
        </td>
    </tr>
    <?php
                    $k++;
                } while($k < $opt3_count);

                $j++;
            } while($j < $opt2_count);
        } // for
    }
    ?>
    </tbody>
    </table>
</div>

<div class="btn_list01 btn_list">
    <input type="button" value="선택삭제" id="sel_option_delete" class="btn btn_02">
	<? if($it['it_id']) { ?>
    <span style="float:right;"><input type="button" value="계산하기" id="price_calc" class="btn btn_03"></span>
	<? } ?>
</div>

<script>

$("#price_calc").click(function() {

	var $el = $("input[name='opt_chk[]']");

	if($el.size() > 0) {

		$el.each(function() {

			$tr = $(this).closest("tr");

			var origin_opt_supply_price	 = parseInt( $tr.find("input[name='opt_supply_price[]']").data("origin_price") );
			var compare_opt_supply_price = parseInt( $tr.find("input[name='opt_supply_price[]']").data("compare_price") );

			var origin_opt_dodome_price	 = parseInt( $tr.find("input[name='opt_dodome_price[]']").data("origin_price") );
			var compare_opt_dodome_price = parseInt( $tr.find("input[name='opt_dodome_price[]']").data("compare_price") );

			var origin_opt_price	= parseInt( $tr.find("input[name='opt_price[]']").data("origin_price") );
			var compare_opt_price	= parseInt( $tr.find("input[name='opt_price[]']").data("compare_price") );

			var origin_opt_app_sale_price  = parseInt( $tr.find("input[name='opt_app_sale_price[]']").data("origin_price") );
			var compare_opt_app_sale_price = parseInt( $tr.find("input[name='opt_app_sale_price[]']").data("compare_price") );

			var origin_opt_drug_price	= parseInt( $tr.find("input[name='opt_drug_price[]']").data("origin_price") );
			var compare_opt_drug_price	= parseInt( $tr.find("input[name='opt_drug_price[]']").data("compare_price") );

			var origin_opt_app_drug_price  = parseInt( $tr.find("input[name='opt_app_drug_price[]']").data("origin_price") );
			var compare_opt_app_drug_price = parseInt( $tr.find("input[name='opt_app_drug_price[]']").data("compare_price") );

			var origin_opt_incen	= parseInt( $tr.find("input[name='opt_incen[]']").data("origin_price") );
			var compare_opt_incen	= parseInt( $tr.find("input[name='opt_incen[]']").data("compare_price") );

			var origin_opt_app_incen	= parseInt( $tr.find("input[name='opt_app_incen[]']").data("origin_price") );
			var compare_opt_app_incen	= parseInt( $tr.find("input[name='opt_app_incen[]']").data("compare_price") );

			$tr.find("input[name='opt_supply_price[]']").val(origin_opt_supply_price-compare_opt_supply_price);
			$tr.find("input[name='opt_dodome_price[]']").val(origin_opt_dodome_price-compare_opt_dodome_price);
			$tr.find("input[name='opt_price[]']").val(origin_opt_price-compare_opt_price);
			$tr.find("input[name='opt_app_sale_price[]']").val(origin_opt_app_sale_price-compare_opt_app_sale_price);
			$tr.find("input[name='opt_drug_price[]']").val(origin_opt_drug_price-compare_opt_drug_price);
			$tr.find("input[name='opt_app_drug_price[]']").val(origin_opt_app_drug_price-compare_opt_app_drug_price);
			$tr.find("input[name='opt_incen[]']").val(origin_opt_incen-compare_opt_incen);
			$tr.find("input[name='opt_app_incen[]']").val(origin_opt_app_incen-compare_opt_app_incen);

		});

	}

});

</script>

<!-- <fieldset>
    <legend>옵션 일괄 적용</legend>
    <?php echo help('전체 옵션의 추가금액, 재고/통보수량 및 사용여부를 일괄 적용할 수 있습니다. 단, 체크된 수정항목만 일괄 적용됩니다.'); ?>
    <label for="opt_com_price">추가금액</label>
    <label for="opt_com_price_chk" class="sound_only">추가금액일괄수정</label><input type="checkbox" name="opt_com_price_chk" value="1" id="opt_com_price_chk" class="opt_com_chk">
    <input type="text" name="opt_com_price" value="0" id="opt_com_price" class="frm_input" size="5">
    <label for="opt_com_stock">재고수량</label>
    <label for="opt_com_stock_chk" class="sound_only">재고수량일괄수정</label><input type="checkbox" name="opt_com_stock_chk" value="1" id="opt_com_stock_chk" class="opt_com_chk">
    <input type="text" name="opt_com_stock" value="0" id="opt_com_stock" class="frm_input" size="5">
    <label for="opt_com_noti">통보수량</label>
    <label for="opt_com_noti_chk" class="sound_only">통보수량일괄수정</label><input type="checkbox" name="opt_com_noti_chk" value="1" id="opt_com_noti_chk" class="opt_com_chk">
    <input type="text" name="opt_com_noti" value="0" id="opt_com_noti" class="frm_input" size="5">
    <label for="opt_com_use">사용여부</label>
    <label for="opt_com_use_chk" class="sound_only">사용여부일괄수정</label><input type="checkbox" name="opt_com_use_chk" value="1" id="opt_com_use_chk" class="opt_com_chk">
    <select name="opt_com_use" id="opt_com_use">
        <option value="1">사용함</option>
        <option value="0">사용안함</option>
    </select>
    <button type="button" id="opt_value_apply" class="btn_frmline">일괄적용</button>
</fieldset> -->
<?php
}
?>
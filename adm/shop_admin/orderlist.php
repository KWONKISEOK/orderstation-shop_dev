<?php
$sub_menu = '400400';
include_once('./_common.php');

$TopTitle = "전체주문현황";

if( $od_status == "" ){
	$sub_menu = '400400';
}else if( $od_status == "주문" ){
	$sub_menu = '400401';
}else if( $od_status == "확정" ){
	$sub_menu = '400402';
}else if( $od_status == "배송" ){
	$sub_menu = '400403';
}else if( $od_status == "완료" ){
	$sub_menu = '400404';
}else if( $od_status == "취소" ){
	$sub_menu = '400406';
}else if( $od_status == "반품접수" ){
	$sub_menu = '400411';
	$TopTitle = "반품접수현황";
}else if( $od_status == "반품반려" ){
	$sub_menu = '400409';
	$TopTitle = "반품반려현황";
}else if( $od_status == "반품완료" ){
	$sub_menu = '400410';
	$TopTitle = "반품완료현황";
}else if( $od_status == "반품전체" ){
	$sub_menu = '400411';
	$TopTitle = "전체반품현황";
}else if( $od_status == "반품승인" ){
	$sub_menu = '400412';
	$TopTitle = "반품승인현황";
}

auth_check($auth[$sub_menu], "r");

$g5['title'] = $TopTitle;
include_once (G5_ADMIN_PATH.'/admin.head.php');
include_once(G5_PLUGIN_PATH.'/jquery-ui/datepicker.php');

$where = array();

$doc = strip_tags($doc);
$sort1 = in_array($sort1, array('od_id', 'od_cart_price', 'od_receipt_price', 'od_cancel_price', 'od_misu', 'od_cash')) ? $sort1 : '';
$sort2 = in_array($sort2, array('desc', 'asc')) ? $sort2 : 'desc';
$sel_field = get_search_string($sel_field);
if( !in_array($sel_field, array('od_id', 'mb_id', 'od_name', 'od_tel', 'od_hp', 'od_b_name', 'comp_name', 'it_id', 'it_name', 'pharm_name', 'pharm_custno')) ){   //검색할 필드 대상이 아니면 값을 제거
    $sel_field = '';
}
$od_status = get_search_string($od_status);
$search = get_search_string($search);
if(! preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $fr_date) ) $fr_date = '';
if(! preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $to_date) ) $to_date = '';

if( strpos($od_status,"반품") !== false ){
	if($fr_date =='') $fr_date = date("Y-m-d", strtotime('-730 day'));
}else{
	if($fr_date =='') $fr_date = date("Y-m-d", strtotime('-2 week'));
}

if($to_date =='') $to_date = date("Y-m-d");
$sql_search = "";
if ($search != "") {
    if ($sel_field != "") {
		if($sel_field == 'comp_name') {
			$where[] = "d.$sel_field like '%$search%' ";
		} else if($sel_field == 'it_name' || $sel_field == 'it_id') {
            $where[] = "b.$sel_field like '%$search%' ";
        } else if($sel_field == 'od_b_name') {
			$where[] = " ( instr( c.od_b_name , '$search' ) > 0 or instr( e.od_b_name , '$search' ) > 0 ) ";
		} else {
			$where[] = "a.$sel_field like '%$search%' ";
		}

    }

    if ($save_search != $search) {
        $page = 1;
    }
}

if( $od_status == "반품접수" ){
	$od_date = "5";
}else if( $od_status == "반품반려" ){
	$od_date = "6";
}else if( $od_status == "반품완료" ){
	$od_date = "7";
}else if( $od_status == "반품전체" ){
	$od_date = "8";
}else if( $od_status == "반품승인" ){
	$od_date = "9";
	$od_status = "반품접수"; //공급사에서 승인을 하면 관리자에서는 반품승인으로 보이는것일뿐 상태는 반품접수상태임.
}

//날자구분
switch($od_date) {
	case '1':
		$where[] = " c.od_date1 between '$fr_date 00:00:00' and '$to_date 23:59:59' ";
		break;
	case '2':
		$where[] = " c.od_date2 between '$fr_date 00:00:00' and '$to_date 23:59:59' ";
		break;
	case '3':
		$where[] = " c.od_date3 between '$fr_date 00:00:00' and '$to_date 23:59:59' ";
		break;
	case '4':
		$where[] = " c.od_date4 between '$fr_date 00:00:00' and '$to_date 23:59:59' ";
		break;
	case '5':
		$where[] = " c.od_return_date_1 between '$fr_date 00:00:00' and '$to_date 23:59:59' and od_return_confirm = 'N' ";
		break;
	case '6':
		$where[] = " c.od_return_date_2 between '$fr_date 00:00:00' and '$to_date 23:59:59' ";
		break;
	case '7':
		$where[] = " c.od_return_date_3 between '$fr_date 00:00:00' and '$to_date 23:59:59' ";
		break;
	case '8':
		$where[] = " ( ( c.od_return_date_1 between '$fr_date 00:00:00' and '$to_date 23:59:59' )
					 or
					 ( c.od_return_date_2 between '$fr_date 00:00:00' and '$to_date 23:59:59' )
					 or
					 ( c.od_return_date_3 between '$fr_date 00:00:00' and '$to_date 23:59:59' ) )
					";
		break;
	case '9':
		$where[] = " c.od_return_date_1 between '$fr_date 00:00:00' and '$to_date 23:59:59' and od_return_confirm = 'Y' ";
		break;
    case '10':
        $where[] = " c.od_cancel_date between '$fr_date 00:00:00' and '$to_date 23:59:59' ";
        break;
	default:
		$where[] = " c.od_date1 between '$fr_date 00:00:00' and '$to_date 23:59:59' ";
		break;
}
//상태
if ($od_status) {
	if( $od_status == "반품전체" ){
		$where[] = " instr( c.od_status , '반품' ) > 0 ";
	}else if($od_status  == "취소외" ){
		$where[] = " c.od_status != '취소' and c.od_status != '반품접수' and c.od_status != '반품승인' and c.od_status != '반품반려' and c.od_status != '반품완료' ";
		
	}else{
		$where[] = " c.od_status = '$od_status' ";
	}
}
//결제구분
if ($od_settle_case) {
	 switch($od_settle_case) {
        case '빌링결제':
            $where[] = " a.od_billkey != '' ";
            break;
        default:
            $where[] = " a.od_settle_case = '$od_settle_case' ";
            break;
    }
}
//입금여부
if ($od_pay_yn) {
    $where[] = " c.od_pay_yn = '$od_pay_yn' ";
}
//공급사코드
if ($comp_code) {
    $where[] = " b.comp_code = '$comp_code' ";
}//주문구분
if ($od_gubun) {
    $where[] = " a.od_gubun = '$od_gubun' ";
}
//서비스구분
if($service_type){
	switch($service_type) {
        case 'OS':
            $where[] = " AND c.service_type is null ";

            break;
        default:
            $where[] = " c.service_type = 'B' ";
            break;
    }
}
if ($is_admin == 'sales') {
	$where[] = " mb_id in (select mb_id from tbl_member where pharm_manager = '{$member['mb_id']}') ";
}


if ($where) {
	$sql_search = ' where  '.implode(' and ', $where);
}

//toptap=2 -> 달콩행복set제외목록
if( $toptap == "2" ){
		$sql_search .= " and b.it_id not in(
			'S00603',
			'S00487',
			'S00489',
			'S00491',
			'S00488',
			'S00490',
			'S00492',
			'S00493',
			'S00604',
			'S00487',
			'S00489',
			'S00491',
			'S00488',
			'S00490',
			'S00492',
			'S00493'
		) ";
}
//toptap=3 -> 달콩행복set제외목록
if( $toptap == "3" ){
		$sql_search .= " and b.it_id in(
			'S00603',
			'S00487',
			'S00489',
			'S00491',
			'S00488',
			'S00490',
			'S00492',
			'S00493',
			'S00604',
			'S00487',
			'S00489',
			'S00491',
			'S00488',
			'S00490',
			'S00492',
			'S00493'	
		) ";
}

if ($sel_field == "")  $sel_field = "od_id";
if ($sort1 == "") $sort1 = "od_id";
if ($sort2 == "") $sort2 = "desc";

//$sql_common = " from tbl_shop_order a,  tbl_shop_order_detail b, tbl_shop_order_receiver c, tbl_member d $sql_search ";

$sql_common = " 
	from tbl_shop_order a inner join tbl_shop_order_detail b on a.od_id = b.od_id 
						  inner join tbl_shop_order_receiver c on b.od_id = c.od_id and b.od_num = c.od_num
						  left outer join(
								SELECT od_id , od_num , GROUP_CONCAT( od_b_name SEPARATOR  '|' ) AS od_b_name FROM (

									SELECT od_id, od_num , od_seq , CONCAT( CONCAT( od_b_name , ': ' ) , od_qty , '개' ) AS od_b_name
									FROM tbl_shop_order_multi_receiver
									GROUP BY od_id , od_seq
									
								) AS X GROUP BY od_id , od_num						  
						  ) as e on b.od_id = e.od_id and b.od_num = e.od_num
						  left join tbl_member d on b.comp_code = d.comp_code
	$sql_search
";
	if($service_gubun !=""){
		switch($service_gubun) {
			case 'O':
				$sql_common .= " and c.service_type is null ";
				break;
			default:
				$sql_common .= " and c.service_type = 'B' ";
				break;
		}
	}

	
//$sql = " select count(od_id) as cnt " . $sql_common;
$sql = " select count(distinct a.od_id, b.comp_code) as cnt " . $sql_common;

//echo $sql;
$row = sql_fetch($sql);
//페이징처리
$total_count = $row['cnt'];

$sql = " select count(distinct a.od_id) as cnt " . $sql_common;
if($service_gubun !=""){
		switch($service_gubun) {
			case 'O':
				$sql .= " and c.service_type is null ";
				break;
			default:
				$sql .= " and c.service_type = 'B' ";
				break;
		}
	}
	
$row = sql_fetch($sql);
//총주문건수
$total_count1 = $row['cnt'];

$rows = 10;//$config['cf_page_rows'];
$total_page  = ceil($total_count / $rows);  // 전체 페이지 계산
//echo $total_page;
if ($page < 1) { $page = 1; } // 페이지가 없으면 첫 페이지 (1 페이지)
$from_record = ($page - 1) * $rows; // 시작 열을 구함

switch ($order_sort) {
    case 'order':
        $order_by = 'a.od_time';
        break;
    case 'cancel':
        $order_by = 'c.od_cancel_date';
        break;
    case 'return_complete':
        $order_by = 'c.od_return_date_3';
        break;
    default:
        $order_by = 'a.od_time';
        break;
}

$iRow = 0;
$sql  = " select a.od_id, b.comp_code, count(*) od_count , 
			/* 아래 해당코드를 구매한 고객에게 달콩제품을 주기 위한 처리 */
			case when 
				INSTR(
				'320004,S00139,320005,320006,320001,S00136,S00307,S00307,S00306,S00306,320007,320008,
				S00138,320003,320002,S00137,S00601,S00602,S00600,S00560,S00559,S00556,S00554,S00558,
				S00557,092001,W11738,W11739,W11740,W11736,W11737,S00052,347001,347002,347003,347004,
				347010,347009,347007,347008,347006,347005,W11744,W11742,W11746,W11741,W11743,W11745,
				S00493,S00491,S00492,S00489,S00490,S00487,S00488,S00164,S00165,S00162,S00163,S00432,
				S00435,S00433,S00434,293004,S00305',b.it_id ) > 0 AND a.od_time BETWEEN '2019-04-29 00:00:00' AND '2019-05-19 23:59:59'
			then 'Y' ELSE 'N' END color_draw , b.it_id
           $sql_common
		   group by a.od_id, b.comp_code
           order by $order_by desc, b.comp_code
           limit $from_record, $rows ";

$result = sql_query($sql);
//echo $sql;
//상품별 만들기
$iRow = 0;
$total_qty = 0;$total_drug = 0;$total_incen = 0;
for ($i=0; $row=sql_fetch_array($result); $i++)
{

	$sql  = " select a.mb_id
					,b.comp_code
					 ,a.od_name
					 ,a.pharm_name
					 ,a.pharm_manager
					 ,(select mb_name from tbl_member where mb_id= a.pharm_manager ) pharm_manager_name
					 ,(select mb_name from tbl_member where pharm_custno= a.pharm_custno and pharm_custno != '' limit 1) pharm_mb_name
					 ,a.od_settle_case
					 ,a.pharm_custno
					 ,a.od_gubun
					 ,a.od_mobile
                     ,a.od_company_yn
					 ,(select mb_pay_yn from tbl_member where mb_id = a.mb_id) mb_pay_yn
					 ,d.comp_name

					 ,b.od_num
					 ,b.it_name
					 ,b.io_id
					 ,b.od_option
					 ,b.od_period_yn
					 ,b.od_period_cnt
					 ,b.od_sum_drug_price
					 ,b.od_total_drug_price
					 ,b.od_total_sale_price
					 ,b.od_sum_sale_price
					 ,case when a.od_multi_yn = 'Y' then e.od_b_name else c.od_b_name end as od_b_name
					 ,c.od_seq
					 ,c.od_date1
					 ,c.od_qty
					 ,c.od_period_date
					 ,c.od_status
					 ,c.od_pay_yn
					 ,b.it_id
					 ,c.od_release_yn
					 ,c.od_return_date_1
					 ,c.od_return_date_2
					 ,c.od_return_date_3
					 ,c.od_return_confirm, c.od_return_delivery_company , c.od_return_delivery_url 
					 ,c.od_cancel_date
					 ,a.service_type ,a.od_multi_yn, c.service_type
				 from tbl_shop_order a inner join tbl_shop_order_detail b on a.od_id = b.od_id 
						  inner join tbl_shop_order_receiver c on b.od_id = c.od_id and b.od_num = c.od_num
						  left outer join(
								SELECT od_id , od_num , GROUP_CONCAT( od_b_name ORDER BY od_b_name asc SEPARATOR  '|' ) AS od_b_name FROM (

									SELECT od_id, od_num , od_seq , CONCAT( CONCAT( od_b_name , ': ' ) , od_qty , '개' ) AS od_b_name
									FROM tbl_shop_order_multi_receiver
									GROUP BY od_id , od_seq
									
								) AS X GROUP BY od_id , od_num						  
						  ) as e on b.od_id = e.od_id and b.od_num = e.od_num
						  left join tbl_member d on b.comp_code = d.comp_code 
				 where b.od_id = ".$row[od_id]. " and b.comp_code = '".$row[comp_code]."' ";
		
	switch($od_date) {
		case '1':
			$sql .= " and  c.od_date1 between '$fr_date 00:00:00' and '$to_date 23:59:59' ";
			break;
		case '2':
			$sql .= " and  c.od_date2 between '$fr_date 00:00:00' and '$to_date 23:59:59' ";
			break;
		case '3':
			$sql .= " and  c.od_date3 between '$fr_date 00:00:00' and '$to_date 23:59:59' ";
			break;
		case '4':
			$sql .= " and c.od_date4 between '$fr_date 00:00:00' and '$to_date 23:59:59' ";
			break;
		case '5':
			$sql .= " and c.od_return_date_1 between '$fr_date 00:00:00' and '$to_date 23:59:59' and od_return_confirm = 'N' ";
			break;
		case '6':
			$sql .= " and c.od_return_date_2 between '$fr_date 00:00:00' and '$to_date 23:59:59' ";
			break;
		case '7':
			$sql .= " and c.od_return_date_3 between '$fr_date 00:00:00' and '$to_date 23:59:59' ";
			break;
		case '8':
			$sql .= " and ( ( c.od_return_date_1 between '$fr_date 00:00:00' and '$to_date 23:59:59' )
						or
					  ( c.od_return_date_2 between '$fr_date 00:00:00' and '$to_date 23:59:59' )
						or
					  ( c.od_return_date_3 between '$fr_date 00:00:00' and '$to_date 23:59:59' ) )
					";
			break;
		case '9':
			$sql .= " and c.od_return_date_1 between '$fr_date 00:00:00' and '$to_date 23:59:59' and od_return_confirm = 'Y' ";
			break;
        case '10':
            $sql .= " and c.od_cancel_date between '$fr_date 00:00:00' and '$to_date 23:59:59' ";
            break;
		default:
			$sql .= " and  c.od_date1 between '$fr_date 00:00:00' and '$to_date 23:59:59' ";
			break;
	}
	//결제구분
	if ($od_settle_case) {
		 switch($od_settle_case) {
			case '빌링결제':
				$sql .= " and a.od_billkey != '' ";
				break;
			default:
				$sql .= " and a.od_settle_case = '$od_settle_case' ";
				break;
		}
	}
	//상태
	if ($od_status) {
		if( $od_status == "반품전체" ){
			$sql .= " and instr( c.od_status , '반품' ) > 0 ";
		}else if( $od_status == "취소외"){
			$sql .= " and c.od_status != '취소' and c.od_status != '반품접수' and c.od_status != '반품승인' and c.od_status != '반품반려' and c.od_status != '반품완료'";
		}
		else{
			$sql .= " and c.od_status = '$od_status' ";
		}
	}
	
	//입금여부
	if ($od_pay_yn) {
		$sql .= " and c.od_pay_yn = '$od_pay_yn' ";
	}
	//공급사코드
	if ($comp_code) {
		$sql .= " and b.comp_code = '$comp_code' ";
	}
   //공급사명
	if ($comp_name) {
		$sql .= " and d.comp_name = '$comp_code' ";
	}
	if ($search != "") {
		if ($sel_field != "") {
			if($sel_field == 'comp_name') {
				$sql .= " and d.$sel_field like '%$search%' ";
			} else if($sel_field == 'it_name' || $sel_field == 'it_id') {
				$sql .= " and b.$sel_field like '%$search%' ";
			} else if($sel_field == 'od_b_name') {
                $sql .= " and ( instr( c.od_b_name , '$search' ) > 0 or instr( e.od_b_name , '$search' ) > 0 ) ";
            } else {
				$sql .= " and a.$sel_field like '%$search%' ";
			}

		}
	}
	//서비스명
	if($service_gubun == 'B'){
		$sql .= "and c.service_type = 'B'";
	}
	if($service_gubun == 'O'){
		$sql .= "and c.service_type is NULL";
	}
	//공급사명
	//if($sel_field == 'comp_name') {
	//	$where[] = "d.$sel_field like '%$search%' ";
	//}
	//상품명
	//if($sel_field == 'it_name') {
	//	$where[] = "b.$sel_field like '%$search%' ";
	//}
	//echo $sql;
    //debug2($sql);
	$result2 = sql_query($sql);
	
	for ($j=0; $row2=sql_fetch_array($result2); $j++)
	{

		$order[$iRow] = $row;
		$order[$iRow][mb_id] = $row2[mb_id];
		$order[$iRow][od_name] = $row2[od_name];
		$order[$iRow][pharm_name] = $row2[pharm_name];
		$order[$iRow][pharm_manager] = $row2[pharm_manager];
		$order[$iRow][pharm_manager_name] = $row2[pharm_manager_name];
		$order[$iRow][pharm_mb_name] = $row2[pharm_mb_name];
		$order[$iRow][od_b_name] = $row2[od_b_name];
		$order[$iRow][mb_pay_yn] = $row2[mb_pay_yn];
		$order[$iRow][od_company_yn] = $row2[od_company_yn];

		$order[$iRow][od_settle_case] = $row2[od_settle_case];
		$order[$iRow][pharm_custno] = $row2[pharm_custno];
		$order[$iRow][od_gubun] = $row2[od_gubun];
		$order[$iRow][comp_name] = $row2[comp_name];
		$order[$iRow][od_mobile] = $row2[od_mobile];

		$order[$iRow][od_num] = $row2[od_num];
		$order[$iRow][it_name] = $row2[it_name];
		$order[$iRow][io_id] = $row2[io_id];
		$order[$iRow][od_option] = $row2[od_option];
		$order[$iRow][od_period_yn] = $row2[od_period_yn];
			if($row2[service_type] == 'B' && $row2[pharm_name] == ""){
					$order[$iRow][od_sum_drug_price] = $row2[od_sum_sale_price];
			}else{
					$order[$iRow][od_sum_drug_price] = $row2[od_sum_drug_price]; //단가		
			}
		if($row2[od_period_yn] == 'Y') { 
				if($row2[service_type] == 'B' && $row2[pharm_name] ==''){
					$order[$iRow][od_total_drug_price] = $row2[od_total_sale_price] / $row2[od_period_cnt];
					$order[$iRow][od_total_incen] = $row2[od_total_incen];
				}else{		
						$order[$iRow][od_total_drug_price] = $row2[od_total_drug_price] / $row2[od_period_cnt]; //OS합계
				$order[$iRow][od_total_incen] = $row2[od_total_incen] / $row2[od_period_cnt];
				}
		} else {
				if($row2[service_type] == 'B' && $row2[pharm_name] ==''){
					$order[$iRow][od_total_drug_price] = $row2[od_total_sale_price]; 
					$order[$iRow][od_total_incen] = $row2[od_total_incen];
				}else{
					$order[$iRow][od_total_drug_price] = $row2[od_total_drug_price]; //정기주문아닌 주문건에대한 합계
					$order[$iRow][od_total_incen] = $row2[od_total_incen];
				}
		}
		$order[$iRow][od_status] = $row2[od_status];
		
		$order[$iRow][od_qty] = $row2[od_qty]; //수량
		$order[$iRow][od_date1] = $row2[od_date1];
		$order[$iRow][od_seq] = $row2[od_seq];
		$order[$iRow][od_period_date] = $row2[od_period_date];
		$order[$iRow][od_status] = $row2[od_status];
		$order[$iRow][od_pay_yn] = $row2[od_pay_yn];
		$order[$iRow][it_id] = $row2[it_id];
		$order[$iRow][od_release_yn] = $row2[od_release_yn];
		$order[$iRow][od_return_confirm] = $row2[od_return_confirm];
		$order[$iRow][service_type] = $row2[service_type];
		$order[$iRow][od_multi_yn] = $row2[od_multi_yn];
        $order[$iRow][od_cancel_date] = $row2[od_cancel_date];
        $order[$iRow][od_return_date_1] = $row2[od_return_date_1];
        $order[$iRow][od_return_date_2] = $row2[od_return_date_2];
        $order[$iRow][od_return_date_3] = $row2[od_return_date_3];

		if($order[$iRow][od_status] != '취소') {
			$total_qty = $total_qty + $order[$iRow][od_qty];
			$total_drug = $total_drug + $order[$iRow][od_total_drug_price];
			if($order[$iRow][od_pay_yn] == 'Y') {
				$total_incen = $total_incen + $order[$iRow][od_total_incen];
			}
		}

		$iRow++;
	}	
}
//debug2($order);

$qstr1 = "od_status=".urlencode($_GET["od_status"])."&amp;od_settle_case=".urlencode($od_settle_case)."&amp;od_date=$od_date&amp;comp_code=$comp_code&amp;od_pay_yn=$od_pay_yn&amp;od_gubun=$od_gubun&amp;order_sort=$order_sort&amp;od_coupon=$od_coupon&amp;fr_date=$fr_date&amp;to_date=$to_date&amp;sel_field=$sel_field&amp;search=$search&amp;service_gubun=$service_gubun&amp;save_search=$search&amp;od_b_name=".urlencode($_GET["od_b_name"]);

if($default['de_escrow_use'])
    $qstr1 .= "&amp;od_escrow=$od_escrow";
$qstr = "$qstr1&amp;sort1=$sort1&amp;sort2=$sort2&amp;page=$page&amp;toptap=$toptap";

$listall = '
	<a href="'.$_SERVER['SCRIPT_NAME'].'?'.$qstr1.'&amp;sort1=$sort1&amp;sort2=$sort2&amp;page=$page&amp;toptap=1" class="ov_listall">전체목록</a>
	<!--
	<a href="'.$_SERVER['SCRIPT_NAME'].'?'.$qstr1.'&amp;sort1=$sort1&amp;sort2=$sort2&amp;page=$page&amp;toptap=2" class="ov_listall">달콩행복set제외목록</a>
	<a href="'.$_SERVER['SCRIPT_NAME'].'?'.$qstr1.'&amp;sort1=$sort1&amp;sort2=$sort2&amp;page=$page&amp;toptap=3" class="ov_listall">달콩행복set목록</a>
	-->
	';


?>

<div class="local_ov01 local_ov">
    <?php echo $listall; ?>
    <span class="btn_ov01"><span class="ov_txt">전체 주문내역</span><span class="ov_num"> <?php echo number_format($total_count1); ?>건</span></span>
    
    <a href="javascript:;" onclick="test_del()" class="btn btn_02">테스트주문삭제</a>
</div>

<script>
    function test_del() {
        if (confirm("테스트 주문을 삭제하시겠습니까?")) {
            location.href="./orderlist_test_del.php";
        }
    }
</script>





<form name="frmorderlist" class="local_sch01 local_sch">
<input type="hidden" name="doc" value="<?php echo $doc; ?>">
<input type="hidden" name="sort1" value="<?php echo $sort1; ?>">
<input type="hidden" name="sort2" value="<?php echo $sort2; ?>">
<input type="hidden" name="page" value="<?php echo $page; ?>">
<input type="hidden" name="save_search" value="<?php echo $search; ?>">
<input type="hidden" name="toptap" value="<?php echo $toptap; ?>">

<div class="local_sch03  tbl_wrap" style="padding:10px;">
    <table>
   
    <tbody>
    <tr>
        <th scope="row">
			<select name="od_date" id="od_date" >
				<option value="1" <?php echo get_selected($od_date, '1'); ?>>주문일</option>
				<option value="2" <?php echo get_selected($od_date, '2'); ?>>확정일</option>
				<option value="3" <?php echo get_selected($od_date, '3'); ?>>배송일</option>
				<option value="4" <?php echo get_selected($od_date, '4'); ?>>완료일</option>
				<option value="5" <?php echo get_selected($od_date, '5'); ?>>반품접수일</option>
				<option value="6" <?php echo get_selected($od_date, '6'); ?>>반품반려일</option>
				<option value="7" <?php echo get_selected($od_date, '7'); ?>>반품완료일</option>
                <option value="10" <?php echo get_selected($od_date, '10'); ?>>취소일</option>
			</select>
		</th>
        <td colspan=7>
            <input type="text" id="fr_date"  name="fr_date" value="<?php echo $fr_date; ?>" class="frm_input" size="10" maxlength="10"> ~
			<input type="text" id="to_date"  name="to_date" value="<?php echo $to_date; ?>" class="frm_input" size="10" maxlength="10">
			<button type="button" onclick="javascript:set_date('오늘');">오늘</button>
			<button type="button" onclick="javascript:set_date('어제');">어제</button>
			<button type="button" onclick="javascript:set_date('이번주');">이번주</button>
			<button type="button" onclick="javascript:set_date('이번달');">이번달</button>
			<button type="button" onclick="javascript:set_date('지난주');">지난주</button>
			<button type="button" onclick="javascript:set_date('지난달');">지난달</button>
			<button type="button" onclick="javascript:set_date('12개월');">12개월</button>
			<button type="button" onclick="javascript:set_date('전체');">전체</button>
        </td>
		<td rowspan="3">
			 <input type="submit" value="검색" class="get_theme_confc btn btn_01">
			 <input type="button" value="엑셀 다운로드" alt="엑셀 다운로드" class="get_theme_confc btn btn_02" style="cursor:pointer" id="ExcelDownButton">

		</td>
		
    </tr>
	<tr>
        <th scope="row">처리상태</th>
        <td>
			<select name="od_status" id="od_status"  >
			    <option value="">==전체==</option>
				<option value="주문" <?php echo get_selected($_GET["od_status"], '주문'); ?>>주문</option>
				<option value="확정" <?php echo get_selected($_GET["od_status"], '확정'); ?>>확정</option>
				<option value="배송" <?php echo get_selected($_GET["od_status"], '배송'); ?>>배송</option>
				<option value="완료" <?php echo get_selected($_GET["od_status"], '완료'); ?>>완료</option>
				<option value="취소" <?php echo get_selected($_GET["od_status"], '취소'); ?>>취소</option>   
				<option value="취소외" <?php echo get_selected($_GET["od_status"], '취소외'); ?>>취소외</option>
                <option value="반품전체" <?php echo get_selected($_GET["od_status"], '반품전체'); ?>>반품전체</option>
                <option value="반품접수" <?php echo get_selected($_GET["od_status"], '반품접수'); ?>>반품접수</option>
				<option value="반품승인" <?php echo get_selected($_GET["od_status"], '반품승인'); ?>>반품승인</option>
				<option value="반품반려" <?php echo get_selected($_GET["od_status"], '반품반려'); ?>>반품반려</option>   
				<option value="반품완료" <?php echo get_selected($_GET["od_status"], '반품완료'); ?>>반품완료</option>   
			</select>
			
        </td>
		<th scope="row">입금여부</th>
        <td>
            <select name="od_pay_yn" id="od_pay_yn" >
				<option value="">==전체==</option>
				<option value="N" <?php echo get_selected($od_pay_yn, 'N'); ?>>미입금</option>
				<option value="Y" <?php echo get_selected($od_pay_yn, 'Y'); ?>>입금</option>
			</select>
        </td>
		<th scope="row">주문구분</th>
        <td>
           <select name="od_gubun" id="od_gubun" >
				<option value="">==전체==</option>
				<option value="1" <?php echo get_selected($od_gubun, '1'); ?>>고객주문</option>
				<option value="2" <?php echo get_selected($od_gubun, '2'); ?>>약국주문</option>
				<option value="3" <?php echo get_selected($od_gubun, '3'); ?>>일반주문</option>
				<option value="9" <?php echo get_selected($od_gubun, '9'); ?>>사내주문</option>
			</select>
        </td>
		<th scope="row">서비스구분</th>
        <td>
           <select name="service_gubun" id="service_gubun" >
				<option value="">==전체==</option>
				<option value="O" <?php echo get_selected($service_gubun, 'O'); ?>>OS</option>
				<option value="B" <?php echo get_selected($service_gubun, 'B'); ?>>이즈브레</option>
			
			</select>
        </td>
    </tr>
	<tr>

        <th scope="row">결제구분</th>
        <td>
			<select name="od_settle_case" id="od_settle_case" >
				<option value="">==전체==</option>
				<option value="무통장" <?php echo get_selected($od_settle_case, '무통장'); ?>>무통장</option>
				<option value="가상계좌" <?php echo get_selected($od_settle_case, '가상계좌'); ?>>가상계좌</option>
				<option value="계좌이체" <?php echo get_selected($od_settle_case, '계좌이체'); ?>>계좌이체</option>
				<option value="휴대폰" <?php echo get_selected($od_settle_case, '휴대폰'); ?>>휴대폰</option>
				<option value="신용카드" <?php echo get_selected($od_settle_case, '신용카드'); ?>>신용카드</option>
				<option value="빌링결제" <?php echo get_selected($od_settle_case, '빌링결제'); ?>>빌링결제</option>
				<option value="KAKAOPAY" <?php echo get_selected($od_settle_case, 'KAKAOPAY'); ?>>KAKAOPAY</option>

			</select>
        </td>
		<th scope="row">검색</th>
        <td>
            <select name="sel_field" id="sel_field">
			<option value="od_id" <?php echo get_selected($sel_field, 'od_id'); ?>>주문번호</option>
			<option value="mb_id" <?php echo get_selected($sel_field, 'mb_id'); ?>>회원 ID</option>
			<option value="od_name" <?php echo get_selected($sel_field, 'od_name'); ?>>주문자</option>
			<option value="od_b_name" <?php echo get_selected($sel_field, 'od_b_name'); ?>>받는분</option>
			<option value="it_name" <?php echo get_selected($sel_field, 'it_name'); ?>>상품명</option>
            <option value="it_id" <?php echo get_selected($sel_field, 'it_id'); ?>>상품코드</option>
			<option value="pharm_name" <?php echo get_selected($sel_field, 'pharm_name'); ?>>약국명</option>
			<option value="pharm_custno" <?php echo get_selected($sel_field, 'pharm_custno'); ?>>약국코드</option>
			<option value="comp_name" <?php echo get_selected($sel_field, 'comp_name'); ?>>업체명</option>
			</select>

            <label for="search" class="sound_only">검색어<strong class="sound_only"> 필수</strong></label>
            <input type="text" name="search" value="<?php echo $search; ?>" id="search" class=" frm_input" autocomplete="off">
        </td>

		<th scope="row">업체선택</th>
        <td>
            <select name="comp_code" id="comp_code" style="width:200px;" >
                <option value="">==업체선택==</option>
                <?php
                $sql2 = " select comp_code, comp_name from tbl_member where mb_type='7' order by comp_name ";
                $result2 = sql_query($sql2);
                for ($i=0; $row2=sql_fetch_array($result2); $i++) {
                    if($comp_code ==$row2['comp_code'])
                        $checked='selected';
                    else
                        $checked ='';

                    echo '<option value="'.$row2['comp_code'].'" '.$checked.' >'.$row2['comp_name'].'</option>'.PHP_EOL;
                }
                ?>
            </select>
        </td>

        <th scope="row">정렬구분</th>
        <td>
            <select name="order_sort" id="order_sort">
                <option value="order" <?php echo get_selected($order_sort, 'order'); ?>>주문일</option>
                <option value="cancel" <?php echo get_selected($order_sort, 'cancel'); ?>>주문취소일</option>
                <option value="return_complete" <?php echo get_selected($order_sort, 'return_complete'); ?>>반품완료일</option>
            </select>
        </td>
    </tr>
	
    </tbody>
    </table>
</div>

</form>

<form name="forderlist" id="forderlist" method="post" autocomplete="off">
<input type="hidden" name="search_od_status" value="<?php echo $_GET["od_status"]; ?>">
<input type="hidden" name="command" value='1' id="command">

<div class="tbl_head01 tbl_wrap">
    <table id="sodr_list" style="overflow-x: scroll; white-space: nowrap;">
    <caption>주문 내역 목록</caption>
    <thead>
    <tr>
        <th scope="col" >서비스명</th>        
        <th scope="col" id="th_ordnum" width="150">주문번호</th>
		<th scope="col" id="th_odrer">약국(ID)</th>		
        <th scope="col" id="th_odrer" width="150">주문자</th>
        <th scope="col" id="th_recvr">받는분</th>
		<th scope="col" id="th_recvr">업체명</th>
		<th scope="col" width="70">
            <label for="chkall" class="sound_only">주문 전체</label>
            <input type="checkbox" name="chkall" value="1" id="chkall" onclick="check_all(this.form)">확정
        </th>

		<th scope="col" id="th_recvr" width="360">		
		<input type="checkbox" name="chkall2" value="1" id="chkall2" onclick="check_all2(this.form)" >주문상품
		</th>

		<th scope="col" id="th_recvr">단가</th>
		<th scope="col" id="th_recvr">수량</th>
		<th scope="col" id="th_recvr">합계</th>
		<th scope="col" id="th_recvr">수수료</th>
		<th scope="col" id="th_recvr">상태</th>	
		<? if(strpos($od_status,"반품") === false){ ?>	
        <th scope="col" >입금</th>
        <!--<th scope="col" ><input type="checkbox" name="chkall3" value="1" id="chkall3" onclick="check_all3(this.form)" ><br>출고여부</th>-->
		<? } ?>
    </tr>

    </thead>
    <tbody>
    <?php
    //for ($i=0; $row=sql_fetch_array($result); $i++)
	$i=0;
	foreach ($order as $key => $row) {

        $s_receipt_way = $s_br = "";
		$od_settle_case = $row['od_settle_case'];
        if ($row['od_settle_case'])
        {
            $s_receipt_way = $row['od_settle_case'];
            $s_br = '<br />';

            // 간편결제
            if($row['od_settle_case'] == '간편결제') {
                switch($row['od_pg']) {
                    case 'lg':
                        $s_receipt_way = 'PAYNOW';
                        break;
                    case 'inicis':
                        $s_receipt_way = 'KPAY';
                        break;
                    case 'kcp':
                        $s_receipt_way = 'PAYCO';
                        break;
                    default:
                        $s_receipt_way = $row['od_settle_case'];
                        break;
                }
            }
        }
        else
        {
            $s_receipt_way = '결제수단없음';
            $s_br = '<br />';
        }

        if ($row['od_receipt_point'] > 0)
            $s_receipt_way .= $s_br."포인트";

        $od_cnt = 0;
        if ($row['mb_id'])
        {
            $sql2 = " select count(*) as cnt from {$g5['g5_shop_order_table']} where mb_id = '{$row['mb_id']}' ";
            $row2 = sql_fetch($sql2);
            $od_cnt = $row2['cnt'];
        }

        // 주문 번호에 device 표시
        $od_mobile = '';
        if($row['od_mobile'])
            $od_mobile = '(M)';

        // 주문번호에 - 추가
        switch(strlen($row['od_id'])) {
            case 16:
                $disp_od_id = substr($row['od_id'],0,8).'-'.substr($row['od_id'],8);
                break;
            default:
                $disp_od_id = substr($row['od_id'],0,8).'-'.substr($row['od_id'],8);
                break;
        }

        // 주문 상태 관련 날짜 출력 (취소,반품)
        $od_status_date = '';
        if ($row['od_status'] == '취소') {
            if ($row['od_cancel_date']) $od_status_date = '<font color="red">('.$row['od_cancel_date'].')</font>';
        } else if (strpos($row['od_status'], '반품') !== false) {
            switch ($row['od_status']) {
                case '반품접수':
                    $od_status_date = '<font color="#228b22">('.$row['od_return_date_1'].')</font>';
                    break;
                case '반품반려':
                    $od_status_date = '<font color="#228b22">('.$row['od_return_date_2'].')</font>';
                    break;
                case '반품승인':
                    if ($row['od_return_date_4'] == '0000-00-00 00:00:00') {
                        $od_status_date = '<font color="#228b22">(2020-12-18 이전 반품승인일 조회는 불가능합니다.)</font>';
                    } else {
                        $od_status_date = '<font color="#228b22">('.$row['od_return_date_4'].')</font>';
                    }
                    break;
                case '반품완료':
                    $od_status_date = '<font color="#228b22">('.$row['od_return_date_3'].')</font>';
                    break;
            }
        }



		switch($row['od_gubun']) {
			case '1':
				$od_gubun = '고객주문';
				$od_num = 1;
				break;
			case '2':
				$od_gubun = '약국주문';
                $od_num = 2;
				break;
			case '3':
				$od_gubun = '일반주문';
                $od_num = 3;
				break;
		}

        // 직원 프로모션 구매 상품인지 체크
        $sql3 = "SELECT ca_staff_use FROM {$g5['g5_shop_category_table']} 
                  WHERE ca_id=(SELECT ca_id FROM {$g5['g5_shop_item_table']} WHERE it_id='$row[it_id]')";
        $row3 = sql_fetch($sql3);

        if ($row3['ca_staff_use'] == true) {
            $od_gubun = 'HC관주문';
            $od_num = 4;
        }

        if($row['od_gubun'] == 9 ){
            $od_gubun = '사내주문';
            $od_num = 9;

        }



		switch ($row['od_pay_yn'] ) {
			case "N" :
				$od_pay_yn = "미입금";
				break;
			case "Y" :
				$od_pay_yn = "입금";
				break;
		}
        
        switch ($row['od_company_yn']){
            case 'Y':
                $od_gubun = '자체배송';
                $od_num = 8;
                break;
        } 
        // 주문 번호에 에스크로 표시
        /*$od_paytype = '';
        if($row['od_test'])
            $od_paytype .= '<span class="list_test">테스트</span>';

        if($default['de_escrow_use'] && $row['od_escrow'])
            $od_paytype .= '<span class="list_escrow">에스크로</span>';
		*/

        //$uid = md5($row['od_id'].$row['od_time'].$row['od_ip']);

        //$invoice_time = is_null_time($row['od_invoice_time']) ? G5_TIME_YMDHIS : $row['od_invoice_time'];
        //$delivery_company = $row['od_delivery_company'] ? $row['od_delivery_company'] : $default['de_delivery_company'];

        //$bg = 'bg'.($i%2);
        //$td_color = 0;
        //if($row['od_cancel_price'] > 0) {
        //    $bg .= 'cancel';
        //    $td_color = 1;
        //}

		$od_id = $row['od_id'];
		$od_count = $row['od_count'];
		$comp_code = $row['comp_code'];

		$RowSpan = true;
		if ( $tmp_od_id == $od_id && $tmp_comp_code == $comp_code ) {
			$RowSpan = false;
		}

		$tmp_od_id = $od_id;
		$tmp_comp_code = $comp_code;

		$RowSpan2 = true;
		if ( $tmp_od_num == $od_num ) {
			$RowSpan2 = false;
		}

		$tmp_od_num = $od_num;

		$od_check_disabled = "";
		if ( $od_status != "확정"  ) {
			$od_check_disabled = "disabled";
		}

		$bgcolor_set = "";
		if( $order[$i][color_draw] == "Y" ){
			$bgcolor_set = "bgcolor='#facea7'";
		}else{
			$bgcolor_set = "";
		}

    ?>

	<!--<tr class="tr<?php echo $od_id; ?>" onMouseOver="trColorChange(this, 'Over')" onMouseOut="trColorChange(this, 'Out')">-->
    <tr class="tr<?php echo $od_id; ?>" <?=$bgcolor_set;?>">
		
         
		<?php if ( $RowSpan ) { ?>
		<td rowspan="<?php echo $od_count; ?>"   headers="th_ordnum"  >
		<?=TransServiceName($row['service_type']);?>
		</td>
        <td rowspan="<?php echo $od_count; ?>"   headers="th_ordnum"  >
            <a href="./orderform.php?od_id=<?php echo $row['od_id']; ?>&amp;<?php echo $qstr; ?>" >
			 <?php echo $disp_od_id; ?>
            <?php echo $od_mobile; ?><br/>
			<?php echo substr($row['od_date1'],11); ?>
			</a>
               
        </td>
		<td rowspan="<?php echo $od_count; ?>" headers="th_odrer" class="td_name ">
		    
			
			<?php if ($row['mb_id']) { ?>
				<?php echo $row['pharm_name']; ?><br/>
				<?php echo $row['mb_id']; ?><br/>
				<?php echo $row['pharm_mb_name']; ?><br/>
				영업 : <?php echo $row['pharm_manager_name']; ?>

            <?php } else { ?>
            비회원
            <?php } ?>
		</td>
		<td rowspan="<?php echo $od_count; ?>"  >
			<?php echo $row['od_name']; ?>(<?php echo $row['pharm_custno']; ?>)
			<div>
				<span class="icon_order_gubun<?php echo $od_num;?>"><?php echo $od_gubun; ?></span>
			</div>					
		</td>       
        <td rowspan="<?php echo $od_count; ?>" ><?php echo str_replace( "|" , "<br>" , get_text($row['od_b_name']) ); ?></a></td>
		<td rowspan="<?php echo $od_count; ?>" class="td_left ">&nbsp;<?php echo $row['comp_name']; ?></td>
		<?php } ?>
		
		<td width="60">
            <input type="hidden" name="od_id[<?php echo $i ?>]" value="<?php echo $row['od_id'] ?>" id="od_id_<?php echo $i ?>">
            <input type="hidden" name="od_num[<?php echo $i ?>]" value="<?php echo $row['od_num'] ?>" id="od_num_<?php echo $i ?>">
            <input type="hidden" name="od_seq[<?php echo $i ?>]" value="<?php echo $row['od_seq'] ?>" id="od_seq_<?php echo $i ?>">
            <input type="hidden" name="od_gubun[<?php echo $i ?>]" value="<?php echo $row['od_gubun'] ?>" id="od_gubun_<?php echo $i ?>">
            <input type="hidden" name="od_pay[<?php echo $i ?>]" value="<?php echo $row['od_pay_yn'] ?>" id="od_pay_<?php echo $i ?>">
            <input type="hidden" name="current_settle_case[<?php echo $i ?>]" value="<?php echo $row['od_settle_case'] ?>" id="current_settle_case_<?php echo $i ?>">
            <input type="hidden" name="od_name[<?php echo $i ?>]" value="<?php echo $row['od_name'] ?>" id="od_name_<?php echo $i ?>">
            <?php
            if ($row[od_status] != '취소') {
                if ($row[od_status] == '주문') {
                    if ($row[od_settle_case] == "가상계좌" and $row[od_pay_yn] == "N") {
                        echo '입금확인중';
                    } else {
                        echo '<label for="chk_'.$i.'" class="sound_only">주문번호 '.$row['od_id'].'</label>';
                        echo '<input type="checkbox" name="chk[]" value="'.$i.'" id="chk_'.$i.'">';
                    }
                } else if (strpos($row[od_status], '반품') !== false) {
                    echo '<font color="#228b22">'.$row[od_status].'</font>';
                } else {
                    echo '<font color="#0000FF">확정</font>';
                }
            } else {
                echo '<font color="#FF0000">취소</font>';
            }
            ?>
        </td>
		<td class="td_left ">
			<? 
			if( $row['od_multi_yn'] == "Y" ){ 
				$od_check_disabled = "disabled";
				echo "<font color='red'>다중배송</font>";
			}
			if( $row['od_pay_yn'] == "N" && $row['od_period_yn'] == 'Y' && $row['od_gubun'] == '3' ){
				$od_check_disabled = "disabled";			
			}
			?> 
			<input type="checkbox" name="trans_chk[]" value="<?php echo $i;?>" onclick="TransOpen()" <?php echo $od_check_disabled;?>  >

			<?php echo $row['it_name']; ?>
			<?php if ($row['od_period_yn'] == 'Y') { ?>
				<span >( <?php echo $row['od_seq'];?>회 - <?php echo $row['od_period_date'];?> ]</span>
			<?php } ?>
			<br/>
			<span><font color="blue"><?php echo $row['od_option']; ?> <?php echo substr( $row['io_id'],7,30 )?></font></span>
			<div id="od_delivery<?php echo $i; ?>" style="display:none;padding:5px 0 0 5px;">
				<select name="od_delivery_company[<?php echo $i; ?>]">
					<option value="">== 선택 ==</option>
					<?php echo get_delivery_company($delivery_company); ?>
				</select>
				<input type="text" name="od_invoice[<?php echo $i; ?>]" value="<?php echo $row['od_invoice']; ?>" class="frm_input2" size="20">
			  </div>
            <br><?php echo $od_status_date; ?>
			
        </td>
		
		<td width="100" class="td_num_right "><?php echo number_format($row['od_sum_drug_price']); ?></td>
		<td width="50" class="td_num_right "><?php echo $row['od_qty']; ?></td>
		<td width="100" class="td_num_right "><?php echo number_format($row['od_total_drug_price']); ?></td>
		<td width="100" class="td_num_right "><?php echo number_format($row['od_total_incen']); ?>P</td>
		<td width="70">
			<?
				if( $row['od_return_confirm'] == "Y" && $row['od_status'] == "반품접수" ){
					echo "<font color='red'>반품승인</font>";
				}else{
					echo $row['od_status'];
				}
			?>
		</td>
        <? if (strpos($od_status, "반품") === false) { ?>
            <?php if ($od_gubun == '일반주문' || $od_gubun = '임직원 프로모션') { // 일반주문은 선택입금 기능을 일괄로 처리 가능하도록 한다. ?>
                <?php if ($RowSpan) { ?>
                <td width="70" rowspan="<?php echo $od_count; ?>">
                    <?php echo $od_pay_yn; ?>
                    <?php if ($row['mb_pay_yn'] == 'Y' || ($row['od_gubun'] == '3' && $row['od_pay_yn'] == 'N')) { //약국관리에서 결제구분 ?>
                        <input type="checkbox" name="chkcash[]" value="<?php echo $i ?>" id="chkcash_<?php echo $i ?>">
                    <?php } ?>
                </td>
                <?php } ?>
            <?php } else { ?>
            <td width="70">
                <?php echo $od_pay_yn; ?>
                <?php if ($row['mb_pay_yn'] == 'Y' || ($row['od_gubun'] == '3' && $row['od_pay_yn'] == 'N')) { //약국관리에서 결제구분 ?>
                    <input type="checkbox" name="chkcash[]" value="<?php echo $i ?>" id="chkcash_<?php echo $i ?>">
                <?php } ?>
            </td>
            <?php } ?>
            <!--
		<td width="60">
		<? if ($row['it_id'] == "S00603" || $row['it_id'] == "S00604") { ?>
			<? if ($row['od_release_yn'] == "Y") { ?>
			출고
			<? } else { ?>
			미출고 <input type="checkbox" name="chk_release[]" value="<?php echo $i ?>" id="chk_<?php echo $i ?>">
			<? } ?>
		<? } ?>
		</td>
		-->
        <? } ?>
    </tr>

    <?php
		$i++;
    }
    sql_free_result($result);


	if ($od_status != "완료" and $od_status != "취소") {?>
	<tr>
	  <td colspan="6" align="left">
	  	
	  </td>
      <td><?php if ($od_status== "주문" or $od_status== "" ) { ?>
			<div class="local_cmd01 ">
			<input type="button" value="선택확정" class="btn_submit" onclick="forderlist_submit(1);">   
			</div>

		<?php } ?></td>
	 <td class="td_left " colspan="2">
	  <?php if ($od_status== "확정" ) { ?>
        선택한 주문건을 
	  	<select name="Change_ODR_STATUS" id="Change_ODR_STATUS" onChange="Change_Order_Status()">
			<option value="" selected="selected" width="150">==선택==</option>
			<option value="배송">배송중</option>
		</select>
		(으)로 변경
		<span class="local_cmd01 " id="TransBtn1">
			<input type="button" value="송장번호저장" class="btn_submit" onclick="forderlist_submit(3);">    
		</span>
	  <?php } ?>
		
	  </td>
	   <td align="right">
	   <strong><font color='#FF3300'> <?php echo number_format($total_qty); ?></font></strong>
	  </td>
	   <td>
	   <strong><font color='#FF3300'> <?php echo number_format($total_drug); ?></font></strong>
	  </td>
	   <td>
	   <strong><font color='#FF3300'> <?php echo number_format($total_incen); ?></font></strong>
	  </td>
	  <td>
	  </td>
	  <? if(strpos($od_status,"반품") === false){ ?>
	  <td>
		<?php if ($is_admin== "super" ) { ?>
			<div class="local_cmd01 ">
			<input type="button" value="선택입금" class="btn_submit" onclick="forderlist_submit(2);">   
			</div>
		<?php } ?>
	  </td>
	  <!--
	  <td>
		<?php if ($is_admin== "super" ) { ?>
			<div class="local_cmd01 ">
			<input type="button" value="선택출고" class="btn_submit" onclick="releaselist_submit();">   
			</div>
		<?php } ?>	  
	  </td>
	  -->
	  <? } ?>
	</tr>
<?php }

    if ($i == 0)
        echo '<tr><td colspan="14" class="empty_table">자료가 없습니다.</td></tr>';
    ?>
    </tbody>
    
    </table>
</div>

</form>

<?php 
echo get_paging(G5_IS_MOBILE ? $config['cf_mobile_pages'] : $config['cf_write_pages'], $page, $total_page, "{$_SERVER['SCRIPT_NAME']}?$qstr&amp;page="); ?>

<script>


function getCheckedCount(_obj){
	var cnt=0;
	var chkLen=0;
	if (_obj == null) return cnt;

	if(_obj.length == null){
		_objLen=1;			
	}else{
		_objLen= _obj.length;
	}
	
	for(var i=0; i<_objLen; i++){
		if(_obj.length == null){
			if(_obj.checked)
				cnt++;
		}else{
			if(_obj[i].checked)
				cnt++;
		}
	}
	return cnt;
}
function TransOpen() {
	var f = document.forderlist;
	if (f.Change_ODR_STATUS.value == "배송"){
		var chk = document.getElementsByName("trans_chk[]");
		for (var i=0; i<chk.length; i++)
		{
			if (chk[i].checked)
			{
				var k = chk[i].value;
				$('#od_delivery'+k).css('display','');
			} else {
				var k = chk[i].value;
				$('#od_delivery'+k).css('display','none');
			}
		}
	}
}
function Change_Order_Status(){

	var f = document.forderlist;

	if (!is_checked("trans_chk[]")) {
		alert("처리 하실 항목을 하나 이상 선택하세요.");
		return false;
	}
	var chk = document.getElementsByName("trans_chk[]");
	for (var i=0; i<chk.length; i++)
	{
		if (chk[i].checked)
		{
			var k = chk[i].value;
			$('#od_delivery'+k).css('display','');
		}
	}	
}

function trColorChange(id, chk) {
	var ID_Len = $("." + id.className).length;

	if (chk == "Over") {
		$("." + id.className).each(function (index, element) {
			$(this).css("background-color", "#F7F7F7");
		});
	} else {
		$("." + id.className).each(function (index, element) {
			$(this).css("background-color", "#FFFFFF");
		});
	}
}


$(function(){
    $("#fr_date, #to_date").datepicker({ changeMonth: true, changeYear: true, dateFormat: "yy-mm-dd", showButtonPanel: true, yearRange: "c-99:c+99", maxDate: "+0d" });

    // 주문상품보기
    $(".orderitem").on("click", function() {
        var $this = $(this);
        var od_id = $this.text().replace(/[^0-9]/g, "");

        if($this.next("#orderitemlist").size())
            return false;

        $("#orderitemlist").remove();

        $.post(
            "./ajax.orderitem.php",
            { od_id: od_id },
            function(data) {
                $this.after("<div id=\"orderitemlist\"><div class=\"itemlist\"></div></div>");
                $("#orderitemlist .itemlist")
                    .html(data)
                    .append("<div id=\"orderitemlist_close\"><button type=\"button\" id=\"orderitemlist-x\" class=\"btn_frmline\">닫기</button></div>");
            }
        );

        return false;
    });

    // 상품리스트 닫기
    $(".orderitemlist-x").on("click", function() {
        $("#orderitemlist").remove();
    });

    $("body").on("click", function() {
        $("#orderitemlist").remove();
    });

    // 엑셀배송처리창
    $("#order_delivery").on("click", function() {
        var opt = "width=600,height=450,left=10,top=10";
        window.open(this.href, "win_excel", opt);
        return false;
    });
});

function set_date(today)
{
    <?php
    $date_term = date('w', G5_SERVER_TIME);
    $week_term = $date_term + 7;
    $last_term = strtotime(date('Y-m-01', G5_SERVER_TIME));
    ?>
    if (today == "오늘") {
        document.getElementById("fr_date").value = "<?php echo G5_TIME_YMD; ?>";
        document.getElementById("to_date").value = "<?php echo G5_TIME_YMD; ?>";
    } else if (today == "어제") {
        document.getElementById("fr_date").value = "<?php echo date('Y-m-d', G5_SERVER_TIME - 86400); ?>";
        document.getElementById("to_date").value = "<?php echo date('Y-m-d', G5_SERVER_TIME - 86400); ?>";
    } else if (today == "이번주") {
        document.getElementById("fr_date").value = "<?php echo date('Y-m-d', strtotime('-'.$date_term.' days', G5_SERVER_TIME)); ?>";
        document.getElementById("to_date").value = "<?php echo date('Y-m-d', G5_SERVER_TIME); ?>";
    } else if (today == "이번달") {
        document.getElementById("fr_date").value = "<?php echo date('Y-m-01', G5_SERVER_TIME); ?>";
        document.getElementById("to_date").value = "<?php echo date('Y-m-d', G5_SERVER_TIME); ?>";
    } else if (today == "지난주") {
        document.getElementById("fr_date").value = "<?php echo date('Y-m-d', strtotime('-'.$week_term.' days', G5_SERVER_TIME)); ?>";
        document.getElementById("to_date").value = "<?php echo date('Y-m-d', strtotime('-'.($week_term - 6).' days', G5_SERVER_TIME)); ?>";
    } else if (today == "지난달") {
        document.getElementById("fr_date").value = "<?php echo date('Y-m-01', strtotime('-1 Month', $last_term)); ?>";
        document.getElementById("to_date").value = "<?php echo date('Y-m-t', strtotime('-1 Month', $last_term)); ?>";
	} else if (today == "12개월") {
        document.getElementById("fr_date").value = "<?php echo date('Y-m-01', strtotime('-1 Year', $last_term)); ?>";
        document.getElementById("to_date").value = "<?php echo date('Y-m-d', G5_SERVER_TIME); ?>";
    } else if (today == "전체") {
        document.getElementById("fr_date").value = "";
        document.getElementById("to_date").value = "";
    }
}
</script>

<script>
function check_all2(f)
{
    var chk = document.getElementsByName("trans_chk[]");

    for (i=0; i<chk.length; i++) {
		if (!chk[i].disabled){
			chk[i].checked = f.chkall2.checked;
		}        
	}
	TransOpen();
}
function check_all3(f)
{
    var chk = document.getElementsByName("chk_release[]");

    for (i=0; i<chk.length; i++) {
		if (!chk[i].disabled){
			chk[i].checked = f.chkall3.checked;
		}        
	}
}
function forderlist_submit(action)
{
	
	var command = document.getElementById("command");
	command.value = action;
	
	var f = document.forderlist;
	
	if(action ==1) {
		if (!is_checked("chk[]")) {
			alert("확정 하실 항목을 하나 이상 선택하세요.");
			return false;
		}

        var chk2= document.getElementsByName("chk[]");

        for (var j=0; j<chk2.length; j++)
        {
            if (chk2[j].checked)
            {
                var m = chk2[j].value;
                var current_settle_case = f.elements['current_settle_case['+m+']'].value;
                //var current_status = f.elements['current_status['+k+']'].value;
                var od_gubun      = f.elements['od_gubun['+m+']'].value;
                var od_pay = f.elements['od_pay['+m+']'].value;
                var od_id = f.elements['od_id['+m+']'].value;

                if (od_gubun == 3 && current_settle_case == '무통장' && od_pay == 'N') {
                    alert('미입금 주문은 확정 할 수 없습니다.\r\n' + '주문번호 : ' + od_id);
                    return false;
                }
            }
        }

		if (!confirm("선택하신 주문서의 주문상태를 확정 상태로 변경하시겠습니까?"))
			return false;
	
	}
	if(action ==3) {

		if (!is_checked("trans_chk[]")) {
			alert("처리 하실 항목을 하나 이상 선택하세요.");
			return false;
		}
		//if (!confirm("선택하신 주문서의 주문상태를 배송중 상태로 변경하시겠습니까?"))
		//	return false;

		var chk = document.getElementsByName("trans_chk[]");

		for (var i=0; i<chk.length; i++)
		{
			if (chk[i].checked)
			{
				var k = chk[i].value;
				//var current_settle_case = f.elements['current_settle_case['+k+']'].value;
				//var current_status = f.elements['current_status['+k+']'].value;
				
				var invoice      = f.elements['od_invoice['+k+']'];					   
				var delivery_company = f.elements['od_delivery_company['+k+']'];
				if ($.trim(delivery_company.value) == '') {
					alert("배송업체를 입력하시기 바랍니다.");
					delivery_company.focus();
					return false;
				}
				if ($.trim(invoice.value) == '') {
					alert("운송장번호를 입력하시기 바랍니다.");
					invoice.focus();
					return false;
				}				
			}
		}
		if (!confirm("선택하신 주문서를 배송중 상태로 변경하시겠습니까?"))
			return false;
	}
	if(action ==2) {
		if (!is_checked("chkcash[]")) {
			alert("입금처리 하실 항목을 하나 이상 선택하세요.");
			return false;
		}
		if (!confirm("선택하신 주문서를 입금처리 상태로 변경하시겠습니까?"))
			return false;
	
	}
    f.action = "./orderlistupdate.php";
	f.submit();
    return true;
}
function releaselist_submit()
{
	var f = document.forderlist;

	if (!is_checked("chk_release[]")) {
		alert("확정 하실 항목을 하나 이상 선택하세요.");
		return false;
	}
	if (!confirm("선택하신 주문서를 출고로 변경하시겠습니까?"))
		return false;

    f.action = "./releaselistupdate.php";
	f.submit();
    return true;
}
</script>
<div id="Excel_Select" title="엑셀 출력 선택">
	<form name="ExcelFrm" id="ExcelFrm" action="orderlist_01.php?<?php echo $qstr; ?>" method="post" target="iFrm">
		<table class="table_style1">
			<tr>
				<td width="150">선택</td>
				<td><input type="radio" name="ptype" value="1" id="ExcelParamSelect1"><label for="ExcelParamSelect1">전체</label>
				    <br/>
					<input type="radio" name="ptype" value="2" checked id="ExcelParamSelect2"><label for="ExcelParamSelect2">송장발행</label>
				</td>
			</tr>
			
		</table>
	</form>
</div>
<script>
	$("#Excel_Select").dialog({
		autoOpen: false,
		resizable: false,
		modal: true,
		width: 800,
		buttons: {
			"엑셀다운로드": function () {
				$("#ExcelFrm").submit();
				$(this).dialog("close");
			},
			Cancel: function () {
				$(this).dialog("close");
			}
		}
	});

	$("#ExcelDownButton").click(function () {
		$("#Excel_Select").dialog("open");
	});

</script>
<!-- (s)hidden Frame -->
<iframe name="iFrm" style="display: none;" ></iframe>

<!--업체선택 검색기능-->

<link href="../select2.css" rel="stylesheet"/>
<script type="text/javascript" src="../select2.js"></script>
<script>
$(document).ready(function () {
	$("#comp_code").select2();
});
</script>

<?php
include_once (G5_ADMIN_PATH.'/admin.tail.php');
?>

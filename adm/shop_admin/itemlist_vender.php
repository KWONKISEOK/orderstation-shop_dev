<?php
$sub_menu = '600300';
include_once('./_common.php');

auth_check($auth[$sub_menu], "r");

$g5['title'] = '상품관리';
include_once (G5_ADMIN_PATH.'/admin.head.php');

// 분류
$ca_list  = '<option value="">선택</option>'.PHP_EOL;
$sql = " select * from {$g5['g5_shop_category_table']} ";
//if ($is_admin != 'super')
//    $sql .= " where ca_mb_id = '{$member['mb_id']}' ";
$sql .= " order by ca_order, ca_id ";
$result = sql_query($sql);
for ($i=0; $row=sql_fetch_array($result); $i++)
{
    $len = strlen($row['ca_id']) / 2 - 1;
    $nbsp = '';
    for ($i=0; $i<$len; $i++) {
        $nbsp .= '&nbsp;&nbsp;&nbsp;';
    }
    $ca_list .= '<option value="'.$row['ca_id'].'">'.$nbsp.$row['ca_name'].'</option>'.PHP_EOL;
}

$where = " and ";
$sql_search = "";
if ($stx != "") {
    if ($sfl != "") {
        $sql_search .= " $where $sfl like '%$stx%' ";
        $where = " and ";
    }
    if ($save_stx != $stx)
        $page = 1;
}

if ($sca != "") {
    $sql_search .= " $where (a.ca_id like '$sca%' or a.ca_id2 like '$sca%' or a.ca_id3 like '$sca%') ";
}

if ($sfl == "")  $sfl = "it_name";

$sql_common = " from {$g5['g5_shop_item_table']} a ,
                     {$g5['g5_shop_category_table']} b
               where (a.ca_id = b.ca_id";
if ($is_admin == 'vender')
    $sql_common .= " and a.comp_code = '{$member['comp_code']}'";

if ($_GET['it_status'] != '')
    $sql_common .= " and a.it_status = '{$_GET['it_status']}' ";

$sql_common .= ") ";
$sql_common .= $sql_search;

// 테이블의 전체 레코드수만 얻음
$sql = " select count(*) as cnt " . $sql_common;
$row = sql_fetch($sql);
$total_count = $row['cnt'];

$rows = $config['cf_page_rows'];
$total_page  = ceil($total_count / $rows);  // 전체 페이지 계산
if ($page < 1) { $page = 1; } // 페이지가 없으면 첫 페이지 (1 페이지)
$from_record = ($page - 1) * $rows; // 시작 열을 구함

if (!empty($sst)) {
    $sst  = "it_time";
    $sod = "desc";
	$sql_order = "order by it_order asc , $sst $sod";
}else{
	$sql_order = "order by it_order asc , it_time desc";
}


$sql  = " select *
           $sql_common
           $sql_order
           limit $from_record, $rows ";
$result = sql_query($sql);

//$qstr  = $qstr.'&amp;sca='.$sca.'&amp;page='.$page;
$qstr  = $qstr.'&amp;sca='.$sca.'&amp;page='.$page.'&amp;save_stx='.$stx;

$listall = '<a href="'.$_SERVER['SCRIPT_NAME'].'" class="ov_listall">전체목록</a>';
?>

<div class="local_ov01 local_ov">
    <?php echo $listall; ?>
    <span class="btn_ov01"><span class="ov_txt">등록된 상품</span><span class="ov_num"> <?php echo $total_count; ?>건</span></span>
</div>

<form name="flist" class="local_sch01 local_sch">
<input type="hidden" name="page" value="<?php echo $page; ?>">
<input type="hidden" name="save_stx" value="<?php echo $stx; ?>">
<input type="hidden" id="comp_code" name="comp_code"  value="<?php echo $member['comp_code'];?>" />

<div class="local_sch03  tbl_wrap" style="padding:10px;">
    <table>
   
    <tbody>
	<tr>
		<th scope="row">등록상태</th>
		<td>
			<select name="it_status" id="it_status" style="width:150px" onchange="this.form.submit()">
				<option value=""<?php echo get_selected($_GET['it_status'], ""); ?>>==등록상태==</option>
				<option value="0"<?php echo get_selected($_GET['it_status'], "0"); ?>>공급사등록</option>
				<option value="2"<?php echo get_selected($_GET['it_status'], "2"); ?>>등록신청</option>
				<option value="3"<?php echo get_selected($_GET['it_status'], "3"); ?>>승인</option>
				<option value="4"<?php echo get_selected($_GET['it_status'], "4"); ?>>서비스중지</option>
			</select>
        </td>
		<th scope="row">판매여부</th>
        <td>
            <select name="it_use" id="it_use" onchange="this.form.submit()" >
				<option value="">==전체==</option>
				<option value="1" <?php echo get_selected($it_use, '1'); ?>>예</option>
				<option value="0" <?php echo get_selected($it_use, '0'); ?>>아니오</option>
			</select>
        </td>
		
		<th scope="row">상품분류</th>
        <td>
           <select name="sca" id="sca">
				<option value="">전체분류</option>
				<?php
				$sql1 = " select ca_id, ca_name from {$g5['g5_shop_category_table']} order by ca_order, ca_id ";
				$result1 = sql_query($sql1);
				for ($i=0; $row1=sql_fetch_array($result1); $i++) {
					$len = strlen($row1['ca_id']) / 2 - 1;
					$nbsp = '';
					for ($i=0; $i<$len; $i++) $nbsp .= '&nbsp;&nbsp;&nbsp;';
					echo '<option value="'.$row1['ca_id'].'" '.get_selected($sca, $row1['ca_id']).'>'.$nbsp.$row1['ca_name'].'</option>'.PHP_EOL;
				}
				?>
			</select>
        </td>
		<th scope="row">검색</th>
        <td>
            <select name="sfl" id="sfl">
				<option value="it_name" <?php echo get_selected($sfl, 'it_name'); ?>>상품명</option>
				<option value="it_id" <?php echo get_selected($sfl, 'it_id'); ?>>상품코드</option>
				<option value="it_maker" <?php echo get_selected($sfl, 'it_maker'); ?>>제조사</option>
			</select>
			<label for="stx" class="sound_only">검색어</label>
			<input type="text" name="stx" value="<?php echo $stx ?>" id="stx" class=" frm_input">
        </td>

		<td>
			 <input type="submit" value="검색" class="get_theme_confc btn btn_01">
			 <input type="button" value="엑셀 다운로드" class="get_theme_confc btn btn_02" style="cursor:pointer" onclick="formSubmit(1);">
		</td>
		
    </tr>
	
    </tbody>
    </table>
</div>


</form>

<form name="fitemlistupdate" method="post" action="./itemlistupdate_vender.php" onsubmit="return fitemlist_submit(this);" autocomplete="off" id="fitemlistupdate">
<input type="hidden" name="sca" value="<?php echo $sca; ?>">
<input type="hidden" name="sst" value="<?php echo $sst; ?>">
<input type="hidden" name="sod" value="<?php echo $sod; ?>">
<input type="hidden" name="sfl" value="<?php echo $sfl; ?>">
<input type="hidden" name="stx" value="<?php echo $stx; ?>">
<input type="hidden" name="page" value="<?php echo $page; ?>">

<div class="tbl_head01 tbl_wrap">
    <table>
    <caption><?php echo $g5['title']; ?> 목록</caption>
    <thead>
    <tr>
        <th scope="col">
            <!-- <label for="chkall" class="sound_only">상품 전체</label>
            <input type="checkbox" name="chkall" value="1" id="chkall" onclick="check_all(this.form)"> -->
        </th>
        <th scope="col"><?php echo subject_sort_link('it_id', 'sca='.$sca); ?>상품코드</a></th>
      
        <th scope="col"><?php echo subject_sort_link('it_status', 'sca='.$sca, 1); ?>등록상태</a></th>
        <th scope="col"><?php echo subject_sort_link('it_time_reg', 'sca='.$sca, 1); ?>신청일</a></th>
		<th scope="col"><?php echo subject_sort_link('it_time_confirm', 'sca='.$sca, 1); ?>승인일</a></th>
   
        <th scope="col" id="th_img">이미지</th>
        <th scope="col" id="th_pc_title"><?php echo subject_sort_link('it_name', 'sca='.$sca); ?>상품명</a></th>
        <th scope="col" id="th_amt"><?php echo subject_sort_link('it_supply_price', 'sca='.$sca); ?>공급가</a></th>
        <!-- <th scope="col" id="th_camt"><?php echo subject_sort_link('it_cust_price', 'sca='.$sca); ?>시중가격</a></th> -->

        <!-- <th scope="col" id="th_pt"><?php echo subject_sort_link('it_point', 'sca='.$sca); ?>포인트</a></th> -->
        <th scope="col" id="th_qty"><?php echo subject_sort_link('it_stock_qty', 'sca='.$sca); ?>재고</a></th>

		<th scope="col">관리</th>
        
    </tr>
    </thead>
    <tbody>
    <?php
    for ($i=0; $row=sql_fetch_array($result); $i++)
    {
        $href = G5_SHOP_URL.'/item.php?it_id='.$row['it_id'];
        $bg = 'bg'.($i%2);

        //$it_point = $row['it_point'];
        //if($row['it_point_type'])
        //    $it_point .= '%';

		switch($row['it_status']) {
            case '0':
                $it_status = '공급사등록';
                break;
			case '2':
                $it_status = '등록요청';
                break;
            case '3':
                $it_status = '승인';
                break;
            case '4':
                $it_status = '서비스중지';
                break;
        }
		$readonly = '';
		if($row['it_status'] != '0') {
			$readonly = 'disabled';
		}
    ?>
    <tr class="<?php echo $bg; ?>">
        <td class="td_chk">
            <label for="chk_<?php echo $i; ?>" class="sound_only"><?php echo get_text($row['it_name']); ?></label>
            <input type="checkbox" name="chk[]" value="<?php echo $i ?>" id="chk_<?php echo $i; ?>" <?php echo $readonly;?> >
        </td>
        <td class="td_num">
            <input type="hidden" name="it_id[<?php echo $i; ?>]" value="<?php echo $row['it_id']; ?>">
            <?php echo $row['it_id']; ?>
        </td>
      
      
        <td>
            <label for="use_<?php echo $i; ?>" class="sound_only">등록상태</label>
            <?php echo $it_status; ?>


        </td>
        <td>
            <?php if(substr($row['it_time_reg'],0,10) != '0000-00-00') echo substr($row['it_time_reg'],0,10); ?>
        </td>
		<td>
            <?php if(substr($row['it_time_confirm'],0,10) != '0000-00-00') echo substr($row['it_time_confirm'],0,10); ?>
        </td>

        <td class="td_img"><a href="<?php echo $href; ?>"><?php echo get_it_image($row['it_id'], 50, 50); ?></a></td>
        <td headers="th_pc_title" class="td_input">
            <label for="name_<?php echo $i; ?>" class="sound_only">상품명</label>
            <?php echo htmlspecialchars2(cut_str($row['it_name'],250, "")); ?>
        </td>
        <td headers="th_amt" class="td_numbig td_input">
            <label for="price_<?php echo $i; ?>" class="sound_only">공급가</label>
            <?php echo number_format($row['it_supply_price']); ?>
        </td>
        <!-- <td headers="th_camt" class="td_numbig td_input">
            <label for="cust_price_<?php echo $i; ?>" class="sound_only">시중가격</label>
            <input type="text" name="it_cust_price[<?php echo $i; ?>]" value="<?php echo $row['it_cust_price']; ?>" id="cust_price_<?php echo $i; ?>" class="tbl_input sit_camt" size="7">
        </td> -->

        <!-- <td headers="th_pt" class="td_numbig td_input"><?php echo $it_point; ?></td> -->
        <td headers="th_qty" class="td_numbig td_input">
			<?php echo $row['it_stock_qty']; ?>
        </td>

<!-- <td class="td_mng td_mng_s"> -->
		 <td width="200">
            <a href="./itemform.php?w=u&amp;it_id=<?php echo $row['it_id']; ?>&amp;ca_id=<?php echo $row['ca_id']; ?>&amp;<?php echo $qstr; ?>" class="btn btn_03"><span class="sound_only"><?php echo htmlspecialchars2(cut_str($row['it_name'],250, "")); ?> </span>수정</a>
            <a href="./itemcopy.php?it_id=<?php echo $row['it_id']; ?>&amp;ca_id=<?php echo $row['ca_id']; ?>" class="itemcopy btn btn_02" target="_blank"><span class="sound_only"><?php echo htmlspecialchars2(cut_str($row['it_name'],250, "")); ?> </span>복사</a>
            <a href="<?php echo $href; ?>" class="btn btn_02"><span class="sound_only"><?php echo htmlspecialchars2(cut_str($row['it_name'],250, "")); ?> </span>보기</a>
        </td>

    </tr>
    <?php
    }
    if ($i == 0)
        echo '<tr><td colspan="12" class="empty_table">자료가 한건도 없습니다.</td></tr>';
    ?>
    </tbody>
    </table>
</div>

<div class="btn_fixed_top">

    <a href="./itemform.php" class="btn btn_01">상품등록</a>
    <!-- <a href="./itemexcel.php" onclick="return excelform(this.href);" target="_blank" class="btn btn_02">상품일괄등록</a> -->
    <input type="submit" name="act_button" value="승인신청" onclick="document.pressed=this.value" class="btn btn_02">
    <?php if ($is_admin == 'super') { ?>
    <input type="submit" name="act_button" value="선택삭제" onclick="document.pressed=this.value" class="btn btn_02">
    <?php } ?>
</div>
<!-- <div class="btn_confirm01 btn_confirm">
    <input type="submit" value="일괄수정" class="btn_submit" accesskey="s">
</div> -->
</form>

<?php 
$qstr ='it_status='.$_GET['it_status'].'&'.$qstr;
echo get_paging(G5_IS_MOBILE ? $config['cf_mobile_pages'] : $config['cf_write_pages'], $page, $total_page, "{$_SERVER['SCRIPT_NAME']}?$qstr&amp;page="); ?>

<script>
function formSubmit(type) {
	var form = "<form action='itemlist_vender_01.php' method='post'>"; 
		form += "<input type='hidden' name='mode' value='excel' />"; 
		form += "<input type='hidden' name='comp_code' value='"+$('#comp_code').val()+"' />"; 
		form += "<input type='hidden' name='it_use' value='"+$('#it_use option:selected').val()+"' />"; 
		form += "<input type='hidden' name='sca' value='"+$('#sca option:selected').val()+"' />"; 	
		form += "<input type='hidden' name='sfl' value='"+$('#sfl option:selected').val()+"' />"; 
		form += "<input type='hidden' name='stx' value='"+$('#stx').val()+"' />"; 
		form += "</form>"; 
		jQuery(form).appendTo("body").submit().remove(); 
}

function fitemlist_submit(f)
{
    if (!is_checked("chk[]")) {
        alert(document.pressed+" 하실 항목을 하나 이상 선택하세요.");
        return false;
    }

    if(document.pressed == "선택삭제") {
        if(!confirm("선택한 자료를 정말 삭제하시겠습니까?")) {
            return false;
        }
    }

    return true;
}

$(function() {
    $(".itemcopy").click(function() {
        var href = $(this).attr("href");
        window.open(href, "copywin", "left=100, top=100, width=300, height=200, scrollbars=0");
        return false;
    });
});

function excelform(url)
{
    var opt = "width=600,height=450,left=10,top=10";
    window.open(url, "win_excel", opt);
    return false;
}
</script>

<?php
include_once (G5_ADMIN_PATH.'/admin.tail.php');
?>

<?php
$sub_menu = '400400';
include_once('./_common.php');
include_once('./admin.shop.lib.php');

define("_ORDERMAIL_", true);


$i_price     = (int)$_POST['od_price'];
$i_send_cost  = (int)$_POST['od_send_cost'];
$i_send_cost2  = (int)$_POST['od_send_cost2'];
$i_send_coupon  = (int)$_POST['od_send_coupon'];
$i_temp_point = (int)$_POST['od_temp_point'];


// 주문금액이 상이함
$sql = " select SUM(IF(io_type = 1, (io_price * ct_qty), ((ct_price + io_price) * ct_qty))) as od_price,
              COUNT(distinct it_id) as cart_count
            from {$g5['g5_shop_cart_table']} where ct_status = '사내' ";

$row = sql_fetch($sql);
$tot_ct_price = $row['od_price'];
$cart_count = $row['cart_count'];
$tot_od_price = $tot_ct_price;


// 주문번호를 얻는다.
$od_id = get_session('ss_cart_id');
//$od_id = get_session('ss_order_id');

$od_email         = get_email_address($od_email);
$od_name          = clean_xss_tags($od_name);
$od_tel           = clean_xss_tags($od_tel);
$od_hp            = clean_xss_tags($od_hp);
$od_zip           = preg_replace('/[^0-9]/', '', $od_zip);
$od_zip1          = substr($od_zip, 0, 3);
$od_zip2          = substr($od_zip, 3);
$od_addr1         = clean_xss_tags($od_addr1);
$od_addr2         = clean_xss_tags($od_addr2);
$od_addr3         = clean_xss_tags($od_addr3);
$od_addr_jibeon   = preg_match("/^(N|R)$/", $od_addr_jibeon) ? $od_addr_jibeon : '';
$od_b_name        = clean_xss_tags($od_b_name);
$od_b_tel         = clean_xss_tags($od_b_tel);
$od_b_hp          = clean_xss_tags($od_b_hp);
$od_b_addr1       = clean_xss_tags($od_b_addr1);
$od_b_addr2       = clean_xss_tags($od_b_addr2);
$od_b_addr3       = clean_xss_tags($od_b_addr3);
$od_b_addr_jibeon = preg_match("/^(N|R)$/", $od_b_addr_jibeon) ? $od_b_addr_jibeon : '';
$od_memo          = clean_xss_tags($od_memo);
$od_deposit_name  = clean_xss_tags($od_deposit_name);
$od_tax_flag      = $default['de_tax_flag_use'];
$od_gubun         = 9;

/*************** 트랜잭션 관련 ****************/
$error_cnt = 0;
mysqli_autocommit($g5['connect_db'], false);
/*************** 트랜잭션 관련 ****************/

// 주문서에 입력
$sql = " insert {$g5['g5_shop_order_table']}
            set od_id             = '$od_id',
                mb_id             = '{$member['mb_id']}',
                od_pwd            = '$od_pwd',
				od_gubun          = '$od_gubun',
                pharm_name        = '$pharm_name',
				pharm_manager     = '$pharm_manager',
				pharm_manager_dept= '$pharm_manager_dept',
				od_name           = '$od_name',
                od_email          = '$od_email',
                od_tel            = '$od_tel',
                od_hp             = '$od_hp',
                od_zip1           = '$od_zip1',
                od_zip2           = '$od_zip2',
                od_addr1          = '$od_addr1',
                od_addr2          = '$od_addr2',
                od_addr3          = '$od_addr3',
                od_addr_jibeon    = '$od_addr_jibeon',
                od_b_name         = '$od_b_name',
                od_b_tel          = '$od_b_tel',
                od_b_hp           = '$od_b_hp',
                od_b_zip1         = '$od_b_zip1',
                od_b_zip2         = '$od_b_zip2',
                od_b_addr1        = '$od_b_addr1',
                od_b_addr2        = '$od_b_addr2',
                od_b_addr3        = '$od_b_addr3',
                od_b_addr_jibeon  = '$od_b_addr_jibeon',
                od_deposit_name   = '$od_deposit_name',
                od_memo           = '$od_memo',
                od_cart_count     = '$cart_count',
                od_cart_price     = '$tot_ct_price',
                od_cart_coupon    = 0,
                od_send_cost      = 0,
                od_send_coupon    = 0,
                od_send_cost2     = 0,
                od_coupon         = 0,
                od_receipt_price  = '$od_receipt_price',
                od_receipt_point  = '$od_receipt_point',
                od_bank_account   = '$od_bank_account',
                od_receipt_time   = '$od_receipt_time',
                od_misu           = '$od_misu',
                od_pg             = '$od_pg',
                od_tno            = '$od_tno',
				od_billkey        = '$od_billkey',
                od_app_no         = '$od_app_no',
                od_escrow         = '$od_escrow',
                od_tax_flag       = '$od_tax_flag',
                od_tax_mny        = '$od_tax_mny',
                od_vat_mny        = '$od_vat_mny',
                od_free_mny       = '$od_free_mny',
                od_status         = '주문',
                od_shop_memo      = '',
                od_hope_date      = '$od_hope_date',
                od_time           = '".G5_TIME_YMDHIS."',
                od_ip             = '$REMOTE_ADDR',
				pharm_custno      = '$pharm_custno',
                od_settle_case    = '$od_settle_case'
                ";
$result = sql_query($sql, false);
/*************** 트랜잭션 관련 ****************/
if( mysqli_errno($g5['connect_db']) ){
	$error_cnt += 1;
}
/*************** 트랜잭션 관련 ****************/

//echo $sql;

//주문 상세정보 생성
 $sql = "select a.io_id, a.ct_option, a.ct_price, a.ct_qty, a.it_sc_cost, a.ct_period_yn, a.ct_period_sdate, a.ct_period_week, a.ct_period_cnt, a.ct_period_memo,b.*
		  from tbl_shop_cart a, tbl_shop_item b
		where a.it_id = b.it_id
		   and a.ct_status = '사내'";

$result = sql_query($sql);
for ($i=0; $row=sql_fetch_array($result); $i++)
{
	$od_num = $i+1;
	//1. 주문상세정보
	//단가정보
	$io_sale_price		= 0;
	$io_drug_price		= 0;
	$io_incen			= 0;
	$io_supply_price    = 0;
	$io_dodome_price    = 0;
	$io_period_rate1    = 0;
	$io_period_cnt1     = 0;
	$od_sum_qty         = 0;
	

	$od_price	= $row['it_supply_price'] * $row['ct_qty'];
	$od_qty		= $row['ct_qty'] ;

	$it_supply_price	= $row['it_supply_price'] ;	
	$it_dodome_price	= $row['it_dodome_price'] ;
	if($od_gubun == '3') {//일반회원 주문
		$it_sale_price		= $row['it_app_sale_price'] ;
		$it_drug_price		= $row['it_app_drug_price'] ;
		$it_incen	= $row['it_app_incen'] ;
	} else {
		$it_sale_price		= $row['it_supply_price'] ;
		$it_drug_price		= $row['it_supply_price'];
		$it_incen		= $row['it_incen'] ;
	}
	if($row['io_id'] != '') {
		//옵션단가 재계산
		$sql = " select *
					from {$g5['g5_shop_item_option_table']} 
					where it_id = '{$row['it_id']}'
					  and io_id = '{$row['io_id']}'";

		$io = sql_fetch($sql);
		$io_supply_price	= $io['io_supply_price'] ;
		$io_dodome_price	= $io['io_dodome_price'];
		$io_period_rate1    = $io['io_period_rate1'];
		$io_period_cnt1		= $io['io_period_cnt1'];

		if($od_gubun == '3') {//일반회원 주문
			$io_sale_price		= $io['io_app_sale_price'] ;
			$io_drug_price		= $io['io_app_drug_price'] ;
			$io_incen			= $io['io_app_incen'] ;

		} else {
			$io_sale_price		= $io['io_price'] ;
			$io_drug_price		= $io['io_drug_price'] ;
			$io_incen			= $io['io_incen'] ;

		}
		$od_sum_supply_price	= $it_supply_price+$io_supply_price;
		$od_sum_dodome_price	= $it_dodome_price+$io_dodome_price;
		$od_sum_sale_price		= $it_sale_price+$io_sale_price;
		$od_sum_drug_price		= $it_drug_price+$io_drug_price;
		$od_sum_incen			= $it_incen+$io_incen  ;			

	} else {
		$od_sum_supply_price	= $it_supply_price;
		$od_sum_dodome_price	= $it_dodome_price;
		$od_sum_sale_price		= $it_sale_price;
		$od_sum_drug_price		= $it_drug_price;
		$od_sum_incen			= $it_incen ;
	}
	

	$od_total_supply_price	= 0;
	$od_total_dodome_price	= 0;
	$od_total_sale_price	= 0 ;
	$od_total_drug_price	= 0;
	$od_total_incen			= 0 ;
	$od_sum_qty				= $od_qty ;

	//상품별
	$sql = " insert tbl_shop_order_detail
            set od_id         = '$od_id',
                it_id         = '{$row['it_id']}',
				od_num        = $od_num,
                it_name       = '{$row['it_name']}',
				io_id         = '{$row['io_id']}',
				od_option     = '{$row['ct_option']}',
				od_price      = '$od_price',
				od_qty        = '$od_sum_qty',
				comp_code     = '{$row['comp_code']}',
				od_supply_price	= '$it_supply_price',
				od_dodome_price	= '$it_dodome_price',
				od_sale_price	= '$it_sale_price',
				od_drug_price	= '$it_drug_price',
				od_incen		= '$it_incen',
				od_itm_supply_price	= '$io_supply_price',
				od_itm_dodome_price	= '$io_dodome_price',
				od_itm_sale_price	= '$io_sale_price',
				od_itm_drug_price	= '$io_drug_price',
				od_itm_incen		= '$io_incen',
				od_sum_supply_price	= '$od_sum_supply_price',
				od_sum_dodome_price	= '$od_sum_dodome_price',
				od_sum_sale_price	= '$od_sum_sale_price',
				od_sum_drug_price	= '$od_sum_drug_price',
				od_sum_incen		= '$od_sum_incen',
				od_send_cost    = '{$row['it_sc_cost']}',
				od_total_supply_price	= '$od_total_supply_price',
				od_total_dodome_price	= '$od_total_dodome_price',
				od_total_sale_price		= '$od_total_sale_price',
				od_total_drug_price		= '$od_total_drug_price',
				od_total_incen		= '$od_total_incen',
				od_period_yn =  '{$row['ct_period_yn']}',
				od_period_week =  '{$row['ct_period_week']}',
				od_period_cnt =  '{$row['ct_period_cnt']}'
				";
				echo $sql;

	sql_query($sql, false);
	/*************** 트랜잭션 관련 ****************/
	if( mysqli_errno($g5['connect_db']) ){
		$error_cnt += 1;
	}
	/*************** 트랜잭션 관련 ****************/

	$od_pay_yn = 'N';
	//일반주문
	$sql = " insert into tbl_shop_order_receiver 
				set od_id         = '$od_id',
				od_num			  = $od_num,
				od_seq			  = 1,
				od_b_name         = '$od_b_name',
				od_b_tel          = '$od_b_tel',
				od_b_hp           = '$od_b_hp',
				od_b_zip1         = '$od_b_zip1',
				od_b_zip2         = '$od_b_zip2',
				od_b_addr1        = '$od_b_addr1',
				od_b_addr2        = '$od_b_addr2',
				od_b_addr3        = '$od_b_addr3',
				od_b_addr_jibeon  = '$od_b_addr_jibeon',
				od_period_date	  = '',
				od_qty            = '$od_qty',					
				od_date1          = '".G5_TIME_YMDHIS."',
				od_pay_yn         = '$od_pay_yn',
				od_status         = '주문'";
			sql_query($sql, false);
			/*************** 트랜잭션 관련 ****************/
			if( mysqli_errno($g5['connect_db']) ){
				$error_cnt += 1;
			}
			/*************** 트랜잭션 관련 ****************/
	

}
//장바구니 상태변경
$sql = "update {$g5['g5_shop_cart_table']}
           set od_id = '$od_id',  ct_status = '주문'
         where ct_status = '사내'";
$result = sql_query($sql, false);
/*************** 트랜잭션 관련 ****************/
if( mysqli_errno($g5['connect_db']) ){
	$error_cnt += 1;
}
/*************** 트랜잭션 관련 ****************/

/*************** 트랜잭션 관련 ****************/
if ($error_cnt > 0) {
	mysqli_rollback($g5['connect_db']);
	mysqli_close($g5['connect_db']);
	echo "
	<script>
		alert('데이터베이스의 에러로 인해 롤백되었습니다.');
		history.back();
	</script>
	";
	exit;
} else {
	mysqli_commit($g5['connect_db']);
} 	
/*************** 트랜잭션 관련 ****************/

goto_url("./orderlistp3.php");


?>
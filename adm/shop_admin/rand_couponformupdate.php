<?php
$sub_menu = '400801';
include_once('./_common.php');

auth_check($auth[$sub_menu], "w");

check_admin_token();

$_POST = array_map('trim', $_POST);

if(!$_POST['cp_subject'])
    alert('쿠폰이름을 입력해 주십시오.');

if($_POST['cp_method'] == 0 && !$_POST['cp_target'])
    alert('적용상품을 입력해 주십시오.');

if($_POST['cp_method'] == 1 && !$_POST['cp_target'])
    alert('적용분류를 입력해 주십시오.');


if(!$_POST['cp_start'] || !$_POST['cp_end'])
    alert('사용 시작일과 종료일을 입력해 주십시오.');

if($_POST['cp_start'] > $_POST['cp_end'])
    alert('사용 시작일은 종료일 이전으로 입력해 주십시오.');

/*
if($_POST['cp_end'] < G5_TIME_YMD)
    alert('종료일은 오늘('.G5_TIME_YMD.')이후로 입력해 주십시오.');
*/
if(!$_POST['cp_price']) {
	//인증쿠폰이 아닐때.
	if( $_POST['cp_method'] != "4" ){
		if($_POST['cp_type'])
			alert('할인비율을 입력해 주십시오.');
		else
			alert('할인금액을 입력해 주십시오.');
	}
}
$cp_duplicate = $_POST['cp_duplicate'];

//인증쿠폰이 아닐때.
if( $_POST['cp_method'] != "4" ){
	if($_POST['cp_type'] && ($_POST['cp_price'] < 1 || $_POST['cp_price'] > 99))
		alert('할인비율을은 1과 99사이 값으로 입력해 주십시오.');
}

if($_POST['cp_method'] == 0) {
    //$sql = " select count(*) as cnt from {$g5['g5_shop_item_table']} where it_id = '$cp_target' and it_nocoupon = '0' ";
	$sql = " select count(*) as cnt from {$g5['g5_shop_item_table']} where instr('$cp_target', concat( concat('|',it_id) , '|' ) ) > 0 and it_nocoupon = '0' ";
    $row = sql_fetch($sql);
    if(!$row['cnt'])
        alert('입력하신 상품코드는 존재하지 않는 코드이거나 쿠폰적용안함으로 설정된 상품입니다.');
} else if($_POST['cp_method'] == 1) {
    //$sql = " select count(*) as cnt from {$g5['g5_shop_category_table']} where ca_id = '$cp_target' and ca_nocoupon = '0' ";
	$sql = " select count(*) as cnt from {$g5['g5_shop_category_table']} where instr('$cp_target', concat( concat('|',ca_id) , '|' ) ) > 0  and ca_nocoupon = '0' ";
    $row = sql_fetch($sql);
    if(!$row['cnt'])
        alert('입력하신 분류코드는 존재하지 않는 분류코드이거나 쿠폰적용안함으로 설정된 분류입니다.');
}

/*************** 트랜잭션 관련 ****************/
$error_cnt = 0;
mysqli_autocommit($g5['connect_db'], false);
/*************** 트랜잭션 관련 ****************/

if($w == '') {

    $sql = " INSERT INTO tbl_shop_coupon_rand
                ( cp_subject, cp_method, cp_target, cp_start, cp_end, cp_type, cp_price, cp_trunc, cp_minimum, cp_maximum, cp_datetime, cp_duplicate)
            VALUES
                ( '$cp_subject', '$cp_method', '$cp_target', '$cp_start', '$cp_end', '$cp_type', '$cp_price', '$cp_trunc', '$cp_minimum', '$cp_maximum', '".G5_TIME_YMDHIS."', '$cp_duplicate' ) ";


    sql_query($sql);
	/*************** 트랜잭션 관련 ****************/
	if( mysqli_errno($g5['connect_db']) ){
		$error_cnt += 1;
	}
	/*************** 트랜잭션 관련 ****************/

    /************* 관리자 로그 처리 START *************/
    $insert_id = sql_insert_id();
    insert_admin_log(400,400801, '쿠폰 추가', '', $insert_id, '', $_SERVER['REQUEST_URI'], $_POST);
    /************* 관리자 로그 처리 END *************/

} else if($w == 'u') {
    $sql = " select * from tbl_shop_coupon_rand where cp_no = '$cp_no' ";
    $cp = sql_fetch($sql);

    if(!$cp['cp_no'])
        alert('쿠폰정보가 존재하지 않습니다.', './rand_couponlist.php');

    $sql_b = " select * from tbl_shop_coupon_rand where cp_no = '$cp_no' ";
    $al_data_backup = sql_fetch($sql_b);

    $sql = " update tbl_shop_coupon_rand
                set cp_subject  = '$cp_subject',
                    cp_method   = '$cp_method',
                    cp_target   = '$cp_target',
                    cp_start    = '$cp_start',
                    cp_end      = '$cp_end',
                    cp_type     = '$cp_type',
                    cp_price    = '$cp_price',
                    cp_trunc    = '$cp_trunc',
                    cp_maximum  = '$cp_maximum',
                    cp_minimum  = '$cp_minimum',
                    cp_duplicate = '$cp_duplicate'
                where cp_no = '$cp_no' ";
    sql_query($sql);
	/*************** 트랜잭션 관련 ****************/
	if( mysqli_errno($g5['connect_db']) ){
		$error_cnt += 1;
	}
	/*************** 트랜잭션 관련 ****************/

    /************* 관리자 로그 처리 START *************/
    $insert_id = sql_insert_id();
    insert_admin_log(400,400801, '쿠폰 수정', '', $cp_no, '', $_SERVER['REQUEST_URI'], $_POST, $al_data_backup);
    /************* 관리자 로그 처리 END *************/

}


/*************** 트랜잭션 관련 ****************/
if ($error_cnt > 0) {
	mysqli_rollback($g5['connect_db']);
	mysqli_close($g5['connect_db']);
	echo "
	<script>
		alert('데이터베이스의 에러로 인해 롤백되었습니다.');
		history.back();
	</script>
	";
	exit;
} else {
	mysqli_commit($g5['connect_db']);
} 	
/*************** 트랜잭션 관련 ****************/

goto_url('./rand_couponlist.php');
?>
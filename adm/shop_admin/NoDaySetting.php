<?php

include_once('./_common.php');

$TopTitle = "정기결제 불가일자 설정";
$sub_menu = '400413';

auth_check($auth[$sub_menu], "r");

$g5['title'] = $TopTitle;
include_once (G5_ADMIN_PATH.'/admin.head.php');
include_once(G5_PLUGIN_PATH.'/jquery-ui/datepicker.php');


$sql = " select * from tbl_noday where del_yn = 'N' order by set_day asc ";

$result = sql_query($sql);

?>

<form name="frm" method="post">

<input type="hidden" name="idx" value=""/>
<input type="hidden" name="mode" value=""/>

<div class="tbl_head01 tbl_wrap">
    <table style="width:400px;">
    <thead>
    <tr>
        <td align="left">
			<div class="local_cmd01 ">
				결제불가일자
				<input type="text" id="set_day" name="set_day" style="width:80px;height:25px;" maxlength="10"/>
				<input type="hidden" id="clone_set_day" name="clone_set_day" style="width:80px;height:25px;" maxlength="10"/>
				<input type="button" value="달력" onclick="$('#clone_set_day').datepicker('show');" style="height:25px;"/>&nbsp;
				<input type="button" value="저장" class="btn_submit" onclick="form_submit('I','');" style="height:25px;">   
				(ex 2020-01-10)
			</div>
		</td>
    </tr>
    </thead>
    <tbody>
	</table>
	<br>
	*주말은 자동제외
    <table id="sodr_list" style="width:400px;">
    <caption>주문 내역 목록</caption>
    <thead>
    <tr>
        <th scope="col" id="th_ordnum">결제불가일자</th>
        <th scope="col">삭제</th>
    </tr>
    </thead>
    <tbody>
	<?
	for ($i=0; $row=sql_fetch_array($result); $i++){
	?>
	<tr>
		<td><?=$row['set_day']?></td>
		<td>
			<input type="button" value="삭제" onclick="form_submit('D','<?=$row['idx']?>');" style="height:25px;">   
		</td>
	</tr>
	<?
	}
	?>
	</tbody>
	</table>
</div>

</form>

<script>

function all_check(f)
{
    var chk = document.getElementsByName("trans_chk[]");

    for (i=0; i<chk.length; i++) {
		if (!chk[i].disabled){
			chk[i].checked = f.chkall.checked;
		}        
	}

}

function form_submit(GetMode,GetIdx){

	var f = document.frm;

	if( GetMode == "I" ){

		var datatimeRegexp = /^(19|20)\d{2}-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[0-1])$/;

		if ( !datatimeRegexp.test($('#set_day').val()) ) {
			alert("날짜는 yyyy-mm-dd 형식으로 입력해주세요.");
			return false;
		}

	}
	
	f.mode.value = GetMode;
	f.idx.value = GetIdx;
	f.action = "./NoDaySetting_Proc.php";
	f.submit();
	
}

</script>
<!-- (s)hidden Frame -->
<iframe name="iFrm" style="display: none;" ></iframe>
<!--업체선택 검색기능-->

<script>

$(function(){

    $("#clone_set_day").datepicker({ 
		changeMonth: true, 
		changeYear: true, 
		dateFormat: "yy-mm-dd", 
		showButtonPanel: true, 
		yearRange: "c-99:c+99", 
        onSelect:function(dateText, inst) {

			$("#set_day").val(dateText);

        }
	});
	
});

</script>

<?php
include_once (G5_ADMIN_PATH.'/admin.tail.php');
?>

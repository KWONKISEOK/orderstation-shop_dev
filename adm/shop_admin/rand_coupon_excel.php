<?php
$sub_menu = '400801';
include_once('./_common.php');

auth_check($auth[$sub_menu], "r");

//쿠폰 관리 비밀번호 치고 들어가기
if( empty(get_cookie("rand_coupon_manager")) ){

    require_once('./rand_coupon_login.php');
    return;

}

$sql = " select cp_subject from tbl_shop_coupon_rand where cp_no = {$cp_no} ";
$rs = sql_fetch($sql);

$ExcelTitle = urlencode( $rs["cp_subject"] ."_". date( "YmdHis" ) );

header( "Content-type: application/vnd.ms-excel" );
header( "Content-type: application/vnd.ms-excel; charset=utf-8" );
header( "Content-Disposition: attachment; filename = " . $ExcelTitle . ".xls" );
header( "Content-Description: PHP4 Generated Data" );
print( "<meta http-equiv=\"Content-Type\" content=\"application/vnd.ms-excel; charset=utf-8\">" );

$sql = " 
		 select A.* , B.mb_hp from tbl_shop_coupon_rand_txt AS A LEFT JOIN tbl_member AS B ON A.mb_id = B.mb_id 
		 where parent_cp_no = {$cp_no} 
	   ";
$result = sql_query($sql);

?>

<table border="1">
<tr bgcolor="F7F7F7">
	<th>쿠폰번호</th>
	<th>등록여부</th>
	<th>등록아이디</th>
	<th>핸드폰번호</th>
	<th>마이페이지 쿠폰등록일자</th>
</tr>
<? while($row = mysqli_fetch_array($result,MYSQL_ASSOC)) { ?>
<tr>
	<td align="left" style="mso-number-format:'\@'"><?=$row["cp_id"];?></td>
	<td align="left" style="mso-number-format:'\@'"><?=$row["use_yn"];?></td>
	<td align="left" style="mso-number-format:'\@'"><?=$row["mb_id"];?></td>
	<td align="left" style="mso-number-format:'\@'"><?=$row["mb_hp"];?></td>
	<td align="left" style="mso-number-format:'\@'"><?=$row["use_datetime"];?></td>
</tr>
<? } ?>
</table>
<?php
include_once('./_common.php');

$ptype	= $_POST['ptype'];

$sql  = " select a.od_id , e.od_id as first_od_id
	                 ,a.mb_id
					 ,a.od_name
					 ,a.pharm_name
					 ,a.pharm_manager
					 ,a.od_name
					 ,a.od_tel
					 ,a.od_hp
					 ,a.od_zip1
					 ,a.od_zip2
					 ,a.od_addr1
					 ,a.od_addr2
					 ,a.od_addr3
					 ,a.od_addr_jibeon					 
					 ,a.od_settle_case
					 ,a.pharm_custno
					 ,a.od_cart_price
					 ,a.od_gubun
					 ,a.od_mobile
					 ,a.od_memo
					 ,a.od_receipt_point
					 ,a.od_receipt_price
					 ,a.od_coupon
					 ,(select mb_referee from tbl_member where mb_id = a.mb_id) mb_referee
					 ,(select mb_name from tbl_member where mb_id= a.pharm_manager ) pharm_manager_name
					 ,a.pharm_manager_dept
					 ,(select mb_id from tbl_member where pharm_custno= a.pharm_custno and mb_type=1 and pharm_custno != '' limit 1) pharm_mb_id
					 ,(select mb_name from tbl_member where pharm_custno= a.pharm_custno  and mb_type=1 and pharm_custno != '' limit 1) pharm_mb_name
					 ,b.it_id
					 ,b.io_id
					 ,b.od_num
					 ,b.it_name
					 ,(select it_model from tbl_shop_item where it_id = b.it_id) it_model
					 ,b.od_send_cost
					 ,b.od_option
					 ,b.od_period_yn
					 ,b.od_period_cnt

					 ,b.od_sale_price
					 ,b.od_dodome_price
					 ,b.od_supply_price
					 ,b.od_drug_price
					 ,b.od_incen


					 ,b.od_itm_sale_price
					 ,b.od_itm_dodome_price
					 ,b.od_itm_supply_price
					 ,b.od_itm_drug_price
					 ,b.od_itm_incen

					
					 ,b.od_total_sale_price
					 ,b.od_total_dodome_price
					 ,b.od_total_supply_price
					 ,b.od_total_drug_price
					 ,b.od_total_incen			
					 
					 , case when a.od_multi_yn = 'Y' then g.od_b_name else c.od_b_name end as od_b_name
					 , case when a.od_multi_yn = 'Y' then g.od_b_tel else c.od_b_tel end as od_b_tel
					 , case when a.od_multi_yn = 'Y' then g.od_b_hp else c.od_b_hp end as od_b_hp
					 , case when a.od_multi_yn = 'Y' then g.od_b_zip1 else c.od_b_zip1 end as od_b_zip1
					 , case when a.od_multi_yn = 'Y' then g.od_b_zip2 else c.od_b_zip2 end as od_b_zip2
					 , case when a.od_multi_yn = 'Y' then g.od_b_addr1 else c.od_b_addr1 end as od_b_addr1
					 , case when a.od_multi_yn = 'Y' then g.od_b_addr2 else c.od_b_addr2 end as od_b_addr2
					 , case when a.od_multi_yn = 'Y' then g.od_b_addr3 else c.od_b_addr3 end as od_b_addr3
					 , case when a.od_multi_yn = 'Y' then g.od_b_addr_jibeon else c.od_b_addr_jibeon end as od_b_addr_jibeon
					 , case when a.od_multi_yn = 'Y' then g.od_delivery_company else c.od_delivery_company end as od_delivery_company
					 , case when a.od_multi_yn = 'Y' then g.od_invoice else c.od_invoice end as od_invoice
					 , case when a.od_multi_yn = 'Y' then g.od_trans_memo else c.od_trans_memo end as od_trans_memo

					 ,c.od_seq
					 ,c.od_date1
					 ,c.od_date2
					 ,c.od_date3
					 ,c.od_date4
					 , case when a.od_multi_yn = 'Y' then g.od_qty else c.od_qty end as od_qty
					 ,c.od_period_date
					 ,c.od_status
					 ,c.od_pay_yn
					 ,c.od_cancel_date
					
					 ,d.comp_name
					 ,e.mb_type
					 ,(select ca_id from tbl_shop_item where it_id = b.it_id) ca_id
					 ,case when h.mb_type = 1 then 
					 	   h.pharm_div
					  else 
						   ( select pharm_div from tbl_member where mb_id = h.mb_recommend limit 1 ) 
					  end as pharm_div	
					 ,a.service_type
					 ,case when a.od_multi_yn = 'Y' then 
					 		 ( SELECT COUNT(od_id) from tbl_shop_order_multi_receiver WHERE od_id = a.od_id AND od_num = b.od_num )	
					  ELSE 0 END AS row_cnt
					 ,a.od_multi_yn
					 ,CASE @vjob WHEN a.od_id THEN @rownum:=@rownum+1 ELSE @rownum:=1 END as ranking
					 ,@vjob:=a.od_id as vjob
				 from tbl_shop_order a inner join tbl_shop_order_detail b on a.od_id = b.od_id 
						  inner join tbl_shop_order_receiver c on b.od_id = c.od_id and b.od_num = c.od_num
						  left join tbl_shop_order_multi_receiver g on b.od_id = g.od_id and b.od_num = g.od_num
						  left join tbl_member d on b.comp_code = d.comp_code
						  left join(
		 					 select b.mb_id , min(a.od_id) AS od_id , max(b.mb_type) as mb_type
							 from tbl_shop_order a inner join tbl_member b ON a.mb_id = b.mb_id AND a.od_status != '취소'
							 WHERE od_receipt_time != '0000-00-00 00:00:00' and mb_datetime <= od_receipt_time 
							 GROUP BY b.mb_id							  
						  ) e ON a.mb_id = e.mb_id AND a.od_id = e.od_id 
						  left join tbl_member h ON a.mb_id = h.mb_id , ( SELECT @vjob:='', @rownum:=0 ) as Z
				 where 1=1 ";
	if($ptype == '2') {
		$sql .= " and c.od_status != '취소'";
	}

	switch($od_date) {
		case '1':
			$sql .= " and  c.od_date1 between '$fr_date 00:00:00' and '$to_date 23:59:59' ";
			break;
		case '2':
			$sql .= " and  c.od_date2 between '$fr_date 00:00:00' and '$to_date 23:59:59' ";
			break;
		case '3':
			$sql .= " and  c.od_date3 between '$fr_date 00:00:00' and '$to_date 23:59:59' ";
			break;
		case '4':
			$sql .= " and c.od_date4 between '$fr_date 00:00:00' and '$to_date 23:59:59' ";
			break;
        case '10':
            $sql .= " and c.od_cancel_date between '$fr_date 00:00:00' and '$to_date 23:59:59' ";
            break;
		default:
			$sql .= " and  c.od_date1 between '$fr_date 00:00:00' and '$to_date 23:59:59' ";
			break;
	}
	//주문구분
	if ($od_gubun) {
		$sql .= " and a.od_gubun = '$od_gubun' ";
	}	
	//결제구분
	if ($od_settle_case) {
		 switch($od_settle_case) {
			case '빌링결제':
				$sql .= " and a.od_billkey != '' ";
				break;
			default:
				$sql .= " and a.od_settle_case = '$od_settle_case' ";
				break;
		}
	}
	//서비스명
	if($service_gubun){
		switch($service_gubun) {
			case 'O':
				$sql .= " and c.service_type is null ";
				break;
			default:
				$sql .= " and c.service_type = 'B' ";
				break;
		}
	}
	//상태
	if ($od_status) {
		if($od_status  == "취소외"){
			$sql .= " and c.od_status != '취소' and c.od_status != '반품접수' and c.od_status != '반품승인' and c.od_status != '반품반려' and c.od_status != '반품완료'";
		
		}else{
		$sql .= " and c.od_status = '$od_status' ";
		}
	}	
	//입금여부
	if ($od_pay_yn) {
		$sql .= " and c.od_pay_yn = '$od_pay_yn' ";
	}
	//공급사코드
	if ($comp_code) {
		$sql .= " and b.comp_code = '$comp_code' ";
	}
   //공급사명
	if ($comp_name) {
		$sql .= " and d.comp_name = '$comp_code' ";
	}
	//공급사명
    if ($sel_field == 'comp_name') {
        $sql .= " and d.$sel_field like '%$search%' ";
    } //상품명
    else if ($sel_field == 'it_name') {
        $sql .= " and $sel_field like '%$search%' ";
    } //상품코드
    else if ($sel_field == 'it_id') {
        $sql .= " and b.$sel_field like '%$search%' ";
    } // 추가
    else {
        $sql .= " and a.$sel_field like '%$search%' ";
    }
	//toptap=2 -> 달콩행복set제외목록
	if( $toptap == "2" ){
		$sql .= " and b.it_id not in(
			'S00603',
			'S00604'
		) ";
	}
	//toptap=3 -> 달콩행복set목록
	if( $toptap == "3" ){
		$sql .= " and b.it_id in(
			'S00603',
			'S00604',
			'S00487',
			'S00489',
			'S00491',
			'S00488',
			'S00490',
			'S00492',
			'S00493',
			'S00487',
			'S00489',
			'S00491',
			'S00488',
			'S00490',
			'S00492',
			'S00493'	
		) ";
	}

    switch ($order_sort) {
        case 'order':
            $order_by = 'a.od_id';
            break;
        case 'cancel':
            $order_by = 'c.od_cancel_date';
            break;
        default:
            $order_by = 'a.od_id';
            break;
    }

	$sql .= " order by $order_by desc, c.od_num, c.od_seq ";
	//echo $sql;
	//exit;

    $result = sql_query($sql);

switch($ptype){	
	case '1':
		$ExcelTitle = urlencode( "주문내역_" . date( "YmdHis" ) );
		header( "Content-type: application/vnd.ms-excel" );
		header( "Content-type: application/vnd.ms-excel; charset=utf-8" );
		header( "Content-Disposition: attachment; filename = " . $ExcelTitle . ".xls" );
		header( "Content-Description: PHP4 Generated Data" );
		print( "<meta http-equiv=\"Content-Type\" content=\"application/vnd.ms-excel; charset=utf-8\">" );
		?>
		
		<table border="1">
		<tr bgcolor="F7F7F7" height="30">
			<th colspan="4">주문정보</th>
			<th colspan="6">약국정보</th>
			<th colspan="7">주문자 정보</th>
			<th colspan="7">받는사람 정보</th>
			<th colspan="24">주문상품</th>
			<th colspan="2">정기배송</th>
			<th rowspan="2">처리상태</th>
			<th colspan="3">결제정보</th>
			<th rowspan="2">입금여부</th>
			<th colspan="3">배송정보</th>
		</tr>
		<tr bgcolor="F7F7F7">
			<th>서비스명</th>
			<th>주문일시</th>
            <th>주문취소일</th>
			<th>주문구분</th>
			<th>주문번호</th>
			<th>주문기기</th>
			<th>담당자명</th>
			<th>회사</th>
			<th>약국명</th>
			<th>약국코드</th>
			<th>아이디</th>
			<th>약사명</th>
			<th>고객명</th>
			<th>고객아이디</th>
			<th>전화번호</th>
			<th>핸드폰</th>
			<th>우편번호</th>
			<th>주소</th>
			<th>고객구분</th>
			<th>이름</th>
			<th>전화번호</th>
			<th>핸드폰</th>
			<th>우편번호</th>
			<th>주소</th>
			<th>배송주의사항</th>
			<th>선물메세지</th>
			<th>업체명</th>
			<th>카테고리</th>
			<th>상품명</th>
			<th>규격</th>
			<th>옵션명</th>
			<th>상품코드</th>
			<th>소비자가</th>
			<th>도도매가</th>
			<th>공급가</th>
			<th>약국판매가</th>
			<th>수수료</th>
			<th>옵션(소비자가)</th>
			<th>옵션(도도매가)</th>
			<th>옵션(공급가)</th>
			<th>옵션(약국판매가)</th>
			<th>옵션(수수료)</th>
			<th>수량</th>
			<th>소비자합</th>
			<th>도도매합계</th>
			<th>약국판매가합</th>
			<th>수수료합</th>
			<th>공급가합</th>
			<th>합계</th>
			<th>배송비</th>
			<th>일자</th>
			<th>회차</th>
			<th>포인트</th>
			<th>쿠폰</th>
			<th>결제금액</th>
			<th>배송회사</th>
			<th>송장번호</th>
			<th>배송일시</th>
		</tr>
		<?php
	
		$res = sql_query($sql);
		if(!$res) exit('Cannot run query.');

		$i = 0;
		while($row = sql_fetch_array($res)) {
			$od_gubun = '';
			switch($row['od_gubun']) {
				case '1':
					$od_gubun = '고객주문';
					break;
				case '2':
					$od_gubun = '약국주문';
					break;
				case '3':
					$od_gubun = '일반주문';
					break;
			}
			$pharm_manager_dept = '';
			/*
			switch ( substr($row["pharm_manager_dept"],0,6) ) {
				case "002001" :
					$pharm_manager_dept = "태전약품";
					break;
				case "002002" :
					$pharm_manager_dept = "티제이팜";
					break;
			}
			*/

			if( $row["pharm_div"] == "1" ){
				$pharm_manager_dept = "태전약품";			
			}else if( $row["pharm_div"] == "2" ){ 
				$pharm_manager_dept = "티제이팜";			
			}
			$mb_referee_str = '';
			switch ( $row[mb_referee] ) {
				case "0" :
					$mb_referee_str = "일반회원";
					break;
				case "1" :
					$mb_referee_str = "OS추천회원";
					break;
				case "2" :
					$mb_referee_str = "모니터링회원";
					break;
				case "3" :
					$mb_referee_str = "OS서팩2회원";
					break;
				case "4" :
					$mb_referee_str = "OS직원";
					break;
				case "5" :
					$mb_referee_str = "얼라이언스약국";
					break;
				
			}
			$ca_str = substr($row[ca_id], 0, 2); 
			switch ( $ca_str ) {
				case "10" :
					$ca_str = "푸드매니지";
					break;
				case "20" :
					$ca_str = "홈매니지";
					break;
				case "30" :
					$ca_str = "라이프매니지";
					break;
				case "40" :
					$ca_str = "뷰티매니지";
					break;
				case "50" :
					$ca_str = "오피스매니지";
					break;
				case "60" :
					$ca_str = "OS브랜드관";
					break;
				case "70" :
					$ca_str = "반려동물";
					break;
			}
			
			switch ($row['od_pay_yn'] ) {
			case "N" :
				$od_pay_yn = "미입금";
				break;
			case "Y" :
				$od_pay_yn = "입금";
				break;
			}
			
			//영업사원주문시 표현을 OS직원으로 표현해주기
			if( $row[mb_type] == 5 ){
				$mb_referee_str = "OS직원";
			}
			$it_code = $row["it_id"];
			if($row["io_id"] != '') $it_code = $row["io_id"];
			
			$od_mobile = '';
			if($row[od_mobile] == 1){
				$od_mobile = "모바일";
			}else{
				$od_mobile = "PC";
			}

			$od_seq = 0;
			if($row["od_period_yn"] == 'Y') { 
				$od_seq = $row["od_seq"];

				$od_total_sale_price	= $row["od_total_sale_price"] / $row["od_period_cnt"];
				$od_total_dodome_price	= $row["od_total_dodome_price"] / $row["od_period_cnt"];			
				$od_total_drug_price	= $row["od_total_drug_price"] / $row["od_period_cnt"] ;
				$od_total_incen			= $row["od_total_incen"] / $row["od_period_cnt"];
				$od_total_supply_price	= $row["od_total_supply_price"] / $row["od_period_cnt"] ;


			} else {
				$od_total_sale_price	= $row["od_total_sale_price"];
				$od_total_dodome_price	= $row["od_total_dodome_price"];			
				$od_total_drug_price	= $row["od_total_drug_price"] ;
				$od_total_incen			= $row["od_total_incen"] ;
				$od_total_supply_price	= $row["od_total_supply_price"] ;
			}

			$od_payment = 0;
			if($od_gubun == 3) {
				$od_payment = $row["od_receipt_price"];
			}

			if( $row["od_id"] == $row["first_od_id"] ){
				$line_bgcolor = " bgcolor='#ccff99' ";
			}else{
				$line_bgcolor = "";
			}


			$row_string = "";
			$line_view = "N";

			//다중배송상품일때 배송지 row 카운트해주기
			if( $row["od_multi_yn"] == "Y" && $row["ranking"] == "1" && $row["row_cnt"] > 0 ){
				$row_string = " rowspan='".$row["row_cnt"]."' ";
				$line_view = "Y";
			}
			if( $row["row_cnt"] == 0 ){
				$row_string = "";
				$line_view = "Y";			
			}
		
			echo "<tr ".$line_bgcolor.">";

			//rowspan 있을때 처리
			if( $line_view == "Y" ){

			echo "<td style=\"mso-number-format:'\@';\" ".$row_string.">" . TransServiceName($row["service_type"]) . " </td>";
			echo "<td style=\"mso-number-format:'\@';\" ".$row_string.">" . $row["od_date1"] . " </td>";
            echo "<td style=\"mso-number-format:'\@';\" ".$row_string.">" . $row["od_cancel_date"] . " </td>";
			echo "<td style=\"mso-number-format:'\@';\" ".$row_string.">" . $od_gubun . " </td>";		
			echo "<td style=\"mso-number-format:'\@';\" ".$row_string.">" . $row["od_id"] . " </td>";
			echo "<td style=\"mso-number-format:'\@';\" ".$row_string.">" . $od_mobile . " </td>";
			echo "<td style=\"mso-number-format:'\@';\" ".$row_string.">" . $row['pharm_manager_name'] . " </td>";
			echo "<td style=\"mso-number-format:'\@';\" ".$row_string.">" . $pharm_manager_dept . "</td>";
			echo "<td style=\"mso-number-format:'\@';\" ".$row_string.">" . $row["pharm_name"] . " </td>";
			echo "<td style=\"mso-number-format:'\@';\" ".$row_string.">" . $row["pharm_custno"] . " </td>";
			echo "<td style=\"mso-number-format:'\@';\" ".$row_string.">" . $row["pharm_mb_id"] . " </td>";
			echo "<td style=\"mso-number-format:'\@';\" ".$row_string.">" . $row["pharm_mb_name"] . " </td>";

			echo "<td style=\"mso-number-format:'\@';\" ".$row_string.">" . $row["od_name"] . " </td>";
			echo "<td style=\"mso-number-format:'\@';\" ".$row_string.">" . $row["mb_id"] . " </td>";
			echo "<td style=\"mso-number-format:'\@';\" ".$row_string.">" . $row["od_tel"] . " </td>";
			echo "<td style=\"mso-number-format:'\@';\" ".$row_string.">" . $row["od_hp"] . " </td>";
			echo "<td style=\"mso-number-format:'\@';\" ".$row_string.">" . $row["od_zip1"] .  $row["od_zip2"]." </td>";
			echo "<td style=\"mso-number-format:'\@';\" ".$row_string.">" . print_address($row['od_addr1'], $row['od_addr2'], $row['od_addr3'], $row['od_addr_jibeon']) . " </td>";
			echo "<td style=\"mso-number-format:'\@';\" ".$row_string.">" . $mb_referee_str . " </td>";

			}

			echo "<td style=\"mso-number-format:'\@';\">" . $row["od_b_name"] . " </td>";
			echo "<td style=\"mso-number-format:'\@';\">" . $row["od_b_tel"] . " </td>";
			echo "<td style=\"mso-number-format:'\@';\">" . $row["od_b_hp"] . " </td>";
			echo "<td style=\"mso-number-format:'\@';\">" . $row["od_b_zip1"] .  $row["od_b_zip2"]." </td>";
			echo "<td style=\"mso-number-format:'\@';\">" . print_address($row['od_b_addr1'], $row['od_b_addr2'], $row['od_b_addr3'], $row['od_b_addr_jibeon']) . " </td>";
			echo "<td style=\"mso-number-format:'\@';\">" . $row["od_trans_memo"] . " </td>";

			//rowspan 있을때 처리
			if( $line_view == "Y" ){

			echo "<td style=\"mso-number-format:'\@';\" ".$row_string.">" . $row["od_memo"] . " </td>";
			echo "<td style=\"mso-number-format:'\@';\" ".$row_string.">" . $row["comp_name"] . " </td>";
			echo "<td style=\"mso-number-format:'\@';\" ".$row_string.">" . $ca_str . " </td>";
			echo "<td style=\"mso-number-format:'\@';\" ".$row_string.">" . $row["it_name"] . " </td>";
			echo "<td style=\"mso-number-format:'\@';\" ".$row_string.">" . $row["it_model"] . " </td>";
			echo "<td style=\"mso-number-format:'\@';\" ".$row_string.">" . $row["od_option"] . " </td>";
			echo "<td style=\"mso-number-format:'\@';\" ".$row_string.">" . $it_code . " </td>";
			echo "<td style=\"mso-number-format:'\@';\" ".$row_string.">" . $row["od_sale_price"] . " </td>";
			echo "<td style=\"mso-number-format:'\@';\" ".$row_string.">" . $row["od_dodome_price"] . " </td>";
			echo "<td style=\"mso-number-format:'\@';\" ".$row_string.">" . $row["od_supply_price"] . " </td>";
			echo "<td style=\"mso-number-format:'\@';\" ".$row_string.">" . $row["od_drug_price"] . " </td>";
			echo "<td style=\"mso-number-format:'\@';\" ".$row_string.">" . $row["od_incen"] . " </td>";
			echo "<td style=\"mso-number-format:'\@';\" ".$row_string.">" . $row["od_itm_sale_price"] . " </td>";
			echo "<td style=\"mso-number-format:'\@';\" ".$row_string.">" . $row["od_itm_dodome_price"] . " </td>";
			echo "<td style=\"mso-number-format:'\@';\" ".$row_string.">" . $row["od_itm_supply_price"] . " </td>";
			echo "<td style=\"mso-number-format:'\@';\" ".$row_string.">" . $row["od_itm_drug_price"] . " </td>";
			echo "<td style=\"mso-number-format:'\@';\" ".$row_string.">" . $row["od_itm_incen"] . " </td>";

			}

			echo "<td style=\"mso-number-format:'\@';\">" . $row["od_qty"] . " </td>";

			//rowspan 있을때 처리
			if( $line_view == "Y" ){

			echo "<td style=\"mso-number-format:'\@';\" ".$row_string.">" . $od_total_sale_price . " </td>";
			echo "<td style=\"mso-number-format:'\@';\" ".$row_string.">" . $od_total_dodome_price . " </td>";			
			echo "<td style=\"mso-number-format:'\@';\" ".$row_string.">" . $od_total_drug_price . " </td>";
			echo "<td style=\"mso-number-format:'\@';\" ".$row_string.">" . $od_total_incen . " </td>";
			echo "<td style=\"mso-number-format:'\@';\" ".$row_string.">" . $od_total_supply_price . " </td>";
			if($row["service_type"] == 'B' && $row["pharm_name"] == ''){ //합계:이즈브레 금액 구분용
				echo "<td style=\"mso-number-format:'\@';\" ".$row_string.">" . $od_total_sale_price . " </td>";
			}else{
				echo "<td style=\"mso-number-format:'\@';\" ".$row_string.">" . $od_total_drug_price . " </td>";
			}
			echo "<td style=\"mso-number-format:'\@';\" ".$row_string.">" . $row["od_send_cost"] . " </td>";
			echo "<td style=\"mso-number-format:'\@';\" ".$row_string.">" . $row["od_period_date"] . " </td>";
			echo "<td style=\"mso-number-format:'\@';\" ".$row_string.">" . $od_seq . " </td>";
			echo "<td style=\"mso-number-format:'\@';\" ".$row_string.">" . $row["od_status"] . " </td>";

			echo "<td style=\"mso-number-format:'\@';\" ".$row_string.">" . $row["od_receipt_point"] . " </td>";
			echo "<td style=\"mso-number-format:'\@';\" ".$row_string.">" . $row["od_coupon"] . " </td>";
			echo "<td style=\"mso-number-format:'\@';\" ".$row_string.">" . $od_pay_yn . " </td>";
			echo "<td style=\"mso-number-format:'\@';\" ".$row_string.">" . $od_payment . " </td>";

			}

			echo "<td style=\"mso-number-format:'\@';\">" . $row["od_delivery_company"] . " </td>";
			echo "<td style=\"mso-number-format:'\@';\">" . $row["od_invoice"] . " </td>";

			//rowspan 있을때 처리
			if( $line_view == "Y" ){

				echo "<td style=\"mso-number-format:'\@';\" ".$row_string.">" . $row["od_date3"] . " </td>";

			}

			echo "</tr>";

			$i++;
		} 
		break;
	case '2':
		$ExcelTitle = urlencode( "주문내역_송장발행_" . date( "YmdHis" ) );
		header( "Content-type: application/vnd.ms-excel" );
		header( "Content-type: application/vnd.ms-excel; charset=utf-8" );
		header( "Content-Disposition: attachment; filename = " . $ExcelTitle . ".xls" );
		header( "Content-Description: PHP4 Generated Data" );
		print( "<meta http-equiv=\"Content-Type\" content=\"application/vnd.ms-excel; charset=utf-8\">" );
		?>
		<table border="1">
		<tr bgcolor="F7F7F7">
			<th>성명</th>
			<th>우편번호</th>
			<th>주소</th>
			<th>전화번호</th>
			<th>주문수량</th>			
			<th>옵션명</th>
			<th>정기배송일</th>
			<th>공급사명</th>
			<th>배송메시지</th>			
		</tr>
		<?php
	
		$res = sql_query($sql);
		if(!$res) exit('Cannot run query.');

		$i = 0;
		while($row = sql_fetch_array($res)) {
			$od_gubun = '';
			switch($row['od_gubun']) {
				case '1':
					$od_gubun = '고객주문';
					break;
				case '2':
					$od_gubun = '약국주문';
					break;
				case '3':
					$od_gubun = '일반주문';
					break;
			}
	
			$it_code = $row["it_id"];
			if($row["io_id"] != '') $it_code = $row["io_id"];


			$goods_name = $row["it_name"];
			if($row["io_id"] != '') $goods_name = $goods_name.' '.$row['od_option'];

			$row_string = "";
			$line_view = "N";

			//다중배송상품일때 배송지 row 카운트해주기
			if( $row["od_multi_yn"] == "Y" && $row["ranking"] == "1" && $row["row_cnt"] > 0 ){
				$row_string = " rowspan='".$row["row_cnt"]."' ";
				$line_view = "Y";
			}
			if( $row["row_cnt"] == 0 ){
				$row_string = "";
				$line_view = "Y";			
			}
		
			echo "<tr>";
			echo "<td style=\"mso-number-format:'\@';\">" . $row["od_b_name"] . " </td>";
			echo "<td style=\"mso-number-format:'\@';\">" . $row["od_b_zip1"] .  $row["od_b_zip2"]." </td>";
			echo "<td style=\"mso-number-format:'\@';\">" . print_address($row['od_b_addr1'], $row['od_b_addr2'], $row['od_b_addr3'], $row['od_b_addr_jibeon']) . " </td>";
			echo "<td style=\"mso-number-format:'\@';\">" . $row["od_b_hp"] . " </td>";		
			echo "<td style=\"mso-number-format:'\@';\">" . $row["od_qty"] . " </td>";

			//rowspan 있을때 처리
			if( $line_view == "Y" ){

			echo "<td style=\"mso-number-format:'\@';\" ".$row_string.">" . $goods_name . " </td>";
			echo "<td style=\"mso-number-format:'\@';\" ".$row_string.">" . $row["od_period_date"] . " </td>";
			echo "<td style=\"mso-number-format:'\@';\" ".$row_string.">" . $row["comp_name"] . " </td>";

			}

			echo "<td style=\"mso-number-format:'\@';\">" . $row["od_trans_memo"] . " </td>";			
			echo "</tr>";

			$i++;
		} 
		break;
	
}



	

?>
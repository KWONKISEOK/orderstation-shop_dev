<?php
include_once('./_common.php');

$mode	= $_POST['mode'];

$where = " where ";
$sql_search = "";
if ($stx != "") {
    if ($sfl != "") {
        $sql_search .= " $where $sfl like '$stx%' ";
        $where = " and ";
    }
    if ($save_stx && ($save_stx != $stx))
        $page = 1;
}

$sql_common = " from {$g5['g5_shop_category_table']} ";
if ($is_admin != 'super')
    $sql_common .= " $where ca_mb_id = '{$member['mb_id']}' ";
$sql_common .= $sql_search;


// 테이블의 전체 레코드수만 얻음
$sql = " select count(ca_id) as cnt " . $sql_common;
$row = sql_fetch($sql);
$total_count = $row['cnt'];


if (!$sst)
{
    $sst  = "ca_id";
    $sod = "asc";
}
$sql_order = "order by $sst $sod";

// 출력할 레코드를 얻음
$sql  = " select 
				ca_id , 
				ca_name , 
				ca_skin_dir , 
				ca_mobile_skin_dir , 
				ca_use , 
				ca_web_view , 
				ca_app_view
             $sql_common
             $sql_order";

$result = sql_query($sql);

switch($mode){
	case 'excel':
		$ExcelTitle = urlencode( "분류관리_" . date( "YmdHis" ) );
		
		header( "Content-type: application/vnd.ms-excel" );
		header( "Content-type: application/vnd.ms-excel; charset=utf-8" );
		header( "Content-Disposition: attachment; filename = " . $ExcelTitle . ".xls" );
		header( "Content-Description: PHP4 Generated Data" );
		print( "<meta http-equiv=\"Content-Type\" content=\"application/vnd.ms-excel; charset=utf-8\">" ); 	
		
		?>
		<table border="1">
		<tr bgcolor="F7F7F7" height="30">
			<th >분류코드</th>
			<th >분류명</th>
			<th>상품수</th>
			<th>판매가능</th>
			<th>약사회원노출</th>
			<th>일반회원노출</th>
		</tr>
		
		<?php		
		
		$res = sql_query($sql);
		if(!$res) exit('Cannot run query.');

		$i = 0;
		while($row = mysqli_fetch_array($res,MYSQL_ASSOC)) {
		$sql1 = "
		SELECT SUM(cnt) AS cnt FROM(
			select COUNT(it_id) as cnt from {$g5['g5_shop_item_table']} where ca_id = '{$row['ca_id']}'
			UNION all
			select COUNT(it_id) as cnt from {$g5['g5_shop_item_table']} where ca_id != '{$row['ca_id']}' and ca_id2 = '{$row['ca_id']}' AND ca_id3 != '{$row['ca_id']}'
			UNION all
			select COUNT(it_id) as cnt from {$g5['g5_shop_item_table']} where ca_id != '{$row['ca_id']}' and ca_id2 != '{$row['ca_id']}' AND ca_id3 = '{$row['ca_id']}'
		) AS X;		
		";
        $row1 = sql_fetch($sql1);
			$ca_use	 = $row["ca_use"];
			if( $row["ca_use"] == 1 ){
				$ca_use = "가능";
			}else if($row["ca_use"] != 1){
				$ca_use = "불가능";
			}
			if( $row["ca_web_view"] == 1 ){
				$ca_web_view = "노출";
			}else if($row["ca_web_view"] != 1){
				$ca_web_veiw = "비노출";
			}
			if( $row["ca_app_view"] == 1 ){
				$ca_app_view = "노출";
			}else if($row["ca_app_view"] != 1){
				$ca_app_view = "비노출";
			}
			echo "<tr>";
			echo "<td style=\"mso-number-format:'\@';\">" . $row["ca_id"] . " </td>";
			echo "<td style=\"mso-number-format:'\@';\">" . $row["ca_name"] . " </td>";
			echo "<td style=\"mso-number-format:'\@';\">" . $row1["cnt"] . " </td>";			
			echo "<td style=\"mso-number-format:'\@';\">" . $ca_use . " </td>";	
			echo "<td style=\"mso-number-format:'\@';\">" . $ca_web_view . " </td>";
			echo "<td style=\"mso-number-format:'\@';\">" . $ca_app_view . " </td>";
			echo "</tr>";

			$i++;
			?>
			
				
			</tr>
		<?php	
		} 

		break;
	
}

?>
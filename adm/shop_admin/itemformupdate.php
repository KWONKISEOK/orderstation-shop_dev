<?php
$sub_menu = '600300';
include_once('./_common.php');
$prd_status = $_REQUEST["prd_status"];
$prd_status_change = $_REQUEST["prd_status_change"];
$prd_status_sold = $_REQUEST["prd_status_sold"];
$prd_status_change_sold = $_REQUEST["prd_status_change_sold"];
$qty_count = $_REQUEST["qty_count"];
$qty_tr = $_REQUEST["qty_tr"];
$qty_old = $_REQUEST["qty_old"];
$prd_status_no_reason_1=$_REQUEST["prd_status_no_reason_1"];
$prd_status_no_reason_date=$_REQUEST["prd_status_no_reason_date"];

$qaconfig = get_qa_config();

if ($w == "u" || $w == "d")
    check_demo();

if ($w == '' || $w == 'u')
    auth_check($auth[$sub_menu], "w");
else if ($w == 'd')
    auth_check($auth[$sub_menu], "d");

check_admin_token();

@mkdir(G5_DATA_PATH."/item", G5_DIR_PERMISSION);
@chmod(G5_DATA_PATH."/item", G5_DIR_PERMISSION);

@mkdir(G5_DATA_PATH."/option", G5_DIR_PERMISSION);
@chmod(G5_DATA_PATH."/option", G5_DIR_PERMISSION);

// input vars 체크
check_input_vars();

// 파일정보
if($w == "u") {
    $sql = " select it_img1, it_img2, it_img3, it_img4, it_img5, it_img6, it_img7, it_img8, it_img9, it_img10 , it_box_img
                from {$g5['g5_shop_item_table']}
                where it_id = '$it_id' ";
    $file = sql_fetch($sql);

    $it_img1    = $file['it_img1'];
    $it_img2    = $file['it_img2'];
    $it_img3    = $file['it_img3'];
    $it_img4    = $file['it_img4'];
    $it_img5    = $file['it_img5'];
    $it_img6    = $file['it_img6'];
    $it_img7    = $file['it_img7'];
    $it_img8    = $file['it_img8'];
    $it_img9    = $file['it_img9'];
    $it_img10   = $file['it_img10'];
	$it_box_img = $file['it_box_img'];
}

$it_img_dir = G5_DATA_PATH.'/item';

// 파일삭제
if ($it_img1_del) {
    $file_img1 = $it_img_dir.'/'.$it_img1;
    @unlink($file_img1);
    delete_item_thumbnail(dirname($file_img1), basename($file_img1));
    $it_img1 = '';
}
if ($it_img2_del) {
    $file_img2 = $it_img_dir.'/'.$it_img2;
    @unlink($file_img2);
    delete_item_thumbnail(dirname($file_img2), basename($file_img2));
    $it_img2 = '';
}
if ($it_img3_del) {
    $file_img3 = $it_img_dir.'/'.$it_img3;
    @unlink($file_img3);
    delete_item_thumbnail(dirname($file_img3), basename($file_img3));
    $it_img3 = '';
}
if ($it_img4_del) {
    $file_img4 = $it_img_dir.'/'.$it_img4;
    @unlink($file_img4);
    delete_item_thumbnail(dirname($file_img4), basename($file_img4));
    $it_img4 = '';
}
if ($it_img5_del) {
    $file_img5 = $it_img_dir.'/'.$it_img5;
    @unlink($file_img5);
    delete_item_thumbnail(dirname($file_img5), basename($file_img5));
    $it_img5 = '';
}
if ($it_img6_del) {
    $file_img6 = $it_img_dir.'/'.$it_img6;
    @unlink($file_img6);
    delete_item_thumbnail(dirname($file_img6), basename($file_img6));
    $it_img6 = '';
}
if ($it_img7_del) {
    $file_img7 = $it_img_dir.'/'.$it_img7;
    @unlink($file_img7);
    delete_item_thumbnail(dirname($file_img7), basename($file_img7));
    $it_img7 = '';
}
if ($it_img8_del) {
    $file_img8 = $it_img_dir.'/'.$it_img8;
    @unlink($file_img8);
    delete_item_thumbnail(dirname($file_img8), basename($file_img8));
    $it_img8 = '';
}
if ($it_img9_del) {
    $file_img9 = $it_img_dir.'/'.$it_img9;
    @unlink($file_img9);
    delete_item_thumbnail(dirname($file_img9), basename($file_img9));
    $it_img9 = '';
}
if ($it_img10_del) {
    $file_img10 = $it_img_dir.'/'.$it_img10;
    @unlink($file_img10);
    delete_item_thumbnail(dirname($file_img10), basename($file_img10));
    $it_img10 = '';
}
/* 단독상품 상세설명 이미지 */
if ($box_img_del) {
    $it_box_img = $it_img_dir.'/'.$it_box_img;
    @unlink($it_box_img);
    delete_item_thumbnail(dirname($it_box_img), basename($it_box_img));
    $it_box_img = '';
}

// 이미지업로드
if ($_FILES['it_img1']['name']) {
    if($w == 'u' && $it_img1) {
        $file_img1 = $it_img_dir.'/'.$it_img1;
        @unlink($file_img1);
        delete_item_thumbnail(dirname($file_img1), basename($file_img1));
    }
    $it_img1 = it_img_upload($_FILES['it_img1']['tmp_name'], $_FILES['it_img1']['name'], $it_img_dir.'/'.$it_id);
}
if ($_FILES['it_img2']['name']) {
    if($w == 'u' && $it_img2) {
        $file_img2 = $it_img_dir.'/'.$it_img2;
        @unlink($file_img2);
        delete_item_thumbnail(dirname($file_img2), basename($file_img2));
    }
    $it_img2 = it_img_upload($_FILES['it_img2']['tmp_name'], $_FILES['it_img2']['name'], $it_img_dir.'/'.$it_id);
}
if ($_FILES['it_img3']['name']) {
    if($w == 'u' && $it_img3) {
        $file_img3 = $it_img_dir.'/'.$it_img3;
        @unlink($file_img3);
        delete_item_thumbnail(dirname($file_img3), basename($file_img3));
    }
    $it_img3 = it_img_upload($_FILES['it_img3']['tmp_name'], $_FILES['it_img3']['name'], $it_img_dir.'/'.$it_id);
}
if ($_FILES['it_img4']['name']) {
    if($w == 'u' && $it_img4) {
        $file_img4 = $it_img_dir.'/'.$it_img4;
        @unlink($file_img4);
        delete_item_thumbnail(dirname($file_img4), basename($file_img4));
    }
    $it_img4 = it_img_upload($_FILES['it_img4']['tmp_name'], $_FILES['it_img4']['name'], $it_img_dir.'/'.$it_id);
}
if ($_FILES['it_img5']['name']) {
    if($w == 'u' && $it_img5) {
        $file_img5 = $it_img_dir.'/'.$it_img5;
        @unlink($file_img5);
        delete_item_thumbnail(dirname($file_img5), basename($file_img5));
    }
    $it_img5 = it_img_upload($_FILES['it_img5']['tmp_name'], $_FILES['it_img5']['name'], $it_img_dir.'/'.$it_id);
}
if ($_FILES['it_img6']['name']) {
    if($w == 'u' && $it_img6) {
        $file_img6 = $it_img_dir.'/'.$it_img6;
        @unlink($file_img6);
        delete_item_thumbnail(dirname($file_img6), basename($file_img6));
    }
    $it_img6 = it_img_upload($_FILES['it_img6']['tmp_name'], $_FILES['it_img6']['name'], $it_img_dir.'/'.$it_id);
}
if ($_FILES['it_img7']['name']) {
    if($w == 'u' && $it_img7) {
        $file_img7 = $it_img_dir.'/'.$it_img7;
        @unlink($file_img7);
        delete_item_thumbnail(dirname($file_img7), basename($file_img7));
    }
    $it_img7 = it_img_upload($_FILES['it_img7']['tmp_name'], $_FILES['it_img7']['name'], $it_img_dir.'/'.$it_id);
}
if ($_FILES['it_img8']['name']) {
    if($w == 'u' && $it_img8) {
        $file_img8 = $it_img_dir.'/'.$it_img8;
        @unlink($file_img8);
        delete_item_thumbnail(dirname($file_img8), basename($file_img8));
    }
    $it_img8 = it_img_upload($_FILES['it_img8']['tmp_name'], $_FILES['it_img8']['name'], $it_img_dir.'/'.$it_id);
}
if ($_FILES['it_img9']['name']) {
    if($w == 'u' && $it_img9) {
        $file_img9 = $it_img_dir.'/'.$it_img9;
        @unlink($file_img9);
        delete_item_thumbnail(dirname($file_img9), basename($file_img9));
    }
    $it_img9 = it_img_upload($_FILES['it_img9']['tmp_name'], $_FILES['it_img9']['name'], $it_img_dir.'/'.$it_id);
}
if ($_FILES['it_img10']['name']) {
    if($w == 'u' && $it_img10) {
        $file_img10 = $it_img_dir.'/'.$it_img10;
        @unlink($file_img10);
        delete_item_thumbnail(dirname($file_img10), basename($file_img10));
    }
    $it_img10 = it_img_upload($_FILES['it_img10']['tmp_name'], $_FILES['it_img10']['name'], $it_img_dir.'/'.$it_id);
}
/* 단독상품 상세설명 이미지 */
if ($_FILES['it_box_img']['name']) {
    if($w == 'u' && $it_box_img) {
        $it_box_img = $it_img_dir.'/'.$it_box_img;
        @unlink($it_box_img);
        delete_item_thumbnail(dirname($it_box_img), basename($it_box_img));
    }
    $it_box_img = it_img_upload($_FILES['it_box_img']['tmp_name'], $_FILES['it_box_img']['name'], $it_img_dir.'/'.$it_id);

}

if ($w == "" || $w == "u")
{
    // 다음 입력을 위해서 옵션값을 쿠키로 한달동안 저장함
    //@setcookie("ck_ca_id",  $ca_id,  time() + 86400*31, $default[de_cookie_dir], $default[de_cookie_domain]);
    //@setcookie("ck_maker",  stripslashes($it_maker),  time() + 86400*31, $default[de_cookie_dir], $default[de_cookie_domain]);
    //@setcookie("ck_origin", stripslashes($it_origin), time() + 86400*31, $default[de_cookie_dir], $default[de_cookie_domain]);
    @set_cookie("ck_ca_id", $ca_id, time() + 86400*31);
    @set_cookie("ck_ca_id2", $ca_id2, time() + 86400*31);
    @set_cookie("ck_ca_id3", $ca_id3, time() + 86400*31);
    @set_cookie("ck_maker", stripslashes($it_maker), time() + 86400*31);
    @set_cookie("ck_origin", stripslashes($it_origin), time() + 86400*31);
}

/*************** 트랜잭션 관련 ****************/
$error_cnt = 0;
mysqli_autocommit($g5['connect_db'], false);
/*************** 트랜잭션 관련 ****************/

// 관련상품을 우선 삭제함
$al_data_old['item_relation'] = sql_fetch_result(" select * from {$g5['g5_shop_item_relation_table']} where it_id = '$it_id' ");
sql_query(" delete from {$g5['g5_shop_item_relation_table']} where it_id = '$it_id' ");
/*************** 트랜잭션 관련 ****************/
if( mysqli_errno($g5['connect_db']) ){
	$error_cnt += 1;
}

/*************** 트랜잭션 관련 ****************/
// 관련상품의 반대도 삭제
$al_data_old['item_relation_reverse'] = sql_fetch_result(" select * from {$g5['g5_shop_item_relation_table']} where it_id2 = '$it_id' ");
sql_query(" delete from {$g5['g5_shop_item_relation_table']} where it_id2 = '$it_id' ");
/*************** 트랜잭션 관련 ****************/
if( mysqli_errno($g5['connect_db']) ){
	$error_cnt += 1;
}

/*************** 트랜잭션 관련 ****************/
// 이벤트상품을 우선 삭제함
$al_data_old['event_item'] = sql_fetch_result(" select * from {$g5['g5_shop_event_item_table']} where it_id = '$it_id' ");
sql_query(" delete from {$g5['g5_shop_event_item_table']} where it_id = '$it_id' ");
/*************** 트랜잭션 관련 ****************/
if( mysqli_errno($g5['connect_db']) ){
	$error_cnt += 1;
}

/*************** 트랜잭션 관련 ****************/
// 관련 상품 영상 삭제함
$al_data_old['item_player'] = sql_fetch_result(" select * from {$g5['g5_shop_item_player_table']} where it_id = '$it_id' ");
sql_query(" delete from {$g5['g5_shop_item_player_table']} where it_id = '$it_id' ");
/*************** 트랜잭션 관련 ****************/
if( mysqli_errno($g5['connect_db']) ){
	$error_cnt += 1;
}



/* 관리자 일때만 옵션관런 수정되도록 - 공급사 수정시 옵션이 삭제되는문제로 인한수정.. */
if( $member['mb_type'] == "9" ){ 

	/*************** 트랜잭션 관련 ****************/
	// 선택옵션
    $al_data_old['item_option'] = sql_fetch_result(" select * from {$g5['g5_shop_item_option_table']} where io_type = '0' and it_id = '$it_id' ");
	sql_query(" delete from {$g5['g5_shop_item_option_table']} where io_type = '0' and it_id = '$it_id' "); // 기존선택옵션삭제
	/*************** 트랜잭션 관련 ****************/
	if( mysqli_errno($g5['connect_db']) ){
		$error_cnt += 1;
	}

	/*************** 트랜잭션 관련 ****************/
	$option_count = (isset($_POST['opt_id']) && is_array($_POST['opt_id'])) ? count($_POST['opt_id']) : array();
	if($option_count) {
		// 옵션명
		$opt1_cnt = $opt2_cnt = $opt3_cnt = 0;
		for($i=0; $i<$option_count; $i++) {
			$_POST['opt_id'][$i] = preg_replace(G5_OPTION_ID_FILTER, '', $_POST['opt_id'][$i]);

			$opt_val = explode(chr(30), $_POST['opt_id'][$i]);
			if($opt_val[0])
				$opt1_cnt++;
			if($opt_val[1])
				$opt2_cnt++;
			if($opt_val[2])
				$opt3_cnt++;
		}

		if($opt1_subject && $opt1_cnt) {
			$it_option_subject = $opt1_subject;
			//if($opt2_subject && $opt2_cnt)
			if($opt2_subject)
				$it_option_subject .= ','.$opt2_subject;
			if($opt3_subject && $opt3_cnt)
				$it_option_subject .= ','.$opt3_subject;
		}
	}

	// 추가옵션
    $al_data_old['item_option_add'] = sql_fetch_result(" select * from {$g5['g5_shop_item_option_table']} where io_type = '1' and it_id = '$it_id' ");
	sql_query(" delete from {$g5['g5_shop_item_option_table']} where io_type = '1' and it_id = '$it_id' "); // 기존추가옵션삭제
	/*************** 트랜잭션 관련 ****************/
	if( mysqli_errno($g5['connect_db']) ){
		$error_cnt += 1;
	}
	/*************** 트랜잭션 관련 ****************/

	$supply_count = (isset($_POST['spl_id']) && is_array($_POST['spl_id'])) ? count($_POST['spl_id']) : array();
	if($supply_count) {
		// 추가옵션명
		$arr_spl = array();
		for($i=0; $i<$supply_count; $i++) {
			$_POST['spl_id'][$i] = preg_replace(G5_OPTION_ID_FILTER, '', $_POST['spl_id'][$i]);

			$spl_val = explode(chr(30), $_POST['spl_id'][$i]);
			if(!in_array($spl_val[0], $arr_spl))
				$arr_spl[] = $spl_val[0];
		}

		$it_supply_subject = implode(',', $arr_spl);
	}

}

// 상품요약정보
$value_array = array();
for($i=0; $i<count($_POST['ii_article']); $i++) {
    $key = $_POST['ii_article'][$i];
    $val = $_POST['ii_value'][$i];
    $value_array[$key] = $val;
}
$it_info_value = addslashes(serialize($value_array));

// 포인트 비율 값 체크
if(($it_point_type == 1 || $it_point_type == 2) && $it_point > 99)
    alert("포인트 비율을 0과 99 사이의 값으로 입력해 주십시오.");

$sql2 = " select comp_code, comp_name from tbl_member where comp_code='$comp_code' order by comp_name ";

$result2 = sql_query($sql2);
	for ($i=0; $row2=sql_fetch_array($result2); $i++) {
		$comp_name=$row2['comp_name'];
	}			
$key_sum=$it_name.','.$comp_name;


$it_name = strip_tags(trim($_POST['it_name']));

if(strpos($it_search,$key_sum)==false){
	$it_search = $it_search.','.$it_name.','.$comp_name;
}

if ($it_name == "")
    alert("상품명을 입력해 주십시오.");



if( empty($it_orderbox_yn) ){
	$it_orderbox_yn = "N";
}else{
	$it_orderbox_yn = "Y";
}


$it_company_array = array($it_company);

foreach ($it_company_array as $value) {
    $company = implode(",", $value);
}


if ($is_admin != 'vender') {
    $sql_common = " ca_id       = '$ca_id',
            ca_id2              = '$ca_id2',
            ca_id3              = '$ca_id3',
            it_skin             = '$it_skin',
            it_mobile_skin      = '$it_mobile_skin',
            it_name             = '$it_name',
            it_maker            = '$it_maker',
            it_origin           = '$it_origin',
            it_brand            = '$it_brand',
            it_model            = '$it_model',
            it_desc             = '$it_desc',
            it_option_subject   = '$it_option_subject',
            it_supply_subject   = '$it_supply_subject',
            it_type1            = '$it_type1',
            it_type2            = '$it_type2',
            it_type3            = '$it_type3',
            it_type4            = '$it_type4',
            it_service_view_1   = '$it_service_view_1',
            it_service_view_2   = '$it_service_view_2',
            it_type5            = '$it_type5',
            it_basic            = '$it_basic',
            it_explan           = '$it_explan',
            it_explan2          = '".strip_tags(trim($_POST['it_explan']))."',
            it_mobile_explan    = '$it_mobile_explan',
            it_sellingpoint    = '$it_sellingpoint',
            it_cust_price       = '$it_cust_price',
            it_price            = '$it_price',
            it_point            = '$it_point',
            it_point_type       = '$it_point_type',
            it_supply_point     = '$it_supply_point',
            it_notax            = '$it_notax',
            it_sell_email       = '$it_sell_email',
            it_use              = '$it_use',
            it_multisend		= '$it_multisend',
            it_nocoupon         = '$it_nocoupon',
            it_soldout          = '$it_soldout',
            it_stock_qty        = '$it_stock_qty',
            it_stock_sms        = '$it_stock_sms',
            it_noti_qty         = '$it_noti_qty',
            it_sc_type          = '$it_sc_type',
            it_sc_method        = '$it_sc_method',
            it_sc_price         = '$it_sc_price',
            it_sc_minimum       = '$it_sc_minimum',
            it_sc_qty           = '$it_sc_qty',
            it_buy_min_qty      = '$it_buy_min_qty',
            it_buy_max_qty      = '$it_buy_max_qty',
            it_head_html        = '$it_head_html',
            it_tail_html        = '$it_tail_html',
            it_mobile_head_html = '$it_mobile_head_html',
            it_mobile_tail_html = '$it_mobile_tail_html',
            it_ip               = '{$_SERVER['REMOTE_ADDR']}',
            it_order            = '$it_order',
            it_tel_inq          = '$it_tel_inq',
            it_info_gubun       = '$it_info_gubun',
            it_info_value       = '$it_info_value',
            it_shop_memo        = '$it_shop_memo',
            ec_mall_pid         = '$ec_mall_pid',
            it_img1             = '$it_img1',
            it_img2             = '$it_img2',
            it_img3             = '$it_img3',
            it_img4             = '$it_img4',
            it_img5             = '$it_img5',
            it_img6             = '$it_img6',
            it_img7             = '$it_img7',
            it_img8             = '$it_img8',
            it_img9             = '$it_img9',
            it_img10            = '$it_img10',
            comp_code           = '$comp_code',
            it_trans_bundle     = '$it_trans_bundle',
            it_web_view         = '$it_web_view',
            it_app_view         = '$it_app_view',
            it_new_view         = '$it_new_view',
            it_search			= '$it_search',
            it_health_info      = '$it_health_info',

            it_supply_price     = '$it_supply_price',
            it_dodome_price     = '$it_dodome_price',
            it_drug_price       = '$it_drug_price',
            it_drug_ori_price   = '$it_drug_ori_price',
            it_incen			= '$it_incen',
            it_period_yn        = '$it_period_yn',
            it_period_min       = '$it_period_min',
            it_period_max       = '$it_period_max',
            it_company          = '$company',
            it_order_close_time = '$it_order_close_time',
            it_release_day      = '$it_release_day',
            it_app_norm_price   = '$it_app_norm_price',
            it_app_sale_price   = '$it_app_sale_price',
            it_app_drug_price   = '$it_app_drug_price',
            it_opt_box			= '$it_opt_box',
            it_box_img			= '$it_box_img',
            it_box_txt			= '$it_box_txt',
            it_orderbox_txt		= '$it_orderbox_txt',
            it_orderbox_yn		= '$it_orderbox_yn',
            it_app_incen        = '$it_app_incen'";

} else {
    $sql_common = " ca_id       = '$ca_id',
            ca_id2              = '$ca_id2',
            ca_id3              = '$ca_id3',
            it_skin             = '$it_skin',
            it_mobile_skin      = '$it_mobile_skin',
            it_name             = '$it_name',
            it_maker            = '$it_maker',
            it_origin           = '$it_origin',
            it_brand            = '$it_brand',
            it_model            = '$it_model',
            it_desc             = '$it_desc',
            it_option_subject   = '$it_option_subject',
            it_supply_subject   = '$it_supply_subject',
            it_type1            = '$it_type1',
            it_type2            = '$it_type2',
            it_type3            = '$it_type3',
            it_type4            = '$it_type4',
            it_service_view_1   = '$it_service_view_1',
            it_service_view_2   = '$it_service_view_2',
            it_type5            = '$it_type5',
            it_basic            = '$it_basic',
            it_explan           = '$it_explan',
            it_explan2          = '".strip_tags(trim($_POST['it_explan']))."',
            it_mobile_explan    = '$it_mobile_explan',
            it_sellingpoint    = '$it_sellingpoint',
            it_cust_price       = '$it_cust_price',
            it_price            = '$it_price',
            it_point            = '$it_point',
            it_point_type       = '$it_point_type',
            it_supply_point     = '$it_supply_point',
            it_notax            = '$it_notax',
            it_sell_email       = '$it_sell_email',
            it_use              = '$it_use',
            it_nocoupon         = '$it_nocoupon',
            it_soldout          = '$it_soldout',
            it_stock_qty        = '$it_stock_qty',
            it_stock_sms        = '$it_stock_sms',
            it_noti_qty         = '$it_noti_qty',
            it_sc_type          = '$it_sc_type',
            it_sc_method        = '$it_sc_method',
            it_sc_price         = '$it_sc_price',
            it_sc_minimum       = '$it_sc_minimum',
            it_sc_qty           = '$it_sc_qty',
            it_buy_min_qty      = '$it_buy_min_qty',
            it_buy_max_qty      = '$it_buy_max_qty',
            it_head_html        = '$it_head_html',
            it_tail_html        = '$it_tail_html',
            it_mobile_head_html = '$it_mobile_head_html',
            it_mobile_tail_html = '$it_mobile_tail_html',
            it_ip               = '{$_SERVER['REMOTE_ADDR']}',
            it_order            = '$it_order',
            it_tel_inq          = '$it_tel_inq',
            it_info_gubun       = '$it_info_gubun',
            it_info_value       = '$it_info_value',
            it_shop_memo        = '$it_shop_memo',
            ec_mall_pid         = '$ec_mall_pid',
            it_img1             = '$it_img1',
            it_img2             = '$it_img2',
            it_img3             = '$it_img3',
            it_img4             = '$it_img4',
            it_img5             = '$it_img5',
            it_img6             = '$it_img6',
            it_img7             = '$it_img7',
            it_img8             = '$it_img8',
            it_img9             = '$it_img9',
            it_img10            = '$it_img10',
            comp_code           = '$comp_code',
            it_supply_price     = '$it_supply_price',
            it_search			= '$it_search',
            it_health_info      = '$it_health_info',
            it_trans_bundle     = '$it_trans_bundle',
            it_period_yn        = '$it_period_yn',
            it_period_min       = '$it_period_min',
            it_period_max       = '$it_period_max',
            it_company          = '$company',
            it_order_close_time = '$it_order_close_time',
            it_release_day      = '$it_release_day'";

    
}


$sql_common .= " , comp_it_id = '".$comp_it_id."' ";

if ($w == "")
{
    $it_id = $_POST['it_id'];

    if (!trim($it_id)) {
        //alert('상품 코드가 없으므로 상품을 추가하실 수 없습니다.');
		//상품코드생성	
		$sql = " select max(it_id) it_id from tbl_shop_item where it_id like 'S%' ";
		$row = sql_fetch($sql);
		if($row['it_id']) {
			$it_id_no = substr($row['it_id'], 1, 6);
			
			$it_id_no = $it_id_no+ 1;
			$it_id = 'S'. str_pad($it_id_no,"5","0",STR_PAD_LEFT);

		} else {
			$it_id = 'S00001';
		}
    }

    $t_it_id = preg_replace("/[A-Za-z0-9\-_]/", "", $it_id);
    if($t_it_id)
        alert('상품 코드는 영문자, 숫자, -, _ 만 사용할 수 있습니다.');
	
	if ($is_admin == 'vender') {
		$sql_common .= " , it_status = '0' ";
	} else {
		$sql_common .= " , it_status = '3' ";
	}

    $sql_common .= " , it_time = '".G5_TIME_YMDHIS."' ";
    $sql_common .= " , it_update_time = '".G5_TIME_YMDHIS."' ";
    $sql = " insert {$g5['g5_shop_item_table']}
                set it_id = '$it_id',
					$sql_common	";

    sql_query($sql);
	/*************** 트랜잭션 관련 ****************/
	if( mysqli_errno($g5['connect_db']) ){
		$error_cnt += 1;
	}
	/*************** 트랜잭션 관련 ****************/
    $sql2 = "insert into {$g5['g5_shop_item_player_table']} (it_id,ca_id,it_player,it_player_you) values ('$it_id', '$ca_id', '$it_player','$it_player_you') ";
	sql_query($sql2);
	if( mysqli_errno($g5['connect_db']) ){
		$error_cnt += 1;
	}
	/*************** 트랜잭션 관련 ****************/
	if($prd_status_no_reason_1 == "단종"){
		$it_noreason = 1;
	}
	else if($prd_status_no_reason_1 == "일시품절"){
		$it_noreason = 2;
	}else{
		$it_noreason = 3;
	}
	/*************** 트랜잭션 관련 ****************/
	/*
		$sql2 = "insert into tbl_shop_item_mail (it_id,ca_id,it_name,it_use,it_noreason,it_repo_date,it_time_update) values ('$it_id', '$ca_id', '$it_name','$it_soldout','$prd_status_no_reason_1','$prd_status_no_reason_date','".G5_TIME_YMDHIS."') "; 
	sql_query($sql2);
	if( mysqli_errno($g5['connect_db']) ){
		$error_cnt += 1;
	}
	*/
	/*************** 트랜잭션 관련 ****************/

    /************* 관리자 로그 처리 START *************/
    $al_data['post'] = $_POST;
    insert_admin_log(600,600300, '상품 등록', '', $it_id, '', $_SERVER['REQUEST_URI'], $al_data);
    /************* 관리자 로그 처리 END *************/
}
else if ($w == "u")
{
    $sql_common .= " , it_update_time = '".G5_TIME_YMDHIS."' ";
    $sql = " update {$g5['g5_shop_item_table']}
                set $sql_common
              where it_id = '$it_id' ";
    sql_query($sql);
	/*************** 트랜잭션 관련 ****************/
	if( mysqli_errno($g5['connect_db']) ){
		$error_cnt += 1;
	}
	/*************** 트랜잭션 관련 ****************/
	
	$sql = "insert into {$g5['g5_shop_item_player_table']} (it_id,ca_id,it_player,it_player_you) values ('$it_id','$ca_id','$it_player','$it_player_you') on duplicate key update it_id='$it_id',it_player='$it_player', it_player_you='$it_player_you', ca_id='$ca_id' ";
	/*$sql = " update {$g5['g5_shop_item_player_table']}
                set it_player='$it_player' ,it_player_you='$it_player_you'
              where it_id = '$it_id' ";*/
    sql_query($sql);
	/*************** 트랜잭션 관련 ****************/
	if( mysqli_errno($g5['connect_db']) ){
		$error_cnt += 1;
	}
	/*************** 트랜잭션 관련 ****************/
	
	//상품품절 사유 
	$sql = "insert into tbl_shop_item_mail (it_id,ca_id,it_name,it_use,it_noreason,it_repo_date,it_time_update) values ('$it_id','$ca_id','$it_name','$it_soldout','$prd_status_no_reason_1','$prd_status_no_reason_date','".G5_TIME_YMDHIS."') on duplicate key update it_id='$it_id',it_use='$it_soldout', it_noreason='$prd_status_no_reason_1',it_repo_date='$prd_status_no_reason_date',it_time_update='".G5_TIME_YMDHIS."'";
    sql_query($sql);
	/*************** 트랜잭션 관련 ****************/
	if( mysqli_errno($g5['connect_db']) ){
		$error_cnt += 1;
	}
	/*************** 트랜잭션 관련 ****************/

    /************* 관리자 로그 처리 START *************/
    $al_data['post'] = str_white_remove($_POST);
    insert_admin_log(600,600300, '상품 수정', '', $it_id, '', $_SERVER['REQUEST_URI'], $al_data, $al_data_old);
    /************* 관리자 로그 처리 END *************/
	
	// 메일 알림
	if($prd_status == "d"){
		
		include_once(G5_LIB_PATH.'/mailer.lib.php');
		$subject = $member['comp_name'].' 판매 가능 여부 변경 알림 메일';
		$content = $member['comp_name'].'  [ '.$it_name.' ] 상품이 판매 중지 상태로 변경되었습니다. 확인해주세요. ';
		
		
		if($prd_status_change =="1"){
			$content = $member['comp_name'].' 의 [ '.$it_name.' ] 상품이 판매 가능 상태로 변경되었습니다. 확인해주세요.';
		}
		mailer($config['cf_admin_email_name'], 'onkorderstation@gmail.com' ,'minjoo.ahn@taejeongroup.com', $subject, $content, 1);
		mailer($config['cf_admin_email_name'], 'onkorderstation@gmail.com' ,'jisoo.im@taejeongroup.com', $subject, $content, 1);
		mailer($config['cf_admin_email_name'], 'onkorderstation@gmail.com' ,'hwijin.park@taejeongroup.com', $subject, $content, 1);
	}

    // 메일 알림
	if($prd_status_sold == "d"){
		include_once(G5_LIB_PATH.'/mailer.lib.php');
		$subject = $member['comp_name'].' 품절 여부 변경 알림 메일';
		$content = $member['comp_name'].' 의 [ '.$it_name.' ] 상품이 품절상태에서 판매 가능 상태로 변경되었습니다. 확인해주세요.';
		if($prd_status_change_sold =="1"){
			$content = $member['comp_name'].' 의 [ '.$it_name.' ] 상품이 품절 상태로 변경되었습니다. 품절 사유 : '.$prd_status_no_reason_1.' [ 재입고 예상일자 ] : '.$prd_status_no_reason_date;
		}
        mailer($config['cf_admin_email_name'], 'onkorderstation@gmail.com' ,'minjoo.ahn@taejeongroup.com', $subject, $content, 1);
        mailer($config['cf_admin_email_name'], 'onkorderstation@gmail.com' ,'jisoo.im@taejeongroup.com', $subject, $content, 1);
        mailer($config['cf_admin_email_name'], 'onkorderstation@gmail.com' ,'hwijin.park@taejeongroup.com', $subject, $content, 1);
	}
	
	if($qty_tr == "d"){
		if($qty_count == 0) {
		include_once(G5_LIB_PATH.'/mailer.lib.php');
		$subject = $member['comp_name'].' 품절 여부 변경 알림 메일';
		$content = $member['comp_name'].' 의 [ '.$it_name.' ] 상품의 재고가 0개로 변경되어 품절되었습니다. 확인해주세요.';

        mailer($config['cf_admin_email_name'], 'onkorderstation@gmail.com' ,'minjoo.ahn@taejeongroup.com', $subject, $content, 1);
        mailer($config['cf_admin_email_name'], 'onkorderstation@gmail.com' ,'jisoo.im@taejeongroup.com', $subject, $content, 1);
        mailer($config['cf_admin_email_name'], 'onkorderstation@gmail.com' ,'hwijin.park@taejeongroup.com', $subject, $content, 1);
		}
	}
	
	
}
/*
else if ($w == "d")
{
    if ($is_admin != 'super')
    {
        $sql = " select it_id from {$g5['g5_shop_item_table']} a, {$g5['g5_shop_category_table']} b
                  where a.it_id = '$it_id'
                    and a.ca_id = b.ca_id
                    and b.ca_mb_id = '{$member['mb_id']}' ";
        $row = sql_fetch($sql);
        if (!$row['it_id'])
            alert("\'{$member['mb_id']}\' 님께서 삭제 할 권한이 없는 상품입니다.");
    }

    itemdelete($it_id);
}
*/

if ($w == "" || $w == "u")
{
    // 관련상품 등록
    $it_id2 = explode(",", $it_list);
    for ($i=0; $i<count($it_id2); $i++)
    {
        if (trim($it_id2[$i]))
        {
            $sql = " insert into {$g5['g5_shop_item_relation_table']}
                        set it_id  = '$it_id',
                            it_id2 = '$it_id2[$i]',
                            ir_no = '$i' ";
            sql_query($sql, false);
			/*************** 트랜잭션 관련 ****************/
			if( mysqli_errno($g5['connect_db']) ){
				$error_cnt += 1;
			}
			/*************** 트랜잭션 관련 ****************/

            // 관련상품의 반대로도 등록
            $sql = " insert into {$g5['g5_shop_item_relation_table']}
                        set it_id  = '$it_id2[$i]',
                            it_id2 = '$it_id',
                            ir_no = '$i' ";
            sql_query($sql, false);
			/*************** 트랜잭션 관련 ****************/
			if( mysqli_errno($g5['connect_db']) ){
				$error_cnt += 1;
			}
			/*************** 트랜잭션 관련 ****************/
        }
    }

    // 이벤트상품 등록
    $ev_id = explode(",", $ev_list);
    for ($i=0; $i<count($ev_id); $i++)
    {
        if (trim($ev_id[$i]))
        {
            $sql = " insert into {$g5['g5_shop_event_item_table']}
                        set ev_id = '$ev_id[$i]',
                            it_id = '$it_id' ";
            sql_query($sql, false);
			/*************** 트랜잭션 관련 ****************/
			if( mysqli_errno($g5['connect_db']) ){
				$error_cnt += 1;
			}
			/*************** 트랜잭션 관련 ****************/
        }
    }
}


/* 관리자 일때만 옵션관런 수정되도록 - 공급사 수정시 옵션이 삭제되는문제로 인한수정.. */
if( $member['mb_type'] == "9" ){ 

	$opt_img_dir = G5_DATA_PATH.'/option';
    //$al_data = array();
	// 옵션이 있는 상품일때 상세페이지 선택박스
	if($option_count) {
		$comma = '';
		$sql = " INSERT INTO {$g5['g5_shop_item_option_table']}
						( `io_id`, `io_type`, `it_id`, `io_name`,`io_price`, io_supply_price, io_dodome_price, io_drug_price, io_incen, io_app_sale_price, io_app_drug_price, io_app_incen, io_period_rate1,io_period_cnt1,`io_stock_qty`, `io_use`, `io_img`, `io_txt`  )
					VALUES ";
		for($i=0; $i<$option_count; $i++) {

			$opt_img = "";
			
			// 이미지업로드
			if ($_FILES['opt_img']['name'][$i]) {

				// 신규파일업로드인데 기존파일이 있을경우 삭제.
				if( $w == 'u' && !empty($opt_img_origin[$i]) ) {
					$opt_file_img = $opt_img_dir.'/'.$opt_img_origin[$i];
					@unlink($opt_file_img);
					delete_item_thumbnail(dirname($opt_file_img), basename($opt_file_img));
				}
				
				$opt_img = it_img_upload3($_FILES['opt_img']['tmp_name'][$i], $_FILES['opt_img']['name'][$i], $opt_img_dir.'/'.$_POST['opt_id'][$i]);

			}else if( empty($_FILES['opt_img']['name'][$i]) && $opt_img_origin[$i] != "" ){

				$opt_img = $opt_img_origin[$i];

			}
			if ($opt_img_del[$i]) {

				$opt_img = $opt_img_dir.'/'.$opt_img_origin[$i];
				@unlink($opt_img);
				delete_item_thumbnail(dirname($opt_img), basename($opt_img));
				$opt_img = '';
			}

			$sql .= $comma . " ( '{$_POST['opt_id'][$i]}', '0', '$it_id', '{$_POST['opt_name'][$i]}', '{$_POST['opt_price'][$i]}','{$_POST['opt_supply_price'][$i]}','{$_POST['opt_dodome_price'][$i]}','{$_POST['opt_drug_price'][$i]}','{$_POST['opt_incen'][$i]}','{$_POST['opt_app_sale_price'][$i]}','{$_POST['opt_app_drug_price'][$i]}','{$_POST['opt_app_incen'][$i]}','{$_POST['opt_period_rate1'][$i]}','{$_POST['opt_period_cnt1'][$i]}', '{$_POST['opt_stock_qty'][$i]}', '{$_POST['opt_use'][$i]}', '{$opt_img}', '{$_POST['opt_txt'][$i]}' )";
			$comma = ' , ';

            /*$al_data[] = array(
                'io_id' => $_POST['opt_id'][$i],
                'io_type' => 0,
                'it_id' => $it_id,
                'io_name' => $_POST['opt_name'][$i],
                'io_price' => $_POST['opt_price'][$i],
                'io_supply_price' => $_POST['opt_supply_price'][$i],
                'io_dodome_price' => $_POST['opt_dodome_price'][$i],
                'io_drug_price' => $_POST['opt_drug_price'][$i],
                'io_incen' => $_POST['opt_incen'][$i],
                'io_app_sale_price' => $_POST['opt_app_sale_price'][$i],
                'io_app_drug_price' => $_POST['opt_app_drug_price'][$i],
                'io_app_incen' => $_POST['opt_app_incen'][$i],
                'io_period_rate1' => $_POST['opt_period_rate1'][$i],
                'io_period_cnt1' => $_POST['opt_period_cnt1'][$i],
                'io_stock_qty' => $_POST['opt_stock_qty'][$i],
                'io_use' => $_POST['opt_use'][$i],
                'io_img' => $opt_img,
                'io_txt' => $_POST['opt_txt'][$i],
            );*/
		}

		sql_query($sql);
		/*************** 트랜잭션 관련 ****************/
		if( mysqli_errno($g5['connect_db']) ){
			$error_cnt += 1;
		}
		/*************** 트랜잭션 관련 ****************/
	}

	// 추가옵션등록
	if($supply_count) {
		$comma = '';
		$sql = " INSERT INTO {$g5['g5_shop_item_option_table']}
						( `io_id`, `io_type`, `it_id`, `io_price`, `io_stock_qty`, `io_noti_qty`, `io_use` )
					VALUES ";
		for($i=0; $i<$supply_count; $i++) {
			$sql .= $comma . " ( '{$_POST['spl_id'][$i]}', '1', '$it_id', '{$_POST['spl_price'][$i]}', '{$_POST['spl_stock_qty'][$i]}', '{$_POST['spl_noti_qty'][$i]}', '{$_POST['spl_use'][$i]}' )";
			$comma = ' , ';
		}

		sql_query($sql);
		/*************** 트랜잭션 관련 ****************/
		if( mysqli_errno($g5['connect_db']) ){
			$error_cnt += 1;
		}
		/*************** 트랜잭션 관련 ****************/
	}

}

/*// 동일 분류내 상품 동일 옵션 적용
$ca_fields = '';
if(is_checked('chk_ca_it_skin'))                $ca_fields .= " , it_skin = '$it_skin' ";
if(is_checked('chk_ca_it_mobile_skin'))         $ca_fields .= " , it_mobile_skin = '$it_mobile_skin' ";
if(is_checked('chk_ca_it_basic'))               $ca_fields .= " , it_basic = '$it_basic' ";
if(is_checked('chk_ca_it_order'))               $ca_fields .= " , it_order = '$it_order' ";
if(is_checked('chk_ca_it_type'))                $ca_fields .= " , it_type1 = '$it_type1', it_type2 = '$it_type2', it_type3 = '$it_type3', it_type4 = '$it_type4', it_type5 = '$it_type5' ";
if(is_checked('chk_ca_it_maker'))               $ca_fields .= " , it_maker = '$it_maker' ";
if(is_checked('chk_ca_it_origin'))              $ca_fields .= " , it_origin = '$it_origin' ";
if(is_checked('chk_ca_it_brand'))               $ca_fields .= " , it_brand = '$it_brand' ";
if(is_checked('chk_ca_it_model'))               $ca_fields .= " , it_model = '$it_model' ";
if(is_checked('chk_ca_it_notax'))               $ca_fields .= " , it_notax = '$it_notax' ";
if(is_checked('chk_ca_it_sell_email'))          $ca_fields .= " , it_sell_email = '$it_sell_email' ";
if(is_checked('chk_ca_it_shop_memo'))           $ca_fields .= " , it_shop_memo = '$it_shop_memo' ";
if(is_checked('chk_ca_it_tel_inq'))             $ca_fields .= " , it_tel_inq = '$it_tel_inq' ";
if(is_checked('chk_ca_it_use'))                 $ca_fields .= " , it_use = '$it_use' ";
if(is_checked('chk_ca_it_nocoupon'))            $ca_fields .= " , it_nocoupon = '$it_nocoupon' ";
if(is_checked('chk_ca_it_soldout'))             $ca_fields .= " , it_soldout = '$it_soldout' ";
if(is_checked('chk_ca_it_info'))                $ca_fields .= " , it_info_gubun = '$it_info_gubun', it_info_value = '$it_info_value' ";
if(is_checked('chk_ca_it_price'))               $ca_fields .= " , it_price = '$it_price' ";
if(is_checked('chk_ca_it_cust_price'))          $ca_fields .= " , it_cust_price = '$it_cust_price' ";
if(is_checked('chk_ca_it_point'))               $ca_fields .= " , it_point = '$it_point' ";
if(is_checked('chk_ca_it_point_type'))          $ca_fields .= " , it_point_type = '$it_point_type' ";
if(is_checked('chk_ca_it_supply_point'))        $ca_fields .= " , it_supply_point = '$it_supply_point' ";
if(is_checked('chk_ca_it_stock_qty'))           $ca_fields .= " , it_stock_qty = '$it_stock_qty' ";
if(is_checked('chk_ca_it_noti_qty'))            $ca_fields .= " , it_noti_qty = '$it_noti_qty' ";
if(is_checked('chk_ca_it_sendcost'))            $ca_fields .= " , it_sc_type = '$it_sc_type', it_sc_method = '$it_sc_method', it_sc_price = '$it_sc_price', it_sc_minimum = '$it_sc_minimum', it_sc_qty = '$it_sc_qty' ";
if(is_checked('chk_ca_it_buy_min_qty'))         $ca_fields .= " , it_buy_min_qty = '$it_buy_min_qty' ";
if(is_checked('chk_ca_it_buy_max_qty'))         $ca_fields .= " , it_buy_max_qty = '$it_buy_max_qty' ";
if(is_checked('chk_ca_it_head_html'))           $ca_fields .= " , it_head_html = '$it_head_html' ";
if(is_checked('chk_ca_it_tail_html'))           $ca_fields .= " , it_tail_html = '$it_tail_html' ";
if(is_checked('chk_ca_it_mobile_head_html'))    $ca_fields .= " , it_mobile_head_html = '$it_mobile_head_html' ";
if(is_checked('chk_ca_it_mobile_tail_html'))    $ca_fields .= " , it_mobile_tail_html = '$it_mobile_tail_html' ";
if(is_checked('chk_ca_1'))                      $ca_fields .= " , it_1_subj = '$it_1_subj', it_1 = '$it_1' ";
if(is_checked('chk_ca_2'))                      $ca_fields .= " , it_2_subj = '$it_2_subj', it_2 = '$it_2' ";
if(is_checked('chk_ca_3'))                      $ca_fields .= " , it_3_subj = '$it_3_subj', it_3 = '$it_3' ";
if(is_checked('chk_ca_4'))                      $ca_fields .= " , it_4_subj = '$it_4_subj', it_4 = '$it_4' ";
if(is_checked('chk_ca_5'))                      $ca_fields .= " , it_5_subj = '$it_5_subj', it_5 = '$it_5' ";
if(is_checked('chk_ca_6'))                      $ca_fields .= " , it_6_subj = '$it_6_subj', it_6 = '$it_6' ";
if(is_checked('chk_ca_7'))                      $ca_fields .= " , it_7_subj = '$it_7_subj', it_7 = '$it_7' ";
if(is_checked('chk_ca_8'))                      $ca_fields .= " , it_8_subj = '$it_8_subj', it_8 = '$it_8' ";
if(is_checked('chk_ca_9'))                      $ca_fields .= " , it_9_subj = '$it_9_subj', it_9 = '$it_9' ";
if(is_checked('chk_ca_10'))                     $ca_fields .= " , it_10_subj = '$it_10_subj', it_10 = '$it_10' ";

if($ca_fields) {
    sql_query(" update {$g5['g5_shop_item_table']} set it_name = it_name {$ca_fields} where ca_id = '$ca_id' ");
    if($ca_id2)
        sql_query(" update {$g5['g5_shop_item_table']} set it_name = it_name {$ca_fields} where ca_id2 = '$ca_id2' ");
    if($ca_id3)
        sql_query(" update {$g5['g5_shop_item_table']} set it_name = it_name {$ca_fields} where ca_id3 = '$ca_id3' ");
}

// 모든 상품 동일 옵션 적용
$all_fields = '';
if(is_checked('chk_all_it_skin'))                $all_fields .= " , it_skin = '$it_skin' ";
if(is_checked('chk_all_it_mobile_skin'))         $all_fields .= " , it_mobile_skin = '$it_mobile_skin' ";
if(is_checked('chk_all_it_basic'))               $all_fields .= " , it_basic = '$it_basic' ";
if(is_checked('chk_all_it_order'))               $all_fields .= " , it_order = '$it_order' ";
if(is_checked('chk_all_it_type'))                $all_fields .= " , it_type1 = '$it_type1', it_type2 = '$it_type2', it_type3 = '$it_type3', it_type4 = '$it_type4', it_type5 = '$it_type5' ";
if(is_checked('chk_all_it_maker'))               $all_fields .= " , it_maker = '$it_maker' ";
if(is_checked('chk_all_it_origin'))              $all_fields .= " , it_origin = '$it_origin' ";
if(is_checked('chk_all_it_brand'))               $all_fields .= " , it_brand = '$it_brand' ";
if(is_checked('chk_all_it_model'))               $all_fields .= " , it_model = '$it_model' ";
if(is_checked('chk_all_it_notax'))               $all_fields .= " , it_notax = '$it_notax' ";
if(is_checked('chk_all_it_sell_email'))          $all_fields .= " , it_sell_email = '$it_sell_email' ";
if(is_checked('chk_all_it_shop_memo'))           $all_fields .= " , it_shop_memo = '$it_shop_memo' ";
if(is_checked('chk_all_it_tel_inq'))             $all_fields .= " , it_tel_inq = '$it_tel_inq' ";
if(is_checked('chk_all_it_use'))                 $all_fields .= " , it_use = '$it_use' ";
if(is_checked('chk_all_it_nocoupon'))            $all_fields .= " , it_nocoupon = '$it_nocoupon' ";
if(is_checked('chk_all_it_soldout'))             $all_fields .= " , it_soldout = '$it_soldout' ";
if(is_checked('chk_all_it_info'))                $all_fields .= " , it_info_gubun = '$it_info_gubun', it_info_value = '$it_info_value' ";
if(is_checked('chk_all_it_price'))               $all_fields .= " , it_price = '$it_price' ";
if(is_checked('chk_all_it_cust_price'))          $all_fields .= " , it_cust_price = '$it_cust_price' ";
if(is_checked('chk_all_it_point'))               $all_fields .= " , it_point = '$it_point' ";
if(is_checked('chk_all_it_point_type'))          $all_fields .= " , it_point_type = '$it_point_type' ";
if(is_checked('chk_all_it_supply_point'))        $all_fields .= " , it_supply_point = '$it_supply_point' ";
if(is_checked('chk_all_it_stock_qty'))           $all_fields .= " , it_stock_qty = '$it_stock_qty' ";
if(is_checked('chk_all_it_noti_qty'))            $all_fields .= " , it_noti_qty = '$it_noti_qty' ";
if(is_checked('chk_all_it_sendcost'))            $all_fields .= " , it_sc_type = '$it_sc_type', it_sc_method = '$it_sc_method', it_sc_price = '$it_sc_price', it_sc_minimum = '$it_sc_minimum', it_sc_qty = '$it_sc_qty' ";
if(is_checked('chk_all_it_buy_min_qty'))         $all_fields .= " , it_buy_min_qty = '$it_buy_min_qty' ";
if(is_checked('chk_all_it_buy_max_qty'))         $all_fields .= " , it_buy_max_qty = '$it_buy_max_qty' ";
if(is_checked('chk_all_it_head_html'))           $all_fields .= " , it_head_html = '$it_head_html' ";
if(is_checked('chk_all_it_tail_html'))           $all_fields .= " , it_tail_html = '$it_tail_html' ";
if(is_checked('chk_all_it_mobile_head_html'))    $all_fields .= " , it_mobile_head_html = '$it_mobile_head_html' ";
if(is_checked('chk_all_it_mobile_tail_html'))    $all_fields .= " , it_mobile_tail_html = '$it_mobile_tail_html' ";
if(is_checked('chk_all_1'))                      $all_fields .= " , it_1_subj = '$it_1_subj', it_1 = '$it_1' ";
if(is_checked('chk_all_2'))                      $all_fields .= " , it_2_subj = '$it_2_subj', it_2 = '$it_2' ";
if(is_checked('chk_all_3'))                      $all_fields .= " , it_3_subj = '$it_3_subj', it_3 = '$it_3' ";
if(is_checked('chk_all_4'))                      $all_fields .= " , it_4_subj = '$it_4_subj', it_4 = '$it_4' ";
if(is_checked('chk_all_5'))                      $all_fields .= " , it_5_subj = '$it_5_subj', it_5 = '$it_5' ";
if(is_checked('chk_all_6'))                      $all_fields .= " , it_6_subj = '$it_6_subj', it_6 = '$it_6' ";
if(is_checked('chk_all_7'))                      $all_fields .= " , it_7_subj = '$it_7_subj', it_7 = '$it_7' ";
if(is_checked('chk_all_8'))                      $all_fields .= " , it_8_subj = '$it_8_subj', it_8 = '$it_8' ";
if(is_checked('chk_all_9'))                      $all_fields .= " , it_9_subj = '$it_9_subj', it_9 = '$it_9' ";
if(is_checked('chk_all_10'))                     $all_fields .= " , it_10_subj = '$it_10_subj', it_10 = '$it_10' ";

if($all_fields) {
    sql_query(" update {$g5['g5_shop_item_table']} set it_name = it_name {$all_fields} ");
}
*/
//$qstr = "$qstr&amp;sca=$sca&amp;page=$page";



/*************** 단가변동 여부 체크 *********************/

if($w == "u"){

		$price_change_cnt = 0;
		//소비자가 체크
		if( ($before_it_price != $it_price) && $before_it_price > 0 ){
			$price_change_cnt++;
		}
		//약국공급가 체크
		if( ($before_it_drug_price != $it_drug_price) && $before_it_drug_price > 0 ){
			$price_change_cnt++;
		}
        //약국공급가(회색) 체크
        /*if( ($before_it_drug_ori_price != $it_drug_ori_price) && $before_it_drug_ori_price > 0 ){
            $price_change_cnt++;
        }*/
		//약국마진 체크
		if( ($before_it_incen != $it_incen) && $before_it_incen > 0 ){
			$price_change_cnt++;
		}
		//도도매가 체크
		if( ($before_it_dodome_price != $it_dodome_price) && $before_it_dodome_price > 0 ){
			$price_change_cnt++;
		}
		//매입가 체크
		if( ($before_it_supply_price != $it_supply_price) && $before_it_supply_price > 0 ){
			$price_change_cnt++;
		}
		//가격변동여부가 있다면 tbl_shop_item 테이블 it_send_yn 컬럼 U 로 업데이트후 히스토리 디비에 넣어준다. (ERP 연동을 위함)
		if( $price_change_cnt > 0 ){

			$sql = " update tbl_shop_item set it_send_yn = 'U' where it_id = '$it_id' ";
			sql_query($sql);
			/*************** 트랜잭션 관련 ****************/
			if( mysqli_errno($g5['connect_db']) ){
				$error_cnt += 1;
			}
			/*************** 트랜잭션 관련 ****************/

			$sql = " insert into tbl_price_change_history(
						it_id , 
						before_it_price , 
						before_it_drug_price , 
						before_it_drug_ori_price , 
						before_it_incen , 
						before_it_dodome_price , 
						before_it_supply_price , 
						after_it_price , 
						after_it_drug_price , 
						after_it_drug_ori_price , 
						after_it_incen , 
						after_it_dodome_price , 
						after_it_supply_price
					)values(
						'$it_id' , 
						'$before_it_price' , 
						'$before_it_drug_price' ,
						'$before_it_drug_ori_price' , 
						'$before_it_incen' , 
						'$before_it_dodome_price' , 
						'$before_it_supply_price' , 
						'$it_price' , 
						'$it_drug_price' ,
						'$it_drug_ori_price' ,  
						'$it_incen' , 
						'$it_dodome_price' , 
						'$it_supply_price'				
					) ";
			sql_query($sql);
			/*************** 트랜잭션 관련 ****************/
			if( mysqli_errno($g5['connect_db']) ){
				$error_cnt += 1;
			}
			/*************** 트랜잭션 관련 ****************/

            /************* 관리자 로그 처리 START *************/
            $price_change_log = array(
                'it_price' => $it_price,
                'it_drug_price' => $it_drug_price,
                'it_drug_ori_price' => $it_drug_ori_price,
                'it_incen' => $it_incen,
                'it_dodome_price' => $it_dodome_price,
                'it_supply_price' => $it_supply_price,
            );
            $price_change_old_log = array(
                'before_it_price' => $before_it_price,
                'before_it_drug_price' => $before_it_drug_price,
                'before_it_drug_ori_price' => $before_it_drug_ori_price,
                'before_it_incen' => $before_it_incen,
                'before_it_dodome_price' => $before_it_dodome_price,
                'before_it_supply_price' => $before_it_supply_price,
            );

            insert_admin_log(600,600300, '상품 단가변동', '', $it_id, '', $_SERVER['REQUEST_URI'], $price_change_log, $price_change_old_log);
            /************* 관리자 로그 처리 END *************/

		}

		/*************** 단가변동 여부 체크 *********************/

}
/*************** 트랜잭션 관련 ****************/
if ($error_cnt > 0) {
	mysqli_rollback($g5['connect_db']);
	mysqli_close($g5['connect_db']);
	echo "
	<script>
		alert('데이터베이스의 에러로 인해 롤백되었습니다.');
		history.back();
	</script>
	";
	exit;
} else {
	mysqli_commit($g5['connect_db']);
} 	
/*************** 트랜잭션 관련 ****************/

if ($w == "u") {
	$qstr  = $qstr.'&amp;s_comp_code='.$s_comp_code.'&amp;sca='.$sca.'&amp;page='.$page.'&amp;save_stx='.$stx;
	$qstr  = $qstr.'&amp;s_it_main1='.$s_it_main1.'&amp;s_it_main2='.$s_it_main2.'&amp;s_it_main3='.$s_it_main3;
	$qstr  = $qstr.'&amp;s_it_type1='.$s_it_type1.'&amp;s_it_type2='.$s_it_type2.'&amp;s_it_type3='.$s_it_type3.'&amp;s_it_type4='.$s_it_type4.'&amp;s_it_time='.$s_it_time.'&amp;it_service_view_1='.$it_service_view_1.'&amp;it_service_view_2='.$it_service_view_2;

    goto_url("./itemform.php?w=u&amp;it_id=$it_id&amp;$qstr");
} else if ($w == "d")  {
    $qstr = "ca_id=$ca_id&amp;sfl=$sfl&amp;sca=$sca&amp;page=$page&amp;stx=".urlencode($stx)."&amp;save_stx=".urlencode($save_stx);
    goto_url("./itemlist.php?$qstr");
}

echo "<meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\">";
?>
<script>
    if (confirm("계속 입력하시겠습니까?"))
        //location.href = "<?php echo "./itemform.php?it_id=$it_id&amp;sort1=$sort1&amp;sort2=$sort2&amp;sel_ca_id=$sel_ca_id&amp;sel_field=$sel_field&amp;search=$search&amp;page=$page"?>";
        location.href = "<?php echo "./itemform.php?".str_replace('&amp;', '&', $qstr); ?>";
    else {
		<?php if ($is_admin == 'vender') { ?>
			location.href = "<?php echo "./itemlist_vender.php?".str_replace('&amp;', '&', $qstr); ?>";
		<?php } else { ?>
			location.href = "<?php echo "./itemlist.php?".str_replace('&amp;', '&', $qstr); ?>";
		<?php } ?>
	}
</script>

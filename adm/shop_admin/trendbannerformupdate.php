<?php
$sub_menu = '700600';
include_once('./_common.php');

check_demo();

if ($W == 'd')
    auth_check($auth[$sub_menu], "d");
else
    auth_check($auth[$sub_menu], "w");

check_admin_token();

// 요이대 최대 탭 갯수
$max_event = 3;

//debug2($_POST);exit;

// 이미지 체크
for ($num = 1; $num <= $max_event; $num++) {
    $bn_id_index = 'bn_id_'.$num;
    $bn_id = (int)$_POST[$bn_id_index];

    $tabimg_index = 'bn_tabimg_'.$num;
    $pcimg_index = 'bn_pcimg_'.$num;
    $mimg_index = 'bn_mimg_'.$num;

    $tabimg_tmp = $_FILES[$tabimg_index]['tmp_name'];
    $tabimg_name = $_FILES[$tabimg_index]['name'];

    $pcimg_tmp = $_FILES[$pcimg_index]['tmp_name'];
    $pcimg_name = $_FILES[$pcimg_index]['name'];

    $mimg_tmp = $_FILES[$mimg_index]['tmp_name'];
    $mimg_name = $_FILES[$mimg_index]['name'];

    //if ($bn_bimg_del)  @unlink(G5_DATA_PATH."/banner/$bn_id");

    //파일이 이미지인지 체크합니다.
    if ($tabimg_tmp || $tabimg_name) {

        if (!preg_match('/\.(gif|jpe?g|bmp|png)$/i', $tabimg_name)) {
            alert("이미지 파일만 업로드 할수 있습니다.");
        }

        $timg = @getimagesize($tabimg_tmp);
        if ($timg['2'] < 1 || $timg['2'] > 16) {
            alert("이미지 파일만 업로드 할수 있습니다.");
        }
    }

    if ($pcimg_tmp || $pcimg_name) {

        if (!preg_match('/\.(gif|jpe?g|bmp|png)$/i', $pcimg_name)) {
            alert("이미지 파일만 업로드 할수 있습니다.");
        }

        $timg = @getimagesize($pcimg_tmp);
        if ($timg['2'] < 1 || $timg['2'] > 16) {
            alert("이미지 파일만 업로드 할수 있습니다.");
        }
    }

    if ($mimg_tmp || $mimg_name) {

        if (!preg_match('/\.(gif|jpe?g|bmp|png)$/i', $mimg_name)) {
            alert("이미지 파일만 업로드 할수 있습니다.");
        }

        $timg = @getimagesize($mimg_tmp);
        if ($timg['2'] < 1 || $timg['2'] > 16) {
            alert("이미지 파일만 업로드 할수 있습니다.");
        }
    }

    $bn_event_id_index = 'bn_event_id_'.$num;
    $bn_event_name_index = 'bn_event_name_'.$num;
    $bn_use_index = 'bn_use_'.$num;

    $wdate = date('YmdHis');
    $bn_id_ver = $bn_id.'_'.$wdate;

    // VALUE
    $bn_event_id = $_POST[$bn_event_id_index];
    $bn_event_name = $_POST[$bn_event_name_index];
    $bn_tabimg =  'tab_'.$bn_id_ver;
    $bn_pcimg = 'pc_'.$bn_id_ver;
    $bn_mimg = 'm_'.$bn_id_ver;
    $bn_use = $_POST[$bn_use_index];

    $sql_img_add = "";
    if ($tabimg_name)
        $sql_img_add .= "bn_tabimg        = '$bn_tabimg',";
    if ($pcimg_name)
        $sql_img_add .= "bn_pcimg          = '$bn_pcimg',";
    if ($mimg_name)
        $sql_img_add .= "bn_mimg           = '$bn_mimg',";


    /*************** 트랜잭션 관련 ****************/
    $error_cnt = 0;
    mysqli_autocommit($g5['connect_db'], false);
    /*************** 트랜잭션 관련 ****************/

    $sql = " update tbl_shop_banner_trend
                set 
                    bn_event_id       = '$bn_event_id',
                    bn_event_name     = '$bn_event_name',
                    ".$sql_img_add."
                    bn_order          = '$num',
                    bn_use            = '$bn_use',
                    bn_udate          = NOW()
              where bn_id = '$bn_id' ";
    sql_query($sql);

    @mkdir(G5_DATA_PATH."/banner_trend", G5_DIR_PERMISSION);
    @chmod(G5_DATA_PATH."/banner_trend", G5_DIR_PERMISSION);

    /**** 데이터베이스 등록 문제 없는 경우 이미지 등록 ****/
    if ($tabimg_name)
        upload_file($tabimg_tmp, 'tab_'.$bn_id_ver,G5_DATA_PATH."/banner_trend");
    if ($pcimg_name)
        upload_file($pcimg_tmp, 'pc_'.$bn_id_ver,G5_DATA_PATH."/banner_trend");
    if ($mimg_name)
        upload_file($mimg_tmp, 'm_'.$bn_id_ver,G5_DATA_PATH."/banner_trend");

    /*************** 트랜잭션 관련 ****************/
    if (mysqli_errno($g5['connect_db'])) {
        $error_cnt += 1;
    }
    /*************** 트랜잭션 관련 ****************/
}

$bn_url = clean_xss_tags($bn_url);

/*************** 트랜잭션 관련 ****************/
if ($error_cnt > 0) {
    mysqli_rollback($g5['connect_db']);
    mysqli_close($g5['connect_db']);
    echo "
	<script>
		alert('데이터베이스의 에러로 인해 롤백되었습니다.');
		history.back();
	</script>
	";
    exit;
} else {
    mysqli_commit($g5['connect_db']);
}
/*************** 트랜잭션 관련 ****************/

goto_url("./trendbannerform.php")
?>

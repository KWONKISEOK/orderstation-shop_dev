<?php
$sub_menu = '400407';
include_once('./_common.php');

auth_check($auth[$sub_menu], "r");

/*************** 트랜잭션 관련 ****************/
$error_cnt = 0;
mysqli_autocommit($g5['connect_db'], false);
/*************** 트랜잭션 관련 ****************/

if ($act == "alldelete") // 모두 삭제이면
{
    $sql = " delete from tbl_shop_cart
              where ct_status='사내' and mb_id='{$member['mb_id']}' ";
    sql_query($sql);
	/*************** 트랜잭션 관련 ****************/
	if( mysqli_errno($g5['connect_db']) ){
		$error_cnt += 1;
	}
	/*************** 트랜잭션 관련 ****************/
}
else if ($act == "seldelete") // 선택삭제
{

	for ($i=0; $i<count($_POST['chk']); $i++)
	{
		// 실제 번호를 넘김
		$k = $_POST['chk'][$i];
		$sql = " delete from tbl_shop_cart where it_id = '{$_POST['it_id'][$k]}' and ct_status='사내' and mb_id='{$member['mb_id']}' ";
		sql_query($sql);
		/*************** 트랜잭션 관련 ****************/
		if( mysqli_errno($g5['connect_db']) ){
			$error_cnt += 1;
		}
		/*************** 트랜잭션 관련 ****************/

	}

}

/*************** 트랜잭션 관련 ****************/
if ($error_cnt > 0) {
	mysqli_rollback($g5['connect_db']);
	mysqli_close($g5['connect_db']);
	echo "
	<script>
		alert('데이터베이스의 에러로 인해 롤백되었습니다.');
		history.back();
	</script>
	";
	exit;
} else {
	mysqli_commit($g5['connect_db']);
} 	
/*************** 트랜잭션 관련 ****************/

goto_url('/adm/shop_admin/orderlistp2.php');

?>
<?php
$sub_menu = '400400';
include_once('./_common.php');

auth_check($auth[$sub_menu], "w");

check_admin_token();

$ct_chk_count = count($_POST['trans_chk']);
if(!$ct_chk_count)
    alert('처리할 자료를 하나 이상 선택해 주십시오.');


$mod_history = '';
$cnt = count($_POST['od_num']);
//$arr_it_id = array();


$change_status = $od_status;

/*************** 트랜잭션 관련 ****************/
$error_cnt = 0;
mysqli_autocommit($g5['connect_db'], false);
/*************** 트랜잭션 관련 ****************/

for ($i=0; $i<$ct_chk_count; $i++)
{
	$k = $_POST['trans_chk'][$i];
	$od_id = $_POST['od_id'];
	$od_num = $_POST['od_num'][$k];
	$od_seq = $_POST['od_seq'][$k];
	$invoice			= $_POST['od_invoice'][$k];
	$delivery_company	= $_POST['od_delivery_company'][$k];

	$delivery['invoice'] = $invoice;
	$delivery['delivery_company'] = $delivery_company;

	$od_b_name         =  $_POST['od_b_name'][$i];

	$od_b_tel          =  $_POST['od_b_tel'][$i];
	$od_b_hp           =  $_POST['od_b_hp'][$i];
	$zip         =  $_POST['od_b_zip'][$i];
	$zip1 = substr($zip , 0, 3) ;
	$zip2 = str_replace('-','',substr($zip ,  3));

	$od_b_zip1         =  $zip1;
	$od_b_zip2         =  $zip2;
	$od_b_addr1        =  $_POST['od_b_addr1'][$i];
	$od_b_addr2        =  $_POST['od_b_addr2'][$i];
	$od_b_addr3        =  $_POST['od_b_addr3'][$i];
	$od_trans_memo     =  $_POST['od_trans_memo'][$i];

	switch ($change_status)
    {        		
        case '배송' :
			//결제마스터 확인
			$sql = " select a.*, b.it_name, od_period_yn,od_period_cnt, b.od_total_sale_price from {$g5['g5_shop_order_table']} a, {$g5['g5_shop_order_detail_table']} b 
					  where a.od_id = b.od_id and b.od_num = '$od_num' and a.od_id =  '{$od_id}' ";
	
			$order = sql_fetch($sql);

			//총주문금액
			$od_total_sale_price  = $order[od_total_sale_price];
			$billkey		= $order[od_billkey];

			$od_settle_case1		= $order[od_settle_case];
			$od_period_yn		= $order[od_period_yn];
			$od_period_cnt		= $order[od_period_cnt];
			$goodname           = $order[it_name];
			$buyername			= $order[od_name];
			$buyertel			= $order[od_hp];	
			$buyeremail			= $order[od_email];
			$service_type		= $order[service_type];
			$od_coupon			= $order[od_coupon]; //쿠폰사용금액
			$get_mb_id			= $order[mb_id]; //주문자 아이디
		
			$od_multi_yn		= $order[od_multi_yn]; // 다중배송 여부
			$od_bill_tno = '';
			$res_cd = '00';




			//신용카드결제 빌링처리
//			if($od_period_yn == 'Y' and $billkey !='' and $od_pay_yn1=='N') {
//
//
//				//include_once(G5_SHOP_PATH.'/settle_inicis.inc.php');
//				$cancel_msg = iconv_euckr('쇼핑몰 운영자 빌링 승인요청');
//
//				//결제금액(총 주문금액 / 횟수)
//				//$pay_amount =  $od_cart_price/$od_period_cnt;
//				$pay_amount =  (int)( ($od_total_sale_price-$od_coupon) / $od_period_cnt );
//
//				/* * ************************
//				 * 1. 라이브러리 인클루드 *
//				 * ************************ */
//				 require_once(G5_SHOP_PATH.'/inicis/libs/INIpay41Lib.php');
//
//				/* * *************************************
//				 * 2. INIpay41 클래스의 인스턴스 생성 *
//				 * ************************************* */
//				$inipay = new INIpay41;
//
//				/* * *********************
//				 * 2. 정보 설정 *
//				 * 승인번호 : INIpayBillorder0000120180911135907287159(최채영 1000원 승인)
//				 * ********************* */
//				if( $service_type == "B" ){
//					$where_query = " and service_type = 'B' ";
//					$add_column = " ,service_type ";
//					$add_value = " ,'B' ";
//					$mid = 'order00002';
//					$m_url = "https://safetystation.co.kr";
//				}else{
//					$where_query = "";
//					$add_column = "";
//					$add_value = "";
//					$mid = 'order00001';
//					$m_url = "https://www.orderstation.co.kr";
//				}
//				$inipay->m_inipayHome = "/var/www/html/orderstation-shop/shop/inicis/";      // INIpay Home (절대경로로 적절히 수정)
//				$inipay->m_keyPw = "1111";        // 키패스워드(상점아이디에 따라 변경)
//				$inipay->m_type = "reqrealbill";       // 고정 (절대 수정금지)
//				$inipay->m_pgId = "INIpayBill";       // 고정 (절대 수정금지)
//				$inipay->m_payMethod = "Card";           // 고정 (절대 수정금지)
//				$inipay->m_billtype = "Card";              // 고정 (절대 수정금지)
//				$inipay->m_subPgIp = "203.238.3.10";       // 고정 (절대 수정금지)
//				$inipay->m_debug = "true";        // 로그모드("true"로 설정하면 상세한 로그가 생성됨)
//				$inipay->m_mid = $mid;         // 상점아이디
//				$inipay->m_billKey = $billkey;        // billkey 입력
//				$inipay->m_goodName = iconv("UTF-8","EUC-KR", $goodname);       // 상품명 (최대 40자)
//				$inipay->m_currency = 'WON';       // 화폐단위 
//				$inipay->m_price = $pay_amount;        // 가격 
//				$inipay->m_buyerName = iconv("UTF-8","EUC-KR", $buyername);       // 구매자 (최대 15자) 
//				$inipay->m_buyerTel = $buyertel;       // 구매자이동전화 
//				$inipay->m_buyerEmail = $buyeremail;       // 구매자이메일
//				$inipay->m_cardQuota = '00';       // 할부기간
//				$inipay->m_quotaInterest = 0;      // 무이자 할부 여부 (1:YES, 0:NO)
//				$inipay->m_url = $m_url;    // 상점 인터넷 주소
//				//$inipay->m_cardPass = $cardpass;       // 키드 비번(앞 2자리)
//				//$inipay->m_regNumber = $regnumber;       // 주민 번호 및 사업자 번호 입력
//				$inipay->m_authentification = '01'; //( 신용카드 빌링 관련 공인 인증서로 인증을 받은 경우 고정값 "01"로 세팅)  
//				$inipay->m_oid = $oid;        //주문번호
//				$inipay->m_merchantreserved1 = $MerchantReserved1;  // Tax : 부가세 , TaxFree : 면세 (예 : Tax=10&TaxFree=10) 
//				$inipay->m_merchantreserved2 = $MerchantReserved2;  // 예비2
//				$inipay->m_merchantreserved3 = $MerchantReserved3;  // 예비3
//				
//				//print_r($inipay);
//				//exit;
//
//				/* * ******************************
//				 * 3. 실시간 신용카드 빌링 요청 *
//				 * ****************************** */
//				$inipay->startAction();
//				/* * **********************************************************
//				 * 4. 실시간 신용카드 빌링 결과                             *
//				 * ***********************************************************
//				 *                                                          *
//				 * $inipay->m_tid 	  // 거래번호                       *
//				 * $inipay->m_resultCode  // "00"이면 성공                  *
//				 * $inipay->m_resultMsg   // 결과에 대한 설명               *
//				 * $inipay->m_authCode    // 승인번호                       *
//				 * $inipay->m_pgAuthDate  // 이니시스 승인날짜 (YYYYMMDD)   *
//				 * $inipay->m_pgAuthTime  // 이니시스 승인시간 (HHMMSS)     *
//				 * $inipay->m_prtcCode		// 부분취소가능여부 (1:가능 , 0:불가능)	*
//				 *                                                          *
//				 * ********************************************************** */
//				$res_cd  = $inipay->m_resultCode;
//				$res_msg = $inipay->m_resultMsg;
//				$od_bill_tno = $inipay->m_tid ;
//				$get_cardCode = $inipay->m_cardCode ;
//				$get_cardNumber = $inipay->m_cardNumber ;
//
//				if($res_cd == '00') {
//
//					$get_od_billkey = $billkey;
//
//					//정기주문 카드정보 저장해주기
//					if( !empty($get_mb_id) && !empty($get_cardCode) && !empty($get_cardNumber) && !empty($get_od_billkey) ){
//
//						$sql = " select count(m_idx) as m_cnt from tbl_bill_info where mb_id = '".$get_mb_id."' 
//								 and cardCode = '".$get_cardCode."' and cardNumber = '".$get_cardNumber."' ".$where_query."
//							   ";
//						$bill_info = sql_fetch($sql);
//
//						if( $bill_info["m_cnt"] == 0 ){
//							
//							$sql = " insert into tbl_bill_info( mb_id , cardCode , cardNumber , od_billkey , regDate ".$add_column." ) 
//									values( '".$get_mb_id."','".$get_cardCode."','".$get_cardNumber."','".$get_od_billkey."',now() ".$add_value." )
//								   ";
//							sql_query($sql, true);
//							/*************** 트랜잭션 관련 ****************/
//							if( mysqli_errno($g5['connect_db']) ){
//								$error_cnt += 1;
//							}
//							/*************** 트랜잭션 관련 ****************/
//
//						}
//
//					}
//
//					//승인정보 저장
//					 $sql = " update tbl_shop_order_receiver set od_pay_price = '$pay_amount', od_bill_tno = '$od_bill_tno', od_pay_yn = 'Y', od_pay_date = '".G5_TIME_YMDHIS."' , od_bill_tno = '$od_bill_tno'
//					 where od_id = '$od_id1' and  od_num = '$od_num1' and  od_seq = '$od_seq1' ";
//
//					 sql_query($sql, true);
//					/*************** 트랜잭션 관련 ****************/
//					if( mysqli_errno($g5['connect_db']) ){
//						$error_cnt += 1;
//					}
//					/*************** 트랜잭션 관련 ****************/
//					
//				} else {
//					$pg_res_cd = $res_cd;
//					$pg_res_msg = iconv_utf8($res_msg);
//
//					//오류메시지
//					$url = "./orderform.php?od_id=$od_id&amp;$qstr";
//					alert('오류코드 : '.$pg_res_cd.' 오류내용 : '.$pg_res_msg, $url);
//				}
//
//			}
			if($res_cd == '00') {
				//배송정보 업데이트


				if($od_multi_yn != "Y"){
					order_update_delivery($od_id, $od_num, $od_seq, $delivery);
					//$sql = " update tbl_shop_order_receiver set od_delivery_company = '{$delivery['delivery_company']}', od_invoice = '{$delivery['invoice']}' where od_id = '$od_id' and od_num = '$od_num'  and od_seq = '$od_seq'";
					// sql_query($sql);
				}else {
					$cnt = count($_POST['od_b_name']);
					for ($k = 0; $k < $cnt; $k++) {
						$od_num = $_POST['od_num'][$k];
						$od_seq = $_POST['od_seq'][$k];
						$invoice = $_POST['od_invoice'][$k];
						$delivery_company = $_POST['od_delivery_company'][$k];
						$delivery['invoice'] = $invoice;
						$delivery['delivery_company'] = $delivery_company;
						order_update_delivery($od_id, $od_num, $od_seq, $delivery);

					//	$sql = " update tbl_shop_order_multi_receiver set od_delivery_company = '{$delivery['delivery_company']}', od_invoice = '{$delivery['invoice']}' where od_id = '$od_id' and od_num = '$od_num'  and od_seq = '$od_seq'";
					}
				}
				if($od_multi_yn != "Y") {
					//결제여부확인, 배송지 확인
					$sql = " select * from tbl_shop_order_receiver where od_id =  '$od_id' and od_num = '$od_num' and  od_seq = '$od_seq' ";
				} else{
					$sql = " select * from tbl_shop_order_receiver where od_id =  '$od_id' and od_num = '$od_num'";
				}


				$receiver = sql_fetch($sql);

				$od_b_addr1n			= $receiver[od_b_addr1];
				$od_pay_yn1          = $receiver[od_pay_yn];
				$od_delivery_company = $receiver[od_delivery_company];
				$od_invoice = $receiver[od_invoice];

				$od_id1 = $od_id;
				$od_num1 = $od_num;
				$od_seq1 = $od_seq;

				if($od_multi_yn != "Y") {
					//상태정보업데이트
					$sql = " update tbl_shop_order_receiver set od_status = '배송', od_date3 = '".G5_TIME_YMDHIS."' where od_id = '$od_id1' and  od_num = '{$od_num1}' and  od_seq = '{$od_seq1}' ";
				} else {
					$sql = " update tbl_shop_order_receiver set od_status = '배송', od_date3 = '".G5_TIME_YMDHIS."' where od_id = '$od_id1' and  od_num = '{$od_num1}'";
				}
				sql_query($sql);
				/*************** 트랜잭션 관련 ****************/
				if( mysqli_errno($g5['connect_db']) ){
					$error_cnt += 1;
				}
				/*************** 트랜잭션 관련 ****************/

                /************* 관리자 로그 처리 START *************/
                insert_admin_log(400,400400, '주문 상태 변경 - 배송', '', $od_id1, $od_seq1, $_SERVER['REQUEST_URI']);
                /************* 관리자 로그 처리 END *************/
				
				//알림톡
				include_once(G5_LIB_PATH.'/kakao.alimtalk.lib.php');
				$recv_number = preg_replace('/[^0-9]/', '', $buyertel);

				//이즈브레앱 주문일때 
				if( $service_type == "B" ){
					$alim_id = "isbre";
					$template_code = 'isbre_004';
				}else if($od_multi_yn != "Y"){ // 일반주문
					$alim_id = "orderstation";
					$template_code = 'os_order_003';
				}else{ // 다중배송
					$alim_id = "orderstation";
					$template_code = 'os_order_003_1';
				}

				// 주문일시, 주문상품, 입금요청액
				$sql = " SELECT a.od_time, a.od_hp, a.od_b_name, a.od_misu, b.it_name from tbl_shop_order a, tbl_shop_order_detail b WHERE a.od_id = b.od_id AND a.od_id = '$od_id'";
				$count = " SELECT count(*) as count from tbl_shop_order a, tbl_shop_order_detail b WHERE a.od_id = b.od_id AND a.od_id = '$od_id'";
				$m_row = sql_fetch($sql);
				$c_row = sql_fetch($count);

					if ($recv_number) {
						// AlimTalk BEGIN --------------------------------------------------------
						$alimtalk = new AlimTalk($alim_id);
						$to = $alimtalk->set_phone_number($recv_number);
						$text = $alimtalk->get_template($template_code);
						$text = str_replace('#{이름}', $buyername, $text);
						$text = str_replace('#{주문번호}', $od_id1, $text);
						if($c_row['count']==1){
							$text = str_replace('#{주문상품}', $m_row['it_name'], $text);
						} else{
							$text = str_replace('#{주문상품}', $m_row['it_name'].' 외 '.($c_row['count'] -1).'건', $text);
						}

						if($od_multi_yn != "Y"){
							$text = str_replace('#{택배사}', $od_delivery_company, $text);
							$text = str_replace('#{송장번호}', $od_invoice, $text);
						} else {
							$text = str_replace('#{수령인}', $od_b_name.' 외 '.($cnt -1).'명', $text);
						}
						$alimtalk->set_message($template_code, $to, $text);
						$alimtalk->send();
						// AlimTalk END   --------------------------------------------------------
					}
			}

		    break;
		case '완료' :
			 $sql = " update tbl_shop_order_receiver set od_status = '{$change_status}', od_date4 = '".G5_TIME_YMDHIS."' where od_id = '{$od_id}' and  od_num = '{$od_num}' and  od_seq = '{$od_seq}' ";
			sql_query($sql, true);
			/*************** 트랜잭션 관련 ****************/
			if( mysqli_errno($g5['connect_db']) ){
				$error_cnt += 1;
			}
			/*************** 트랜잭션 관련 ****************/

            /************* 관리자 로그 처리 START *************/
            insert_admin_log(400,400400, '주문 상태 변경 - 완료', '', $od_id, $od_seq, $_SERVER['REQUEST_URI']);
            /************* 관리자 로그 처리 END *************/

			//재고처리 로직은 나중에..

            // 완료인 경우에만 상품구입 합계수량을 상품테이블에 저장한다.
            //$sql2 = " select it_id from {$g5['g5_shop_order_detail_table']} where od_id = '$od_id' and ct_status = '완료' group by it_id ";
            //$result2 = sql_query($sql2);
            //for ($k=0; $row2=sql_fetch_array($result2); $k++) {
                $sql3 = " select sum(od_qty) as sum_qty from {$g5['g5_shop_order_receiver_table']} where od_id = '$od_id' and od_num = '$od_num' and od_seq = '$od_seq'  ";
                $row3 = sql_fetch($sql3);

                $sql4 = " update {$g5['g5_shop_item_table']} set it_sum_qty = '{$row3['sum_qty']}' where it_id = '$it_id' ";
                sql_query($sql4);
				/*************** 트랜잭션 관련 ****************/
				if( mysqli_errno($g5['connect_db']) ){
					$error_cnt += 1;
				}
				/*************** 트랜잭션 관련 ****************/
           // }
            break;
		case '수정내역저장' :	

			//다중배송주문이 아닐경우
			if($od_multi_yn != "Y"){

				//배송정보저장
				order_update_delivery($od_id, $od_num, $od_seq, $change_status, $delivery);

				$sql_b = " select * from tbl_shop_order_receiver where od_id = '$od_id' and od_num = '$od_num' and od_seq = '$od_seq' ";
				$al_data_backup = sql_fetch($sql_b);

				$sql = " update tbl_shop_order_receiver
				set  od_b_name         = '$od_b_name',
						od_b_tel          = '$od_b_tel',
						od_b_hp           = '$od_b_hp',
						od_b_zip1         = '$od_b_zip1',
						od_b_zip2         = '$od_b_zip2',
						od_b_addr1        = '$od_b_addr1',
						od_b_addr2        = '$od_b_addr2',
						od_b_addr3        = '$od_b_addr3',
						od_trans_memo     = '$od_trans_memo',
 					   od_delivery_company = '{$delivery['delivery_company']}', 
				    	od_invoice = '{$delivery['invoice']}'
				 where od_id = '$od_id' and od_num = '$od_num' and od_seq = '$od_seq'  ";

				sql_query($sql, true);
				/*************** 트랜잭션 관련 ****************/
				if( mysqli_errno($g5['connect_db']) ){
					$error_cnt += 1;
				}
				/*************** 트랜잭션 관련 ****************/

                /************* 관리자 로그 처리 START *************/
                insert_admin_log(400,400400, '배송정보 수정내역 저장', '', $od_id, $od_seq, $_SERVER['REQUEST_URI'], $_POST, $al_data_backup);
                /************* 관리자 로그 처리 END *************/

			}else if($od_multi_yn == "Y"){

				$cnt = count($_POST['od_b_name']);
			
				for ($k=0; $k<$cnt; $k++)
				{
					$od_num				= $_POST['od_num'][$k];
					$od_seq				= $_POST['od_seq'][$k];
					$invoice			= $_POST['od_invoice'][$k];
					$delivery_company	= $_POST['od_delivery_company'][$k];

					$od_b_name			= $_POST['od_b_name'][$k];
					$od_b_tel			= $_POST['od_b_tel'][$k];
					$od_b_hp			= $_POST['od_b_hp'][$k];

					$zip				= $_POST['od_b_zip'][$k];
					$zip1				= substr($zip , 0, 3) ;
					$zip2				= str_replace('-','',substr($zip ,  3));

					$od_b_zip1			= $zip1;
					$od_b_zip2			= $zip2;
					$od_b_addr1			= $_POST['od_b_addr1'][$k];
					$od_b_addr2			= $_POST['od_b_addr2'][$k];
					$od_b_addr3			= $_POST['od_b_addr3'][$k];
					$od_trans_memo		= $_POST['od_trans_memo'][$k];

					$add_query = "";

					if( !empty($delivery_company) && !empty($invoice) ){
						$add_query = ",
							od_delivery_company = '$delivery_company', 
							od_invoice			= '$invoice'						
						";
					}

                    $sql_b = " select * from tbl_shop_order_multi_receiver where od_id = '$od_id' and od_num = '$od_num' and od_seq = '$od_seq' ";
                    $al_data_backup = sql_fetch($sql_b);

					$sql = " update tbl_shop_order_multi_receiver
						set od_b_name			= '$od_b_name',
							od_b_tel			= '$od_b_tel',
							od_b_hp				= '$od_b_hp',
							od_b_zip1			= '$od_b_zip1',
							od_b_zip2			= '$od_b_zip2',
							od_b_addr1			= '$od_b_addr1',
							od_b_addr2			= '$od_b_addr2',
							od_b_addr3			= '$od_b_addr3',
							od_trans_memo		= '$od_trans_memo'
							".$add_query."
					where od_id = '$od_id' and od_num = '$od_num' and od_seq = '$od_seq'  ";

					sql_query($sql, true);
					/*************** 트랜잭션 관련 ****************/
					if( mysqli_errno($g5['connect_db']) ){
						$error_cnt += 1;
					}
					/*************** 트랜잭션 관련 ****************/

                    /************* 관리자 로그 처리 START *************/
                    insert_admin_log(400,400400, '배송정보 수정내역 저장', '', $od_id, $od_seq, $_SERVER['REQUEST_URI'], $_POST, $al_data_backup);
                    /************* 관리자 로그 처리 END *************/

				}

			}

            break;
		default:
    
    } // switch end
}

/*************** 트랜잭션 관련 ****************/
if ($error_cnt > 0) {
	mysqli_rollback($g5['connect_db']);
	mysqli_close($g5['connect_db']);
	echo "
	<script>
		alert('데이터베이스의 에러로 인해 롤백되었습니다.');
		history.back();
	</script>
	";
	exit;
} else {
	mysqli_commit($g5['connect_db']);
} 	
/*************** 트랜잭션 관련 ****************/

$qstr = "sort1=$sort1&amp;sort2=$sort2&amp;sel_field=$sel_field&amp;search=$search&amp;page=$page";

$url = "./orderform.php?od_id=$od_id&amp;$qstr";

goto_url($url);

?>

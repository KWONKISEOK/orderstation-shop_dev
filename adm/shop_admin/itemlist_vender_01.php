<?php
include_once('./_common.php');

$mode	= $_POST['mode'];

$where = " and ";
$sql_search = "";
if ($stx != "") {
    if ($sfl != "") {
        $sql_search .= " $where $sfl like '%$stx%' ";
        $where = " and ";
    }
    if ($save_stx != $stx)
        $page = 1;
}

if ($sca != "") {
    $sql_search .= " $where (a.ca_id like '$sca%' or a.ca_id2 like '$sca%' or a.ca_id3 like '$sca%') ";
}

if ($sfl == "")  $sfl = "it_name";

$sql_common = " from {$g5['g5_shop_item_table']} a 
                     left join {$g5['g5_shop_category_table']} b on b.ca_id = a.ca_id
					 left join tbl_member c on c.comp_code = a.comp_code
               where c.mb_type = 7";

if ($comp_code != '')
$sql_common .= " and a.comp_code = '$comp_code' ";

if ($it_use != '')
$sql_common .= " and a.it_use ='$it_use' ";

$sql_common .= $sql_search;


if (!$sst) {
    $sst  = "it_time";
    $sod = "desc";
}
$sql_order = "order by $sst $sod";

switch($mode){
	
	case 'excel':
		$ExcelTitle = urlencode( "상품리스트" . date( "YmdHis" ) );
		header( "Content-type: application/vnd.ms-excel" );
		header( "Content-type: application/vnd.ms-excel; charset=utf-8" );
		header( "Content-Disposition: attachment; filename = " . $ExcelTitle . ".xls" );
		header( "Content-Description: PHP4 Generated Data" );
		print( "<meta http-equiv=\"Content-Type\" content=\"application/vnd.ms-excel; charset=utf-8\">" ); ?>
		<table border="1">
		<tr bgcolor="F7F7F7" height="30">
			  <th>오더스테이션 상품코드</th>
			  <th>공급사 상품코드</th>
			  <th>1차 카테고리</th>
			  <th>2차 카테고리</th>
			  <th>상품명</th>
			  <th>공급업체</th>
			  <th>규격</th>
			  <th>제조사/제조국</th>
			  <th>원산지</th>
			  <th>과세</th>
			  <th>공급가</th>
			  <th>소비자가</th>
			  <th>검색어</th>
			  <th>재고량</th>
			  <th>최소주문수량</th>
			  <th>최대주문수량</th>
			  <th>판매여부</th>
			  <th>등록일자</th>
			  <th>배송비</th>
			  <th>묶음배송</th>
			  <th>마감시간</th>
			  <th>출고일</th>
			  <th>정기주문가능여부</th>
		</tr>
		<?php		
		$sql  = " select *, (select ca_name from tbl_shop_category where ca_id = a.ca_id2) ca_name2
           $sql_common
           $sql_order ";

		$res = sql_query($sql);
		if(!$res) exit('Cannot run query.');

		$i = 0;
		while($row = mysqli_fetch_array($res,MYSQL_ASSOC)) {

			$it_id       = $row["it_id"];
		
			switch ( $row["it_notax"] ) {
				case "0" :
					$it_notax = "과세";
					break;
				case "1" :
					$it_notax = "비과세";
					break;
				case "2" :
					$it_notax = "면세";
					break;
			}
			switch ( $row["it_sc_type"] ) {
				case "1" :
					$it_sc_type = "무료배송";
					break;
				case "2" :
					$it_sc_type = "조건부 무료배송";
					break;
				case "3" :
					$it_sc_type = "유료배송";
					break;
			}
			switch ( $row["it_trans_bundle"] ) {
				case "1" :
					$it_trans_bundle = "<font color='blue'>묶음배송</font>";
					break;
				case "0" :
					$it_trans_bundle = "개별배송";
					break;
			}
			switch ( $row["it_period_yn"] ) {
				case "Y" :
					$it_period_yn = "가능";
					break;
				case "N" :
					$it_period_yn = "불가능";
					break;
			}
			switch ( $row["it_use"] ) {
				case "1" :
					$it_use = "<font color='blue'>판매</font>";
					break;
				case "0" :
					$it_use = "<font color='red'>사용안함</font>";
					break;
			}
			?>
			<tr bgcolor="">
				<td style="mso-number-format:'\@';"><?php echo $row["it_id"];?></td>
				<td style="mso-number-format:'\@';"><?php echo $row["comp_it_id"];?></td>
				<td><?php echo $row["ca_name"];?></td>
				<td><?php echo $row["ca_name2"];?></td>
				<td><?php echo $row["it_name"];?></td>
				<td><?php echo $row["comp_name"];?></td>
				<td><?php echo $row["it_model"];?></td>
				<td><?php echo $row["it_maker"];?></td>
				<td><?php echo $row["it_origin"];?></td>
				<td><?php echo $it_notax;?></td>
				<td><?php echo $row["it_supply_price"];?></td>
				<td><?php echo $row["it_price"];?></td>
				<td><?php echo $row["it_search"];?></td>
				<td><?php echo $row["it_stock_qty"];?></td>
				<td><?php echo $row["it_buy_min_qty"];?></td>
				<td><?php echo $row["it_buy_max_qty"];?></td>
				<td><?php echo $it_use;?></td>
				<td><?php echo $row["it_time"];?></td>
				<td><?php echo $it_sc_type;?></td>
				<td><?php echo $it_trans_bundle;?></td>
				<td><?php echo $row["it_order_close_time"];?></td>
				<td><?php echo $row["it_release_day"];?></td>
				<td><?php echo $it_period_yn;?></td>
			</tr>
		<?php			
		} 
		break;
	
}



	

?>
<?php
$sub_menu = '400800';
include_once('./_common.php');

auth_check($auth[$sub_menu], "r");

$g5['title'] = '쿠폰관리 로그인';
include_once (G5_ADMIN_PATH.'/admin.head.php');
?>

<div class="tbl_head01 tbl_wrap">

<form name="fcoupon_login" method="post" action="./coupon_login_ok.php">

<table width="100%" height="300">
<tr>
	<td height="100%">
		비밀번호 : <input type="password" name="coupon_pwd" class="frm_input"/ > 
		<input type="button" value="로그인" class="get_theme_confc btn btn_01" onclick="javascript:C_login();">
	</td>
</tr>
</table>

</form>

</div>

<script>

	function C_login(){
		var frm = document.fcoupon_login;

		if (frm.coupon_pwd.value == "" ){
			alert("비밀번호를 입력해주세요");
			return;
		}

		frm.submit();
		
	}

</script>

<?php
include_once (G5_ADMIN_PATH.'/admin.tail.php');
?>
<?php
$sub_menu = '400407';
include_once('./_common.php');

auth_check($auth[$sub_menu], "r");

$g5['title'] = '장바구니';
include_once (G5_ADMIN_PATH.'/admin.head.php');

if( $member['mb_id'] != "admin" ){
	echo "최고 관리자만 접속 가능한 메뉴입니다.";
	exit;
}

$sql  = " select * from tbl_shop_cart 
           where ct_status='사내' and mb_id='{$member['mb_id']}'";
$result = sql_query($sql);
//echo $sql;


//$qstr  = $qstr.'&amp;sca='.$sca.'&amp;page='.$page;
$qstr  = $qstr.'&amp;comp_code='.$comp_code.'&amp;sca='.$sca.'&amp;page='.$page.'&amp;save_stx='.$stx;

$listall = '<a href="'.$_SERVER['SCRIPT_NAME'].'" class="ov_listall">전체목록</a>';
?>

<div class="local_ov01 local_ov">
    
	<button type="button" class='btn' onclick="return form_check('seldelete');">선택삭제</button>
	<button type="button" class='btn' onclick="return form_check('alldelete');">비우기</button>


</div>


<form name="fitemlistupdate" method="post" action="./orderlistp2_update.php" onsubmit="return fitemlist_submit(this);" autocomplete="off" id="fitemlistupdate">
<input type="hidden" name="sca" value="<?php echo $sca; ?>">
<input type="hidden" name="sst" value="<?php echo $sst; ?>">
<input type="hidden" name="sod" value="<?php echo $sod; ?>">
<input type="hidden" name="sfl" value="<?php echo $sfl; ?>">
<input type="hidden" name="stx" value="<?php echo $stx; ?>">
<input type="hidden" name="page" value="<?php echo $page; ?>">
<input type="hidden" name="act" value="">

<div class="tbl_head01 tbl_wrap">
    <table>
    <caption><?php echo $g5['title']; ?> 목록</caption>
    <thead>
    <tr>
        <th scope="col">
            <label for="chkall" class="sound_only">상품 전체</label>
            <input type="checkbox" name="chkall" value="1" id="chkall" onclick="check_all(this.form)">
        </th>
        <th scope="col">상품코드</th>
        <th scope="col" id="th_img">이미지</th>
        <th scope="col" id="th_pc_title">상품명</th>
		<th scope="col" id="th_qty">수량</th>
        <th scope="col" id="th_amt">판매가</th>
		 <th scope="col" >배송</th>
     
    </tr>
    </thead>
    <tbody>
    <?php
    for ($i=0; $row=sql_fetch_array($result); $i++)
    {
        $href = G5_SHOP_URL.'/item.php?it_id='.$row['it_id'];
        $bg = 'bg'.($i%2);


    ?>
    <tr class="<?php echo $bg; ?>">
        <td class="td_chk">
            <label for="chk_<?php echo $i; ?>" class="sound_only"><?php echo get_text($row['it_name']); ?></label>
            <input type="checkbox" name="chk[]" value="<?php echo $i; ?>" id="chk_<?php echo $i; ?>">
        </td>
        <td class="td_num">
            <input type="hidden" name="it_id[<?php echo $i; ?>]" value="<?php echo $row['it_id']; ?>">
            <?php echo $row['it_id']; ?>
        </td>
      
        <td class="td_img"><a href="<?php echo $href; ?>"><?php echo get_it_image($row['it_id'], 50, 50); ?></a></td>
        <td headers="th_pc_title" style="text-align:left;">           
            <?php echo htmlspecialchars2(cut_str($row['it_name'],250, "")); ?>
        </td>
		<td>            
            <input type="text" name="ct_qty[<?php echo $i; ?>]" value="<?php echo $row['ct_qty']; ?>" size=10>
        </td>
        <td headers="th_amt" class="td_numbig td_input">
            <?php echo number_format($row['ct_price']); ?><br>
        </td>
		<td>
            
            무료배송


        </td>
    
    </tr>
    <?php
    }?>
	<tr>
	  <td colspan="8" align="left">
	  	
		<div class="local_cmd01 ">
		
		<a href="orderlistp1.php" class="btn btn_02"> 계속 쇼핑하기</a>
		<!-- <input type="submit" name="act_button" value="선택주문" onclick="document.pressed=this.value" class="btn btn_02"> -->
		<a href="orderlistp4.php" class="btn btn_01"> 주문하기</a>
		<!-- <button type="button" onclick="return form_check('buy');" class="btn_submit"><i class="fa fa-credit-card" aria-hidden="true"></i> 주문하기</button> -->

		</div>

		</td>
		

	</tr>
	<?php
    if ($i == 0)
        echo '<tr><td colspan="12" class="empty_table">자료가 한건도 없습니다.</td></tr>';
    ?>
    </tbody>
    </table>
</div>

<div class="btn_fixed_top">

	<a href="orderlistp1.php" class="btn btn_01"> 상품목록 이동</a>
	<a href="orderlistp3.php" class="btn btn_02"> 주문내역 이동</a>
   
</div>

</form>


<script>

function form_check(act) {
    var f = document.fitemlistupdate;
    //var cnt = f.records.value;

    if (act == "buy")
    {
        if($("input[name^=chk]:checked").length < 1) {
            alert("주문하실 상품을 하나이상 선택해 주십시오.");
            return false;
        }
		<?php if($member['mb_type'] != 1 and $period=='y') { ?>
		if($("input[name^=chk]:checked").length > 1) {
            alert("정기주문은 한상품씩 주문가능합니다.");
            return false;
        }
		<?php } ?>

        f.act.value = act;
        f.submit();
    }
    else if (act == "alldelete")
    {
        f.act.value = act;
        f.submit();
    }
    else if (act == "seldelete")
    {
        if($("input[name^=chk]:checked").length < 1) {
            alert("삭제하실 상품을 하나이상 선택해 주십시오.");
            return false;
        }

        f.act.value = act;
        f.submit();
    }

    return true;
}


</script>

<?php
include_once (G5_ADMIN_PATH.'/admin.tail.php');
?>

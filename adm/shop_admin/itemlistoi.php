<?php

include_once('./_common.php');

include_once (G5_ADMIN_PATH.'/admin.head.nomenu.php');


$where = " and ";
$sql_search = "";
if ($stx != "") {
    if ($sfl != "") {
        $sql_search .= " $where $sfl like '%$stx%' ";
        $where = " and ";
    }
    if ($save_stx != $stx)
        $page = 1;
}


if ($sfl == "")  $sfl = "it_name";

$sql_common = " from {$g5['g5_shop_item_table']} a ,
					 tbl_member c
               where a.comp_code = c.comp_code and it_option = 1 and it_id not in (select io_id from tbl_shop_item_option) ";


if ($comp_code != '')
$sql_common .= " and a.comp_code = '{$comp_code}' ";


$sql_common .= $sql_search;

// 테이블의 전체 레코드수만 얻음
$sql = " select count(*) as cnt " . $sql_common;
$row = sql_fetch($sql);
$total_count = $row['cnt'];

$rows = $config['cf_page_rows'];
$total_page  = ceil($total_count / $rows);  // 전체 페이지 계산
if ($page < 1) { $page = 1; } // 페이지가 없으면 첫 페이지 (1 페이지)
$from_record = ($page - 1) * $rows; // 시작 열을 구함

//if (!$sst) {
//    $sst  = "io_time";
//    $sod = "desc";
//}
//$sql_order = "order by $sst $sod";


$sql  = " select *
           $sql_common
           $sql_order
           limit $from_record, $rows ";
$result = sql_query($sql);
//echo $sql;
//$qstr  = $qstr.'&amp;sca='.$sca.'&amp;page='.$page;
$qstr  = $qstr.'&amp;sca='.$sca.'&amp;page='.$page.'&amp;save_stx='.$stx;

?>

<form name="flist" class="local_sch01 local_sch">
<input type="hidden" name="page" value="<?php echo $page; ?>">
<input type="hidden" name="save_stx" value="<?php echo $stx; ?>">


<label for="comp_code" class="sound_only">업체선택</label>
<select name="comp_code" id="comp_code" onchange="this.form.submit()">
    <option value="">==업체선택==</option>
    <?php
    $sql2 = " select comp_code, comp_name from tbl_member where mb_type='7' order by comp_name ";
    $result2 = sql_query($sql2);
    for ($i=0; $row2=sql_fetch_array($result2); $i++) {
       if($_GET['comp_code'] ==$row2['comp_code']) 
			$checked='selected';
		else 
			$checked ='';

        echo '<option value="'.$row2['comp_code'].'" '.$checked.' >'.$row2['comp_name'].'</option>'.PHP_EOL;
    }
    ?>
</select>


<label for="sfl" class="sound_only">검색대상</label>
<select name="sfl" id="sfl">
    <option value="it_name" <?php echo get_selected($sfl, 'it_name'); ?>>상품명</option>
    <option value="it_id" <?php echo get_selected($sfl, 'it_id'); ?>>상품코드</option>
   
</select>

<label for="stx" class="sound_only">검색어</label>
<input type="text" name="stx" value="<?php echo $stx; ?>" id="stx" class="frm_input">
<input type="submit" value="검색" class="btn_submit">
* 등록이 안된 옵션상품만 검색이 됩니다.
<input type="button" value="선택등록" class="get_theme_confc btn btn_01" onclick="flist_submit();">   
</form>

<form name="fitemlistupdate" method="post" action="./itemlistupdate.php" onsubmit="return fitemlist_submit(this);" autocomplete="off" id="fitemlistupdate">
<input type="hidden" name="sca" value="<?php echo $sca; ?>">
<input type="hidden" name="sst" value="<?php echo $sst; ?>">
<input type="hidden" name="sod" value="<?php echo $sod; ?>">
<input type="hidden" name="sfl" value="<?php echo $sfl; ?>">
<input type="hidden" name="stx" value="<?php echo $stx; ?>">
<input type="hidden" name="page" value="<?php echo $page; ?>">

<div class="tbl_head01 tbl_wrap">
    <table>
    <caption><?php echo $g5['title']; ?> 목록</caption>
    <thead>
    <tr>
        <th scope="col">
            <label for="chkall" class="sound_only">상품 전체</label>
            <input type="checkbox" name="chkall" value="1" id="chkall" onclick="check_all(this.form)">
        </th>
		<th scope="col">업체명</th>
        <th scope="col">상품코드</th>
		<th scope="col" id="th_pc_title">상품명</th>
        
    </tr>
    </thead>
    <tbody>
    <?php
    for ($i=0; $row=sql_fetch_array($result); $i++)
    {
        $href = G5_SHOP_URL.'/item.php?it_id='.$row['it_id'];
        $bg = 'bg'.($i%2);

    ?>
    <tr class="<?php echo $bg; ?>">
        <td class="td_chk">
            <label for="chk_<?php echo $i; ?>" class="sound_only"><?php echo get_text($row['it_name']); ?></label>
            <input type="checkbox" name="chk[]" value="<?php echo $i ?>" id="chk_<?php echo $i; ?>">
        </td>

        <td>
           <?php echo $row['comp_name']; ?>
        </td>
		<td class="td_num">
            <input type="hidden" name="it_id[<?php echo $i; ?>]" value="<?php echo $row['it_id']; ?>">
            <?php echo $row['it_id']; ?>
        </td>
		 <td headers="th_pc_title" style="text-align:left;">           
            <?php echo htmlspecialchars2(cut_str($row['it_name'],250, "")); ?>
        </td>
    </tr>
    <?php
    }
    if ($i == 0)
        echo '<tr><td colspan="12" class="empty_table">자료가 한건도 없습니다.</td></tr>';
    ?>
    </tbody>
    </table>
</div>

</form>

<?php 
echo get_paging(G5_IS_MOBILE ? $config['cf_mobile_pages'] : $config['cf_write_pages'], $page, $total_page, "{$_SERVER['SCRIPT_NAME']}?$qstr&amp;page="); ?>

<script>
function check_all(f)
{
    var chk = document.getElementsByName("chk[]");

    for (i=0; i<chk.length; i++)
        chk[i].checked = f.chkall.checked;
}
function flist_submit()
{
	
	//if (!is_checked("chk[]")) {
    //    alert("등록 하실 항목을 하나 이상 선택하세요.");
    //    return false;
    //}
	
	var f = document.fitemlistupdate;
	var chk = document.getElementsByName("chk[]");
	var it_ids = '';
	for (var i=0; i<chk.length; i++)
	{
		if (chk[i].checked)
		{
			var k = chk[i].value;
			var it_id      = f.elements['it_id['+k+']'].value;					   
			it_ids = it_ids + it_id + ',';
		}
	}
	
	if (it_ids == '') {
        alert("등록 하실 항목을 하나 이상 선택하세요.");
        return false;
    } else {
		it_ids = it_ids.substring(0, it_ids.length - 1);
		$("#opt1" , parent.fitemform).val(it_ids);
		$("#opt1_subject" , parent.fitemform).val('선택');

		parent.fnTrHide();
	}
}

</script>
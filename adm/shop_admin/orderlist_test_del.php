<?php
include("./_common.php");


$common = "SELECT a.od_id,a.mb_id,b.it_id,c.t_code
                    FROM tbl_shop_order AS a 
                    LEFT JOIN tbl_shop_order_detail AS b ON a.od_id=b.od_id 
                    LEFT JOIN tbl_shop_item_auth AS c ON b.it_id=c.it_id WHERE c.mb_id=a.mb_id";

$od_id_sql = " SELECT od_id FROM 
                ($common)  AS ts
                    WHERE ts.t_code =1";

$od_id_result = sql_fetch_result($od_id_sql);

if (!$od_id_result) {
   alert("삭제할 데이터가 없습니다.");
    exit;
}

/* 테스트 주문 삭제 처리 */

for($i = 0; $i < count($od_id_result); $i ++) {
    $od = sql_fetch("select * from tbl_shop_order where od_id = ({$od_id_result[$i][od_id]});");
    $data = json_encode($od);

    $sql = "insert into tbl_shop_order_backup 
        set de_type='basic',
            de_key='{$od_id_result[$i][od_id]}',
            de_data='$data',
            mb_id='".$member['mb_id']."',
            de_ip='".$_SERVER['REMOTE_ADDR']."',
            de_datetime='".date('Y-m-d H:i:s')."'";
    @sql_query($sql);

    $sql = "DELETE FROM tbl_shop_order WHERE od_id = '{$od_id_result[$i][od_id]}'";
    @sql_query($sql);

    $sql = "DELETE FROM tbl_shop_order_detail WHERE od_id = '{$od_id_result[$i][od_id]}' ";
    @sql_query($sql);

    $sql = "DELETE FROM tbl_shop_order_receiver WHERE od_id = '{$od_id_result[$i][od_id]}' ";
    @sql_query($sql);

    $sql = "DELETE FROM tbl_shop_order_data WHERE od_id = '{$od_id_result[$i][od_id]}' ";
    @sql_query($sql);

    $sql = "DELETE FROM tbl_shop_order_multi_receiver WHERE od_id = '{$od_id_result[$i][od_id]}' ";
    @sql_query($sql);

    $sql = "DELETE FROM tbl_shop_order_drug WHERE od_id = '{$od_id_result[$i][od_id]}' ";
    @sql_query($sql);
}

alert("삭제되었습니다.");
?>
<script>
    history.back();
</script>
<?php
include_once('./_common.php');

// 배너 이미지 확인 팝업

$bn_id = $_GET['bn_id'];
$b_title = $_GET['b_title'];

$bimg = '';

if ($b_title == "eventbanner") {
    $bimg = G5_DATA_PATH . '/eventbanner/' . $bn_id;
} else {
    $bimg = G5_DATA_PATH . '/banner/' . $bn_id;
}

if (file_exists($bimg)) {
    $size = @getimagesize($bimg);
    if ($size[0] && $size[0] > 800)
        $width = 800;
    else
        $width = $size[0];

    if ($b_title == "eventbanner")
        echo '<img src="' . G5_DATA_URL . '/eventbanner/' . $bn_id . '" width="' . $width . '">';
    else
        echo '<img src="' . G5_DATA_URL . '/banner/' . $bn_id . '" width="' . $width . '">';
} else {
    echo "";
}
?>
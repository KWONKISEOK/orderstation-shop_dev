<?php
$sub_menu = '400300';
include_once('./_common.php');

check_demo();

check_admin_token();

if (!count($_POST['chk'])) {
    alert($_POST['act_button']." 하실 항목을 하나 이상 체크하세요.");
}

/*************** 트랜잭션 관련 ****************/
$error_cnt = 0;
mysqli_autocommit($g5['connect_db'], false);
/*************** 트랜잭션 관련 ****************/
		
if ($_POST['act_button'] == "선택승인") {

    auth_check($auth[$sub_menu], 'w');

	$item_dir = G5_DATA_PATH.'/item/';
	$item_mod_dir = G5_DATA_PATH.'/item_mod/';

    for ($i=0; $i<count($_POST['chk']); $i++) {

        // 실제 번호를 넘김
        $k = $_POST['chk'][$i];

        $sql = "update {$g5['g5_shop_item_table']}
			   set it_status2     = '3',
				   it_img1 = case when it_img1_mod != '' and it_img1_mod is not null then it_img1_mod else it_img1 end ,
				   it_img2 = case when it_img2_mod != '' and it_img2_mod is not null then it_img2_mod else it_img2 end ,
				   it_img3 = case when it_img3_mod != '' and it_img3_mod is not null then it_img3_mod else it_img3 end ,
				   it_img4 = case when it_img4_mod != '' and it_img4_mod is not null then it_img4_mod else it_img4 end ,
				   it_img5 = case when it_img5_mod != '' and it_img5_mod is not null then it_img5_mod else it_img5 end ,
				   it_img6 = case when it_img6_mod != '' and it_img6_mod is not null then it_img6_mod else it_img6 end ,
				   it_img7 = case when it_img7_mod != '' and it_img7_mod is not null then it_img7_mod else it_img7 end ,
				   it_img8 = case when it_img8_mod != '' and it_img8_mod is not null then it_img8_mod else it_img8 end ,
				   it_img9 = case when it_img9_mod != '' and it_img9_mod is not null then it_img9_mod else it_img9 end ,
				   it_img10 = case when it_img10_mod != '' and it_img10_mod is not null then it_img10_mod else it_img10 end ,
				   it_explan = case when it_explan_mod != '' and it_explan_mod is not null then it_explan_mod else it_explan end ,
				   it_mobile_explan = case when it_mobile_explan_mod != '' and it_mobile_explan_mod is not null then it_mobile_explan_mod else it_mobile_explan end ,
				   it_time_mod_confirm = '".G5_TIME_YMDHIS."',
				   it_time_mod_update = '".G5_TIME_YMDHIS."'
                 where it_id   = '{$_POST['it_id'][$k]}' ";
        sql_query($sql);
		/*************** 트랜잭션 관련 ****************/
		if( mysqli_errno($g5['connect_db']) ){
			$error_cnt += 1;
		}
		/*************** 트랜잭션 관련 ****************/

        /************* 관리자 로그 처리 START *************/
        insert_admin_log(600,600260, '선택 승인', '', $_POST['it_id'][$k], '', $_SERVER['REQUEST_URI']);
        /************* 관리자 로그 처리 END *************/

		$sql2 = " select 
					it_img1_mod, it_img2_mod, it_img3_mod, it_img4_mod, it_img5_mod, it_img6_mod, it_img7_mod, it_img8_mod, it_img9_mod, it_img10_mod
					from {$g5['g5_shop_item_table']}
					where it_id = '{$_POST['it_id'][$k]}' ";
		$file = sql_fetch($sql2);

		$it_img1    = $file['it_img1_mod'];
		$it_img2    = $file['it_img2_mod'];
		$it_img3    = $file['it_img3_mod'];
		$it_img4    = $file['it_img4_mod'];
		$it_img5    = $file['it_img5_mod'];
		$it_img6    = $file['it_img6_mod'];
		$it_img7    = $file['it_img7_mod'];
		$it_img8    = $file['it_img8_mod'];
		$it_img9    = $file['it_img9_mod'];
		$it_img10   = $file['it_img10_mod'];

		// 기존 item 폴더에 이미지를 생성하지 않은 경우 최초 디렉토리 생성 필요
        if(!is_dir($item_dir.$_POST['it_id'][$k])) {
            @mkdir($item_dir.$_POST['it_id'][$k], G5_DIR_PERMISSION);
            @chmod($item_dir.$_POST['it_id'][$k], G5_DIR_PERMISSION);
        }

		if(!empty($it_img1)){
			copy($item_mod_dir.$it_img1, $item_dir.$it_img1); 
		}
		if(!empty($it_img2)){
			copy($item_mod_dir.$it_img2, $item_dir.$it_img2); 
		}
		if(!empty($it_img3)){
			copy($item_mod_dir.$it_img3, $item_dir.$it_img3); 
		}
		if(!empty($it_img4)){
			copy($item_mod_dir.$it_img4, $item_dir.$it_img4); 
		}
		if(!empty($it_img5)){
			copy($item_mod_dir.$it_img5, $item_dir.$it_img5); 
		}
		if(!empty($it_img6)){
			copy($item_mod_dir.$it_img6, $item_dir.$it_img6); 
		}
		if(!empty($it_img7)){
			copy($item_mod_dir.$it_img7, $item_dir.$it_img7); 
		}
		if(!empty($it_img8)){
			copy($item_mod_dir.$it_img8, $item_dir.$it_img8); 
		}
		if(!empty($it_img9)){
			copy($item_mod_dir.$it_img9, $item_dir.$it_img9); 
		}
		if(!empty($it_img10)){
			copy($item_mod_dir.$it_img10, $item_dir.$it_img10); 
		}


    }



} else if ($_POST['act_button'] == "선택초기화") {

    if ($is_admin != 'super')
        alert('상품 삭제는 최고관리자만 가능합니다.');

    auth_check($auth[$sub_menu], 'd');

    for ($i=0; $i<count($_POST['chk']); $i++) {
        // 실제 번호를 넘김
        $k = $_POST['chk'][$i];

        $sql = "update {$g5['g5_shop_item_table']}
                   set it_status2     = '0',
				   	   it_price2	  = 0,
					   it_supply_price2 = 0,
					   it_img1_mod = '',
					   it_img2_mod = '',
					   it_img3_mod = '',
					   it_img4_mod = '',
					   it_img5_mod = '',
					   it_img6_mod = '',
					   it_img7_mod = '',
					   it_img8_mod = '',
					   it_img9_mod = '',
					   it_img10_mod = '',
					   it_explan_mod = '',
					   it_mobile_explan_mod = '',
					   it_mod_memo = '',
					   it_time_mod_reg = '0000-00-00 00:00:00',
                       it_time_mod_confirm = '0000-00-00 00:00:00',
                       it_time_mod_update = '0000-00-00 00:00:00'
                 where it_id   = '{$_POST['it_id'][$k]}' ";
			
        sql_query($sql);
		/*************** 트랜잭션 관련 ****************/
		if( mysqli_errno($g5['connect_db']) ){
			$error_cnt += 1;
		}
		/*************** 트랜잭션 관련 ****************/

        /************* 관리자 로그 처리 START *************/
        insert_admin_log(600,600260, '선택 초기화', '', $_POST['it_id'][$k], '', $_SERVER['REQUEST_URI']);
        /************* 관리자 로그 처리 END *************/
    }
}

/*************** 트랜잭션 관련 ****************/
if ($error_cnt > 0) {
	mysqli_rollback($g5['connect_db']);
	mysqli_close($g5['connect_db']);
	echo "
	<script>
		alert('데이터베이스의 에러로 인해 롤백되었습니다.');
		history.back();
	</script>
	";
	exit;
} else {
	mysqli_commit($g5['connect_db']);
} 	
/*************** 트랜잭션 관련 ****************/

goto_url("./itemlist_reg2.php?sca=$sca&amp;sst=$sst&amp;sod=$sod&amp;sfl=$sfl&amp;stx=$stx&amp;page=$page");
?>

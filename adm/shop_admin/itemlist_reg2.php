<?php
$sub_menu = '600260';
include_once('./_common.php');

auth_check($auth[$sub_menu], "r");

$g5['title'] = '상품승인처리';
include_once (G5_ADMIN_PATH.'/admin.head.php');


$where = " and ";
$sql_search = "";
if ($stx != "") {
    if ($sfl != "") {
        $sql_search .= " $where $sfl like '%$stx%' ";
        $where = " and ";
    }
    if ($save_stx != $stx)
        $page = 1;
}
/*
if ($sca != "") {
    $sql_search .= " $where (a.ca_id like '$sca%' or a.ca_id2 like '$sca%' or a.ca_id3 like '$sca%') ";
}
*/
if ($sfl == "")  $sfl = "it_name";

$sql_common = " from {$g5['g5_shop_item_table']} a ,
                     {$g5['g5_shop_category_table']} b,
					 tbl_member c
               where (a.ca_id = b.ca_id and a.comp_code = c.comp_code";

if ($_GET['comp_code'] != '')
	$sql_common .= " and a.comp_code = '{$_GET['comp_code']}' ";

//if ($_GET['it_status'] != '')
    $sql_common .= " and a.it_status2 = '2' and c.mb_type=7 ";

$sql_common .= ") ";
$sql_common .= $sql_search;

// 테이블의 전체 레코드수만 얻음
$sql = " select count(*) as cnt " . $sql_common;
$row = sql_fetch($sql);
$total_count = $row['cnt'];

$rows = $config['cf_page_rows'];
$total_page  = ceil($total_count / $rows);  // 전체 페이지 계산
if ($page < 1) { $page = 1; } // 페이지가 없으면 첫 페이지 (1 페이지)
$from_record = ($page - 1) * $rows; // 시작 열을 구함

if (!$sst) {
    $sst  = "it_id";
    $sod = "desc";
}
$sql_order = "order by $sst $sod";


$sql  = " select a.*, c.comp_name
           $sql_common
           $sql_order
           limit $from_record, $rows ";
$result = sql_query($sql);

//$qstr  = $qstr.'&amp;sca='.$sca.'&amp;page='.$page;
$qstr  = $qstr.'&amp;sca='.$sca.'&amp;page='.$page.'&amp;save_stx='.$stx;

$listall = '<a href="'.$_SERVER['SCRIPT_NAME'].'" class="ov_listall">전체목록</a>';
?>

<link rel="stylesheet" href="/css/tip-skyblue/tip-skyblue.css" type="text/css" />
<script type="text/javascript" src="/js/jquery.poshytip.min.js"></script>

<div class="local_ov01 local_ov">
    <?php echo $listall; ?>
    <span class="btn_ov01"><span class="ov_txt"> 등록 신청 </span><span class="ov_num"> <?php echo $total_count; ?>건</span></span>
</div>

<form name="flist" class="local_sch01 local_sch">
<input type="hidden" name="page" value="<?php echo $page; ?>">
<input type="hidden" name="save_stx" value="<?php echo $stx; ?>">


<label for="comp_code" class="sound_only">업체선택</label>
<select name="comp_code" id="comp_code"  onchange="this.form.submit()">
    <option value="">==업체선택==</option>
    <?php
    $sql2 = " select comp_code, comp_name from tbl_member where mb_type='7' order by comp_code ";
    $result2 = sql_query($sql2);
    for ($i=0; $row2=sql_fetch_array($result2); $i++) {
		if($_GET['comp_code'] ==$row2['comp_code']) 
			$checked='selected';
		else 
			$checked ='';

        echo '<option value="'.$row2['comp_code'].'" '.$checked.' >'.$row2['comp_name'].'</option>'.PHP_EOL;
    }
    ?>
</select>

<label for="sfl" class="sound_only">검색대상</label>
<select name="sfl" id="sfl">
    <option value="it_name" <?php echo get_selected($sfl, 'it_name'); ?>>상품명</option>
    <option value="it_id" <?php echo get_selected($sfl, 'it_id'); ?>>상품코드</option>
    <option value="it_maker" <?php echo get_selected($sfl, 'it_maker'); ?>>제조사</option>
    <option value="it_origin" <?php echo get_selected($sfl, 'it_origin'); ?>>원산지</option>
    <option value="it_sell_email" <?php echo get_selected($sfl, 'it_sell_email'); ?>>판매자 e-mail</option>
</select>

<label for="stx" class="sound_only">검색어</label>
<input type="text" name="stx" value="<?php echo $stx; ?>" id="stx" class="frm_input">
<input type="submit" value="검색" class="btn_submit">&nbsp;&nbsp;&nbsp;
<input type="button" value="엑셀 다운로드" class="get_theme_confc btn btn_02" style="cursor:pointer" onclick="formSubmit(1);">
</form>

<form name="fitemlistupdate" method="post" action="./itemlistupdate_reg2.php" onsubmit="return fitemlist_submit(this);" autocomplete="off" id="fitemlistupdate">
<input type="hidden" name="sca" value="<?php echo $sca; ?>">
<input type="hidden" name="sst" value="<?php echo $sst; ?>">
<input type="hidden" name="sod" value="<?php echo $sod; ?>">
<input type="hidden" name="sfl" value="<?php echo $sfl; ?>">
<input type="hidden" name="stx" value="<?php echo $stx; ?>">
<input type="hidden" name="page" value="<?php echo $page; ?>">

<div class="tbl_head01 tbl_wrap">
    <table>
    <caption><?php echo $g5['title']; ?> 목록</caption>
    <thead>
    <tr>
        <th scope="col">
            <label for="chkall" class="sound_only">상품 전체</label>
            <input type="checkbox" name="chkall" value="1" id="chkall" onclick="check_all(this.form)">
        </th>
        <th scope="col"><?php echo subject_sort_link('it_id', 'sca='.$sca); ?>상품코드</a></th>
		<th scope="col" width="200">공급사명</th>
        <th scope="col" width="150"><?php echo subject_sort_link('it_time_mod_reg', 'sca='.$sca, 1); ?>변경신청일</a></th>
		
        <th scope="col" id="th_img" width="200">이미지</th>
        <th scope="col" id="th_pc_title" ><?php echo subject_sort_link('it_name', 'sca='.$sca); ?>상품명</a></th>
        <th scope="col" id="th_amt"><?php echo subject_sort_link('it_price2', 'sca='.$sca); ?>수정요청소비자가</a></th>
        <th scope="col" id="th_amt"><?php echo subject_sort_link('it_supply_price2', 'sca='.$sca); ?>수정요청공급가</a></th>

		<th scope="col">관리</th>
        
    </tr>
    </thead>
    <tbody>
    <?php
    for ($i=0; $row=sql_fetch_array($result); $i++)
    {
        $href = G5_SHOP_URL.'/item.php?it_id='.$row['it_id'];
        $bg = 'bg'.($i%2);

      

    ?>
    <tr class="<?php echo $bg; ?>">
        <td class="td_chk">
            <label for="chk_<?php echo $i; ?>" class="sound_only"><?php echo get_text($row['it_name']); ?></label>
            <input type="checkbox" name="chk[]" value="<?php echo $i ?>" id="chk_<?php echo $i; ?>">
        </td>
        <td class="td_num">
            <input type="hidden" name="it_id[<?php echo $i; ?>]" value="<?php echo $row['it_id']; ?>">
            <?php echo $row['it_id']; ?>
        </td>
        <td>
           <?php echo $row['comp_name']; ?>
        </td>
        <td>
           <?php echo $row['it_time_mod_reg']; ?>
        </td>
        <td class="td_img">
			<a href="javascript:ViewInfo('<?=$row['it_id']?>');" class="btn btn_03">변경요청내용 보기</a>
		</td>
        <td>
           <?php echo htmlspecialchars2(cut_str($row['it_name'],200, "")); ?>
        </td>
        <td headers="th_amt" class="td_numbig ">
            <?php echo number_format($row['it_price2']); ?>
        </td>

        <!-- <td headers="th_pt" class="td_numbig td_input"><?php echo $it_point; ?></td> -->
        <td headers="th_amt" class="td_numbig">
            <?php echo number_format($row['it_supply_price2']); ?>
        </td>

<!-- <td class="td_mng td_mng_s"> -->
		 <td width="100">
            <a href="./itemform.php?w=u&amp;it_id=<?php echo $row['it_id']; ?>&amp;ca_id=<?php echo $row['ca_id']; ?>&amp;<?php echo $qstr; ?>" class="btn btn_03"><span class="sound_only"><?php echo htmlspecialchars2(cut_str($row['it_name'],250, "")); ?> </span>수정</a>
            
        </td>

    </tr>
    <?php
    }
    if ($i == 0)
        echo '<tr><td colspan="12" class="empty_table">자료가 한건도 없습니다.</td></tr>';
    ?>
    </tbody>
    </table>
</div>

<div class="btn_fixed_top">

    <!-- <a href="./itemform.php" class="btn btn_01">상품등록</a> -->
    <!-- <a href="./itemexcel.php" onclick="return excelform(this.href);" target="_blank" class="btn btn_02">상품일괄등록</a> -->
    <input type="submit" name="act_button" value="선택승인" onclick="document.pressed=this.value" class="btn btn_02">
    <?php if ($is_admin == 'super') { ?>
    <input type="submit" name="act_button" value="선택초기화" onclick="document.pressed=this.value" class="btn btn_01"> 
    <?php } ?>
</div>
<!-- <div class="btn_confirm01 btn_confirm">
    <input type="submit" value="일괄수정" class="btn_submit" accesskey="s">
</div> -->
</form>

<?php 
$qstr ='comp_code='.$_GET['comp_code'].'&'.$qstr;
echo get_paging(G5_IS_MOBILE ? $config['cf_mobile_pages'] : $config['cf_write_pages'], $page, $total_page, "{$_SERVER['SCRIPT_NAME']}?$qstr&amp;page="); ?>

<script src="/js/jquery.bpopup.min.js"></script>
<script>

function ViewInfo( GetId ){


	$('#popupLayer').bPopup({

		modalClose: false,
		content:'iframe', //'ajax', 'iframe' or 'image'
		iframeAttr:'frameborder=auto',
		iframeAttr:'frameborder=0',
		contentContainer:'.popupContent',
		loadUrl:'./itemlist_reg2_info.php?it_id='+GetId

	});


}

function formSubmit(type) {
	var form = "<form action='itemlist_reg_02.php' method='post'>"; 
		form += "<input type='hidden' name='mode' value='excel' />"; 
		form += "<input type='hidden' name='comp_code' value='"+$('#comp_code option:selected').val()+"' />"; 
		form += "<input type='hidden' name='sfl' value='"+$('#sfl option:selected').val()+"' />"; 
		form += "<input type='hidden' name='stx' value='"+$('#stx').val()+"' />"; 
		form += "</form>"; 
		jQuery(form).appendTo("body").submit().remove(); 
}

function fitemlist_submit(f)
{
    if (!is_checked("chk[]")) {
        alert(document.pressed+" 하실 항목을 하나 이상 선택하세요.");
        return false;
    }

    if(document.pressed == "선택삭제") {
        if(!confirm("선택한 자료를 정말 삭제하시겠습니까?")) {
            return false;
        }
    }

    return true;
}

function excelform(url)
{
    var opt = "width=600,height=450,left=10,top=10";
    window.open(url, "win_excel", opt);
    return false;
}


$('.tooltip').poshytip({
				className: 'tip-skyblue',
				bgImageFrameSize: 9,
				offsetX: 0,
				offsetY: 20
			});


</script>

<style>
	#popupLayer {display:none;border:5px solid #cccccc;margin-top:50px;padding:5px;background-color:#ffffff;z-index:5;}
	#popupLayer .b-close {position:absolute;top:10px;right:25px;color:#f37a20;font-weight:bold;cursor:hand;}
	#popupLayer .popupContent {margin:0;padding:0;text-align:center;border:0;}
	#popupLayer .popupContent iframe {width:1400px;height:800px;border:0;padding:0px;margin:0;z-index:10;}
</style>

<div id="popupLayer">
	<div class="popupContent"></div>
	<div class="b-close">
		<button type="button" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button"><span class="ui-button-text">&nbsp;X&nbsp;</span></button>
	</div>
</div>

<?php
include_once (G5_ADMIN_PATH.'/admin.tail.php');
?>

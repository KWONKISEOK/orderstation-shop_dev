<?php
$sub_menu = '400400';
include_once('./_common.php');

auth_check($auth[$sub_menu], "r");

if( empty($od_period_date) ){
	echo "<script>
			alert('발송예정일값이 없습니다.');
			history.back();
		  </script>
		";
	exit;
}
if( empty($od_id) ){
	echo "<script>
			alert('주문번호값이 없습니다.');
			history.back();
		  </script>
		";
	exit;
}
if( empty($od_seq) ){
	echo "<script>
			alert('회차값이 없습니다.');
			history.back();
		  </script>
		";
	exit;
}

//입금처리 된 건인지 검사 
$sql = " select count(od_pay_yn) as cnt from tbl_shop_order_receiver where od_id = '{$od_id}' and od_seq = '{$od_seq}' and od_pay_yn = 'Y' ";

$rs = sql_fetch($sql);

if( $rs["cnt"] > 0 ){
	echo "<script>
			alert('입금처리된 건은 날짜변경 불가합니다.');
			history.back();
		  </script>
		";
	exit;
}

//미입금인건만 날짜 변경
$sql = " update tbl_shop_order_receiver set od_period_date = '{$od_period_date}' where od_id = '{$od_id}' and od_seq = '{$od_seq}' and od_pay_yn = 'N' ";
sql_query($sql);

echo "<script>
		alert('변경완료');
		parent.ClosePop();
	  </script>
	";
exit;

?>
<?php
$sub_menu = '400400';
include_once('./_common.php');

if( $od_status == "" ){
	$sub_menu = '400400';
	$TopTitle = "주문";
}else if( $od_status == "반품접수" ){
	$sub_menu = '400408';
	$TopTitle = $od_status;
}else if( $od_status == "반품반려" ){
	$sub_menu = '400409';
	$TopTitle = $od_status;
}else if( $od_status == "반품완료" ){
	$sub_menu = '400410';
	$TopTitle = $od_status;
}else if( $od_status == "반품전체" ){
	$sub_menu = '400411';
	$TopTitle = $od_status;
}else if( $od_status == "반품승인" ){
	$sub_menu = '400412';
	$TopTitle = $od_status;
}else{
	$TopTitle = "주문";
}

auth_check($auth[$sub_menu], "r");

$g5['title'] = $TopTitle.'내역';
include_once (G5_ADMIN_PATH.'/admin.head.php');
include_once(G5_PLUGIN_PATH.'/jquery-ui/datepicker.php');

$comp_code = $member[comp_code];

$where = array();

$doc = strip_tags($doc);
$sort1 = in_array($sort1, array('od_id', 'od_cart_price', 'od_receipt_price', 'od_cancel_price', 'od_misu', 'od_cash')) ? $sort1 : '';
$sort2 = in_array($sort2, array('desc', 'asc')) ? $sort2 : 'desc';
$sel_field = get_search_string($sel_field);
if( !in_array($sel_field, array('od_id', 'mb_id', 'od_name', 'od_tel', 'od_hp', 'od_b_name', 'comp_name', 'it_name', 'pharm_name', 'pharm_custno')) ){   //검색할 필드 대상이 아니면 값을 제거
    $sel_field = '';
}
$od_status = get_search_string($od_status);
$search = get_search_string($search);
if(! preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $fr_date) ) $fr_date = '';
if(! preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $to_date) ) $to_date = '';
if($od_status == "반품접수" || $od_status == "반품승인"){
	if($fr_date =='') $fr_date = date("Y-m-d", strtotime('-730 day'));
}else{
	if($fr_date =='') $fr_date = date("Y-m-d", strtotime('-2 week'));
}
if($to_date =='') $to_date = date("Y-m-d");
$sql_search = "";
if ($search != "") {
    if ($sel_field != "") {
		if($sel_field == 'comp_name') {
			$where[] = "d.$sel_field like '%$search%' ";
		} else if($sel_field == 'it_name') {
			$where[] = "b.$sel_field like '%$search%' ";
		} else {
			$where[] = "a.$sel_field like '%$search%' ";
		}

    }

    if ($save_search != $search) {
        $page = 1;
    }
}

if( $od_status == "반품접수" ){
	$od_date = "5";
}else if( $od_status == "반품반려" ){
	$od_date = "6";
}else if( $od_status == "반품완료" ){
	$od_date = "7";
}else if( $od_status == "반품승인" ){
	$od_date = "8";
	$od_status = "반품접수";
}

//날자구분
switch($od_date) {
	case '1':
		$where[] = " c.od_date1 between '$fr_date 00:00:00' and '$to_date 23:59:59' ";
		break;
	case '2':
		$where[] = " c.od_date2 between '$fr_date 00:00:00' and '$to_date 23:59:59' ";
		break;
	case '3':
		$where[] = " c.od_date3 between '$fr_date 00:00:00' and '$to_date 23:59:59' ";
		break;
	case '4':
		$where[] = " c.od_date4 between '$fr_date 00:00:00' and '$to_date 23:59:59' ";
		break;
	case '5':
		$where[] = " c.od_return_date_1 between '$fr_date 00:00:00' and '$to_date 23:59:59' and c.od_return_confirm = 'N' ";
		break;
	case '6':
		$where[] = " c.od_return_date_2 between '$fr_date 00:00:00' and '$to_date 23:59:59' ";
		break;
	case '7':
		$where[] = " c.od_return_date_3 between '$fr_date 00:00:00' and '$to_date 23:59:59' ";
		break;
	case '8':
		$where[] = " c.od_return_date_1 between '$fr_date 00:00:00' and '$to_date 23:59:59' and c.od_return_confirm = 'Y' ";
		break;
	default:
		$where[] = " c.od_date1 between '$fr_date 00:00:00' and '$to_date 23:59:59' ";
		break;
}

//공급사코드
$where[] = " b.comp_code = '$comp_code' ";


//상태
if ($od_status) {
    $where[] = " c.od_status = '$od_status' ";
}

//일반 정기
if ($od_period_yn == 'Y') {
    $where[] = " b.od_period_yn = '$od_period_yn' ";
}
//주문구분
if ($od_gubun) {
    $where[] = " a.od_gubun = '$od_gubun' ";
}


/*if ($is_admin == 'sales') {
	$where[] = " mb_id in (select mb_id from tbl_member where pharm_manager = '{$member['mb_id']}') ";
}
*/


if ($where) {
	$sql_search = ' where a.od_id = b.od_id and b.od_id = c.od_id and b.od_num = c.od_num and b.comp_code = d.comp_code and '.implode(' and ', $where);
}

if ($sel_field == "")  $sel_field = "od_id";
if ($sort1 == "") $sort1 = "od_id";
if ($sort2 == "") $sort2 = "desc";

$sql_common = " from tbl_shop_order a,  tbl_shop_order_detail b, tbl_shop_order_receiver c, tbl_member d $sql_search ";

//$sql = " select count(od_id) as cnt " . $sql_common;
$sql = " select count(distinct a.od_id, b.comp_code) as cnt " . $sql_common;

$row = sql_fetch($sql);
//페이징처리
$total_count = $row['cnt'];

$sql = " select count(distinct a.od_id) as cnt " . $sql_common;
$row = sql_fetch($sql);
//총주문건수
$total_count1 = $row['cnt'];

$rows = 10;//$config['cf_page_rows'];
$total_page  = ceil($total_count / $rows);  // 전체 페이지 계산
if ($page < 1) { $page = 1; } // 페이지가 없으면 첫 페이지 (1 페이지)
$from_record = ($page - 1) * $rows; // 시작 열을 구함

$iRow = 0;
$sql  = " select a.od_id, b.comp_code, count(*) od_count
           $sql_common
		   group by a.od_id, b.comp_code
           order by a.od_time desc, b.comp_code
           limit $from_record, $rows ";
$result = sql_query($sql);
//echo $sql;
//상품별 만들기
$iRow = 0;
for ($i=0; $row=sql_fetch_array($result); $i++)
{
	//푸른솔푸드일때만 입금확인중 나오게 처리
	if( $row[comp_code] == "268" ){
		$add_sql = "";
	}else{
		$add_sql = " and not ( a.od_settle_case = '가상계좌' and c.od_pay_yn = 'N' ) ";
	}

	$sql  = " select a.mb_id
					 ,a.od_name
					 ,a.pharm_name
					 ,a.pharm_manager
					 ,(select comp_it_id from tbl_shop_item where it_id = b.it_id limit 1 ) comp_it_id
					 ,(select mb_name from tbl_member where mb_id= a.pharm_manager ) pharm_manager_name
					 ,(select mb_name from tbl_member where pharm_custno= a.pharm_custno and pharm_custno != '' limit 1) pharm_mb_name
					 ,a.od_settle_case
					 ,a.pharm_custno
					 ,a.od_gubun
					 ,a.od_mobile
					 ,(select mb_pay_yn from tbl_member where mb_id = a.mb_id) mb_pay_yn
					 ,d.comp_name
					 
					 ,b.od_num
					 ,b.it_name
					 ,b.od_option
					 ,b.od_period_yn
					 ,b.od_period_cnt
					 ,b.od_sum_drug_price
					 ,b.od_total_drug_price
					 ,b.od_total_incen
					 ,c.od_b_name
					 ,c.od_seq
					 ,c.od_date1
					 ,c.od_qty
					 ,c.od_period_date
					 ,c.od_status
					 ,c.od_pay_yn
					 ,b.it_id
					 ,c.od_release_yn
					 ,c.od_return_date_1
					 ,c.od_return_date_2
					 ,c.od_return_date_3
					 ,c.od_return_confirm, c.od_return_delivery_company , c.od_return_delivery_url
				 from tbl_shop_order a, tbl_shop_order_detail b, tbl_shop_order_receiver c, tbl_member d
				 where a.od_id = b.od_id 
				   and b.od_id = c.od_id 
				   and b.od_num = c.od_num
				   and b.comp_code = d.comp_code
				   and b.od_id = ".$row[od_id]. " and b.comp_code = '".$row[comp_code]."'  {$add_sql}   ";
		
	switch($od_date) {
		case '1':
			$sql .= " and  c.od_date1 between '$fr_date 00:00:00' and '$to_date 23:59:59' ";
			break;
		case '2':
			$sql .= " and  c.od_date2 between '$fr_date 00:00:00' and '$to_date 23:59:59' ";
			break;
		case '3':
			$sql .= " and  c.od_date3 between '$fr_date 00:00:00' and '$to_date 23:59:59' ";
			break;
		case '4':
			$sql .= " and c.od_date4 between '$fr_date 00:00:00' and '$to_date 23:59:59' ";
			break;
		case '5':
			$sql .= " and c.od_return_date_1 between '$fr_date 00:00:00' and '$to_date 23:59:59' and c.od_return_confirm = 'N' ";
			break;
		case '6':
			$sql .= " and c.od_return_date_2 between '$fr_date 00:00:00' and '$to_date 23:59:59' ";
			break;
		case '7':
			$sql .= " and c.od_return_date_3 between '$fr_date 00:00:00' and '$to_date 23:59:59' ";
			break;
		case '8':
			$sql .= " and c.od_return_date_1 between '$fr_date 00:00:00' and '$to_date 23:59:59' and c.od_return_confirm = 'Y' ";
			break;
		default:
			$sql .= " and  c.od_date1 between '$fr_date 00:00:00' and '$to_date 23:59:59' ";
			break;
	}

	//일반 정기
	if ($od_period_yn ==  'Y') {
		$sql .= " and b.od_period_yn = '$od_period_yn' ";
	}

	//상태
	if ($od_status) {
		$sql .= " and c.od_status = '$od_status' ";
	}
	
	//상품명
	if($sel_field == 'it_name') {
		$where[] = "b.$sel_field like '%$search%' ";
	}
	$result2 = sql_query($sql);
	for ($j=0; $row2=sql_fetch_array($result2); $j++)
	{

		$order[$iRow] = $row;
		$order[$iRow][comp_it_id] = $row2[comp_it_id];
		$order[$iRow][mb_id] = $row2[mb_id];
		$order[$iRow][od_name] = $row2[od_name];
		$order[$iRow][pharm_name] = $row2[pharm_name];
		$order[$iRow][pharm_manager] = $row2[pharm_manager];
		$order[$iRow][pharm_manager_name] = $row2[pharm_manager_name];
		$order[$iRow][pharm_mb_name] = $row2[pharm_mb_name];
		$order[$iRow][od_b_name] = $row2[od_b_name];
		$order[$iRow][mb_pay_yn] = $row2[mb_pay_yn];

		$order[$iRow][od_settle_case] = $row2[od_settle_case];
		$order[$iRow][pharm_custno] = $row2[pharm_custno];
		$order[$iRow][od_gubun] = $row2[od_gubun];
		$order[$iRow][comp_name] = $row2[comp_name];
		$order[$iRow][od_mobile] = $row2[od_mobile];

		$order[$iRow][od_num] = $row2[od_num];
		$order[$iRow][it_name] = $row2[it_name];
		$order[$iRow][od_option] = $row2[od_option];
		$order[$iRow][od_period_yn] = $row2[od_period_yn];

		$order[$iRow][od_sum_drug_price] = $row2[od_sum_drug_price];		
		if($row2[od_period_yn] == 'Y') {		
			$order[$iRow][od_total_drug_price] = $row2[od_total_drug_price] / $row2[od_period_cnt];
			$order[$iRow][od_total_incen] = $row2[od_total_incen] / $row2[od_period_cnt];
		} else {
			$order[$iRow][od_total_drug_price] = $row2[od_total_drug_price];
			$order[$iRow][od_total_incen] = $row2[od_total_incen];
		}
		$order[$iRow][od_status] = $row2[od_status];
		
		$order[$iRow][od_qty] = $row2[od_qty];
		$order[$iRow][od_date1] = $row2[od_date1];
		$order[$iRow][od_seq] = $row2[od_seq];
		$order[$iRow][od_period_date] = $row2[od_period_date];
		$order[$iRow][od_status] = $row2[od_status];
		$order[$iRow][od_pay_yn] = $row2[od_pay_yn];
		$order[$iRow][it_id] = $row2[it_id];
		$order[$iRow][od_release_yn] = $row2[od_release_yn];
		$order[$iRow][od_return_confirm] = $row2[od_return_confirm];

		$iRow++;
	}	
}

//print_r($order);

$qstr1 = "od_status=".urlencode($od_status)."&amp;od_settle_case=".urlencode($od_settle_case)."&amp;od_date=$od_date&amp;comp_code=$comp_code&amp;od_period_yn=$od_period_yn&amp;od_gubun=$od_gubun&amp;od_coupon=$od_coupon&amp;fr_date=$fr_date&amp;to_date=$to_date&amp;sel_field=$sel_field&amp;search=$search&amp;save_search=$search";
if($default['de_escrow_use'])
    $qstr1 .= "&amp;od_escrow=$od_escrow";
$qstr = "$qstr1&amp;sort1=$sort1&amp;sort2=$sort2&amp;page=$page";

$listall = '<a href="'.$_SERVER['SCRIPT_NAME'].'" class="ov_listall">전체목록</a>';


?>

<div class="local_ov01 local_ov">
    <?php echo $listall; ?>
    <span class="btn_ov01"><span class="ov_txt">전체 주문내역</span><span class="ov_num"> <?php echo number_format($total_count1); ?>건</span></span>
    
</div>





<form name="frmorderlist" class="local_sch01 local_sch">
<input type="hidden" name="doc" value="<?php echo $doc; ?>">
<input type="hidden" name="sort1" value="<?php echo $sort1; ?>">
<input type="hidden" name="sort2" value="<?php echo $sort2; ?>">
<input type="hidden" name="page" value="<?php echo $page; ?>">
<input type="hidden" name="save_search" value="<?php echo $search; ?>">
<input type="hidden" name="comp_code" value="<?php echo $comp_code; ?>">

<div class="local_sch03  tbl_wrap" style="padding:10px;">
    <table>
   
    <tbody>
    <tr>
        <th scope="row">
			<select name="od_date" id="od_date" >
				<option value="1" <?php echo get_selected($od_date, '1'); ?>>주문일</option>
				<option value="2" <?php echo get_selected($od_date, '2'); ?>>확정일</option>
				<option value="3" <?php echo get_selected($od_date, '3'); ?>>배송일</option>
				<option value="4" <?php echo get_selected($od_date, '4'); ?>>완료일</option>
				<option value="5" <?php echo get_selected($od_date, '5'); ?>>반품접수일</option>
				<option value="6" <?php echo get_selected($od_date, '6'); ?>>반품반려일</option>
				<option value="7" <?php echo get_selected($od_date, '7'); ?>>반품완료일</option>
			</select>
		</th>
        <td colspan=5>
            <input type="text" id="fr_date"  name="fr_date" value="<?php echo $fr_date; ?>" class="frm_input" size="10" maxlength="10"> ~
			<input type="text" id="to_date"  name="to_date" value="<?php echo $to_date; ?>" class="frm_input" size="10" maxlength="10">
			<button type="button" onclick="javascript:set_date('오늘');">오늘</button>
			<button type="button" onclick="javascript:set_date('어제');">어제</button>
			<button type="button" onclick="javascript:set_date('이번주');">이번주</button>
			<button type="button" onclick="javascript:set_date('이번달');">이번달</button>
			<button type="button" onclick="javascript:set_date('지난주');">지난주</button>
			<button type="button" onclick="javascript:set_date('지난달');">지난달</button>
			<button type="button" onclick="javascript:set_date('12개월');">12개월</button>
			<button type="button" onclick="javascript:set_date('전체');">전체</button>
        </td>
		<td rowspan="3">
			 <input type="submit" value="검색" class="get_theme_confc btn btn_01">
			 <input type="button" value="엑셀 다운로드" alt="엑셀 다운로드" class="get_theme_confc btn btn_02" style="cursor:pointer" id="ExcelDownButton">

		</td>
		
    </tr>
	<tr>
        <th scope="row">처리상태</th>
        <td>
			<select name="od_status" id="od_status"  >
			    <option value="">==전체==</option>
				<option value="주문" <?php echo get_selected($_GET["od_status"], '주문'); ?>>주문</option>
				<option value="확정" <?php echo get_selected($_GET["od_status"], '확정'); ?>>확정</option>
				<option value="배송" <?php echo get_selected($_GET["od_status"], '배송'); ?>>배송</option>
				<option value="완료" <?php echo get_selected($_GET["od_status"], '완료'); ?>>완료</option>
				<option value="취소" <?php echo get_selected($_GET["od_status"], '취소'); ?>>취소</option>   
				<option value="반품접수" <?php echo get_selected($_GET["od_status"], '반품접수'); ?>>반품접수</option>   
				<option value="반품승인" <?php echo get_selected($_GET["od_status"], '반품승인'); ?>>반품승인</option>   
				<option value="반품반려" <?php echo get_selected($_GET["od_status"], '반품반려'); ?>>반품반려</option>   
				<option value="반품완료" <?php echo get_selected($_GET["od_status"], '반품완료'); ?>>반품완료</option>  
			</select>
			
        </td>
		<th scope="row">일반정기</th>
        <td>
			<select name="od_period_yn" id="od_period_yn" >
				<option value="">==전체==</option>
				<option value="N" <?php echo get_selected($od_period_yn, 'N'); ?>>일반주문</option>
				<option value="Y" <?php echo get_selected($od_period_yn, 'Y'); ?>>정기주문</option>

			</select>
        </td>
		<th scope="row">주문구분</th>
        <td>
           <select name="od_gubun" id="od_gubun" >
				<option value="">==전체==</option>
				<option value="1" <?php echo get_selected($od_gubun, '1'); ?>>고객주문</option>
				<option value="2" <?php echo get_selected($od_gubun, '2'); ?>>약국주문</option>
				<option value="3" <?php echo get_selected($od_gubun, '3'); ?>>일반주문</option>
                <option value="9" <?php echo get_selected($od_gubun, '9'); ?>>사내주문</option>
			</select>
        </td>
    </tr>
	<tr>

        
		<th scope="row">검색</th>
        <td>
            <select name="sel_field" id="sel_field">
				<option value="od_id" <?php echo get_selected($sel_field, 'od_id'); ?>>주문번호</option>			
				<option value="od_name" <?php echo get_selected($sel_field, 'od_name'); ?>>주문자</option>
				<option value="it_name" <?php echo get_selected($sel_field, 'it_name'); ?>>상품명</option>
				<option value="mb_id" <?php echo get_selected($sel_field, 'mb_id'); ?>>아이디</option>   
			</select>
			<label for="search" class="sound_only">검색어<strong class="sound_only"> 필수</strong></label>
			<input type="text" name="search" value="<?php echo $search; ?>" id="search" class=" frm_input" autocomplete="off">
        </td>
		


    </tr>
	
    </tbody>
    </table>
</div>


</form>

<form name="forderlist" id="forderlist" method="post" autocomplete="off">
<input type="hidden" name="search_od_status" value="<?php echo $od_status; ?>">
<input type="hidden" name="command" value='1' id="command">

<div class="tbl_head01 tbl_wrap">
    <table id="sodr_list">
    <caption>주문 내역 목록</caption>
    <thead>
    <tr>
        <th scope="col" id="th_ordnum">주문번호</th>
		<th scope="col" id="th_odrer">약국(ID)</th>		
        <th scope="col" id="th_odrer">주문자</th>		
        <th scope="col" id="th_recvr">받는분</th>
		<th scope="col" >
            <label for="chkall" class="sound_only">주문 전체</label>
            <input type="checkbox" name="chkall" value="1" id="chkall" onclick="check_all(this.form)">확정
        </th>
		<th scope="col" id="th_recvr">
			<input type="checkbox" name="chkall2" value="1" id="chkall2" onclick="check_all2(this.form)" >주문상품
		</th>
		<th scope="col" id="th_recvr">수량</th>
		<th scope="col" id="th_recvr">처리상태</th>	
		<? 
		//푸드나무 공급사일때 자체상품코드 처리
		if( $comp_code == "405" ){ ?>
			<th scope="col" >자체사용 상품코드</th>
		<? } ?>

		<? 
		//푸른솔푸드 공급사일때 출고여부 처리
		if( $comp_code == "268" ){ ?>
		<th scope="col" ><input type="checkbox" name="chkall3" value="1" id="chkall3" onclick="check_all3(this.form)" ><br>출고여부</th>
		<? } ?>
    </tr>

    </thead>
    <tbody>
    <?php
    //for ($i=0; $row=sql_fetch_array($result); $i++)
	$i=0;
	foreach ($order as $key => $row) {
		
        $s_receipt_way = $s_br = "";
        if ($row['od_settle_case'])
        {
            $s_receipt_way = $row['od_settle_case'];
            $s_br = '<br />';

            // 간편결제
            if($row['od_settle_case'] == '간편결제') {
                switch($row['od_pg']) {
                    case 'lg':
                        $s_receipt_way = 'PAYNOW';
                        break;
                    case 'inicis':
                        $s_receipt_way = 'KPAY';
                        break;
                    case 'kcp':
                        $s_receipt_way = 'PAYCO';
                        break;
                    default:
                        $s_receipt_way = $row['od_settle_case'];
                        break;
                }
            }
        }
        else
        {
            $s_receipt_way = '결제수단없음';
            $s_br = '<br />';
        }

        if ($row['od_receipt_point'] > 0)
            $s_receipt_way .= $s_br."포인트";

        $od_cnt = 0;
        if ($row['mb_id'])
        {
            $sql2 = " select count(*) as cnt from {$g5['g5_shop_order_table']} where mb_id = '{$row['mb_id']}' ";
            $row2 = sql_fetch($sql2);
            $od_cnt = $row2['cnt'];
        }

        // 주문 번호에 device 표시
        $od_mobile = '';
        if($row['od_mobile'])
            $od_mobile = '(M)';

        // 주문번호에 - 추가
        switch(strlen($row['od_id'])) {
            case 16:
                $disp_od_id = substr($row['od_id'],0,8).'-'.substr($row['od_id'],8);
                break;
            default:
                $disp_od_id = substr($row['od_id'],0,8).'-'.substr($row['od_id'],8);
                break;
        }

		switch($row['od_gubun']) {
			case '1':
				$od_gubun = '고객주문';
				break;
			case '2':
				$od_gubun = '약국주문';
				break;
			case '3':
				$od_gubun = '일반주문';
				break;
            case '9':
                $od_gubun = '사내주문';
                break;
		}
		switch ($row['od_pay_yn'] ) {
			case "N" :
				$od_pay_yn = "미입금";
				break;
			case "Y" :
				$od_pay_yn = "입금";
				break;
		}


		$od_id = $row['od_id'];
		$od_count = $row['od_count'];
		$comp_code = $row['comp_code'];

		$RowSpan = true;
		if ( $tmp_od_id == $od_id && $tmp_comp_code == $comp_code ) {
			$RowSpan = false;
		}

		$tmp_od_id = $od_id;
		$tmp_comp_code = $comp_code;

		$RowSpan2 = true;
		if ( $tmp_od_num == $od_num ) {
			$RowSpan2 = false;
		}

		$tmp_od_num = $od_num;

		$od_check_disabled = "";
		if ( $od_status != "확정"  ) {
			$od_check_disabled = "disabled";
		}

    ?>

    <tr class="tr<?php echo $od_id; ?>" onMouseOver="trColorChange(this, 'Over')" onMouseOut="trColorChange(this, 'Out')">
		
		<?php if ( $RowSpan ) { ?>
        <td rowspan="<?php echo $od_count; ?>"   headers="th_ordnum"  >		
            <a href="./orderform.php?od_id=<?php echo $row['od_id']; ?>&amp;<?php echo $qstr; ?>" >			
			 <?php echo $disp_od_id; ?>
            <?php echo $od_mobile; ?><br/>
			<?php echo substr($row['od_date1'],11); ?>
			</a>
              

        </td>
		<td rowspan="<?php echo $od_count; ?>" headers="th_odrer" class="td_name ">
		    
			
			<?php if ($row['mb_id']) { ?>
				<?php echo $row['pharm_name']; ?><br/>
				<?php echo $row['mb_id']; ?><br/>


            <?php } else { ?>
            비회원
            <?php } ?>
		</td>
		<td rowspan="<?php echo $od_count; ?>"  >
			<?php echo $row['od_name']; ?>(<?php echo $row['pharm_custno']; ?>)
			<div>
				<span class="icon_order_gubun<?php echo $row['od_gubun'];?>"><?php echo $od_gubun; ?></span>
			</div>					
		</td>
        <td rowspan="<?php echo $od_count; ?>" ><?php echo get_text($row['od_b_name']); ?></a></td>

		<?php } ?>
		
		<td width="60">
		<input type="hidden" name="od_id[<?php echo $i ?>]" value="<?php echo $row['od_id'] ?>" id="od_id_<?php echo $i ?>">
		<input type="hidden" name="od_num[<?php echo $i ?>]" value="<?php echo $row['od_num'] ?>" id="od_num_<?php echo $i ?>">
		<input type="hidden" name="od_seq[<?php echo $i ?>]" value="<?php echo $row['od_seq'] ?>" id="od_seq_<?php echo $i ?>">
        <input type="hidden" name="od_gubun[<?php echo $i ?>]" value="<?php echo $row['od_gubun'] ?>" id="od_gubun_<?php echo $i ?>">
        <input type="hidden" name="od_pay[<?php echo $i ?>]" value="<?php echo $row['od_pay_yn'] ?>" id="od_pay<?php echo $i ?>">
        <input type="hidden" name="current_settle_case[<?php echo $i ?>]" value="<?php echo $row['od_settle_case'] ?>" id="current_settle_case_<?php echo $i ?>">

			<?php if ($row[od_status] != '취소') { ?>
				<?php if ($row[od_status] == '주문') {
						if ($row[od_settle_case] == "가상계좌" and $row[od_pay_yn] == "N") {?>
							입금확인중
						<?php } else { ?>
							
							
							<label for="chk_<?php echo $i; ?>" class="sound_only">주문번호 <?php echo $row['od_id']; ?></label>
							<input type="checkbox" name="chk[]" value="<?php echo $i ?>" id="chk_<?php echo $i ?>">
							<?php } ?>
				<?php } else { ?>
					
					<font color="#0000FF">확정</font>
				<?php } ?>
		<?php } else { ?>
			<font color="#FF0000">취소</font>
		<?php } ?>
			
        </td>
		<td class="td_left ">
			<input type="checkbox" name="trans_chk[]" value="<?php echo $i;?>" onclick="TransOpen()" <?php echo $od_check_disabled;?>  >

			<?php echo $row['it_name']; ?>
			<?php if ($row['od_period_yn'] == 'Y') { ?>
				<span >( <?php echo $row['od_seq'];?>회 - <?php echo $row['od_period_date'];?> ]</span>
			<?php } ?>
			<br/>
			<span><font color="blue"><?php echo $row['od_option']; ?></font></span>
			<div id="od_delivery<?php echo $i; ?>" style="display:none;padding:5px 0 0 5px;">
				<select name="od_delivery_company[<?php echo $i; ?>]">
					<option value="">== 선택 ==</option>
					<?php echo get_delivery_company($delivery_company); ?>
				</select>
				<input type="text" name="od_invoice[<?php echo $i; ?>]" value="<?php echo $row['od_invoice']; ?>" class="frm_input2" size="20">
			  </div>

			
        </td>
	
		<td width="50" class="td_num_right "><?php echo $row['od_qty']; ?></td>
		
		<td width="100">
			<?
				if( $row['od_return_confirm'] == "Y" && $row['od_status'] == "반품접수" ){
					echo "<font color='red'>반품승인</font>";
				}else{
					echo $row['od_status'];
				}
			?>
		</td>
		<? 
		//푸드나무 공급사일때 자체상품코드 처리
		if( $comp_code == "405" && $RowSpan ){ ?>
		<td rowspan="<?php echo $od_count; ?>"  style="width:150px"; >
				<?php echo $row['comp_it_id'] ?>
		</td>		
		<? } ?>		
		<? 
		//푸른솔푸드 공급사일때 출고여부 처리
		if( $comp_code == "268" && $RowSpan ){ ?>
		<td width="60">
		<? if( $row['it_id'] == "S00603" || $row['it_id'] == "S00604" ){ ?>
			<? if( $row['od_release_yn'] == "Y" ){ ?>
			출고
			<? }else{ ?>
			미출고 <input type="checkbox" name="chk_release[]" value="<?php echo $i ?>" id="chk_<?php echo $i ?>">
			<? } ?>
		<? } ?>
		</td>
        <? } ?>		
    </tr>

    <?php
		$i++;
    }
    sql_free_result($result);

	if ($od_status != "완료" and $od_status != "취소") {?>
	<tr>
	  <td colspan="4" align="left">
	  	
	  </td>
      <td><?php if ($od_status== "주문" or $od_status== "" ) { ?>
			<div class="local_cmd01 ">
			<input type="button" value="선택확정" class="btn_submit" onclick="forderlist_submit(1);">   
			</div>

		<?php } ?></td>
	 <td class="td_left " colspan="3">
	  <?php if ($od_status== "확정" ) { ?>
        선택한 주문건을 
	  	<select name="Change_ODR_STATUS" id="Change_ODR_STATUS" onChange="Change_Order_Status()">
			<option value="" selected="selected" width="150">==선택==</option>
			<option value="배송">배송중</option>
		</select>
		(으)로 변경
		<span class="local_cmd01 " id="TransBtn1">
			<input type="button" value="송장번호저장" class="btn_submit" onclick="forderlist_submit(3);">    
		</span>
	  <?php } ?>
		
	  </td>
	  <? 
	  //푸드나무 공급사일때 자체상품코드 처리
	  if( $comp_code == "405" ){ ?>
	  <td></td>
	  <? } ?>
	  <? 
	  //푸른솔푸드 공급사일때 출고여부 처리
	  if( $comp_code == "268" ){ ?>
	  <td><input type="button" value="선택출고" class="btn_submit" onclick="releaselist_submit();">   </td>
	  <? } ?>

	</tr>
<?php }

    if ($i == 0)
        echo '<tr><td colspan="14" class="empty_table">자료가 없습니다.</td></tr>';
    ?>

    </tbody>

    </table>
</div>



</form>
<?php

?>

<?php //$qstr ='comp_code='.$_GET['comp_code'].'&'.$qstr;
echo get_paging(G5_IS_MOBILE ? $config['cf_mobile_pages'] : $config['cf_write_pages'], $page, $total_page, "{$_SERVER['SCRIPT_NAME']}?$qstr&amp;page="); ?>

<script>

function TransOpen() {
	var f = document.forderlist;
	if (f.Change_ODR_STATUS.value == "배송"){
		var chk = document.getElementsByName("trans_chk[]");
		for (var i=0; i<chk.length; i++)
		{
			if (chk[i].checked)
			{
				var k = chk[i].value;
				$('#od_delivery'+k).css('display','');
			} else {
				var k = chk[i].value;
				$('#od_delivery'+k).css('display','none');
			}
		}
	}
}
function Change_Order_Status(){

	var f = document.forderlist;

	if (!is_checked("trans_chk[]")) {
		alert("처리 하실 항목을 하나 이상 선택하세요.");
		return false;
	}
	var chk = document.getElementsByName("trans_chk[]");
	for (var i=0; i<chk.length; i++)
	{
		if (chk[i].checked)
		{
			var k = chk[i].value;
			$('#od_delivery'+k).css('display','');
		}
	}	
}

function trColorChange(id, chk) {
	var ID_Len = $("." + id.className).length;

	if (chk == "Over") {
		$("." + id.className).each(function (index, element) {
			$(this).css("background-color", "#F7F7F7");
		});
	} else {
		$("." + id.className).each(function (index, element) {
			$(this).css("background-color", "#FFFFFF");
		});
	}
}


$(function(){
    $("#fr_date, #to_date").datepicker({ changeMonth: true, changeYear: true, dateFormat: "yy-mm-dd", showButtonPanel: true, yearRange: "c-99:c+99", maxDate: "+0d" });

    // 주문상품보기
    $(".orderitem").on("click", function() {
        var $this = $(this);
        var od_id = $this.text().replace(/[^0-9]/g, "");

        if($this.next("#orderitemlist").size())
            return false;

        $("#orderitemlist").remove();

        $.post(
            "./ajax.orderitem.php",
            { od_id: od_id },
            function(data) {
                $this.after("<div id=\"orderitemlist\"><div class=\"itemlist\"></div></div>");
                $("#orderitemlist .itemlist")
                    .html(data)
                    .append("<div id=\"orderitemlist_close\"><button type=\"button\" id=\"orderitemlist-x\" class=\"btn_frmline\">닫기</button></div>");
            }
        );

        return false;
    });

    // 상품리스트 닫기
    $(".orderitemlist-x").on("click", function() {
        $("#orderitemlist").remove();
    });

    $("body").on("click", function() {
        $("#orderitemlist").remove();
    });

    // 엑셀배송처리창
    $("#order_delivery").on("click", function() {
        var opt = "width=600,height=450,left=10,top=10";
        window.open(this.href, "win_excel", opt);
        return false;
    });
});

function set_date(today)
{
    <?php
    $date_term = date('w', G5_SERVER_TIME);
    $week_term = $date_term + 7;
    $last_term = strtotime(date('Y-m-01', G5_SERVER_TIME));
    ?>
    if (today == "오늘") {
        document.getElementById("fr_date").value = "<?php echo G5_TIME_YMD; ?>";
        document.getElementById("to_date").value = "<?php echo G5_TIME_YMD; ?>";
    } else if (today == "어제") {
        document.getElementById("fr_date").value = "<?php echo date('Y-m-d', G5_SERVER_TIME - 86400); ?>";
        document.getElementById("to_date").value = "<?php echo date('Y-m-d', G5_SERVER_TIME - 86400); ?>";
    } else if (today == "이번주") {
        document.getElementById("fr_date").value = "<?php echo date('Y-m-d', strtotime('-'.$date_term.' days', G5_SERVER_TIME)); ?>";
        document.getElementById("to_date").value = "<?php echo date('Y-m-d', G5_SERVER_TIME); ?>";
    } else if (today == "이번달") {
        document.getElementById("fr_date").value = "<?php echo date('Y-m-01', G5_SERVER_TIME); ?>";
        document.getElementById("to_date").value = "<?php echo date('Y-m-d', G5_SERVER_TIME); ?>";
    } else if (today == "지난주") {
        document.getElementById("fr_date").value = "<?php echo date('Y-m-d', strtotime('-'.$week_term.' days', G5_SERVER_TIME)); ?>";
        document.getElementById("to_date").value = "<?php echo date('Y-m-d', strtotime('-'.($week_term - 6).' days', G5_SERVER_TIME)); ?>";
    } else if (today == "지난달") {
        document.getElementById("fr_date").value = "<?php echo date('Y-m-01', strtotime('-1 Month', $last_term)); ?>";
        document.getElementById("to_date").value = "<?php echo date('Y-m-t', strtotime('-1 Month', $last_term)); ?>";
	} else if (today == "12개월") {
        document.getElementById("fr_date").value = "<?php echo date('Y-m-01', strtotime('-1 Year', $last_term)); ?>";
        document.getElementById("to_date").value = "<?php echo date('Y-m-d', G5_SERVER_TIME); ?>";
    } else if (today == "전체") {
        document.getElementById("fr_date").value = "";
        document.getElementById("to_date").value = "";
    }
}
</script>

<script>

function check_all2(f)
{
    var chk = document.getElementsByName("trans_chk[]");

    for (i=0; i<chk.length; i++) {
		if (!chk[i].disabled){
			chk[i].checked = f.chkall2.checked;
		}        
	}
	TransOpen();
}
function check_all3(f)
{
    var chk = document.getElementsByName("chk_release[]");

    for (i=0; i<chk.length; i++) {
		if (!chk[i].disabled){
			chk[i].checked = f.chkall3.checked;
		}        
	}
}
function forderlist_submit(action)
{
	
	var command = document.getElementById("command");
	command.value = action;
	
	var f = document.forderlist;
	
	if(action ==1) {
		if (!is_checked("chk[]")) {
			alert("확정 하실 항목을 하나 이상 선택하세요.");
			return false;
		}

        var chk2 = document.getElementsByName("chk[]");
		var chk2_str = '선택하신 주문 중 미입금 주문이 있습니다. 미입금 주문건은 입금 확인 후 확정 가능합니다.\r\n\n' + '[미입금 주문번호 리스트]';
		var chk2_flag = false;
		var prev_od_id = '';

        for (var j=0; j<chk2.length; j++)
        {
            if (chk2[j].checked)
            {
                var m = chk2[j].value;

                // 주문정보
                var current_settle_case = f.elements['current_settle_case['+m+']'].value;
                var od_gubun      = f.elements['od_gubun['+m+']'].value;
                var od_pay = f.elements['od_pay['+m+']'].value;
                var od_id = f.elements['od_id['+m+']'].value;

                if (od_gubun == 3 && current_settle_case == '무통장' && od_pay == 'N' && od_id != prev_od_id) {
                    chk2_str += '\r\n' + od_id;
                    chk2_flag = true;
                    prev_od_id = od_id;
                }
            }
        }

        if (chk2_flag == true) {
            alert(chk2_str);
            return false;
        }

		if (!confirm("선택하신 주문서의 주문상태를 확정 상태로 변경하시겠습니까?"))
			return false;
	
	}
	if(action ==3) {

		if (!is_checked("trans_chk[]")) {
			alert("처리 하실 항목을 하나 이상 선택하세요.");
			return false;
		}
		//if (!confirm("선택하신 주문서의 주문상태를 배송중 상태로 변경하시겠습니까?"))
		//	return false;

		var chk = document.getElementsByName("trans_chk[]");

		for (var i=0; i<chk.length; i++)
		{
			if (chk[i].checked)
			{
				var k = chk[i].value;
				//var current_settle_case = f.elements['current_settle_case['+k+']'].value;
				//var current_status = f.elements['current_status['+k+']'].value;
				
				var invoice      = f.elements['od_invoice['+k+']'];					   
				var delivery_company = f.elements['od_delivery_company['+k+']'];
				if ($.trim(delivery_company.value) == '') {
					alert("배송업체를 입력하시기 바랍니다.");
					delivery_company.focus();
					return false;
				}
				if ($.trim(invoice.value) == '') {
					alert("운송장번호를 입력하시기 바랍니다.");
					invoice.focus();
					return false;
				}				
			}
		}
		if (!confirm("선택하신 주문서를 배송중 상태로 변경하시겠습니까?"))
			return false;
	}
	/*if(action ==2) {
		if (!is_checked("chkcash[]")) {
			alert("입금처리 하실 항목을 하나 이상 선택하세요.");
			return false;
		}
		if (!confirm("선택하신 주문서를 입금처리 상태로 변경하시겠습니까?"))
			return false;
	
	}
	*/
    f.action = "./orderlistupdate.php";
	f.submit();
    return true;
}
function releaselist_submit()
{
	var f = document.forderlist;

	if (!is_checked("chk_release[]")) {
		alert("확정 하실 항목을 하나 이상 선택하세요.");
		return false;
	}
	if (!confirm("선택하신 주문서를 출고로 변경하시겠습니까?"))
		return false;

    f.action = "./releaselistupdate.php";
	f.submit();
    return true;
}
</script>
<div id="Excel_Select" title="엑셀 출력 선택">
	<form name="ExcelFrm" id="ExcelFrm" action="orderprintresultlistvender.php?<?php echo $qstr; ?>" method="post" target="iFrm">
		<table class="table_style1">
			<tr>
				<td width="150">선택</td>
				<td>
					<!--
					<input type="radio" name="ptype" value="1" id="ExcelParamSelect1"><label for="ExcelParamSelect1">전체</label>
				    <br/>
					-->
					<input type="radio" name="ptype" value="2" checked id="ExcelParamSelect2"><label for="ExcelParamSelect2">송장발행</label>
				</td>
			</tr>
			
		</table>
	</form>
</div>
<script>
	$("#Excel_Select").dialog({
		autoOpen: false,
		resizable: false,
		modal: true,
		width: 800,
		buttons: {
			"엑셀다운로드": function () {
				$("#ExcelFrm").submit();
				$(this).dialog("close");
			},
			Cancel: function () {
				$(this).dialog("close");
			}
		}
	});

	$("#ExcelDownButton").click(function () {
		$("#Excel_Select").dialog("open");
	});

	
</script>
<!-- (s)hidden Frame -->
<iframe name="iFrm" style="display: none;" ></iframe>


<?php
include_once (G5_ADMIN_PATH.'/admin.tail.php');
?>

<?php
include_once('./_common.php');

/*if (check_admin_token() == true) {

    if ($_POST['od_id']) {

        $length = 6;
        $characters = '0123456789abcdefghijklmnopqrstuvwxyz';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[mt_rand(0, $charactersLength - 1)];
        }
        echo $randomString;

    }

} else {
    echo 'not token';
}*/
//$od_id = '2021021500033';
$result = true;

/*************** 트랜잭션 관련 ****************/
$error_cnt = 0;
mysqli_autocommit($g5['connect_db'], false);
/*************** 트랜잭션 관련 ****************/

$sql = "select * from {$g5['g5_shop_order_table']} where od_id='{$od_id}'";
$od = sql_fetch($sql);

if ($od) {

    $length = 6; // 비밀번호 자리수 설정
    $characters = '0123456789abcdefghijklmnopqrstuvwxyz'; // 소문자, 숫자 설정
    $charactersLength = strlen($characters);
    $randomPassword = '';
    for ($i = 0; $i < $length; $i++) {
        $randomPassword .= $characters[mt_rand(0, $charactersLength - 1)];
    }
    //echo $randomPassword;

    $sql = "update {$g5['g5_shop_order_table']} set od_pwd='".get_encrypt_string($randomPassword)."' where od_id='$od_id' ";
//    echo $sql;
    sql_query($sql);

    /*************** 트랜잭션 관련 ****************/
    if (mysqli_errno($g5['connect_db'])) {
        $error_cnt += 1;
    }
    /*************** 트랜잭션 관련 ****************/
    //echo $error_cnt;
    if ($error_cnt > 0) {
        mysqli_rollback($g5['connect_db']);
        mysqli_close($g5['connect_db']);

        $result = false;
        echo $result;
        exit;
    } else {
        /************* 관리자 로그 처리 START *************/
        $al_data['od_pwd'] = $randomPassword;
        insert_admin_log(400, 400400, '주문 비밀번호 재발급', '', $od_id, '', $_SERVER['REQUEST_URI'], $al_data);
        /************* 관리자 로그 처리 END *************/

        $recv_name = $od['od_name'];
        $recv_number = $od['od_hp'];

        include_once(G5_LIB_PATH.'/kakao.alimtalk.lib.php');
        // AlimTalk BEGIN --------------------------------------------------------
        $alimtalk = new AlimTalk();
        $to = $alimtalk->set_phone_number($recv_number);

        $template_code = 'orderstation_order_12';
        $text = $alimtalk->get_template($template_code);
        $text = str_replace('#{이름}', $recv_name, $text);
        $text = str_replace('#{주문번호}', $od_id, $text);
        $text = str_replace('#{주문비밀번호}', $randomPassword, $text);

        $alimtalk->set_message($template_code, $to, $text);
        $alimtalk->send();
        // AlimTalk END   --------------------------------------------------------

        echo $result;

        mysqli_commit($g5['connect_db']);
    }

}
<?php
$sub_menu = '600670';
include_once('./_common.php');

if( empty($f_idx) ){
	echo "<script>
			alert('파일idx 고유값이 없습니다.');
			parent.location.reload();
		  </script>";
	exit;	
}

/*************** 트랜잭션 관련 ****************/
$error_cnt = 0;
$ErrorMsg = "";
mysqli_autocommit($g5['connect_db'], false);
/*************** 트랜잭션 관련 ****************/

$sql = " 
	insert into tbl_shop_item( 
		comp_code , 
		it_id , 
		ca_id , ca_id2 , ca_id3 , it_name , it_web_view , it_app_view , it_multisend , it_point_type , it_brand , it_model , it_tel_inq , it_use , 
		it_status , it_desc , it_search , it_health_info , it_price , it_soldout , it_stock_qty , it_notax , it_supply_price , it_dodome_price , 
		it_drug_price , it_incen , it_app_drug_price , it_app_sale_price , it_app_incen , it_sc_type , it_sc_method , it_sc_price , it_sc_minimum ,
		it_trans_bundle , it_order_close_time , it_release_day , it_period_yn , it_time , it_update_time , 
		it_basic , it_head_html , it_tail_html , it_mobile_head_html , it_mobile_tail_html , it_use_avg , 
		it_shop_memo , comp_it_id , it_info_gubun , it_info_value , f_idx , 
		it_type1 , it_type2 , it_type3 , it_type4 , it_explan , it_sellingpoint , it_mobile_explan , it_cust_price , 
		it_noti_qty , it_buy_min_qty , it_buy_max_qty , it_app_norm_price
	)	
	select 
		comp_code , 
		case when it_id >= 10000 then CONCAT( 'S', it_id) 
			 when it_id >= 1000 then CONCAT( 'S0', it_id) 
			 when it_id <= 999 then CONCAT( 'S00', it_id) 
		END AS it_id, 
		ca_id , ca_id2 , ca_id3 , it_name , it_web_view , it_app_view , it_multisend , it_point_type , it_brand , it_model , it_tel_inq , it_use , 
		it_status , it_desc , it_search , it_health_info , it_price , it_soldout , it_stock_qty , it_notax , it_supply_price , it_dodome_price , 
		it_drug_price , it_incen , it_app_drug_price , it_app_sale_price , it_app_incen , it_sc_type , it_sc_method , it_sc_price , it_sc_minimum ,
		it_trans_bundle , it_order_close_time , it_release_day , it_period_yn , it_time , it_update_time , 
		it_basic , it_head_html , it_tail_html , it_mobile_head_html , it_mobile_tail_html , it_use_avg , 
		it_shop_memo , comp_it_id , it_info_gubun , it_info_value , f_idx , 
		it_type1 , it_type2 , it_type3 , it_type4 , it_explan , it_sellingpoint , it_mobile_explan , it_cust_price , 
		it_noti_qty , it_buy_min_qty , it_buy_max_qty , it_app_norm_price
	from(

		select 
			comp_code , 
			( SELECT CAST( REPLACE( MAX(it_id) , 'S' , '' ) AS UNSIGNED ) FROM tbl_shop_item WHERE SUBSTR(it_id,1,1) = 'S' ) + (@rownum:=@rownum+1) AS it_id , 
			ca_id , ca_id2 , ca_id3 , it_name , it_web_view , it_app_view , it_multisend , it_point_type , it_brand , it_model , it_tel_inq , it_use , 
			it_status , it_desc , it_search , it_health_info , it_price , it_soldout , it_stock_qty , it_notax , it_supply_price , it_dodome_price , 
			it_drug_price , it_incen , it_app_drug_price , it_app_sale_price , it_app_incen , it_sc_type , it_sc_method , it_sc_price , it_sc_minimum ,
			it_trans_bundle , it_order_close_time , it_release_day , it_period_yn , it_time , it_update_time , 
			it_basic , it_head_html , it_tail_html , it_mobile_head_html , it_mobile_tail_html , it_use_avg , 
			it_shop_memo , comp_it_id , it_info_gubun , it_info_value , {$f_idx} as f_idx , 
			it_type1 , it_type2 , it_type3 , it_type4 , it_explan , it_sellingpoint , it_mobile_explan , it_cust_price , 
			it_noti_qty , it_buy_min_qty , it_buy_max_qty , it_app_norm_price
		from tbl_shop_item_excel , (SELECT @rownum:=0) TMP where f_idx = {$f_idx}

	) as X ";

sql_query($sql);
/*************** 트랜잭션 관련 ****************/
if( mysqli_errno($g5['connect_db']) ){
	$error_cnt += 1;
}
/*************** 트랜잭션 관련 ****************/

/************* 관리자 로그 처리 START *************/
$al_data_backup = sql_fetch_result("select * from tbl_shop_item_excel where f_idx = {$f_idx}");
insert_admin_log(600,600670, '상품 등록', '', $f_idx, '', $_SERVER['REQUEST_URI'], $al_data_backup);
/************* 관리자 로그 처리 END *************/

//상품이동이 완료되었다면 해당 idx값의데이터를 지워준다.
$sql = " delete from tbl_shop_item_excel where f_idx = {$f_idx} ";
sql_query($sql);
/*************** 트랜잭션 관련 ****************/
if( mysqli_errno($g5['connect_db']) ){
	$error_cnt += 1;
}
/*************** 트랜잭션 관련 ****************/		


/*************** 트랜잭션 관련 ****************/
if ($error_cnt > 0) {
	mysqli_rollback($g5['connect_db']);
	mysqli_close($g5['connect_db']);
	echo "
	<script>
		alert('데이터베이스의 에러로 인해 롤백되었습니다.');
		parent.location.reload();
	</script>
	";
	exit;
} else {
	mysqli_commit($g5['connect_db']);
} 	
/*************** 트랜잭션 관련 ****************/

echo "<script>
		alert('상품이동이 완료되었습니다.');
		parent.location.reload();
	  </script>";
exit;

?>
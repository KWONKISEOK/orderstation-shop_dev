<?php
$sub_menu = '400407';
include_once('./_common.php');

auth_check($auth[$sub_menu], "r");

$g5['title'] = '사내주문';
include_once (G5_ADMIN_PATH.'/admin.head.php');

if( $member['mb_id'] != "admin" ){
	echo "최고 관리자만 접속 가능한 메뉴입니다.";
	exit;
}

// 분류
$ca_list  = '<option value="">선택</option>'.PHP_EOL;
$sql = " select * from {$g5['g5_shop_category_table']} ";
if ($is_admin != 'super')
    $sql .= " where ca_mb_id = '{$member['mb_id']}' ";
$sql .= " order by ca_order, ca_id ";
$result = sql_query($sql);
for ($i=0; $row=sql_fetch_array($result); $i++)
{
    $len = strlen($row['ca_id']) / 2 - 1;
    $nbsp = '';
    for ($i=0; $i<$len; $i++) {
        $nbsp .= '&nbsp;&nbsp;&nbsp;';
    }
    $ca_list .= '<option value="'.$row['ca_id'].'">'.$nbsp.$row['ca_name'].'</option>'.PHP_EOL;
}

$where = " and ";
$sql_search = "";
if ($stx != "") {
    if ($sfl != "") {
        $sql_search .= " $where $sfl like '%$stx%' ";
        $where = " and ";
    }
    if ($save_stx != $stx)
        $page = 1;
}

if ($sca != "") {
    $sql_search .= " $where (a.ca_id like '$sca%' or a.ca_id2 like '$sca%' or a.ca_id3 like '$sca%') ";
}

if ($sfl == "")  $sfl = "it_name";

$sql_common = " from {$g5['g5_shop_item_table']} a                    
                     left join {$g5['g5_shop_category_table']} b on b.ca_id = a.ca_id
					 left join tbl_member c on c.comp_code = a.comp_code
               where c.mb_type = 7";

if ($comp_code != '')
$sql_common .= " and a.comp_code = '$comp_code' ";

if ($it_use != '')
$sql_common .= " and a.it_use ='$it_use' ";

$sql_common .= $sql_search;

// 테이블의 전체 레코드수만 얻음
$sql = " select count(*) as cnt " . $sql_common;
$row = sql_fetch($sql);
$total_count = $row['cnt'];

$rows = $config['cf_page_rows'];
$total_page  = ceil($total_count / $rows);  // 전체 페이지 계산
if ($page < 1) { $page = 1; } // 페이지가 없으면 첫 페이지 (1 페이지)
$from_record = ($page - 1) * $rows; // 시작 열을 구함

if (!$sst) {
    $sst  = "it_time";
    $sod = "desc";
}
$sql_order = "order by $sst $sod";

$sql  = " select *
           $sql_common
           $sql_order
           limit $from_record, $rows ";
$result = sql_query($sql);
//echo $sql;


//$qstr  = $qstr.'&amp;sca='.$sca.'&amp;page='.$page;
$qstr  = $qstr.'&amp;comp_code='.$comp_code.'&amp;sca='.$sca.'&amp;page='.$page.'&amp;save_stx='.$stx;

$listall = '<a href="'.$_SERVER['SCRIPT_NAME'].'" class="ov_listall">전체목록</a>';
?>

<div class="local_ov01 local_ov">
    <?php echo $listall; ?>
    <span class="btn_ov01"><span class="ov_txt">등록된 상품</span><span class="ov_num"> <?php echo $total_count; ?>건</span></span>
</div>

<form name="flist" class="local_sch01 local_sch">
<input type="hidden" name="page" value="<?php echo $page; ?>">
<input type="hidden" name="save_stx" value="<?php echo $stx; ?>">


<div class="local_sch03  tbl_wrap" style="padding:10px;">
    <table>
   
    <tbody>
	<tr>
        <th scope="row">업체선택</th>
        <td>
			<select name="comp_code" id="comp_code" onchange="this.form.submit()">
				<option value="">==업체선택==</option>
				<?php
				$sql2 = " select comp_code, comp_name from tbl_member where mb_type='7' order by comp_name ";
				$result2 = sql_query($sql2);
				for ($i=0; $row2=sql_fetch_array($result2); $i++) {
				   if($comp_code ==$row2['comp_code']) 
						$checked='selected';
					else 
						$checked ='';

					echo '<option value="'.$row2['comp_code'].'" '.$checked.' >'.$row2['comp_name'].'</option>'.PHP_EOL;
				}
				?>
			</select>
			
        </td>
		<th scope="row">판매여부</th>
        <td>
            <select name="it_use" id="it_use" onchange="this.form.submit()" >
				<option value="">==전체==</option>
				<option value="1" <?php echo get_selected($it_use, '1'); ?>>예</option>
				<option value="0" <?php echo get_selected($it_use, '0'); ?>>아니오</option>
			</select>
        </td>
		
		<th scope="row">상품분류</th>
        <td>
           <select name="sca" id="sca">
				<option value="">전체분류</option>
				<?php
				$sql1 = " select ca_id, ca_name from {$g5['g5_shop_category_table']} order by ca_order, ca_id ";
				$result1 = sql_query($sql1);
				for ($i=0; $row1=sql_fetch_array($result1); $i++) {
					$len = strlen($row1['ca_id']) / 2 - 1;
					$nbsp = '';
					for ($i=0; $i<$len; $i++) $nbsp .= '&nbsp;&nbsp;&nbsp;';
					echo '<option value="'.$row1['ca_id'].'" '.get_selected($sca, $row1['ca_id']).'>'.$nbsp.$row1['ca_name'].'</option>'.PHP_EOL;
				}
				?>
			</select>
        </td>
		<th scope="row">검색</th>
        <td>
            <select name="sfl" id="sfl">
				<option value="it_name" <?php echo get_selected($sfl, 'it_name'); ?>>상품명</option>
				<option value="it_id" <?php echo get_selected($sfl, 'it_id'); ?>>상품코드</option>
				<option value="it_maker" <?php echo get_selected($sfl, 'it_maker'); ?>>제조사</option>
			</select>
			<label for="stx" class="sound_only">검색어</label>
			<input type="text" name="stx" value="<?php echo $stx ?>" id="stx" class=" frm_input">
        </td>

		<td>
			 <input type="submit" value="검색" class="get_theme_confc btn btn_01">

		</td>
		
    </tr>
	
    </tbody>
    </table>
</div>

</form>

<form name="fitemlistupdate" method="post" action="./orderlistp1_update.php" onsubmit="return fitemlist_submit(this);" autocomplete="off" id="fitemlistupdate">
<input type="hidden" name="sca" value="<?php echo $sca; ?>">
<input type="hidden" name="sst" value="<?php echo $sst; ?>">
<input type="hidden" name="sod" value="<?php echo $sod; ?>">
<input type="hidden" name="sfl" value="<?php echo $sfl; ?>">
<input type="hidden" name="stx" value="<?php echo $stx; ?>">
<input type="hidden" name="page" value="<?php echo $page; ?>">

<div class="tbl_head01 tbl_wrap">
    <table>
    <caption><?php echo $g5['title']; ?> 목록</caption>
    <thead>
    <tr>
        <th scope="col">
            <label for="chkall" class="sound_only">상품 전체</label>
            <input type="checkbox" name="chkall" value="1" id="chkall" onclick="check_all(this.form)">
        </th>
        <th scope="col">상품코드</th>
		<th scope="col">공급사명</th>
       
        
        <th scope="col" id="th_img">이미지</th>
        <th scope="col" id="th_pc_title">상품명</th>
		<th scope="col" id="th_qty">수량</th>
        <th scope="col" id="th_amt">판매가</th>
		 <th scope="col" >판매상태</th>
     
    </tr>
    </thead>
    <tbody>
    <?php
    for ($i=0; $row=sql_fetch_array($result); $i++)
    {
        $href = G5_SHOP_URL.'/item.php?it_id='.$row['it_id'];
        $bg = 'bg'.($i%2);


    ?>
    <tr class="<?php echo $bg; ?>">
        <td class="td_chk">
            <label for="chk_<?php echo $i; ?>" class="sound_only"><?php echo get_text($row['it_name']); ?></label>
            <input type="checkbox" name="chk[]" value="<?php echo $i ?>" id="chk_<?php echo $i; ?>">
        </td>
        <td class="td_num">
            <input type="hidden" name="it_id[<?php echo $i; ?>]" value="<?php echo $row['it_id']; ?>">
            <?php echo $row['it_id']; ?>
        </td>
      
        <td>
           <?php echo $row['comp_name']; ?>
        </td>
        
      
        <td class="td_img"><a href="<?php echo $href; ?>"><?php echo get_it_image($row['it_id'], 50, 50); ?></a></td>
        <td headers="th_pc_title" style="text-align:left;">           
            <?php echo htmlspecialchars2(cut_str($row['it_name'],250, "")); ?>
        </td>
		<td>            
            <input type="text" name="ct_qty[<?php echo $i; ?>]" value="" size=10>
        </td>
        <td headers="th_amt" class="td_numbig td_input">
            <?php echo number_format($row['it_supply_price']); ?><br>
        </td>
		<td>
            
            <?php echo ($row['it_use'] ? '판매' : '판매안함'); ?> 


        </td>
    
    </tr>
    <?php
    }?>
	<tr>
	  <td colspan="2" align="left">
	  	
		<div class="local_cmd01 ">
		<!-- <input type="button" value="장바구니" class="btn_submit" onclick="forderlist_submit(1);">    -->
		<input type="submit" name="act_button" value="장바구니" onclick="document.pressed=this.value" class="btn btn_02">
		</div>

		</td>
		<td colspan="6" align="left">
		</td>

	</tr>
	<?php
    if ($i == 0)
        echo '<tr><td colspan="12" class="empty_table">자료가 한건도 없습니다.</td></tr>';
    ?>
    </tbody>
    </table>
</div>

<div class="btn_fixed_top">

	<!-- <input type="submit" name="act_button" value="장바구니" onclick="document.pressed=this.value" class="btn btn_02"> -->

	<a href="orderlistp2.php" class="btn btn_01"> 장바구니 이동</a>
	<a href="orderlistp3.php" class="btn btn_02"> 주문내역 이동</a>
   
</div>

</form>

<?php 
echo get_paging(G5_IS_MOBILE ? $config['cf_mobile_pages'] : $config['cf_write_pages'], $page, $total_page, "{$_SERVER['SCRIPT_NAME']}?$qstr&amp;page="); ?>

<script>

function fitemlist_submit(f)
{
    if (!is_checked("chk[]")) {
        alert("주문 하실 항목을 하나 이상 선택하세요.");
        return false;
    }

    return true;
}

$(function() {
    $(".itemcopy").click(function() {
        var href = $(this).attr("href");
        window.open(href, "copywin", "left=100, top=100, width=300, height=200, scrollbars=0");
        return false;
    });
});

function excelform(url)
{
    var opt = "width=600,height=450,left=10,top=10";
    window.open(url, "win_excel", opt);
    return false;
}
</script>

<?php
include_once (G5_ADMIN_PATH.'/admin.tail.php');
?>

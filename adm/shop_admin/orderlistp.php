<?php
$sub_menu = '400400';
include_once('./_common.php');

auth_check($auth[$sub_menu], "r");

alert('준비중입니다');

exit;

$g5['title'] = '주문내역';
include_once (G5_ADMIN_PATH.'/admin.head.php');
include_once(G5_PLUGIN_PATH.'/jquery-ui/datepicker.php');

$where = array();

$doc = strip_tags($doc);
$sort1 = in_array($sort1, array('od_id', 'od_cart_price', 'od_receipt_price', 'od_cancel_price', 'od_misu', 'od_cash')) ? $sort1 : '';
$sort2 = in_array($sort2, array('desc', 'asc')) ? $sort2 : 'desc';
$sel_field = get_search_string($sel_field);
if( !in_array($sel_field, array('od_id', 'mb_id', 'od_name', 'od_tel', 'od_hp', 'od_b_name', 'comp_name', 'it_name', 'pharm_name', 'pharm_custno')) ){   //검색할 필드 대상이 아니면 값을 제거
    $sel_field = '';
}
$od_status = get_search_string($od_status);
$search = get_search_string($search);
if(! preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $fr_date) ) $fr_date = '';
if(! preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $to_date) ) $to_date = '';
if($fr_date =='') $fr_date = date("Y-m-d", strtotime('-2 week'));
if($to_date =='') $to_date = date("Y-m-d");
$sql_search = "";
if ($search != "") {
    if ($sel_field != "") {
		if($sel_field == 'comp_name') {
			$where[] = "d.$sel_field like '%$search%' ";
		} else if($sel_field == 'it_name') {
			$where[] = "b.$sel_field like '%$search%' ";
		} else {
			$where[] = "a.$sel_field like '%$search%' ";
		}

    }

    if ($save_search != $search) {
        $page = 1;
    }
}

//날자구분
switch($od_date) {
	case '1':
		$where[] = " c.od_date1 between '$fr_date 00:00:00' and '$to_date 23:59:59' ";
		break;
	case '2':
		$where[] = " c.od_date2 between '$fr_date 00:00:00' and '$to_date 23:59:59' ";
		break;
	case '3':
		$where[] = " c.od_date3 between '$fr_date 00:00:00' and '$to_date 23:59:59' ";
		break;
	case '4':
		$where[] = " c.od_date4 between '$fr_date 00:00:00' and '$to_date 23:59:59' ";
		break;
	default:
		$where[] = " c.od_date1 between '$fr_date 00:00:00' and '$to_date 23:59:59' ";
		break;
}
//상태
if ($od_status) {
    $where[] = " c.od_status = '$od_status' ";
}
//결제구분
if ($od_settle_case) {
	 switch($od_settle_case) {
        case '빌링결제':
            $where[] = " a.od_billkey != '' ";
            break;
        default:
            $where[] = " a.od_settle_case = '$od_settle_case' ";
            break;
    }
}
//입금여부
if ($od_pay_yn) {
    $where[] = " c.od_pay_yn = '$od_pay_yn' ";
}
//공급사코드
if ($comp_code) {
    $where[] = " b.comp_code = '$comp_code' ";
}//주문구분
if ($od_gubun) {
    $where[] = " a.od_gubun = '$od_gubun' ";
}


if ($is_admin == 'sales') {
	$where[] = " mb_id in (select mb_id from tbl_member where pharm_manager = '{$member['mb_id']}') ";
}


if ($where) {
	$sql_search = ' where a.od_id = b.od_id and b.od_id = c.od_id and b.od_num = c.od_num and b.comp_code = d.comp_code and '.implode(' and ', $where);
}

if ($sel_field == "")  $sel_field = "od_id";
if ($sort1 == "") $sort1 = "od_id";
if ($sort2 == "") $sort2 = "desc";

$sql_common = " from tbl_shop_order a,  tbl_shop_order_detail b, tbl_shop_order_receiver c, tbl_member d $sql_search ";

//$sql = " select count(od_id) as cnt " . $sql_common;
$sql = " select count(distinct a.od_id, b.comp_code) as cnt " . $sql_common;

$row = sql_fetch($sql);
//페이징처리
$total_count = $row['cnt'];

$sql = " select count(distinct a.od_id) as cnt " . $sql_common;
$row = sql_fetch($sql);
//총주문건수
$total_count1 = $row['cnt'];

$rows = 10;//$config['cf_page_rows'];
$total_page  = ceil($total_count / $rows);  // 전체 페이지 계산
if ($page < 1) { $page = 1; } // 페이지가 없으면 첫 페이지 (1 페이지)
$from_record = ($page - 1) * $rows; // 시작 열을 구함

$iRow = 0;
$sql  = " select a.od_id, b.comp_code, count(*) od_count
           $sql_common
		   group by a.od_id, b.comp_code
           order by a.od_time desc, b.comp_code
           limit $from_record, $rows ";
$result = sql_query($sql);
//echo $sql;
//상품별 만들기
$iRow = 0;
for ($i=0; $row=sql_fetch_array($result); $i++)
{
	$sql  = " select a.mb_id
					 ,a.od_name
					 ,a.pharm_name
					 ,a.pharm_manager
					 ,(select mb_name from tbl_member where mb_id= a.pharm_manager and pharm_manager != '' limit 1) pharm_manager_name
					 ,(select mb_name from tbl_member where pharm_custno= a.pharm_custno and pharm_custno != '' limit 1) pharm_mb_name
					 ,a.od_settle_case
					 ,a.pharm_custno
					 ,a.od_gubun
					 ,a.od_mobile
					 ,(select mb_pay_yn from tbl_member where mb_id = a.mb_id) mb_pay_yn
					 ,d.comp_name
					 
					 ,b.od_num
					 ,b.it_name
					 ,b.od_option
					 ,b.od_period_yn
					 ,b.od_period_cnt
					 ,b.od_sum_drug_price
					 ,b.od_total_drug_price
					 ,b.od_total_incen
					 ,c.od_b_name
					 ,c.od_seq
					 ,c.od_date1
					 ,c.od_qty
					 ,c.od_period_date
					 ,c.od_status
					 ,c.od_pay_yn
				 from tbl_shop_order a, tbl_shop_order_detail b, tbl_shop_order_receiver c, tbl_member d
				 where a.od_id = b.od_id 
				   and b.od_id = c.od_id 
				   and b.od_num = c.od_num
				   and b.comp_code = d.comp_code
				   and b.od_id = ".$row[od_id]. " and b.comp_code = '".$row[comp_code]."'";
		
	switch($od_date) {
		case '1':
			$sql .= " and  c.od_date1 between '$fr_date 00:00:00' and '$to_date 23:59:59' ";
			break;
		case '2':
			$sql .= " and  c.od_date2 between '$fr_date 00:00:00' and '$to_date 23:59:59' ";
			break;
		case '3':
			$sql .= " and  c.od_date3 between '$fr_date 00:00:00' and '$to_date 23:59:59' ";
			break;
		case '4':
			$sql .= " and c.od_date4 between '$fr_date 00:00:00' and '$to_date 23:59:59' ";
			break;
		default:
			$sql .= " and  c.od_date1 between '$fr_date 00:00:00' and '$to_date 23:59:59' ";
			break;
	}
	//결제구분
	if ($od_settle_case) {
		 switch($od_settle_case) {
			case '빌링결제':
				$sql .= " and a.od_billkey != '' ";
				break;
			default:
				$sql .= " and a.od_settle_case = '$od_settle_case' ";
				break;
		}
	}
	//상태
	if ($od_status) {
		$sql .= " and c.od_status = '$od_status' ";
	}
	
	//입금여부
	if ($od_pay_yn) {
		$sql .= " and c.od_pay_yn = '$od_pay_yn' ";
	}
	//공급사코드
	if ($comp_code) {
		$sql .= " and b.comp_code = '$comp_code' ";
	}
   //공급사명
	if ($comp_name) {
		$sql .= " and d.comp_name = '$comp_code' ";
	}
	//공급사명
	if($sel_field == 'comp_name') {
		$where[] = "d.$sel_field like '%$search%' ";
	}
	//상품명
	if($sel_field == 'it_name') {
		$where[] = "b.$sel_field like '%$search%' ";
	}
	$result2 = sql_query($sql);
	for ($j=0; $row2=sql_fetch_array($result2); $j++)
	{

		$order[$iRow] = $row;
		$order[$iRow][mb_id] = $row2[mb_id];
		$order[$iRow][od_name] = $row2[od_name];
		$order[$iRow][pharm_name] = $row2[pharm_name];
		$order[$iRow][pharm_manager] = $row2[pharm_manager];
		$order[$iRow][pharm_manager_name] = $row2[pharm_manager_name];
		$order[$iRow][pharm_mb_name] = $row2[pharm_mb_name];
		$order[$iRow][od_b_name] = $row2[od_b_name];
		$order[$iRow][mb_pay_yn] = $row2[mb_pay_yn];

		$order[$iRow][od_settle_case] = $row2[od_settle_case];
		$order[$iRow][pharm_custno] = $row2[pharm_custno];
		$order[$iRow][od_gubun] = $row2[od_gubun];
		$order[$iRow][comp_name] = $row2[comp_name];
		$order[$iRow][od_mobile] = $row2[od_mobile];

		$order[$iRow][od_num] = $row2[od_num];
		$order[$iRow][it_name] = $row2[it_name];
		$order[$iRow][od_option] = $row2[od_option];
		$order[$iRow][od_period_yn] = $row2[od_period_yn];

		$order[$iRow][od_sum_drug_price] = $row2[od_sum_drug_price];		
		if($row2[od_period_yn] == 'Y') {		
			$order[$iRow][od_total_drug_price] = $row2[od_total_drug_price] / $row2[od_period_cnt];
			$order[$iRow][od_total_incen] = $row2[od_total_incen] / $row2[od_period_cnt];
		} else {
			$order[$iRow][od_total_drug_price] = $row2[od_total_drug_price];
			$order[$iRow][od_total_incen] = $row2[od_total_incen];
		}
		$order[$iRow][od_status] = $row2[od_status];
		
		$order[$iRow][od_qty] = $row2[od_qty];
		$order[$iRow][od_date1] = $row2[od_date1];
		$order[$iRow][od_seq] = $row2[od_seq];
		$order[$iRow][od_period_date] = $row2[od_period_date];
		$order[$iRow][od_status] = $row2[od_status];
		$order[$iRow][od_pay_yn] = $row2[od_pay_yn];


		$iRow++;
	}	
}

//print_r($order);

$qstr1 = "od_status=".urlencode($od_status)."&amp;od_settle_case=".urlencode($od_settle_case)."&amp;od_date=$od_date&amp;comp_code=$comp_code&amp;od_pay_yn=$od_pay_yn&amp;od_gubun=$od_gubun&amp;od_coupon=$od_coupon&amp;fr_date=$fr_date&amp;to_date=$to_date&amp;sel_field=$sel_field&amp;search=$search&amp;save_search=$search";
if($default['de_escrow_use'])
    $qstr1 .= "&amp;od_escrow=$od_escrow";
$qstr = "$qstr1&amp;sort1=$sort1&amp;sort2=$sort2&amp;page=$page";

$listall = '<a href="'.$_SERVER['SCRIPT_NAME'].'" class="ov_listall">전체목록</a>';


?>

<div class="local_ov01 local_ov">
    <?php echo $listall; ?>
    <span class="btn_ov01"><span class="ov_txt">전체 주문내역</span><span class="ov_num"> <?php echo number_format($total_count1); ?>건</span></span>
    
</div>





<form name="frmorderlist" class="local_sch01 local_sch">
<input type="hidden" name="doc" value="<?php echo $doc; ?>">
<input type="hidden" name="sort1" value="<?php echo $sort1; ?>">
<input type="hidden" name="sort2" value="<?php echo $sort2; ?>">
<input type="hidden" name="page" value="<?php echo $page; ?>">
<input type="hidden" name="save_search" value="<?php echo $search; ?>">

<div class="local_sch03  tbl_wrap" style="padding:10px;">
    <table>
   
    <tbody>
    <tr>
        <th scope="row">
			<select name="od_date" id="od_date" >
				<option value="1" <?php echo get_selected($od_date, '1'); ?>>주문일</option>
				<option value="2" <?php echo get_selected($od_date, '2'); ?>>확정일</option>
				<option value="3" <?php echo get_selected($od_date, '3'); ?>>배송일</option>
				<option value="4" <?php echo get_selected($od_date, '4'); ?>>완료일</option>
			</select>
		</th>
        <td colspan=5>
            <input type="text" id="fr_date"  name="fr_date" value="<?php echo $fr_date; ?>" class="frm_input" size="10" maxlength="10"> ~
			<input type="text" id="to_date"  name="to_date" value="<?php echo $to_date; ?>" class="frm_input" size="10" maxlength="10">
			<button type="button" onclick="javascript:set_date('오늘');">오늘</button>
			<button type="button" onclick="javascript:set_date('어제');">어제</button>
			<button type="button" onclick="javascript:set_date('이번주');">이번주</button>
			<button type="button" onclick="javascript:set_date('이번달');">이번달</button>
			<button type="button" onclick="javascript:set_date('지난주');">지난주</button>
			<button type="button" onclick="javascript:set_date('지난달');">지난달</button>
			<button type="button" onclick="javascript:set_date('12개월');">12개월</button>
			<button type="button" onclick="javascript:set_date('전체');">전체</button>
        </td>
		<td rowspan="3">
			 <input type="submit" value="검색" class="get_theme_confc btn btn_01">
			 <input type="button" value="엑셀 다운로드" alt="엑셀 다운로드" class="get_theme_confc btn btn_02" style="cursor:pointer" id="ExcelDownButton">

		</td>
		
    </tr>
	<tr>
        <th scope="row">처리상태</th>
        <td>
			<select name="od_status" id="od_status"  >
			    <option value="">==전체==</option>
				<option value="주문" <?php echo get_selected($od_status, '주문'); ?>>주문</option>
				<option value="확정" <?php echo get_selected($od_status, '확정'); ?>>확정</option>
				<option value="배송" <?php echo get_selected($od_status, '배송'); ?>>배송</option>
				<option value="완료" <?php echo get_selected($od_status, '완료'); ?>>완료</option>
				<option value="취소" <?php echo get_selected($od_status, '취소'); ?>>취소</option>   
			</select>
			
        </td>
		<th scope="row">입금여부</th>
        <td>
            <select name="od_pay_yn" id="od_pay_yn" >
				<option value="">==전체==</option>
				<option value="N" <?php echo get_selected($od_pay_yn, 'N'); ?>>미입금</option>
				<option value="Y" <?php echo get_selected($od_pay_yn, 'Y'); ?>>입금</option>
			</select>
        </td>
		<th scope="row">주문구분</th>
        <td>
           <select name="od_gubun" id="od_gubun" >
				<option value="">==전체==</option>
				<option value="1" <?php echo get_selected($od_gubun, '1'); ?>>고객주문</option>
				<option value="2" <?php echo get_selected($od_gubun, '2'); ?>>약국주문</option>
				<option value="3" <?php echo get_selected($od_gubun, '3'); ?>>일반주문</option>
			</select>
        </td>
    </tr>
	<tr>

        <th scope="row">결제구분</th>
        <td>
			<select name="od_settle_case" id="od_settle_case" >
				<option value="">==전체==</option>
				<option value="무통장" <?php echo get_selected($od_settle_case, '무통장'); ?>>미입금</option>
				<option value="가상계좌" <?php echo get_selected($od_settle_case, '가상계좌'); ?>>가상계좌</option>
				<option value="계좌이체" <?php echo get_selected($od_settle_case, '계좌이체'); ?>>계좌이체</option>
				<option value="휴대폰" <?php echo get_selected($od_settle_case, '휴대폰'); ?>>휴대폰</option>
				<option value="신용카드" <?php echo get_selected($od_settle_case, '신용카드'); ?>>신용카드</option>
				<option value="빌링결제" <?php echo get_selected($od_settle_case, '빌링결제'); ?>>빌링결제</option>
				<option value="KAKAOPAY" <?php echo get_selected($od_settle_case, 'KAKAOPAY'); ?>>KAKAOPAY</option>

			</select>
        </td>
		<th scope="row">검색</th>
        <td>
            <select name="sel_field" id="sel_field">
			<option value="od_id" <?php echo get_selected($sel_field, 'od_id'); ?>>주문번호</option>
			<option value="mb_id" <?php echo get_selected($sel_field, 'mb_id'); ?>>회원 ID</option>
			<option value="od_name" <?php echo get_selected($sel_field, 'od_name'); ?>>주문자</option>
			<option value="od_b_name" <?php echo get_selected($sel_field, 'od_b_name'); ?>>받는분</option>
			<option value="it_name" <?php echo get_selected($sel_field, 'it_name'); ?>>상품명</option>
			<option value="pharm_name" <?php echo get_selected($sel_field, 'pharm_name'); ?>>약국명</option>
			<option value="pharm_custno" <?php echo get_selected($sel_field, 'pharm_custno'); ?>>약국코드</option>
			<option value="comp_name" <?php echo get_selected($sel_field, 'comp_name'); ?>>업체명</option>
   
			</select>

<label for="search" class="sound_only">검색어<strong class="sound_only"> 필수</strong></label>
<input type="text" name="search" value="<?php echo $search; ?>" id="search" class=" frm_input" autocomplete="off">
        </td>
		<th scope="row">업체선택</th>
        <td>

		<select name="comp_code" id="comp_code" >
			<option value="">==업체선택==</option>
			<?php
			$sql2 = " select comp_code, comp_name from tbl_member where mb_type='7' order by comp_name ";
			$result2 = sql_query($sql2);
			for ($i=0; $row2=sql_fetch_array($result2); $i++) {
				if($comp_code ==$row2['comp_code']) 
					$checked='selected';
				else 
					$checked ='';

				echo '<option value="'.$row2['comp_code'].'" '.$checked.' >'.$row2['comp_name'].'</option>'.PHP_EOL;
			}
			?>
		</select>


		
        </td>
    </tr>
	
    </tbody>
    </table>
</div>

</form>

<form name="forderlist" id="forderlist" method="post" autocomplete="off">
<input type="hidden" name="search_od_status" value="<?php echo $od_status; ?>">
<input type="hidden" name="command" value='1' id="command">

<div class="tbl_head01 tbl_wrap">
    <table id="sodr_list">
    <caption>주문 내역 목록</caption>
    <thead>
    <tr>
        
        <th scope="col" id="th_ordnum">주문번호</th>
		<th scope="col" id="th_odrer">약국(ID)</th>		
        <th scope="col" id="th_odrer">주문자</th>		
        <th scope="col" id="th_recvr">받는분</th>
		<th scope="col" id="th_recvr">업체명</th>
		<th scope="col" >
            <label for="chkall" class="sound_only">주문 전체</label>
            <input type="checkbox" name="chkall" value="1" id="chkall" onclick="check_all(this.form)">
        </th>

		<th scope="col" id="th_recvr" width="360">주문상품</th>
		<th scope="col" id="th_recvr">단가</th>
		<th scope="col" id="th_recvr">수량</th>
		<th scope="col" id="th_recvr">합계</th>
		<th scope="col" id="th_recvr">수수료</th>
		<th scope="col" id="th_recvr">상태</th>	
        <th scope="col" >입금</th>
    </tr>

    </thead>
    <tbody>
    <?php
    //for ($i=0; $row=sql_fetch_array($result); $i++)
	$i=0;
	foreach ($order as $key => $row) {
		
        $s_receipt_way = $s_br = "";
		$od_settle_case = $row['od_settle_case'];
        if ($row['od_settle_case'])
        {
            $s_receipt_way = $row['od_settle_case'];
            $s_br = '<br />';

            // 간편결제
            if($row['od_settle_case'] == '간편결제') {
                switch($row['od_pg']) {
                    case 'lg':
                        $s_receipt_way = 'PAYNOW';
                        break;
                    case 'inicis':
                        $s_receipt_way = 'KPAY';
                        break;
                    case 'kcp':
                        $s_receipt_way = 'PAYCO';
                        break;
                    default:
                        $s_receipt_way = $row['od_settle_case'];
                        break;
                }
            }
        }
        else
        {
            $s_receipt_way = '결제수단없음';
            $s_br = '<br />';
        }

        if ($row['od_receipt_point'] > 0)
            $s_receipt_way .= $s_br."포인트";

        $od_cnt = 0;
        if ($row['mb_id'])
        {
            $sql2 = " select count(*) as cnt from {$g5['g5_shop_order_table']} where mb_id = '{$row['mb_id']}' ";
            $row2 = sql_fetch($sql2);
            $od_cnt = $row2['cnt'];
        }

        // 주문 번호에 device 표시
        $od_mobile = '';
        if($row['od_mobile'])
            $od_mobile = '(M)';

        // 주문번호에 - 추가
        switch(strlen($row['od_id'])) {
            case 16:
                $disp_od_id = substr($row['od_id'],0,8).'-'.substr($row['od_id'],8);
                break;
            default:
                $disp_od_id = substr($row['od_id'],0,8).'-'.substr($row['od_id'],8);
                break;
        }

		switch($row['od_gubun']) {
			case '1':
				$od_gubun = '고객주문';
				break;
			case '2':
				$od_gubun = '약국주문';
				break;
			case '3':
				$od_gubun = '일반주문';
				break;
		}
		switch ($row['od_pay_yn'] ) {
			case "N" :
				$od_pay_yn = "미입금";
				break;
			case "Y" :
				$od_pay_yn = "입금";
				break;
		}

        // 주문 번호에 에스크로 표시
        /*$od_paytype = '';
        if($row['od_test'])
            $od_paytype .= '<span class="list_test">테스트</span>';

        if($default['de_escrow_use'] && $row['od_escrow'])
            $od_paytype .= '<span class="list_escrow">에스크로</span>';
		*/

        //$uid = md5($row['od_id'].$row['od_time'].$row['od_ip']);

        //$invoice_time = is_null_time($row['od_invoice_time']) ? G5_TIME_YMDHIS : $row['od_invoice_time'];
        //$delivery_company = $row['od_delivery_company'] ? $row['od_delivery_company'] : $default['de_delivery_company'];

        //$bg = 'bg'.($i%2);
        //$td_color = 0;
        //if($row['od_cancel_price'] > 0) {
        //    $bg .= 'cancel';
        //    $td_color = 1;
        //}

		$od_id = $row['od_id'];
		$od_count = $row['od_count'];
		$comp_code = $row['comp_code'];

		$RowSpan = true;
		if ( $tmp_od_id == $od_id && $tmp_comp_code == $comp_code ) {
			$RowSpan = false;
		}

		$tmp_od_id = $od_id;
		$tmp_comp_code = $comp_code;

		$RowSpan2 = true;
		if ( $tmp_od_num == $od_num ) {
			$RowSpan2 = false;
		}

		$tmp_od_num = $od_num;

		$od_check_disabled = "";
		if ( $od_status != "확정"  ) {
			$od_check_disabled = "disabled";
		}

    ?>

    <tr class="tr<?php echo $od_id; ?>" onMouseOver="trColorChange(this, 'Over')" onMouseOut="trColorChange(this, 'Out')">
		
         
		<?php if ( $RowSpan ) { ?>
        <td rowspan="<?php echo $od_count; ?>"   headers="th_ordnum"  >		
            <a href="./orderform.php?od_id=<?php echo $row['od_id']; ?>&amp;<?php echo $qstr; ?>" >			
			 <?php echo $disp_od_id; ?>
            <?php echo $od_mobile; ?><br/>
			<?php echo substr($row['od_date1'],11); ?>
			</a>
               
        </td>
		
		<td rowspan="<?php echo $od_count; ?>" headers="th_odrer" class="td_name ">
		    
			
			<?php if ($row['mb_id']) { ?>
				<?php echo $row['pharm_name']; ?><br/>
				<?php echo $row['mb_id']; ?><br/>
				<?php echo $row['pharm_mb_name']; ?><br/>
				영업 : <?php echo $row['pharm_manager_name']; ?>

            <?php } else { ?>
            비회원
            <?php } ?>
		</td>
		<td rowspan="<?php echo $od_count; ?>" >
			<?php echo $row['od_name']; ?>(<?php echo $row['pharm_custno']; ?>)<br/>
			<?php if($od_gubun=='일반주문') {?>
				<span class="btn btn_03"><?php echo $od_gubun; ?></span>
			<?php } else { ?>
				<span class="btn btn_02"><?php echo $od_gubun; ?></span>
			<?php } ?>
		
		</td>       


		
        <td rowspan="<?php echo $od_count; ?>" ><?php echo get_text($row['od_b_name']); ?></a></td>

	

		<td rowspan="<?php echo $od_count; ?>" class="td_left ">
			&nbsp;&nbsp;<?php echo $row['comp_name']; ?>		
			
        </td>
		<?php } ?>
		
		<td width="60">
		<input type="hidden" name="od_id[<?php echo $i ?>]" value="<?php echo $row['od_id'] ?>" id="od_id_<?php echo $i ?>">
		<input type="hidden" name="od_num[<?php echo $i ?>]" value="<?php echo $row['od_num'] ?>" id="od_num_<?php echo $i ?>">
		<input type="hidden" name="od_seq[<?php echo $i ?>]" value="<?php echo $row['od_seq'] ?>" id="od_seq_<?php echo $i ?>">

			<?php if ($row[od_status] != '취소') { ?>
				<?php if ($row[od_status] == '주문') {
						if ($od_settle_case == "가상계좌" and $od_pay_yn == "N") {?>
							입금확인중
						<?php } else { ?>
							
							
							<label for="chk_<?php echo $i; ?>" class="sound_only">주문번호 <?php echo $row['od_id']; ?></label>
							<input type="checkbox" name="chk[]" value="<?php echo $i ?>" id="chk_<?php echo $i ?>">
							<?php } ?>
				<?php } else { ?>
					
					<font color="#0000FF">확정</font>
				<?php } ?>
		<?php } else { ?>
			<font color="#FF0000">취소</font>
		<?php } ?>
			
        </td>
		<td class="td_left ">
			<input type="checkbox" name="trans_chk[]" value="<?php echo $i;?>" <?php echo $od_check_disabled;?>  >

			<?php echo $row['it_name']; ?>
			<?php if ($row['od_period_yn'] == 'Y') { ?>
				<span >( <?php echo $row['od_seq'];?>회 - <?php echo $row['od_period_date'];?> ]</span>
			<?php } ?>
			<br/>
			<span><font color="blue"><?php echo $row['od_option']; ?></font></span>
			<div id="od_delivery<?php echo $i; ?>" style="display:none;padding:5px 0 0 5px;">
				<select name="od_delivery_company[<?php echo $i; ?>]">
					<option value="">== 선택 ==</option>
					<?php echo get_delivery_company($delivery_company); ?>
				</select>
				<input type="text" name="od_invoice[<?php echo $i; ?>]" value="<?php echo $row['od_invoice']; ?>" class="frm_input2" size="20">
			  </div>

			
        </td>
		
		<td width="100" class="td_num_right "><?php echo number_format($row['od_sum_drug_price']); ?></td>
		<td width="50" class="td_num_right "><?php echo $row['od_qty']; ?></td>
		<td width="100" class="td_num_right "><?php echo number_format($row['od_total_drug_price']); ?></td>
		<td width="100" class="td_num_right "><?php echo number_format($row['od_total_incen']); ?>P</td>
		<td width="50">
			<?php echo $row['od_status']; ?>
			
		</td>
		<td width="60">
		<?php echo $od_pay_yn;?>
		<?php if($row['mb_pay_yn'] == 'Y') { //약국관리에서 결제구분 ?>
		<input type="checkbox" name="chkcash[]" value="<?php echo $i ?>" id="chkcash_<?php echo $i ?>">		
		<?php } ?>
		</td>

       
    </tr>

    <?php
		$i++;
    }
    sql_free_result($result);


	if ($od_status != "완료" and $od_status != "취소") {?>
	<tr>
	  <td colspan="5" align="left">
	  	
	  </td>
      <td><?php if ($od_status== "주문" or $od_status== "" ) { ?>
			<div class="local_cmd01 ">
			<input type="button" value="선택확정" class="btn_submit" onclick="forderlist_submit(1);">   
			</div>

		<?php } ?></td>
	 <td class="td_left " colspan="6">
	  <?php if ($od_status== "확정" ) { ?>
        선택한 주문건을 
	  	<select name="Change_ODR_STATUS" id="Change_ODR_STATUS" onChange="Change_Order_Status(this.value,1)">
			<option value="" selected="selected" width="150">==선택==</option>
			<option value="배송">배송중</option>
		</select>
		(으)로 변경
		<span class="local_cmd01 " id="TransBtn1">
			<input type="button" value="송장번호저장" class="btn_submit" onclick="forderlist_submit(3);">    
		</span>
	  <?php } ?>
		
	  </td>
	  <td>
		<?php if ($is_admin== "super" ) { ?>
			<div class="local_cmd01 ">
			<input type="button" value="선택입금" class="btn_submit" onclick="forderlist_submit(2);">   
			</div>
		<?php } ?>
	  </td>
	</tr>
<?php }

    if ($i == 0)
        echo '<tr><td colspan="13" class="empty_table">자료가 없습니다.</td></tr>';
    ?>
    </tbody>
    <!-- <tfoot>
    <tr class="orderlist">
        <th scope="row" colspan="3">&nbsp;</th>
        <td>&nbsp;</td>
        <td><?php echo number_format($tot_itemcount); ?>건</td>
        <th scope="row">합 계</th>
        <td><?php echo number_format($tot_orderprice); ?></td>
        <td><?php echo number_format($tot_receiptprice); ?></td>
        <td><?php echo number_format($tot_ordercancel); ?></td>
        <td><?php echo number_format($tot_couponprice); ?></td>
        <td><?php echo number_format($tot_misu); ?></td>
        <td></td>
    </tr>
    </tfoot> -->
    </table>
</div>
<style>
ul.delivery {
    list-style:none;
    margin:0;
    padding:0;
	width:100%;
}

ul.delivery li {
    margin: 2px;
    padding: 2px;
    border : 0;
    float: left;
}
</style>




</form>
<?php

?>

<?php //$qstr ='comp_code='.$_GET['comp_code'].'&'.$qstr;
echo get_paging(G5_IS_MOBILE ? $config['cf_mobile_pages'] : $config['cf_write_pages'], $page, $total_page, "{$_SERVER['SCRIPT_NAME']}?$qstr&amp;page="); ?>

<script>
function getCheckedCount(_obj){
	var cnt=0;
	var chkLen=0;
	if (_obj == null) return cnt;

	if(_obj.length == null){
		_objLen=1;			
	}else{
		_objLen= _obj.length;
	}
	
	for(var i=0; i<_objLen; i++){
		if(_obj.length == null){
			if(_obj.checked)
				cnt++;
		}else{
			if(_obj[i].checked)
				cnt++;
		}
	}
	return cnt;
}
function Change_Order_Status(ch,key){
	//var command = document.getElementById("command");
	//command.value = action;
	
	var f = document.forderlist;

	if (!is_checked("trans_chk[]")) {
		alert("처리 하실 항목을 하나 이상 선택하세요.");
		return false;
	}
	var chk = document.getElementsByName("trans_chk[]");

	for (var i=0; i<chk.length; i++)
	{
		if (chk[i].checked)
		{
			var k = chk[i].value;
			//var current_settle_case = f.elements['current_settle_case['+k+']'].value;
			//var current_status = f.elements['current_status['+k+']'].value;
			
			//var box      = f.elements['od_delivery['+k+']'];	
			//alert(box);
			$('#od_delivery'+k).css('display','');
			//box.style.display = "";

			/*var delivery_company = f.elements['od_delivery_company['+k+']'];
			if ($.trim(delivery_company.value) == '') {
				alert("배송업체를 입력하시기 바랍니다.");
				delivery_company.focus();
				return false;
			}
			*/

		}
	}

	
}


function trColorChange(id, chk) {
	var ID_Len = $("." + id.className).length;

	if (chk == "Over") {
		$("." + id.className).each(function (index, element) {
			$(this).css("background-color", "#F7F7F7");
		});
	} else {
		$("." + id.className).each(function (index, element) {
			$(this).css("background-color", "#FFFFFF");
		});
	}
}


$(function(){
    $("#fr_date, #to_date").datepicker({ changeMonth: true, changeYear: true, dateFormat: "yy-mm-dd", showButtonPanel: true, yearRange: "c-99:c+99", maxDate: "+0d" });

    // 주문상품보기
    $(".orderitem").on("click", function() {
        var $this = $(this);
        var od_id = $this.text().replace(/[^0-9]/g, "");

        if($this.next("#orderitemlist").size())
            return false;

        $("#orderitemlist").remove();

        $.post(
            "./ajax.orderitem.php",
            { od_id: od_id },
            function(data) {
                $this.after("<div id=\"orderitemlist\"><div class=\"itemlist\"></div></div>");
                $("#orderitemlist .itemlist")
                    .html(data)
                    .append("<div id=\"orderitemlist_close\"><button type=\"button\" id=\"orderitemlist-x\" class=\"btn_frmline\">닫기</button></div>");
            }
        );

        return false;
    });

    // 상품리스트 닫기
    $(".orderitemlist-x").on("click", function() {
        $("#orderitemlist").remove();
    });

    $("body").on("click", function() {
        $("#orderitemlist").remove();
    });

    // 엑셀배송처리창
    $("#order_delivery").on("click", function() {
        var opt = "width=600,height=450,left=10,top=10";
        window.open(this.href, "win_excel", opt);
        return false;
    });
});

function set_date(today)
{
    <?php
    $date_term = date('w', G5_SERVER_TIME);
    $week_term = $date_term + 7;
    $last_term = strtotime(date('Y-m-01', G5_SERVER_TIME));
    ?>
    if (today == "오늘") {
        document.getElementById("fr_date").value = "<?php echo G5_TIME_YMD; ?>";
        document.getElementById("to_date").value = "<?php echo G5_TIME_YMD; ?>";
    } else if (today == "어제") {
        document.getElementById("fr_date").value = "<?php echo date('Y-m-d', G5_SERVER_TIME - 86400); ?>";
        document.getElementById("to_date").value = "<?php echo date('Y-m-d', G5_SERVER_TIME - 86400); ?>";
    } else if (today == "이번주") {
        document.getElementById("fr_date").value = "<?php echo date('Y-m-d', strtotime('-'.$date_term.' days', G5_SERVER_TIME)); ?>";
        document.getElementById("to_date").value = "<?php echo date('Y-m-d', G5_SERVER_TIME); ?>";
    } else if (today == "이번달") {
        document.getElementById("fr_date").value = "<?php echo date('Y-m-01', G5_SERVER_TIME); ?>";
        document.getElementById("to_date").value = "<?php echo date('Y-m-d', G5_SERVER_TIME); ?>";
    } else if (today == "지난주") {
        document.getElementById("fr_date").value = "<?php echo date('Y-m-d', strtotime('-'.$week_term.' days', G5_SERVER_TIME)); ?>";
        document.getElementById("to_date").value = "<?php echo date('Y-m-d', strtotime('-'.($week_term - 6).' days', G5_SERVER_TIME)); ?>";
    } else if (today == "지난달") {
        document.getElementById("fr_date").value = "<?php echo date('Y-m-01', strtotime('-1 Month', $last_term)); ?>";
        document.getElementById("to_date").value = "<?php echo date('Y-m-t', strtotime('-1 Month', $last_term)); ?>";
	} else if (today == "12개월") {
        document.getElementById("fr_date").value = "<?php echo date('Y-m-01', strtotime('-1 Year', $last_term)); ?>";
        document.getElementById("to_date").value = "<?php echo date('Y-m-d', G5_SERVER_TIME); ?>";
    } else if (today == "전체") {
        document.getElementById("fr_date").value = "";
        document.getElementById("to_date").value = "";
    }
}
</script>

<script>

function forderlist_submit(action)
{
	
	var command = document.getElementById("command");
	command.value = action;
	
	var f = document.forderlist;
	
	if(action ==1) {
		if (!is_checked("chk[]")) {
			alert("확정 하실 항목을 하나 이상 선택하세요.");
			return false;
		}
		if (!confirm("선택하신 주문서의 주문상태를 확정 상태로 변경하시겠습니까?"))
			return false;
	
	}
	if(action ==3) {

		if (!is_checked("trans_chk[]")) {
			alert("처리 하실 항목을 하나 이상 선택하세요.");
			return false;
		}
		//if (!confirm("선택하신 주문서의 주문상태를 배송중 상태로 변경하시겠습니까?"))
		//	return false;

		var chk = document.getElementsByName("trans_chk[]");

		for (var i=0; i<chk.length; i++)
		{
			if (chk[i].checked)
			{
				var k = chk[i].value;
				//var current_settle_case = f.elements['current_settle_case['+k+']'].value;
				//var current_status = f.elements['current_status['+k+']'].value;
				
				var invoice      = f.elements['od_invoice['+k+']'];					   
				var delivery_company = f.elements['od_delivery_company['+k+']'];
				if ($.trim(delivery_company.value) == '') {
					alert("배송업체를 입력하시기 바랍니다.");
					delivery_company.focus();
					return false;
				}
				if ($.trim(invoice.value) == '') {
					alert("운송장번호를 입력하시기 바랍니다.");
					invoice.focus();
					return false;
				}				
			}
		}
		if (!confirm("선택하신 주문서를 배송중 상태로 변경하시겠습니까?"))
			return false;
	}
	if(action ==2) {
		if (!is_checked("chkcash[]")) {
			alert("입금처리 하실 항목을 하나 이상 선택하세요.");
			return false;
		}
		if (!confirm("선택하신 주문서를 입금처리 상태로 변경하시겠습니까?"))
			return false;
	
	}
    f.action = "./orderlistupdate.php";
	f.submit();
    return true;
}
</script>
<div id="Excel_Select" title="엑셀 출력 선택">
	<form name="ExcelFrm" id="ExcelFrm" action="orderprintresultlist.php?<?php echo $qstr; ?>" method="post" target="iFrm">
		<table class="table_style1">
			<tr>
				<td width="150">선택</td>
				<td><input type="radio" name="ptype" value="1" id="ExcelParamSelect1"><label for="ExcelParamSelect1">전체</label>
				    <br/>
					<input type="radio" name="ptype" value="2" checked id="ExcelParamSelect2"><label for="ExcelParamSelect2">송장발행</label>
				</td>
			</tr>
			
		</table>
	</form>
</div>
<script>
	$("#Excel_Select").dialog({
		autoOpen: false,
		resizable: false,
		modal: true,
		width: 800,
		buttons: {
			"엑셀다운로드": function () {
				$("#ExcelFrm").submit();
				$(this).dialog("close");
			},
			Cancel: function () {
				$(this).dialog("close");
			}
		}
	});

	$("#ExcelDownButton").click(function () {
		$("#Excel_Select").dialog("open");
	});

</script>
<!-- (s)hidden Frame -->
<iframe name="iFrm" style="display: none;" ></iframe>


<?php
include_once (G5_ADMIN_PATH.'/admin.tail.php');
?>

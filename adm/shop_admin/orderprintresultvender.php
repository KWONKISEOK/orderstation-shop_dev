<?php
//$sub_menu = '500120';
include_once('./_common.php');

//auth_check($auth[$sub_menu], "r");

//print_r2($_GET); exit;

/*
function multibyte_digit($source)
{
    $search  = array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9");
    $replace = array("０","１","２","３","４","５","６","７","８","９");
    return str_replace($search, $replace, (string)$source);
}
*/

function conv_telno($t)
{
    // 숫자만 있고 0으로 시작하는 전화번호
    if (!preg_match("/[^0-9]/", $t) && preg_match("/^0/", $t))  {
        if (preg_match("/^01/", $t)) {
            $t = preg_replace("/([0-9]{3})(.*)([0-9]{4})/", "\\1-\\2-\\3", $t);
        } else if (preg_match("/^02/", $t)) {
            $t = preg_replace("/([0-9]{2})(.*)([0-9]{4})/", "\\1-\\2-\\3", $t);
        } else {
            $t = preg_replace("/([0-9]{3})(.*)([0-9]{4})/", "\\1-\\2-\\3", $t);
        }
    }

    return $t;
}

    $fr_date1 = date_conv($fr_date1);
	if($to_date1 == '') $to_date1 = date('Y-m-d');
    $to_date1 = date_conv($to_date1);

    $sql = " SELECT a.*, b.*,
					(select comp_name from tbl_member where comp_code = b.comp_code) comp_name
               FROM {$g5['g5_shop_order_table']} a, {$g5['g5_shop_order_detail_table']} b
              where a.od_id = b.od_id and b.od_status != '쥐소'";

        $sql .= " and a.od_time between '$fr_date1 00:00:00' and '$to_date1 23:59:59' ";
		$sql .= " and b.comp_code = '$comp_code' ";

	if ($od_status1) {
		$sql .= " and b.od_status = '$od_status1' ";
	}   
	
	if ($is_admin == 'sales') {
		$sql .= " and a.mb_id in (select mb_id from tbl_member where pharm_manager = '{$member['mb_id']}') ";
	}

    $sql .="  order by a.od_time desc, b.comp_code";

    $result = sql_query($sql);
    $cnt = @sql_num_rows($result);
    if (!$cnt)
        alert("출력할 내역이 없습니다.");

    /*================================================================================
    php_writeexcel http://www.bettina-attack.de/jonny/view.php/projects/php_writeexcel/
    =================================================================================*/
    include_once(G5_LIB_PATH.'/Excel/php_writeexcel/class.writeexcel_workbook.inc.php');
    include_once(G5_LIB_PATH.'/Excel/php_writeexcel/class.writeexcel_worksheet.inc.php');

    $fname = tempnam(G5_DATA_PATH, "tmp-orderlist.xls");
    $workbook = new writeexcel_workbook($fname);
    $worksheet = $workbook->addworksheet();
    // Put Excel data
    $data = array('주문일시',
'주문구분',
'주문번호',
'담당자명',
'약국명',
'약국코드',
'약사명',
'회원ID',
'주문자명',
'주문자hp',
'우편번호',
'주소',
'이름',
'배송지hp',
'배송지우편번호',
'배송지주소',
'배송주의사항(메시지)',
'상품코드',
'상품명',
'옵션상품코드',
'옵션명',
'주문수량',
'공급사명',
'배송비',

'주문상태',
'정기주문여부',
'배송회사',
'송장번호',
'배송일시');

    $data = array_map('iconv_euckr', $data);

    $col = 0;
    foreach($data as $cell) {
        $worksheet->write(0, $col++, $cell);
    }

    $save_it_id = '';
    for($i=1; $row=sql_fetch_array($result); $i++)
    {     
		

        $row = array_map('iconv_euckr', $row);	

		switch($row['od_gubun']) {
			case '1':
				$od_gubun = iconv("utf-8", "euc-kr", '고객주문');
				break;
			case '2':
				$od_gubun = iconv("utf-8", "euc-kr", '약국주문');
				break;
			case '3':
				$od_gubun = iconv("utf-8", "euc-kr", '일반주문');
				break;
		}
		$sql = " select a.*, (select mb_name from tbl_member where mb_id = a.pharm_manager) pharm_manager  from tbl_member a where pharm_custno='{$row['pharm_custno']}' ";
		$row2 = sql_fetch($sql);
		$row2 = array_map('iconv_euckr', $row2);	


		$worksheet->write($i, 0, ' '.$row['od_time']);
		$worksheet->write($i, 1, ' '.$od_gubun);
		$worksheet->write($i, 2, ' '.$row['od_id']);
		$worksheet->write($i, 3, ' '.$row2['pharm_manager']);
		$worksheet->write($i, 4, ' '.$row2['pharm_name']);
		$worksheet->write($i, 5, ' '.$row2['pharm_custno']);
		$worksheet->write($i, 6, ' '.$row2['mb_name']);
		$worksheet->write($i, 7, ' '.$row['mb_id']);
		$worksheet->write($i, 8, ' '.$row['od_name']);
		$worksheet->write($i, 9, ' '.$row['od_hp']);
		$worksheet->write($i, 10, ' '.$row['od_zip1'].$row['od_zip2']);
		$worksheet->write($i, 11, print_address($row['od_addr1'], $row['od_addr2'], $row['od_addr3'], $row['od_addr_jibeon']));
		$worksheet->write($i, 12, ' '.$row['od_b_name']);
		$worksheet->write($i, 13, ' '.$row['od_b_hp']);
		$worksheet->write($i, 14, ' '.$row['od_b_zip1'].$row['od_b_zip2']);
		$worksheet->write($i, 15, print_address($row['od_b_addr1'], $row['od_b_addr2'], $row['od_b_addr3'], $row['od_b_addr_jibeon']));
        $worksheet->write($i, 16, $row['od_memo']);
        $worksheet->write($i, 17, ' '.$row['it_id']);
        $worksheet->write($i, 18, ' '.$row['it_name']);
        $worksheet->write($i, 19, ' '.$row['io_id']);
        $worksheet->write($i, 20, ' '.$row['od_option']);
        $worksheet->write($i, 21, ' '.$row['od_qty']);		
		$worksheet->write($i, 22, ' '.$row['comp_name']);
        $worksheet->write($i, 23, ' '.$row['od_send_cost']);        
        //$worksheet->write($i, 24, ' '.$row['od_drug_price']);
		//$worksheet->write($i, 25, ' '.$row['od_incen']);
		//$worksheet->write($i, 26, ' '.$row['od_total_drug_price']);
		//$worksheet->write($i, 27, ' '.$row['od_total_incen']);
		$worksheet->write($i, 24, ' '.$row['od_status']);
		$worksheet->write($i, 25, ' '.$row['od_period_yn']);
		$worksheet->write($i, 26, ' '.$row['od_delivery_company']);
        $worksheet->write($i, 27, ' '.$row['od_invoice']);
        $worksheet->write($i, 28, ' '.$row['od_invoice_time']);
    }

    $workbook->close();

    header("Content-Type: application/x-msexcel; name=\"orderlist-".date("ymd", time()).".xls\"");
    header("Content-Disposition: inline; filename=\"orderlist-".date("ymd", time()).".xls\"");
    $fh=fopen($fname, "rb");
    fpassthru($fh);
    unlink($fname);

    exit;



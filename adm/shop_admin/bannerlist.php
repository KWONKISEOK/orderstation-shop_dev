<?php
$sub_menu = '700500';
include_once('./_common.php');

auth_check($auth[$sub_menu], "r");

$g5['title'] = '배너관리';
include_once(G5_ADMIN_PATH . '/admin.head.php');

$sql_common = " from {$g5['g5_shop_banner_table']} ";

// 위치 검색 값
$sel_ca_id = get_search_string($sel_ca_id);
$sql_common_add = "";

if ($sel_ca_id != "") {
    $sql_common_add .= " where bn_position='$sel_ca_id' ";
}

// 테이블의 전체 레코드수만 얻음
$sql = " select count(*) as cnt " . $sql_common . $sql_common_add;
$row = sql_fetch($sql);
$total_count = $row['cnt'];

$rows = $config['cf_page_rows'];
$total_page = ceil($total_count / $rows);  // 전체 페이지 계산
if ($page < 1) {
    $page = 1;
} // 페이지가 없으면 첫 페이지 (1 페이지)
$from_record = ($page - 1) * $rows; // 시작 열을 구함

$colspan = 12;
?>

    <div class="local_ov01 local_ov">
        <span class="btn_ov01"><span class="ov_txt"> 등록된 배너 </span><span
                    class="ov_num"> <?php echo $total_count; ?>개</span></span>
    </div>

    <div class="btn_fixed_top">
        <a href="./bannerform.php" class="btn_01 btn">배너추가</a>
    </div>
    <form name="fsearchlist" id="fsearchlist" action="" method="get">
        <select name="sel_ca_id" id="sel_ca_id" style="margin-bottom: 5px;" onchange="flist_search(this);">
            <option value="">전체분류</option>
            <option value="메인" <?php echo get_selected($sel_ca_id, '메인'); ?>>메인</option>
            <option value="왼쪽" <?php echo get_selected($sel_ca_id, '왼쪽'); ?>>왼쪽</option>
            <option value="메인하단" <?php echo get_selected($sel_ca_id, '메인하단'); ?>>메인하단</option>
            <option value="무이자_page" <?php echo get_selected($sel_ca_id, '무이자_page'); ?>>무이자(페이지)</option>
            <option value="무이자_side" <?php echo get_selected($sel_ca_id, '무이자_side'); ?>>무이자(사이드)</option>
            <option value="무이자_item" <?php echo get_selected($sel_ca_id, '무이자_item'); ?>>무이자(상품상세)</option>
        </select>
    </form>
    <form name="fmemberlist" id="fmemberlist" action="" onsubmit="return fmemberlist_submit(this);" method="post">
        <div class="tbl_head01 tbl_wrap">
            <table>
                <caption><?php echo $g5['title']; ?> 목록</caption>
                <thead>
                <tr>
                    <th scope="col" id="mb_list_chk">
                        <label for="chkall" class="sound_only">회원 전체</label>
                        <input type="checkbox" name="chkall" value="1" id="chkall" onclick="check_all(this.form)">
                    </th>
                    <th scope="col" id="th_id">ID</th>
                    <th scope="col" id="th_dvc">접속기기</th>
                    <th scope="col" id="th_loc">위치</th>
                    <th scope="col" id="th_loc">제목</th>
                    <th scope="col" id="th_loc">이미지확인</th>
                    <th scope="col" id="th_st">시작일시</th>
                    <th scope="col" id="th_end">종료일시</th>
                    <th scope="col" id="th_odr">출력순서</th>
                    <th scope="col" id="th_hit">조회</th>
                    <th scope="col" id="th_view">노출대상</th>
                    <th scope="col" id="th_mng">관리</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $sql_add = "";
                if ($sel_ca_id != "") {
                    $sql_add .= " where bn_position='$sel_ca_id' ";
                }

                $sql = " select * from {$g5['g5_shop_banner_table']} $sql_add
          order by bn_order, bn_id desc
          limit $from_record, $rows  ";
                $result = sql_query($sql);
                for ($i = 0; $row = sql_fetch_array($result); $i++) {
                    // 테두리 있는지
                    $bn_border = $row['bn_border'];
                    // 새창 띄우기인지
                    $bn_new_win = ($row['bn_new_win']) ? 'target="_blank"' : '';

                    $bimg = G5_DATA_PATH . '/banner/' . $row['bn_id'];
                    //alert($bimg);
                    //exit;

                    if (file_exists($bimg)) {
                        $size = @getimagesize($bimg);
                        if ($size[0] && $size[0] > 800)
                            $width = 800;
                        else
                            $width = $size[0];

                        $bn_img = "";
                        $bn_img .= '<img src="' . G5_DATA_URL . '/banner/' . $row['bn_id'] . '" width="' . $width . '" alt="' . $row['bn_alt'] . '">';
                    }

                    switch ($row['bn_device']) {
                        case 'pc':
                            $bn_device = 'PC';
                            break;
                        case 'mobile':
                            $bn_device = '모바일';
                            break;
                        default:
                            $bn_device = 'PC와 모바일';
                            break;
                    }

                    $bn_begin_time = substr($row['bn_begin_time'], 2, 14);
                    $bn_end_time = substr($row['bn_end_time'], 2, 14);

                    $bg = 'bg' . ($i % 2);
                    ?>

                    <tr class="<?php echo $bg; ?>">
                        <td headers="mb_list_chk" class="td_chk">
                            <input type="hidden" name="bn_id[<?php echo $i ?>]" value="<?php echo $row['bn_id'] ?>"
                                   id="bn_id_<?php echo $i ?>">
                            <input type="checkbox" name="chk[]" value="<?php echo $i ?>" id="chk_<?php echo $i ?>">
                        </td>
                        <td headers="th_id" class="td_num"><?php echo $row['bn_id']; ?></td>
                        <td headers="th_dvc"><?php echo $bn_device; ?></td>
                        <td headers="th_loc"><?php echo $row['bn_position']; ?></td>
                        <td headers="th_loc"><?php echo $row['bn_alt']; ?></td>
                        <td headers="th_img" class="td_img_view sbn_img">
                            <div class="sbn_image"><?php echo $bn_img; ?></div>
                            <input type="hidden" class="b_title" value="banner">
                            <button type="button" class="sbn_img_view btn_frmline" value="<?php echo $row['bn_id'] ?>">
                                이미지확인
                            </button>
                        </td>
                        <td headers="th_st" class="td_datetime"><?php echo $bn_begin_time; ?></td>
                        <td headers="th_end" class="td_datetime"><?php echo $bn_end_time; ?></td>
                        <td headers="th_odr" class="td_num">
                            <input name="bn_order[<?php echo $i; ?>]" value="<?php echo $row['bn_order'] ?>"
                                   style="width:50px"/>
                        </td>
                        <td headers="th_hit" class="td_num"><?php echo $row['bn_hit']; ?></td>
                        <td headers="th_view" class="td_num">
                            <?
                            if ($row['bn_view'] == "a") {
                                echo "전체";
                            } else if ($row['bn_view'] == "b") {
                                echo "소비자";
                            } else if ($row['bn_view'] == "c") {
                                echo "약사";
                            }
                            ?>
                        </td>
                        <td headers="th_mng" class="td_mng td_mns_m">
                            <a href="./bannerform.php?w=u&amp;bn_id=<?php echo $row['bn_id']; ?>"
                               class="btn btn_03">수정</a>
                            <a href="./bannerformupdate.php?w=d&amp;bn_id=<?php echo $row['bn_id']; ?>"
                               onclick="return delete_confirm(this);" class="btn btn_02">삭제</a>
                        </td>
                    </tr>
                    <?php
                }
                ?>
                <?php if ($i > 0) { ?>
                    <tr>
                        <td colspan=8></td>
                        <td colspan=4 style="text-align: left;">

                            <div class="local_cmd01 ">
                                <input type="button" value="순서저장" class="btn_submit" onclick="flist_submit();">
                            </div>

                        </td>
                    </tr>
                <?php } ?>
                <?php
                if ($i == 0)
                    echo "<tr><td colspan=\"" . $colspan . "\" class=\"empty_table\">자료가 없습니다.</td></tr>";
                ?>
                </tbody>
            </table>

        </div>
    </form>
<?php echo get_paging(G5_IS_MOBILE ? $config['cf_mobile_pages'] : $config['cf_write_pages'], $page, $total_page, "{$_SERVER['SCRIPT_NAME']}?$qstr&amp;page="); ?>

    <script>
        function flist_submit() {
            var f = document.fmemberlist;

            if (!is_checked("chk[]")) {
                alert("변경 하실 항목을 하나 이상 선택하세요.");
                return false;
            }

            f.action = "./bannerlist_update.php";
            f.submit();
            return true;
        }

        function flist_search() {
            var f = document.fsearchlist;

            f.action = "./bannerlist.php";
            f.submit();
            return true;
        }

        // 배너 이미지 확인 팝업
        $(function () {
            $(".sbn_img_view").on("click", function () {
                $.ajax({
                    url: './ajax.itemeventbanner.php',
                    type: 'GET',
                    data: {'bn_id': $(this).val(), 'b_title': $('.b_title').val()},
                    success: function (html) {
                        if (html == '') {
                            alert("등록된 배너가 존재하지 않습니다 ");
                        } else {
                            $('.dim-layer').show();
                            $('.dim-layer .pop-conts').html(html);
                            return false;
                        }
                    }
                });
            });
            $('.btn-r').on("click", function () {
                $('.dim-layer').hide();
                return false;
            });
        });
    </script>

<link rel="stylesheet" href="<?php echo G5_ADMIN_URL; ?>/css/banner.css">

<?php
include_once(G5_ADMIN_PATH . '/admin.tail.php');
?>
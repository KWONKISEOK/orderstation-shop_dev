<?php
$sub_menu = '400801';
include_once('./_common.php');

auth_check($auth[$sub_menu], "r");

//쿠폰 관리 비밀번호 치고 들어가기
if( empty(get_cookie("rand_coupon_manager")) ){

    require_once('./rand_coupon_login.php');
    return;

}

$sql_common = " from tbl_shop_coupon_rand as A ";

$sql_search = " where (1) ";
if ($stx) {
    $sql_search .= " and ( ";
    switch ($sfl) {
        case 'mb_id' :
            $sql_search .= " ({$sfl} = '{$stx}') ";
            break;
        default :
            $sql_search .= " ({$sfl} like '%{$stx}%') ";
            break;
    }
    $sql_search .= " ) ";
}

if (!$sst) {
    $sst  = "cp_no";
    $sod = "desc";
}
$sql_order = " order by {$sst} {$sod} ";

$sql = " select count(*) as cnt
            {$sql_common}
            {$sql_search}
            {$sql_order} ";
$row = sql_fetch($sql);
$total_count = $row['cnt'];

$rows = $config['cf_page_rows'];
$total_page  = ceil($total_count / $rows);  // 전체 페이지 계산
if ($page < 1) $page = 1; // 페이지가 없으면 첫 페이지 (1 페이지)
$from_record = ($page - 1) * $rows; // 시작 열을 구함

$sql = " select * 
			, ifnull( ( select count(*) AS reg_cnt from tbl_shop_coupon_rand_txt WHERE parent_cp_no = A.cp_no ) , 0 ) as reg_cnt
			, ifnull( ( select count(*) AS reg_cnt from tbl_shop_coupon_rand_txt WHERE parent_cp_no = A.cp_no and use_yn = 'Y' ) , 0 ) as use_cnt
            {$sql_common}
            {$sql_search}
            {$sql_order}
            limit {$from_record}, {$rows} ";

$result = sql_query($sql);

$g5['title'] = '페이퍼 쿠폰관리';
include_once (G5_ADMIN_PATH.'/admin.head.php');

$colspan = 9;
?>
<div class="local_ov">
    <span class="btn_ov01"><span class="ov_txt">전체 </span><span class="ov_num"> <?php echo number_format($total_count) ?> 개</span></span>
</div>
<form name="fsearch" id="fsearch" class="local_sch01 local_sch" method="get">

<select name="sfl" title="검색대상">
    <option value="cp_subject"<?php echo get_selected($_GET['sfl'], "cp_subject"); ?>>쿠폰이름</option>
</select>
<label for="stx" class="sound_only">검색어<strong class="sound_only"> 필수</strong></label>
<input type="text" name="stx" value="<?php echo $stx ?>" id="stx" class="frm_input">
<input type="submit" class="btn_submit" value="검색">
</form>

<form name="fcouponlist" id="fcouponlist" method="post">
<input type="hidden" name="sst" value="<?php echo $sst; ?>">
<input type="hidden" name="sod" value="<?php echo $sod; ?>">
<input type="hidden" name="sfl" value="<?php echo $sfl; ?>">
<input type="hidden" name="stx" value="<?php echo $stx; ?>">
<input type="hidden" name="page" value="<?php echo $page; ?>">
<input type="hidden" name="token" value="">

<div class="tbl_head01 tbl_wrap">
    <table>
    <caption><?php echo $g5['title']; ?></caption>
    <thead>
    <tr>
        <th scope="col">쿠폰이름</th>
        <th scope="col">쿠폰종류</th>
        <th scope="col" width="10%">쿠폰번호생성</th>
        <th scope="col" width="10%">발급갯수(등록갯수)</th>
        <th scope="col" width="10%">적용상품</th>
        <th scope="col" width="10%"><?php echo subject_sort_link('cp_end') ?>사용기한</a></th>
        <th scope="col" width="5%">관리</th>
    </tr>
    </thead>
    <tbody>
    <?php

    for ($i=0; $row=sql_fetch_array($result); $i++) {
        switch($row['cp_method']) {
            case '0':
                $cp_method = '개별상품할인';
				$cp_target = "개별상품";
                break;
            case '1':
                $cp_method = '카테고리할인';
				$cp_target = "카테고리";
                break;
            case '2':
                $cp_method = '주문금액할인';
                $cp_target = '주문금액';
                break;
            case '3':
                $cp_method = '배송비할인';
                $cp_target = '배송비';
                break;
            case '4':
                $cp_method = '인증쿠폰';
                $cp_target = '인증쿠폰';
                break;
        }

        $link1 = '<a href="./orderform.php?od_id='.$row['od_id'].'">';
        $link2 = '</a>';

 
        $bg = 'bg'.($i%2);
    ?>

    <tr class="<?php echo $bg; ?>">
        <td class="td_left"><?php echo $row['cp_subject']; ?></td>
        <td><?php echo $cp_method; ?></td>
        <td class="td_mng ">
			<a href="javascript:MakeCoupon('<?=$row['cp_no']?>');" class="btn btn_03">쿠폰생성</a>
		</td>
		<td>
			<a href="./rand_coupon_excel.php?cp_no=<?=$row['cp_no']?>"><?=$row['reg_cnt']?> (<?=$row['use_cnt']?>)</a>
		</td>
        <td><?php echo $cp_target; ?></td>
        <td class="td_datetime"><?php echo substr($row['cp_start'], 2, 8); ?> ~ <?php echo substr($row['cp_end'], 2, 8); ?></td>
        <td class="td_mng td_mng_s">
            <a href="./rand_couponform.php?w=u&amp;cp_no=<?php echo $row['cp_no']; ?>&amp;<?php echo $qstr; ?>" class="btn btn_03">수정</a>
        </td>
    </tr>

    <?php
    }

    if ($i == 0)
        echo '<tr><td colspan="'.$colspan.'" class="empty_table">자료가 없습니다.</td></tr>';
    ?>
    </tbody>
    </table>
</div>
<div class="btn_fixed_top">
     <!--<input type="submit" name="act_button" value="선택삭제" onclick="document.pressed=this.value" class="btn btn_02">-->
	 <a href="./rand_couponform.php" id="coupon_add" class="btn btn_01">쿠폰 추가</a> 
</div>

</form>

<style>
	#popupLayer {display:none;border:5px solid #cccccc;margin-top:50px;padding:5px;background-color:#ffffff;z-index:5;}
	#popupLayer .b-close {position:absolute;top:10px;right:25px;color:#f37a20;font-weight:bold;cursor:hand;}
	#popupLayer .popupContent {margin:0;padding:0;text-align:center;border:0;}
	#popupLayer .popupContent iframe {width:500px;height:300px;border:0;padding:0px;margin:0;z-index:10;}
</style>

<div id="popupLayer">
	<div class="popupContent"></div>
	<div class="b-close">
		<button type="button" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button"><span class="ui-button-text">&nbsp;X&nbsp;</span></button>
	</div>
</div>

<script src="/js/jquery.bpopup.min.js"></script>

<?php echo get_paging(G5_IS_MOBILE ? $config['cf_mobile_pages'] : $config['cf_write_pages'], $page, $total_page, "{$_SERVER['SCRIPT_NAME']}?$qstr&amp;page="); ?>

<script>

function MakeCoupon(cp_no){

	$('#popupLayer').bPopup({

		modalClose: false,
		content:'iframe', //'ajax', 'iframe' or 'image'
		iframeAttr:'frameborder=auto',
		iframeAttr:'frameborder=0',
		contentContainer:'.popupContent',
		loadUrl:'./rand_make_coupon.php?cp_no='+cp_no

	});

}

</script>

<?php
include_once (G5_ADMIN_PATH.'/admin.tail.php');
?>
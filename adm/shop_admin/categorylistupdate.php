<?php
$sub_menu = '600200';
include_once('./_common.php');

check_demo();

auth_check($auth[$sub_menu], "w");

check_admin_token();

/*************** 트랜잭션 관련 ****************/
$error_cnt = 0;
mysqli_autocommit($g5['connect_db'], false);
/*************** 트랜잭션 관련 ****************/

$al_data = array();
for ($i=0; $i<count($_POST['ca_id']); $i++)
{
    if ($_POST['ca_mb_id'][$i])
    {
        $sql = " select mb_id from {$g5['member_table']} where mb_id = '{$_POST['ca_mb_id'][$i]}' ";
        $row = sql_fetch($sql);
        if (!$row['mb_id'])
            alert("\'{$_POST['ca_mb_id'][$i]}\' 은(는) 존재하는 회원아이디가 아닙니다.", "./categorylist.php?$qstr");
    }
    
    if( !empty($_POST['ca_skin'][$i]) && ! is_include_path_check($_POST['ca_skin'][$i]) ){
        alert('오류 : 데이터폴더가 포함된 path 를 포함할수 없습니다.');
    }

    $sql = " update {$g5['g5_shop_category_table']}
                set ca_use              = '{$_POST['ca_use'][$i]}',
                    ca_web_view         = '{$_POST['ca_web_view'][$i]}',
                    ca_app_view         = '{$_POST['ca_app_view'][$i]}'
              where ca_id = '{$_POST['ca_id'][$i]}' ";
    sql_query($sql);
	/*************** 트랜잭션 관련 ****************/
	if( mysqli_errno($g5['connect_db']) ){
		$error_cnt += 1;
	}
	/*************** 트랜잭션 관련 ****************/

    $al_data[] = array(
        'ca_id' => $_POST['ca_id'][$i],
        'ca_use' => $_POST['ca_use'][$i],
        'ca_web_view' => $_POST['ca_web_view'][$i],
        'ca_app_view' => $_POST['ca_app_view'][$i],
    );
}

/************* 관리자 로그 처리 START *************/
insert_admin_log(600,600200, '일괄 수정', '', '', '', $_SERVER['REQUEST_URI'], $al_data);
/************* 관리자 로그 처리 END *************/

/*************** 트랜잭션 관련 ****************/
if ($error_cnt > 0) {
	mysqli_rollback($g5['connect_db']);
	mysqli_close($g5['connect_db']);
	echo "
	<script>
		alert('데이터베이스의 에러로 인해 롤백되었습니다.');
		history.back();
	</script>
	";
	exit;
} else {
	mysqli_commit($g5['connect_db']);
} 	
/*************** 트랜잭션 관련 ****************/

goto_url("./categorylist.php?$qstr");
?>

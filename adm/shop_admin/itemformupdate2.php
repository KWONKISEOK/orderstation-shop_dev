<?php
$sub_menu = '600300';
include_once('./_common.php');
include_once(G5_LIB_PATH.'/mailer.lib.php');

if ($w == "u" || $w == "d")
    check_demo();

if ($w == '' || $w == 'u')
    auth_check($auth[$sub_menu], "w");
else if ($w == 'd')
    auth_check($auth[$sub_menu], "d");

check_admin_token();

@mkdir(G5_DATA_PATH."/item_mod", G5_DIR_PERMISSION);
@chmod(G5_DATA_PATH."/item_mod", G5_DIR_PERMISSION);

// input vars 체크
check_input_vars();

// 파일정보
if($w == "u") {
    $sql = " select 
				it_img1_mod, it_img2_mod, it_img3_mod, it_img4_mod, it_img5_mod, it_img6_mod, it_img7_mod, it_img8_mod, it_img9_mod, it_img10_mod
                from {$g5['g5_shop_item_table']}
                where it_id = '$it_id' ";
    $file = sql_fetch($sql);

    $it_img1    = $file['it_img1_mod'];
    $it_img2    = $file['it_img2_mod'];
    $it_img3    = $file['it_img3_mod'];
    $it_img4    = $file['it_img4_mod'];
    $it_img5    = $file['it_img5_mod'];
    $it_img6    = $file['it_img6_mod'];
    $it_img7    = $file['it_img7_mod'];
    $it_img8    = $file['it_img8_mod'];
    $it_img9    = $file['it_img9_mod'];
    $it_img10   = $file['it_img10_mod'];

}

$it_img_dir = G5_DATA_PATH.'/item_mod';

// 파일삭제
if ($it_img1_del) {
    $file_img1 = $it_img_dir.'/'.$it_img1;
    @unlink($file_img1);
    delete_item_thumbnail(dirname($file_img1), basename($file_img1));
    $it_img1 = '';
}
if ($it_img2_del) {
    $file_img2 = $it_img_dir.'/'.$it_img2;
    @unlink($file_img2);
    delete_item_thumbnail(dirname($file_img2), basename($file_img2));
    $it_img2 = '';
}
if ($it_img3_del) {
    $file_img3 = $it_img_dir.'/'.$it_img3;
    @unlink($file_img3);
    delete_item_thumbnail(dirname($file_img3), basename($file_img3));
    $it_img3 = '';
}
if ($it_img4_del) {
    $file_img4 = $it_img_dir.'/'.$it_img4;
    @unlink($file_img4);
    delete_item_thumbnail(dirname($file_img4), basename($file_img4));
    $it_img4 = '';
}
if ($it_img5_del) {
    $file_img5 = $it_img_dir.'/'.$it_img5;
    @unlink($file_img5);
    delete_item_thumbnail(dirname($file_img5), basename($file_img5));
    $it_img5 = '';
}
if ($it_img6_del) {
    $file_img6 = $it_img_dir.'/'.$it_img6;
    @unlink($file_img6);
    delete_item_thumbnail(dirname($file_img6), basename($file_img6));
    $it_img6 = '';
}
if ($it_img7_del) {
    $file_img7 = $it_img_dir.'/'.$it_img7;
    @unlink($file_img7);
    delete_item_thumbnail(dirname($file_img7), basename($file_img7));
    $it_img7 = '';
}
if ($it_img8_del) {
    $file_img8 = $it_img_dir.'/'.$it_img8;
    @unlink($file_img8);
    delete_item_thumbnail(dirname($file_img8), basename($file_img8));
    $it_img8 = '';
}
if ($it_img9_del) {
    $file_img9 = $it_img_dir.'/'.$it_img9;
    @unlink($file_img9);
    delete_item_thumbnail(dirname($file_img9), basename($file_img9));
    $it_img9 = '';
}
if ($it_img10_del) {
    $file_img10 = $it_img_dir.'/'.$it_img10;
    @unlink($file_img10);
    delete_item_thumbnail(dirname($file_img10), basename($file_img10));
    $it_img10 = '';
}

// 이미지업로드
if ($_FILES['it_img1_mod']['name']) {
    if($w == 'u' && $it_img1) {
        $file_img1 = $it_img_dir.'/'.$it_img1;
        @unlink($file_img1);
        delete_item_thumbnail(dirname($file_img1), basename($file_img1));
    }
    $it_img1 = it_img_upload2($_FILES['it_img1_mod']['tmp_name'], $_FILES['it_img1_mod']['name'], $it_img_dir.'/'.$it_id);
}
if ($_FILES['it_img2_mod']['name']) {
    if($w == 'u' && $it_img2) {
        $file_img2 = $it_img_dir.'/'.$it_img2;
        @unlink($file_img2);
        delete_item_thumbnail(dirname($file_img2), basename($file_img2));
    }
    $it_img2 = it_img_upload2($_FILES['it_img2_mod']['tmp_name'], $_FILES['it_img2_mod']['name'], $it_img_dir.'/'.$it_id);
}
if ($_FILES['it_img3_mod']['name']) {
    if($w == 'u' && $it_img3) {
        $file_img3 = $it_img_dir.'/'.$it_img3;
        @unlink($file_img3);
        delete_item_thumbnail(dirname($file_img3), basename($file_img3));
    }
    $it_img3 = it_img_upload2($_FILES['it_img3_mod']['tmp_name'], $_FILES['it_img3_mod']['name'], $it_img_dir.'/'.$it_id);
}
if ($_FILES['it_img4_mod']['name']) {
    if($w == 'u' && $it_img4) {
        $file_img4 = $it_img_dir.'/'.$it_img4;
        @unlink($file_img4);
        delete_item_thumbnail(dirname($file_img4), basename($file_img4));
    }
    $it_img4 = it_img_upload2($_FILES['it_img4_mod']['tmp_name'], $_FILES['it_img4_mod']['name'], $it_img_dir.'/'.$it_id);
}
if ($_FILES['it_img5_mod']['name']) {
    if($w == 'u' && $it_img5) {
        $file_img5 = $it_img_dir.'/'.$it_img5;
        @unlink($file_img5);
        delete_item_thumbnail(dirname($file_img5), basename($file_img5));
    }
    $it_img5 = it_img_upload2($_FILES['it_img5_mod']['tmp_name'], $_FILES['it_img5_mod']['name'], $it_img_dir.'/'.$it_id);
}
if ($_FILES['it_img6_mod']['name']) {
    if($w == 'u' && $it_img6) {
        $file_img6 = $it_img_dir.'/'.$it_img6;
        @unlink($file_img6);
        delete_item_thumbnail(dirname($file_img6), basename($file_img6));
    }
    $it_img6 = it_img_upload2($_FILES['it_img6_mod']['tmp_name'], $_FILES['it_img6_mod']['name'], $it_img_dir.'/'.$it_id);
}
if ($_FILES['it_img7_mod']['name']) {
    if($w == 'u' && $it_img7) {
        $file_img7 = $it_img_dir.'/'.$it_img7;
        @unlink($file_img7);
        delete_item_thumbnail(dirname($file_img7), basename($file_img7));
    }
    $it_img7 = it_img_upload2($_FILES['it_img7_mod']['tmp_name'], $_FILES['it_img7_mod']['name'], $it_img_dir.'/'.$it_id);
}
if ($_FILES['it_img8_mod']['name']) {
    if($w == 'u' && $it_img8) {
        $file_img8 = $it_img_dir.'/'.$it_img8;
        @unlink($file_img8);
        delete_item_thumbnail(dirname($file_img8), basename($file_img8));
    }
    $it_img8 = it_img_upload2($_FILES['it_img8_mod']['tmp_name'], $_FILES['it_img8_mod']['name'], $it_img_dir.'/'.$it_id);
}
if ($_FILES['it_img9_mod']['name']) {
    if($w == 'u' && $it_img9) {
        $file_img9 = $it_img_dir.'/'.$it_img9;
        @unlink($file_img9);
        delete_item_thumbnail(dirname($file_img9), basename($file_img9));
    }
    $it_img9 = it_img_upload2($_FILES['it_img9_mod']['tmp_name'], $_FILES['it_img9_mod']['name'], $it_img_dir.'/'.$it_id);
}
if ($_FILES['it_img10_mod']['name']) {
    if($w == 'u' && $it_img10) {
        $file_img10 = $it_img_dir.'/'.$it_img10;
        @unlink($file_img10);
        delete_item_thumbnail(dirname($file_img10), basename($file_img10));
    }
    $it_img10 = it_img_upload2($_FILES['it_img10_mod']['tmp_name'], $_FILES['it_img10_mod']['name'], $it_img_dir.'/'.$it_id);
}

if ($w == "" || $w == "u")
{
    // 다음 입력을 위해서 옵션값을 쿠키로 한달동안 저장함
    //@setcookie("ck_ca_id",  $ca_id,  time() + 86400*31, $default[de_cookie_dir], $default[de_cookie_domain]);
    //@setcookie("ck_maker",  stripslashes($it_maker),  time() + 86400*31, $default[de_cookie_dir], $default[de_cookie_domain]);
    //@setcookie("ck_origin", stripslashes($it_origin), time() + 86400*31, $default[de_cookie_dir], $default[de_cookie_domain]);
    @set_cookie("ck_ca_id", $ca_id, time() + 86400*31);
    @set_cookie("ck_ca_id2", $ca_id2, time() + 86400*31);
    @set_cookie("ck_ca_id3", $ca_id3, time() + 86400*31);
    @set_cookie("ck_maker", stripslashes($it_maker), time() + 86400*31);
    @set_cookie("ck_origin", stripslashes($it_origin), time() + 86400*31);
}

/*************** 트랜잭션 관련 ****************/
$error_cnt = 0;
mysqli_autocommit($g5['connect_db'], false);
/*************** 트랜잭션 관련 ****************/


$it_id = strip_tags(trim($_POST['it_id']));
if ($it_id == "")
    alert("상품고유값이 없습니다.");

 if ($is_admin == 'vender') {

	 	$it_mod_memo = get_text($it_mod_memo);

		$sql_common = "
				it_status2			 = '2',
                it_price2            = '$it_price2',
				it_supply_price2     = '$it_supply_price2',
                it_img1_mod          = '$it_img1',
                it_img2_mod          = '$it_img2',
                it_img3_mod          = '$it_img3',
                it_img4_mod          = '$it_img4',
                it_img5_mod          = '$it_img5',
                it_img6_mod          = '$it_img6',
                it_img7_mod          = '$it_img7',
                it_img8_mod          = '$it_img8',
                it_img9_mod          = '$it_img9',
                it_img10_mod         = '$it_img10',
                it_explan_mod        = '$it_explan_mod',
                it_mobile_explan_mod = '$it_mobile_explan_mod',
				it_mod_memo			 = '$it_mod_memo',
				it_time_mod_reg		 = '".G5_TIME_YMDHIS."',
				it_time_mod_update	 = '".G5_TIME_YMDHIS."'
               ";


    $sql = " update {$g5['g5_shop_item_table']}
                set $sql_common
              where it_id = '$it_id' ";
    sql_query($sql);
	/*************** 트랜잭션 관련 ****************/
	if( mysqli_errno($g5['connect_db']) ){
		$error_cnt += 1;
	}
	/*************** 트랜잭션 관련 ****************/

}


/*************** 트랜잭션 관련 ****************/
if ($error_cnt > 0) {
	mysqli_rollback($g5['connect_db']);
	mysqli_close($g5['connect_db']);
	echo "
	<script>
		alert('데이터베이스의 에러로 인해 롤백되었습니다.');
		history.back();
	</script>
	";
	exit;
} else {
	mysqli_commit($g5['connect_db']);


	if( !empty($comp_code) ){

		$sql = " SELECT * FROM tbl_member WHERE comp_code = '{$comp_code}' ";
		$rs = sql_fetch($sql);

		$subject = "상품변경요청 ";
		$content = "공급사코드:".$rs['comp_code']." "."공급사명:".$rs['comp_name']." "."상품코드:".$it_id;

		mailer($config['cf_admin_email_name'], $config['cf_admin_email'], $config['cf_admin_email'], $subject, $content, 1);

	}

} 	
/*************** 트랜잭션 관련 ****************/

if ($w == "u") {
	$qstr  = $qstr.'&amp;s_comp_code='.$s_comp_code.'&amp;sca='.$sca.'&amp;page='.$page.'&amp;save_stx='.$stx;
	$qstr  = $qstr.'&amp;s_it_main1='.$s_it_main1.'&amp;s_it_main2='.$s_it_main2.'&amp;s_it_main3='.$s_it_main3;
	$qstr  = $qstr.'&amp;s_it_type1='.$s_it_type1.'&amp;s_it_type2='.$s_it_type2.'&amp;s_it_type3='.$s_it_type3.'&amp;s_it_type4='.$s_it_type4.'&amp;s_it_time='.$s_it_time;

    goto_url("./itemform2.php?w=u&amp;it_id=$it_id&amp;$qstr");
} else if ($w == "d")  {
    $qstr = "ca_id=$ca_id&amp;sfl=$sfl&amp;sca=$sca&amp;page=$page&amp;stx=".urlencode($stx)."&amp;save_stx=".urlencode($save_stx);
    goto_url("./itemlist.php?$qstr");
}

echo "<meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\">";
?>
<script>
    if (confirm("계속 입력하시겠습니까?"))
        location.href = "<?php echo "./itemform2.php?".str_replace('&amp;', '&', $qstr); ?>";
    else {
		<?php if ($is_admin == 'vender') { ?>
			location.href = "<?php echo "./itemlist_vender2.php?".str_replace('&amp;', '&', $qstr); ?>";
		<?php } else { ?>
			location.href = "<?php echo "./itemlist.php?".str_replace('&amp;', '&', $qstr); ?>";
		<?php } ?>
	}
</script>

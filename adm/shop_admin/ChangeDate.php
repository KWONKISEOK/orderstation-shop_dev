<?php
$sub_menu = '400400';
include_once('./_common.php');

auth_check($auth[$sub_menu], "r");

include_once(G5_PATH.'/head.sub.php');
include_once(G5_PLUGIN_PATH.'/jquery-ui/datepicker.php');

if( empty($od_id) || empty($od_seq) ){
	echo "필수값이 없습니다.";
	exit;
}

?>
<link type="text/css" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/themes/base/jquery-ui.css" rel="stylesheet" />
<link type="text/css" href="/plugin/jquery-ui/style.css?ver=171222">

<div class="tbl_head01 tbl_wrap" style="padding:10px;margin-top:25px;">

<form name="frm">
	<input type="hidden" name="od_id" value="<?=$od_id?>"/>
	<input type="hidden" name="od_seq" value="<?=$od_seq?>"/>
    <table>
    <thead>
	<tr>
        <th scope="col">배송일자변경</th>
	</tr>
	</thead>
	<tbody>
		<tr>
			<td>
				발송예정일
				<input type="text" id="od_period_date" name="od_period_date" style="width:100px;" autocomplete="off"/>&nbsp;
				<input type="button" value="저장" onclick="javascript:FrmSend();"/>
				(미입금인 경우만 변경 가능합니다.)
			</td>
		</tr>
	</tbody>
	</table>

</form>

<iframe src="" id="iFrm" name="iFrm" frameborder="0"/></iframe>

</div>

<script>

function FrmSend(){

	var frm = document.frm;

	if( frm.od_period_date.value == "" ){
		alert("발송예정일을 입력해주세요");
		return;
	}

	frm.target = "iFrm";
	frm.method = "post";
	frm.action = "./ChangeDateProc.php";
	frm.submit();

}

function ClosePop(){

	parent.location.reload();

}

$(function(){

    $("#od_period_date").datepicker({ 
		changeMonth: true, 
		changeYear: true, 
		dateFormat: "yy-mm-dd", 
		showButtonPanel: true, 
		yearRange: "c-99:c+99"
	});
	
});

</script>
<?php
$sub_menu = '600300';
include_once('./_common.php');
include_once(G5_EDITOR_LIB);
include_once(G5_LIB_PATH.'/iteminfo.lib.php');

auth_check($auth[$sub_menu], "w");

$html_title = "상품 변경 신청";

$qstr  = $qstr.'&amp;s_comp_code='.$s_comp_code.'&amp;sca='.$sca.'&amp;page='.$page.'&amp;save_stx='.$stx;
$qstr  = $qstr.'&amp;s_it_main1='.$s_it_main1.'&amp;s_it_main2='.$s_it_main2.'&amp;s_it_main3='.$s_it_main3;
$qstr  = $qstr.'&amp;s_it_type1='.$s_it_type1.'&amp;s_it_type2='.$s_it_type2.'&amp;s_it_type3='.$s_it_type3.'&amp;s_it_type4='.$s_it_type4.'&amp;s_it_time='.$s_it_time;

if ($w == "")
{
    $html_title .= "";
	$comp_code = $member['comp_code'];

    // 옵션은 쿠키에 저장된 값을 보여줌. 다음 입력을 위한것임
    //$it[ca_id] = _COOKIE[ck_ca_id];
    $it['ca_id'] = get_cookie("ck_ca_id");
    $it['ca_id2'] = get_cookie("ck_ca_id2");
    $it['ca_id3'] = get_cookie("ck_ca_id3");
    if (!$it['ca_id'])
    {
        $sql = " select ca_id from {$g5['g5_shop_category_table']} order by ca_order, ca_id limit 1 ";
        $row = sql_fetch($sql);
        if (!$row['ca_id'])
            alert("등록된 분류가 없습니다. 우선 분류를 등록하여 주십시오.", './categorylist.php');
        $it['ca_id'] = $row['ca_id'];
    }
    //$it[it_maker]  = stripslashes($_COOKIE[ck_maker]);
    //$it[it_origin] = stripslashes($_COOKIE[ck_origin]);
    $it['it_maker']  = stripslashes(get_cookie("ck_maker"));
    $it['it_origin'] = stripslashes(get_cookie("ck_origin"));
}
else if ($w == "u")
{
    $html_title .= "";

    /*if ($is_admin != 'super')
    {
        $sql = " select it_id from {$g5['g5_shop_item_table']} a, {$g5['g5_shop_category_table']} b
                  where a.it_id = '$it_id'
                    and a.ca_id = b.ca_id
                    and b.ca_mb_id = '{$member['mb_id']}' ";
        $row = sql_fetch($sql);
        if (!$row['it_id'])
            alert("\'{$member['mb_id']}\' 님께서 수정 할 권한이 없는 상품입니다.");
    }*/

    $sql = " select * from {$g5['g5_shop_item_table']} where it_id = '$it_id' ";
    $it = sql_fetch($sql);

    if(!$it)
        alert('상품정보가 존재하지 않습니다.');

	$comp_code = $it['comp_code'];

    if (!$ca_id)
        $ca_id = $it['ca_id'];

    $sql = " select * from {$g5['g5_shop_category_table']} where ca_id = '$ca_id' ";
    $ca = sql_fetch($sql);
}
else
{
    alert();
}

//$qstr  = $qstr.'&amp;sca='.$sca.'&amp;page='.$page;

$g5['title'] = $html_title;
include_once (G5_ADMIN_PATH.'/admin.head.php');

// 분류리스트
$category_select = '';
$script = '';
$sql = " select * from {$g5['g5_shop_category_table']} ";
//if ($is_admin != 'super')
//    $sql .= " where ca_mb_id = '{$member['mb_id']}' ";
$sql .= " order by ca_order, ca_id ";
$result = sql_query($sql);
for ($i=0; $row=sql_fetch_array($result); $i++)
{
    $len = strlen($row['ca_id']) / 2 - 1;

    $nbsp = "";
    for ($i=0; $i<$len; $i++)
        $nbsp .= "&nbsp;&nbsp;&nbsp;";

    $category_select .= "<option value=\"{$row['ca_id']}\">$nbsp{$row['ca_name']}</option>\n";

    $script .= "ca_use['{$row['ca_id']}'] = {$row['ca_use']};\n";
    $script .= "ca_stock_qty['{$row['ca_id']}'] = {$row['ca_stock_qty']};\n";
    //$script .= "ca_explan_html['$row[ca_id]'] = $row[ca_explan_html];\n";
    $script .= "ca_sell_email['{$row['ca_id']}'] = '{$row['ca_sell_email']}';\n";
}
/*
// 재입고알림 설정 필드 추가
if(!sql_query(" select it_stock_sms from {$g5['g5_shop_item_table']} limit 1 ", false)) {
    sql_query(" ALTER TABLE `{$g5['g5_shop_item_table']}`
                    ADD `it_stock_sms` tinyint(4) NOT NULL DEFAULT '0' AFTER `it_stock_qty` ", true);
}

// 추가옵션 포인트 설정 필드 추가
if(!sql_query(" select it_supply_point from {$g5['g5_shop_item_table']} limit 1 ", false)) {
    sql_query(" ALTER TABLE `{$g5['g5_shop_item_table']}`
                    ADD `it_supply_point` int(11) NOT NULL DEFAULT '0' AFTER `it_point_type` ", true);
}

// 상품메모 필드 추가
if(!sql_query(" select it_shop_memo from {$g5['g5_shop_item_table']} limit 1 ", false)) {
    sql_query(" ALTER TABLE `{$g5['g5_shop_item_table']}`
                    ADD `it_shop_memo` text NOT NULL AFTER `it_use_avg` ", true);
}

// 지식쇼핑 PID 필드추가
// 상품메모 필드 추가
if(!sql_query(" select ec_mall_pid from {$g5['g5_shop_item_table']} limit 1 ", false)) {
    sql_query(" ALTER TABLE `{$g5['g5_shop_item_table']}`
                    ADD `ec_mall_pid` varchar(255) NOT NULL AFTER `it_shop_memo` ", true);
}
*/

$pg_anchor ='<ul class="anchor">
<li><a href="#anc_sitfrm_cate">상품분류</a></li>
<li><a href="#anc_sitfrm_skin">스킨설정</a></li>
<li><a href="#anc_sitfrm_ini">기본정보</a></li>
<li><a href="#anc_sitfrm_compact">요약정보</a></li>
<li><a href="#anc_sitfrm_cost">가격 및 재고</a></li>
<li><a href="#anc_sitfrm_sendcost">배송비</a></li>
<li><a href="#anc_sitfrm_img">상품이미지</a></li>
<li><a href="#anc_sitfrm_relation">관련상품</a></li>
<li><a href="#anc_sitfrm_event">관련이벤트</a></li>
<li><a href="#anc_sitfrm_optional">상세설명설정</a></li>
<li><a href="#anc_sitfrm_extra">여분필드</a></li>
</ul>
';

/*
// 쿠폰적용안함 설정 필드 추가
if(!sql_query(" select it_nocoupon from {$g5['g5_shop_item_table']} limit 1", false)) {
    sql_query(" ALTER TABLE `{$g5['g5_shop_item_table']}`
                    ADD `it_nocoupon` tinyint(4) NOT NULL DEFAULT '0' AFTER `it_use` ", true);
}

// 스킨필드 추가
if(!sql_query(" select it_skin from {$g5['g5_shop_item_table']} limit 1", false)) {
    sql_query(" ALTER TABLE `{$g5['g5_shop_item_table']}`
                    ADD `it_skin` varchar(255) NOT NULL DEFAULT '' AFTER `ca_id3`,
                    ADD `it_mobile_skin` varchar(255) NOT NULL DEFAULT '' AFTER `it_skin` ", true);
}
*/

$readonly = false;
if($is_admin =='vender' and  $it['it_status'] == 3) {
	$readonly = true;
}
?>

<form name="fitemform" action="./itemformupdate2.php" method="post" enctype="MULTIPART/FORM-DATA" autocomplete="off" onsubmit="return fitemformcheck(this)">

<input type="hidden" name="codedup" value="<?php echo $default['de_code_dup_use']; ?>">
<input type="hidden" name="w" value="<?php echo $w; ?>">
<input type="hidden" name="sca" value="<?php echo $sca; ?>">
<input type="hidden" name="sst" value="<?php echo $sst; ?>">
<input type="hidden" name="sod"  value="<?php echo $sod; ?>">
<input type="hidden" name="sfl" value="<?php echo $sfl; ?>">
<input type="hidden" name="stx"  value="<?php echo $stx; ?>">
<input type="hidden" name="page" value="<?php echo $page; ?>">
<input type="hidden" name="s_it_main1" value="<?php echo $s_it_main1; ?>">
<input type="hidden" name="s_it_main2" value="<?php echo $s_it_main2; ?>">
<input type="hidden" name="s_it_main3" value="<?php echo $s_it_main3; ?>">
<input type="hidden" name="s_it_type1" value="<?php echo $s_it_type1; ?>">
<input type="hidden" name="s_it_type2" value="<?php echo $s_it_type2; ?>">
<input type="hidden" name="s_it_type3" value="<?php echo $s_it_type3; ?>">
<input type="hidden" name="s_it_type4" value="<?php echo $s_it_type4; ?>">
<input type="hidden" name="s_it_time" value="<?php echo $s_it_time; ?>">
<input type="hidden" name="s_comp_code" value="<?php echo $s_comp_code; ?>">
<input type="hidden" name="it_id" value="<?php echo $it['it_id']; ?>">
<?php if ($is_admin != 'super') { ?>
<input type="hidden" name="comp_code" value="<?php echo $comp_code; ?>">
<?php } ?>



<section id="anc_sitfrm_cost">

    <div class="tbl_frm01 tbl_wrap">
        <table>
        <caption>변경사항</caption>
        <colgroup>
            <col class="grid_4">
            <col>
            <col class="grid_3">
        </colgroup>
        <tbody>
        <tr>
            <th scope="row"><label for="it_price2">수정요청 소비자가</label></th>
            <td>
                
				<?php if($readonly) {?>
				<input type="text" name="it_price2" value="<?php echo $it['it_price2']; ?>" id="it_price2" class="frm_input" size="8"> 원 
				<?php } else { ?>
				<input type="text" name="it_price2" value="<?php echo $it['it_price2']; ?>" id="it_price2" class="frm_input" size="8" > 원
				<?php } ?>
            </td>
			<th ><label for="it_supply_price2">수정요청 공급가</label></th>
				<td >
					<?php if($readonly) {?>
					<input type="text" name="it_supply_price2" value="<?php echo $it['it_supply_price2']; ?>" id="it_supply_price2" class="frm_input" size="8" > 원 
					<?php } else { ?>
					<input type="text" name="it_supply_price2" value="<?php echo $it['it_supply_price2']; ?>" id="it_supply_price2" class="frm_input" size="8" > 원
					<?php } ?>
				</td>
        </tr>
		</table>
	</div>
</section>



<section id="anc_sitfrm_img">
    

    <div class="tbl_frm01 tbl_wrap">
        <table>
        <caption>이미지 업로드</caption>
        <colgroup>
            <col class="grid_4">
            <col>
        </colgroup>
        <tbody>
        <?php 
		
		$ImgSubject = array(
			"이미지 1","이미지 2","이미지 3","이미지 4","이미지 5",
			"이미지 6","이미지 7","이미지 8","이미지 9","이미지 10"
		);

		for($i=1; $i<=10; $i++) { 
		?>
        <tr>
            <th scope="row"><label for="it_img<?php echo $i; ?>_mod"><?=$ImgSubject[$i-1];?></label></th>
            <td>
                <input type="file" name="it_img<?php echo $i; ?>_mod" id="it_img<?php echo $i; ?>_mod">
                <?php
                $it_img_mod = G5_DATA_PATH.'/item_mod/'.$it['it_img'.$i.'_mod'];

                if(is_file($it_img_mod) && $it['it_img'.$i.'_mod']) {
                    $size = @getimagesize($it_img_mod);
                    $thumb = get_it_thumbnail($it['it_img'.$i.'_mod'], 25, 25);
                ?>
                <label for="it_img<?php echo $i; ?>_del"><span class="sound_only">이미지 <?php echo $i; ?> </span>파일삭제</label>
                <input type="checkbox" name="it_img<?php echo $i; ?>_del" id="it_img<?php echo $i; ?>_del" value="1">
                <span class="sit_wimg_limg<?php echo $i; ?>"><?php echo $thumb; ?></span>
                <div id="limg<?php echo $i; ?>" class="banner_or_img">
                    <img src="<?php echo G5_DATA_URL; ?>/item_mod/<?php echo $it['it_img'.$i.'_mod']; ?>" alt="" width="<?php echo $size[0]; ?>" height="<?php echo $size[1]; ?>">
                    <button type="button" class="sit_wimg_close">닫기</button>
                </div>
                <script>
                $('<button type="button" id="it_limg<?php echo $i; ?>_view" class="btn_frmline sit_wimg_view">이미지 확인</button>').appendTo('.sit_wimg_limg<?php echo $i; ?>');
                </script>
                <?php } ?>
            </td>
        </tr>
        <?php } ?>
        <tr>
            <th scope="row"><label for="it_img<?php echo $i; ?>_mod">상품설명</label></th>
            <td><?php echo editor_html('it_explan_mod', get_text($it['it_explan_mod'], 0)); ?></td>
		</tr>
        <tr>
            <th scope="row"><label for="it_img<?php echo $i; ?>_mod">모바일 상품설명</label></th>
            <td><?php echo editor_html('it_mobile_explan_mod', get_text($it['it_mobile_explan_mod'], 0)); ?></td>
		</tr>
		<tr>
			<th scope="row">상세페이지 수정요청사항</th>
			<td>
				<textarea name="it_mod_memo" maxlength="1000" style="height:50px;"><?=$it['it_mod_memo']?></textarea>
			</td>
		</tr>
        </tbody>
        </table>
    </div>
</section>




<div class="btn_fixed_top">
	<?php if ($is_admin == 'vender') { ?>
		<a href="./itemlist_vender2.php?<?php echo $qstr; ?>" class="btn btn_02">목록</a>
		<input type="submit" value="확인" class="btn_submit btn" accesskey="s">
	<?php } else { ?>
		<a href="./itemlist.php?<?php echo $qstr; ?>" class="btn btn_02">목록</a>
	<?php } ?>

    <a href="<?php echo G5_SHOP_URL ;?>/item.php?it_id=<?php echo $it_id ;?>" class="btn_02  btn">상품보기</a>
</div>
</form>

<script>

$(document).on("keypress", 'form', function (e) {
    var code = e.keyCode || e.which;
    //console.log(code);
    if (code == 13) {
        //console.log('Inside');
        e.preventDefault();
        return false;
    }
});



var f = document.fitemform;

<?php if ($w == 'u') { ?>
$(".banner_or_img").addClass("sit_wimg");
$(function() {
    $(".sit_wimg_view").bind("click", function() {
        var sit_wimg_id = $(this).attr("id").split("_");
        var $img_display = $("#"+sit_wimg_id[1]);

        $img_display.toggle();

        if($img_display.is(":visible")) {
            $(this).text($(this).text().replace("확인", "닫기"));
        } else {
            $(this).text($(this).text().replace("닫기", "확인"));
        }

        var $img = $("#"+sit_wimg_id[1]).children("img");
        var width = $img.width();
        var height = $img.height();
        if(width > 700) {
            var img_width = 700;
            var img_height = Math.round((img_width * height) / width);

            $img.width(img_width).height(img_height);
        }
    });
    $(".sit_wimg_close").bind("click", function() {
        var $img_display = $(this).parents(".banner_or_img");
        var id = $img_display.attr("id");
        $img_display.toggle();
        var $button = $("#it_"+id+"_view");
        $button.text($button.text().replace("닫기", "확인"));
    });
});
<?php } ?>


function fitemformcheck(f)
{
	var cnt = 0;

	oEditors.getById["it_explan_mod"].exec("UPDATE_CONTENTS_FIELD", []);
	oEditors.getById["it_mobile_explan_mod"].exec("UPDATE_CONTENTS_FIELD", []);
    var it_explan_mod = $("#it_explan_mod").val();
		it_explan_mod=it_explan_mod.replace(/&nbsp;/gi,"");
		it_explan_mod=it_explan_mod.replace(/<br>/gi,"");
		it_explan_mod=it_explan_mod.replace(/ /gi,"");
    var it_mobile_explan_mod = $("#it_mobile_explan_mod").val();
		it_mobile_explan_mod=it_mobile_explan_mod.replace(/&nbsp;/gi,"");
		it_mobile_explan_mod=it_mobile_explan_mod.replace(/<br>/gi,"");
		it_mobile_explan_mod=it_mobile_explan_mod.replace(/ /gi,"");

	if( f.it_price2.value != "0" ) {

		try{
			if( !parseInt(f.it_price2.value) > 0 || f.it_price2.value == "" ){
				alert("수정요청 소비자가를 0보다큰 숫자형식으로 입력해주세요");
				return false;
			}		
		}
		catch (e){
				alert("수정요청 소비자가를 0보다큰 숫자형식으로 입력해주세요");
				return false;
		}

		cnt++;

	}

	if( f.it_supply_price2.value != "0" ) {

		try{
			if( !parseInt(f.it_supply_price2.value) > 0 || f.it_supply_price2.value == "" ){
				alert("수정요청 공급가를 0보다큰 숫자형식으로 입력해주세요");
				return false;
			}		
		}
		catch (e){
				alert("수정요청 공급가를 0보다큰 숫자형식으로 입력해주세요");
				return false;
		}

		cnt++;

	}

	if( f.it_img1_mod.value != "" ){ cnt++; }
	if( f.it_img2_mod.value != "" ){ cnt++; }
	if( f.it_img3_mod.value != "" ){ cnt++; }
	if( f.it_img4_mod.value != "" ){ cnt++; }
	if( f.it_img5_mod.value != "" ){ cnt++; }
	if( f.it_img6_mod.value != "" ){ cnt++; }
	if( f.it_img7_mod.value != "" ){ cnt++; }
	if( f.it_img8_mod.value != "" ){ cnt++; }
	if( f.it_img9_mod.value != "" ){ cnt++; }
	if( f.it_img10_mod.value != "" ){ cnt++; }

	if( it_explan_mod != "" && it_explan_mod != "<p></p>" ){
		cnt++;
	}
	if( it_mobile_explan_mod != "" && it_explan_mod != "<p></p>" ){
		cnt++;
	}

	if( f.it_mod_memo.value != "" ){
		cnt++;
	}

	if( cnt == 0 ){
		alert("수정요청 가격 , 수정요청 이미지 , 상품설명을 입력해주세요");
		return false;
	}

	<?php echo get_editor_js('it_explan_mod'); ?>
	<?php echo get_editor_js('it_mobile_explan_mod'); ?>

    return true;
}


function enterkey() {
        if (window.event.keyCode == 13) {
			var ca_id = $("#sch_relation").val();
			var it_name = $.trim($("#sch_name").val());
			var $relation = $("#relation");

			if(ca_id == "" && it_name == "") {
				$relation.html("<p>상품의 분류를 선택하시거나 상품명을 입력하신 후 검색하여 주십시오.</p>");
				return false;
			}

			$("#relation").load(
				"./itemformrelation.php",
				{ it_id: "<?php echo $it_id; ?>", ca_id: ca_id, it_name: it_name }
			);
        }
}

</script>

<?php
include_once (G5_ADMIN_PATH.'/admin.tail.php');
?>


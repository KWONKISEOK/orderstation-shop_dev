<?php
//$sub_menu = '500120';
include_once('./_common.php');

//auth_check($auth[$sub_menu], "r");

//print_r2($_REQUEST); exit;

/*
function multibyte_digit($source)
{
    $search  = array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9");
    $replace = array("０","１","２","３","４","５","６","７","８","９");
    return str_replace($search, $replace, (string)$source);
}
*/

function conv_telno($t)
{
    // 숫자만 있고 0으로 시작하는 전화번호
    if (!preg_match("/[^0-9]/", $t) && preg_match("/^0/", $t)) {
        if (preg_match("/^01/", $t)) {
            $t = preg_replace("/([0-9]{3})(.*)([0-9]{4})/", "\\1-\\2-\\3", $t);
        } else if (preg_match("/^02/", $t)) {
            $t = preg_replace("/([0-9]{2})(.*)([0-9]{4})/", "\\1-\\2-\\3", $t);
        } else {
            $t = preg_replace("/([0-9]{3})(.*)([0-9]{4})/", "\\1-\\2-\\3", $t);
        }
    }

    return $t;
}

$fr_date = date_conv($fr_date);
if ($to_date == '') $to_date = date('Y-m-d');
$to_date = date_conv($to_date);

$sql = " select a.od_id
	                 ,a.mb_id
					 ,a.od_name
					 ,a.pharm_name
					 ,a.pharm_manager
					 ,a.od_name
					 ,a.od_tel
					 ,a.od_hp
					 ,a.od_zip1
					 ,a.od_zip2
					 ,a.od_addr1
					 ,a.od_addr2
					 ,a.od_addr3
					 ,a.od_addr_jibeon					 
					 ,a.od_settle_case
					 ,a.pharm_custno
					 ,a.od_cart_price
					 ,a.od_gubun
					 ,a.od_mobile
					 ,(select comp_it_id from tbl_shop_item where it_id = b.it_id limit 1 ) comp_it_id
					 ,(select comp_it_id from tbl_shop_item where it_id = b.io_id limit 1 ) comp_io_id
					 ,(select mb_pay_yn from tbl_member where mb_id = a.mb_id) mb_pay_yn					 
					 ,(select mb_name from tbl_member where mb_id= a.pharm_manager ) pharm_manager_name
					 ,(select mb_name from tbl_member where pharm_custno= a.pharm_custno and pharm_custno != '' limit 1) pharm_mb_name
					 ,b.it_id
					 ,b.io_id
					 ,b.od_num
					 ,b.it_name
					 ,b.od_option
					 ,b.od_period_yn
					 ,b.od_period_cnt
					 ,b.od_sum_drug_price
					 ,b.od_sum_incen
					 ,b.od_total_drug_price
					 ,b.od_total_incen
					 ,b.od_total_supply_price
					 ,c.od_b_name
					 ,c.od_b_tel
					 ,c.od_b_hp
					 ,c.od_b_zip1
					 ,c.od_b_zip2
					 ,c.od_b_addr1
					 ,c.od_b_addr2
					 ,c.od_b_addr3
					 ,c.od_b_addr_jibeon
					 ,c.od_seq
					 ,c.od_date1
					 ,c.od_date2
					 ,c.od_date3
					 ,c.od_date4
					 ,c.od_qty
					 ,c.od_period_date
					 ,c.od_status
					 ,c.od_pay_yn
					 ,c.od_delivery_company
					 ,c.od_invoice
					 ,c.od_trans_memo
					 ,d.comp_name
				 from tbl_shop_order a, tbl_shop_order_detail b, tbl_shop_order_receiver c, tbl_member d
				 where a.od_id = b.od_id 
				   and b.od_id = c.od_id 
				   and b.od_num = c.od_num
				   and b.comp_code = d.comp_code
				   and c.od_status != '취소'";

switch ($od_date) {
    case '1':
        $sql .= " and  c.od_date1 between '$fr_date 00:00:00' and '$to_date 23:59:59' ";
        break;
    case '2':
        $sql .= " and  c.od_date2 between '$fr_date 00:00:00' and '$to_date 23:59:59' ";
        break;
    case '3':
        $sql .= " and  c.od_date3 between '$fr_date 00:00:00' and '$to_date 23:59:59' ";
        break;
    case '4':
        $sql .= " and c.od_date4 between '$fr_date 00:00:00' and '$to_date 23:59:59' ";
        break;
    default:
        $sql .= " and  c.od_date1 between '$fr_date 00:00:00' and '$to_date 23:59:59' ";
        break;
}
//주문구분
if ($od_gubun) {
    $sql .= " and a.od_gubun = '$od_gubun' ";
}

//상태
if ($od_status) {
    $sql .= " and c.od_status = '$od_status' ";
}
//일반 정기
if ($od_period_yn == 'Y') {
    $sql .= " and b.od_period_yn = '$od_period_yn' ";
}
//공급사코드
$sql .= " and b.comp_code = '$comp_code' ";


//상품명
if ($sel_field == 'it_name') {
    $sql .= " and $sel_field like '%$search%' ";
}
$sql .= " order by a.od_id desc, c.od_num, c.od_seq ";
//echo $sql;
//exit;


$result = sql_query($sql);
$cnt = @sql_num_rows($result);
if (!$cnt)
    alert("출력할 내역이 없습니다.");

/*================================================================================
php_writeexcel http://www.bettina-attack.de/jonny/view.php/projects/php_writeexcel/
=================================================================================*/
include_once(G5_LIB_PATH.'/Excel/php_writeexcel/class.writeexcel_workbook.inc.php');
include_once(G5_LIB_PATH.'/Excel/php_writeexcel/class.writeexcel_worksheet.inc.php');

$fname = tempnam(G5_DATA_PATH, "tmp-orderlist.xls");
$workbook = new writeexcel_workbook($fname);
$worksheet = $workbook->addworksheet();
// Put Excel data
if ($ptype == 1) {
    $data = array('주문일시',
                  '주문구분',
                  '주문번호',
                  '담당자명',
                  '약국명',
                  '약국코드',
                  '약사명',
                  '회원ID',
                  '주문자명',
                  '주문자hp',
                  '우편번호',
                  '주소',
                  '이름',
                  '배송지hp',
                  '배송지우편번호',
                  '배송지주소',
                  '배송주의사항',
                  '상품코드',
                  '상품명',
                  '옵션상품코드',
                  '옵션명',
                  '주문수량',
                  '공급가(옵션포함)',
                  '주문상태',
                  '정기주문여부',
                  '배송예정일',
                  '정기주문회차',
                  '배송회사',
                  '송장번호',
                  '배송일시');
    array_push($data, "공급사 상품코드");

} else {

    $data = array('이름', '배송지hp', '배송지우편번호', '배송지주소', '오더스테이션 상품코드', '공급사 상품코드', '옵션명', '주문수량', '공급사명', '배송메시지');
    if ($comp_code == "389") {
        $data = array('약국명', '이름', '배송지hp', '배송지우편번호', '배송지주소', '오더스테이션 상품코드', '공급사 상품코드', '옵션명', '주문수량', '공급사명', '배송메시지');
    }
}

$data = array_map('iconv_euckr', $data);

$col = 0;
foreach ($data as $cell) {
    $worksheet->write(0, $col++, $cell);
}

$save_it_id = '';
for ($i = 1; $row = sql_fetch_array($result); $i++) {

    //아스키 0부터127제거
    $od_option = mb_convert_encoding(preg_replace('/[\x00-\x1F\x7F\xA0]/u', ' ', $row['od_option']), "euc-kr", "utf-8");

    $row = array_map('iconv_euckr', $row);

    if ($ptype == 1) {

        switch ($row['od_gubun']) {
            case '1':
                $od_gubun = iconv("utf-8", "euc-kr", '고객주문');
                break;
            case '2':
                $od_gubun = iconv("utf-8", "euc-kr", '약국주문');
                break;
            case '3':
                $od_gubun = iconv("utf-8", "euc-kr", '일반주문');
                break;
        }
        //$sql = " select a.*, (select mb_name from tbl_member where mb_id = a.pharm_manager) pharm_manager  from tbl_member a where pharm_custno='{$row['pharm_custno']}' ";
        //$row2 = sql_fetch($sql);
        //$row2 = array_map('iconv_euckr', $row2);

        $worksheet->write($i, 0, ' '.$row['od_date1']);
        $worksheet->write($i, 1, ' '.$od_gubun);
        $worksheet->write($i, 2, ' '.$row['od_id']);
        $worksheet->write($i, 3, ' '.$row['pharm_manager_name']);
        $worksheet->write($i, 4, ' '.$row['pharm_name']);
        $worksheet->write($i, 5, ' '.$row['pharm_custno']);
        $worksheet->write($i, 6, ' '.$row['pharm_mb_name']);
        $worksheet->write($i, 7, ' '.$row['mb_id']);
        $worksheet->write($i, 8, ' '.$row['od_name']);
        $worksheet->write($i, 9, ' '.$row['od_hp']);
        $worksheet->write($i, 10, ' '.$row['od_zip1'].$row['od_zip2']);
        $worksheet->write($i, 11, print_address($row['od_addr1'], $row['od_addr2'], $row['od_addr3'], $row['od_addr_jibeon']));
        $worksheet->write($i, 12, ' '.$row['od_b_name']);
        $worksheet->write($i, 13, ' '.$row['od_b_hp']);
        $worksheet->write($i, 14, ' '.$row['od_b_zip1'].$row['od_b_zip2']);
        $worksheet->write($i, 15, print_address($row['od_b_addr1'], $row['od_b_addr2'], $row['od_b_addr3'], $row['od_b_addr_jibeon']));
        $worksheet->write($i, 16, $row['od_memo']);
        $worksheet->write($i, 17, ' '.$row['it_id']);
        $worksheet->write($i, 18, ' '.$row['it_name']);
        $worksheet->write($i, 19, ' '.$row['io_id']);
        $worksheet->write($i, 20, ' '.$od_option);
        $worksheet->write($i, 21, ' '.$row['od_qty']);


        if ($row['od_period_yn'] == 'Y') {
            $od_seq = $row['od_seq'];
            $worksheet->write($i, 22, ' '.$row['od_total_supply_price'] / $row['od_period_cnt']);

        } else {
            $od_seq = 0;
            $worksheet->write($i, 22, ' '.$row['od_total_supply_price']);
        }
        $worksheet->write($i, 23, ' '.$row['od_status']);
        $worksheet->write($i, 24, ' '.$row['od_period_yn']);
        $worksheet->write($i, 25, ' '.$row['od_period_date']);
        $worksheet->write($i, 26, ' '.$od_seq);
        $worksheet->write($i, 27, ' '.$row['od_delivery_company']);
        $worksheet->write($i, 28, ' '.$row['od_invoice']);
        $worksheet->write($i, 29, ' '.$row['od_date3']);
        if ($row['comp_io_id'] != '') {
            $worksheet->write($i, 30, ' '.$row['comp_io_id']);
        } else {
            $worksheet->write($i, 30, ' '.$row['comp_it_id']);
        }
    } else {
        if ($comp_code == "389") { //비옥신만 약국명 출력
            array('약국명', '이름', '배송지hp', '배송지우편번호', '배송지주소', '오더스테이션 상품코드', '공급사 상품코드', '옵션명', '주문수량', '공급사명', '배송메시지');
            $worksheet->write($i, 0, ' '.$row['pharm_name']);
            $worksheet->write($i, 1, ' '.$row['od_b_name']);
            $worksheet->write($i, 2, ' '.$row['od_b_hp']);
            $worksheet->write($i, 3, ' '.$row['od_b_zip1'].$row['od_b_zip2']);
            $worksheet->write($i, 4, print_address($row['od_b_addr1'], $row['od_b_addr2'], $row['od_b_addr3'], $row['od_b_addr_jibeon']));
            $worksheet->write($i, 5, ' '.$row['it_id']);
            if ($row['comp_io_id'] != '') {
                $worksheet->write($i, 6, ' '.$row['comp_io_id']);
            } else {
                $worksheet->write($i, 6, ' '.$row['comp_it_id']);
            }
            $worksheet->write($i, 7, ' '.$row['it_name'].' '.$od_option);
            $worksheet->write($i, 8, ' '.$row['od_qty']);
            $worksheet->write($i, 9, ' '.$row['comp_name']);
            $worksheet->write($i, 10, $row['od_trans_memo']);
        }
        array('이름.', '배송지hp', '배송지우편번호', '배송지주소', '옵션명', '주문수량', '공급사명', '배송메시지');
        $worksheet->write($i, 0, ' '.$row['od_b_name']);
        $worksheet->write($i, 1, ' '.$row['od_b_hp']);
        $worksheet->write($i, 2, ' '.$row['od_b_zip1'].$row['od_b_zip2']);
        $worksheet->write($i, 3, print_address($row['od_b_addr1'], $row['od_b_addr2'], $row['od_b_addr3'], $row['od_b_addr_jibeon']));
        $worksheet->write($i, 4, ' '.$row['it_id']);
        if ($row['comp_io_id'] != '') {
            $worksheet->write($i, 5, ' '.$row['comp_io_id']);
        } else {
            $worksheet->write($i, 5, ' '.$row['comp_it_id']);
        }
        $worksheet->write($i, 6, ' '.$row['it_name'].' '.$od_option);
        $worksheet->write($i, 7, ' '.$row['od_qty']);
        $worksheet->write($i, 8, ' '.$row['comp_name']);
        $worksheet->write($i, 9, $row['od_trans_memo']);
    }


}

$workbook->close();

header("Content-Type: application/x-msexcel; name=\"orderlist-".date("ymd", time()).".xls\"");
header("Content-Disposition: inline; filename=\"orderlist-".date("ymd", time()).".xls\"");
$fh = fopen($fname, "rb");
fpassthru($fh);
unlink($fname);

exit;



<?php
$sub_menu = '400801';
include_once('./_common.php');

auth_check($auth[$sub_menu], "r");

if( empty($cp_no) ){
	echo "<script>
			alert('필수값이 없습니다.');
			history.back();
		  </script>
		";
	exit;
}

if( empty($cp_make_cnt) ){
	echo "<script>
			alert('생성갯수값이 없습니다.');
			history.back();
		  </script>
		";
	exit;
}

$sql = "";

/*************** 트랜잭션 관련 ****************/
$error_cnt = 0;
mysqli_autocommit($g5['connect_db'], false);
/*************** 트랜잭션 관련 ****************/

for( $i=1; $i <= $cp_make_cnt; $i++){

	do {
		$cp_id = get_coupon_id();

		$sql3 = "
			SELECT SUM(cnt) AS cnt FROM(
				SELECT COUNT(*) AS cnt from tbl_shop_coupon WHERE cp_id = '{$cp_id}'
				UNION ALL
				SELECT COUNT(*) AS cnt from tbl_shop_coupon_rand_txt WHERE cp_id = '{$cp_id}'
			) AS X
		";

		$row3 = sql_fetch($sql3);

		if(!$row3['cnt']){
			break;
		}

	} while(1);

	$sql = " insert into tbl_shop_coupon_rand_txt(cp_id,parent_cp_no,cp_datetime)values('{$cp_id}',{$cp_no},now()); ";
	sql_query($sql);	

	/*************** 트랜잭션 관련 ****************/
	if( mysqli_errno($g5['connect_db']) ){
		$error_cnt += 1;
	}
	/*************** 트랜잭션 관련 ****************/

    /************* 관리자 로그 처리 START *************/
    insert_admin_log(400,400801, '쿠폰 생성', '', $cp_id, '', $_SERVER['REQUEST_URI'], $_POST);
    /************* 관리자 로그 처리 END *************/

}

/*************** 트랜잭션 관련 ****************/
if ($error_cnt > 0) {
	mysqli_rollback($g5['connect_db']);
	mysqli_close($g5['connect_db']);
	echo "
	<script>
		alert('데이터베이스의 에러로 인해 롤백되었습니다.');
		history.back();
	</script>
	";
	exit;
} else {
	mysqli_commit($g5['connect_db']);
} 
/*************** 트랜잭션 관련 ****************/

echo "<script>
		parent.ClosePop();
		alert('등록완료');
	  </script>
	";

?>
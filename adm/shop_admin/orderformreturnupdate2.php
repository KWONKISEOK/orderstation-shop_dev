<?php
$sub_menu = '400400';
include_once('./_common.php');

$ct_chk_count = count($_POST['trans_chk']);
if(!$ct_chk_count){
    alert('처리할 자료를 하나 이상 선택해 주십시오.');
}

if( $od_return_confirm_check != "Y" ){
    alert('상품회수 동의여부를 체크해주세요.');
	exit;
}

$cnt = count($_POST['od_num']);

/*************** 트랜잭션 관련 ****************/
$error_cnt = 0;
mysqli_autocommit($g5['connect_db'], false);
/*************** 트랜잭션 관련 ****************/

for ($i=0; $i<$cnt; $i++)
{
    $k = $_POST['trans_chk'][$i];
    $od_num = $_POST['od_num'][$k];
	$od_seq = $_POST['od_seq'][$k];
	
	$sql = " update tbl_shop_order_receiver set 
		 od_return_confirm			= 'Y',
		 od_return_delivery_company = '$od_return_delivery_company',
		 od_return_delivery_url		= '$od_return_delivery_url'
	 where od_id = '$od_id' and od_num = '$od_num' and od_seq = '$od_seq'  ";

	sql_query($sql, true);
	/*************** 트랜잭션 관련 ****************/
	if( mysqli_errno($g5['connect_db']) ){
		$error_cnt += 1;
	}
	/*************** 트랜잭션 관련 ****************/

}

/*************** 트랜잭션 관련 ****************/
if ($error_cnt > 0) {
	mysqli_rollback($g5['connect_db']);
	mysqli_close($g5['connect_db']);
	echo "
	<script>
		alert('데이터베이스의 에러로 인해 롤백되었습니다.');
		history.back();
	</script>
	";
	exit;
} else {
	mysqli_commit($g5['connect_db']);
} 	
/*************** 트랜잭션 관련 ****************/

if( !empty($od_status) ){
	$add_param = "od_status=$od_status&amp;";
}

$qstr = $add_param."sort1=$sort1&amp;sort2=$sort2&amp;sel_field=$sel_field&amp;search=$search&amp;page=$page";

$url = "./orderform.php?od_id=$od_id&amp;$qstr";

goto_url($url);

?>

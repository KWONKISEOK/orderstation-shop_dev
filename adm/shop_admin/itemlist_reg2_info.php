<?php
$sub_menu = '600300';
include_once('./_common.php');
include_once(G5_EDITOR_LIB);
include_once(G5_LIB_PATH.'/iteminfo.lib.php');

auth_check($auth[$sub_menu], "w");

$sql = " select * from {$g5['g5_shop_item_table']} where it_id = '$it_id' ";
$it = sql_fetch($sql);

if(!$it){
	alert('상품정보가 존재하지 않습니다.');
}

?>
<!doctype html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta http-equiv="imagetoolbar" content="no">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<title>상품승인처리 | 오더스테이션</title>

<link rel="stylesheet" href="/adm/css/admin.css?date=20200824">
<script src="/js/jquery-1.8.3.min.js"></script>
<script src="/js/common.js?ver=171222"></script>
<script src="/js/wrest.js?ver=171222"></script>
<script src="/js/placeholders.min.js"></script>
<link rel="stylesheet" href="/js/font-awesome/css/font-awesome.min.css">

</head>
<body style="background:#fff;">

<section id="anc_sitfrm_cost">

    <div class="tbl_frm01 tbl_wrap">
        <table>
        <caption>변경사항</caption>
        <colgroup>
            <col class="grid_4">
            <col>
            <col class="grid_3">
        </colgroup>
        <tbody>
        <tr>
            <th scope="row"><label for="it_price2">수정요청 소비자가</label></th>
            <td>
				<?php echo $it['it_price2']; ?>
            </td>
			<th><label for="it_supply_price2">수정요청 공급가</label></th>
			<td>
				<?php echo $it['it_supply_price2']; ?>
			</td>
        </tr>
		</table>
	</div>
</section>

<section id="anc_sitfrm_img">
    

    <div class="tbl_frm01 tbl_wrap">
        <table>
        <caption>이미지 업로드</caption>
        <colgroup>
            <col class="grid_4">
            <col>
        </colgroup>
        <tbody>
        <?php 
		
		$ImgSubject = array(
			"이미지 1","이미지 2","이미지 3","이미지 4","이미지 5",
			"이미지 6","이미지 7","이미지 8","이미지 9","이미지 10"
		);

		for($i=1; $i<=10; $i++) { 
		?>
        <tr>
            <th scope="row"><label for="it_img<?php echo $i; ?>_mod"><?=$ImgSubject[$i-1];?></label></th>
            <td>
                <?php
                $it_img_mod = G5_DATA_PATH.'/item_mod/'.$it['it_img'.$i.'_mod'];

                if(is_file($it_img_mod) && $it['it_img'.$i.'_mod']) {
                    $size = @getimagesize($it_img_mod);
                    $thumb = get_it_thumbnail($it['it_img'.$i.'_mod'], 25, 25);
                ?>
                <span class="sit_wimg_limg<?php echo $i; ?>"><?php echo $thumb; ?></span>
                <div id="limg<?php echo $i; ?>" class="banner_or_img">
                    <img src="<?php echo G5_DATA_URL; ?>/item_mod/<?php echo $it['it_img'.$i.'_mod']; ?>" alt="" style="max-width:200px;">
					(원본사이즈 <?php echo $size[0]; ?> x <?php echo $size[1]; ?>)
                </div>
                <?php } ?>
            </td>
        </tr>
        <?php } ?>
        <tr>
            <th scope="row"><label for="it_img<?php echo $i; ?>_mod">상품설명</label></th>
            <td><?php echo editor_html('it_explan_mod', get_text($it['it_explan_mod'], 0)); ?></td>
		</tr>
        <tr>
            <th scope="row"><label for="it_img<?php echo $i; ?>_mod">모바일 상품설명</label></th>
            <td><?php echo editor_html('it_mobile_explan_mod', get_text($it['it_mobile_explan_mod'], 0)); ?></td>
		</tr>
		<tr>
			<th scope="row">상세페이지 수정요청사항</th>
			<td>
				<textarea name="it_mod_memo" maxlength="1000" style="height:50px;"><?=$it['it_mod_memo']?></textarea>
			</td>
		</tr>
        </tbody>
        </table>
    </div>
</section>

</body>

</html>
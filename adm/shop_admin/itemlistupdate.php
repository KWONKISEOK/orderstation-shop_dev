<?php
$sub_menu = '600300';
include_once('./_common.php');

check_demo();

check_admin_token();

if (!count($_POST['chk'])) {
    alert($_POST['act_button']." 하실 항목을 하나 이상 체크하세요.");
}

/*************** 트랜잭션 관련 ****************/
$error_cnt = 0;
mysqli_autocommit($g5['connect_db'], false);
/*************** 트랜잭션 관련 ****************/

if ($_POST['act_button'] == "선택수정") {

    auth_check($auth[$sub_menu], 'w');

    for ($i=0; $i<count($_POST['chk']); $i++) {

        // 실제 번호를 넘김
        $k = $_POST['chk'][$i];

        $sql = "update {$g5['g5_shop_item_table']}
                   set it_use         = '{$_POST['it_use'][$k]}',
                       it_soldout     = '{$_POST['it_soldout'][$k]}',
					   it_type1       = '{$_POST['it_type1'][$k]}',
					   it_type2       = '{$_POST['it_type2'][$k]}',
					   it_type3       = '{$_POST['it_type3'][$k]}',
					   it_type4       = '{$_POST['it_type4'][$k]}',
					   it_main1       = '{$_POST['it_main1'][$k]}',
					   it_main2       = '{$_POST['it_main2'][$k]}',
					   it_main3       = '{$_POST['it_main3'][$k]}',
					   it_service_view_1 = '{$_POST['it_service_view_1'][$k]}',
					   it_service_view_2 = '{$_POST['it_service_view_2'][$k]}',
					   it_crm_yn      = '{$_POST['it_crm_yn'][$k]}',
                       it_update_time = '".G5_TIME_YMDHIS."'
                 where it_id   = '{$_POST['it_id'][$k]}' ";
        sql_query($sql);
		/*************** 트랜잭션 관련 ****************/
		if( mysqli_errno($g5['connect_db']) ){
			$error_cnt += 1;
		}
		/*************** 트랜잭션 관련 ****************/

        /************* 관리자 로그 처리 START *************/
        $al_data = array(
            'it_use' => $_POST['it_use'][$k],
            'it_soldout' => $_POST['it_soldout'][$k],
            'it_type1' => $_POST['it_type1'][$k],
            'it_type2' => $_POST['it_type2'][$k],
            'it_type3' => $_POST['it_type3'][$k],
            'it_type4' => $_POST['it_type4'][$k],
            'it_main1' => $_POST['it_main1'][$k],
            'it_main2' => $_POST['it_main2'][$k],
            'it_main3' => $_POST['it_main3'][$k],
            'it_service_view_1' => $_POST['it_service_view_1'][$k],
            'it_service_view_2' => $_POST['it_service_view_2'][$k],
            'it_crm_yn' => $_POST['it_crm_yn'][$k],
            'it_update_time' => $_POST['it_update_time'][$k]
        );

        insert_admin_log(600,600300, '선택 수정', '', $_POST['it_id'][$k], '', $_SERVER['REQUEST_URI'], $al_data);
        /************* 관리자 로그 처리 END *************/
    }
} else if ($_POST['act_button'] == "옵션상품으로등록") {

	auth_check($auth[$sub_menu], 'w');

    for ($i=0; $i<count($_POST['chk']); $i++) {

        // 실제 번호를 넘김
        $k = $_POST['chk'][$i];
		$sql = "update {$g5['g5_shop_item_table']}
                   set it_option         = '1'
                 where it_id   = '{$_POST['it_id'][$k]}' ";
        sql_query($sql);		
		/*************** 트랜잭션 관련 ****************/
		if( mysqli_errno($g5['connect_db']) ){
			$error_cnt += 1;
		}
		/*************** 트랜잭션 관련 ****************/
    }
} else if ($_POST['act_button'] == "선택삭제") {

    if ($is_admin != 'super')
        alert('상품 삭제는 최고관리자만 가능합니다.');

    auth_check($auth[$sub_menu], 'd');

    // _ITEM_DELETE_ 상수를 선언해야 itemdelete.inc.php 가 정상 작동함
    define('_ITEM_DELETE_', true);

    for ($i=0; $i<count($_POST['chk']); $i++) {
        // 실제 번호를 넘김
        $k = $_POST['chk'][$i];

        // include 전에 $it_id 값을 반드시 넘겨야 함
        $it_id = $_POST['it_id'][$k];
        include ('./itemdelete.inc.php');
    }
}

/*************** 트랜잭션 관련 ****************/
if ($error_cnt > 0) {
	mysqli_rollback($g5['connect_db']);
	mysqli_close($g5['connect_db']);
	echo "
	<script>
		alert('데이터베이스의 에러로 인해 롤백되었습니다.');
		history.back();
	</script>
	";
	exit;
} else {
	mysqli_commit($g5['connect_db']);
} 	
/*************** 트랜잭션 관련 ****************/

$qstr  = $qstr.'&amp;s_comp_code='.$s_comp_code.'&amp;sca='.$sca.'&amp;page='.$page.'&amp;save_stx='.$stx;
$qstr  = $qstr.'&amp;s_it_main1='.$s_it_main1.'&amp;s_it_main2='.$s_it_main2.'&amp;s_it_main3='.$s_it_main3.'&amp;s_it_crm_yn='.$s_it_crm_yn;
$qstr  = $qstr.'&amp;s_it_type1='.$s_it_type1.'&amp;s_it_type2='.$s_it_type2.'&amp;s_it_type3='.$s_it_type3.'&amp;s_it_type4='.$s_it_type4.'&amp;s_it_time='.$s_it_time;
$qstr  = $qstr.'&amp;sst='.$sst.'&amp;amp;sod='.$sod.'&amp;sfl='.$sfl.'&amp;it_service_view_1='.$it_service_view_1.'&amp;it_service_view_2='.$it_service_view_2;


goto_url("./itemlist.php?$qstr");
?>

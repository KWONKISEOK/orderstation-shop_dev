<?php
$sub_menu = '600670';
include_once('./_common.php');
include_once(G5_LIB_PATH.'/PHPExcel-1.8/Classes/PHPExcel.php');

// 상품이 많을 경우 대비 설정변경
ini_set('memory_limit', '50M');

auth_check($auth[$sub_menu], "w");

$UpFile = $_FILES["excel_file"];

$UpFileName = filefilter($UpFile["name"]);

$UpFilePathInfo = pathinfo($UpFileName);
$UpFileExt = strtolower($UpFilePathInfo["extension"]);

if (empty($f_comp_code)) {
    echo "<script>
			alert('공급사 코드값이 없습니다.');
		  </script>";
    exit;
}
if ($UpFileExt != "xls" && $UpFileExt != "xlsx") {
    echo "<script>
			alert('엑셀파일만 업로드 가능합니다.');
		  </script>";
    exit;
}

//업로드된 엑셀파일을 서버의 지정된 곳에 옮기기 위해 경로 적절히 설정
$upload_path = G5_DATA_PATH."/upload_excel";
$MakeUpFileName = date("Ymd_His")."_".$UpFileName; //utf8 변경 파일명 utf8로 변경하지 않으면 한글파일명일때 안됨
$EucFileName = date("Ymd_His")."_".filefilter($UpFile["name"]); //디비에 들어갈 파일명
$upfile_path = $upload_path."/".$MakeUpFileName;

if (is_uploaded_file($UpFile["tmp_name"])) {

    if (!move_uploaded_file($UpFile["tmp_name"], $upfile_path)) {
        echo "<script>
				alert('업로드된 파일을 옮기는 중 에러가 발생했습니다.');
			  </script>";
    }

    /*************** 트랜잭션 관련 ****************/
    $error_cnt = 0;
    $ErrorMsg = "";
    mysqli_autocommit($g5['connect_db'], false);
    /*************** 트랜잭션 관련 ****************/

    $sql = " insert into item_excel_fileinfo (f_comp_code,f_name,f_gubun,f_memo,f_regid,f_regip)
			 values('{$f_comp_code}','{$EucFileName}','{$f_gubun}','{$f_memo}','{$member['mb_id']}','{$_SERVER['REMOTE_ADDR']}'); ";
    sql_query($sql);
    /*************** 트랜잭션 관련 ****************/
    if (mysqli_errno($g5['connect_db'])) {
        $error_cnt += 1;
    }
    /*************** 트랜잭션 관련 ****************/


    $sql = " select LAST_INSERT_ID() as f_idx ";
    $row = sql_fetch($sql);
    $f_idx = $row['f_idx'];

    /************* 관리자 로그 처리 START *************/
    $al_data = array(
        'f_comp_code' => $f_comp_code,
        'f_name' => $EucFileName,
        'f_memo' => $f_memo
    );
    insert_admin_log(600,600670, 'UPDATE 파일 등록', '', $f_idx, '', $_SERVER['REQUEST_URI'], $al_data);
    /************* 관리자 로그 처리 END *************/

    //상품의 중복 상품이동을 막기위해 마지막에 넣는 idx 를 제외한 것은 다 지워준다.
    $sql = " delete from tbl_shop_item_excel where f_idx < {$f_idx} ";
    sql_query($sql);
    /*************** 트랜잭션 관련 ****************/
    if (mysqli_errno($g5['connect_db'])) {
        $error_cnt += 1;
    }
    /*************** 트랜잭션 관련 ****************/

    try {

        // 업로드한 PHP 파일을 읽어온다.
        $objPHPExcel = PHPExcel_IOFactory::load($upfile_path);
        $sheetsCount = 1;

        // 시트Sheet별로 읽기
        for ($sheet = 0; $sheet < $sheetsCount; $sheet++) {

            $objPHPExcel->setActiveSheetIndex($sheet);
            $activesheet = $objPHPExcel->getActiveSheet();

            $highestRow = $activesheet->getHighestDataRow();          // 마지막 행
            $highestColumn = $activesheet->getHighestColumn();    // 마지막 컬럼

            // 한줄읽기
            for ($row = 1; $row <= $highestRow; $row++) {

                // $rowData가 한줄의 데이터를 셀별로 배열처리 된다.
                $rowData = $activesheet->rangeToArray("A".$row.":".$highestColumn.$row, NULL, TRUE, FALSE);

                // Row가 빈값이 1번째 열로 체크 (상품코드)
                $isRow = false;
                foreach($rowData as $cell) {
                    if (!empty($cell[0])) $isRow = true;
                }

                if ($isRow == true) {
                    // $rowData에 들어가는 값은 계속 초기화 되기때문에 값을 담을 새로운 배열을 선안하고 담는다.
                    $allData[$row] = $rowData[0];
                }

            }
        }

    } catch (exception $exception) {
        $error_cnt += 1;
    }


    $GetCnt = count($allData);

    /**
     * ERP 연동 관련 필드
     * it_price 일반판매가 (소비자가)
     * it_drug_price 약국판매가 (약국공급가)
     * it_drug_ori_price 약국공급가(회색) : 신규필드
     * it_incen 수수료 (약국마진)
     * it_dodome_price 도매가(도도매가)
     * it_supply_price 매입가
     */

    //파일데이터 넣어주기
    if ($GetCnt > 0) {

        $sql = "";

        //1행타이틀 제외
        for ($k = 2; $k <= $GetCnt; $k++) {

            /****** 상품유형 $allData[$k][4] ******/
            $it_type1 = 0;
            $it_type2 = 0;
            $it_type3 = 0;
            $it_type4 = 0;
            if(strpos($allData[$k][6], "1") !== false) {
                $it_type1 = 1;
            }
            if(strpos($allData[$k][6], "2") !== false) {
                $it_type2 = 1;
            }
            if(strpos($allData[$k][6], "3") !== false) {
                $it_type3 = 1;
            }
            if(strpos($allData[$k][6], "4") !== false) {
                $it_type4 = 1;
            }
            /************************************/

            $sql = " insert into tbl_shop_item_excel( ";
            $sql = $sql." f_idx , ";
            $sql = $sql." comp_code , ";
            $sql = $sql." it_id , ";
            $sql = $sql." ca_id , ";
            $sql = $sql." ca_id2 , ";
            $sql = $sql." ca_id3 , ";
            $sql = $sql." it_name , ";
            /******* 상품유형 *******/
            $sql = $sql." it_type1 , ";
            $sql = $sql." it_type2 , ";
            $sql = $sql." it_type3 , ";
            $sql = $sql." it_type4 , ";
            /***********************/
            $sql = $sql." it_model , ";
            $sql = $sql." it_multisend , ";
            $sql = $sql." it_point_type , ";
            $sql = $sql." it_brand , ";
            $sql = $sql." it_notax , ";
            $sql = $sql." it_cust_price, "; // 시중가격
            $sql = $sql." it_price , "; // 소비자가
            $sql = $sql." it_supply_price , "; // 매입가
            $sql = $sql." it_dodome_price , "; // 도도매가
            $sql = $sql." it_drug_price , "; // 약국공급가
            $sql = $sql." it_incen , "; // 약국마진
            $sql = $sql." it_drug_ori_price , "; // 약국공급가(회색)
            $sql = $sql." it_app_norm_price , "; // 앱소비자가
            $sql = $sql." it_app_drug_price , "; // 앱판매가(약국)
            $sql = $sql." it_app_sale_price , "; // 앱판매가
            $sql = $sql." it_app_incen , "; // 앱수수료
            $sql = $sql." it_search , ";
            $sql = $sql." it_stock_qty , ";
            $sql = $sql." it_buy_min_qty , ";
            $sql = $sql." it_buy_max_qty , ";
            $sql = $sql." it_use , ";
            $sql = $sql." it_soldout , ";
            $sql = $sql." it_trans_bundle , ";
            $sql = $sql." it_order_close_time , ";
            $sql = $sql." it_release_day , ";
            $sql = $sql." it_period_yn , ";
            $sql = $sql." it_sc_type , ";
            $sql = $sql." it_sc_method , ";
            $sql = $sql." it_sc_price , ";
            $sql = $sql." it_sc_minimum , ";
            $sql = $sql." it_crm_yn, ";
            /*기본값 처리 (예외로 넣어줘야 하는 필드)*/
            $sql = $sql." it_health_info , ";
            $sql = $sql." it_basic  , ";
            $sql = $sql." it_head_html  , ";
            $sql = $sql." it_tail_html  , ";
            $sql = $sql." it_mobile_head_html  , ";
            $sql = $sql." it_mobile_tail_html  ,";
            $sql = $sql." it_shop_memo  , ";
            $sql = $sql." it_sellingpoint , ";
            $sql = $sql." it_use_avg  , ";
            $sql = $sql." it_status , ";
            $sql = $sql." comp_it_id , it_info_value )values( ";

            $sql = $sql." {$f_idx}, ";
            $sql = $sql." '{$f_comp_code}', ";

            foreach($allData[$k] as $key => $value) {
                if ($key == 1 || $key == 7) // 특정 항목 값 제외
                    continue;

                if ($key == 6) { // 상품유형 처리
                    $sql = $sql." {$it_type1}, ";
                    $sql = $sql." {$it_type2}, ";
                    $sql = $sql." {$it_type3}, ";
                    $sql = $sql." {$it_type4}, ";
                } else {
                    $sql = $sql." '{$value}', ";
                }

            }

            /*기본값 처리 (예외로 넣어줘야 하는 필드)*/
            $sql = $sql." '', ";
            $sql = $sql." '', ";
            $sql = $sql." '', ";
            $sql = $sql." '', ";
            $sql = $sql." '', ";
            $sql = $sql." '', ";
            $sql = $sql." '', ";
            $sql = $sql." '', ";
            $sql = $sql." 0.0, ";
            $sql = $sql." '3', ";
            $sql = $sql." '', '' ); ";
            sql_query($sql);
            /*************** 트랜잭션 관련 ****************/
            if (mysqli_errno($g5['connect_db'])) {
                $error_cnt += 1;
                $ErrorMsg .= " {$k}행 ";
            }
            /*************** 트랜잭션 관련 ****************/

        }

    }

    /*************** 트랜잭션 관련 ****************/
    if ($error_cnt > 0) {
        mysqli_rollback($g5['connect_db']);
        mysqli_close($g5['connect_db']);
        echo "
		<script>
			alert('데이터베이스의 에러로 인해 롤백되었습니다.');
			parent.document.getElementById('ErrorView').innerHTML='[".$ErrorMsg."]';
		</script>
		";
        exit;
    } else {
        mysqli_commit($g5['connect_db']);
    }
    /*************** 트랜잭션 관련 ****************/

    echo "<script>
			alert('엑셀 임시저장이 완료되었습니다.');
			parent.location.reload();
		  </script>";
    exit;

} else {

    echo "<script>
			alert('파일등록에 실패하였습니다.');
			parent.location.reload();
		  </script>";
    exit;
}

?>
<?php
include_once('./_common.php');
include_once(G5_LIB_PATH.'/iteminfo.lib.php');

$mode = $_POST['mode'];

$where = " and ";
$sql_search = "";


if ($stx != "") {
    if ($sfl != "it_id") {
        $sql_search .= " $where $sfl like '%$stx%' ";
        $where = " and ";

    } else if ($sfl == "it_id") {

        if ($sfl != "") {
            $sfl = "a.".$sfl;
            $sql_search .= " $where instr('$stx',$sfl)";
            $where = " and ";
        }
    }
    if ($save_stx != $stx)
        $page = 1;
}

if ($sca != "") {
    $sql_search .= " $where (a.ca_id like '$sca%' or a.ca_id2 like '$sca%' or a.ca_id3 like '$sca%') ";
}

if ($sfl == "") $sfl = "it_name";

$sql_common = " from {$g5['g5_shop_item_table']} a 
                     left join {$g5['g5_shop_category_table']} b on b.ca_id = a.ca_id
					 left join tbl_member c on c.comp_code = a.comp_code
					 left join erp_code d on a.it_id = d.onk_code
					 left join tbl_shop_item_option e ON a.it_id = e.it_id
               where c.mb_type = 7";

if ($s_comp_code != '') {
    $comp_code = $s_comp_code;
}
if ($comp_code != '')
    $sql_common .= " and a.comp_code = '$comp_code' ";

if ($it_use != '')
    $sql_common .= " and a.it_use ='$it_use' ";

$sql_common .= " and a.it_status > 2 ";

if ($it_soldout != '')
    $sql_common .= " and a.it_soldout ='$it_soldout' ";

if ($s_it_main1 != "") {
    $sql_common .= " and a.it_main1 = '1' ";
}
if ($s_it_main2 != "") {
    $sql_common .= " and a.it_main2 = '1' ";
}
if ($s_it_main3 != "") {
    $sql_common .= " and a.it_main3 = '1' ";
}
if ($s_it_type1 != "") {
    $sql_common .= " and a.it_type1 = '1' ";
}
if ($s_it_type2 != "") {
    $sql_common .= " and a.it_type2 = '1' ";
}
if ($s_it_type3 != "") {
    $sql_common .= " and a.it_type3 = '1' ";
}
if ($s_it_time != "") {
    $sql_common .= " and DATE(a.it_time) >= '".$s_it_time."' ";
}
if ($s_it_crm_yn != "") {
    $sql_common .= " and a.it_crm_yn = '1' ";
}
$sql_common .= $sql_search;


if (!$sst) {
    $sst = "it_time";
    $sod = "desc";
}
$sql_order = "order by a.it_id asc ";

switch ($mode) {

    case 'excel':
        $ExcelTitle = urlencode("상품리스트".date("YmdHis"));

        header("Content-type: application/vnd.ms-excel");
        header("Content-type: application/vnd.ms-excel; charset=utf-8");
        header("Content-Disposition: attachment; filename = ".$ExcelTitle.".xls");
        header("Content-Description: PHP4 Generated Data");
        header('Set-Cookie: fileDownload=true; path=/');
        print("<meta http-equiv=\"Content-Type\" content=\"application/vnd.ms-excel; charset=utf-8\">");
        ?>
        <style>
            br{mso-data-placement:same-cell;}
            span{color: darkred;}
        </style>
        <table border="1">
            <tr height="120">
                <th bgcolor="#f08080">상품코드</th>
                <th bgcolor="#f08080">태전ERP코드</th>
                <th width="130">기본분류<br>
                    <span>코드값으로 입력</span>
                </th>
                <th width="130">2차 분류<br>
                    <span>코드값으로 입력</span>
                </th>
                <th width="130">3차 분류<br>
                    <span>코드값으로 입력</span>
                </th>
                <th>상품명</th>
                <th width="280">상품유형<br>
                    <span>
                        1:베스트,2:MD추천,3:신상품,4:타임세일<br>
                        여러개사용시 , 로 묶음 ex)1,2,3,4
                    </span>
                </th>
                <th bgcolor="#f08080">공급업체</th>
                <th>규격</th>
                <th>제조사/제조국</th>
                <th>원산지</th>
                <th>브랜드</th>
                <th width="80">과세<br>
                    <span>
                        0: 과세<br>
                        1: 비과세<br>
                        2:면세
                    </span>
                </th>

                <th>시중가격</th>
                <th>소비자가</th>
                <th>매입가</th>
                <th>도도매가</th>
                <th>약국공급가</th>
                <th>약국마진</th>
                <th>약국공급가(회색)</th>
                <th>앱소비자가</th>
                <th>앱판매가(약국)</th>
                <th>앱판매가</th>
                <th>앱수수료</th>

                <th>검색어</th>
                <th width="110">재고량<br>
                    <span>
                        0~99999
                    </span>
                </th>
                <th>최소주문수량</th>
                <th>최대주문수량</th>
                <th width="90">판매여부<br>
                    <span>
                        1:판매<br>
                        0:사용안함
                    </span>
                </th>
                <th width="70">품절여부<br>
                    <span>
                        1:품절<br>
                        0:판매
                    </span>
                </th>
                <th width="110">묶음·개별 배송<br>
                    <span>
                        1:묶음배송<br>
                        0:개별배송
                    </span>
                </th>
                <th>마감시간</th>
                <th>출고일</th>
                <th width="150">정기주문가능여부<br>
                    <span>
                        Y:가능
                        N:불가능
                    </span>
                </th>
                <th width="150">배송비 유형<br>
                    <span>
                        0:쇼핑몰 기본설정<br>
                        1:무료배송<br>
                        2:조건부 무료배송<br>
                        3:유료배송<br>
                        4:수량별부과
                    </span>
                <th width="110">배송비 결제<br>
                    <span>
                        0:선불<br>
                        1:착불<br>
                        2:사용자선택
                    </span>
                </th>
                <th>기본배송비</th>
                <th width="160">배송비 상세조건<br>
                    <span>
                        조건부 무료배송 시<br>
                        배송비 최소금액 입력
                    </span>
                </th>
                <th>CRM 재고관리 대상제품<br>
                    <span>
                        1:Y<br>
                        0:N
                    </span>
                </th>
            </tr>
            <?php
            $sql = " 
			SELECT X.* 
				,CASE @vjob WHEN X.it_id THEN @rownum:=@rownum+1 ELSE @rownum:=1 END as ranking
				,@vjob:=X.it_id as vjob
				,IFNULL( case when @rownum = 1 then Y.io_cnt ELSE 0 END , -1 ) AS row_span_cnt
			FROM(
						select a.* , e.io_id , e.io_name , c.comp_name
							,(select ca_id from tbl_shop_category where length(ca_id) = '2' and ca_id = left(a.ca_id, 2)) ca_name1
							,(select ca_id from tbl_shop_category where length(ca_id) = '4' and  ca_id = left(a.ca_id, 4)) ca_name2
							,(select ca_id from tbl_shop_category where ca_id = a.ca_id ) ca_name3
							,(select io_use from tbl_shop_item_option where io_id = a.it_id limit 1) io_use
							, case when d.erp_code > '0' then d.erp_code else a.it_id end as erp_code
			   $sql_common
			   $sql_order 
			) AS X LEFT JOIN(
				SELECT COUNT(it_id) AS io_cnt , it_id FROM tbl_shop_item_option GROUP BY it_id
			) AS Y ON X.it_id = Y.it_id	, ( SELECT @vjob:='', @rownum:=0 ) as Z  ";

            $res = sql_query($sql);
            if (!$res) exit('Cannot run query.');

            $i = 0;
            while ($row = sql_fetch_array($res)) {
                $it_type = '';
                if($row['it_type1'] == 1) {
                    $it_type .= '1';
                }
                if($row['it_type2'] == 1) {
                    $it_type .= ',2';
                }
                if($row['it_type3'] == 1) {
                    $it_type .= ',3';
                }
                if($row['it_type4'] == 1) {
                    $it_type .= ',4';
                }
                $tot_it_type = $it_type;
                ?>
                <tr bgcolor="">
                    <? if ($row["row_span_cnt"] > 0) { ?> <!-- 0보다 크면 -->
                        <td style="mso-number-format:'\@';" rowspan="<?= $row["row_span_cnt"] ?>"><?php echo $row["it_id"]; ?></td>
                        <td style="mso-number-format:'\@';" rowspan="<?= $row["row_span_cnt"] ?>"> <?php echo $row["erp_code"]; ?></td>
                        <td rowspan="<?= $row["row_span_cnt"] ?>"><?php echo $row["ca_id"]; ?></td>
                        <td rowspan="<?= $row["row_span_cnt"] ?>"><?php echo $row["ca_id2"]; ?></td>
                        <td rowspan="<?= $row["row_span_cnt"] ?>"><?php echo $row["ca_id3"]; ?></td>

                        <td rowspan="<?= $row["row_span_cnt"] ?>"><?php echo $row["it_name"]; ?></td>
                        <td rowspan="<?= $row["row_span_cnt"] ?>"><?php echo $tot_it_type; ?></td>
                        <td rowspan="<?= $row["row_span_cnt"] ?>"><?php echo $row["comp_name"]; ?></td>
                        <td rowspan="<?= $row["row_span_cnt"] ?>"><?php echo $row["it_model"]; ?></td><!--규격-->
                        <td rowspan="<?= $row["row_span_cnt"] ?>"><?php echo $row["it_maker"]; ?></td><!--제조사/제조국-->
                        <td rowspan="<?= $row["row_span_cnt"] ?>"><?php echo $row["it_origin"]; ?></td><!--원산지-->
                        <td rowspan="<?= $row["row_span_cnt"] ?>"><?php echo $row["it_brand"]; ?></td><!--브랜드-->
                        <td rowspan="<?= $row["row_span_cnt"] ?>"><?php echo $row["it_notax"]; ?></td><!--과세-->

                        <td rowspan="<?= $row["row_span_cnt"] ?>"><?php echo $row["it_cust_price"]; ?></td>
                        <td rowspan="<?= $row["row_span_cnt"] ?>"><?php echo $row["it_price"]; ?></td>
                        <td rowspan="<?= $row["row_span_cnt"] ?>"><?php echo $row["it_supply_price"]; ?></td>
                        <td rowspan="<?= $row["row_span_cnt"] ?>"><?php echo $row["it_dodome_price"]; ?></td>
                        <td rowspan="<?= $row["row_span_cnt"] ?>"><?php echo $row["it_drug_price"]; ?></td>
                        <td rowspan="<?= $row["row_span_cnt"] ?>"><?php echo $row["it_incen"]; ?></td>
                        <td rowspan="<?= $row["row_span_cnt"] ?>"><?php echo $row["it_drug_ori_price"]; ?></td>
                        <td rowspan="<?= $row["row_span_cnt"] ?>"><?php echo $row["it_app_norm_price"]; ?></td>
                        <td rowspan="<?= $row["row_span_cnt"] ?>"><?php echo $row["it_app_drug_price"]; ?></td>
                        <td rowspan="<?= $row["row_span_cnt"] ?>"><?php echo $row["it_app_sale_price"]; ?></td>
                        <td rowspan="<?= $row["row_span_cnt"] ?>"><?php echo $row["it_app_incen"]; ?></td>

                        <td rowspan="<?= $row["row_span_cnt"] ?>"><?php echo $row["it_search"]; ?></td><!--검색어-->
                        <td rowspan="<?= $row["row_span_cnt"] ?>"><?php echo $row["it_stock_qty"]; ?></td><!--재고량-->
                        <td rowspan="<?= $row["row_span_cnt"] ?>"><?php echo $row["it_buy_min_qty"]; ?></td>최소주문수량
                        <td rowspan="<?= $row["row_span_cnt"] ?>"><?php echo $row["it_buy_max_qty"]; ?></td>최대주문수량
                        <td rowspan="<?= $row["row_span_cnt"] ?>"><?php echo $row["it_use"]; ?></td>판매여부
                        <td rowspan="<?= $row["row_span_cnt"] ?>"><?php echo $row["it_soldout"]; ?></td>품절여부
                        <td rowspan="<?= $row["row_span_cnt"] ?>"><?php echo $row["it_trans_bundle"]; ?></td>묶음,개별 배송
                        <td rowspan="<?= $row["row_span_cnt"] ?>"><?php echo $row["it_order_close_time"]; ?></td>마감시간
                        <td rowspan="<?= $row["row_span_cnt"] ?>"><?php echo $row["it_release_day"]; ?></td>출고일
                        <td rowspan="<?= $row["row_span_cnt"] ?>"><?php echo $row["it_period_yn"]; ?></td>
                        <td rowspan="<?= $row["row_span_cnt"] ?>"><?php echo $row["it_sc_type"]; ?></td>
                        <td rowspan="<?= $row["row_span_cnt"] ?>"><?php echo $row["it_sc_method"]; ?></td>
                        <td rowspan="<?= $row["row_span_cnt"] ?>"><?php echo $row["it_sc_price"] ?></td>
                        <td rowspan="<?= $row["row_span_cnt"] ?>"><?php echo $row["it_sc_minimum"]; ?></td>
                        <td rowspan="<?= $row["row_span_cnt"] ?>"><?php echo $row["it_crm_yn"]; ?></td>
                    <? } ?>

                    <? if ($row["row_span_cnt"] < 0) { ?> <!-- 0보다 작으면 -->
                        <td style="mso-number-format:'\@';"><?php echo $row["it_id"]; ?></td>
                        <td style="mso-number-format:'\@';"><?php echo $row["erp_code"]; ?></td>
                        <td><?php echo $row["ca_id"]; ?></td>
                        <td><?php echo $row["ca_id2"]; ?></td>
                        <td><?php echo $row["ca_id3"]; ?></td>

                        <td><?php echo $row["it_name"]; ?></td>
                        <td><?php echo $tot_it_type; ?></td>
                        <td><?php echo $row["comp_name"]; ?></td>
                        <td><?php echo $row["it_model"]; ?></td>
                        <td><?php echo $row["it_maker"]; ?></td>
                        <td><?php echo $row["it_origin"]; ?></td>
                        <td><?php echo $row["it_brand"]; ?></td>
                        <td><?php echo $row["it_notax"]; ?></td>

                        <td><?php echo $row["it_cust_price"]; ?></td>
                        <td><?php echo $row["it_price"]; ?></td>
                        <td><?php echo $row["it_supply_price"]; ?></td>
                        <td><?php echo $row["it_dodome_price"]; ?></td>
                        <td><?php echo $row["it_drug_price"]; ?></td>
                        <td><?php echo $row["it_incen"]; ?></td>
                        <td><?php echo $row["it_drug_ori_price"]; ?></td>
                        <td><?php echo $row["it_app_norm_price"]; ?></td>
                        <td><?php echo $row["it_app_drug_price"]; ?></td>
                        <td><?php echo $row["it_app_sale_price"]; ?></td>
                        <td><?php echo $row["it_app_incen"]; ?></td>

                        <td><?php echo $row["it_search"]; ?></td>
                        <td><?php echo $row["it_stock_qty"]; ?></td>
                        <td><?php echo $row["it_buy_min_qty"]; ?></td>
                        <td><?php echo $row["it_buy_max_qty"]; ?></td>
                        <td><?php echo $row["it_use"]; ?></td>
                        <td><?php echo $row["it_soldout"]; ?></td>
                        <td><?php echo $row["it_trans_bundle"]; ?></td>
                        <td><?php echo $row["it_order_close_time"]; ?></td>
                        <td><?php echo $row["it_release_day"]; ?></td>
                        <td><?php echo $row["it_period_yn"]; ?></td>
                        <td><?php echo $row["it_sc_type"]; ?></td>
                        <td><?php echo $row["it_sc_method"]; ?></td>
                        <td><?php echo $row["it_sc_price"] ?></td>
                        <td><?php echo $row["it_sc_minimum"]; ?></td>
                        <td><?php echo $row["it_crm_yn"]; ?></td>

                    <? } ?>
                </tr>
                <?php
                $i++;
            }
            ?>

        </table>
        <?php
        break;

}


?>
<?php
$sub_menu = "200150";
include_once('./_common.php');

auth_check($auth[$sub_menu], 'w');

if ($w == '') {
    $required_mb_id = 'required';
    $required_mb_id_class = 'required alnum_';
    $required_mb_password = 'required';
    $sound_only = '<strong class="sound_only">필수</strong>';

    $mb['mb_mailling'] = 1;
    $mb['mb_open'] = 1;
    $mb['mb_level'] = $config['cf_register_level'];
    $html_title = '추가';

    $mb['mb_type'] = 1;

    $comp_trans_compare = 0;
    $comp_trans_price = 0;
    $comp_trans_use = 0;

} else if ($w == 'u') {
    $mb = get_member($mb_id);
    if (!$mb['mb_id'])
        alert('존재하지 않는 회원자료입니다.');

    if ($is_admin != 'super' && $mb['mb_level'] >= $member['mb_level'])
        alert('자신보다 권한이 높거나 같은 회원은 수정할 수 없습니다.');

    $required_mb_id = 'readonly';
    $required_mb_password = '';
    $html_title = '수정';

    $mb['mb_name'] = get_text($mb['mb_name']);
    $mb['mb_nick'] = get_text($mb['mb_nick']);
    $mb['mb_email'] = get_text($mb['mb_email']);
    $mb['mb_homepage'] = get_text($mb['mb_homepage']);
    $mb['mb_birth'] = get_text($mb['mb_birth']);
    $mb['mb_tel'] = get_text($mb['mb_tel']);
    $mb['mb_hp'] = get_text($mb['mb_hp']);
    $mb['mb_addr1'] = get_text($mb['mb_addr1']);
    $mb['mb_addr2'] = get_text($mb['mb_addr2']);
    $mb['mb_addr3'] = get_text($mb['mb_addr3']);
    $mb['mb_signature'] = get_text($mb['mb_signature']);
    $mb['mb_recommend'] = get_text($mb['mb_recommend']);
    $mb['mb_profile'] = get_text($mb['mb_profile']);
    /*$mb['mb_1'] = get_text($mb['mb_1']);
    $mb['mb_2'] = get_text($mb['mb_2']);
    $mb['mb_3'] = get_text($mb['mb_3']);
    $mb['mb_4'] = get_text($mb['mb_4']);
    $mb['mb_5'] = get_text($mb['mb_5']);
    $mb['mb_6'] = get_text($mb['mb_6']);
    $mb['mb_7'] = get_text($mb['mb_7']);
    $mb['mb_8'] = get_text($mb['mb_8']);
    $mb['mb_9'] = get_text($mb['mb_9']);
    $mb['mb_10'] = get_text($mb['mb_10']);*/

    $comp_trans_compare = get_text($mb['comp_trans_compare']);
    $comp_trans_price = get_text($mb['comp_trans_price']);
    $comp_trans_use = get_text($mb['comp_trans_use']);

} else
    alert('제대로 된 값이 넘어오지 않았습니다.');

// 본인확인방법
switch ($mb['mb_certify']) {
    case 'hp':
        $mb_certify_case = '휴대폰';
        $mb_certify_val = 'hp';
        break;
    case 'ipin':
        $mb_certify_case = '아이핀';
        $mb_certify_val = 'ipin';
        break;
    case 'admin':
        $mb_certify_case = '관리자 수정';
        $mb_certify_val = 'admin';
        break;
    default:
        $mb_certify_case = '';
        $mb_certify_val = 'admin';
        break;
}

// 본인확인
$mb_certify_yes = $mb['mb_certify'] ? 'checked="checked"' : '';
$mb_certify_no = !$mb['mb_certify'] ? 'checked="checked"' : '';

// 성인인증
$mb_adult_yes = $mb['mb_adult'] ? 'checked="checked"' : '';
$mb_adult_no = !$mb['mb_adult'] ? 'checked="checked"' : '';

//메일수신
$mb_mailling_yes = $mb['mb_mailling'] ? 'checked="checked"' : '';
$mb_mailling_no = !$mb['mb_mailling'] ? 'checked="checked"' : '';

// SMS 수신
$mb_sms_yes = $mb['mb_sms'] ? 'checked="checked"' : '';
$mb_sms_no = !$mb['mb_sms'] ? 'checked="checked"' : '';

// 정보 공개
$mb_open_yes = $mb['mb_open'] ? 'checked="checked"' : '';
$mb_open_no = !$mb['mb_open'] ? 'checked="checked"' : '';


if ($mb['mb_intercept_date']) $g5['title'] = "차단된 ";
else $g5['title'] .= "";
$g5['title'] .= '관리자 '.$html_title;
include_once('./admin.head.php');

// add_javascript('js 구문', 출력순서); 숫자가 작을 수록 먼저 출력됨
add_javascript(G5_POSTCODE_JS, 0);    //다음 주소 js
?>
<script type="text/javascript"
        src="https://openapi.map.naver.com/openapi/v3/maps.js?clientId=iQB4Bbn7JFBS2Qb0UVSY&submodules=geocoder"></script>
<form name="fmember" id="fmember" action="./member_form_update.php" onsubmit="return fmember_submit(this);"
      method="post" enctype="multipart/form-data">
    <input type="hidden" name="w" value="<?php echo $w ?>">
    <input type="hidden" name="sfl" value="<?php echo $sfl ?>">
    <input type="hidden" name="stx" value="<?php echo $stx ?>">
    <input type="hidden" name="sst" value="<?php echo $sst ?>">
    <input type="hidden" name="sod" value="<?php echo $sod ?>">
    <input type="hidden" name="page" value="<?php echo $page ?>">
    <input type="hidden" name="token" value="">

    <!-- admin hidden value -->
    <input type="hidden" name="mb_level" value="2">
    <input type="hidden" name="admin_form" value="Y">

    <div class="tbl_frm01 tbl_wrap">
        <table>
            <caption><?php echo $g5['title']; ?></caption>
            <colgroup>
                <col class="grid_4">
                <col>
                <col class="grid_4">
                <col>
            </colgroup>
            <tbody>
            <tr>
                <th scope="row"><label for="mb_id">아이디<?php echo $sound_only ?></label></th>
                <td>
                    <input type="text" name="mb_id" value="<?php echo $mb['mb_id'] ?>"
                           id="mb_id" <?php echo $required_mb_id ?>
                           class="frm_input <?php echo $required_mb_id_class ?>" size="15" maxlength="20">
                    <?php if ($w == 'u') { ?>
                    <a href="./boardgroupmember_form.php?mb_id=<?php echo $mb['mb_id'] ?>"
                       class="btn_frmline">접근가능그룹보기</a><?php } ?>
                </td>
                <th scope="row"><label for="mb_password">비밀번호<?php echo $sound_only ?></label></th>
                <td><input type="password" name="mb_password" id="mb_password" <?php echo $required_mb_password ?>
                           class="frm_input <?php echo $required_mb_password ?>" size="15" maxlength="20"></td>
            </tr>
            <tr>
                <th scope="row"><label for="mb_name">이름(실명)<strong class="sound_only">필수</strong></label></th>
                <td><input type="text" name="mb_name" value="<?php echo $mb['mb_name'] ?>" id="mb_name" required
                           class="required frm_input" size="15" maxlength="20"></td>
                <th scope="row"><label for="mb_nick">닉네임<strong class="sound_only">필수</strong></label></th>
                <td><input type="text" name="mb_nick" value="<?php echo $mb['mb_nick'] ?>" id="mb_nick" required
                           class="required frm_input" size="15" maxlength="20"></td>
            </tr>
            <tr>
                <th scope="row"><label for="mb_email">E-mail</label></th>
                <td><input type="text" name="mb_email" value="<?php echo $mb['mb_email'] ?>" id="mb_email" required
                           maxlength="100" class="required frm_input email" size="30"></td>
                <th scope="row"><label for="mb_homepage">홈페이지</label></th>
                <td><input type="text" name="mb_homepage" value="<?php echo $mb['mb_homepage'] ?>" id="mb_homepage"
                           class="frm_input" maxlength="255" size="15"></td>
            </tr>
            <tr>
                <th scope="row"><label for="mb_hp">휴대폰번호</label></th>
                <td><input type="text" name="mb_hp" value="<?php echo $mb['mb_hp'] ?>" id="mb_hp" class="frm_input"
                           size="15" maxlength="20"></td>
                <th scope="row"><label for="mb_tel">전화번호</label></th>
                <td><input type="text" name="mb_tel" value="<?php echo $mb['mb_tel'] ?>" id="mb_tel" class="frm_input"
                           size="15" maxlength="20"></td>
            </tr>
            <tr>
                <th scope="row">생년월일</th>
                <td>
                    <input type="text" name="mb_birth" value="<?php echo $mb['mb_birth'] ?>" id="mb_birth"
                           class="frm_input" maxlength="10">
                </td>
                <th scope="row">본인확인방법</th>
                <td>
                    <input type="radio" name="mb_certify_case" value="ipin"
                           id="mb_certify_ipin" <?php if ($mb['mb_certify'] == 'ipin') echo 'checked="checked"'; ?>>
                    <label for="mb_certify_ipin">아이핀</label>
                    <input type="radio" name="mb_certify_case" value="hp"
                           id="mb_certify_hp" <?php if ($mb['mb_certify'] == 'hp') echo 'checked="checked"'; ?>>
                    <label for="mb_certify_hp">휴대폰</label>
                </td>
            </tr>
            <tr>
                <th scope="row">본인확인</th>
                <td>
                    <input type="radio" name="mb_certify" value="1" id="mb_certify_yes" <?php echo $mb_certify_yes; ?>>
                    <label for="mb_certify_yes">예</label>
                    <input type="radio" name="mb_certify" value="" id="mb_certify_no" <?php echo $mb_certify_no; ?>>
                    <label for="mb_certify_no">아니오</label>
                </td>
                <th scope="row">성인인증</th>
                <td>
                    <input type="radio" name="mb_adult" value="1" id="mb_adult_yes" <?php echo $mb_adult_yes; ?>>
                    <label for="mb_adult_yes">예</label>
                    <input type="radio" name="mb_adult" value="0" id="mb_adult_no" <?php echo $mb_adult_no; ?>>
                    <label for="mb_adult_no">아니오</label>
                </td>
            </tr>
            <tr>
                <th scope="row"><label for="mb_icon">회원아이콘</label></th>
                <td colspan="2">

                    <input type="file" name="mb_icon" id="mb_icon">
                    <?php
                    $mb_dir = substr($mb['mb_id'], 0, 2);
                    $icon_file = G5_DATA_PATH.'/member/'.$mb_dir.'/'.$mb['mb_id'].'.gif';
                    if (file_exists($icon_file)) {
                        $icon_url = G5_DATA_URL.'/member/'.$mb_dir.'/'.$mb['mb_id'].'.gif';
                        echo '<img src="'.$icon_url.'" alt="">';
                        echo '<input type="checkbox" id="del_mb_icon" name="del_mb_icon" value="1">삭제';
                    }
                    ?>
                </td>
                <td><?php echo help('*이미지 크기는 <strong>넓이 '.$config['cf_member_icon_width'].'픽셀 높이 '.$config['cf_member_icon_height'].'픽셀</strong>로 해주세요.') ?></td>
            </tr>
            <tr>
                <th scope="row"><label for="mb_img">회원이미지</label></th>
                <td colspan="2">

                    <input type="file" name="mb_img" id="mb_img">
                    <?php
                    $mb_dir = substr($mb['mb_id'], 0, 2);
                    $icon_file = G5_DATA_PATH.'/member_image/'.$mb_dir.'/'.$mb['mb_id'].'.gif';
                    if (file_exists($icon_file)) {
                        $icon_url = G5_DATA_URL.'/member_image/'.$mb_dir.'/'.$mb['mb_id'].'.gif';
                        echo '<img src="'.$icon_url.'" alt="">';
                        echo '<input type="checkbox" id="del_mb_img" name="del_mb_img" value="1">삭제';
                    }
                    ?>
                </td>
                <td><?php echo help('*이미지 크기는 <strong>넓이 '.$config['cf_member_img_width'].'픽셀 높이 '.$config['cf_member_img_height'].'픽셀</strong>로 해주세요.') ?></td>
            </tr>
            <tr>
                <th scope="row">메일 수신</th>
                <td>
                    <input type="radio" name="mb_mailling" value="1"
                           id="mb_mailling_yes" <?php echo $mb_mailling_yes; ?>>
                    <label for="mb_mailling_yes">예</label>
                    <input type="radio" name="mb_mailling" value="0" id="mb_mailling_no" <?php echo $mb_mailling_no; ?>>
                    <label for="mb_mailling_no">아니오</label>
                </td>
                <th scope="row"><label for="mb_sms_yes">SMS 수신</label></th>
                <td>
                    <input type="radio" name="mb_sms" value="1" id="mb_sms_yes" <?php echo $mb_sms_yes; ?>>
                    <label for="mb_sms_yes">예</label>
                    <input type="radio" name="mb_sms" value="0" id="mb_sms_no" <?php echo $mb_sms_no; ?>>
                    <label for="mb_sms_no">아니오</label>
                </td>
            </tr>
            <tr>
                <th scope="row">정보 공개</th>
                <td colspan="3">
                    <input type="radio" name="mb_open" value="1" id="mb_open_yes" <?php echo $mb_open_yes; ?>>
                    <label for="mb_open_yes">예</label>
                    <input type="radio" name="mb_open" value="0" id="mb_open_no" <?php echo $mb_open_no; ?>>
                    <label for="mb_open_no">아니오</label>
                </td>
            </tr>
            <!-- <tr>
        <th scope="row"><label for="mb_signature">서명</label></th>
        <td colspan="3"><textarea  name="mb_signature" id="mb_signature"><?php echo $mb['mb_signature'] ?></textarea></td>
    </tr>
    <tr>
        <th scope="row"><label for="mb_profile">자기 소개</label></th>
        <td colspan="3"><textarea name="mb_profile" id="mb_profile"><?php echo $mb['mb_profile'] ?></textarea></td>
    </tr> -->
            <tr>
                <th scope="row"><label for="mb_memo">메모</label></th>
                <td colspan="3"><textarea name="mb_memo" id="mb_memo"><?php echo $mb['mb_memo'] ?></textarea></td>
            </tr>

            <?php if ($w == 'u') { ?>
                <tr>
                    <th scope="row">회원가입일</th>
                    <td><?php echo $mb['mb_datetime'] ?></td>
                    <th scope="row">최근접속일</th>
                    <td><?php echo $mb['mb_today_login'] ?></td>
                </tr>
                <tr>
                    <th scope="row">IP</th>
                    <td colspan="3"><?php echo $mb['mb_ip'] ?></td>
                </tr>
                <?php if ($config['cf_use_email_certify']) { ?>
                    <tr>
                        <th scope="row">인증일시</th>
                        <td colspan="3">
                            <?php if ($mb['mb_email_certify'] == '0000-00-00 00:00:00') { ?>
                                <?php echo help('회원님이 메일을 수신할 수 없는 경우 등에 직접 인증처리를 하실 수 있습니다.') ?>
                                <input type="checkbox" name="passive_certify" id="passive_certify">
                                <label for="passive_certify">수동인증</label>
                            <?php } else { ?>
                                <?php echo $mb['mb_email_certify'] ?>
                            <?php } ?>
                        </td>
                    </tr>
                <?php } ?>
            <?php } ?>

            <tr>
                <th scope="row"><label for="mb_leave_date">탈퇴일자</label></th>
                <td>
                    <input type="text" name="mb_leave_date" value="<?php echo $mb['mb_leave_date'] ?>"
                           id="mb_leave_date" class="frm_input" maxlength="8">
                    <input type="checkbox" value="<?php echo date("Ymd"); ?>" id="mb_leave_date_set_today" onclick="if (this.form.mb_leave_date.value==this.form.mb_leave_date.defaultValue) {
this.form.mb_leave_date.value=this.value; } else { this.form.mb_leave_date.value=this.form.mb_leave_date.defaultValue; }">
                    <label for="mb_leave_date_set_today">탈퇴일을 오늘로 지정</label>
                </td>
                <th scope="row">접근차단일자</th>
                <td>
                    <input type="text" name="mb_intercept_date" value="<?php echo $mb['mb_intercept_date'] ?>"
                           id="mb_intercept_date" class="frm_input" maxlength="8">
                    <input type="checkbox" value="<?php echo date("Ymd"); ?>" id="mb_intercept_date_set_today" onclick="if
(this.form.mb_intercept_date.value==this.form.mb_intercept_date.defaultValue) { this.form.mb_intercept_date.value=this.value; } else {
this.form.mb_intercept_date.value=this.form.mb_intercept_date.defaultValue; }">
                    <label for="mb_intercept_date_set_today">접근차단일을 오늘로 지정</label>
                </td>
            </tr>

            <tr>
                <th scope="row"><font color="#ff0000">*</font>회원구분</th>
                <td colspan="3">
                    <label for="mb_type" class="sound_only">회원구분</label>
                    <select name="mb_type" id="mb_type" style="width:135px">
                        <option value="9"<?php echo get_selected($mb['mb_type'], "9"); ?>>관리자</option>
                    </select>
                </td>
            </tr>

            </tbody>
        </table>

        <div id="wrap_list">

        </div>

    </div>

    <div class="btn_fixed_top">
        <a href="./member_list6.php?<?php echo $qstr ?>" class="btn btn_02">목록</a>
        <!-- <a href="javascript:history.go(-1);" class="btn btn_02">목록</a>-->
        <input type="submit" value="확인" class="btn_submit btn" accesskey='s'>
    </div>
</form>


<script>

    function onChangeAddr(step, val) {
        var sido = $("#pharm_sido");
        var gugun = $("#pharm_gugun");
        var dong = $("#pharm_dong");
        var obj = "";

        if (step == 1) {
            obj = gugun;
            gugun.empty().data('option');
            gugun.append("<option value=''>시/군/구</option>");
        }
        if (step == 2) {
            obj = dong;
        }
        dong.empty().data('option');
        dong.append("<option value=''>동/면/읍</option>");
        $.ajax({
            type: "post",
            dataType: "xml",
            url: "/addr.php",
            data: {"mem_sido": sido.val(), "mem_gugun": gugun.val(), "step": step},
            success: function (data) {
                var row = $(data).find("ROW").find("CELL");
                var size = row.length;
                for (var i = 0; i < size; i++) {
                    var cell = row.eq(i);
                    $("<option></option>").text(cell.find("NAME").text()).attr("value", cell.find("NAME").text()).appendTo(obj);
                }

                if (step == 1) {
                    gugun.val(val);
                }
                if (step == 2) {
                    dong.val(val);
                }
            },
            error: function (request, status, errorThrown) {
                //alert(errorThrown);
            }
        });
    }


    function fmember_submit(f) {
        if (!f.mb_icon.value.match(/\.(gif|jpe?g|png)$/i) && f.mb_icon.value) {
            alert('아이콘은 이미지 파일만 가능합니다.');
            return false;
        }

        if (!f.mb_img.value.match(/\.(gif|jpe?g|png)$/i) && f.mb_img.value) {
            alert('회원이미지는 이미지 파일만 가능합니다.');
            return false;
        }

        <?php if($mb['mb_type'] == 5) { ?>
        var dept = $("#dept option:selected").val();
        var dept2 = $("#dept2 option:selected").val();
        var pharm_manager_dept = dept2;
        if (dept2 == '') pharm_manager_dept = dept;
        $('#pharm_manager_dept').val(pharm_manager_dept);
        <?php } ?>

        return true;
    }

    function AddRow() {

        var tr_data = "<tr>";
        tr_data += "	<td style=\"text-align:center;width:60px;\">";
        tr_data += "		<input type=\"hidden\" name=\"Charge_Idx[]\" value=\"0\">";
        tr_data += "		<input type=\"hidden\" name=\"DelYn[]\" value=\"N\">";
        tr_data += "		<select name=\"Charge_Task[]\">";
        tr_data += "			<option value=\"CS\">CS</option>";
        tr_data += "			<option value=\"영업\">영업</option>";
        tr_data += "			<option value=\"출고\">출고</option>";
        tr_data += "			<option value=\"정산\">정산</option>";
        tr_data += "		</select>";
        tr_data += "	</td>";
        tr_data += "	<td style=\"text-align:center;\"><input type=\"text\" name=\"Charge_Name[]\" maxlength='50' class=\"frm_input\" style=\"width:120px;\"></td>";
        tr_data += "	<td style=\"text-align:center;\"><input type=\"text\" name=\"Charge_Bigo[]\" maxlength='100' class=\"frm_input\" style=\"width:220px;\"></td>";
        tr_data += "	<td style=\"text-align:center;\"><input type=\"text\" name=\"Charge_Tel[]\" maxlength='50' class=\"frm_input\" style=\"width:180px;\"></td>";
        tr_data += "	<td style=\"text-align:center;\"><input type=\"text\" name=\"Charge_Hp[]\" maxlength='50' class=\"frm_input\" style=\"width:180px;\"></td>";
        tr_data += "	<td style=\"text-align:center;\"><input type=\"text\" name=\"Charge_Email[]\" maxlength='50' class=\"frm_input\" style=\"width:180px;\"></td>";
        tr_data += "	<td style=\"text-align:center;\">";
        tr_data += "       <button type=\"button\" class=\"btn_submit btn\" onclick=\"javascript:DelRow(this);\">삭제</button>";
        tr_data += "	</td>";
        tr_data += "</tr>	";

        $('#group_table').append(tr_data);

    }

    function DelRow(gobj) {

        var tr = $(gobj).parent().parent();
        tr.find('input[name^=DelYn]').val("Y"); //개별삭제
        tr.css("display", "none");

    }

    function ChargeReg() {

        var frm = document.fmember;

        if (!confirm('담당자정보를 저장하시겠습니까?')) {
            return false;
        }
        frm.target = "proc_frm";
        frm.method = "post";
        frm.action = "./Charge_Proc.php";
        frm.submit();

        //기본값으로 돌려놓음.
        frm.target = "_self";
        frm.action = "./member_form_update.php";

    }

    function ConfirmSend(GetMbId) {

        if (confirm("승인하시겠습니까?")) {

            document.c_frm.method = "post";
            document.c_frm.action = "/adm/confirm_update.php";
            document.c_frm.submit();

        }

    }

</script>

<form name="c_frm">
    <input type="hidden" name="w" value="<?php echo $w ?>">
    <input type="hidden" name="sfl" value="<?php echo $sfl ?>">
    <input type="hidden" name="stx" value="<?php echo $stx ?>">
    <input type="hidden" name="sst" value="<?php echo $sst ?>">
    <input type="hidden" name="sod" value="<?php echo $sod ?>">
    <input type="hidden" name="page" value="<?php echo $page ?>">
    <input type="hidden" name="token" value="">
    <input type="hidden" name="mb_id" value="<?=$mb['mb_id']?>">
</form>


<div style="display:none;">
    <iframe src="" name="proc_frm" height="0"></iframe>
</div>

<?php
include_once('./admin.tail.php');
?>

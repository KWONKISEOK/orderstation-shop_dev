<?php
include_once('./_common.php');

$sql = " select * from {$g5['new_win_table']}
          where nw_id='$nw_id'";
$nw = sql_fetch($sql, false);
?>

<link rel="stylesheet" href="<?php echo G5_THEME_CSS_URL; ?>/default_shop_add.css?ver=<?php echo date('Ymd'); ?>">
<!-- <link rel="stylesheet" href="<?php echo G5_URL; ?>/skin/shop/basic/style.css?ver=112"> -->
<?php if (!defined('_SHOP_')) { ?>
    <link rel="stylesheet" href="<?php echo G5_URL; ?>/skin/board/basic/style.css?ver=<?php echo date('Ymd'); ?>">
<?php } ?>

<link rel="stylesheet" href="<?php echo G5_JS_URL ?>/font-awesome/css/font-awesome.min.css">
<style>
    .dim-layer {
        position: fixed;
        _position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        z-index: 100;
    }

    .dim-layer .dimBg {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        background: #000;
        opacity: .5;
        filter: alpha(opacity=50);
    }
</style>

<!-- 팝업레이어 시작 { -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/jquery-ui.min.js"></script>
<div class="dim-layer">
    <div class="dimBg"></div>
    <div id="hd_pop">
        <?php
        if ($nw['nw_version'] == "1") {
            ?>
            <div id="hd_pops_<?php echo $nw['nw_id'] ?>" class="hd_pops" style="top:150px;left:150px">
                <div class="hd_pops_con" style="width:<?php echo $nw['nw_width'] ?>px;height:<?php echo $nw['nw_height'] ?>px">
                    <?php echo conv_content($nw['nw_content'], 1); ?>
                </div>
                <div class="hd_pops_footer">
                    <button class="hd_pops_reject hd_pops_<?php echo $nw['nw_id']; ?> <?php echo $nw['nw_disable_hours']; ?>">
                        <strong><?php echo $nw['nw_disable_hours']; ?></strong>시간 동안 다시 열람하지 않습니다.
                    </button>
                    <button class="hd_pops_close hd_pops_<?php echo $nw['nw_id']; ?>">닫기
                        <i class="fa fa-times" aria-hidden="true"></i></button>
                </div>
            </div>
            <?php
        } else {
            $pop_close = "";
            if ($nw['nw_version'] == "2") {
                // 검은색 버튼 닫기
                $pop_close = G5_IMG_URL."/pop/close_black.png";
            } else if ($nw['nw_version'] == "3") {
                // 흰색 버튼 닫기
                $pop_close = G5_IMG_URL."/pop/close_white.png";
            }
            ?>
            <div id="hd_pops_<?php echo $nw['nw_id'] ?>" class="hd_pops" style="top:150px;left:150px;background: none;border: 0;">
                <div class="hd_pops_con" style="width:<?php echo $nw['nw_width'] ?>px;<?php echo ($nw['nw_height'] > 0) ? 'height:'.$nw['nw_height'].'px;' : 'height:auto;' ?>">
                    <?php echo conv_content($nw['nw_content'], 1); ?>
                    <span class="hd_pops_close hd_pops_<?php echo $nw['nw_id']; ?>" style="position: absolute;top: 24px;right: 24px;">
                        <img src="<?php echo $pop_close ?>" alt="" style="width: 20px;">
                    </span>
                </div>
                <div class="hd_pops_footer" style="top: <?php echo $nw['nw_top_2'] ?>px; text-align: center;padding: 10px;font-weight: 900;border-radius: 0 0 20px 20px;">
                    <span class="hd_pops_reject hd_pops_<?php echo $nw['nw_id']; ?> <?php echo $nw['nw_disable_hours']; ?>" >
                        <i class="fa fa-check-square" aria-hidden="true"></i>&nbsp;
                        <strong><?php echo $nw['nw_disable_hours']; ?></strong>시간 동안 다시 열람하지 않습니다.
                    </span>
                </div>
            </div>
            <?php
        }
        ?>
    </div>
</div>

<script>
    $(function () {

        $(".hd_pops_reject").click(function () {
            var id = $(this).attr('class').split(' ');
            var ck_name = id[1];
            var exp_time = parseInt(id[2]);
            $("#" + id[1]).css("display", "none");
            //set_cookie(ck_name, 1, exp_time, g5_cookie_domain);
        });
        $('.hd_pops_close').click(function () {
            var idb = $(this).attr('class').split(' ');
            $('#' + idb[1]).css('display', 'none');
        });
        $("#hd").css("z-index", 1000);
    });

    $(document).ready(function () {
        $(".hd_pops").draggable();
    });
</script>
<!-- } 팝업레이어 끝 -->
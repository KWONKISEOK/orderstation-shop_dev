<?php

include_once("./_common.php");

$mb_id = $_POST['mb_id'];
$mb_type = $_POST['mb_type'];

//공급사 tbl_company
function sfSalesRs($code_type, $sel)
{

    $sql = "select mb_id, mb_name
			  from tbl_member where mb_type = 5";


    $res = sql_query($sql);

    while ($row = sql_fetch_array($res)) {
        $str = $str . "<option value='" . $row['mb_id'] . "'";
        if ($sel == $row['mb_id']) $str = $str . "selected";
        $str = $str . ">";
        $str = $str . $row['mb_name'] . "</option>";
    }
    return $str;
}


$mb = get_member($mb_id);

$chk_sql = " select * from tbl_charge_info_mod where DelYn = 'N' and mb_id = '{$mb_id}' order by Charge_RegDate asc ";
$chk_result = sql_query($chk_sql);
$chk_result_cnt = sql_num_rows($chk_result);

?>

<h3 style="padding:10px;font-size:1.2em;">
    공급사 추가정보(관리자에게 승인요청 하기)
    <? if (!empty($mb['comp_charge_datetime_1'])) { ?>
        승인요청일:<?= $mb['comp_charge_datetime_1'] ?>
    <? } ?>
</h3>
<input type="hidden" name="mb_id" value="<?= $mb_id ?>"/>
<input type="hidden" name="comp_code" value="<?= $mb['comp_code'] ?>"/>

<?php if ($chk_result_cnt == 0) { ?>
<table>
    <tr>
        <th scope="row">담당자정보</th>
        <td colspan='3'>
            <br>
            <table style="width:80%;" border="0" id="group_table">
                <tr>
                    <th style="text-align:center;width:60px;">업무</th>
                    <th style="text-align:center;width:120px;">담당자명</th>
                    <th style="text-align:center;width:220px;">비고(직책)</th>
                    <th style="text-align:center;width:180px;">Tel</th>
                    <th style="text-align:center;width:180px;">HP</th>
                    <th style="text-align:center;width:180px;">이메일</th>
                    <th style="text-align:center;width:80px;"><a href="javascript:AddRow();" class="btn btn_02">추가</a>
                    </th>
                </tr>
                <?
                $tr_sql = " select * from tbl_charge_info where DelYn = 'N' and mb_id = '{$mb_id}' order by Charge_RegDate asc ";
                $tr_result = sql_query($tr_sql);
                $tr_result_cnt = sql_num_rows($tr_result);

                if ($tr_result_cnt == 0) {

                    ?>
                    <tr>
                        <td style="text-align:center;width:60px;">
                            <input type="hidden" name="Charge_Idx[]" value="0">
                            <input type="hidden" name="DelYn[]" value="N">
                            <select name="Charge_Task[]">
                                <option value="CS">CS</option>
                                <option value="영업">영업</option>
                                <option value="출고">출고</option>
                                <option value="정산">정산</option>
                            </select>
                        </td>
                        <td style="text-align:center;"><input type="text" name="Charge_Name[]" maxlength="50"
                                                              class="frm_input" style="width:120px;"></td>
                        <td style="text-align:center;"><input type="text" name="Charge_Bigo[]" maxlength="100"
                                                              class="frm_input" style="width:220px;"></td>
                        <td style="text-align:center;"><input type="text" name="Charge_Tel[]" maxlength="50"
                                                              class="frm_input" style="width:180px;"></td>
                        <td style="text-align:center;"><input type="text" name="Charge_Hp[]" maxlength="50"
                                                              class="frm_input" style="width:180px;"></td>
                        <td style="text-align:center;"><input type="text" name="Charge_Email[]" maxlength="50"
                                                              class="frm_input" style="width:180px;"></td>
                        <td></td>
                    </tr>
                    <?

                } else {

                    $i = 1;
                    while ($tr_row = sql_fetch_array($tr_result)) {

                        ?>
                        <tr>
                            <td style="text-align:center;width:60px;">
                                <input type="hidden" name="Charge_Idx[]" value="<?= $tr_row["Charge_Idx"] ?>">
                                <input type="hidden" name="DelYn[]" value="N">
                                <select name="Charge_Task[]">
                                    <option value="CS" <?if ($tr_row["Charge_Task"] == "CS"){ ?>selected<?} ?> >CS</option>
                                    <option value="영업" <?if ($tr_row["Charge_Task"] == "영업"){ ?>selected<?} ?> >영업</option>
                                    <option value="출고" <?if ($tr_row["Charge_Task"] == "출고"){ ?>selected<?} ?> >출고</option>
                                    <option value="정산" <?if ($tr_row["Charge_Task"] == "정산"){ ?>selected<?} ?> >정산</option>
                                </select>
                            </td>
                            <td style="text-align:center;">
                                <input type="text" name="Charge_Name[]" maxlength="50" class="frm_input"
                                       style="width:120px;" value="<?= $tr_row["Charge_Name"] ?>">
                            </td>
                            <td style="text-align:center;">
                                <input type="text" name="Charge_Bigo[]" maxlength="100" class="frm_input"
                                       style="width:220px;" value="<?= $tr_row["Charge_Bigo"] ?>">
                            </td>
                            <td style="text-align:center;">
                                <input type="text" name="Charge_Tel[]" maxlength="50" class="frm_input"
                                       style="width:180px;" value="<?= $tr_row["Charge_Tel"] ?>">
                            </td>
                            <td style="text-align:center;">
                                <input type="text" name="Charge_Hp[]" maxlength="50" class="frm_input"
                                       style="width:180px;" value="<?= $tr_row["Charge_Hp"] ?>">
                            </td>
                            <td style="text-align:center;">
                                <input type="text" name="Charge_Email[]" maxlength="50" class="frm_input"
                                       style="width:180px;" value="<?= $tr_row["Charge_Email"] ?>">
                            </td>
                            <td style="text-align:center;">
                                <? if ($i > 1) { ?>
                                    <button type="button" class="btn_submit btn" onclick="javascript:DelRow(this);">삭제
                                    </button>
                                <? } ?>
                            </td>
                        </tr>
                        <?
                        $i++;

                    }

                }

                ?>

            </table>
            <br>
            <br>
        </td>
    </tr>
    <tr>
        <th scope="row"><font color="#ff0000">*</font> 업체명
        </td>
        <td><input type="text" name="comp_name_mod" size="25" value="<?php echo $mb['comp_name']; ?>" maxlength="25"
                   class="frm_input"></td>
        <th scope="row" style="display:none;"></th>
        <td style="display:none;"></td>
    </tr>
    <tr>
        <th scope="row"> 최소주문금액</th>
        <td><input type="text" name="comp_limit_mod" size="25" class="frm_input"
                   value="<?php echo $mb['comp_limit']; ?>" maxlength="25"></td>
        <th scope="row"> 브랜드명</th>
        <td><input type="text" name="comp_brand_mod" size="25" class="frm_input"
                   value="<?php echo $mb['comp_brand']; ?>" maxlength="25"></td>

    </tr>
    <tr>
        <th scope="row"> 대표자명</th>
        <td><input type="text" name="comp_ceo_name_mod" size="20" value="<?php echo $mb['comp_ceo_name']; ?>"
                   maxlength="20" class="frm_input"></td>
        <th scope="row"> 대표전화번호</th>
        <td><input type="text" name="comp_tel_mod" value="<?php echo $mb['comp_tel']; ?>" maxlength="20" size="20"
                   class="frm_input">
        </td>
    </tr>
    <tr>
        <th scope="row"> 담당자명</th>
        <td><input type="text" name="comp_md_name_mod" size="20" value="<?php echo $mb['comp_md_name']; ?>"
                   maxlength="20" class="frm_input"></td>
        <th scope="row"> 담당자연락처</th>
        <td><input type="text" name="comp_md_tel_mod" value="<?php echo $mb['comp_md_tel']; ?>" maxlength="20"
                   size="20" class="frm_input">
        </td>
    </tr>
    <tr>
        <th scope="row"> 주문시간</th>
        <td colspan="3"><textarea name="comp_order_time_mod"
                                  style="width:95%; height:50px"><?php echo $mb['comp_order_time']; ?></textarea>
        </td>
    </tr>
    <tr>
        <th scope="row"> 배송시간</th>
        <td colspan="3"><textarea name="comp_trans_time_mod"
                                  style="width:95%; height:50px"><?php echo $mb['comp_trans_time']; ?></textarea>
        </td>
    </tr>
</table>
<?php } else { ?>
<table>
    <tr>
        <th scope="row">담당자정보</th>
        <td colspan='3'>
            <br>
            <table style="width:80%;" border="0" id="group_table">
                <tr>
                    <th style="text-align:center;width:60px;">업무</th>
                    <th style="text-align:center;width:120px;">담당자명</th>
                    <th style="text-align:center;width:220px;">비고(직책)</th>
                    <th style="text-align:center;width:180px;">Tel</th>
                    <th style="text-align:center;width:180px;">HP</th>
                    <th style="text-align:center;width:180px;">이메일</th>
                    <th style="text-align:center;width:80px;"><a href="javascript:AddRow();" class="btn btn_02">추가</a>
                    </th>
                </tr>
                <?
                $tr_sql = " select * from tbl_charge_info_mod where DelYn = 'N' and mb_id = '{$mb_id}' order by Charge_RegDate asc ";
                $tr_result = sql_query($tr_sql);
                $tr_result_cnt = sql_num_rows($tr_result);

                if ($tr_result_cnt == 0) {

                    ?>
                    <tr>
                        <td style="text-align:center;width:60px;">
                            <input type="hidden" name="Charge_Idx[]" value="0">
                            <input type="hidden" name="DelYn[]" value="N">
                            <select name="Charge_Task[]">
                                <option value="CS">CS</option>
                                <option value="영업">영업</option>
                                <option value="출고">출고</option>
                                <option value="정산">정산</option>
                            </select>
                        </td>
                        <td style="text-align:center;"><input type="text" name="Charge_Name[]" maxlength="50"
                                                              class="frm_input" style="width:120px;"></td>
                        <td style="text-align:center;"><input type="text" name="Charge_Bigo[]" maxlength="100"
                                                              class="frm_input" style="width:220px;"></td>
                        <td style="text-align:center;"><input type="text" name="Charge_Tel[]" maxlength="50"
                                                              class="frm_input" style="width:180px;"></td>
                        <td style="text-align:center;"><input type="text" name="Charge_Hp[]" maxlength="50"
                                                              class="frm_input" style="width:180px;"></td>
                        <td style="text-align:center;"><input type="text" name="Charge_Email[]" maxlength="50"
                                                              class="frm_input" style="width:180px;"></td>
                        <td></td>
                    </tr>
                    <?

                } else {

                    $i = 1;
                    while ($tr_row = sql_fetch_array($tr_result)) {

                        ?>
                        <tr>
                            <td style="text-align:center;width:60px;">
                                <input type="hidden" name="Charge_Idx[]" value="<?= $tr_row["Charge_Idx"] ?>">
                                <input type="hidden" name="DelYn[]" value="N">
                                <select name="Charge_Task[]">
                                    <option value="CS" <?
                                    if ($tr_row["Charge_Task"] == "CS"){ ?>selected<?
                                    } ?> >CS
                                    </option>
                                    <option value="영업" <?
                                    if ($tr_row["Charge_Task"] == "영업"){ ?>selected<?
                                    } ?> >영업
                                    </option>
                                    <option value="출고" <?
                                    if ($tr_row["Charge_Task"] == "출고"){ ?>selected<?
                                    } ?> >출고
                                    </option>
                                    <option value="정산" <?
                                    if ($tr_row["Charge_Task"] == "정산"){ ?>selected<?
                                    } ?> >정산
                                    </option>
                                </select>
                            </td>
                            <td style="text-align:center;">
                                <input type="text" name="Charge_Name[]" maxlength="50" class="frm_input"
                                       style="width:120px;" value="<?= $tr_row["Charge_Name"] ?>">
                            </td>
                            <td style="text-align:center;">
                                <input type="text" name="Charge_Bigo[]" maxlength="100" class="frm_input"
                                       style="width:220px;" value="<?= $tr_row["Charge_Bigo"] ?>">
                            </td>
                            <td style="text-align:center;">
                                <input type="text" name="Charge_Tel[]" maxlength="50" class="frm_input"
                                       style="width:180px;" value="<?= $tr_row["Charge_Tel"] ?>">
                            </td>
                            <td style="text-align:center;">
                                <input type="text" name="Charge_Hp[]" maxlength="50" class="frm_input"
                                       style="width:180px;" value="<?= $tr_row["Charge_Hp"] ?>">
                            </td>
                            <td style="text-align:center;">
                                <input type="text" name="Charge_Email[]" maxlength="50" class="frm_input"
                                       style="width:180px;" value="<?= $tr_row["Charge_Email"] ?>">
                            </td>
                            <td style="text-align:center;">
                                <? if ($i > 1) { ?>
                                    <button type="button" class="btn_submit btn" onclick="javascript:DelRow(this);">삭제
                                    </button>
                                <? } ?>
                            </td>
                        </tr>
                        <?
                        $i++;

                    }

                }

                ?>

            </table>
            <br>
            <br>
        </td>
    </tr>
    <tr>
        <th scope="row"><font color="#ff0000">*</font> 업체명
        </td>
        <td><input type="text" name="comp_name_mod" size="25" value="<?php echo $mb['comp_name_mod']; ?>" maxlength="25"
                   class="frm_input"></td>
        <th scope="row" style="display:none;"></th>
        <td style="display:none;"></td>
    </tr>
    <tr>
        <th scope="row"> 최소주문금액</th>
        <td><input type="text" name="comp_limit_mod" size="25" class="frm_input"
                   value="<?php echo $mb['comp_limit_mod']; ?>" maxlength="25"></td>
        <th scope="row"> 브랜드명</th>
        <td><input type="text" name="comp_brand_mod" size="25" class="frm_input"
                   value="<?php echo $mb['comp_brand_mod']; ?>" maxlength="25"></td>

    </tr>
    <tr>
        <th scope="row"> 대표자명</th>
        <td><input type="text" name="comp_ceo_name_mod" size="20" value="<?php echo $mb['comp_ceo_name_mod']; ?>"
                   maxlength="20" class="frm_input"></td>
        <th scope="row"> 대표전화번호</th>
        <td><input type="text" name="comp_tel_mod" value="<?php echo $mb['comp_tel_mod']; ?>" maxlength="20" size="20"
                   class="frm_input">
        </td>
    </tr>
    <tr>
        <th scope="row"> 담당자명</th>
        <td><input type="text" name="comp_md_name_mod" size="20" value="<?php echo $mb['comp_md_name_mod']; ?>"
                   maxlength="20" class="frm_input"></td>
        <th scope="row"> 담당자연락처</th>
        <td><input type="text" name="comp_md_tel_mod" value="<?php echo $mb['comp_md_tel_mod']; ?>" maxlength="20"
                   size="20" class="frm_input">
        </td>
    </tr>
    <tr>
        <th scope="row"> 주문시간</th>
        <td colspan="3"><textarea name="comp_order_time_mod"
                                  style="width:95%; height:50px"><?php echo $mb['comp_order_time_mod']; ?></textarea>
        </td>
    </tr>
    <tr>
        <th scope="row"> 배송시간</th>
        <td colspan="3"><textarea name="comp_trans_time_mod"
                                  style="width:95%; height:50px"><?php echo $mb['comp_trans_time_mod']; ?></textarea>
        </td>
    </tr>
</table>
<?php } ?>
<br>
<div style="width:90%;text-align:center;">
    <a href="javascript:SendReg();" class="btn btn_03">승인요청하기</a>
</div>
<br>

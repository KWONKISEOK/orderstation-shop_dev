<?php
$sub_menu = "100200";
include_once('./_common.php');

if ($is_admin != 'super')
    alert('최고관리자만 접근 가능합니다.');

$mb = get_member($mb_id);
if (!$mb['mb_id'])
    alert('존재하는 회원아이디가 아닙니다.');

$cnt = count($_POST['r_hidden']);

// 값이 1이면 각각 r, w, d로,
// 값이 0이면 빈 값으로 변경.
for ($i = 0; $i < $cnt; $i++) {

    if ($_POST['r_hidden'][$i] == 1) {
        $_POST['r_hidden'][$i] = "r";
    } else {
        $_POST['r_hidden'][$i] = "";
    }

    if ($_POST['w_hidden'][$i] == 1) {
        $_POST['w_hidden'][$i] = "w";
    } else {
        $_POST['w_hidden'][$i] = "";
    }

    if ($_POST['d_hidden'][$i] == 1) {
        $_POST['d_hidden'][$i] = "d";
    } else {
        $_POST['d_hidden'][$i] = "";
    }

    $sql = "";

    /*************** 트랜잭션 관련 ****************/
    $error_cnt = 0;
    mysqli_autocommit($g5['connect_db'], false);
    /*************** 트랜잭션 관련 ****************/

    $sql = " insert into {$g5['auth_table']}
            set mb_id   = '{$_POST['mb_id']}',
                au_menu = '{$_POST['au_menu'][$i]}',
                au_auth = '{$_POST['r_hidden'][$i]},{$_POST['w_hidden'][$i]},{$_POST['d_hidden'][$i]}' ";

    $result = sql_query($sql, FALSE);

    if (!$result) {
        $sql = " update {$g5['auth_table']}
                set au_auth = '{$_POST['r_hidden'][$i]},{$_POST['w_hidden'][$i]},{$_POST['d_hidden'][$i]}'
              where mb_id   = '{$_POST['mb_id']}'
                and au_menu = '{$_POST['au_menu'][$i]}' ";
        sql_query($sql);

    }

    /*************** 트랜잭션 관련 ****************/
    if (mysqli_errno($g5['connect_db'])) {
        $error_cnt += 1;
    }
    /*************** 트랜잭션 관련 ****************/

    /*************** 트랜잭션 관련 ****************/
    if ($error_cnt > 0) {
        mysqli_rollback($g5['connect_db']);
        mysqli_close($g5['connect_db']);
        echo "
	<script>
		alert('데이터베이스의 에러로 인해 롤백되었습니다.');
		history.back();
	</script>
	";
        exit;
    } else {
        mysqli_commit($g5['connect_db']);
    }
    /*************** 트랜잭션 관련 ****************/

}

?>
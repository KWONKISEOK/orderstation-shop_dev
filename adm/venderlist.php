<?php
$sub_menu = '300830';
include_once('./_common.php');

auth_check($auth[$sub_menu], "r");


$g5['title'] = '공급사정보공유';
include_once (G5_ADMIN_PATH.'/admin.head.php');

$sql_common = " from tbl_write_vender";

// 테이블의 전체 레코드수만 얻음
$sql = " select count(*) as cnt " . $sql_common;
$row = sql_fetch($sql);
$total_count = $row['cnt'];

$rows = $config['cf_page_rows'];
$total_page  = ceil($total_count / $rows);  // 전체 페이지 계산
if ($page < 1) { $page = 1; } // 페이지가 없으면 첫 페이지 (1 페이지)
$from_record = ($page - 1) * $rows; // 시작 열을 구함

$sql = "select * $sql_common order by wr_id desc limit $from_record, {$config['cf_page_rows']} ";
$result = sql_query($sql);
?>

<div class="local_ov01 local_ov">
    <?php if ($page > 1) {?><a href="<?php echo $_SERVER['SCRIPT_NAME']; ?>">처음으로</a><?php } ?>
    <span class="btn_ov01"><span class="ov_txt">전체 내용</span><span class="ov_num"> <?php echo $total_count; ?>건</span></span>
</div>

<? if( $member["mb_type"] == "9" ){ ?>
<div class="btn_fixed_top">
    <a href="./venderform.php" class="btn btn_01">내용 추가</a>
</div>
<? } ?>

<div class="tbl_head01 tbl_wrap">
    <table>
    <caption><?php echo $g5['title']; ?> 목록</caption>
    <thead>
    <tr>
        <th scope="col">ID</th>
        <th scope="col">제목</th>
		<th scope="col">작성자</th>
		<th scope="col">등록일</th>
		<? if( $member["mb_type"] == "9" ){ ?>
		<th scope="col">공급사선택</th>
        <th scope="col">관리</th>
		<? } ?>
    </tr>
    </thead>
    <tbody>
    <?php for ($i=0; $row=sql_fetch_array($result); $i++) {
        $bg = 'bg'.($i%2);
    ?>
    <tr class="<?php echo $bg; ?>">
        <td class="td_id"><?php echo $row['wr_id']; ?></td>
        <td class="td_left"><a href="./venderview.php?wr_id=<?php echo $row['wr_id']; ?>" ><?php echo htmlspecialchars2($row['wr_subject']); ?></a></td>
		<td class=""><?php echo htmlspecialchars2($row['wr_name']); ?></td>
		<td class=""><?php echo htmlspecialchars2($row['wr_datetime']); ?></td>
		<? if( $member["mb_type"] == "9" ){ ?>
		<td>
			<? if( $row['wr_4'] == "A" ){ ?> 전체 <?}else if( $row['wr_4'] == "C" ){ ?> 선택 <? } ?>
		</td>
        <td class="td_mng td_mng_l">
			<?php if( ($row['mb_id'] == $member['mb_id']) || $member['mb_type'] == "9" ) { ?>
            <a href="./venderform.php?w=u&amp;wr_id=<?php echo $row['wr_id']; ?>" class="btn btn_03"><span class="sound_only"><?php echo htmlspecialchars2($row['wr_subject']); ?> </span>수정</a>
            <a href="./venderformupdate.php?w=d&amp;wr_id=<?php echo $row['wr_id']; ?>" onclick="return delete_confirm(this);" class="btn btn_02"><span class="sound_only"><?php echo htmlspecialchars2($row['wr_subject']); ?> </span>삭제</a>
			<?php } ?>
        </td>
		<? } ?>
    </tr>
    <?php
    }
    if ($i == 0) {
        echo '<tr><td colspan="5" class="empty_table">자료가 한건도 없습니다.</td></tr>';
    }
    ?>
    </tbody>
    </table>
</div>

<?php echo get_paging(G5_IS_MOBILE ? $config['cf_mobile_pages'] : $config['cf_write_pages'], $page, $total_page, "{$_SERVER['SCRIPT_NAME']}?$qstr&amp;page="); ?>

<?php

//admin.tail.php 파일에 있는 ViewPopup 함수는 게시판 확인페이지에서는 다시 부르면 안된다.
$ViewPopupYN = "N";


include_once (G5_ADMIN_PATH.'/admin.tail.php');
?>

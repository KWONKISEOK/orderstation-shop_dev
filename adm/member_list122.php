<?php
$sub_menu = "200100";
include_once('./_common.php');
require("INIomr.php");
//check_demo();

if (!count($_POST['chk'])) {
    alert("정산 하실 항목을 하나 이상 체크하세요.");
}

//auth_check($auth[$sub_menu], 'w');

//check_admin_token();

$req_date = str_replace('-', '', $_POST['fr_date']);
$yearmonth = $_POST['yearmonth'];
//print_r($_POST);
for ($i = 0; $i < count($_POST['chk']); $i++) {
    // 실제 번호를 넘김
    $k = $_POST['chk'][$i];
    $mb_id = $_POST['mb_id'][$k];

    $sql = "select * ";
    $sql .= "from ( ";
    $sql .= "  select ";
    $sql .= "      pharm_custno, od_calculate_yn, sum(case when od_period_yn = 'Y' then od_total_incen / od_period_cnt ELSE od_total_incen end) incen ";
    $sql .= "  from tbl_shop_order a ";
    $sql .= "  inner join tbl_shop_order_detail b ";
    $sql .= "      on a.od_id = b.od_id ";
    $sql .= "  inner join tbl_shop_order_receiver c ";
    $sql .= " 	    on a.od_id = c.od_id and b.od_num = c.od_num ";
    $sql .= "  where ";
    $sql .= "      c.od_pay_yn = 'Y' and a.od_gubun = '3' and date_format(c.od_date1, '%Y%m') = '".$yearmonth."' and c.od_status != '취소'  and a.pharm_custno != '' and b.it_id not in('S00603','S00604') ";
    $sql .= "  group by pharm_custno, od_calculate_yn ";
    $sql .= ") aa ";
    $sql .= "inner join tbl_member bb ";
    $sql .= "  on aa.pharm_custno = bb.pharm_custno and mb_type = '1' ";
    $sql .= "  where bb.mb_id = '$mb_id' ";
    $res = sql_query($sql);
    if (!$res) exit('Cannot run query.');

    while ($row = sql_fetch_array($res)) {

        $mb_id1 = $row["mb_id"];
        $mb_name = $row["mb_name"];
        $mb_tel = $row["mb_tel"];
        $pharm_name = $row["pharm_name"];
        $pharm_custno = $row["pharm_custno"];
        $pharm_number = $row["pharm_number"];


        $sales_name = $row["sales_name"];
        $mb_status = $row["pharm_status"];
        $pharm_bank_agree = $row["pharm_bank_agree"];
        $pharm_bank_agree_date = $row["pharm_bank_agree_date"];
        $pharm_bank_user = $row["pharm_bank_user"];
        $pharm_bank_num = $row["pharm_bank_num"];
        $pharm_bank_code = $row["pharm_bank_code"];
        $od_calculate_yn = $row["od_calculate_yn"];
        $incen = $row["incen"];
        $org_incen = $incen;

        if ($pharm_bank_agree == '') {
            $mem_bank_agree = "N";
        }

        $pharm_bank_name = ViewBank($pharm_bank_code);
        if ($incen > 30000) {
            $incen2 = ($incen * 0.003) * 10;
            $incen3 = ($incen2 * 0.01) * 10;
            if ($incen2 >= 1000) {
                $incen = $incen - ($incen2 + $incen3);
            } else {
                $incen2 = 0;
                $incen3 = 0;
            }
        } else {
            $incen2 = 0;
            $incen3 = 0;
        }
        $incen = floor($incen);
        $res_cd = '00';
        //계좌이체
        if ($pharm_bank_agree == "Y" and $od_calculate_yn == "N" and $incen > 0) {
            /*
            '			'###############################################################################
            '			'# 2. 인스턴스 초기화 #
            '			'######################
            '			PInst = INIpay.Initialize("")
            '
            '			'###############################################################################
            '			'# 3. 거래 유형 설정 #
            '			'#####################
            '			INIpay.SetActionType CLng(PInst), "REQUESTOM"
            '
            '			'###############################################################################
            '			'# 4. 정보 설정 #
            '			'################
            '			INIpay.SetField CLng(PInst), "id_merchant", ""  ' 상점아이디(M I D)
            '			INIpay.SetField CLng(PInst), "id_mall", MEM_ID ' 판매자ID
            '			INIpay.SetField CLng(PInst), "dt_pay", Calculate_Date  ' 지급일(YYYYMMDD)
            '			INIpay.SetField CLng(PInst), "no_comp", MEM_PHARM_NUMBER  ' 사업자번호
            '			INIpay.SetField CLng(PInst), "nm_regist", MEM_BANK_USER  ' 예금주
            '			INIpay.SetField CLng(PInst), "cd_bank", MEM_BANK_CODE  ' 은행코드
            '			INIpay.SetField CLng(PInst), "no_acct", MEM_BANK_NUM  ' 계좌번호
            '			INIpay.SetField CLng(PInst), "amt_supply", INCEN  ' 금액
            '			INIpay.SetField CLng(PInst), "passwd", ""  ' 상점패스워드
            '			INIpay.SetField CLng(PInst), "cl_service", ""  ' 당일지급신청여부
            '
            '			INIpay.SetField CLng(PInst), "test", "false"
            '			INIpay.SetField CLng(PInst), "debug", "true"
            '

            '			'###############################################################################
            '			'# 5. 취소 요청 #
            '			'################
            '			INIpay.StartAction(CLng(PInst))
            '
            '			'###############################################################################
            '			'# 6. 취소 결과 #
            '			'################
            '			ResultCode = INIpay.GetResult(CLng(PInst), "resultcode") '결과코드 ("00"이면 취소성공)
            '			ResultMsg = INIpay.GetResult(CLng(PInst), "resultmsg") '결과내용
            openonk001/onk2008*****
            '
            '			'###############################################################################
            계좌정보요청으로 등록된 정보와 계좌번호 은행 불일치 오류
            INIomr Object
            (
                [reqtype] => REQUEST
                [inipayhome] => /var/www/html/orderstation-shop/shop/inicis
                [id_merchant] => openonk001
                [id_mall] => jeon7238
                [cl_id] =>
                [no_comp] => 1313677253
                [nm_comp] =>
                [nm_regist] => 조혜전
                [cd_bank] => 81
                [no_acct] => 35291027203207
                [no_tel] =>
                [passwd] => onk2008*****
                [cl_gubun] =>
                [dt_pay] => 20181023
                [amt_supply] => 3137
                [cl_service] =>
                [nm_return] =>
                [id_buyer] =>
                [m_serviceurl] =>
                [m_resultcode] =>
                [m_resultmsg] =>
                [debug] => true
            )

            */

            $iniomr = new INIomr;
            $iniomr->reqtype = "REQUEST";
            $iniomr->inipayhome = "/var/www/html/orderstation-shop/shop/inicis";
            $iniomr->id_merchant = "openonk001";
            $iniomr->id_mall = $mb_id1;
            $iniomr->dt_pay = $req_date;
            $iniomr->no_comp = $pharm_number;
            $iniomr->nm_regist = iconv("utf8", "euckr", $pharm_bank_user);
            $iniomr->cd_bank = $pharm_bank_code;
            $iniomr->no_acct = $pharm_bank_num;
            $iniomr->amt_supply = $incen;
            $iniomr->passwd = "onk2008*****";
            $iniomr->cl_service = "";
            //$iniomr->test = "true";
            $iniomr->debug = "true";
            //print_r($iniomr);
            //exit;

            /****************
             * 4. 승인 요청 *
             ****************/
            $iniomr->startAction();
            /****************************************************************
             * 5. 요청 결과                                            *
             ****************************************************************/
            $res_cd = $iniomr->getResultCode();
            $res_msg = $iniomr->getResultMsg();

            if ($res_cd != '00') {
                //alert(' 이니시스 확인중입니다. 잠시만 기다려 주세요. ');
                //alert(iconv_utf8($res_msg).' 코드 : '.$res_cd);
                echo iconv_utf8($res_msg);
                echo '<br>';
                echo $pharm_name;
                exit;
            } else {

                $sql = "update tbl_shop_order_receiver a inner join tbl_shop_order b on a.od_id = b.od_id 	
						  set a.od_calculate_yn = 'Y' 
						where a.od_status != '취소'
						  and b.od_gubun = '3' 
						  and date_format(a.od_date1, '%Y%m') = '".$yearmonth."'
						  and b.pharm_custno = '$pharm_custno'";

                sql_query($sql, true);
                $sql = "insert into tbl_pharm_calculate(mb_id, cal_price) values ('$mb_id1','$incen')";
                sql_query($sql, true);


            }
        }

    }

}

$qstr1 = "token=$token&amp;list_year=$list_year&amp;list_month=$list_month&amp;sfl=$sfl&amp;stx=$stx&amp;search=$search&amp;save_search=$search";
$qstr = "$qstr1&amp;sort1=$sort1&amp;sort2=$sort2&amp;page=$page";

goto_url('./member_list12.php?'.$qstr);

?>

<?php
$sub_menu = "200150";
include_once('./_common.php');

auth_check($auth[$sub_menu], 'r');



if(! preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $fr_date) ) $fr_date = '';
if(! preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $to_date) ) $to_date = '';
if($fr_date =='') $fr_date = date("Y-m-d", strtotime('-1 year'));
if($to_date =='') $to_date = date("Y-m-d");



$sql_common = " from {$g5['member_table']} a ";

$sql_search = " where (1) ";
if ($stx) {
    $sql_search .= " and ( ";
    switch ($sfl) {
        
        case 'mb_name' :
        case 'mb_id' :
            $sql_search .= " ({$sfl} like '%{$stx}%') ";
            break;
		case 'pharm_name' :
            $sql_search .= " ({$sfl} like '%{$stx}%') ";
            break;
        default :
            $sql_search .= " ({$sfl} like '{$stx}%') ";
            break;
    }
    $sql_search .= " ) ";
}


    $sql_search .= " and mb_type = '9' ";



if (!$sst) {
    $sst = "mb_datetime";
    $sod = "desc";
}

$sql_order = " order by {$sst} {$sod} ";

$sql = " select count(*) as cnt {$sql_common} {$sql_search} {$sql_order} ";
$row = sql_fetch($sql);
$total_count = $row['cnt'];

$rows = $config['cf_page_rows'];
$total_page  = ceil($total_count / $rows);  // 전체 페이지 계산
if ($page < 1) $page = 1; // 페이지가 없으면 첫 페이지 (1 페이지)
$from_record = ($page - 1) * $rows; // 시작 열을 구함


$listall = '<a href="'.$_SERVER['SCRIPT_NAME'].'" class="ov_listall">전체목록</a>';

$g5['title'] = '관리자관리';
include_once('./admin.head.php');

$sql = " select a.*
		{$sql_common} {$sql_search} {$sql_order} limit {$from_record}, {$rows} ";
//echo $sql;

$result = sql_query($sql);

$colspan = 6;
?>

<div class="local_ov01 local_ov">
    <?php echo $listall ?>
    <span class="btn_ov01"><span class="ov_txt">총회원수 </span><span class="ov_num"> <?php echo number_format($total_count) ?>명 </span></span>
   
</div>

<form id="fsearch" name="fsearch" class="local_sch01 local_sch" method="get">


<div class="local_sch03  tbl_wrap" style="padding:10px;">
    <table>
   
    <tbody>
	
	<tr>
        
		
   
		<th scope="row">검색</th>
        <td>
            <select name="sfl" id="sfl" >
			
			<option value="mb_name" <?php echo get_selected($sfl, 'mb_name'); ?>>이름</option>
			<option value="mb_id" <?php echo get_selected($sfl, 'mb_id'); ?>>아이디</option>
			
			
			</select>
			<label for="stx" class="sound_only">검색어</label>
			<input type="text" name="stx" value="<?php echo $stx ?>" id="stx" class=" frm_input">
			<!-- <input type="submit" class="btn_submit" value="검색"> -->
        </td>
		<td >
			 <input type="submit" value="검색" class="get_theme_confc btn btn_01">

		</td>
		
    </tr>
	
    </tbody>
    </table>
</div>

<div class="btn_fixed_top">  
    <?php if ($is_admin == 'super') { ?>
    <a href="./member_form_admin.php" id="member_add" class="btn btn_01">회원추가</a>
    <?php } ?>
</div>

</form>

<!-- <div class="local_desc01 local_desc">
    <p>
        회원자료 삭제 시 다른 회원이 기존 회원아이디를 사용하지 못하도록 회원아이디, 이름, 닉네임은 삭제하지 않고 영구 보관합니다.
    </p>
</div> -->


<form name="fmemberlist" id="fmemberlist" action="" onsubmit="return fmemberlist_submit(this);" method="post">
<input type="hidden" name="sst" value="<?php echo $sst ?>">
<input type="hidden" name="sod" value="<?php echo $sod ?>">
<input type="hidden" name="sfl" value="<?php echo $sfl ?>">
<input type="hidden" name="stx" value="<?php echo $stx ?>">
<input type="hidden" name="page" value="<?php echo $page ?>">
<input type="hidden" name="token" value="">

<div class="tbl_head01 tbl_wrap">
    <table>
    <caption><?php echo $g5['title']; ?> 목록</caption>
    <thead>
    <tr>
        <th scope="col" id="mb_list_id" >아이디</th>
		<th scope="col" id="mb_list_name">이름</th>
        <th scope="col" id="mb_list_nick">연락처</th>		
      
        <th scope="col" id="mb_list_lastcall">최종로그인</a></th>
		<th scope="col" id="mb_list_lastcall">가입일</a></th>
		<th scope="col" id="mb_list_mng">관리</th>
    </tr>
    </thead>
    <tbody>
    <?php
    for ($i=0; $row=sql_fetch_array($result); $i++) {

        // 접근가능한 그룹수
        $sql2 = " select count(*) as cnt from {$g5['group_member_table']} where mb_id = '{$row['mb_id']}' ";
        $row2 = sql_fetch($sql2);
        $group = '';
        if ($row2['cnt'])
            $group = '<a href="./boardgroupmember_form.php?mb_id='.$row['mb_id'].'">'.$row2['cnt'].'</a>';

        if ($is_admin == 'group') {
            $s_mod = '';
        } else {
            $s_mod = '<a href="./member_form_admin.php?'.$qstr.'&amp;w=u&amp;mb_id='.$row['mb_id'].'" class="btn btn_03">수정</a>';
        }
        $s_grp = '<a href="./boardgroupmember_form.php?mb_id='.$row['mb_id'].'" class="btn btn_02">그룹</a>';

        $leave_date = $row['mb_leave_date'] ? $row['mb_leave_date'] : date('Ymd', G5_SERVER_TIME);
        $intercept_date = $row['mb_intercept_date'] ? $row['mb_intercept_date'] : date('Ymd', G5_SERVER_TIME);

        $mb_id = $row['mb_id'];

    
        $bg = 'bg'.($i%2);

		$mb_id           = $row["mb_id"];
		$mb_name         = $row["mb_name"];
		$mb_hp          = $row["mb_hp"];
		$mb_datetime          = substr($row["mb_datetime"], 0, 10);
		$mb_today_login          = substr($row["mb_today_login"], 0, 10);
    ?>

    <tr class="<?php echo $bg; ?>">

		<td align="center"><?php echo $mb_id; ?></td>
		<td align="center"><?php echo $mb_name; ?></td>
		<td align="center"><?php echo $mb_hp; ?></td>

		<td align="center"><?php echo $mb_today_login; ?></td>
		<td align="center"><?php echo $mb_datetime; ?></td>
		
		<td width="150">
			<?php echo $s_mod ?>
			
		</td>

	</tr>

    <?php
    }
    if ($i == 0)
        echo "<tr><td colspan=\"".$colspan."\" class=\"empty_table\">자료가 없습니다.</td></tr>";
    ?>
    </tbody>
    </table>
</div>


</form>

<?php 
//$qstr ='mb_type='.$_GET['mb_type'].'&'.$qstr;
echo get_paging(G5_IS_MOBILE ? $config['cf_mobile_pages'] : $config['cf_write_pages'], $page, $total_page, '?'.$qstr.'&amp;page='); ?>

<script>
function fmemberlist_submit(f)
{
    if (!is_checked("chk[]")) {
        alert(document.pressed+" 하실 항목을 하나 이상 선택하세요.");
        return false;
    }

    if(document.pressed == "선택삭제") {
        if(!confirm("선택한 자료를 정말 삭제하시겠습니까?")) {
            return false;
        }
    }

    return true;
}
function fnLogin(id) {
	if(confirm(id+" 로 로그인하시겠습니까?")) {
		location.href='/bbs/login_check_admin.php?mb_id=' + id;
	}
}
</script>

<?php
include_once ('./admin.tail.php');
?>

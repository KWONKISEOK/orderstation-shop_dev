<?php
$sub_menu = '300830';
include_once('./_common.php');
/*
if ($w == "u" || $w == "d")
    check_demo();

if ($w == 'd')
    auth_check($auth[$sub_menu], "d");
else
    auth_check($auth[$sub_menu], "w");

check_admin_token();
*/

@mkdir(G5_DATA_PATH."/vender", G5_DIR_PERMISSION);
@chmod(G5_DATA_PATH."/vender", G5_DIR_PERMISSION);

$wr_file      = $_FILES['wr_file']['tmp_name'];
$wr_file_name = $_FILES['wr_file']['name'];


//파일이 이미지인지 체크합니다.
if( $wr_file || $wr_file_name ){

    if( !preg_match('/\.(gif|jpe?g|bmp|png|xls|xlsx|doc|docx|ppt|pptx|pdf|psd|zip)$/i', $wr_file_name) ){
        alert("업로드할수 없는 파일형식 입니다.");
    }

}

if ($w == "" || $w == "u")
{

	if($_FILES['wr_file']['name'] != "")
	{
		$origin_filename = $_FILES['wr_file']['name'];
		$file_array=explode(".",$_FILES['wr_file']['name']);
		$file_ext1 = array_pop($file_array);

		$trans_filename = date("ymdHis",time()) . "." . $file_ext1;

		if(file_exists(G5_DATA_PATH."/vender" . "/". $trans_filename)) { alert("파일명을 바꾼후 등록해주세요."); exit; }

		if(!move_uploaded_file($_FILES['wr_file']['tmp_name'], G5_DATA_PATH."/vender" . "/". $trans_filename)) 
		{ 
			alert("파일 등록에 실패하였습니다."); 
		}

	}

	if( !empty($wr_id) && !empty($wr_file_del) && $wr_file_del == "Y" ){

		$sql = " select * from tbl_write_vender where wr_id = '$wr_id' ";
		$wr_row = sql_fetch($sql);

		@unlink(G5_DATA_PATH."/vender/{$wr_row['wr_2']}");

		$add_sql = "
				wr_1				= '' , 
				wr_2				= '' , 
		";


	}else{
	
		$wr_file_del = "N";

		if($_FILES['wr_file']['name'] != ""){

			$add_sql = "
					wr_1				= '". $origin_filename ."' , 
					wr_2				= '". $trans_filename ."' , 
			";

		}


	}

}

$sql_common = " wr_subject          = '$wr_subject',
                wr_content          = '$wr_content',
                mb_id				= '{$member['mb_id']}',
                wr_name             = '{$member['mb_name']}', 
				".$add_sql."
				wr_3				= '". $wr_file_del ."' , 
				wr_4				= '". $wr_4 ."' 
				";
				

if ($w == "")
{
    $sql = " insert tbl_write_vender
                set $sql_common ";
    sql_query($sql);

	$wr_id = sql_insert_id();

}
else if ($w == "u")
{
    $sql = " update tbl_write_vender
                set $sql_common
              where wr_id = '$wr_id' ";
    sql_query($sql);
}
else if ($w == "d")
{
    $sql = " delete from tbl_write_vender where wr_id = '$wr_id' ";
    sql_query($sql);

	if( $member["mb_type"] == "9" ){

		//공지 읽음여부 다 지워주기 
		$sql2 = " delete from tbl_write_vender_choice where wr_id = '$wr_id' ";
		sql_query($sql2);

	}

}


//*********** 공급사 선택시 처리 **************//

if ($w == "" && $member["mb_type"] == "9"){

	for ($i=0; $i<count($_POST['vender_comp_code']); $i++)
	{
		$sql = " insert into tbl_write_vender_choice(wr_id,wr_comp_code,wr_gubun)values('{$wr_id}',".$_POST['vender_comp_code'][$i].",'C') ";
		sql_query($sql);
	}

}

if ($w == "u" && $member["mb_type"] == "9"){

	//1단계 모두 삭제로 만들기
	$sql = " update tbl_write_vender_choice set wr_del_yn = 'Y' where wr_id = '$wr_id' ";
	sql_query($sql);

	//2단계 넘어온값 중에서 있으면 삭제여부 , 확인여부 , 확인날짜 초기화 
	for ($i=0; $i<count($_POST['vender_comp_code']); $i++)
	{

		$k = $_POST['vender_comp_code'][$i];

		$sql = " select * from tbl_write_vender where wr_id = '$wr_id' and wr_comp_code = '{$k}' ";
		$wr_row = sql_fetch($sql);

		if( $wr_row['wr_id'] > 0 ){

			$sql = " update tbl_write_vender_choice set 
						set wr_del_yn = 'N' , wr_confirm_yn = 'N' , wr_confirm_time = null
						where wr_id = '{$wr_id}' and wr_comp_code = '{$k}' ";

			sql_query($sql);

		}else{
		
			$sql = " insert into tbl_write_vender_choice(wr_id,wr_comp_code,wr_gubun)values('{$wr_id}','{$k}','C') ";
			sql_query($sql);
		
		}

	}

	//2단계에서 삭제여부가 N으로 해당되지 않은것들 지워주기.
	$sql = " delete from tbl_write_vender_choice where wr_del_yn = 'Y' and wr_id = '$wr_id' ";
	sql_query($sql);

}



//****************************************//

goto_url("./venderlist.php");
?>

<?php
$sub_menu = "200200";
include_once('./_common.php');

auth_check($auth[$sub_menu], 'w');

if ($w == '') {
    $required_mb_id = 'required';
    $required_mb_id_class = 'required alnum_';
    $required_mb_password = 'required';
    $sound_only = '<strong class="sound_only">필수</strong>';

    $mb['mb_mailling'] = 1;
    $mb['mb_open'] = 1;
    $mb['mb_level'] = $config['cf_register_level'];
    $html_title = '추가';

    $mb['mb_type'] = 1;

    $comp_trans_compare = 0;
    $comp_trans_price = 0;
    $comp_trans_use = 0;

}

// 본인확인방법
switch ($mb['mb_certify']) {
    case 'hp':
        $mb_certify_case = '휴대폰';
        $mb_certify_val = 'hp';
        break;
    case 'ipin':
        $mb_certify_case = '아이핀';
        $mb_certify_val = 'ipin';
        break;
    case 'admin':
        $mb_certify_case = '관리자 수정';
        $mb_certify_val = 'admin';
        break;
    default:
        $mb_certify_case = '';
        $mb_certify_val = 'admin';
        break;
}

// 본인확인
$mb_certify_yes = $mb['mb_certify'] ? 'checked="checked"' : '';
$mb_certify_no = !$mb['mb_certify'] ? 'checked="checked"' : '';

// 성인인증
$mb_adult_yes = $mb['mb_adult'] ? 'checked="checked"' : '';
$mb_adult_no = !$mb['mb_adult'] ? 'checked="checked"' : '';

//메일수신
$mb_mailling_yes = $mb['mb_mailling'] ? 'checked="checked"' : '';
$mb_mailling_no = !$mb['mb_mailling'] ? 'checked="checked"' : '';

// SMS 수신
$mb_sms_yes = $mb['mb_sms'] ? 'checked="checked"' : '';
$mb_sms_no = !$mb['mb_sms'] ? 'checked="checked"' : '';

// 정보 공개
$mb_open_yes = $mb['mb_open'] ? 'checked="checked"' : '';
$mb_open_no = !$mb['mb_open'] ? 'checked="checked"' : '';

if ($mb['mb_intercept_date']) $g5['title'] = "차단된 ";
else $g5['title'] .= "";
$g5['title'] .= '프로모션 회원 '.$html_title;
include_once('./admin.head.php');

// add_javascript('js 구문', 출력순서); 숫자가 작을 수록 먼저 출력됨
add_javascript(G5_POSTCODE_JS, 0);    //다음 주소 js
?>
<script type="text/javascript" src="https://openapi.map.naver.com/openapi/v3/maps.js?clientId=iQB4Bbn7JFBS2Qb0UVSY&submodules=geocoder"></script>
<form name="fmember" id="fmember" action="./point_member_form_update.php" onsubmit="return fmember_submit(this);" method="post" enctype="multipart/form-data">
    <input type="hidden" name="w" value="<?php echo $w ?>">
    <input type="hidden" name="sfl" value="<?php echo $sfl ?>">
    <input type="hidden" name="stx" value="<?php echo $stx ?>">
    <input type="hidden" name="sst" value="<?php echo $sst ?>">
    <input type="hidden" name="sod" value="<?php echo $sod ?>">
    <input type="hidden" name="page" value="<?php echo $page ?>">
    <input type="hidden" name="token" value="">

    <!-- 가입 정보 HIDDEN VALUE -->
    <input type="hidden" name="mb_homepage" id="mb_homepage" class="frm_input" value="">
    <input type="hidden" name="mb_hp" id="mb_hp" class="frm_input" value="">
    <input type="hidden" name="mb_tel" id="mb_tel" class="frm_input" value="">
    <input type="hidden" name="mb_birth" id="mb_birth" class="frm_input" value="">
    <input type="hidden" name="mb_email" id="mb_email" class="frm_input" value="">
    <input type="hidden" name="mb_certify" id="mb_certify" class="frm_input" value="0">
    <input type="hidden" name="mb_adult" id="mb_adult" class="frm_input" value="0">
    <input type="hidden" name="mb_zip" id="mb_zip" class="frm_input" value="">
    <input type="hidden" name="mb_addr1" id="mb_addr1" class="frm_input" value="">
    <input type="hidden" name="mb_addr2" id="mb_addr2" class="frm_input" value="">
    <input type="hidden" name="mb_addr3" id="mb_addr3" class="frm_input" value="">
    <input type="hidden" name="mb_addr_jibeon" id="mb_addr_jibeon" class="frm_input" value="">
    <input type="hidden" name="mb_mailling" id="mb_mailling" class="frm_input" value="0">
    <input type="hidden" name="mb_sms" id="mb_sms" class="frm_input" value="0">
    <input type="hidden" name="mb_open" id="mb_open" class="frm_input" value="0">
    <input type="hidden" name="mb_push" id="mb_push" class="frm_input" value="0">
    <input type="hidden" name="pharm_status" value="2   ">


    <div class="tbl_frm01 tbl_wrap">
        <table>
            <caption><?php echo $g5['title']; ?></caption>
            <colgroup>
                <col class="grid_4">
                <col>
            </colgroup>
            <tbody>
            <tr>
                <th scope="row"><label for="mb_id">아이디<?php echo $sound_only ?></label></th>
                <td>
                    <input type="text" name="mb_id" value="" id="mb_id" <?php echo $required_mb_id ?> class="frm_input <?php echo $required_mb_id_class ?>" size="60" maxlength="20">
                </td>
            </tr>
            <tr>
                <th scope="row"><label for="mb_password">비밀번호<?php echo $sound_only ?></label></th>
                <td>
                    <input type="password" name="mb_password" id="mb_password" <?php echo $required_mb_password ?> class="frm_input <?php echo $required_mb_password ?>" size="60" maxlength="20">
                </td>
            </tr>
            <tr>
                <th scope="row"><label for="mb_name">이름(실명)<strong class="sound_only">필수</strong></label></th>
                <td>
                    <input type="text" name="mb_name" value="<?php echo $mb['mb_name'] ?>" id="mb_name" required class="required frm_input" size="60" maxlength="20">
                </td>
            </tr>
            <tr>
                <th scope="row"><label for="mb_nick">닉네임<strong class="sound_only">필수</strong></label></th>
                <td>
                    <input type="text" name="mb_nick" value="<?php echo $mb['mb_nick'] ?>" id="mb_nick" required class="required frm_input" size="60" maxlength="20">
                </td>
            </tr>
            <tr>
                <th scope="row"><label for="mb_level">회원 권한</label></th>
                <td><?php echo get_member_level_select('mb_level', 1, 1, 1) ?></td>
            </tr>
            <tr>
                <th scope="row"><font color="#ff0000">*</font>회원구분</th>
                <td>
                    <label for="mb_type" class="sound_only">회원구분</label>
                    <select name="mb_type" id="mb_type" style="width:135px">
                        <option value="0"<?php echo get_selected($mb['mb_type'], "0"); ?> selected>일반회원</option>
                    </select>
                </td>
            </tr>
             <tr>
                 <th scope="row"><label for="mb_recom">약국코드</label></th>
                 <td>
                     <select name="mb_recommend" id="mb_recommend" style="width:180px;">
                         <option value="">=약국명(약국코드)=</option>
                         <?php echo get_pharm_list('', '');?>
                     </select>
                 </td>
             </tr>
            </tbody>
        </table>

        <div id="wrap_list">

        </div>

    </div>

    <div class="btn_fixed_top">
        <a href="javascript:history.go(-1);" class="btn btn_02">목록</a>
        <input type="submit" value="확인" class="btn_submit btn" accesskey='s'>
    </div>
</form>

<link href="./select2.css" rel="stylesheet"/>
<script type="text/javascript" src="./select2.js"></script>
<script>
    $(document).ready(function () {
        $("#mb_recommend").select2();
    });

    $("#mb_type").change(function () {
        fnMbType();
    });

    function fmember_submit(f) {
        return true;
    }


</script>

<div style="display:none;">
    <iframe src="" name="proc_frm" height="0"></iframe>
</div>

<?php
include_once('./admin.tail.php');
?>

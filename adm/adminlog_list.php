<?php
$sub_menu = "100330";
include_once('./_common.php');

auth_check($auth[$sub_menu], 'r');

if (empty($sel_year))
    $sel_year = date('Y');

$sql_common = " from {$g5['admin_log_table']}{$sel_year} ";

$sql_search = " where (1) ";

if ($stx) {
    $sql_search .= " and ( ";
    switch ($sfl) {
        case 'al_admin_id' :
            $sql_search .= " ({$sfl} = '{$stx}') ";
            break;
        default :
            $sql_search .= " ({$sfl} like '%{$stx}%') ";
            break;
    }
    $sql_search .= " ) ";
}

if (empty($fr_date))
    $fr_date = date($sel_year.'-m-d');
if (empty($to_date))
    $to_date = date($sel_year.'-m-d');

if ($fr_date != '' && $to_date != '') {
    $sql_search .= " and al_date between '$fr_date 00:00:00' and '$to_date 23:59:59' ";
}

if (!$sst) {
    $sst = "al_num";
    $sod = "desc";
}
$sql_order = " order by {$sst} {$sod} ";

$sql = " select count(*) as cnt
            {$sql_common}
            {$sql_search}
            {$sql_order} ";
$row = sql_fetch($sql);
$total_count = $row['cnt'];

$rows = $config['cf_page_rows'];
$total_page = ceil($total_count / $rows);  // 전체 페이지 계산
if ($page < 1) $page = 1; // 페이지가 없으면 첫 페이지 (1 페이지)
$from_record = ($page - 1) * $rows; // 시작 열을 구함

$sql = " select *
            {$sql_common}
            {$sql_search}
            {$sql_order}
            limit {$from_record}, {$rows} ";
$result = sql_query($sql);

$listall = '<a href="'.$_SERVER['SCRIPT_NAME'].'" class="ov_listall">전체목록</a>';

$g5['title'] = '관리자로그 관리';

include_once('./admin.head.php');
include_once(G5_PLUGIN_PATH.'/jquery-ui/datepicker.php');

$colspan = 6;

$qstr .= "&amp;fr_date=$fr_date&amp;to_date=$to_date";

?>

<div class="local_ov01 local_ov">
    <?php echo $listall ?>
    <span class="btn_ov01">
        <span class="ov_txt">전체</span>
        <span class="ov_num"> <?php echo number_format($total_count) ?> 건</span>
    </span>
</div>

<form name="fsearch" id="fsearch" class="local_sch01 local_sch" method="get">
    <div class="local_sch03  tbl_wrap" style="padding:10px;">
        <table>

            <tbody>
            <tr>
                <td>
                    <label for="sel_year" class="sound_only">년도선택</label>
                    <select name="sel_year" id="sel_year" onchange="datepicker_set(this);">
                        <option value="2020" <?php echo get_selected($sel_year, "2020"); ?>>2020</option>
                        <option value="2019" <?php echo get_selected($sel_year, "2019"); ?>>2019</option>
                    </select> 년
                    <input type="text" id="fr_date"  name="fr_date" value="<?php echo $fr_date; ?>" class="frm_input" size="10" maxlength="10"> ~
                    <input type="text" id="to_date"  name="to_date" value="<?php echo $to_date; ?>" class="frm_input" size="10" maxlength="10">
                    <button type="button" onclick="javascript:set_date('today');">오늘</button>
                </td>
            </tr>
            <tr>
                <td>
                    <label for="sfl" class="sound_only">검색대상</label>
                    <select name="sfl" id="sfl">
                        <option value="al_admin_id"<?php echo get_selected($_GET['sfl'], "al_admin_id"); ?>>관리자아이디</option>
                        <option value="al_content"<?php echo get_selected($_GET['sfl'], "al_content"); ?>>내용</option>
                    </select>
                    <label for="stx" class="sound_only">검색어</label>
                    <input type="text" name="stx" value="<?php echo $stx ?>" id="stx" class="frm_input">
                    <button type="button" class="get_theme_confc btn btn_01" style="background: #ff4081" onclick="submit_chk();">검색</button>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</form>

<form name="fpointlist" id="fpointlist" method="post" action="./point_list_delete.php" onsubmit="return fpointlist_submit(this);">
    <input type="hidden" name="sst" value="<?php echo $sst ?>">
    <input type="hidden" name="sod" value="<?php echo $sod ?>">
    <input type="hidden" name="sfl" value="<?php echo $sfl ?>">
    <input type="hidden" name="stx" value="<?php echo $stx ?>">
    <input type="hidden" name="page" value="<?php echo $page ?>">
    <input type="hidden" name="token" value="">

    <div class="tbl_head01 tbl_wrap">
        <table>
            <caption><?php echo $g5['title']; ?> 목록</caption>
            <thead>
            <tr>
                <th scope="col"><?php echo subject_sort_link('al_admin_id') ?>관리자 아이디</a></th>
                <th scope="col">1depth</th>
                <th scope="col">2depth</th>
                <th scope="col">작업</th>
                <th scope="col">고유코드</th>
                <th scope="col">IP</th>
                <th scope="col">발생시간</th>
            </tr>
            </thead>
            <tbody>
            <?php
            for ($i = 0; $row = sql_fetch_array($result); $i++) {
                $bg = 'bg'.($i % 2);
                ?>
                <tr class="<?php echo $bg; ?>">
                    <td class="td_center"><?php echo $row['al_admin_id'] ?></td>
                    <td class="td_center"><?php echo $row['al_depth1'] ?></td>
                    <td class="td_center"><?php echo $row['al_depth2'] ?></td>
                    <td class="td_center"><?php echo $row['al_subject'] ?></td>
                    <td class="td_center"><?php echo $row['al_key'] ?></td>
                    <td class="td_center"><?php echo $row['al_ip'] ?></td>
                    <td class="td_datetime"><?php echo $row['al_date'] ?></td>
                </tr>

                <?php
            }

            if ($i == 0)
                echo '<tr><td colspan="'.$colspan.'" class="empty_table">자료가 없습니다.</td></tr>';
            ?>
            </tbody>
        </table>
    </div>

    <div class="btn_fixed_top">
        <!--<input type="submit" name="act_button" value="선택삭제" onclick="document.pressed=this.value" class="btn btn_02">-->
    </div>

</form>

<?php echo get_paging(G5_IS_MOBILE ? $config['cf_mobile_pages'] : $config['cf_write_pages'], $page, $total_page, "{$_SERVER['SCRIPT_NAME']}?$qstr&amp;page="); ?>

<script>
    $(function () {
        //var date = new Date();
        //var now_year = date.getFullYear();
        var now_year = $('#sel_year').val();

        $("#fr_date, #to_date").datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: "yy-mm-dd",
            showButtonPanel: true,
            updateViewDate: true,
            yearRange: (now_year)+":"+now_year
        });
    });

    function datepicker_set () {
        var fr_date = $('#fr_date');
        var to_date = $('#to_date');

        // 년도 변경 시 value 초기화
        fr_date.val('');
        to_date.val('');

        // 선택 년도 변수 설정
        var newYear = $('#sel_year').val();

        // 선택 년도에 따라 기간 설정
        fr_date.datepicker("option", {yearRange: newYear + ":" + newYear});
        fr_date.datepicker('option', 'defaultDate',newYear+'-01-01');
        to_date.datepicker("option", {yearRange: newYear + ":" + newYear});
        to_date.datepicker('option', 'defaultDate',newYear+'-01-01');
    }

    function submit_chk() {
        var fr_date = $('#fr_date');
        var to_date = $('#to_date');

        // 날짜 빈값 체크
        if (fr_date.val() != '' && to_date.val() == '') {
            alert('시작 날짜를 선택해주세요.');
            return false;
        } else if (fr_date.val() == '' && to_date.val() != '') {
            alert('끝 날짜를 선택해주세요.');
            return false;
        }

        // 날짜 기간 오류 체크
        var sdate = new Date(fr_date.val());
        var edate = new Date(to_date.val());
        if (edate - sdate < 0) {
            alert("끝 날짜가 시작 날짜보다 이전일수 없습니다");
            return false;
        }

        $('#fsearch').submit();
    }

    function set_date ($type) {
        if ($type == 'today') {
            document.getElementById("fr_date").value = "<?php echo G5_TIME_YMD; ?>";
            document.getElementById("to_date").value = "<?php echo G5_TIME_YMD; ?>";
        }
    }

</script>

<?php
include_once('./admin.tail.php');
?>

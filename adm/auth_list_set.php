<?php
include_once('./_common.php');
include_once(G5_PATH . '/head.sub.php');


global $menu;
foreach ($amenu as $key => $value) {
    for ($i = 1; $i < count($menu['menu' . $key]); $i++) {
        if ($_REQUEST["menu"] == '100' && !($i==1 || $i==3)) {
            $auth_menu[$menu['menu100'][$i][0]] = $menu['menu100'][$i][1];
            $title = "환경설정";
        } else if ($_REQUEST["menu"] == '200') {
            $auth_menu[$menu['menu200'][$i][0]] = $menu['menu200'][$i][1];
            $title = "회원관리";
        } else if ($_REQUEST["menu"] == '300') {
            $auth_menu[$menu['menu300'][$i][0]] = $menu['menu300'][$i][1];
            $title = "게시판관리";
        } else if ($_REQUEST["menu"] == '400' &&  !($i >= 14)) {
            $auth_menu[$menu['menu400'][$i][0]] = $menu['menu400'][$i][1];
            $title = "쇼핑몰관리";
        } else if ($_REQUEST["menu"] == '500') {
            $auth_menu[$menu['menu500'][$i][0]] = $menu['menu500'][$i][1];
            $title = "실적관리";
        } else if ($_REQUEST["menu"] == '600' && !($i >= 11)) {
            $auth_menu[$menu['menu600'][$i][0]] = $menu['menu600'][$i][1];
            $title = "상품관리";
        } else if ($_REQUEST["menu"] == '700') {
            $auth_menu[$menu['menu700'][$i][0]] = $menu['menu700'][$i][1];
            $title = "기타관리";
        } else if ($_REQUEST["menu"] == '900') {
            $auth_menu[$menu['menu900'][$i][0]] = $menu['menu900'][$i][1];
            $title = "SMS관리";
        }
    }
}

$mb_id = $_REQUEST["mb_id"];
$au_menu = $_REQUEST["menu"];

$sql_common = " from {$g5['auth_table']} a left join {$g5['member_table']} b on (a.mb_id=b.mb_id) ";
$sql_search = " where a.mb_id = '$mb_id'and a.au_menu like '$au_menu%'";
$sql_group = " group by a.au_menu";

$sql = "select a.mb_id, a.au_menu, a.au_auth
             {$sql_common}
             {$sql_search}";
$result = sql_fetch_result($sql);

?>

<script src="<?php echo G5_ADMIN_URL ?>/admin.js"></script>
<form name="fauthlist2" id="fauthlist2" action="./auth_list_update.php" method="post">
    <input type="hidden" name="menu" value="<?php echo $au_menu ?>">
    <div class="tbl_head01 tbl_wrap">

        <table>
            <thead>
            <tr>
                <th scope="col" colspan="5">
                    <?php echo $title; ?>
                    <input type="hidden" name="mb_id" value="<?php echo $mb_id ?>" id="mb_id" required
                           class="required frm_input">
                </th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td class="tg-0lax"></td>
                <td class="tg-0lax"></td>
                <td class="tg-baqh">r(읽기)</td>
                <td class="tg-baqh">w(쓰기)</td>
                <td class="tg-baqh">d(삭제)</td>
            </tr>

            <?php
            foreach ($auth_menu as $key => $value) {

                if (!(substr($key, -3) == '000' || $key == '-' || !$key)) {

                    $chk = "";
                    foreach ($result as $v) {
                        if ($key == $v['au_menu']) {
                            $chk = $v['au_auth'];
                        }
                    }
                    ?>
                    <tr>
                        <td>
                            <input type="checkbox" onclick="chk(this)" <?php echo (adm_auth_check($key, $mb_id) === "b") ? "checked" : " "?> >
                        </td>
                        <td class="tg-0lax">
                            <?php
                            echo $key . ' ' . $value;
                            ?>
                            <input type="hidden" name="au_menu[]" id="au_menu" value="<?php echo $key ?>">
                        </td>
                        <td class="tg-0lax">
                            <input type="checkbox" name="r[]" value="1" id="r" <?php echo (strpos($chk, 'r') !== false) ? 'checked' : '' ?>>
                            <input type="hidden" name="r_hidden[]" class="r_hidden" value="">
                        </td>
                        <td class="tg-0lax">
                            <input type="checkbox" name="w[]" value="1" id="w" <?php echo (strpos($chk, 'w') !== false) ? 'checked' : '' ?>>
                            <input type="hidden" name="w_hidden[]" class="w_hidden" value="">
                        </td>
                        <td class="tg-0lax">
                            <input type="checkbox" name="d[]" value="1" id="d" <?php echo (strpos($chk, 'd') !== false) ? 'checked' : '' ?>>
                            <input type="hidden" name="d_hidden[]" class="d_hidden" value="">
                        </td>
                    </tr>
                <?php }
            }
            ?>
            </tbody>
        </table>
        <div class="btn_list01 btn_list" style="width: 130px; margin: 10px auto;">
            <input type="button" id="value_btn" value="설정완료" class="btn btn_03" onclick="frm();">&nbsp;
            <input type="button" name="btn" value="닫기" onClick='window.close()' class="btn btn_02">
        </div>
        <div class="col-lg-12" id="ex3_Result1"></div>
    </div>
</form>

<script>
    // 권한 체크 되어있으면 value = 1 , 아니면 0.
    function frm() {

        var f = $('#fauthlist2');

        $("input:checkbox[name^=r]").each(function (i, v) {
            r_hidden = $(v).siblings('.r_hidden');

            if ($(v).is(":checked") !== false) {
                r_hidden.val(1);
            } else {
                r_hidden.val(0);
            }
        });

        $("input:checkbox[name^=w]").each(function (i, v) {
            w_hidden = $(v).siblings('.w_hidden');

            if ($(v).is(":checked") !== false) {
                w_hidden.val(1);
            } else {
                w_hidden.val(0);
            }
        });

        $("input:checkbox[name^=d]").each(function (i, v) {
            d_hidden = $(v).siblings('.d_hidden');

            if ($(v).is(":checked") !== false) {
                d_hidden.val(1);
            } else {
                d_hidden.val(0);
            }
        });

        var form_data = f.serialize();
        $.ajax({
            type: "POST",
            url: g5_admin_url+"/auth_list_update.php",
            data: form_data,
            async: false,
            success: function () {
                opener.location.reload(true);
                location.reload();
            }
        });
    }

    //r,w,d 전체 체크
    function chk(v){
        $(v).closest('tr').find('[name^=r]').prop('checked', v.checked);
        $(v).closest('tr').find('[name^=w]').prop('checked', v.checked);
        $(v).closest('tr').find('[name^=d]').prop('checked', v.checked);
    }

</script>
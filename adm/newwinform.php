<?php
$sub_menu = '100310';
include_once('./_common.php');
include_once(G5_EDITOR_LIB);

auth_check($auth[$sub_menu], "w");

$nw_id = preg_replace('/[^0-9]/', '', $nw_id);

$html_title = "팝업레이어";

if ($w == "u") {
    $html_title .= " 수정";
    $sql = " select * from {$g5['new_win_table']} where nw_id = '$nw_id' ";
    $nw = sql_fetch($sql);
    if (!$nw['nw_id']) alert("등록된 자료가 없습니다.");
} else {
    $html_title .= " 입력";
    $nw['nw_device'] = 'both';
    $nw['nw_disable_hours'] = 24;
    $nw['nw_left'] = 10;
    $nw['nw_top'] = 10;
    $nw['nw_width'] = 450;
    $nw['nw_height'] = 500;
    $nw['nw_content_html'] = 2;
}

$g5['title'] = $html_title;
include_once (G5_ADMIN_PATH.'/admin.head.php');
?>

<form name="frmnewwin" action="./newwinformupdate.php" onsubmit="return frmnewwin_check(this);" method="post">
<input type="hidden" name="w" value="<?php echo $w; ?>">
<input type="hidden" name="nw_id" value="<?php echo $nw_id; ?>">
<input type="hidden" name="token" value="">

<div class="local_desc01 local_desc">
    <p>초기화면 접속 시 자동으로 뜰 팝업레이어를 설정합니다.</p>
    <p>PC버전에서 노출되는 팝업은 최대 3개입니다. 등록 시 참고해주세요.</p>
    <p>Mobile 버전에서 노출되는 팝업의 갯수는 제한이 없습니다. 슬라이드 순서는 노출순서 값이 낮은 팝업부터 적용됩니다.</p>
</div>

<div class="tbl_frm01 tbl_wrap">
    <table>
    <caption><?php echo $g5['title']; ?></caption>
    <colgroup>
        <col class="grid_4">
        <col>
    </colgroup>
    <tbody>
    <tr style="display:none;">
        <th scope="row"><label for="nw_division">구분</label></th>
        <td>
            <?php echo help("커뮤니티에 표시될 것인지 쇼핑몰에 표시될 것인지를 설정합니다."); ?>
            <select name="nw_division" id="nw_division">
                <option value="both"<?php echo get_selected($nw['nw_division'], 'both', true); ?>>커뮤니티와 쇼핑몰</option>
                <option value="comm"<?php echo get_selected($nw['nw_division'], 'comm'); ?>>커뮤니티</option>
                <option value="shop"<?php echo get_selected($nw['nw_division'], 'shop'); ?>>쇼핑몰</option>
            </select>
        </td>
    </tr>
    <tr>
        <th scope="row"><label for="nw_device">노출서비스 선택</label></th>
        <td>
            <input type="checkbox" name="nw_service_view_1" value="Y" <?if( $nw['nw_service_view_1'] == "Y" ){?> checked <?}?> />오더스테이션&nbsp;
            <input type="checkbox" name="nw_service_view_2" value="Y" <?if( $nw['nw_service_view_2'] == "Y" ){?> checked <?}?> />이즈브레앱&nbsp;
        </td>
    </tr>
    <tr>
        <th scope="row"><label for="nw_device">노출권한</label></th>
        <td>
            <?php echo help("팝업레이어가 표시될 권한을 설정합니다."); ?>
            <input type="radio" name="nw_type" value="A" <?if( $nw['nw_type'] == "A" || $nw['nw_type'] == "" ){?> checked <?}?> />전체&nbsp;
            <input type="radio" name="nw_type" value="B" <?if( $nw['nw_type'] == "B" ){?> checked <?}?> />일반회원&nbsp;
            <input type="radio" name="nw_type" value="C" <?if( $nw['nw_type'] == "C" ){?> checked <?}?> />약국회원
        </td>
    </tr>
    <tr>
        <th scope="row"><label for="nw_order">노출순서</label></th>
        <td>
            <input type="number" name="nw_order" value="<?php echo ($nw['nw_order']) ? $nw['nw_order'] : 0; ?>" class="frm_input">
        </td>
    </tr>
    <!--
    <tr>
        <th scope="row"><label for="nw_device">팝업 디자인</label></th>
        <td>
            <input type="radio" name="nw_version" value="1" <?if( $nw['nw_version'] == "1" ){?> checked <?}?> />기본&nbsp;
            <input type="radio" name="nw_version" value="2" <?if( $nw['nw_version'] == "2" ){?> checked <?}?> />테두리제거(black)&nbsp;
            <input type="radio" name="nw_version" value="3" <?if( $nw['nw_version'] == "3" ){?> checked <?}?> />테두리제거(white)&nbsp;
        </td>
    </tr>
    -->
    <tr>
        <th scope="row"><label for="nw_device">접속기기</label></th>
        <td>
            <?php echo help("팝업레이어가 표시될 접속기기를 설정합니다."); ?>
            <select name="nw_device" id="nw_device">
                <option value="both"<?php echo get_selected($nw['nw_device'], 'both', true); ?>>PC와 모바일</option>
                <option value="pc"<?php echo get_selected($nw['nw_device'], 'pc'); ?>>PC</option>
                <option value="mobile"<?php echo get_selected($nw['nw_device'], 'mobile'); ?>>모바일</option>
            </select>
        </td>
    </tr>
    <!--
    <tr>
        <th scope="row"><label for="nw_disable_hours">시간<strong class="sound_only"> 필수</strong></label></th>
        <td>
            <?php echo help("고객이 다시 보지 않음을 선택할 시 몇 시간동안 팝업레이어를 보여주지 않을지 설정합니다."); ?>
            <input type="text" name="nw_disable_hours" value="<?php echo $nw['nw_disable_hours']; ?>" id="nw_disable_hours" required class="frm_input required" size="5"> 시간
        </td>
    </tr>
    -->
    <tr>
        <th scope="row"><label for="nw_begin_time">시작일시<strong class="sound_only"> 필수</strong></label></th>
        <td>
            <input type="text" name="nw_begin_time" value="<?php echo $nw['nw_begin_time']; ?>" id="nw_begin_time" required class="frm_input required" size="21" maxlength="19">
            <input type="checkbox" name="nw_begin_chk" value="<?php echo date("Y-m-d 00:00:00", G5_SERVER_TIME); ?>" id="nw_begin_chk" onclick="if (this.checked == true) this.form.nw_begin_time.value=this.form.nw_begin_chk.value; else this.form.nw_begin_time.value = this.form.nw_begin_time.defaultValue;">
            <label for="nw_begin_chk">시작일시를 오늘로</label>
        </td>
    </tr>
    <tr>
        <th scope="row"><label for="nw_end_time">종료일시<strong class="sound_only"> 필수</strong></label></th>
        <td>
            <input type="text" name="nw_end_time" value="<?php echo $nw['nw_end_time']; ?>" id="nw_end_time" required class="frm_input required" size="21" maxlength="19">
            <input type="checkbox" name="nw_end_chk" value="<?php echo date("Y-m-d 23:59:59", G5_SERVER_TIME+(60*60*24*7)); ?>" id="nw_end_chk" onclick="if (this.checked == true) this.form.nw_end_time.value=this.form.nw_end_chk.value; else this.form.nw_end_time.value = this.form.nw_end_time.defaultValue;">
            <label for="nw_end_chk">종료일시를 오늘로부터 7일 후로</label>
        </td>
    </tr>
    <!-- 20210219 팝업 디자인 리뉴얼로 미사용 처리 -->
    <!--<tr>
        <th scope="row"><label for="nw_left">팝업레이어 좌측 위치<strong class="sound_only"> 필수</strong></label></th>
        <td>
           <input type="text" name="nw_left" value="<?php echo $nw['nw_left']; ?>" id="nw_left" required class="frm_input required" size="5"> px
        </td>
    </tr>
    <tr>
        <th scope="row"><label for="nw_top">팝업레이어 상단 위치<strong class="sound_only"> 필수</strong></label></th>
        <td>
            <input type="text" name="nw_top" value="<?php echo $nw['nw_top']; ?>" id="nw_top" required class="frm_input required"  size="5"> px
            &nbsp; ※ 모바일 추천 값 : 160px (단, 적용하려는 팝업 위치에 따라 다름)
        </td>
    </tr>
    <tr>
        <th scope="row"><label for="nw_top_2">팝업레이어 하단 위치</label></th>
        <td>
            <input type="text" name="nw_top_2" value="<?php echo $nw['nw_top_2']; ?>" id="nw_top_2" required class="frm_input required"  size="5"> px
            &nbsp; ※ 팝업 디자인이 기본이 아닌 경우 입력
        </td>
    </tr>
    <tr>
        <th scope="row"><label for="nw_width">팝업레이어 넓이<strong class="sound_only"> 필수</strong></label></th>
        <td>
            <input type="text" name="nw_width" value="<?php echo $nw['nw_width'] ?>" id="nw_width" required class="frm_input required" size="5"> px
            &nbsp; ※ 모바일 추천 값 : 315px
        </td>
    </tr>
    <tr>
        <th scope="row"><label for="nw_height">팝업레이어 높이<strong class="sound_only"> 필수</strong></label></th>
        <td>
            <input type="text" name="nw_height" value="<?php echo $nw['nw_height'] ?>" id="nw_height" required class="frm_input required" size="5"> px
        </td>
    </tr>-->
    <!-- 20210219 팝업 디자인 리뉴얼로 미사용 처리 -->
    <tr>
        <th scope="row"><label for="nw_subject">팝업 제목<strong class="sound_only"> 필수</strong></label></th>
        <td>
            <input type="text" name="nw_subject" value="<?php echo stripslashes($nw['nw_subject']) ?>" id="nw_subject" required class="frm_input required" size="80">
        </td>
    </tr>
    <tr>
        <th scope="row"><label for="nw_href">팝업 링크</label></th>
        <td>
            <input type="text" name="nw_href" value="<?php echo stripslashes($nw['nw_href']) ?>" id="nw_href" required class="frm_input required" size="80">
        </td>
    </tr>
    <tr>
        <th scope="row"><label for="nw_content">내용</label></th>
        <td><?php echo editor_html('nw_content', get_text($nw['nw_content'], 0)); ?></td>
    </tr>
    </tbody>
    </table>
</div>

<div class="btn_fixed_top">
    <a href="./newwinlist.php" class=" btn btn_02">목록</a>
    <input type="submit" value="확인" class="btn_submit btn" accesskey="s">
</div>
</form>

<script>
function frmnewwin_check(f)
{
    errmsg = "";
    errfld = "";

    if (f.nw_service_view_1.checked == false && f.nw_service_view_2.checked == false) {
        alert('노출서비스를 선택하세요.');
        return false;
    }

    <?php echo get_editor_js('nw_content'); ?>

    check_field(f.nw_subject, "제목을 입력하세요.");

    if (errmsg != "") {
        alert(errmsg);
        errfld.focus();
        return false;
    }
    return true;
}
</script>

<?php
include_once (G5_ADMIN_PATH.'/admin.tail.php');
?>

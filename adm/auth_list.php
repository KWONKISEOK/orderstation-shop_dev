<?php
$sub_menu = "100200";
include_once('./_common.php');

if ($is_admin != 'super')
    alert('최고관리자만 접근 가능합니다.');

$sql_common = " from {$g5['member_table']} a";
$sql_join = "left join {$g5['auth_table']} b on (a.mb_id=b.mb_id) ";

$sql_search = " where (1) ";
if ($stx) {
    $sql_search .= " and ( ";
    switch ($sfl) {
        default :
            $sql_search .= " ({$sfl} like '%{$stx}%') ";
            break;
    }
    $sql_search .= " ) ";
}
    $sql_search .= " and mb_type = '9' and !(a.mb_id='admin') ";


if (!$sst) {
    $sst = "mb_datetime";
    $sod = "desc";
}
$sql_group = " group by a.mb_id";

$sql_order = " order by $sst $sod ";


$sql = " select count(*) as cnt
            {$sql_common}
            {$sql_search}
            {$sql_order} ";
$row = sql_fetch($sql);
$total_count = $row['cnt'];


$rows = $config['cf_page_rows'];
$total_page  = ceil($total_count / $rows);  // 전체 페이지 계산
if ($page < 1) $page = 1; // 페이지가 없으면 첫 페이지 (1 페이지)
$from_record = ($page - 1) * $rows; // 시작 열을 구함

$sql = " select a.mb_id, a.mb_nick, a.mb_name, b.au_menu, b.au_auth
            {$sql_common}
            {$sql_join}
            {$sql_search}
            {$sql_group}
            {$sql_order}
            limit {$from_record}, {$rows} ";

$result = sql_query($sql);

$listall = '<a href="'.$_SERVER['SCRIPT_NAME'].'" class="ov_listall btn_ov02">전체목록</a>';

$g5['title'] = "관리권한설정";
include_once('./admin.head.php');

$colspan = 11;
?>

<div class="local_ov01 local_ov">
    <?php echo $listall ?>
    <span class="btn_ov01"><span class="ov_txt">총회원수</span><span class="ov_num"><?php echo number_format($total_count) ?>건</span></span>
</div>

<form name="fsearch" id="fsearch" class="local_sch01 local_sch" method="get">
<input type="hidden" name="sfl" value="a.mb_id" id="sfl">

<label for="stx" class="sound_only">회원아이디<strong class="sound_only"> 필수</strong></label>
<input type="text" name="stx" value="<?php echo $stx ?>" id="stx" required class="required frm_input">
<input type="submit" value="검색" id="fsearch_submit" class="btn_submit">

</form>
<input type="hidden" name="sst" value="<?php echo $sst ?>">
<input type="hidden" name="sod" value="<?php echo $sod ?>">
<input type="hidden" name="sfl" value="<?php echo $sfl ?>">
<input type="hidden" name="stx" value="<?php echo $stx ?>">
<input type="hidden" name="page" value="<?php echo $page ?>">
<input type="hidden" name="token" value="">
<div class="tbl_head01 tbl_wrap">
    <table>
    <caption><?php echo $g5['title']; ?> 목록</caption>
    <thead>
    <tr>
        <th scope="col"><?php echo subject_sort_link('a.mb_id') ?>회원아이디</a></th>
        <th scope="col"><?php echo subject_sort_link('mb_nick') ?>이름</a></th>
        <th scope="col">환경설정</th>
        <th scope="col">회원관리</th>
        <th scope="col">게시판관리</th>
        <th scope="col">쇼핑몰관리</th>
        <th scope="col">실적관리</th>
        <th scope="col">상품관리</th>
        <th scope="col">기타관리</th>
        <th scope="col">SMS관리</th>
        <th  scope="col"></th>
    </tr>
    </thead>
    <tbody>
    <?php
    $count = 0;

    for ($i=0; $row=sql_fetch_array($result); $i++)
    {
        $mb_nick = get_sideview($row['mb_id'], $row['mb_name'], $row['mb_email'], $row['mb_homepage']);

        $bg = 'bg'.($i%2);
    ?>
    <tr class="<?php echo $bg; ?>">
        <td class="td_mbid">
            <?php echo $row['mb_id'] ?></a></td>
        <td class="td_auth_mbnick" style="text-align:left;"><?php echo $mb_nick ?></td>
        <td class="td_Preferences">
            <input type="checkbox" name="menu100" value="<?php echo $row['mb_id'] ?>" id="menu100" <?php echo (adm_auth_check('100', $row['mb_id']) == true) ? "checked" : " "?> disabled="true">
            <input type="button"  class="bt btn_04" name="menu100" value="설정" onclick="auth_pop('100', '<?php echo $row['mb_id'] ?>')"></td>
        <td class="td_Preferences">
            <input type="checkbox" name="menu200" value="<?php echo $row['mb_id'] ?>" id="menu200" <?php echo (adm_auth_check('200', $row['mb_id']) == true) ? "checked" : " "?> disabled="true">&nbsp;
            <input type="button" class="bt btn_04" name="menu200" value="설정" onclick="auth_pop('200', '<?php echo $row['mb_id'] ?>')"></td>
        <td class="td_Preferences">
            <input type="checkbox" name="menu300" value="<?php echo $row['mb_id'] ?>" id="menu300" <?php echo (adm_auth_check('300', $row['mb_id']) == true) ? "checked" : " "?> disabled="true">
            <input type="button" class="bt btn_04" name="menu300" value="설정" onclick="auth_pop('300', '<?php echo $row['mb_id'] ?>')"></td>
        <td class="td_Preferences">
            <input type="checkbox" name="menu400" value="<?php echo $row['mb_id'] ?>" id="menu400" <?php echo (adm_auth_check('400', $row['mb_id']) == true) ? "checked" : " "?> disabled="true">&nbsp;
            <input type="button" class="bt btn_04" name="menu400" value="설정" onclick="auth_pop('400', '<?php echo $row['mb_id'] ?>')"></td>
        <td class="td_Preferences">
            <input type="checkbox" name="menu500" value="<?php echo $row['mb_id'] ?>" id="menu500" <?php echo (adm_auth_check('500', $row['mb_id']) == true) ? "checked" : " "?> disabled="true">
            <input type="button" class="bt btn_04" name="menu500" value="설정" onclick="auth_pop('500', '<?php echo $row['mb_id'] ?>')"></td>
        <td class="td_Preferences">
            <input type="checkbox" name="menu600" value="<?php echo $row['mb_id'] ?>" id="menu600" <?php echo (adm_auth_check('600', $row['mb_id']) == true) ? "checked" : " "?> disabled="true">&nbsp;
            <input type="button" class="bt btn_04" name="menu600" value="설정" onclick="auth_pop('600', '<?php echo $row['mb_id'] ?>')"></td>
        <td class="td_Preferences">
            <input type="checkbox" name="menu700" value="<?php echo $row['mb_id'] ?>" id="menu700" <?php echo (adm_auth_check('700', $row['mb_id']) == true) ? "checked" : " "?> disabled="true">
            <input type="button" class="bt btn_04" name="menu700" value="설정" onclick="auth_pop('700', '<?php echo $row['mb_id'] ?>')"></td>
        <td class="td_Preferences">
            <input type="checkbox" name="menu900" value="<?php echo $row['mb_id'] ?>" id="menu900" <?php echo (adm_auth_check('900', $row['mb_id']) == true) ? "checked" : " "?> disabled="true">&nbsp;
            <input type="button" class="bt btn_04" name="menu900" value="설정" onclick="auth_pop('900', '<?php echo $row['mb_id'] ?>')"></td>
        <td><input type="button" class="bt btn_03" name="<?php echo $row['mb_id'] ?>" value="초기화" onclick="auth_reset('<?php echo $row['mb_id']?>')"></td>
    </tr>
    <?php
        $count++;
    }
    if ($count == 0)
        echo '<tr><td colspan="'.$colspan.'" class="empty_table">자료가 없습니다.</td></tr>';
    ?>
    </tbody>
    </table>
</div>

<?php

if (strstr($sfl, 'mb_id'))
    $mb_id = $stx;
else
    $mb_id = '';

$pagelist = get_paging(G5_IS_MOBILE ? $config['cf_mobile_pages'] : $config['cf_write_pages'], $page, $total_page, $_SERVER['SCRIPT_NAME'].'?'.$qstr.'&amp;page=');
echo $pagelist;

?>

<script>
    // 권한 설정 팝업
    function auth_pop(menu_id, mb_id) {
        var pop_url = "auth_list_set.php?menu="+menu_id+"&mb_id="+mb_id;
        window.open(pop_url,'window_name','width=450,height=500,left=200,top=200,location=no,status=no,scrollbars=yes');
    }

    // 관리 권한 초기화
    function auth_reset(mb_id){
        if (confirm("정말 초기화 하시겠습니까?"))
        $.ajax({
            type: "POST",
            url: g5_admin_url+"/ajax.auth_reset.php",
            data: {'mb_id': mb_id},
            dataType: "json",
            async: false,
            success: function () {
                location.reload();
            }
        });
    }
</script>

<?php
include_once ('./admin.tail.php');
?>

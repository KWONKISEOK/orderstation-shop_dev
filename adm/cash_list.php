<?php
$sub_menu = "200400";
include_once('./_common.php');

auth_check($auth[$sub_menu], 'r');

$sql_common = " from {$g5['cash_table']} ";



if ($stx) {
    $sql_search .= " where";
    switch ($sfl) {
        case 'mb_id' :
            $sql_search .= " {$sfl} = '{$stx}' ";
            break;
        default :
            $sql_search .= " {$sfl} like '%{$stx}%' ";
            break;
    }
}
    $sst  = "ca_id";
    $sod = "desc";

$sql_order = " order by {$sst} {$sod} ";

$sql = " select count(*) as cnt
            {$sql_common}
            {$sql_search}
            {$sql_order} ";
$row = sql_fetch($sql);

$total_count = $row['cnt'];



$rows = $config['cf_page_rows'];
$total_page  = ceil($total_count / $rows);  // 전체 페이지 계산
if ($page < 1) $page = 1; // 페이지가 없으면 첫 페이지 (1 페이지)
$from_record = ($page - 1) * $rows; // 시작 열을 구함

$sql = " select *
            {$sql_common}
            {$sql_search}
            {$sql_order}
            limit {$from_record}, {$rows} ";
$result = sql_query($sql);


$listall = '<a href="'.$_SERVER['SCRIPT_NAME'].'" class="ov_listall">전체목록</a>';

$mb = array();
if ($sfl == 'mb_id' && $stx)
    $mb = get_member($stx);

$g5['title'] = '적립금관리';
include_once ('./admin.head.php');

$colspan = 9;

if (strstr($sfl, "mb_id"))
    $mb_id = $stx;
else
    $mb_id = "";
?>

<div class="local_ov01 local_ov">
    <?php

    echo $listall ?>
    <span class="btn_ov01"><span class="ov_txt">전체 </span><span class="ov_num"> <?php echo number_format($total_count) ?> 건 </span></span>
    <?php
    if (isset($mb['mb_id']) && $mb['mb_id']) {
        echo '&nbsp;<span class="btn_ov01"><span class="ov_txt">' . $mb['mb_id'] .' 님 포인트 합계 </span><span class="ov_num"> ' . number_format($mb['mb_cash']) . '점</span></span>';
    } else {
        $row2 = sql_fetch(" select sum(ca_cash) as sum_cash from {$g5['cash_table']} ");
        echo '&nbsp;<span class="btn_ov01"><span class="ov_txt">전체 합계</span><span class="ov_num">'.number_format($row2['sum_cash']).'점 </span></span>';
    }
    ?>
</div>

<form name="fsearch" id="fsearch" class="local_sch01 local_sch" method="get">
<label for="sfl" class="sound_only">검색대상</label>
<select name="sfl" id="sfl">
    <option value="mb_id"<?php echo get_selected($_GET['sfl'], "mb_id"); ?>>회원아이디</option>
    <option value="ca_content"<?php echo get_selected($_GET['sfl'], "ca_content"); ?>>내용</option>
</select>
<label for="stx" class="sound_only">검색어<strong class="sound_only"> 필수</strong></label>
<input type="text" name="stx" value="<?php echo $stx ?>" id="stx" required class="required frm_input">
<input type="submit" class="btn_submit" value="검색">
</form>

<form name="fcashlist" id="fcashlist" method="cast" action="./cash_list_delete.php" onsubmit="return fcashlist_submit(this);">
<input type="hidden" name="sst" value="<?php echo $sst ?>">
<input type="hidden" name="sod" value="<?php echo $sod ?>">
<input type="hidden" name="sfl" value="<?php echo $sfl ?>">
<input type="hidden" name="stx" value="<?php echo $stx ?>">
<input type="hidden" name="page" value="<?php echo $page ?>">
<input type="hidden" name="token" value="">

<div class="tbl_head01 tbl_wrap">
    <table>
    <caption><?php echo $g5['title']; ?> 목록</caption>
    <thead>
    <tr>
        <th scope="col">
            <label for="chkall" class="sound_only">적립금 내역 전체</label>
            <input type="checkbox" name="chkall" value="1" id="chkall" onclick="check_all(this.form)">
        </th>
        <th scope="col"><?php echo subject_sort_link('mb_id') ?>회원아이디</a></th>
        <th scope="col">이름</th>
        <th scope="col">닉네임</th>
        <th scope="col"><?php echo subject_sort_link('ca_content') ?>적립금 내용</a></th>
        <th scope="col"><?php echo subject_sort_link('ca_cash') ?>적립금</a></th>
        <th scope="col"><?php echo subject_sort_link('ca_datetime') ?>일시</a></th>
        <th scope="col">만료일</th>
        <th scope="col">적립금합</th>
    </tr>
    </thead>
    <tbody>
    <?php
    for ($i=0; $row=sql_fetch_array($result); $i++) {
        if ($i==0 || ($row2['mb_id'] != $row['mb_id'])) {
            $sql2 = " select mb_id, mb_name, mb_nick, mb_email, mb_homepage, mb_cash from {$g5['member_table']} where mb_id = '{$row['mb_id']}' ";
            $row2 = sql_fetch($sql2);
        }

        $mb_nick = get_sideview($row['mb_id'], $row2['mb_nick'], $row2['mb_email'], $row2['mb_homepage']);

        $link1 = $link2 = '';
        if (!preg_match("/^\@/", $row['ca_rel_table']) && $row['ca_rel_table']) {
            $link1 = '<a href="'.G5_BBS_URL.'/board.php?bo_table='.$row['ca_rel_table'].'&amp;wr_id='.$row['ca_rel_id'].'" target="_blank">';
            $link2 = '</a>';
        }

        $expr = '';
        if($row['ca_expired'] == 1)
            $expr = ' txt_expired';

        $bg = 'bg'.($i%2);
    ?>

    <tr class="<?php echo $bg; ?>">
        <td class="td_chk">
            <input type="hidden" name="mb_id[<?php echo $i ?>]" value="<?php echo $row['mb_id'] ?>" id="mb_id_<?php echo $i ?>">
            <input type="hidden" name="ca_id[<?php echo $i ?>]" value="<?php echo $row['ca_id'] ?>" id="ca_id_<?php echo $i ?>">
            <label for="chk_<?php echo $i; ?>" class="sound_only"><?php echo $row['ca_content'] ?> 내역</label>
            <input type="checkbox" name="chk[]" value="<?php echo $i ?>" id="chk_<?php echo $i ?>">
        </td>
        <td class="td_left"><a href="?sfl=mb_id&amp;stx=<?php echo $row['mb_id'] ?>"><?php echo $row['mb_id'] ?></a></td>
        <td class="td_left"><?php echo get_text($row2['mb_name']); ?></td>
        <td class="td_left sv_use"><div><?php echo $mb_nick ?></div></td>
        <td class="td_left"><?php echo $link1 ?><?php echo $row['ca_content'] ?><?php echo $link2 ?></td>
        <td class="td_num td_pt"><?php echo number_format($row['ca_cash']) ?></td>
        <td class="td_datetime"><?php echo $row['ca_datetime'] ?></td>
        <td class="td_datetime2<?php echo $expr; ?>">
            <?php if ($row['ca_expired'] == 1) { ?>
            만료<?php echo substr(str_replace('-', '', $row['ca_expire_date']), 2); ?>
            <?php } else echo $row['ca_expire_date'] == '9999-12-31' ? '&nbsp;' : $row['ca_expire_date']; ?>
        </td>
        <td class="td_num td_pt"><?php echo number_format($row['ca_mb_cash']) ?></td>
    </tr>

    <?php
    }

    if ($i == 0)
        echo '<tr><td colspan="'.$colspan.'" class="empty_table">자료가 없습니다.</td></tr>';
    ?>
    </tbody>
    </table>
</div>
    <div class="btn_fixed_top">
        <a href="./cash_form.php" class="btn btn_01" >적립금 관리</a>
    </div>
</form>

<?php echo get_paging(G5_IS_MOBILE ? $config['cf_mobile_pages'] : $config['cf_write_pages'], $page, $total_page, "{$_SERVER['SCRIPT_NAME']}?$qstr&amp;page="); ?>

<script>
$(function () {
    ca_expire_check();
});

function ca_expire_check (input) {
    var ca_expire_date = $('#ca_expire_date');
    var today = new Date();

    if (typeof input == 'undefined') {
        today.setDate(today.getDate() + <?php echo $ca_expire_term-1; ?>); //15일 더하여 setting
    } else {
        today.setDate(today.getDate() + (input-1)); //15일 더하여 setting
    }

    var year = today.getFullYear();
    var month = today.getMonth() + 1;
    var day = today.getDate();

    var expire_date = year + '-' + month + '-' + day;

    ca_expire_date.html(expire_date)
}

function fcashlist_submit(f)
{
    if (!is_checked("chk[]")) {
        alert(document.pressed+" 하실 항목을 하나 이상 선택하세요.");
        return false;
    }

    if(document.pressed == "선택삭제") {
        if(!confirm("선택한 자료를 정말 삭제하시겠습니까?")) {
            return false;
        }
    }

    return true;
}
</script>

<?php
include_once ('./admin.tail.php');
?>

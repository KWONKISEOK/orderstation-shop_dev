<?php
$sub_menu = '300830';
include_once('./_common.php');
include_once(G5_EDITOR_LIB);

auth_check($auth[$sub_menu], "w");



$html_title = "공급사정보공유";
$g5['title'] = $html_title.' 관리';

if ($w == "u")
{
    $html_title .= " 수정";
    $readonly = " readonly";

    $sql = " select * from tbl_write_vender where wr_id = '$wr_id' ";
    $wr = sql_fetch($sql);
    if (!$wr['wr_id'])
        alert('등록된 자료가 없습니다.');
}
else
{
    $html_title .= ' 입력';
    $wr['wr_html'] = 2;
    $wr['wr_skin'] = 'basic';
    $wr['wr_mobile_skin'] = 'basic';
}

include_once (G5_ADMIN_PATH.'/admin.head.php');
?>

<form name="frmcontentform" action="./venderformupdate.php" onsubmit="return frmcontentform_check(this);" method="post" enctype="MULTIPART/FORM-DATA" >
<input type="hidden" name="w" value="<?php echo $w; ?>">
<input type="hidden" name="wr_id" value="<?php echo $wr['wr_id']; ?>">
<input type="hidden" name="wr_html" value="1">
<input type="hidden" name="token" value="">

<div class="tbl_frm01 tbl_wrap">
    <table>
    <caption><?php echo $g5['title']; ?> 목록</caption>
    <colgroup>
        <col class="grid_4">
        <col>
    </colgroup>
    <tbody>
	<? if( $member['mb_type'] == "9" ){ ?>
    <tr>
        <th scope="row"><label for="wr_subject">공급사선택</label></th>
        <td>
			<input type="radio" name="wr_4" id="wr_4_1" value="A" onclick="javascript:ChoiceComp('A');" <?if($wr['wr_4']=="A"||empty($wr['wr_4'])){?>checked<?}?> />전체
			<input type="radio" name="wr_4" id="wr_4_2" value="C" onclick="javascript:ChoiceComp('C');" <?if($wr['wr_4']=="C"){?>checked<?}?>/>선택
		</td>
    </tr>
	<? } ?>
	<tr id="choice_space" style="display:none;">
		<th scope="row"></th>
		<td>
			<select name="comp_code" id="comp_code" style="width:200px;">
				<option value="">==업체선택==</option>
				<?php
				$sql2 = " select comp_code, comp_name from tbl_member where mb_type='7' order by comp_name ";
				$result2 = sql_query($sql2);
				for ($i=0; $row2=sql_fetch_array($result2); $i++) {
					if($comp_code ==$row2['comp_code']) 
						$checked='selected';
					else 
						$checked ='';

					echo '<option value="'.$row2['comp_code'].'" '.$checked.' >'.$row2['comp_name'].'</option>'.PHP_EOL;
				}
				?>
			</select>	
			<input type="button" name="addComp" value="공급사추가">
			<div>&nbsp;</div>


			<table border="0" style="width:400px;">
			<?php
			$sql3 = " select a.* , b.comp_name , b.comp_code from tbl_write_vender_choice a inner join tbl_member b on a.wr_comp_code = b.comp_code 
					  where a.wr_gubun = 'C' and a.wr_id = '{$wr['wr_id']}' ";

			$result3 = sql_query($sql3);

			if( count(sql_fetch_array(sql_query($sql3))) > 0 ){
			?>
				<tr name="trComp">
					<td colspan="2" style="border-top:1px solid #ffffff;border-bottom:1px solid #ffffff;"></td>
				</tr>
				<?
				for ($i=0; $row3=sql_fetch_array($result3); $i++) {
				?>
				<tr name="trComp">
					<td colspan="2" style="border-top:1px solid #ffffff;border-bottom:1px solid #ffffff;">
						<span><?=$row3["comp_name"];?></span>
						<input type="hidden" name="vender_comp_code[]" value='<?=$row3["comp_code"];?>' >
						<button name="delComp">삭제</button>
					</td>
				</tr>
				<? } ?>
			<? }else{ ?>
				<tr name="trComp">
					<td colspan="2" style="border-top:1px solid #ffffff;border-bottom:1px solid #ffffff;"></td>
				</tr>
			<? } ?>
			</table>


		</td>
	</tr>
    <tr>
        <th scope="row"><label for="wr_subject">제목</label></th>
        <td><input type="text" name="wr_subject" value="<?php echo htmlspecialchars2($wr['wr_subject']); ?>" id="wr_subject" required class="frm_input required" size="90"></td>
    </tr>
    <tr>
        <th scope="row"><label for="wr_subject">첨부파일</label></th>
        <td>
			<input type="file" name="wr_file" id="wr_file" ><br><br>
			<? if( !empty($wr['wr_2']) ){ ?>
			첨부파일 : <?=$wr['wr_1']?> &nbsp;<a href="./venderfiledown.php?wr_id=<?=$wr['wr_id']?>&tname=<?=$wr['wr_2']?>"><strong>[다운로드]</strong></a>
			&nbsp;<input type="checkbox" name="wr_file_del" value="Y" <?if( $wr['wr_2'] == "Y" ){ ?> checked <? }?> >파일삭제
			<? } ?>
		</td>
    </tr>
    <tr>
        <th scope="row">내용</th>
        <td><?php echo editor_html('wr_content', get_text($wr['wr_content'], 0)); ?></td>
    </tr>
   
    </tbody>
    </table>
</div>

<div class="btn_fixed_top">
    <a href="./venderlist.php" class="btn btn_02">목록</a>
    <input type="submit" value="확인" class="btn btn_submit" accesskey="s">
</div>

</form>

<?php
// [KVE-2018-2089] 취약점 으로 인해 파일 경로 수정시에만 자동등록방지 코드 사용
?>
<script>


function frmcontentform_check(f)
{
    errmsg = "";
    errfld = "";

    <?php echo get_editor_js('wr_content'); ?>
    <?php echo chk_editor_js('wr_content'); ?>
   
	check_field(f.wr_subject, "제목을 입력하세요.");
    check_field(f.wr_content, "내용을 입력하세요.");

    if (errmsg != "") {
        alert(errmsg);
        errfld.focus();
        return false;
    }
  
    return true;
}

function ChoiceComp(GetValue){

	if( GetValue == "A" ){
		document.getElementById("choice_space").style.display = "none";
	}	
	if( GetValue == "C" ){
		document.getElementById("choice_space").style.display = "";
	}	

}


</script>

<!--업체선택 검색기능-->

<link href="/adm/select2.css" rel="stylesheet"/>
<script type="text/javascript" src="/adm/select2.js"></script>
<script>
$(document).ready(function () {

	<?if( !empty( $wr['wr_4'] ) && ( $member["mb_type"] == "9" )  ){?>
	ChoiceComp('<?=$wr['wr_4']?>');
	<? } ?>


	$("#comp_code").select2();

    //추가 버튼
    $(document).on("click","input[name=addComp]",function(){

		var choice_txt = $("#comp_code option:selected").text();
		var choice_val = $("#comp_code option:selected").val();

		if( choice_val == "" ){
			alert("공급업체를 선택해주십시요.");
		}else{
         
			var addCompText =  '<tr name="trComp" >'+
				'   <td colspan="2" style="border-top:1px solid #ffffff;border-bottom:1px solid #ffffff;height:35px;">'+
				'		<span>'+choice_txt+'</span>'+
				'       <input type="hidden" name="vender_comp_code[]" value='+choice_val+' >'+
				'       <button name="delComp">삭제</button>'+
				'   </td>'+
				'</tr>';
				 
			var trHtml = $( "tr[name=trComp]:last" ); //last를 사용하여 마지막 태그 호출
			 
			trHtml.after(addCompText); //마지막에 붙인다.

		}
    });
     
    //삭제 버튼
    $(document).on("click","button[name=delComp]",function(){
         
        var trHtml = $(this).parent().parent();
         
        trHtml.remove(); //tr 테그 삭제
         
    });

});
</script>

<?php
include_once (G5_ADMIN_PATH.'/admin.tail.php');
?>

<?php
if (!defined('G5_USE_SHOP') || !G5_USE_SHOP) return;

$sql = " select count(a.it_status2) as cnt from tbl_shop_item a , tbl_shop_category b, tbl_member c 
		 where (a.ca_id = b.ca_id and a.comp_code = c.comp_code and a.it_status2 = '2' and c.mb_type=7 )  ";
$row = sql_fetch($sql);
//반품신청건수
$return_cnt_view = "(" .$row['cnt']. ")";

$menu['menu600'] = array (
    array('600000', '상품관리', G5_ADMIN_URL.'/shop_admin/itemsellrank.php', 'shop_stats'),
	array('600200', '분류관리', G5_ADMIN_URL.'/shop_admin/categorylist.php', 'scf_cate'),
    array('600250', '상품승인처리', G5_ADMIN_URL.'/shop_admin/itemlist_reg.php', 'scf_item_reg'),
    array('600260', '상품변경승인처리'.$return_cnt_view , G5_ADMIN_URL.'/shop_admin/itemlist_reg2.php', 'scf_item_reg2'),
    array('600300', '상품관리', G5_ADMIN_URL.'/shop_admin/itemlist.php', 'scf_item'),
	array('600400', '옵션관리', G5_ADMIN_URL.'/shop_admin/itemlisto.php', 'scf_item'),
    array('600660', '오더스테이션 상품Q&A', G5_ADMIN_URL.'/shop_admin/itemqalist.php', 'scf_item_qna'),
    array('600680', '이즈브레앱 상품Q&A', G5_ADMIN_URL.'/shop_admin/itemqalist2.php', 'scf_item_qna'),
    array('600650', '사용후기', G5_ADMIN_URL.'/shop_admin/itemuselist.php', 'scf_ps'),
    array('600670', '대표상품 엑셀업로드', G5_ADMIN_URL.'/shop_admin/item_excelupload.php', 'scf_item_excel')
    //array('600620', '상품재고관리', G5_ADMIN_URL.'/shop_admin/itemstocklist.php', 'scf_item_stock'),
    //array('600610', '상품유형관리', G5_ADMIN_URL.'/shop_admin/itemtypelist.php', 'scf_item_type'),
    //array('600500', '상품옵션재고관리', G5_ADMIN_URL.'/shop_admin/optionstocklist.php', 'scf_item_option'),
);

//최고관리자일때 만 사내주문 메뉴 보이게
if( $member['mb_id'] == "admin" ){
    array('', '-----------------------', '#', '');
    array_push($menu['menu600'], array('', '-----------------------', '#', '', 1) );
    array_push($menu['menu600'], array('600690', '상품 동영상 관리', G5_ADMIN_URL.'/shop_admin/item_video.php', 'scf_item_movie', 1) );
    }
?>
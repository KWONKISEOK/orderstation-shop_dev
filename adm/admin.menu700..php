<?php
if (!defined('G5_USE_SHOP') || !G5_USE_SHOP) return;


$menu['menu700'] = array (
    array('700000', '기타관리', G5_ADMIN_URL.'/shop_admin/itemsellrank.php', 'shop_stats'),
   // array('700110', '매출현황', G5_ADMIN_URL.'/shop_admin/sale1.php', 'sst_order_stats'),
    //array('700100', '상품판매순위', G5_ADMIN_URL.'/shop_admin/itemsellrank.php', 'sst_rank'),
    //array('700120', '주문내역출력', G5_ADMIN_URL.'/shop_admin/orderprint.php', 'sst_print_order', 1),
    //array('700400', '재입고SMS알림', G5_ADMIN_URL.'/shop_admin/itemstocksms.php', 'sst_stock_sms', 1),
	array('700320', '이벤트배너관리', G5_ADMIN_URL.'/shop_admin/itemeventbanner.php', 'scf_banner', 1),
    array('700300', '이벤트관리', G5_ADMIN_URL.'/shop_admin/itemevent.php', 'scf_event'),
    array('700310', '이벤트일괄처리', G5_ADMIN_URL.'/shop_admin/itemeventlist.php', 'scf_event_mng'),
    array('700500', '배너관리', G5_ADMIN_URL.'/shop_admin/bannerlist.php', 'scf_banner', 1),
    array('700600', '요즘은 이런게 대세', G5_ADMIN_URL.'/shop_admin/trendbannerform.php', 'scf_trend', 1),
    array('700140', '보관함현황', G5_ADMIN_URL.'/shop_admin/wishlist.php', 'sst_wish'),
    //array('700210', '가격비교사이트', G5_ADMIN_URL.'/shop_admin/price.php', 'sst_compare', 1)
);
?>
<?php
$sub_menu = "200140";
include_once("./_common.php");

auth_check($auth[$sub_menu], 'w');

if( empty($mb_id) ){
	echo "잘못된 접근입니다.";
	exit;
}

$sql = " update tbl_charge_info set DelYn = 'Y' where mb_id = '{$mb_id}' ";
sql_query( $sql );

$sql2 = " 
		insert into tbl_charge_info(
			mb_id , Charge_Task , Charge_Name , Charge_Bigo , Charge_Tel , Charge_Hp , Charge_Email , Charge_RegDate , DelYn
		)
		select 
			mb_id , Charge_Task , Charge_Name , Charge_Bigo , Charge_Tel , Charge_Hp , Charge_Email , Charge_RegDate , 'N'
		from tbl_charge_info_mod where mb_id = '{$mb_id}' and DelYn='N'
	   ";
sql_query( $sql2 );


$sql3 = " update tbl_member set comp_name				= comp_name_mod , 
							    comp_limit				= comp_limit_mod,
							    comp_brand				= comp_brand_mod,
							    comp_ceo_name			= comp_ceo_name_mod,
							    comp_tel				= comp_tel_mod,
							    comp_md_name			= comp_md_name_mod,
							    comp_md_tel				= comp_md_tel_mod,
							    comp_order_time			= comp_order_time_mod,
							    comp_trans_time			= comp_trans_time_mod,
							    comp_charge_datetime_2	= now() , 
							    comp_change_status		= '1'
		  where mb_id = '{$mb_id}' 
		";
sql_query( $sql3 );

goto_url('./member_form.php?'.$qstr.'&amp;w=u&amp;mb_id='.$mb_id, false);
?>
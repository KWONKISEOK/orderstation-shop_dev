<?php
include_once('./_common.php');

$mode	= $_POST['mode'];

//$sql_common = " from {$g5['member_table']} a ";

//$sql_search = " where (1) ";
if ($stx) {
    $sql_search .= " and ( ";
    switch ($sfl) {
        
        case 'mb_name' :
        case 'pharm_custno' :
            $sql_search .= " ({$sfl} like '%{$stx}') ";
            break;
		case 'pharm_name' :
            $sql_search .= " ({$sfl} like '%{$stx}%') ";
            break;
		case 'sales_name' :
            $sql_search .= " (a.pharm_manager in ( select z.mb_id from tbl_member z where z.mb_type='5' and z.mb_name ='{$stx}')) ";
            break;
        default :
            $sql_search .= " ({$sfl} like '{$stx}%') ";
            break;
    }
    $sql_search .= " ) ";
}
if ($is_admin != 'super')
    $sql_search .= " and mb_level <= '{$member['mb_level']}' ";

if ($is_admin == 'sales')
    $sql_search .= " and mb_manager = '{$member['mb_id']}' ";

if ($pharm_status) {
    $sql_search .= " and pharm_status = '$pharm_status' ";
}
if ($mb_referee) {
    $sql_search .= " and mb_referee = '$mb_referee' ";
}
if ($pharm_div) {
    $sql_search .= " and pharm_div = '$pharm_div' ";
}
if ($pharm_search) {
    $sql_search .= " and pharm_search = '$pharm_search' ";
}



if ($pharm_control) {
    $sql_search .= " and pharm_control = '$pharm_control' ";
}
if ($pharm_look) {
    $sql_search .= " and pharm_look = '$pharm_look' ";
}
if ($mb_pay_yn) {
    $sql_search .= " and mb_pay_yn = '$mb_pay_yn' ";
}
if ($pharm_status) {
    $sql_search .= " and pharm_status = '$pharm_status' ";
}
if ($pharm_map) {    
	if ( $pharm_map == 'Y' ) {
		$sql_search .= " and pharm_map_lat != '' ";
	} else {
		$sql_search .= " and pharm_map_lat = '' ";
	}
}
if ($pharm_sido) {
    $sql_search .= " and pharm_sido = '$pharm_sido' ";
}
if ($pharm_gugun) {
    $sql_search .= " and pharm_gugun = '$pharm_gugun' ";
}
if ($pharm_dong) {
    $sql_search .= " and pharm_dong = '$pharm_dong' ";
}

if ($sales_id) {
    $sql_search .= " and pharm_manager = '$sales_id' ";
}



switch($mode){
	
	case 'excel1':
		$ExcelTitle = urlencode( "약국회원" . date( "YmdHis" ) );
		header( "Content-type: application/vnd.ms-excel" );
		header( "Content-type: application/vnd.ms-excel; charset=utf-8" );
		header( "Content-Disposition: attachment; filename = " . $ExcelTitle . ".xls" );
		header( "Content-Description: PHP4 Generated Data" );
		print( "<meta http-equiv=\"Content-Type\" content=\"application/vnd.ms-excel; charset=utf-8\">" ); ?>
		<table border="1">
		<tr bgcolor="F7F7F7" height="30">
			<th>약국코드</th>
			<th>아이디</th>
			<th>이름</th>
			<th>성별</th>
			<th>생년월일</th>
			<th>전화번호</th>
			<th>핸드폰번호</th>
			<th>이메일</th>
			<th>가입경로</th>
			<th>sms수신동의</th>
			<th>담당영업사원</th>
			<th>사용여부</th>
			<th>거래구분</th>
			<th>결제구분</th>
			<th>검색노출여부</th>
			<th>약국명</th>
			<th>약사면허번호</th>
			<th>사업자등록번호</th>
			<th>요양기관번호</th>
			<th>약국전화번호</th>
			<th>약국FAX</th>
			<th>휴무일</th>
			<th>약국소개</th>
			<th>우편번호</th>
			<th>주소1</th>
			<th>주소2</th>
			<th>지역분류1</th>
			<th>지역분류2</th>
			<th>지역분류3</th>
			<th>추천여부</th>
			<th>약국매출비중(처방의약품)</th>
			<th>약국매출비중(일반의약품)</th>
			<th>출고통제</th>
			<th>출고통제날짜</th>
		</tr>
		<?php		
		$sql = " select * 
				from tbl_member a where a.mb_type = 1 ".$sql_search ." order by mb_datetime desc";
//echo "<pre>".$sql."</pre>";
		$res = sql_query($sql);
		if(!$res) exit('Cannot run query1.');

		$i = 0;
		while($row = mysqli_fetch_array($res,MYSQL_ASSOC)) {

			$pharm_status       = $row["pharm_status"];
			$mb_referee         = $row["mb_referee"];
			$mb_joinroute       = $row["mb_joinroute"];

			switch ( $row["pharm_div"] ) {
				case "1" :
					$pharm_div = "태전약품";
					break;
				case "2" :
					$pharm_div = "티제이팜";
					break;
			}
			switch ( $row["mb_pay_yn"] ) {
				case "Y" :
					$mb_pay_yn = "오더스테이션결제";
					break;
				case "N" :
					$mb_pay_yn = "직거래";
					break;
			}
			switch ( $row["pharm_look"] ) {
				case "0" :
					$pharm_look = "미선택";
					break;
				case "1" :
					$pharm_look = "지역노출";
				    break;
				case "2" :
					$pharm_look = "전체노출";
					break;
			}

			switch ($row["mb_sms"]){
			    case "0":
			        $mb_sms = "X";
			        break;
			    case "1" :
			        $mb_sms = "O";
			        break;
			}
			switch ( $pharm_status ) {
				case "1" :
					$mb_status_str = "사용(대기)";
					break;
				case "2" :
					$mb_status_str = "사용(승인)";
					break;
				case "3" :
					$mb_status_str = "사용안함";
					break;
				case "N" :
					$mb_status_str = "신규가입";
					break;
				case "L" :
					$mb_status_str = "erp등록중";
					break;
				case "D" :
					$mb_status_str = "휴면";
					break;
			}

			switch ( $mb_referee ) {
				case "0" :
					$mb_referee_str = "일반회원";
					break;
				case "1" :
					$mb_referee_str = "OS추천회원";
					break;
				case "2" :
					$mb_referee_str = "모니터링회원";
					break;
				case "3" :
					$mb_referee_str = "OS서팩2회원";
					break;
				case "4" :
					$mb_referee_str = "OS직원";
					break;
				case "5" :
					$mb_referee_str = "얼라이언스약국";
					break;
			}
			switch ( $mb_joinroute ) {
				case "01" :
					$mb_joinroute_str = "티제이팜 영업사원추천";
					break;
				case "02" :
					$mb_joinroute_str = "태전약품 영업사원추천";
					break;
				case "03" :
					$mb_joinroute_str = "오엔케이 영업사원추천";
					break;
				case "04" :
					$mb_joinroute_str = "박람회 등";
					break;
				case "05" :
					$mb_joinroute_str = "주변(약사님)추천";
					break;
				case "06" :
					$mb_joinroute_str = "고객센터 추천";
					break;
				case "07" :
					$mb_joinroute_str = "보도자료(네이버 등)";
					break;
				case "08" :
					$mb_joinroute_str = "기타";
					break;
			}

			?>
			<tr bgcolor="">
				<td style="mso-number-format:\@"><?php echo $row["pharm_custno"];?></td>
				<td style="mso-number-format:\@"><?php echo $row["mb_id"];?></td>
				<td style="mso-number-format:\@"><?php echo $row["mb_name"];?></td>
				<td style="mso-number-format:\@"><?php echo $row["mb_sex"];?></td>
				<td style="mso-number-format:\@"><?php echo $row["mb_birth"];?></td>
				<td style="mso-number-format:\@"><?php echo $row["mb_tel"];?></td>
				<td style="mso-number-format:\@"><?php echo $row["mb_hp"];?></td>
				<td style="mso-number-format:\@"><?php echo $row["mb_email"];?></td>
				<td style="mso-number-format:\@"><?php echo $mb_joinroute_str;?></td>
				<td style="mso-number-format:\@"><?php echo $mb_sms;?></td>
				<td style="mso-number-format:\@"><?php echo $row["pharm_manager"];?></td>
				<td style="mso-number-format:\@"><?php echo $mb_status_str;?></td>
				<td style="mso-number-format:\@"><?php echo $pharm_div;?></td>
				<td style="mso-number-format:\@"><?php echo $mb_pay_yn;?></td>
				<td style="mso-number-format:\@"><?php echo $row["pharm_search"];?></td>
				<td style="mso-number-format:\@"><?php echo $row["pharm_name"];?></td>
				<td style="mso-number-format:\@"><?php echo $row["pharm_license"];?></td>
				<td style="mso-number-format:\@"><?php echo $row["pharm_number"];?></td>
				<td style="mso-number-format:\@"><?php echo $row["pharm_sanatorium"];?></td>
				<td style="mso-number-format:\@"><?php echo $row["pharm_tel"];?></td>
				<td style="mso-number-format:\@"><?php echo $row["pharm_fax"];?></td>
				<td style="mso-number-format:\@"><?php echo $row["pharm_hoilday"];?></td>
				<td style="mso-number-format:\@"><?php echo $row["pharm_intro"];?></td>
				<td style="mso-number-format:\@"><?php echo $row["mb_zip1"];?> <?php echo $row["mb_zip2"];?> </td>
				<td style="mso-number-format:\@"><?php echo $row["mb_addr1"];?></td>
				<td style="mso-number-format:\@"><?php echo $row["mb_addr2"];?></td>
				<td style="mso-number-format:\@"><?php echo $row["pharm_sido"];?></td>
				<td style="mso-number-format:\@"><?php echo $row["pharm_gugun"];?></td>
				<td style="mso-number-format:\@"><?php echo $row["pharm_dong"];?></td>
				<td style="mso-number-format:\@"><?php echo $pharm_look;?></td>
				<td style="mso-number-format:\@"><?php echo $row["pharm_account1"];?></td>
				<td style="mso-number-format:\@"><?php echo $row["pharm_account2"];?></td>
				<td style="mso-number-format:\@"><?php echo $row["pharm_control"];?></td>
				<td style="mso-number-format:\@"><?php echo $row["pharm_control_date"];?></td>
			</tr>
		<?php			
		} 
		break;
	
}



	

?>
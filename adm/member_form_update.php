<?php
$sub_menu = "200100";
include_once("./_common.php");
include_once(G5_LIB_PATH."/register.lib.php");
include_once(G5_LIB_PATH.'/thumbnail.lib.php');

if ($w == 'u') { 
    check_demo();
    $u = "수정";    
} else
     $u = "추가";

switch ($mb_type){
    case '0': $depth2=200120; $subject='일반 회원 ';
        break;
    case '1': $depth2=200100; $subject='약국 회원 ' ;
        break;
    case '5': $depth2=200130; $subject='영업사 ';
        break;
    case '7': $depth2=200140; $subject='공급사 ';
        break;
    case '9': $depth2=200150; $subject='관리자 ';
        break;
}


auth_check($auth[$sub_menu], 'w');

check_admin_token();

$mb_id = trim($_POST['mb_id']);
/*
// 휴대폰번호 체크
$mb_hp = hyphen_hp_number($_POST['mb_hp']);
if($mb_hp) {
    $result = exist_mb_hp($mb_hp, $mb_id);
    if ($result)
        alert($result);
}

// 인증정보처리
if($_POST['mb_certify_case'] && $_POST['mb_certify']) {
    $mb_certify = $_POST['mb_certify_case'];
    $mb_adult = $_POST['mb_adult'];
} else {
    $mb_certify = '';
    $mb_adult = 0;
}
*/
$mb_zip1 = substr($_POST['mb_zip'], 0, 3);
$mb_zip2 = substr($_POST['mb_zip'], 3);
$mb_type = $_POST['mb_type'];

$sql_common = "  mb_name = '{$_POST['mb_name']}',
                 mb_nick = '{$_POST['mb_nick']}',
                 mb_email = '{$_POST['mb_email']}',
                 mb_homepage = '{$_POST['mb_homepage']}',
                 mb_birth = '{$_POST['mb_birth']}',
				 mb_tel = '{$_POST['mb_tel']}',
                 mb_hp = '{$mb_hp}',
                 mb_certify = '{$mb_certify}',
                 mb_adult = '{$mb_adult}',
                 mb_zip1 = '$mb_zip1',
                 mb_zip2 = '$mb_zip2',
                 mb_addr1 = '{$_POST['mb_addr1']}',
                 mb_addr2 = '{$_POST['mb_addr2']}',
                 mb_addr3 = '{$_POST['mb_addr3']}',
                 mb_addr_jibeon = '{$_POST['mb_addr_jibeon']}',
                 mb_signature = '{$_POST['mb_signature']}',
                 mb_leave_date = '{$_POST['mb_leave_date']}',
                 mb_intercept_date='{$_POST['mb_intercept_date']}',
                 mb_memo = '{$_POST['mb_memo']}',
                 mb_mailling = '{$_POST['mb_mailling']}',
                 mb_sms = '{$_POST['mb_sms']}',
                 mb_open = '{$_POST['mb_open']}',
                 mb_profile = '{$_POST['mb_profile']}',
				 mb_type = '{$_POST['mb_type']}',
                 mb_level = '{$_POST['mb_level']}',
                 mb_grade = '{$_POST['mb_grade']}'";

if($mb_type=='0') {
	$sql_common .= " ,mb_referee = '{$_POST['mb_referee']}'";
	$sql_common .= " ,pharm_status		= '{$_POST['pharm_status']}'";
	$sql_common .= " ,pharm_sido		= '{$_POST['pharm_sido']}'";
	$sql_common .= " ,pharm_gugun		= '{$_POST['pharm_gugun']}'";
	$sql_common .= " ,pharm_dong		= '{$_POST['pharm_dong']}'";

}
if($mb_type=='1') {

	$sql_common .= " ,pharm_name		= '{$_POST['pharm_name']}'";
	$sql_common .= " ,pharm_license		= '{$_POST['pharm_license']}'";
	$sql_common .= " ,pharm_number		= '{$_POST['pharm_number']}'";
	$sql_common .= " ,pharm_sanatorium  = '{$_POST['pharm_sanatorium']}'";
	$sql_common .= " ,pharm_tel			= '{$_POST['pharm_tel']}'";
	$sql_common .= " ,pharm_fax			= '{$_POST['pharm_fax']}'";

	$sql_common .= " ,pharm_manager		= '{$_POST['pharm_manager']}'";
	$sql_common .= " ,pharm_div			= '{$_POST['pharm_div']}'";
	$sql_common .= " ,mb_pay_yn			= '{$_POST['mb_pay_yn']}'";
	$sql_common .= " ,pharm_search		= '{$_POST['pharm_search']}'";

	$sql_common .= " ,mb_referee		= '{$_POST['mb_referee']}'";
	$sql_common .= " ,pharm_look		= '{$_POST['pharm_look']}'";

	//$sql_common .= " ,pharm_control		= '{$_POST['pharm_control']}'";
	$sql_common .= " ,pharm_status		= '{$_POST['pharm_status']}'";

	$sql_common .= " ,pharm_sido		= '{$_POST['pharm_sido']}'";
	$sql_common .= " ,pharm_gugun		= '{$_POST['pharm_gugun']}'";
	$sql_common .= " ,pharm_dong		= '{$_POST['pharm_dong']}'";

	$sql_common .= " ,pharm_holiday		= '{$_POST['pharm_holiday']}'";
	$sql_common .= " ,pharm_intro		= '{$_POST['pharm_intro']}'";
	$sql_common .= " ,mb_joinroute		= '{$_POST['mb_joinroute']}'";
	$sql_common .= " ,pharm_account1	= '{$_POST['pharm_account1']}'";
	$sql_common .= " ,pharm_account2	= '{$_POST['pharm_account2']}'";

}
if($mb_type=='5') {
	$sql_common .= " ,pharm_manager_dept		= '{$_POST['pharm_manager_dept']}'";
	$sql_common .= " ,pharm_status		= '{$_POST['pharm_status']}'";
}
if($mb_type=='7') {
	//공급사코드가 없으면 생성
	$comp_code =$_POST['comp_code'];
	if($comp_code =='') {
		$sql = " select max(comp_code) comp_code from {$g5['member_table']} where mb_type = 7 ";
		$row = sql_fetch($sql);
		$comp_code = $row['comp_code'] + 1;
	}

	$sql_common .= " ,comp_code		= '$comp_code'";
	$sql_common .= " ,comp_name		= '{$_POST['comp_name']}'";
	$sql_common .= " ,comp_brand		= '{$_POST['comp_brand']}'";
	$sql_common .= " ,comp_limit		= '{$_POST['comp_limit']}'";
	$sql_common .= " ,comp_tel		= '{$_POST['comp_tel']}'";
	$sql_common .= " ,comp_ceo_name		= '{$_POST['comp_ceo_name']}'";
	$sql_common .= " ,comp_order_time		= '{$_POST['comp_order_time']}'";
	$sql_common .= " ,comp_trans_time		= '{$_POST['comp_trans_time']}'";
	$sql_common .= " ,pharm_status		= '{$_POST['pharm_status']}'";
	$sql_common .= " ,comp_md_name		= '{$_POST['comp_md_name']}'";
	$sql_common .= " ,comp_md_tel		= '{$_POST['comp_md_tel']}'";

	//배송비 상한가 정보
	$sql_common .= " ,comp_trans_compare	= '{$_POST['comp_trans_compare']}'";
	$sql_common .= " ,comp_trans_price		= '{$_POST['comp_trans_price']}'";
	$sql_common .= " ,comp_trans_use		= '{$_POST['comp_trans_use']}'";
	
}

//echo $sql_common;
//exit;


if ($w == '')
{
    $mb = get_member($mb_id);
    if ($mb['mb_id'])
        alert('이미 존재하는 회원아이디입니다.\\nＩＤ : '.$mb['mb_id'].'\\n이름 : '.$mb['mb_name'].'\\n닉네임 : '.$mb['mb_nick'].'\\n메일 : '.$mb['mb_email']);

    // 닉네임중복체크
    /*$sql = " select mb_id, mb_name, mb_nick, mb_email from {$g5['member_table']} where mb_nick = '{$_POST['mb_nick']}' ";
    $row = sql_fetch($sql);
    if ($row['mb_id'])
        alert('이미 존재하는 닉네임입니다.\\nＩＤ : '.$row['mb_id'].'\\n이름 : '.$row['mb_name'].'\\n닉네임 : '.$row['mb_nick'].'\\n메일 : '.$row['mb_email']);
		*/

    // 이메일중복체크
    $sql = " select mb_id, mb_name, mb_nick, mb_email from {$g5['member_table']} where mb_email = '{$_POST['mb_email']}' ";
    $row = sql_fetch($sql);
    if ($row['mb_id'])
        alert('이미 존재하는 이메일입니다.\\nＩＤ : '.$row['mb_id'].'\\n이름 : '.$row['mb_name'].'\\n닉네임 : '.$row['mb_nick'].'\\n메일 : '.$row['mb_email']);

    sql_query(" insert into {$g5['member_table']} set mb_id = '{$mb_id}', mb_password = '".get_encrypt_string($mb_password)."', mb_datetime = '".G5_TIME_YMDHIS."', mb_ip = '{$_SERVER['REMOTE_ADDR']}', mb_email_certify = '".G5_TIME_YMDHIS."', {$sql_common} ");
    

}
else if ($w == 'u')
{
    $mb = get_member($mb_id);
    if (!$mb['mb_id'])
        alert('존재하지 않는 회원자료입니다.');

    if ($is_admin != 'super' && $mb['mb_level'] >= $member['mb_level'])
        alert('자신보다 권한이 높거나 같은 회원은 수정할 수 없습니다.');

    if ($is_admin !== 'super' && is_admin($mb['mb_id']) === 'super' ) {
        alert('최고관리자의 비밀번호를 수정할수 없습니다.');
    }

    if ($_POST['mb_id'] == $member['mb_id'] && $_POST['mb_level'] != $mb['mb_level'])
        alert($mb['mb_id'].' : 로그인 중인 관리자 레벨은 수정 할 수 없습니다.');

    // 닉네임중복체크
    //$sql = " select mb_id, mb_name, mb_nick, mb_email from {$g5['member_table']} where mb_nick = '{$_POST['mb_nick']}' and mb_id <> '$mb_id' ";
    //$row = sql_fetch($sql);
    //if ($row['mb_id'])
    //    alert('이미 존재하는 닉네임입니다.\\nＩＤ : '.$row['mb_id'].'\\n이름 : '.$row['mb_name'].'\\n닉네임 : '.$row['mb_nick'].'\\n메일 : '.$row['mb_email']);

    // 이메일중복체크
    //$sql = " select mb_id, mb_name, mb_nick, mb_email from {$g5['member_table']} where mb_email = '{$_POST['mb_email']}' and mb_id <> '$mb_id' ";
    //$row = sql_fetch($sql);
    //if ($row['mb_id'])
    //    alert('이미 존재하는 이메일입니다.\\nＩＤ : '.$row['mb_id'].'\\n이름 : '.$row['mb_name'].'\\n닉네임 : '.$row['mb_nick'].'\\n메일 : '.$row['mb_email']);

    $mb_dir = substr($mb_id,0,2);

    // 회원 아이콘 삭제
    if ($del_mb_icon)
        @unlink(G5_DATA_PATH.'/member/'.$mb_dir.'/'.$mb_id.'.gif');

    $image_regex = "/(\.(gif|jpe?g|png))$/i";
    $mb_icon_img = $mb_id.'.gif';

    // 아이콘 업로드
    if (isset($_FILES['mb_icon']) && is_uploaded_file($_FILES['mb_icon']['tmp_name'])) {
        if (!preg_match($image_regex, $_FILES['mb_icon']['name'])) {
            alert($_FILES['mb_icon']['name'] . '은(는) 이미지 파일이 아닙니다.');
        }

        if (preg_match($image_regex, $_FILES['mb_icon']['name'])) {
            $mb_icon_dir = G5_DATA_PATH.'/member/'.$mb_dir;
            @mkdir($mb_icon_dir, G5_DIR_PERMISSION);
            @chmod($mb_icon_dir, G5_DIR_PERMISSION);

            $dest_path = $mb_icon_dir.'/'.$mb_icon_img;

            move_uploaded_file($_FILES['mb_icon']['tmp_name'], $dest_path);
            chmod($dest_path, G5_FILE_PERMISSION);
            
            if (file_exists($dest_path)) {
                $size = @getimagesize($dest_path);
                if ($size[0] > $config['cf_member_icon_width'] || $size[1] > $config['cf_member_icon_height']) {
                    $thumb = null;
                    if($size[2] === 2 || $size[2] === 3) {
                        //jpg 또는 png 파일 적용
                        $thumb = thumbnail($mb_icon_img, $mb_icon_dir, $mb_icon_dir, $config['cf_member_icon_width'], $config['cf_member_icon_height'], true, true);
                        if($thumb) {
                            @unlink($dest_path);
                            rename($mb_icon_dir.'/'.$thumb, $dest_path);
                        }
                    }
                    if( !$thumb ){
                        // 아이콘의 폭 또는 높이가 설정값 보다 크다면 이미 업로드 된 아이콘 삭제
                        @unlink($dest_path);
                    }
                }
            }
        }
    }
    
    $mb_img_dir = G5_DATA_PATH.'/member_image/';
    if( !is_dir($mb_img_dir) ){
        @mkdir($mb_img_dir, G5_DIR_PERMISSION);
        @chmod($mb_img_dir, G5_DIR_PERMISSION);
    }
    $mb_img_dir .= substr($mb_id,0,2);

    // 회원 이미지 삭제
    if ($del_mb_img)
        @unlink($mb_img_dir.'/'.$mb_icon_img);

    // 아이콘 업로드
    if (isset($_FILES['mb_img']) && is_uploaded_file($_FILES['mb_img']['tmp_name'])) {
        if (!preg_match($image_regex, $_FILES['mb_img']['name'])) {
            alert($_FILES['mb_img']['name'] . '은(는) 이미지 파일이 아닙니다.');
        }
        
        if (preg_match($image_regex, $_FILES['mb_img']['name'])) {
            @mkdir($mb_img_dir, G5_DIR_PERMISSION);
            @chmod($mb_img_dir, G5_DIR_PERMISSION);
            
            $dest_path = $mb_img_dir.'/'.$mb_icon_img;
            
            move_uploaded_file($_FILES['mb_img']['tmp_name'], $dest_path);
            chmod($dest_path, G5_FILE_PERMISSION);

            if (file_exists($dest_path)) {
                $size = @getimagesize($dest_path);
                if ($size[0] > $config['cf_member_img_width'] || $size[1] > $config['cf_member_img_height']) {
                    $thumb = null;
                    if($size[2] === 2 || $size[2] === 3) {
                        //jpg 또는 png 파일 적용
                        $thumb = thumbnail($mb_icon_img, $mb_img_dir, $mb_img_dir, $config['cf_member_img_width'], $config['cf_member_img_height'], true, true);
                        if($thumb) {
                            @unlink($dest_path);
                            rename($mb_img_dir.'/'.$thumb, $dest_path);
                        }
                    }
                    if( !$thumb ){
                        // 아이콘의 폭 또는 높이가 설정값 보다 크다면 이미 업로드 된 아이콘 삭제
                        @unlink($dest_path);
                    }
                }
            }
        }
    }

    if ($mb_password)
        $sql_password = " , mb_password = '".get_encrypt_string($mb_password)."' ";
    else
        $sql_password = "";

    if ($passive_certify)
        $sql_certify = " , mb_email_certify = '".G5_TIME_YMDHIS."' ";
    else
        $sql_certify = "";

        
    $al_data_old = sql_fetch("select * from {$g5['member_table']} where mb_id = '$mb_id'");

    $sql = " update {$g5['member_table']}
                set {$sql_common}
                     {$sql_password}
                     {$sql_certify}
                where mb_id = '{$mb_id}' ";
    sql_query($sql);


}
else
    alert('제대로 된 값이 넘어오지 않았습니다.');

    
/************* 관리자 로그 처리 START *************/
$al_data = sql_fetch("select * from {$g5['member_table']} where mb_id = '$mb_id'");

insert_admin_log(200,$depth2, $subject.$u, '', $mb_id, '', $_SERVER['REQUEST_URI'], $al_data, $al_data_old);

    
if (!$admin_form) {
    goto_url('./member_form.php?'.$qstr.'&amp;w=u&amp;mb_id='.$mb_id, false);
} else {
    goto_url('./member_form_admin.php?'.$qstr.'&amp;w=u&amp;mb_id='.$mb_id, false);
}

?>
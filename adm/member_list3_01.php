<?php
include_once('./_common.php');

$mode	= $_POST['mode'];

$sql_common = " from {$g5['member_table']} a ";

//$sql_search = " where (1) ";
if ($stx) {
    $sql_search .= " and ( ";
    switch ($sfl) {
        
        case 'mb_name' :
        case 'mb_id' :
            $sql_search .= " ({$sfl} like '%{$stx}%') ";
            break;
		case 'pharm_name' :
            $sql_search .= " ({$sfl} like '%{$stx}%') ";
            break;
        default :
            $sql_search .= " ({$sfl} like '{$stx}%') ";
            break;
    }
    $sql_search .= " ) ";
}

   // $sql_search .= " and mb_type = '0' ";

$sql_search .= " and mb_datetime between '$fr_date 00:00:00' and '$to_date 23:59:59' ";

if ($pharm_sido) {
    $sql_search .= " and pharm_sido2 = '$pharm_sido' ";
}
if ($pharm_gugun) {
    $sql_search .= " and pharm_gugun2 = '$pharm_gugun' ";
}
if ($pharm_dong) {
    $sql_search .= " and pharm_dong2 = '$pharm_dong' ";
}


//상태
if ($pharm_status) {
    $sql_search .= " and pharm_status = '$pharm_status' ";
}

/*회원구분*/
if ($mb_referee != "") {
    $sql_search .= " and mb_referee = '$mb_referee' ";
}
/*추천약국구분*/
if ($mb_referee2 != "") {
    $sql_search .= " and mb_referee2 = '$mb_referee2' ";
}

if ($where) {
	$sql_search = implode(' and ', $where);
}

switch($mode){
	
	case 'excel1':
		
		$ExcelTitle = urlencode( "소비자회원" . date( "YmdHis" ) );
	
		header( "Content-type: application/vnd.ms-excel" );
		header( "Content-type: application/vnd.ms-excel; charset=utf-8" );
		header( "Content-Disposition: attachment; filename = " . $ExcelTitle . ".xls" );
		header( "Content-Description: PHP4 Generated Data" );
		print( "<meta http-equiv=\"Content-Type\" content=\"application/vnd.ms-excel; charset=utf-8\">" ); 
	
	?>
	
		<table border="1">
		<tr  height="30">
			<th>아이디</th>
			<th>이름</th>
			<th>핸드폰</th>
			<th>이메일</th>
			<th>핸드폰수신여부</th>
			<th>이메일수신여부</th>
			<th>푸쉬수신여부</th>
			<th>포인트</th>
			<th>관리약국</th>
			<th>담당영업사원</th>
			<th>약국코드</th>
			<th>가입일</th>
			<th>상태</th>
			<th>구매횟수</th>
			<th>약국시도</th>
			<th>약국구군</th>
			<th>약국동읍면</th>
			<th>회원구분</th>
			<th>추천약국구분</th>
		</tr>
		<?php		
		$sql = " select * from( select * 
					, (select pharm_name from tbl_member where a.mb_recommend = mb_id) as  pharm_name2
					, (select pharm_custno from tbl_member where a.mb_recommend = mb_id) as  pharm_custno2
					, (select pharm_sido from tbl_member where a.mb_recommend = mb_id) as  pharm_sido2
					, (select pharm_gugun from tbl_member where a.mb_recommend = mb_id) as  pharm_gugun2
					, (select pharm_dong from tbl_member where a.mb_recommend = mb_id) as  pharm_dong2
					,(select count(distinct aa.od_id) from  tbl_shop_order aa  
						inner join tbl_shop_order_detail bb  
						on aa.od_id = bb.od_id   
						inner join tbl_shop_order_receiver cc  
						 on aa.od_id = cc.od_id and bb.od_num = cc.od_num  
						where 1=1 and cc.od_status != '취소' and aa.mb_id = a.mb_id) as order_cnt 
					, (select concat(ad_zip1, ad_zip2) from tbl_shop_order_address where a.mb_id = mb_id limit 1) as  zipcode
					, (select ad_addr1 from tbl_shop_order_address where a.mb_id = mb_id limit 1) as  addr1
					, (select ad_addr2 from tbl_shop_order_address where a.mb_id = mb_id limit 1) as  addr2
					, (select bb.mb_name from tbl_member aa, tbl_member bb where aa.pharm_manager = bb.mb_id and a.mb_recommend = aa.mb_id) as  sales_name
					, (select mb_referee from tbl_member where mb_id = a.mb_recommend) mb_referee2
				from tbl_member a where a.mb_type = 0 and a.mb_recommend not in('test01','test02')) as x where 1=1 ".$sql_search ." order by mb_datetime desc ";

		//echo "<pre>".$sql."</pre>";
		$res = sql_query($sql);
		if(!$res) exit('Cannot run query.');

		$i = 0;
		while($row = mysqli_fetch_array($res,MYSQL_ASSOC)) {

			$pharm_status       = $row["pharm_status"];
			$mb_referee         = $row["mb_referee"];
			$mb_referee2        = $row["mb_referee2"];

			switch ( $row["pharm_div"] ) {
				case "1" :
					$pharm_div = "태전약품";
					break;
				case "2" :
					$pharm_div = "티제이팜";
					break;
			}
			switch ( $pharm_status ) {
				case "1" :
					$mb_status_str = "사용(대기)";
					break;
				case "2" :
					$mb_status_str = "사용(승인)";
					break;
				case "3" :
					$mb_status_str = "사용안함";
					break;
				case "N" :
					$mb_status_str = "신규가입";
					break;
				case "L" :
					$mb_status_str = "erp등록중";
					break;
				case "D" :
					$mb_status_str = "휴면";
					break;
			}

			switch ( $mb_referee ) {
				case "0" :
					$mb_referee_str = "일반회원";
					break;
				case "1" :
					$mb_referee_str = "OS추천회원";
					break;
				case "2" :
					$mb_referee_str = "모니터링회원";
					break;
				case "3" :
					$mb_referee_str = "OS서팩2회원";
					break;
				case "4" :
					$mb_referee_str = "OS직원";
					break;
			}
			switch ( $mb_referee2 ) {
				case "0" :
					$mb_referee_str2 = "일반회원";
					break;
				case "3" :
					$mb_referee_str2 = "OS서팩2회원";
					break;
				case "5" :
					$mb_referee_str2 = "얼라이언스약국";
					break;
			}
	
			?>
			<tr bgcolor="<%=trstyle%>">
				<td align="center" style="mso-number-format:\@"><?php echo $row["mb_id"];?></td>
				<td align="center" style="mso-number-format:\@"><?php echo $row["mb_name"];?></td>
				<td align="center" style="mso-number-format:\@"><?php echo $row["mb_hp"];?></td>
				<td align="center" style="mso-number-format:\@"><?php echo $row["mb_email"];?></td>
				<td align="center" style="mso-number-format:\@"><?php echo $row["mb_sms"];?></td>
				<td align="center" style="mso-number-format:\@"><?php echo $row["mb_mailling"];?></td>
				<td align="center" style="mso-number-format:\@"><?php echo $row["mb_push"];?></td>
				<td align="center" style="mso-number-format:\@"><?php echo $row["mb_point"];?></td>
				<td align="center" style="mso-number-format:\@"><?php echo $row["pharm_name2"];?></td>
				<td align="center" style="mso-number-format:\@"><?php echo $row["sales_name"];?></td>
				<td align="center" style="mso-number-format:\@"><?php echo $row["pharm_custno2"];?></td>
				<td align="center" style="mso-number-format:\@"><?php echo $row["mb_datetime"];?></td>
				<td align="center" style="mso-number-format:\@"><?php echo $mb_status_str;?></td>
				<td align="center" style="mso-number-format:\@"><?php echo $row["order_cnt"];?></td>
				<td align="center" style="mso-number-format:\@"><?php echo $row["pharm_sido2"];?></td>
				<td align="center" style="mso-number-format:\@"><?php echo $row["pharm_gugun2"];?></td>
				<td align="center" style="mso-number-format:\@"><?php echo $row["pharm_dong2"];?></td>
				<td align="center" style="mso-number-format:\@"><?php echo $mb_referee_str;?></td>
				<td align="center" style="mso-number-format:\@"><?php echo $mb_referee_str2;?></td>
			</tr>
		<?php			
		} 
		break;
	case 'excel2':
		
		$ExcelTitle = urlencode( "소비자회원-포인트" . date( "YmdHis" ) );
	
		header( "Content-type: application/vnd.ms-excel" );
		header( "Content-type: application/vnd.ms-excel; charset=utf-8" );
		header( "Content-Disposition: attachment; filename = " . $ExcelTitle . ".xls" );
		header( "Content-Description: PHP4 Generated Data" );
		print( "<meta http-equiv=\"Content-Type\" content=\"application/vnd.ms-excel; charset=utf-8\">" ); 
		
		?>

		<table border="1">
		<tr bgcolor="F7F7F7" height="30">
			<th>아이디</th>
			<th>이름</th>
			<th>회원구분</th>
			<th>추천약국구분</th>
			<th>내역</th>
			<th>포인트</th>
			<th>사용포인트</th>
			<th>등록일자</th>
			<th>만료일자</th>
			<th>잔여포인트</th>
		</tr>
		<?php		
		$sql = " select * from( 
					  select a.mb_id , a.pharm_status , a.mb_referee , a.mb_name , a.mb_datetime , b.po_id , b.po_datetime , b.po_content , b.po_point , 
					  b.po_use_point , b.po_expired , b.po_expire_date , b.po_mb_point , b.po_rel_table , b.po_rel_id
					, (select mb_referee from tbl_member where mb_id = a.mb_recommend) mb_referee2
					, (select pharm_sido from tbl_member where a.mb_recommend = mb_id) as  pharm_sido2
					, (select pharm_gugun from tbl_member where a.mb_recommend = mb_id) as  pharm_gugun2
					, (select pharm_dong from tbl_member where a.mb_recommend = mb_id) as  pharm_dong2
				   from tbl_member a, tbl_point b
				  where a.mb_id = b.mb_id and a.mb_type = 0 ) as x where 1=1 ".$sql_search ." order by x.mb_id, x.po_datetime desc";

		//echo "<pre>".$sql."</pre>";

		$res = sql_query($sql);
		if(!$res) exit('Cannot run query.');

		$i = 0;
		while($row = mysqli_fetch_array($res,MYSQL_ASSOC)) {

			//$pharm_status       = $row["pharm_status"];
			$mb_referee         = $row["mb_referee"];
			$mb_referee2        = $row["mb_referee2"];

			switch ( $row["pharm_div"] ) {
				case "1" :
					$pharm_div = "태전약품";
					break;
				case "2" :
					$pharm_div = "티제이팜";
					break;
			}
			switch ( $pharm_status ) {
				case "1" :
					$mb_status_str = "사용(대기)";
					break;
				case "2" :
					$mb_status_str = "사용(승인)";
					break;
				case "3" :
					$mb_status_str = "사용안함";
					break;
				case "n" :
					$mb_status_str = "신규가입";
					break;
				case "l" :
					$mb_status_str = "erp등록중";
					break;
			}

			switch ( $mb_referee ) {
				case "0" :
					$mb_referee_str = "일반회원";
					break;
				case "1" :
					$mb_referee_str = "OS추천회원";
					break;
				case "2" :
					$mb_referee_str = "모니터링회원";
					break;
				case "3" :
					$mb_referee_str = "OS서팩2회원";
					break;
				case "4" :
					$mb_referee_str = "OS직원";
					break;
			}
			switch ( $mb_referee2 ) {
				case "0" :
					$mb_referee_str2 = "일반회원";
					break;
				case "3" :
					$mb_referee_str2 = "OS서팩2회원";
					break;
				case "5" :
					$mb_referee_str2 = "얼라이언스약국";
					break;
			}
			?>
			<tr bgcolor="<%=trstyle%>">
				<td align="center" style="mso-number-format:\@"><?php echo $row["mb_id"];?></td>
				<td align="center" style="mso-number-format:\@"><?php echo $row["mb_name"];?></td>
				<td align="center" style="mso-number-format:\@"><?php echo $mb_referee_str;?></td>
				<td align="center" style="mso-number-format:\@"><?php echo $mb_referee_str2;?></td>
				<td align="center" style="mso-number-format:\@"><?php echo $row["po_content"];?></td>
				<td align="center" style="mso-number-format:\@">
				<?php if( (int)$row["po_point"] < 0 && $row["po_use_point"] == 0 ){?>
					0
				<?php }else{ ?>
					<?php echo $row["po_point"];?>
				<?php }?>
				</td>
				<td align="center" style="mso-number-format:\@">
				<?php if( (int)$row["po_point"] < 0 && $row["po_use_point"] == 0 ){?>
					<?php echo $row["po_point"];?>
				<?php }else{ ?>
					0
				<?php }?>
				</td>
				<td align="center" style="mso-number-format:\@"><?php echo $row["po_datetime"];?></td>
				<td align="center" style="mso-number-format:\@"><?php echo $row["po_expire_date"];?></td>
				<td align="center" style="mso-number-format:\@"><?php echo $row["po_mb_point"];?></td>		
			</tr>
		<?php			
		} 
		break;
	case 'excel3':
		
		$ExcelTitle = urlencode( "소비자회원-쿠폰" . date( "YmdHis" ) );
		header( "Content-type: application/vnd.ms-excel" );
		header( "Content-type: application/vnd.ms-excel; charset=utf-8" );
		header( "Content-Disposition: attachment; filename = " . $ExcelTitle . ".xls" );
		header( "Content-Description: PHP4 Generated Data" );
		print( "<meta http-equiv=\"Content-Type\" content=\"application/vnd.ms-excel; charset=utf-8\">" ); 
		
		?>
		<table border="1">
		<tr bgcolor="F7F7F7" height="30">
			<th>아이디</th>
			<th>이름</th>
			<th>회원구분</th>
			<th>추천약국구분</th>
			<th>제목</th>
			<th>쿠폰종류</th>
			<th>발행일</th>
			<th>사용기간</th>
			<th>발행금액</th>
			<th>사용날짜</th>
		</tr>
		<?php		
		$sql = " select * from( 
						select a.mb_id , a.pharm_status , a.mb_referee , a.mb_name , a.mb_datetime
					    ,b.cp_no , b.cp_id , b.cp_subject , b.cp_method , b.cp_target , b.cz_id , b.cp_start , b.cp_end
						,b.cp_price , b.cp_type , b.cp_trunc , b.cp_minimum , b.cp_maximum , b.od_id , b.cp_datetime
					    ,(select cl_datetime from tbl_shop_coupon_log where cp_id = b.cp_id) cl_datetime
						, (select mb_referee from tbl_member where mb_id = a.mb_recommend) mb_referee2
						, (select pharm_sido from tbl_member where a.mb_recommend = mb_id) as  pharm_sido2
						, (select pharm_gugun from tbl_member where a.mb_recommend = mb_id) as  pharm_gugun2
						, (select pharm_dong from tbl_member where a.mb_recommend = mb_id) as  pharm_dong2
				   from tbl_member a, tbl_shop_coupon b
				  where a.mb_id = b.mb_id and a.mb_type = 0 ) as x where 1=1  ".$sql_search ." order by x.mb_id, x.cp_datetime desc";

		//echo "<pre>".$sql."</pre>";

		$res = sql_query($sql);
		if(!$res) exit('Cannot run query.');

		$i = 0;
		while($row = mysqli_fetch_array($res,MYSQL_ASSOC)) {

			//$pharm_status       = $row["pharm_status"];
			$mb_referee         = $row["mb_referee"];
			$mb_referee2         = $row["mb_referee2"];

			switch ( $row["cp_method"] ) {
				case "0" :
					$cp_method = "개별상품할인";
					break;
				case "1" :
					$cp_method = "카테고리할인";
					break;
				case "2" :
					$cp_method = "주문금액할인";
					break;
				case "3" :
					$cp_method = "배송비할인";
					break;
			}
			switch ( $pharm_status ) {
				case "1" :
					$mb_status_str = "사용(대기)";
					break;
				case "2" :
					$mb_status_str = "사용(승인)";
					break;
				case "3" :
					$mb_status_str = "사용안함";
					break;
				case "n" :
					$mb_status_str = "신규가입";
					break;
				case "l" :
					$mb_status_str = "erp등록중";
					break;
			}

			switch ( $mb_referee ) {
				case "0" :
					$mb_referee_str = "일반회원";
					break;
				case "1" :
					$mb_referee_str = "OS추천회원";
					break;
				case "2" :
					$mb_referee_str = "모니터링회원";
					break;
				case "3" :
					$mb_referee_str = "OS서팩2회원";
					break;
				case "4" :
					$mb_referee_str = "OS직원";
					break;
			}
			switch ( $mb_referee2 ) {
				case "0" :
					$mb_referee_str2 = "일반회원";
					break;
				case "3" :
					$mb_referee_str2 = "OS서팩2회원";
					break;
				case "5" :
					$mb_referee_str2 = "얼라이언스약국";
					break;
			}			
			?>
			<tr bgcolor="<%=trstyle%>">
				<td align="center" style="mso-number-format:\@"><?php echo $row["mb_id"];?></td>
				<td align="center" style="mso-number-format:\@"><?php echo $row["mb_name"];?></td>
				<td align="center" style="mso-number-format:\@"><?php echo $mb_referee_str;?></td>
				<td align="center" style="mso-number-format:\@"><?php echo $mb_referee_str2;?></td>
				<td align="center" style="mso-number-format:\@"><?php echo $row["cp_subject"];?></td>
				<td align="center" style="mso-number-format:\@"><?php echo $cp_method;?></td>
				<td align="center" style="mso-number-format:\@"><?php echo $row["cp_datetime"];?></td>
				<td align="center" style="mso-number-format:\@"><?php echo $row["cp_start"];?> ~ <?php echo $row["cp_end"];?></td>
				<td align="center" style="mso-number-format:\@"><?php echo $row["cp_price"];?></td>	
				<td align="center" style="mso-number-format:\@"><?php echo $row["cl_datetime"];?></td>	
			</tr>
		<?php			
		} 
		break;
	
}



	

?>
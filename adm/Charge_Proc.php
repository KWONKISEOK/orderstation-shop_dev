<?php
$sub_menu = "200140";
include_once("./_common.php");

auth_check($auth[$sub_menu], 'w');

$mb_id			= $_POST["mb_id"];
$Charge_Idx		= $_POST["Charge_Idx"];
$DelYn			= $_POST["DelYn"];
$Charge_Task	= $_POST["Charge_Task"];
$Charge_Name	= $_POST["Charge_Name"];
$Charge_Bigo	= $_POST["Charge_Bigo"];
$Charge_Tel		= $_POST["Charge_Tel"];
$Charge_Hp		= $_POST["Charge_Hp"];
$Charge_Email	= $_POST["Charge_Email"];

if( empty($mb_id) ){
	echo "<script>alert('잘못된 접근입니다.');</script>";
	exit;
}

for($i=0; $i<count($DelYn); $i++){

	$add_query	  = "";
	$Get_Charge_Idx	  = $Charge_Idx[$i];
	$Get_Charge_Task  = $Charge_Task[$i];
	$Get_Charge_Name  = $Charge_Name[$i];
	$Get_Charge_Bigo  = $Charge_Bigo[$i];
	$Get_Charge_Tel	  = $Charge_Tel[$i];
	$Get_Charge_Hp	  = $Charge_Hp[$i];
	$Get_Charge_Email = $Charge_Email[$i];
	$Get_DelYn		  = $DelYn[$i];

	//0 이면 insert , 0이 아니면 update
	if( $Get_Charge_Idx == "0" ){

		$sql = "
			insert into tbl_charge_info(
				mb_id , 
				Charge_Task , 
				Charge_Name , 
				Charge_Bigo , 
				Charge_Tel , 
				Charge_Hp , 
				Charge_Email , 
				DelYn
			)values(
				'{$mb_id}',
				'{$Get_Charge_Task}',
				'{$Get_Charge_Name}',
				'{$Get_Charge_Bigo}',
				'{$Get_Charge_Tel}',
				'{$Get_Charge_Hp}',
				'{$Get_Charge_Email}',
				'N'
			)
		";

		sql_query($sql);

	}else{

		if( $Get_DelYn == "Y" ){
			$add_query = " Charge_DelDate = now() ,  ";
		}

		$sql = "
			update tbl_charge_info set Charge_Task	= '{$Get_Charge_Task}' , 
									   Charge_Name	= '{$Get_Charge_Name}',
									   Charge_Bigo	= '{$Get_Charge_Bigo}',
									   Charge_Tel	= '{$Get_Charge_Tel}',
									   Charge_Hp	= '{$Get_Charge_Hp}',
									   Charge_Email = '{$Get_Charge_Email}',
									   ". $add_query ."
									   DelYn		= '{$Get_DelYn}'
			where Charge_Idx = {$Get_Charge_Idx} and mb_id = '{$mb_id}'
		";

		sql_query($sql);
	
	}

}

echo "<script>
		alert('저장완료하였습니다.');
		parent.location.reload();
	  </script>";

?>
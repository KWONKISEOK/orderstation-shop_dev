<?php
$sub_menu = '900201';
include_once('./_common.php');

if( empty($f_idx) ){
    echo "<script>
			alert('파일idx 고유값이 없습니다.');
			parent.location.reload();
		  </script>";
    exit;
}

/*************** 트랜잭션 관련 ****************/
$error_cnt = 0;
$ErrorMsg = "";
mysqli_autocommit($g5['connect_db'], false);
/*************** 트랜잭션 관련 ****************/

//해당 idx값의 데이터를 지워준다.
$sql = " delete from sms_excel_temp where f_idx = {$f_idx} ";
sql_query($sql);

$sql = " delete from sms_excel_fileinfo where f_idx = {$f_idx} ";
sql_query($sql);
/*************** 트랜잭션 관련 ****************/
if( mysqli_errno($g5['connect_db']) ){
    $error_cnt += 1;
}
/*************** 트랜잭션 관련 ****************/

/*************** 트랜잭션 관련 ****************/
if ($error_cnt > 0) {
    mysqli_rollback($g5['connect_db']);
    mysqli_close($g5['connect_db']);
    echo "
	<script>
		alert('데이터베이스의 에러로 인해 롤백되었습니다.');
		parent.location.reload();
	</script>
	";
    exit;
} else {
    mysqli_commit($g5['connect_db']);
}
/*************** 트랜잭션 관련 ****************/

echo "<script>
		alert('임시저장된 리스트가 삭제되었습니다.');
		parent.location.reload();
	  </script>";
exit;

?>
<?php
$sub_menu = "900200";
include_once("./_common.php");

auth_check($auth[$sub_menu], "r");

$g5['title'] = "SMS 전송";

include_once(G5_ADMIN_PATH.'/admin.head.php');
?>

<!--<div class="local_ov01 local_ov">-->
<!-- <div class="local_sch03  tbl_wrap" style="padding:10px;">
	* SMS 수신허용한 회원에 한에서만 발송됩니다.<br>
</div> -->

<div id="sms5_send_new">
    <form name="form_sms" id="form_sms" method="post" action="send_write.php" onsubmit="return sms5_chk_send(this);"  >
    <div id="send_write">
        <input type="hidden" name="kind" value="S" id="kind">
		<input type="hidden" name="send_list" value="">
		<input type="hidden" name="buy_months" value="<?echo $buy_months;?>">
	
        <h2>보낼내용</h2>
        <div class="sms5_box write_wrap">
            <span class="box_ico"></span>
            <label for="wr_message" id="wr_message_lbl">내용</label>
            <textarea name="wr_message" id="wr_message" class="box_txt box_square" onkeyup="byte_check('wr_message', 'sms_bytes');" accesskey="m"></textarea>

            <div id="sms_byte"><span id="sms_bytes">0</span> / <span id="sms_max_bytes">80</span> byte</div>

            <button type="button" id="write_sc_btn" class="write_scemo_btn">특수<br>기호</button>
            <div id="write_sc" class="write_scemo">
                <span class="scemo_ico"></span>
                <div class="scemo_list">
                    <button type="button" class="scemo_add" onclick="javascript:add('■')">■</button>
                    <button type="button" class="scemo_add" onclick="javascript:add('□')">□</button>
                    <button type="button" class="scemo_add" onclick="javascript:add('▣')">▣</button>
                    <button type="button" class="scemo_add" onclick="javascript:add('◈')">◈</button>
                    <button type="button" class="scemo_add" onclick="javascript:add('◆')">◆</button>
                    <button type="button" class="scemo_add" onclick="javascript:add('◇')">◇</button>
                    <button type="button" class="scemo_add" onclick="javascript:add('♥')">♥</button>
                    <button type="button" class="scemo_add" onclick="javascript:add('♡')">♡</button>
                    <button type="button" class="scemo_add" onclick="javascript:add('●')">●</button>
                    <button type="button" class="scemo_add" onclick="javascript:add('○')">○</button>
                    <button type="button" class="scemo_add" onclick="javascript:add('▲')">▲</button>
                    <button type="button" class="scemo_add" onclick="javascript:add('▼')">▼</button>
                    <button type="button" class="scemo_add" onclick="javascript:add('▶')">▶</button>
                    <button type="button" class="scemo_add" onclick="javascript:add('▷')">▷</button>
                    <button type="button" class="scemo_add" onclick="javascript:add('◀')">◀</button>
                    <button type="button" class="scemo_add" onclick="javascript:add('◁')">◁</button>
                    <button type="button" class="scemo_add" onclick="javascript:add('☎')">☎</button>
                    <button type="button" class="scemo_add" onclick="javascript:add('☏')">☏</button>
                    <button type="button" class="scemo_add" onclick="javascript:add('♠')">♠</button>
                    <button type="button" class="scemo_add" onclick="javascript:add('♤')">♤</button>
                    <button type="button" class="scemo_add" onclick="javascript:add('♣')">♣</button>
                    <button type="button" class="scemo_add" onclick="javascript:add('♧')">♧</button>
                    <button type="button" class="scemo_add" onclick="javascript:add('★')">★</button>
                    <button type="button" class="scemo_add" onclick="javascript:add('☆')">☆</button>
                    <button type="button" class="scemo_add" onclick="javascript:add('☞')">☞</button>
                    <button type="button" class="scemo_add" onclick="javascript:add('☜')">☜</button>
                    <button type="button" class="scemo_add" onclick="javascript:add('▒')">▒</button>
                    <button type="button" class="scemo_add" onclick="javascript:add('⊙')">⊙</button>
                    <button type="button" class="scemo_add" onclick="javascript:add('㈜')">㈜</button>
                    <button type="button" class="scemo_add" onclick="javascript:add('№')">№</button>
                    <button type="button" class="scemo_add" onclick="javascript:add('㉿')">㉿</button>
                    <button type="button" class="scemo_add" onclick="javascript:add('♨')">♨</button>
                    <button type="button" class="scemo_add" onclick="javascript:add('™')">™</button>
                    <button type="button" class="scemo_add" onclick="javascript:add('℡')">℡</button>
                    <button type="button" class="scemo_add" onclick="javascript:add('∑')">∑</button>
                    <button type="button" class="scemo_add" onclick="javascript:add('∏')">∏</button>
                    <button type="button" class="scemo_add" onclick="javascript:add('♬')">♬</button>
                    <button type="button" class="scemo_add" onclick="javascript:add('♪')">♪</button>
                    <button type="button" class="scemo_add" onclick="javascript:add('♩')">♩</button>
                    <button type="button" class="scemo_add" onclick="javascript:add('♭')">♭</button>
                </div>
                <div class="scemo_cls"><button type="button" class="scemo_cls_btn">닫기</button></div>
            </div>
            <button type="button" id="write_emo_btn" class="write_scemo_btn">이모<br>티콘</button>
            <div id="write_emo" class="write_scemo">
                <span class="scemo_ico"></span>
                <div class="scemo_list">
                    <button type="button" class="scemo_add" onclick="javascript:add('*^^*')">*^^*</button>
                    <button type="button" class="scemo_add" onclick="javascript:add('♡.♡')">♡.♡</button>
                    <button type="button" class="scemo_add" onclick="javascript:add('@_@')">@_@</button>
                    <button type="button" class="scemo_add" onclick="javascript:add('☞_☜')">☞_☜</button>
                    <button type="button" class="scemo_add" onclick="javascript:add('ㅠ ㅠ')">ㅠ ㅠ</button>
                    <button type="button" class="scemo_add" onclick="javascript:add('Θ.Θ')">Θ.Θ</button>
                    <button type="button" class="scemo_add" onclick="javascript:add('^_~♥')">^_~♥</button>
                    <button type="button" class="scemo_add" onclick="javascript:add('~o~')">~o~</button>
                    <button type="button" class="scemo_add" onclick="javascript:add('★.★')">★.★</button>
                    <button type="button" class="scemo_add" onclick="javascript:add('(!.!)')">(!.!)</button>
                    <button type="button" class="scemo_add" onclick="javascript:add('⊙.⊙')">⊙.⊙</button>
                    <button type="button" class="scemo_add" onclick="javascript:add('q.p')">q.p</button>
                    <button type="button" class="scemo_add emo_long" onclick="javascript:add('┏( \'\')┛')">┏( \'\')┛</button>
                    <button type="button" class="scemo_add emo_long" onclick="javascript:add('@)-)--')">@)-)--')</button>
                    <button type="button" class="scemo_add emo_long" onclick="javascript:add('↖(^-^)↗')">↖(^-^)↗</button>
                    <button type="button" class="scemo_add emo_long" onclick="javascript:add('(*^-^*)')">(*^-^*)</button>
                </div>
                <div class="scemo_cls"><button type="button" class="scemo_cls_btn">닫기</button></div>
            </div>

        </div>

		<!-- <div class="write_inner">
			
		</div> -->
		<div id="write_recv" class="write_inner">
            <h2>받는사람</h2>
            <button type="button" class="write_floater write_floater_btn" onclick="hp_list_del()">선택삭제</button>

            <label for="hp_list" class="sound_only">받는사람들</label>
            <select name="hp_list" id="hp_list" size="5"></select>

            <div id="recv_add">
                 <label for="hp_name" class="sound_only">이름</label>
                <input type="text" name="hp_name" id="hp_name" class="frm_input" size="11" maxlength="20" onkeypress="if(event.keyCode==13) document.getElementById('hp_number').focus();" placeholder="이름"><br> 
                <label for="hp_number" class="sound_only">번호</label>
                <input type="text" name="hp_number" id="hp_number" class="frm_input" size="11" maxlength="20" onkeypress="if(event.keyCode==13) hp_add()" placeholder="번호">
                <button type="button" onclick="hp_add()">추가</button><br>
            </div>
        </div>
		
        <div id="write_rsv" class="write_inner">
            <h2>예약전송</h2>

            <div class="write_floater">
                <label for="wr_booking"><span class="sound_only">예약전송 </span>사용</label>
                <input type="checkbox" name="wr_booking" id="wr_booking" onclick="booking(this.checked)">
            </div>

            <select name="wr_by" id="wr_by" disabled>
                <option value="<?php echo date('Y')?>"><?php echo date('Y')?></option>
                <option value="<?php echo date('Y')+1?>"><?php echo date('Y')+1?></option>
            </select>
            <label for="wr_by">년</label><br>
            <select name="wr_bm" id="wr_bm" disabled>
                <?php for ($i=1; $i<=12; $i++) { ?>
                <option value="<?php echo sprintf("%02d",$i)?>"<?php echo get_selected(date('m'), $i); ?>><?php echo sprintf("%02d",$i)?></option>
            <?php } ?>
            </select>
            <label for="wr_bm">월</label>
            <select name="wr_bd" id="wr_bd" disabled>
                <?php for ($i=1; $i<=31; $i++) { ?>
                <option value="<?php echo sprintf("%02d",$i)?>"<?php echo get_selected(date('d'), $i); ?>><?php echo sprintf("%02d",$i)?></option>
                <?php } ?>
            </select>
            <label for="wr_bd">일</label><br>
                <select name="wr_bh" id="wr_bh" disabled>
                <?php for ($i=0; $i<24; $i++) { ?>
                <option value="<?php echo sprintf("%02d",$i)?>"<?php echo get_selected(date('H')+1, $i); ?>><?php echo sprintf("%02d",$i)?></option>
                <?php } ?>
            </select>
            <label for="wr_bh">시</label>
            <select name="wr_bi" id="wr_bi" disabled>
                <?php for ($i=0; $i<=59; $i+=5) { ?>
                <option value="<?php echo sprintf("%02d",$i)?>"><?php echo sprintf("%02d",$i)?></option>
                <?php } ?>
            </select>
            <label for="wr_bi">분</label>
        </div>

        <div class="btn_confirm01 btn_confirm">
            <input type="submit" value="전송" class="btn_submit">
            <!-- <input type="submit" value="전송" onclick="send()"> -->
        </div>
    </div>

	<div id="send_book">
		<input type="radio" name="sms_check" value="0" checked class="sms_check" id="receiver_type0"> <label for="receiver_type0">번호입력</label>
        <input type="radio" name="sms_check" value="1" class="sms_check" id="receiver_type1"> <label for="receiver_type1">약국전체</label>
		<input type="radio" name="sms_check" value="2" class="sms_check" id="receiver_type2"> <label for="receiver_type2">소비자전체</label>
		<input type="radio" name="sms_check" value="3" class="sms_check" id="receiver_type3"> <label for="receiver_type3">선택발송</label>
		<input type="radio" name="sms_check" value="4" class="sms_check" id="receiver_type4"> <label for="receiver_type4">공급사전체</label>
		<input type="radio" name="sms_check" value="5" class="sms_check" id="receiver_type5"> <label for="receiver_type5">공급사선택</label>
		<input type="radio" name="sms_check" value="6" class="sms_check" id="receiver_type6"> <label for="receiver_type6">최근n개월구매자</label>


        <iframe id='imember' src="./send_member.php" frameborder="0" width="100%" height="100%" marginwidth="0" marginheight="0" style="display:none;"></iframe>
		<iframe id='imember_company' src="./send_member1.php" frameborder="0" width="100%" height="100%" marginwidth="0" marginheight="0" style="display:none;"></iframe>
		<iframe id='imember_buy' src="./send_buyer.php" frameborder="0" width="100%" height="100%" marginwidth="0" marginheight="0" style="display:none;"></iframe>
    </div>
</form>
</div>

<script>
var is_mms_mode = false;
var is_sms5_submitted = false;  //중복 submit방지

var sms_check = null;


function sms5_chk_send(f)
{
    if( is_sms5_submitted == false ){
        is_sms5_submitted = true;
		var hp_list = document.getElementById('hp_list');
        var wr_message = document.getElementById('wr_message');

		var list = '';

        if (!wr_message.value) {
            alert('메세지를 입력해주세요.');
            wr_message.focus();
            is_sms5_submitted = false;
            return false;
        }
		
        //if (!sms_check) {
        //    alert('메시지를 받을 대상을 선택해주세요.');
        //    is_sms5_submitted = false;
       //     return false;
       //}	
		

		for (i=0; i<hp_list.length; i++)
            list += hp_list.options[i].value + '/';


        //w = document.body.clientWidth/2 - 200;
        //h = document.body.clientHeight/2 - 100;
        //act = window.open('sms_ing.php', 'act', 'width=300, height=200, left=' + w + ', top=' + h);
        //act.focus();

        f.send_list.value = list;

        return true;
    } else {
        alert("데이터 전송중입니다.");
		return false;
    }
}

function booking(val)
{
    if (val)
    {
        document.getElementById('wr_by').disabled = false;
        document.getElementById('wr_bm').disabled = false;
        document.getElementById('wr_bd').disabled = false;
        document.getElementById('wr_bh').disabled = false;
        document.getElementById('wr_bi').disabled = false;
    }
    else
    {
        document.getElementById('wr_by').disabled = true;
        document.getElementById('wr_bm').disabled = true;
        document.getElementById('wr_bd').disabled = true;
        document.getElementById('wr_bh').disabled = true;
        document.getElementById('wr_bi').disabled = true;
    }
}

function add(str) {
    var conts = document.getElementById('wr_message');
    var bytes = document.getElementById('sms_bytes');
    conts.focus();
    conts.value+=str;
    byte_check('wr_message', 'sms_bytes');
    return;
}



function byte_check(wr_message, sms_bytes)
{
    var conts = document.getElementById(wr_message);
    var bytes = document.getElementById(sms_bytes);
    var max_bytes = document.getElementById("sms_max_bytes");

    var i = 0;
    var cnt = 0;
    var exceed = 0;
    var ch = '';

    for (i=0; i<conts.value.length; i++)
    {
        ch = conts.value.charAt(i);
        if (escape(ch).length > 4) {
            cnt += 2;
        } else {
            cnt += 1;
        }
    }

    bytes.innerHTML = cnt;

    if (cnt > 80 && !is_mms_mode)
    {
		is_mms_mode = true;
		document.getElementById('kind').value = 'M';
		alert('sms 메시지 내용은 80바이트 이내입니다. mms 발송으로 전환됩니다.');
        return;
    } else if (cnt <= 80 && is_mms_mode) {
		is_mms_mode = false;
		document.getElementById('kind').value = 'S';
		alert('메시지 내용이 80바이트 이내이므로 sms 발송으로 전환됩니다.');	
		return;
	}
}

byte_check('wr_message', 'sms_bytes');
document.getElementById('wr_message').focus();
</script>

<script>

$("input:radio[name=sms_check]").click(function () {
  // getter
  var radioVal = $('input:radio[name=sms_check]:checked').val();
	if(radioVal == '3') {
		$("#imember").show();
	} else {
		$("#imember").hide();
	}

	if(radioVal == '5') {
		$("#imember_company").show();
	} else {
		$("#imember_company").hide();
	}
	if(radioVal == '6') {
		$("#imember_buy").show();
	} else {
		$("#imember_buy").hide();
	}

});
function iShow() {
	$("#imember").show();
}
function iHide() {
	$("#imember").hide();
}


$(function(){
	$(".box_txt").bind("focus keydown", function(){
        $("#wr_message_lbl").hide();
    });
    $(".write_scemo_btn").click(function(){
        $(".write_scemo").hide();
        $(this).next(".write_scemo").show();
    });
    $(".scemo_cls_btn").click(function(){
        $(".write_scemo").hide();
    });
	
	$(".sms_check", this).click(function() {
		sms_check = $(this).val();
		//console.log(sms_check);
	});
});
function hp_add()
{
    var hp_number = document.getElementById('hp_number'),
        hp_name = document.getElementById('hp_name'),
        hp_list = document.getElementById('hp_list'),
        pattern = /^01[016789][0-9]{3,4}[0-9]{4}$/,
        pattern2 = /^01[016789]-[0-9]{3,4}-[0-9]{4}$/;

    if( !hp_number.value ){
        alert("휴대폰번호를 입력해 주세요.");
        hp_number.select();
        return;
    }

    if(!pattern.test(hp_number.value) && !pattern2.test(hp_number.value)) {
        alert("휴대폰번호 형식이 올바르지 않습니다.");
        hp_number.select();
        return;
    }

    if (!pattern2.test(hp_number.value)) {
        hp_number.value = hp_number.value.replace(new RegExp("(01[016789])([0-9]{3,4})([0-9]{4})"), "$1-$2-$3");
    }

    var item = hp_name.value+':'+hp_number.value;
    var value = hp_number.value+':'+hp_number.value;

    for (i=0; i<hp_list.length; i++) {
        if (hp_list[i].value == value) {
            alert('이미 같은 목록이 있습니다.');
            return;
        }
    }
	/*
    if( jQuery.inArray( hp_number.value , sms_obj.phone_number ) > -1 ){
       alert('목록에 이미 같은 휴대폰 번호가 있습니다.');
       return;
    } else {
        sms_obj.phone_number.push( hp_number.value );
    }
	*/
    hp_list.options[hp_list.length] = new Option(item, value);

    hp_number.value = '';
    hp_name.value = '';
    hp_name.select();
}

function hp_list_del()
{
    var hp_list = document.getElementById('hp_list');

    if (hp_list.selectedIndex < 0) {
        alert('삭제할 목록을 선택해주세요.');
        return;
    }

    var regExp = /(01[016789]{1}|02|0[3-9]{1}[0-9]{1})-?[0-9]{3,4}-?[0-9]{4}/,
        hp_number_option = hp_list.options[hp_list.selectedIndex],
        result = (hp_number_option.outerHTML.match(regExp));
    //if( result !== null ){
    //    sms_obj.phone_number = sms_obj.array_remove( sms_obj.phone_number, result[0] );
    //}
    hp_list.options[hp_list.selectedIndex] = null;
}
function sms_add(mb_name, mb_hp){
        //var bk_no = document.getElementsByName('bk_no');
	var ck_no = '';
	var count = 0;


	var hp_list = document.getElementById('hp_list');
	var item    = mb_name+':'+mb_hp;
	var value   = mb_name+':'+mb_hp;

	for (i=0; i<hp_list.length; i++) {
		if (hp_list[i].value == value) {
			alert('이미 같은 목록이 있습니다.');
			return;
		}
	}

	hp_list.options[hp_list.length] = new Option(item, value);
}

</script>

<?php
include_once(G5_ADMIN_PATH.'/admin.tail.php');
?>
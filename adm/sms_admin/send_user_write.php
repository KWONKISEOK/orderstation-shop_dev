<?php

echo "접근불가";
exit;


$sub_menu = "900200";
include_once("./_common.php");
include_once(G5_LIB_PATH."/surem.lib.php");

//auth_check($auth[$sub_menu], "w");

//check_admin_token();

$g5['title'] = "문자전송중";

$wr_message = clean_xss_tags(trim($wr_message));

if (!$wr_message)
    alert('메세지를 입력해주세요.');


$subject = '';
$msg     = $wr_message;
$reqtime = $wr_booking ? $wr_by . $wr_bm . $wr_bd . $wr_bh . $wr_bi : '00000000000000';
$result  = $wr_booking ? 'R' : '0';


//$surem = new SureM();				// testing
$surem = new SureM('development');	// development
//$surem = new SureM('production');	// production

//개별전송
if($gubun =='NO') {

	$list = array();
	$hps = array();

	$send_list = explode('/', $send_list);
	$wr_overlap = 1; // 중복번호를 체크함
	$overlap = 0;
	$duplicate_data = array();
	$duplicate_data['hp'] = array();
	$str_serialize = "";
	
	for($i=0; $i<count($send_list); $i++) 
	{

		$item = explode(':', $send_list[$i]);

		$hp = get_hp($item[1], 0);
		$name = $item[0];
		if(!$hp) continue;

		if ($wr_overlap && array_overlap($hps, $hp)) {
			$overlap++;
			array_push( $duplicate_data['hp'], $row['bk_hp'] );
			continue;
		}

		array_push($list, array('bk_hp' => $hp, 'bk_name' => $name));
		array_push($hps, $hp);

	}

	if( count($duplicate_data['hp']) ){ //중복된 번호가 있다면
		$duplicate_data['total'] = $overlap;
		$str_serialize = serialize($duplicate_data);
	}

	$total_count = count($list);

	for($i=0; $i<$total_count; $i++) {

		$callname  = $list[$i]['bk_name'];
		$callphone = str_replace('-', '', $list[$i]['bk_hp']); 

		$insert_sql = $surem->insert_query_builder($callname, $callphone, $subject, $msg, $reqtime, $result, $kind) ;
		//echo $insert_sql . '<br />';
		sql_query($insert_sql);
	}

} else {

	//DB전송
	
	$where = "and mb_type = '0' and mb_sms = 1 and mb_recommend = '".$member['mb_id']."'";
	//$where = "and mb_id = 'freejcw' ";	// for 테스트용
	//$where = "and mb_id = 'freejcw' or mb_id = 'chaeyoung' ";	// for 테스트용
	$count_sql = "select count(*) as cnt from {$g5['member_table']} where 1 $where ";
	//echo $count_sql . '<br />';

	$total_res = sql_fetch($count_sql);
	$total_count = $total_res['cnt'];
	if (!$total_count) alert('발송할 회원 데이터가 없습니다.');

	
	$sql = "select * from {$g5['member_table']} where 1 $where order by mb_no asc; ";
	//echo $sql;
	$qry = sql_query($sql);

	while($res = sql_fetch_array($qry)) {

		$callname  = $res['mb_name'];
		$callphone = str_replace('-', '', $res['mb_hp']); 
		
		
		$insert_sql = $surem->insert_query_builder($callname, $callphone, $subject, $msg, $reqtime, $result, $kind) ;
		//echo $insert_sql . '<br />';
		sql_query($insert_sql);
	}

}

alert_go_page('SMS '.$total_count.'건을 처리 완료하였습니다.');

function alert_go_page($msg) {
	$html = "<script>
	alert('$msg');
	location.href = 'send_user.php';</script>";

	echo $html;
	exit;
}
/* 
function win_close_alert($msg) {
	$html = "<script>
	act = window.open('sms_ing.php', 'act', 'width=300, height=200');
	act.close();
	alert('$msg');
	history.back();</script>";

	echo $html;
	exit;
}
*/
?>
<script>
//act = window.open('sms_ing.php', 'act', 'width=300, height=200');
//act.close();
//location.href = 'send.php';
</script>
<?php
include_once(G5_ADMIN_PATH.'/admin.tail.php');
?>
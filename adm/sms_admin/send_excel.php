<?php
$sub_menu = '900201';
include_once('./_common.php');

auth_check($auth[$sub_menu], "r");

$g5['title'] = '엑셀업로드 SMS 전송';
include_once(G5_ADMIN_PATH.'/admin.head.php');


// 테이블의 전체 레코드수만 얻음
$sql = " select count(*) as cnt from sms_excel_fileinfo ";
$row = sql_fetch($sql);
$total_count = $row['cnt'];

$rows = $config['cf_page_rows'];
$total_page = ceil($total_count / $rows);  // 전체 페이지 계산
if ($page < 1) {
    $page = 1;
} // 페이지가 없으면 첫 페이지 (1 페이지)
$from_record = ($page - 1) * $rows; // 시작 열을 구함

$sql = "SELECT a.*, ifnull(b.send_cnt,0) AS send_cnt
          FROM sms_excel_fileinfo AS a 
          LEFT JOIN (SELECT COUNT(f_idx) AS send_cnt , f_idx FROM sms_excel_temp GROUP BY f_idx) AS b ON a.f_idx = b.f_idx
		  ORDER BY a.f_idx DESC limit $from_record, $rows 
		";
$result = sql_query($sql);

$qstr .= ($qstr ? '&amp;' : '').'sca='.$sca.'&amp;save_stx='.$stx;

?>


<form name="frm" method="post" enctype="MULTIPART/FORM-DATA">
    <input type="hidden" name="kind" value="S" id="kind">
    <input type="hidden" name="page" value="<?php echo $page; ?>">

    <div class="tbl_head01 tbl_wrap">

        <div>
            <a href="<?php echo G5_URL; ?>/<?php echo G5_LIB_DIR; ?>/Excel/sms_excel.xlsx" class="btn btn_02">엑셀양식
                다운로드
            </a>
        </div>
        <br>

        <table>
            <tr>
                <td scope="col" width="100">엑셀파일</a></td>
                <td class="td_center" width="250"><input name="excel_file" type="file" size="50"></td>
                <td scope="col" width="80">문자 내용</a></td>
                <td class="td_left">
                    <!--<input type="text" name="f_memo" class="frm_input" size="60" maxlength="1000"/>-->
                    <textarea name="f_content" id="f_content" cols="30" rows="10" style="resize: none;" onkeyup="byte_check('f_content', 'sms_bytes');"></textarea>
                    <div id="sms_byte"><span id="sms_bytes">0</span> / <span id="sms_max_bytes">80</span> byte</div>
                    <a href="javascript:SendFrm();" class="btn btn_03">엑셀 임시저장하기</a>
                </td>
            </tr>
            <tr>
                <td>Error 행번호:</td>
                <td colspan="5">
                    <div id="ErrorView" style="float:left;font-weight:bold;color:red;"></div>
                </td>
            </tr>
        </table>

        <div>&nbsp;</div>

        <table>
            <caption><?php echo $g5['title']; ?> 목록</caption>
            <thead>
            <tr>
                <th scope="col" width="300">파일명</a></th>
                <th scope="col" width="120">등록아이디</a></th>
                <th scope="col" width="120">등록아이피</a></th>
                <th scope="col" width="150">등록일</a></th>
                <th scope="col">문자내용</a></th>
                <th scope="col" width="170">데이터이동</th>
            </tr>
            </thead>
            <tbody>
            <?php
            for ($i = 0; $row = sql_fetch_array($result); $i++) {

                $bg = 'bg'.($i % 2);
                ?>

                <tr class="<?php echo $bg; ?>">
                    <td class="td_center">
                        <a href="./send_exceldownload.php?f_idx=<?=$row["f_idx"]; ?>"><?=$row["f_name"]; ?></a>
                    </td>
                    <td class="td_center"><?= $row["f_regid"] ?></td>
                    <td class="td_center"><?= $row["f_regip"] ?></td>
                    <td class="td_center"><?= $row["f_regdate"] ?></td>
                    <td class="td_center"><?= nl2br($row["f_content"]) ?></td>
                    <td class="td_center">
                        <?php if ($row['f_status'] == 0) { ?>
                            <? if ($row['send_cnt'] > 0) { ?>
                                <a href="javascript:SendSms(<?= $row["f_idx"] ?>);" class="btn btn_01">
                                    (<?= $row['send_cnt'] ?>)명 발송
                                </a><br><br>
                                <a href="javascript:DelSendTemp(<?= $row["f_idx"] ?>);" class="btn btn_02">임시저장삭제</a>
                            <? } ?>
                        <?php } else { ?>
                            발송완료
                        <?php } ?>
                    </td>
                </tr>

                <?php
            }

            if ($i == 0) {
                echo '<tr><td colspan="6" class="empty_table">자료가 없습니다.</td></tr>';
            }
            ?>
            </tbody>
        </table>

    </div>

</form>

<form name="frm2">
    <input type="hidden" name="f_idx"/>
</form>

<!-- (s)hidden Frame -->
<iframe name="iFrm" style="display: none;"></iframe>

<style>
    #popupLayer {
        display: none;
        border: 5px solid #cccccc;
        margin-top: 50px;
        padding: 5px;
        background-color: #ffffff;
        z-index: 5;
    }

    #popupLayer .b-close {
        position: absolute;
        top: 10px;
        right: 25px;
        color: #f37a20;
        font-weight: bold;
        cursor: hand;
    }

    #popupLayer .popupContent {
        margin: 0;
        padding: 0;
        text-align: center;
        border: 0;
    }

    #popupLayer .popupContent iframe {
        width: 1500px;
        height: 800px;
        border: 0;
        padding: 0px;
        margin: 0;
        z-index: 10;
    }
</style>

<div id="popupLayer">
    <div class="popupContent"></div>
    <div class="b-close">
        <button type="button" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button">
            <span class="ui-button-text">&nbsp;X&nbsp;</span>
        </button>
    </div>
</div>

<script src="/js/jquery.bpopup.min.js"></script>


<?php echo get_paging(G5_IS_MOBILE ? $config['cf_mobile_pages'] : $config['cf_write_pages'], $page, $total_page, "{$_SERVER['SCRIPT_NAME']}?$qstr&amp;page="); ?>

<!--업체선택 검색기능-->
<link href="../select2.css" rel="stylesheet"/>
<script type="text/javascript" src="../select2.js"></script>
<script>

    var is_mms_mode = false;
    var is_sms5_submitted = false;  //중복 submit방지

    var sms_check = null;

    function byte_check(wr_message, sms_bytes)
    {
        var conts = document.getElementById(wr_message);
        var bytes = document.getElementById(sms_bytes);
        var max_bytes = document.getElementById("sms_max_bytes");

        var i = 0;
        var cnt = 0;
        var exceed = 0;
        var ch = '';

        for (i=0; i<conts.value.length; i++)
        {
            ch = conts.value.charAt(i);
            if (escape(ch).length > 4) {
                cnt += 2;
            } else {
                cnt += 1;
            }
        }

        bytes.innerHTML = cnt;

        if (cnt > 80 && !is_mms_mode)
        {
            is_mms_mode = true;
            document.getElementById('kind').value = 'M';
            alert('sms 메시지 내용은 80바이트 이내입니다. mms 발송으로 전환됩니다.');
            return;
        } else if (cnt <= 80 && is_mms_mode) {
            is_mms_mode = false;
            document.getElementById('kind').value = 'S';
            alert('메시지 내용이 80바이트 이내이므로 sms 발송으로 전환됩니다.');
            return;
        }
    }

    function SendFrm() {
        var frm = document.frm;

        if (frm.excel_file.value == "") {
            alert("엑셀파일을 선택해주세요");
            return;
        }

        if (frm.f_content.value == "") {
            alert("문자 내용을 입력해주세요");
            return;
        }

        frm.target = "iFrm";
        frm.action = "send_excelupload_proc.php";
        frm.submit();
    }

    function SendSms(idx)
    {
        var frm2 = document.frm2;

        if (idx == "") {
            alert("파일idx 값이 없습니다.");
            return
        }

        if (!confirm("문자를 발송하시겠습니까?")) {
            return;
        }

        if( is_sms5_submitted == false ){
            is_sms5_submitted = true;

            frm2.f_idx.value = idx;
            frm2.target = "frm";
            frm2.method = "post";
            frm2.action = "./send_excel_write.php";
            frm2.submit();

            return true;
        } else {
            alert("데이터 전송중입니다.");
            return false;
        }
    }

    // 임시 저장 삭제
    function DelSendTemp(GetId) {

        var frm2 = document.frm2;

        if (GetId == "") {
            alert("파일idx 값이 없습니다.");
            return
        }

        if (!confirm("임시저장데이터를 삭제하시겠습니까?")) {
            return;
        }

        frm2.f_idx.value = GetId;
        frm2.target = "iFrm";
        frm2.method = "post";
        frm2.action = "./send_exceldata_del.php";
        frm2.submit();

    }

</script>

<?php
include_once(G5_ADMIN_PATH.'/admin.tail.php');
?>

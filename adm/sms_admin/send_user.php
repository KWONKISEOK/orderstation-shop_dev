<?php

echo "접근불가";
exit;


$sub_menu = "900200";
include_once("./_common.php");

/*
function get_selected($field, $value)
{
    return ($field==$value) ? ' selected="selected"' : '';
}
*/

$gubun1 = $_REQUEST['gubun1'];
$gubun2 = $_REQUEST['gubun2'];


for ($i=0; $i<count($_POST['chk']); $i++)
{
	// 실제 번호를 넘김
	$k = $_POST['chk'][$i];
	$hp_data = $hp_data . $_POST['mb_hp'][$k] .'/';
}

$hp_data = substr( $hp_data, 0,  strlen($hp_data) -1);
?>
<!doctype html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta http-equiv="imagetoolbar" content="no">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<title>SMS 전송 | 오더스테이션</title>
<link rel="stylesheet" href="/adm/css/admin.css">
<link rel="stylesheet" href="/adm/sms_admin/css/sms5.css?ver=171222">
<!--[if lte IE 8]>
<script src="/js/html5.js"></script>
<![endif]-->
<script>
// 자바스크립트에서 사용하는 전역변수 선언
var g5_url       = "";
var g5_bbs_url   = "/bbs";
var g5_is_member = "1";
var g5_is_admin  = "";
var g5_is_mobile = "";
var g5_bo_table  = "";
var g5_sca       = "";
var g5_editor    = "";
var g5_cookie_domain = ".orderstation.co.kr";
var g5_admin_url = "/adm";
</script>
<script src="/js/jquery-1.8.3.min.js"></script>
<script src="/js/common.js?ver=171222"></script>
<script src="/js/wrest.js?ver=171222"></script>
<script src="/js/placeholders.min.js"></script>
<link rel="stylesheet" href="/js/font-awesome/css/font-awesome.min.css">




</head>
<body style="background-color:#fff" >
<div id="wrapper1">

    <div id="container1" class="container-small">


		<div class="local_sch03  tbl_wrap" style="padding:10px;">
			* 예시<br>000회원님, OOOO을 구매하셨습니다. 상품 관련해서 궁금하신 사항이 있거나 상담이 필요하시면 앱에 1:1 건강상담을 남겨주시거나 약국에 방문하여 문의해 주세요.<br>
		</div>

		<div id="sms5_send_new">

			<div id="send_write">
				<form name="form_sms" id="form_sms" method="post" action="send_user_write.php" onsubmit="return sms5_chk_send(this);"  >
				<input type="hidden" name="gubun" value="<?php echo $gubun2;?>">
				<input type="hidden" name="send_list" value="<?php echo $hp_data;?>">
				<input type="hidden" name="kind" value="S" id="kind">
				
				<table>
				<tr>
				<td>

				<h2>보낼내용</h2>
				<div class="sms5_box write_wrap">
					<span class="box_ico"></span>
					<label for="wr_message" id="wr_message_lbl">내용</label>
					<textarea name="wr_message" id="wr_message" class="box_txt box_square" onkeyup="byte_check('wr_message', 'sms_bytes');" accesskey="m"></textarea>

					<div id="sms_byte"><span id="sms_bytes">0</span> / <span id="sms_max_bytes">80</span> byte</div>

					<button type="button" id="write_sc_btn" class="write_scemo_btn">특수<br>기호</button>
					<div id="write_sc" class="write_scemo">
						<span class="scemo_ico"></span>
						<div class="scemo_list">
							<button type="button" class="scemo_add" onclick="javascript:add('■')">■</button>
							<button type="button" class="scemo_add" onclick="javascript:add('□')">□</button>
							<button type="button" class="scemo_add" onclick="javascript:add('▣')">▣</button>
							<button type="button" class="scemo_add" onclick="javascript:add('◈')">◈</button>
							<button type="button" class="scemo_add" onclick="javascript:add('◆')">◆</button>
							<button type="button" class="scemo_add" onclick="javascript:add('◇')">◇</button>
							<button type="button" class="scemo_add" onclick="javascript:add('♥')">♥</button>
							<button type="button" class="scemo_add" onclick="javascript:add('♡')">♡</button>
							<button type="button" class="scemo_add" onclick="javascript:add('●')">●</button>
							<button type="button" class="scemo_add" onclick="javascript:add('○')">○</button>
							<button type="button" class="scemo_add" onclick="javascript:add('▲')">▲</button>
							<button type="button" class="scemo_add" onclick="javascript:add('▼')">▼</button>
							<button type="button" class="scemo_add" onclick="javascript:add('▶')">▶</button>
							<button type="button" class="scemo_add" onclick="javascript:add('▷')">▷</button>
							<button type="button" class="scemo_add" onclick="javascript:add('◀')">◀</button>
							<button type="button" class="scemo_add" onclick="javascript:add('◁')">◁</button>
							<button type="button" class="scemo_add" onclick="javascript:add('☎')">☎</button>
							<button type="button" class="scemo_add" onclick="javascript:add('☏')">☏</button>
							<button type="button" class="scemo_add" onclick="javascript:add('♠')">♠</button>
							<button type="button" class="scemo_add" onclick="javascript:add('♤')">♤</button>
							<button type="button" class="scemo_add" onclick="javascript:add('♣')">♣</button>
							<button type="button" class="scemo_add" onclick="javascript:add('♧')">♧</button>
							<button type="button" class="scemo_add" onclick="javascript:add('★')">★</button>
							<button type="button" class="scemo_add" onclick="javascript:add('☆')">☆</button>
							<button type="button" class="scemo_add" onclick="javascript:add('☞')">☞</button>
							<button type="button" class="scemo_add" onclick="javascript:add('☜')">☜</button>
							<button type="button" class="scemo_add" onclick="javascript:add('▒')">▒</button>
							<button type="button" class="scemo_add" onclick="javascript:add('⊙')">⊙</button>
							<button type="button" class="scemo_add" onclick="javascript:add('㈜')">㈜</button>
							<button type="button" class="scemo_add" onclick="javascript:add('№')">№</button>
							<button type="button" class="scemo_add" onclick="javascript:add('㉿')">㉿</button>
							<button type="button" class="scemo_add" onclick="javascript:add('♨')">♨</button>
							<button type="button" class="scemo_add" onclick="javascript:add('™')">™</button>
							<button type="button" class="scemo_add" onclick="javascript:add('℡')">℡</button>
							<button type="button" class="scemo_add" onclick="javascript:add('∑')">∑</button>
							<button type="button" class="scemo_add" onclick="javascript:add('∏')">∏</button>
							<button type="button" class="scemo_add" onclick="javascript:add('♬')">♬</button>
							<button type="button" class="scemo_add" onclick="javascript:add('♪')">♪</button>
							<button type="button" class="scemo_add" onclick="javascript:add('♩')">♩</button>
							<button type="button" class="scemo_add" onclick="javascript:add('♭')">♭</button>
						</div>
						<div class="scemo_cls"><button type="button" class="scemo_cls_btn">닫기</button></div>
					</div>
					<button type="button" id="write_emo_btn" class="write_scemo_btn">이모<br>티콘</button>
					<div id="write_emo" class="write_scemo">
						<span class="scemo_ico"></span>
						<div class="scemo_list">
							<button type="button" class="scemo_add" onclick="javascript:add('*^^*')">*^^*</button>
							<button type="button" class="scemo_add" onclick="javascript:add('♡.♡')">♡.♡</button>
							<button type="button" class="scemo_add" onclick="javascript:add('@_@')">@_@</button>
							<button type="button" class="scemo_add" onclick="javascript:add('☞_☜')">☞_☜</button>
							<button type="button" class="scemo_add" onclick="javascript:add('ㅠ ㅠ')">ㅠ ㅠ</button>
							<button type="button" class="scemo_add" onclick="javascript:add('Θ.Θ')">Θ.Θ</button>
							<button type="button" class="scemo_add" onclick="javascript:add('^_~♥')">^_~♥</button>
							<button type="button" class="scemo_add" onclick="javascript:add('~o~')">~o~</button>
							<button type="button" class="scemo_add" onclick="javascript:add('★.★')">★.★</button>
							<button type="button" class="scemo_add" onclick="javascript:add('(!.!)')">(!.!)</button>
							<button type="button" class="scemo_add" onclick="javascript:add('⊙.⊙')">⊙.⊙</button>
							<button type="button" class="scemo_add" onclick="javascript:add('q.p')">q.p</button>
							<button type="button" class="scemo_add emo_long" onclick="javascript:add('┏( \'\')┛')">┏( \'\')┛</button>
							<button type="button" class="scemo_add emo_long" onclick="javascript:add('@)-)--')">@)-)--')</button>
							<button type="button" class="scemo_add emo_long" onclick="javascript:add('↖(^-^)↗')">↖(^-^)↗</button>
							<button type="button" class="scemo_add emo_long" onclick="javascript:add('(*^-^*)')">(*^-^*)</button>
						</div>
						<div class="scemo_cls"><button type="button" class="scemo_cls_btn">닫기</button></div>
					</div>

				</div>

				<!-- <div id="write_preset">
					{이름} : 받는사람 이름
				</div>
 -->
				<div id="write_reply">
					<br>[수신거부]1544-5462<br>헬프라인으로 연락주시면됩니다.</p>
				</div>

				<div id="write_recv" class="write_inner">
					<h2>받는사람</h2>
					<!-- <button type="button" class="write_floater write_floater_btn" onclick="hp_list_del()">선택삭제</button>

					<label for="hp_list" class="sound_only">받는사람들</label>
					<select name="hp_list" id="hp_list" size="5"></select> -->
					<?php if($gubun2=='ALL') { 
					  echo '전체발송';
					} else { 
						$arr_hp = explode(':', $hp_data);
						foreach ( $arr_hp as $hp )
						{
							echo $hp.'<br/>';
						}
					}?>

					
				</div>

				</td>
				<td width="100">
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				</td>
				<td valign="top">
				<div id="write_rsv" class="write_inner">
					<h2>예약전송</h2>

					<div class="write_floater">
						<label for="wr_booking"><span class="sound_only">예약전송 </span>사용</label>
						<input type="checkbox" name="wr_booking" id="wr_booking" onclick="booking(this.checked)">
					</div>

					<select name="wr_by" id="wr_by" disabled>
						<option value="<?php echo date('Y')?>"><?php echo date('Y')?></option>
						<option value="<?php echo date('Y')+1?>"><?php echo date('Y')+1?></option>
					</select>
					<label for="wr_by">년</label><br>
					<select name="wr_bm" id="wr_bm" disabled>
						<?php for ($i=1; $i<=12; $i++) { ?>
						<option value="<?php echo sprintf("%02d",$i)?>"<?php echo get_selected(date('m'), $i); ?>><?php echo sprintf("%02d",$i)?></option>
					<?php } ?>
					</select>
					<label for="wr_bm">월</label>
					<select name="wr_bd" id="wr_bd" disabled>
						<?php for ($i=1; $i<=31; $i++) { ?>
						<option value="<?php echo sprintf("%02d",$i)?>"<?php echo get_selected(date('d'), $i); ?>><?php echo sprintf("%02d",$i)?></option>
						<?php } ?>
					</select>
					<label for="wr_bd">일</label><br>
						<select name="wr_bh" id="wr_bh" disabled>
						<?php for ($i=0; $i<24; $i++) { ?>
						<option value="<?php echo sprintf("%02d",$i)?>"<?php echo get_selected(date('H')+1, $i); ?>><?php echo sprintf("%02d",$i)?></option>
						<?php } ?>
					</select>
					<label for="wr_bh">시</label>
					<select name="wr_bi" id="wr_bi" disabled>
						<?php for ($i=0; $i<=59; $i+=5) { ?>
						<option value="<?php echo sprintf("%02d",$i)?>"><?php echo sprintf("%02d",$i)?></option>
						<?php } ?>
					</select>
					<label for="wr_bi">분</label>
				</div>

				<div class="btn_confirm01 btn_confirm">
					<input type="submit" value="전송" class="btn_submit">
					<!-- <input type="submit" value="전송" onclick="send()"> -->
				</div>
				</td>
				</tr>
				</table>

				</form>
			</div>

		</div>


  </div>

</div>
<script>
	var is_mms_mode = false;
	var is_sms5_submitted = false;  //중복 submit방지

	var sms_check = null;

	function sms5_chk_send(f)
	{
		if( is_sms5_submitted == false ){
			is_sms5_submitted = true;
			var hp_list = document.getElementById('hp_list');
			var wr_message = document.getElementById('wr_message');
			var list = '';

			if (!wr_message.value) {
				alert('메세지를 입력해주세요.');
				wr_message.focus();
				is_sms5_submitted = false;
				return false;
			}
			
			if (hp_list.length < 1) {
				alert('받는 사람을 입력해주세요.');
				//hp_number.focus();
				is_sms5_submitted = false;
				return false;
			}

			//for (i=0; i<hp_list.length; i++)
			//	list += hp_list.options[i].value + '/';

			//w = document.body.clientWidth/2 - 200;
			//h = document.body.clientHeight/2 - 100;
			//act = window.open('sms_ing.php', 'act', 'width=300, height=200, left=' + w + ', top=' + h);
			//act.focus();

			//f.send_list.value = list;
			return true;
		} else {
			alert("데이터 전송중입니다.");
		}
	}

	function booking(val)
	{
		if (val)
		{
			document.getElementById('wr_by').disabled = false;
			document.getElementById('wr_bm').disabled = false;
			document.getElementById('wr_bd').disabled = false;
			document.getElementById('wr_bh').disabled = false;
			document.getElementById('wr_bi').disabled = false;
		}
		else
		{
			document.getElementById('wr_by').disabled = true;
			document.getElementById('wr_bm').disabled = true;
			document.getElementById('wr_bd').disabled = true;
			document.getElementById('wr_bh').disabled = true;
			document.getElementById('wr_bi').disabled = true;
		}
	}

	function add(str) {
		var conts = document.getElementById('wr_message');
		var bytes = document.getElementById('sms_bytes');
		conts.focus();
		conts.value+=str;
		byte_check('wr_message', 'sms_bytes');
		return;
	}

	function byte_check(wr_message, sms_bytes)
	{
		var conts = document.getElementById(wr_message);
		var bytes = document.getElementById(sms_bytes);
		var max_bytes = document.getElementById("sms_max_bytes");

		var i = 0;
		var cnt = 0;
		var exceed = 0;
		var ch = '';

		for (i=0; i<conts.value.length; i++)
		{
			ch = conts.value.charAt(i);
			if (escape(ch).length > 4) {
				cnt += 2;
			} else {
				cnt += 1;
			}
		}

		bytes.innerHTML = cnt;

		if (cnt > 80 && !is_mms_mode)
		{
			is_mms_mode = true;
			document.getElementById('kind').value = 'M';
			alert('sms 메시지 내용은 80바이트 이내입니다. mms 발송으로 전환됩니다.');
			return;
		} else if (cnt <= 80 && is_mms_mode) {
			is_mms_mode = false;
			document.getElementById('kind').value = 'S';
			alert('메시지 내용이 80바이트 이내이므로 sms 발송으로 전환됩니다.');	
			return;
		}
	}

	byte_check('wr_message', 'sms_bytes');
	document.getElementById('wr_message').focus();
</script>

<script>
$(function(){
	$(".box_txt").bind("focus keydown", function(){
        $("#wr_message_lbl").hide();
    });
    $(".write_scemo_btn").click(function(){
        $(".write_scemo").hide();
        $(this).next(".write_scemo").show();
    });
    $(".scemo_cls_btn").click(function(){
        $(".write_scemo").hide();
    });
	
	$(".sms_check", this).click(function() {
		sms_check = $(this).val();
		console.log(sms_check);
	});
});
</script>


</body>
</html>


<?php
$sub_menu = '900201';
include_once('./_common.php');
include_once(G5_LIB_PATH.'/PHPExcel-1.8/Classes/PHPExcel.php');

// 상품이 많을 경우 대비 설정변경
ini_set('memory_limit', '50M');

auth_check($auth[$sub_menu], "w");

$UpFile	= $_FILES["excel_file"];

$UpFileName		= iconv( "UTF-8", "EUC-KR", filefilter($UpFile["name"]) );

$UpFilePathInfo = pathinfo($UpFileName);
$UpFileExt		= strtolower($UpFilePathInfo["extension"]);

if($UpFileExt != "xls" && $UpFileExt != "xlsx") {
    echo "<script>
			alert('엑셀파일만 업로드 가능합니다.');
		  </script>";
    exit;
}

//업로드된 엑셀파일을 서버의 지정된 곳에 옮기기 위해 경로 적절히 설정
$upload_path		= G5_DATA_PATH."/upload_excel";
$MakeUpFileName		= date("Ymd_His")."_".$UpFileName; //utf8 변경 파일명 utf8로 변경하지 않으면 한글파일명일때 안됨
$EucFileName		= date("Ymd_His")."_".filefilter($UpFile["name"]); //디비에 들어갈 파일명
$upfile_path		= $upload_path."/".$MakeUpFileName;

if(is_uploaded_file($UpFile["tmp_name"])) {

    if(!move_uploaded_file($UpFile["tmp_name"],$upfile_path)) {
        echo "<script>
				alert('업로드된 파일을 옮기는 중 에러가 발생했습니다.');
			  </script>";
        exit;
    }

    /*************** 트랜잭션 관련 ****************/
    $error_cnt = 0;
    $ErrorMsg = "";
    mysqli_autocommit($g5['connect_db'], false);
    /*************** 트랜잭션 관련 ****************/

    $sql = " insert into sms_excel_fileinfo (kind, f_name, f_content, f_regid, f_regip)
			 values('{$kind}', '{$EucFileName}', '{$f_content}', '{$member['mb_id']}', '{$_SERVER['REMOTE_ADDR']}'); ";
    sql_query($sql);
    /*************** 트랜잭션 관련 ****************/
    if( mysqli_errno($g5['connect_db']) ){
        $error_cnt += 1;
    }
    /*************** 트랜잭션 관련 ****************/

    $sql = " select LAST_INSERT_ID() as f_idx ";
    $row = sql_fetch($sql);
    $f_idx = $row['f_idx'];

    try {

        // 업로드한 PHP 파일을 읽어온다.
        $objPHPExcel = PHPExcel_IOFactory::load($upfile_path);
        $sheetsCount = 1;

        // 시트Sheet별로 읽기
        for($sheet = 0; $sheet < $sheetsCount; $sheet++) {

            $objPHPExcel -> setActiveSheetIndex($sheet);
            $activesheet = $objPHPExcel -> getActiveSheet();
            $highestRow = $activesheet -> getHighestRow();          // 마지막 행
            $highestColumn = $activesheet -> getHighestColumn();    // 마지막 컬럼

            // 한줄읽기
            for($row = 1; $row <= $highestRow; $row++) {

                // $rowData가 한줄의 데이터를 셀별로 배열처리 된다.
                $rowData = $activesheet -> rangeToArray("A" . $row . ":" . $highestColumn . $row, NULL, TRUE, FALSE);

                // $rowData에 들어가는 값은 계속 초기화 되기때문에 값을 담을 새로운 배열을 선안하고 담는다.
                $allData[$row] = $rowData[0];
            }
        }

    } catch(exception $exception) {
        $error_cnt += 1;
    }

    $GetCnt = count($allData);


    //파일데이터 넣어주기
    if( $GetCnt > 0 ){

        $sql = "";

        //1행타이틀 제외
        for( $k = 2; $k <= $GetCnt; $k++ ){

            if (!$allData[$k][5]) continue;

            $sql = " insert into sms_excel_temp ( ";
            $sql = $sql . " f_idx , ";
            $sql = $sql . " mb_id , ";
            $sql = $sql . " mb_name , ";
            $sql = $sql . " mb_sex , ";
            $sql = $sql . " mb_birth , ";
            $sql = $sql . " buy_ct , ";
            $sql = $sql . " mb_hp , ";
            $sql = $sql . " f_regdate) values ( ";
            $sql = $sql . " {$f_idx}, ";
            $sql = $sql . " '{$allData[$k][0]}', ";
            $sql = $sql . " '{$allData[$k][1]}', ";
            $sql = $sql . " '{$allData[$k][2]}', ";
            $sql = $sql . " '{$allData[$k][3]}', ";
            $sql = $sql . " '{$allData[$k][4]}', ";
            $sql = $sql . " '{$allData[$k][5]}', ";
            $sql = $sql . " NOW() ); " ;
//debug2($sql);exit;
            sql_query($sql);
            /*************** 트랜잭션 관련 ****************/
            if( mysqli_errno($g5['connect_db']) ){
                $error_cnt += 1;
                $ErrorMsg .= " {$k}행 ";
            }
            /*************** 트랜잭션 관련 ****************/

        }

    }

    /*************** 트랜잭션 관련 ****************/
    if ($error_cnt > 0) {
        mysqli_rollback($g5['connect_db']);
        mysqli_close($g5['connect_db']);
        echo "
		<script>
			alert('데이터베이스의 에러로 인해 롤백되었습니다.');
			parent.document.getElementById('ErrorView').innerHTML='[".$ErrorMsg."]';
		</script>
		";
        exit;
    } else {
        mysqli_commit($g5['connect_db']);
    }
    /*************** 트랜잭션 관련 ****************/

    echo "<script>
			alert('엑셀 임시저장이 완료되었습니다.');
			parent.location.reload();
		  </script>";
    exit;

}else{

    echo "<script>
			alert('파일등록에 실패하였습니다.');
			parent.location.reload();
		  </script>";
    exit;
}

?>
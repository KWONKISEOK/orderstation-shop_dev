<?php
$sub_menu = "900300";
include_once("./_common.php");
include_once (G5_ADMIN_PATH.'/admin.head.nomenu.php');
$sql_common = " from {$g5['member_table']} a ";

$sql_search = " where (1) ";
if ($stx) {
    $sql_search .= " and ( ";
    switch ($sfl) {
        
        case 'mb_name' :
        case 'pharm_custno' :
            $sql_search .= " ({$sfl} like '%{$stx}') ";
            break;
		case 'pharm_name' :
            $sql_search .= " ({$sfl} like '%{$stx}%') ";
            break;
        default :
            $sql_search .= " ({$sfl} like '{$stx}%') ";
            break;
    }
    $sql_search .= " ) ";
}
if ($is_admin != 'super')
    $sql_search .= " and mb_level <= '{$member['mb_level']}' ";

if ($is_admin == 'sales')
    $sql_search .= " and mb_manager = '{$member['mb_id']}' ";

    $sql_search .= " and mb_type in ('0', '1') ";

//상태
if ($pharm_status !='') {
    $sql_search .= " and pharm_status = '$pharm_status' ";
}
if ($mb_referee !='') {
    $sql_search .= " and mb_referee = '$mb_referee' ";
}
if ($mb_type !='') {
    $sql_search .= " and mb_type = '$mb_type' ";
}
if ($mb_sms !='') {
    $sql_search .= " and mb_sms = '$mb_sms' ";
}

if ($pharm_sido) {
    $sql_search .= " and pharm_sido = '$pharm_sido' ";
}
if ($pharm_gugun) {
    $sql_search .= " and pharm_gugun = '$pharm_gugun' ";
}
if ($pharm_dong) {
    $sql_search .= " and pharm_dong = '$pharm_dong' ";
}



if (!$sst) {
    $sst = "mb_datetime";
    $sod = "desc";
}

$sql_order = " order by {$sst} {$sod} ";

$sql = " select count(*) as cnt {$sql_common} {$sql_search} {$sql_order} ";
$row = sql_fetch($sql);
$total_count = $row['cnt'];

$rows = $config['cf_page_rows'];
$total_page  = ceil($total_count / $rows);  // 전체 페이지 계산
if ($page < 1) $page = 1; // 페이지가 없으면 첫 페이지 (1 페이지)
$from_record = ($page - 1) * $rows; // 시작 열을 구함

$g5['title'] = '약국회원관리';
//include_once('./admin.head.php');

$qstr = "mb_referee=$mb_referee&amp;pharm_status=$pharm_status&amp;mb_type=$mb_type&amp;mb_sms=$mb_sms&amp;pharm_sido=$pharm_sido&amp;pharm_gugun=$pharm_gugun&amp;pharm_dong=$pharm_dong&amp;sfl=$sfl&amp;stx=$stx";


$sql = " select a.* 
		{$sql_common} {$sql_search} {$sql_order} limit {$from_record}, {$rows} ";

$result = sql_query($sql);

$colspan = 6;
?>
<style>
.container_wr {
    padding: 6px;
}
</style>
<form id="fsearch" name="fsearch" class="local_sch01 local_sch" method="get">


<div class="local_sch03  tbl_wrap" style="padding:10px;">
    <table>
   
    <tbody>
	<tr>
        <th scope="row">추천회원구분</th>
        <td>
			<select name="mb_referee" id="mb_referee"  >
			    <option value="">==전체==</option>
				<option value="0" <?php echo get_selected($mb_referee, '0'); ?>>일반회원</option>
				<option value="1" <?php echo get_selected($mb_referee, '1'); ?>>OS추천회원</option>
				<option value="2" <?php echo get_selected($mb_referee, '2'); ?>>모니터링회원</option>
				<option value="3" <?php echo get_selected($mb_referee, '3'); ?>>OS서팩2회원</option>
				<option value="4" <?php echo get_selected($mb_referee, '4'); ?>>OS직원</option>
			</select>
			
        </td>
		<th scope="row">회원구분</th>
        <td>
            <select name="mb_type" id="mb_type" >
				<option value="">==전체==</option>
				<option value="1" <?php echo get_selected($mb_type, '1'); ?>>약국</option>
				<option value="0" <?php echo get_selected($mb_type, '0'); ?>>소비자</option>
			</select>
        </td>
		
		<th scope="row">수신여부</th>
        <td>
           <select name="mb_sms" id="mb_sms" >
				<option value="">==전체==</option>
				<option value="1" <?php echo get_selected($mb_sms, '1'); ?>>수신</option>
				<option value="0" <?php echo get_selected($mb_sms, '0'); ?>>미수신</option>
			</select>
        </td>
		<th scope="row">사용여부</th>
        <td>
           <select name="pharm_status" id="pharm_status" >
				<option value="">==전체==</option>
				<option value="N" <?php echo get_selected($pharm_status, 'N'); ?>>신규가입</option>
				<option value="L" <?php echo get_selected($pharm_status, 'L'); ?>>ERP등록중</option>
				<option value="1" <?php echo get_selected($pharm_status, '1'); ?>>사용대기</option>
				<option value="2" <?php echo get_selected($pharm_status, '2'); ?>>사용(승인)</option>
				<option value="3" <?php echo get_selected($pharm_status, '3'); ?>>사용안함</option>
			</select>
        </td>
	
		
		<td rowspan="2">
			 <input type="submit" value="검색" class="get_theme_confc btn btn_01">
			
		</td>
    </tr>
	
	<tr>

     
		<th scope="row">지역</th>
        <td colspan='3'>
            <select name="pharm_sido" id="pharm_sido" onChange="onChangeAddr(1)">
				<option value="">=시도=</option>
				<?php
				$sql = "SELECT distinct sido ";
				$sql .= "FROM tbl_addr ";
				$sql .= "ORDER BY sido ";
				$res = sql_query($sql);
				for ($i=0; $row=sql_fetch_array($res); $i++) {
					$sido = $row["sido"];
					?>
					<option value="<?php echo $sido; ?>" <?php if ( $pharm_sido == $sido ) {
						echo "selected";
					} ?>><?php echo $sido; ?></option>
					<?php
				}
				?>
			</select>
			<select name="pharm_gugun" id="pharm_gugun" onchange="onChangeAddr(2,2)">
				<option value="">==전체==</option>
				<?php
				$sql = "SELECT distinct gugun ";
				$sql .= "FROM tbl_addr ";
				$sql .= " where sido =  '".$pharm_sido. "'";
				$sql .= "ORDER BY gugun ";
				$res = sql_query($sql);
				for ($i=0; $row=sql_fetch_array($res); $i++) {
					$gugun = $row["gugun"];
					?>
					<option value="<?php echo $gugun; ?>" <?php if ( $pharm_gugun == $gugun ) {
						echo "selected";
					} ?>><?php echo $gugun; ?></option>
					<?php
				}
				?>
			</select>
			<select name="pharm_dong" id="pharm_dong" >
				<option value="">==전체==</option>
				<?php
				$sql = "SELECT distinct dong ";
				$sql .= "FROM tbl_addr ";
				$sql .= " where gugun =  '".$pharm_gugun. "'";
				$sql .= "ORDER BY dong ";
				$res = sql_query($sql);
				for ($i=0; $row=sql_fetch_array($res); $i++) {
					$dong = $row["dong"];
					?>
					<option value="<?php echo $dong; ?>" <?php if ( $pharm_dong == $dong ) {
						echo "selected";
					} ?>><?php echo $dong; ?></option>
					<?php
				}
				?>
			</select>
        </td>
		
		<th scope="row">검색</th>
        <td>
            <select name="sfl" id="sfl" >
			<option value="pharm_name" <?php echo get_selected($sfl, 'pharm_name'); ?>>약국명</option>
			<option value="mb_name" <?php echo get_selected($sfl, 'mb_name'); ?>>이름</option>
			<option value="mb_id" <?php echo get_selected($sfl, 'mb_id'); ?>>아이디</option>
			<option value="pharm_custno" <?php echo get_selected($sfl, 'pharm_custno'); ?>>약국코드</option>
			
			</select>
			<label for="stx" class="sound_only">검색어</label>
			<input type="text" name="stx" value="<?php echo $stx ?>" id="stx" class=" frm_input">
			<!-- <input type="submit" class="btn_submit" value="검색"> -->
        </td>
		
    </tr>
	
    </tbody>
    </table>
</div>



</form>

<!-- <div class="local_desc01 local_desc">
    <p>
        회원자료 삭제 시 다른 회원이 기존 회원아이디를 사용하지 못하도록 회원아이디, 이름, 닉네임은 삭제하지 않고 영구 보관합니다.
    </p>
</div> -->


<form name="fmemberlist" id="fmemberlist" action="" onsubmit="return fmemberlist_submit(this);" method="post">
<input type="hidden" name="sst" value="<?php echo $sst ?>">
<input type="hidden" name="sod" value="<?php echo $sod ?>">
<input type="hidden" name="sfl" value="<?php echo $sfl ?>">
<input type="hidden" name="stx" value="<?php echo $stx ?>">


<input type="hidden" name="page" value="<?php echo $page ?>">
<input type="hidden" name="token" value="">

<div class="tbl_head01 tbl_wrap">
    <table>
    <caption><?php echo $g5['title']; ?> 목록</caption>
    <thead>
    <tr>
        <th scope="col" id="mb_list_chk" >
            <label for="chkall" class="sound_only">회원 전체</label>
            <input type="checkbox" name="chkall" value="1" id="chkall" onclick="check_all(this.form)">
        </th>
        <th scope="col" id="mb_list_id" >아이디</th>
		<th scope="col" id="mb_list_name">이름</th>
        <th scope="col" id="mb_list_nick">연락처</th>
		<th scope="col" id="mb_list_auth">수신여부</th>
		<th scope="col" id="mb_list_auth">가입일</th>
	
    </tr>
    </thead>
    <tbody>
    <?php
    for ($i=0; $row=sql_fetch_array($result); $i++) {

          $bg = 'bg'.($i%2);

		$mb_time       = substr($row["mb_datetime"], 0, 10);
	

    ?>

    <tr class="<?php echo $bg; ?>">
        
        <td headers="mb_list_chk" class="td_chk" >
            <input type="hidden" name="mb_hp[<?php echo $i ?>]" value="<?php echo $row['mb_hp'] ?>" id="mb_hp_<?php echo $i ?>">
			<input type="hidden" name="mb_name[<?php echo $i ?>]" value="<?php echo $row['mb_name'] ?>" id="mb_name_<?php echo $i ?>">
            <label for="chk_<?php echo $i; ?>" class="sound_only"><?php echo get_text($row['mb_name']); ?> <?php echo get_text($row['mb_nick']); ?>님</label>
            <input type="checkbox" name="chk[]" value="<?php echo $i ?>" id="chk_<?php echo $i ?>">
        </td>

		<td align="center"><?php echo $row["mb_id"]; ?></td>
		<td align="center"><?php echo $row["mb_name"]; ?></td>
		<td align="center"><?php echo $row["mb_hp"]; ?></td>		
		<td align="center"><?php echo ($row["mb_sms"] ? 'Y' : 'N'); ?> </td>		
		<td align="center"><?php echo $mb_time; ?></td>

	</tr>

   <?php
    }
	?>
	<tr>

	  <td colspan=13 style="text-align: left;">
		
		<div class="local_cmd01 ">
		<input type="button" value="선택추가" class="btn_submit" onclick="flist_submit();">   
		</div>
	
	  </td>
	</tr>
	<?php
    if ($i == 0)
        echo "<tr><td colspan=\"".$colspan."\" class=\"empty_table\">자료가 없습니다.</td></tr>";
    ?>
    </tbody>
    </table>
</div>


</form>

<?php 


echo get_paging( $config['cf_write_pages'], $page, $total_page, "{$_SERVER['SCRIPT_NAME']}?$qstr&amp;page="); ?>

<script>
function check_all(f)
{
    var chk = document.getElementsByName("chk[]");

    for (i=0; i<chk.length; i++)
        chk[i].checked = f.chkall.checked;
}
function flist_submit()
{
	
	//if (!is_checked("chk[]")) {
    //    alert("등록 하실 항목을 하나 이상 선택하세요.");
    //    return false;
    //}
	
	var f = document.fmemberlist;
	var chk = document.getElementsByName("chk[]");
	var mb_hps = '';
	for (var i=0; i<chk.length; i++)
	{
		if (chk[i].checked)
		{
			var k = chk[i].value;
			var mb_name      = f.elements['mb_name['+k+']'].value;
			var mb_hp      = f.elements['mb_hp['+k+']'].value;			
			parent.sms_add(mb_name, mb_hp);
		}
	}	
	
}
</script>
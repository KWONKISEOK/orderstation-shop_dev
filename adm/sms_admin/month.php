<?php
$sub_menu = '900100';
include_once('./_common.php');

$start_year = 2018;

$current_year = date('Y');
$current_month = date('m');

if (!$year) $year = $current_year;
if (!$month) $month = $current_month; 

$colspan = 4;

auth_check($auth[$sub_menu], "r");

$g5['title'] = "SMS 관리";

$sms_history_table = "orderstation_sms.suredata_log_$year$month";

$total_res = sql_fetch("select count(*) as cnt from $sms_history_table");
$total_count = $total_res['cnt'];

include_once(G5_ADMIN_PATH.'/admin.head.php');
?>

<form name="search_form" method="get" action="<?echo $_SERVER['SCRIPT_NAME']?>" class="local_sch01 local_sch" >
<select name="year" id="year" onchange="search_form.submit();">
	<?php for ($i = $start_year; $i <= $current_year; $i++) {?>
    <option value="<?php echo $i?>"<?php echo get_selected($i, $year);?>><?php echo $i ?>년</option>
	<?php }?>
</select>
<select name="month" id="month" onchange="search_form.submit();">
	<?php for ($i = 1; $i <= 12; $i++) { 
		$j = sprintf("%02d", $i);
	?>
    <option value="<?php echo $j; ?>"<?php echo get_selected($j, $month);?>><?php echo $j; ?>월</option>
	<?php } ?>	
</select>
</form>

<div class="tbl_head01 tbl_wrap">
    <table>
    <caption><?php echo $g5['title']; ?> 목록</caption>
    <thead>
    <tr>
        <th scope="col">일자</th>
        <th scope="col">총건수</th>
        <th scope="col">성공</th>
        <th scope="col">실패</th>
     </tr>
     </thead>
     <tbody>
        <?php if (!$total_count) { ?>
        <tr>
            <td colspan="<?php echo $colspan; ?>" class="empty_table" >
                데이터가 없습니다.
            </td>
        </tr>
		<?php
		}
		
		$sql = "select 
					left(intime, 8) as day
				  , count(*) as total_cnt
				  , (select count(*) from $sms_history_table where left(intime, 8) = day and result = 2) as success_cnt
				  , (select count(*) from $sms_history_table where left(intime, 8) = day and result <> 2) as fail_cnt
				from 
					$sms_history_table
				group by left(intime, 8) 
				order by left(intime, 8) desc";
		//echo $sql;
		$qry = sql_query($sql);
		
		$success_count = 0;
		$fail_count = 0;
		while($res = sql_fetch_array($qry)) {
			$bg = 'bg'.($line++%2);
			$success_count += $res['success_cnt'];
			$fail_count += $res['fail_cnt'];			
		?>
		<tr class="<?php echo $bg; ?>">
			<td class="td_datetime"><a href="day.php?date=<?php echo date('Ymd', strtotime($res['day']))?>"><?php echo date('Y-m-d', strtotime($res['day']))?></a></td>
			<td class="td_num"><?php echo number_format($res['total_cnt'])?></td>		
			<td class="td_num"><?php echo number_format($res['success_cnt'])?></td>
			<td class="td_num"><?php echo number_format($res['fail_cnt'])?></td>
		</tr>
		<?php } ?>
        <?php if ($total_count) { ?>
		<tr>
			<td>합계</td>
			<td class="td_num"><?php echo number_format($total_count)?></td>		
			<td class="td_num"><?php echo number_format($success_count)?></td>
			<td class="td_num"><?php echo number_format($fail_count)?></td>
		</tr>
		<?php } ?>
    </tbody>
    </table>
</div>

<?php echo get_paging(G5_IS_MOBILE ? $config['cf_mobile_pages'] : $config['cf_write_pages'], $page, $total_page, $_SERVER['SCRIPT_NAME']."?st=$st&amp;sv=$sv&amp;page="); ?>

<?php
include_once(G5_ADMIN_PATH.'/admin.tail.php');
?>
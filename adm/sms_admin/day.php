<?php
$sub_menu = '900100';
include_once('./_common.php');

if (!$date) alert('SMS 발송리스트가 존재하지 않습니다.');

$colspan = 5;

auth_check($auth[$sub_menu], "r");

$g5['title'] = "SMS 관리";

$sms_history_table = "orderstation_sms.suredata_log_".substr($date, 0, 6);

$sql_search = " and left(intime, 8) = '$date' ";

$total_res = sql_fetch("select count(*) as cnt from $sms_history_table where 1 $sql_search");
$total_count = $total_res['cnt'];

include_once(G5_ADMIN_PATH.'/admin.head.php');
?>

<div class="btn_fixed_top">
	<a href="javascript:history.go(-1);" class="btn btn_02">목록</a>
</div>

<div class="local_sch03  tbl_wrap" style="padding:10px;">
	▶ 발송 리스트 (<?php echo date('Y-m-d', strtotime($date)); ?>)
</div>
<div class="tbl_head01 tbl_wrap">
    <table>
    <caption><?php echo $g5['title']; ?> 목록</caption>
    <thead>
    <tr>
        <th scope="col">보내는 번호</th>
        <th scope="col">받는 번호</th>
        <th scope="col">메시지</th>
        <th scope="col">전송여부</th>
		<th scope="col">타입</th>
     </tr>
     </thead>
     <tbody>
        <?php if (!$total_count) { ?>
        <tr>
            <td colspan="<?php echo $colspan; ?>" class="empty_table" >
                데이터가 없습니다.
            </td>
        </tr>
		<?php
		}
		
		$sql = "select * from $sms_history_table where 1 $sql_search order by seqno desc ";
		//echo $sql;
		$qry = sql_query($sql);
		
		$success_count = 0;
		$fail_count = 0;
		while($res = sql_fetch_array($qry)) {
			$bg = 'bg'.($line++%2);
			$success_count += $res['success_cnt'];
			$fail_count += $res['fail_cnt'];
		?>
		<tr class="<?php echo $bg; ?>">
			<td><?php echo $res['REQPHONE']?></td>
			<td><?php echo $res['CALLPHONE']?></td>
			<td class="td_left"><?php echo $res['MSG']?></td>
			<td>
				<?php echo surem_result($res['RESULT'])?> 
				<?php if ($res['RESULT'] != 'Y' && $res['RESULT'] != '2') echo "(".surem_error_code($res['RESULT'], $res['ERRCODE']).")"; ?>
			</td>
			<td><?php echo surem_kind($res['KIND'])?></td>
		</tr>
		<?php } ?>
    </tbody>
    </table>
</div>

<?php echo get_paging(G5_IS_MOBILE ? $config['cf_mobile_pages'] : $config['cf_write_pages'], $page, $total_page, $_SERVER['SCRIPT_NAME']."?st=$st&amp;sv=$sv&amp;page="); ?>

<?php
include_once(G5_ADMIN_PATH.'/admin.tail.php');
?>
<?php
$sub_menu = "900300";
include_once("./_common.php");
include_once (G5_ADMIN_PATH.'/admin.head.nomenu.php');

?>

	
<?php
if ($buy_months !='') {
    $buy_months = $buy_months;
}else{
	$buy_months = '3';
}

$sql = " SELECT COUNT(*)as cnt
from(select a.od_name AS od_name,replace(REPLACE(c.od_b_hp,'-',''), ' ' ,'') AS CALLPHONE from tbl_shop_order a inner join tbl_shop_order_detail b on a.od_id = b.od_id                           
                                                  inner join tbl_shop_order_receiver c on b.od_id = c.od_id and b.od_num = c.od_num                                                                
                                                  left join(                                 
                                                         select b.mb_id , min(a.od_id) AS od_id , max(b.mb_type) as mb_type                                 
                                                         from tbl_shop_order a inner join tbl_member b ON a.mb_id = b.mb_id AND a.od_status != '취소'                              
                                                         WHERE od_receipt_time != '0000-00-00 00:00:00' and mb_datetime <= od_receipt_time                                 
                                                         GROUP BY b.mb_id                                                                                          
                                                  ) e ON a.mb_id = e.mb_id AND a.od_id = e.od_id                                  
                                                  left join tbl_member h ON a.mb_id = h.mb_id                                 
                                 where 1=1 AND a.service_type is null And  c.od_date1 BETWEEN DATE_ADD(Now(),INTERVAL -$buy_months MONTH) AND now() AND c.od_status!='취소' 
								 and instr( c.od_status , '반품' ) <= 0  AND replace(replace(c.od_b_hp,'-',''), ' ' ,'') IN(SELECT replace(replace(mb_hp,'-',''), ' ' ,'') FROM tbl_member WHERE service_type IS NULL and mb_sms='1') GROUP BY CALLPHONE)grp";
$row = sql_fetch($sql);
$total_count = $row['cnt'];

$rows = $config['cf_page_rows'];
$total_page  = ceil($total_count / $rows);  // 전체 페이지 계산
if ($page < 1) $page = 1; // 페이지가 없으면 첫 페이지 (1 페이지)
$from_record = ($page - 1) * $rows; // 시작 열을 구함
//echo $from_record;
?>
<?php
$g5['title'] = '최근3개월구매자';
//include_once('./admin.head.php');

$qstr = "buy_months=$buy_months&amp;mb_referee=$mb_referee&amp;pharm_status=$pharm_status&amp;mb_type=$mb_type&amp;mb_sms=$mb_sms&amp;sfl=$sfl&amp;stx=$stx";


$sql = " select c.od_b_name AS od_name,replace(REPLACE(c.od_b_hp,'-',''), ' ' ,'') AS CALLPHONE from tbl_shop_order a inner join tbl_shop_order_detail b on a.od_id = b.od_id                           
                                                  inner join tbl_shop_order_receiver c on b.od_id = c.od_id and b.od_num = c.od_num                                                               
                                                  left join(                                 
                                                         select b.mb_id , min(a.od_id) AS od_id , max(b.mb_type) as mb_type                                 
                                                         from tbl_shop_order a inner join tbl_member b ON a.mb_id = b.mb_id AND a.od_status != '취소'                              
                                                         WHERE od_receipt_time != '0000-00-00 00:00:00' and mb_datetime <= od_receipt_time                                 
                                                         GROUP BY b.mb_id                                                                                          
                                                  ) e ON a.mb_id = e.mb_id AND a.od_id = e.od_id                                  
                                                  left join tbl_member h ON a.mb_id = h.mb_id                                 
                                 where 1=1 AND a.service_type is null AND  c.od_date1 BETWEEN DATE_ADD(Now(),INTERVAL -$buy_months MONTH) AND now() AND c.od_status!='취소' 
								 and instr( c.od_status , '반품' ) <= 0  AND replace(replace(c.od_b_hp,'-',''), ' ' ,'') IN(SELECT replace(replace(mb_hp,'-',''), ' ' ,'') FROM tbl_member WHERE service_type IS NULL AND mb_sms='1') GROUP BY CALLPHONE order by od_name asc limit {$from_record}, {$rows}";

$result = sql_query($sql);

$colspan = 2;

?>

<style>
.container_wr {
    padding: 6px;
}
</style>


<!-- <div class="local_desc01 local_desc">
    <p>
        회원자료 삭제 시 다른 회원이 기존 회원아이디를 사용하지 못하도록 회원아이디, 이름, 닉네임은 삭제하지 않고 영구 보관합니다.
    </p>
</div> -->


<form id="fsearch" name="fsearch" class="local_sch01 local_sch" method="get">

<input type="hidden" name="page" value="<?php echo $page ?>">

<input type="hidden" name="token" value="">
<div class="local_sch03  tbl_wrap" style="padding:10px;">

    <table>
   
    <tbody>
	<tr>
	<?php echo '[조회결과] 총 '.$total_count.'명(sms 수신동의)';?>
	</tr>
	<tr>
        
		<th scope="row">기간선택</th>
        <td>
          최근<select name="buy_months" id="buy_months">
		   <option value="<?echo $buy_months?>">==전체==</option>
		   <?php  for($k = 1; $k < 13 ; $k++){?>
				
				<option value="<?php echo $k?>" <?php  if($buy_months=="$k"){  echo "selected";  }?> ><?php echo $k;?></option>
		   <?php } ?>
			</select>개월
			&nbsp;&nbsp;&nbsp;
			 <input type="submit" value="선택" class="get_theme_confc btn btn_01">
			 기본 셋팅값은 3개월 입니다. 다른 기간은 개월 설정 후 선택 꼭 눌러주세요. 
        </td>
		
    </tr>
	
    </tbody>
    </table>
</div>


<div class="tbl_head01 tbl_wrap">
    <table>
    <caption><?php echo $g5['title']; ?> 목록</caption>
    <thead>
    <tr>
 
		<th scope="col" id="mb_list_name">이름</th>
        <th scope="col" id="mb_list_nick">연락처</th>
		
	
    </tr>
    </thead>
	
    <tbody>
    <?php
	
	
    for ($i=0; $row=sql_fetch_array($result); $i++) {

          $bg = 'bg'.($i%2);
	

    ?>

    <tr class="<?php echo $bg; ?>">
   
		
		<td align="center"><?php echo $row["od_name"]; ?></td>
		<td align="center"><?php echo $row["CALLPHONE"]; ?></td>		
		
	</tr>

   <?php
    }
	?>
	<tr>

	 
	</tr>
	<?php
    if ($i == 0)
        echo "<tr><td colspan=\"".$colspan."\" class=\"empty_table\">자료가 없습니다.</td></tr>";
    ?>
    </tbody>
    </table>
</div>


<div id="preloader">
	 <img src="/adm/jqGrid/waiting.gif" align="absmiddle"> Loading...		
</div>


</form>
<script>

$(document).ready(function(){
		$("#preloader").hide();
		$('#fsearch').submit(function(){

            $("#preloader").show();

		});

	var months_value = $("#buy_months option:selected").val();

	parent.form_sms.buy_months.value = months_value;

});
</script>

<?php 


echo get_paging( $config['cf_write_pages'], $page, $total_page, "{$_SERVER['SCRIPT_NAME']}?$qstr&amp;page="); ?>

<?php
$sub_menu = "900201";
include_once("./_common.php");
include_once(G5_LIB_PATH."/surem.lib.php");

ini_set('memory_limit', '50M');

$g5['title'] = "문자전송중";

if (!$f_idx)
    alert('확인되지 않는 데이터입니다.');

$sql = "SELECT * FROM sms_excel_fileinfo WHERE f_idx='$f_idx'";
$row = sql_fetch($sql);

if (!$row)
    alert('확인되지 않는 데이터입니다.');

$wr_message = clean_xss_tags(trim(addslashes($row['f_content'])));
if (!$wr_message)
    alert('메세지를 입력해주세요.');

// 회원구분(0:일반, 1:약국, 5:영업자, 7:공급사, 9:관리자)
$subject = '';
$msg = $wr_message;
$reqtime = $wr_booking ? $wr_by.$wr_bm.$wr_bd.$wr_bh.$wr_bi : '00000000000000';
$result = $wr_booking ? 'R' : '0';
$kind = $row['kind'];

$surem = new SureM();				// testing
//$surem = new SureM('development');    // development
//$surem = new SureM('production');	// production

$list = array();
$hps = array();

$sql = "SELECT mb_name, mb_hp FROM sms_excel_temp WHERE f_idx='$f_idx'";
$sql_result = sql_query($sql);

$send_list = '';
for ($i=0; $row=sql_fetch_array($sql_result); $i++) {
    $send_list .= $row['mb_name'].':'.$row['mb_hp'].'/';
}

$send_list = explode('/', $send_list);
$wr_overlap = 1; // 중복번호를 체크함
$overlap = 0;
$duplicate_data = array();
$duplicate_data['hp'] = array();
$str_serialize = "";

for ($i = 0; $i < count($send_list); $i++) {

    $item = explode(':', $send_list[$i]);

    $hp = get_hp($item[1], 0);
    $name = $item[0];

    if (!$hp) continue;

    if ($wr_overlap && array_overlap($hps, $hp)) {
        $overlap++;
        array_push($duplicate_data['hp'], $hp);
        continue;
    }

    array_push($list, array('bk_hp' => $hp, 'bk_name' => $name));
    array_push($hps, $hp);
}

if (count($duplicate_data['hp'])) { //중복된 번호가 있다면
    $duplicate_data['total'] = $overlap;
    $str_serialize = serialize($duplicate_data);
}

$total_count = count($list);

for ($i = 0; $i < $total_count; $i++) {

    $callname = $list[$i]['bk_name'];
    $callphone = str_replace('-', '', $list[$i]['bk_hp']);

    $insert_sql = $surem->insert_query_builder($callname, $callphone, $subject, $msg, $reqtime, $result, $kind);
//    echo $insert_sql;exit;
    sql_query($insert_sql);
}

// 성공 처리
$sql = "UPDATE sms_excel_fileinfo SET f_status=1 WHERE f_idx='$f_idx'";
$sql_result = sql_query($sql);

alert_go_page('전체 '.($total_count+$overlap).'건 중 '.$total_count.'건을 처리 완료하였습니다. (중복번호 : '.$overlap.'건)');


function alert_go_page($msg)
{
    $html = "<script>
	alert('$msg');
	location.href = 'send_excel.php';</script>";

    echo $html;
    exit;
}

include_once(G5_ADMIN_PATH.'/admin.tail.php');
?>
<?php
$sub_menu = "900300";
include_once("./_common.php");
include_once (G5_ADMIN_PATH.'/admin.head.nomenu.php');
$sql_common = " from {$g5['member_table']} a ";

$sql_common = " from {$g5['member_table']} a ";

$sql_search = " where (1) ";
if ($stx) {
    $sql_search .= " and ( ";
    switch ($sfl) {
        
        case 'mb_name' :
        case 'mb_id' :
            $sql_search .= " ({$sfl} like '%{$stx}%') ";
            break;
		case 'comp_name' :
		case 'comp_md_name' :
            $sql_search .= " ({$sfl} like '%{$stx}%') ";
            break;
        default :
            $sql_search .= " ({$sfl} like '{$stx}%') ";
            break;
    }
    $sql_search .= " ) ";
}
if ($is_admin != 'super')
    $sql_search .= " and mb_level <= '{$member['mb_level']}' ";

if ($pharm_status)
    $sql_search .= " and pharm_status = '$pharm_status' ";

//if ($_GET['mb_type'] != '')
    $sql_search .= " and mb_type = '7' ";


if (!$sst) {
    $sst = "mb_datetime";
    $sod = "desc";
}

$sql_order = " order by {$sst} {$sod} ";

$sql = " select count(*) as cnt {$sql_common} {$sql_search} {$sql_order} ";
$row = sql_fetch($sql);
$total_count = $row['cnt'];

$rows = $config['cf_page_rows'];
$total_page  = ceil($total_count / $rows);  // 전체 페이지 계산
if ($page < 1) $page = 1; // 페이지가 없으면 첫 페이지 (1 페이지)
$from_record = ($page - 1) * $rows; // 시작 열을 구함

$listall = '<a href="'.$_SERVER['SCRIPT_NAME'].'" class="ov_listall">전체목록</a>';

$g5['title'] = '약국회원관리';
//include_once('./admin.head.php');

$sql = " select a.* 
		{$sql_common} {$sql_search} {$sql_order} limit {$from_record}, {$rows} ";
//echo $sql;

$result = sql_query($sql);

$colspan = 6;
?>
<style>
.container_wr {
    padding: 6px;
}
</style>
<form id="fsearch" name="fsearch" class="local_sch01 local_sch" method="get">


<div class="local_sch03  tbl_wrap" style="padding:10px;">
    <table>
   
    <tbody>
	
	<tr>
        
		<th scope="row">사용여부</th>
        <td>
           <select name="pharm_status" id="pharm_status" >
				<option value="">==전체==</option>
				
				<option value="2" <?php echo get_selected($pharm_status, '2'); ?>>사용</option>
				<option value="3" <?php echo get_selected($pharm_status, '3'); ?>>사용안함</option>
			</select>
        </td>
   
		<th scope="row">검색</th>
        <td>
            <select name="sfl" id="sfl" >
			<option value="comp_name" <?php echo get_selected($sfl, 'comp_name'); ?>>공급사명</option>
			<option value="comp_md_name" <?php echo get_selected($sfl, 'comp_md_name'); ?>>담당자명</option>
			<option value="mb_id" <?php echo get_selected($sfl, 'mb_id'); ?>>아이디</option>
			
			
			</select>
			<label for="stx" class="sound_only">검색어</label>
			<input type="text" name="stx" value="<?php echo $stx ?>" id="stx" class=" frm_input">
        </td>
		<td >
			 <input type="submit" value="검색" class="get_theme_confc btn btn_01">
		</td>
		
    </tr>
	
    </tbody>
    </table>
</div>




</form>

<!-- <div class="local_desc01 local_desc">
    <p>
        회원자료 삭제 시 다른 회원이 기존 회원아이디를 사용하지 못하도록 회원아이디, 이름, 닉네임은 삭제하지 않고 영구 보관합니다.
    </p>
</div> -->


<form name="fmemberlist" id="fmemberlist" action="" onsubmit="return fmemberlist_submit(this);" method="post">
<input type="hidden" name="sst" value="<?php echo $sst ?>">
<input type="hidden" name="sod" value="<?php echo $sod ?>">
<input type="hidden" name="sfl" value="<?php echo $sfl ?>">
<input type="hidden" name="stx" value="<?php echo $stx ?>">


<input type="hidden" name="page" value="<?php echo $page ?>">
<input type="hidden" name="token" value="">

<div class="tbl_head01 tbl_wrap">
    <table>
    <caption><?php echo $g5['title']; ?> 목록</caption>
    <thead>
    <tr>
        <th scope="col" id="mb_list_chk" >
            <label for="chkall" class="sound_only">회원 전체</label>
            <input type="checkbox" name="chkall" value="1" id="chkall" onclick="check_all(this.form)">
        </th>
		<th scope="col" id="mb_list_id" >공급사명</th>
        <th scope="col" id="mb_list_id" >아이디</th>
		<th scope="col" id="mb_list_name">이름</th>
        <th scope="col" id="mb_list_nick">연락처</th>
		<th scope="col" id="mb_list_auth">가입일</th>
	
    </tr>
    </thead>
    <tbody>
    <?php
    for ($i=0; $row=sql_fetch_array($result); $i++) {

          $bg = 'bg'.($i%2);
		
	
		$mb_time       = substr($row["mb_datetime"], 0, 10);
	

    ?>

    <tr class="<?php echo $bg; ?>">
        
        <td headers="mb_list_chk" class="td_chk" >
            <input type="hidden" name="mb_hp[<?php echo $i ?>]" value="<?php echo $row['comp_md_tel'] ?>" id="mb_hp_<?php echo $i ?>">
			<input type="hidden" name="mb_name[<?php echo $i ?>]" value="<?php echo $row['comp_md_name'] ?>" id="mb_name_<?php echo $i ?>">
            <label for="chk_<?php echo $i; ?>" class="sound_only"><?php echo get_text($row['comp_md_name']); ?> </label>
            <input type="checkbox" name="chk[]" value="<?php echo $i ?>" id="chk_<?php echo $i ?>">
        </td>
		<td align="center"><?php echo $row["comp_name"]; ?></td>
		<td align="center"><?php echo $row["mb_id"]; ?></td>
		<td align="center"><?php echo $row["comp_md_name"]; ?></td>
		<td align="center"><?php echo $row["comp_md_tel"]; ?></td>		
		<td align="center"><?php echo $mb_time; ?></td>

	</tr>

   <?php
    }
	?>
	<tr>

	  <td colspan=13 style="text-align: left;">
		
		<div class="local_cmd01 ">
		<input type="button" value="선택추가" class="btn_submit" onclick="flist_submit();">   
		</div>
	
	  </td>
	</tr>
	<?php
    if ($i == 0)
        echo "<tr><td colspan=\"".$colspan."\" class=\"empty_table\">자료가 없습니다.</td></tr>";
    ?>
    </tbody>
    </table>
</div>


</form>

<?php 
$qstr ='m=1&sales_id='.$_GET['sales_id'].'&'.$qstr;
echo get_paging(G5_IS_MOBILE ? $config['cf_mobile_pages'] : $config['cf_write_pages'], $page, $total_page, "{$_SERVER['SCRIPT_NAME']}?$qstr&amp;page="); ?>

<script>
function check_all(f)
{
    var chk = document.getElementsByName("chk[]");

    for (i=0; i<chk.length; i++)
        chk[i].checked = f.chkall.checked;
}
function flist_submit()
{
	
	//if (!is_checked("chk[]")) {
    //    alert("등록 하실 항목을 하나 이상 선택하세요.");
    //    return false;
    //}
	
	var f = document.fmemberlist;
	var chk = document.getElementsByName("chk[]");
	var mb_hps = '';
	for (var i=0; i<chk.length; i++)
	{
		if (chk[i].checked)
		{
			var k = chk[i].value;
			var mb_name      = f.elements['mb_name['+k+']'].value;
			var mb_hp      = f.elements['mb_hp['+k+']'].value;			
			parent.sms_add(mb_name, mb_hp);
		}
	}	
	
}
</script>
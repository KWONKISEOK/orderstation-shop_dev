<?php
$sub_menu = "900200";
include_once("./_common.php");
include_once(G5_LIB_PATH."/surem.lib.php");

//auth_check($auth[$sub_menu], "w");

//check_admin_token();

$g5['title'] = "문자전송중";

$wr_message = clean_xss_tags(trim($wr_message));

if (!$wr_message)
    alert('메세지를 입력해주세요.');

//if ($sms_check == '')
//    alert('메세지 받을 대상을 선택해주세요.');

// 회원구분(0:일반, 1:약국, 5:영업자, 7:공급사, 9:관리자)
$subject = '';
$msg     = $wr_message;
$reqtime = $wr_booking ? $wr_by . $wr_bm . $wr_bd . $wr_bh . $wr_bi : '00000000000000';
$result  = $wr_booking ? 'R' : '0';


$surem = new SureM();				// testing
//$surem = new SureM('development');	// development
//$surem = new SureM('production');	// production

//개별전송
if($sms_check =='0' or $sms_check =='3' or $sms_check =='5') {

	$list = array();
	$hps = array();

	$send_list = explode('/', $send_list);
	$wr_overlap = 1; // 중복번호를 체크함
	$overlap = 0;
	$duplicate_data = array();
	$duplicate_data['hp'] = array();
	$str_serialize = "";
	
	//while ($row = array_shift($send_list))
	for($i=0; $i<count($send_list); $i++) 
	{	
		//$item =  $row;

		$item = explode(':', $send_list[$i]);

		$hp = get_hp($item[1], 0);
		$name = $item[0];
	
		if(!$hp) continue;

		if ($wr_overlap && array_overlap($hps, $hp)) {
			$overlap++;
			array_push( $duplicate_data['hp'], $row['bk_hp'] );
			continue;
		}

		array_push($list, array('bk_hp' => $hp, 'bk_name' => $name));
		array_push($hps, $hp);

	}

	if( count($duplicate_data['hp']) ){ //중복된 번호가 있다면
		$duplicate_data['total'] = $overlap;
		$str_serialize = serialize($duplicate_data);
	}

	$total_count = count($list);
//echo $total_count;

	for($i=0; $i<$total_count; $i++) {

		$callname  = $list[$i]['bk_name'];
		$callphone = str_replace('-', '', $list[$i]['bk_hp']); 

		$insert_sql = $surem->insert_query_builder($callname, $callphone, $subject, $msg, $reqtime, $result, $kind) ;
		//echo $insert_sql . '<br />';
	
		sql_query($insert_sql);
	}

}else if($sms_check == '6'){

    $sql2 = "select a.od_name AS od_name,replace(REPLACE(c.od_b_hp,'-',''), ' ' ,'') AS CALLPHONE 
              from tbl_shop_order a 
                  inner join tbl_shop_order_detail b on a.od_id = b.od_id                           
                  inner join tbl_shop_order_receiver c on b.od_id = c.od_id and b.od_num = c.od_num                                                                
                  left join(                                 
                     select b.mb_id , min(a.od_id) AS od_id , max(b.mb_type) as mb_type                                 
                     from tbl_shop_order a inner join tbl_member b ON a.mb_id = b.mb_id AND a.od_status != '취소'                              
                     WHERE od_receipt_time != '0000-00-00 00:00:00' and mb_datetime <= od_receipt_time                                 
                     GROUP BY b.mb_id                                                                                          
                  ) e ON a.mb_id = e.mb_id AND a.od_id = e.od_id                                  
                  left join tbl_member h ON a.mb_id = h.mb_id                                 
                     where 1=1 AND a.service_type is null AND  c.od_date1 between DATE_ADD(Now(),INTERVAL -$buy_months MONTH) AND now() AND c.od_status!='취소'
                     and instr( c.od_status , '반품' ) <= 0  AND replace(replace(c.od_b_hp,'-',''), ' ' ,'') IN (SELECT replace(replace(mb_hp,'-',''), ' ' ,'') FROM tbl_member WHERE service_type IS NULL  AND mb_sms='1') GROUP BY CALLPHONE";
	$qry = sql_query($sql2);
	$i = 0;
	while($res = sql_fetch_array($qry)) {
        $callname  = $res['od_name'];
        $callphone = str_replace('-', '', $res['CALLPHONE']);
		$insert_sql = $surem->insert_query_builder($callname, $callphone, $subject, $msg, $reqtime, $result, $kind) ;
		sql_query($insert_sql);
		$i++;
	}
	$total_count = $i;
		
} else {

	//DB전송
	switch ($sms_check) {
		case '1': $mb_type = '1'; break;
		case '2': $mb_type = '0'; break;
		case '4': $mb_type = '7'; break;
	}
	$where = "and mb_type = '$mb_type' and mb_sms = 1 ";
	//$where = "and mb_id = 'freejcw' ";	// for 테스트용
	//$where = "and mb_id = 'freejcw' or mb_id = 'chaeyoung' ";	// for 테스트용
	$count_sql = "select count(*) as cnt from {$g5['member_table']} where 1 $where ";
	//echo $count_sql . '<br />';

	$total_res = sql_fetch($count_sql);
	$total_count = $total_res['cnt'];
	if (!$total_count) alert('발송할 회원 데이터가 없습니다.');

	
	$sql = "select * from {$g5['member_table']} where 1 $where order by mb_no asc; ";
	//echo $sql;
	$qry = sql_query($sql);

	while($res = sql_fetch_array($qry)) {

		if($mb_type==7) {
			$callname  = $res['comp_md_name'];
			$callphone = str_replace('-', '', $res['comp_md_tel']); 			
		} else {
			$callname  = $res['mb_name'];
			$callphone = str_replace('-', '', $res['mb_hp']); 
		}
		
		$insert_sql = $surem->insert_query_builder($callname, $callphone, $subject, $msg, $reqtime, $result, $kind) ;
		//echo $insert_sql . '<br />';
		sql_query($insert_sql);
		}
	
}

//$sql3="delete from orderstation_sms.suredata_temp";
//	sql_query($sql3);
alert_go_page('SMS '.$total_count.'건을 처리 완료하였습니다.');
	

function alert_go_page($msg) {
	$html = "<script>
	alert('$msg');
	location.href = 'send.php';</script>";

	echo $html;
	exit;
}
/* 
function win_close_alert($msg) {
	$html = "<script>
	act = window.open('sms_ing.php', 'act', 'width=300, height=200');
	act.close();
	alert('$msg');
	history.back();</script>";

	echo $html;
	exit;
}
*/
?>
<script>
//act = window.open('sms_ing.php', 'act', 'width=300, height=200');
//act.close();
//location.href = 'send.php';
</script>
<?php
include_once(G5_ADMIN_PATH.'/admin.tail.php');
?>
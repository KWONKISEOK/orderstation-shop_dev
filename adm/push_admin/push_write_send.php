<?php
$sub_menu = "900901";
include_once("./_common.php");

auth_check($auth[$sub_menu], "r");

$g5['title'] = "PUSH 발송테스트";

include_once(G5_ADMIN_PATH.'/admin.head.php');
?>

<?php
// push 메시지 발송 처리 코드입니다
$cmd = post_if_set('cmd');
if (! $cmd) die("PARAMETER_NOT_FOUND");

// 회원에게 push를 발송한다
if ($cmd == 'send-push') {
	$mb_id   = post_if_set('mb_id');	// 회원 아이디	
	$title = post_if_set('title');	// 메시지 내용
	$message = post_if_set('message');	// 메시지 내용

	//if (! $mb_id || ! $message) die("PARAMETER_NOT_FOUND");
		
	//if (! exist_mb_id($mb_id)) die("DATA_NOT_FOUND");

	// 회원의 디바이스 토큰 정보를 조회한다
	$result = get_mb_id_token($mb_id);

	if (! $result) die("DATA_NOT_FOUND");

	for ($i = 0; $i < count($result); $i++) {
		$token = $result[$i]['token'];
		$os    = $result[$i]['os'];

		send_push($os, $title, $message, $token);

		

	}

	$sql = " insert into tbl_push_history (title,msg,send_user) values('$title', '$message','$mb_id'); ";
		$row = sql_fetch($sql);
	
	die("SUCCESS");
}

?>


<?php
include_once(G5_ADMIN_PATH.'/admin.tail.php');
?>
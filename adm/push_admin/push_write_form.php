<?php
$sub_menu = "900901";
include_once("./_common.php");

auth_check($auth[$sub_menu], "r");

$g5['title'] = "PUSH 발송";

include_once(G5_ADMIN_PATH.'/admin.head.php');
?>

<form name="frmSend" method="post" action="./push_write_send.php">
<input type="hidden" name="cmd" value="send-push" />

<div class="tbl_frm01 tbl_wrap">
    <table>
    <caption><?php echo $g5['title']; ?></caption>
    <colgroup>
        <col class="grid_4">
        <col>
    </colgroup>
    <tbody>
    <!-- <tr>
        <th scope="row"><label for="push_cmd">cmd<strong class="sound_only"> 필수</strong></label></th>
        <td>
            <?php echo help("push 발송을 위해서는 cmd 명령어값이 같이 전송되어야 됩니다 (hidden 필드 처리)."); ?>
            <input type="text" name="cmd" value="send-push" id="push_message" required class="frm_input required" size="20">
        </td>
    </tr>	 -->
    <tr>
        <th scope="row"><label for="push_mb_id">회원아이디</label></th>
        <td>
            <?php echo help("입력하지 않으면 전 회원에게 발송 됩니다."); ?>
            <input type="text" name="mb_id" value="" id="push_mb_id"  class="frm_input">
        </td>
    </tr>
	<tr>
        <th scope="row"><label for="push_title">제목<strong class="sound_only"> 필수</strong></label></th>
        <td>
            <?php echo help("push 메시지를 발송할 대상 회원아이디를 입력합니다."); ?>
            <input type="text" name="title" value="오더스테이션" id="push_title" required class="frm_input required">
        </td>
    </tr>
    <tr>
        <th scope="row"><label for="push_message">메시지<strong class="sound_only"> 필수</strong></label></th>
        <td>
            <?php echo help("push 메시지 내용을 입력합니다."); ?>
            <input type="text" name="message" value="" id="push_message" required class="frm_input required" size="100">
        </td>
    </tr>
    </tbody>
    </table>
</div>

<div class="btn_fixed_top">
    <input type="submit" value="전송하기" class="btn_submit btn" accesskey="s">
</div>
</form>



<?php
$sql_common = " from tbl_push_history a ";

if (!$sst) {
    $sst = "regdate";
    $sod = "desc";
}

$sql_order = " order by {$sst} {$sod} ";

$sql = " select count(*) as cnt {$sql_common} {$sql_search} {$sql_order} ";
$row = sql_fetch($sql);
$total_count = $row['cnt'];

$rows = $config['cf_page_rows'];
$total_page  = ceil($total_count / $rows);  // 전체 페이지 계산
if ($page < 1) $page = 1; // 페이지가 없으면 첫 페이지 (1 페이지)
$from_record = ($page - 1) * $rows; // 시작 열을 구함


$sql = " select a.* 
		{$sql_common} {$sql_search} {$sql_order} limit {$from_record}, {$rows} ";

$result = sql_query($sql);

$colspan = 6;
?>
<style>
.container_wr {
    padding: 6px;
}
</style>


<form name="fmemberlist" id="fmemberlist" action=""  method="post">
<input type="hidden" name="sst" value="<?php echo $sst ?>">
<input type="hidden" name="sod" value="<?php echo $sod ?>">
<input type="hidden" name="sfl" value="<?php echo $sfl ?>">
<input type="hidden" name="stx" value="<?php echo $stx ?>">


<input type="hidden" name="page" value="<?php echo $page ?>">
<input type="hidden" name="token" value="">

<div class="tbl_head01 tbl_wrap">
    <table>
    <caption><?php echo $g5['title']; ?> 목록</caption>
    <thead>
    <tr>

        <th scope="col" id="mb_list_id" >제목</th>
		<th scope="col" id="mb_list_name">메시지</th>
		<th scope="col" id="mb_list_name">받는회원</th>
        <th scope="col" id="mb_list_nick">보낸날자</th>
	
    </tr>
    </thead>
    <tbody>
    <?php
    for ($i=0; $row=sql_fetch_array($result); $i++) {

          $bg = 'bg'.($i%2);

		$mb_time       = substr($row["regdate"], 0, 10);
	

    ?>

    <tr class="<?php echo $bg; ?>">
        
		<td align="center"><?php echo $row["title"]; ?></td>
		<td align="center"><?php echo $row["msg"]; ?></td>
		<td align="center"><?php echo $row["send_user"]; ?></td>		
		<td align="center"><?php echo $mb_time; ?></td>

	</tr>

   <?php
    }
	
    if ($i == 0)
        echo "<tr><td colspan=\"".$colspan."\" class=\"empty_table\">자료가 없습니다.</td></tr>";
    ?>
    </tbody>
    </table>
</div>


</form>

<?php 


echo get_paging( $config['cf_write_pages'], $page, $total_page, "{$_SERVER['SCRIPT_NAME']}?$qstr&amp;page="); ?>



<?php
include_once(G5_ADMIN_PATH.'/admin.tail.php');
?>
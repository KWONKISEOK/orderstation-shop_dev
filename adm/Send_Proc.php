<?php
$sub_menu = "200140";
include_once("./_common.php");
include_once(G5_LIB_PATH.'/mailer.lib.php');

auth_check($auth[$sub_menu], 'w');

$mb_id			= $_POST["mb_id"];
$Charge_Idx		= $_POST["Charge_Idx"];
$DelYn			= $_POST["DelYn"];
$Charge_Task	= $_POST["Charge_Task"];
$Charge_Name	= $_POST["Charge_Name"];
$Charge_Bigo	= $_POST["Charge_Bigo"];
$Charge_Tel		= $_POST["Charge_Tel"];
$Charge_Hp		= $_POST["Charge_Hp"];
$Charge_Email	= $_POST["Charge_Email"];

if( empty($mb_id) ){
	echo "<script>alert('잘못된 접근입니다.');</script>";
	exit;
}

$sql = " update tbl_charge_info_mod set DelYn = 'Y' where mb_id = '{$mb_id}' ";
sql_query( $sql );

for($i=0; $i<count($DelYn); $i++){

	$add_query	  = "";
	$Get_Charge_Idx	  = $Charge_Idx[$i];
	$Get_Charge_Task  = $Charge_Task[$i];
	$Get_Charge_Name  = $Charge_Name[$i];
	$Get_Charge_Bigo  = $Charge_Bigo[$i];
	$Get_Charge_Tel	  = $Charge_Tel[$i];
	$Get_Charge_Hp	  = $Charge_Hp[$i];
	$Get_Charge_Email = $Charge_Email[$i];
	$Get_DelYn		  = $DelYn[$i];

	//0 이면 insert , 0이 아니면 update
	if( $Get_Charge_Idx == "0" ){

		$sql = "
			insert into tbl_charge_info_mod(
				mb_id , 
				Charge_Task , 
				Charge_Name , 
				Charge_Bigo , 
				Charge_Tel , 
				Charge_Hp , 
				Charge_Email , 
				DelYn
			)values(
				'{$mb_id}',
				'{$Get_Charge_Task}',
				'{$Get_Charge_Name}',
				'{$Get_Charge_Bigo}',
				'{$Get_Charge_Tel}',
				'{$Get_Charge_Hp}',
				'{$Get_Charge_Email}',
				'N'
			)
		";

		sql_query($sql);

	}else{

        $sql = "
			insert into tbl_charge_info_mod(
				mb_id , 
				Charge_Task , 
				Charge_Name , 
				Charge_Bigo , 
				Charge_Tel , 
				Charge_Hp , 
				Charge_Email , 
				DelYn
			)values(
				'{$mb_id}',
				'{$Get_Charge_Task}',
				'{$Get_Charge_Name}',
				'{$Get_Charge_Bigo}',
				'{$Get_Charge_Tel}',
				'{$Get_Charge_Hp}',
				'{$Get_Charge_Email}',
				'{$Get_DelYn}'
			)
		";

		sql_query($sql);
	}

}

$sql = "
	update tbl_member set  comp_name_mod			= '{$comp_name_mod}' , 
						   comp_limit_mod			= '{$comp_limit_mod}',
						   comp_brand_mod			= '{$comp_brand_mod}',
						   comp_ceo_name_mod		= '{$comp_ceo_name_mod}',
						   comp_tel_mod				= '{$comp_tel_mod}',
						   comp_md_name_mod			= '{$comp_md_name_mod}',
						   comp_md_tel_mod			= '{$comp_md_tel_mod}',
						   comp_order_time_mod		= '{$comp_order_time_mod}',
						   comp_trans_time_mod		= '{$comp_trans_time_mod}',
						   comp_charge_datetime_1	= now() , 
						   comp_change_status		= '0'
	where mb_id = '{$mb_id}'
";

sql_query($sql);


if( !empty($comp_code) ){

	$sql = " SELECT * FROM tbl_member WHERE comp_code = '{$comp_code}' ";
	$rs = sql_fetch($sql);

	$subject = "공급사 추가정보 승인요청 ";
	$content = "공급사코드:".$rs['comp_code']." "."공급사명:".$rs['comp_name'];

	mailer($config['cf_admin_email_name'], $config['cf_admin_email'], $config['cf_admin_email'], $subject, $content, 1);

}


echo "<script>
		alert('요청완료하였습니다.');
		parent.location.reload();
	  </script>";

?>
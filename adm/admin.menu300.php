<?php
$menu['menu300'] = array (
    array('300000', '게시판관리', ''.G5_ADMIN_URL.'/board_list.php', 'board'),
    array('300100', '게시판관리', ''.G5_ADMIN_URL.'/board_list.php', 'bbs_board'),
    //array('300200', '게시판그룹관리', ''.G5_ADMIN_URL.'/boardgroup_list.php', 'bbs_group'),
    //array('300300', '인기검색어관리', ''.G5_ADMIN_URL.'/popular_list.php', 'bbs_poplist', 1),
    //array('300400', '인기검색어순위', ''.G5_ADMIN_URL.'/popular_rank.php', 'bbs_poprank', 1),
    array('300500', '1:1문의설정', ''.G5_ADMIN_URL.'/qa_config.php', 'qa'),
    array('300600', '내용관리', G5_ADMIN_URL.'/contentlist.php', 'scf_contents', 1),
    array('300700', '오더스테이션 FAQ관리', G5_ADMIN_URL.'/faqmasterlist.php', 'scf_faq', 1),
    array('300800', '이즈브레 FAQ관리', G5_ADMIN_URL.'/faqmasterlist2.php', 'scf_faq', 1),
    //array('300820', '글,댓글 현황', G5_ADMIN_URL.'/write_count.php', 'scf_write_count'),
	array('300830', '공급사정보공유', G5_ADMIN_URL.'/venderlist.php', 'scf_contents', 1),
	array('300840', '보도자료', G5_ADMIN_URL.'/healthlist.php', 'scf_contents', 1),
	array('300850', '제휴문의', G5_ADMIN_URL.'/partnerList.php', 'scf_contents', 1),
	array('300860', '이즈브레앱 홍보영상', G5_ADMIN_URL.'/isapp_movie_list.php', 'movie_contents', 1),
	array('300870', '약국가입문의', G5_ADMIN_URL.'/qa_PharmList.php', 'scf_contents', 1),
);
?>
<?php
include_once('./_common.php');

$mode	= $_POST['mode'];

//$sql_common = " from {$g5['member_table']} a ";

//$sql_search = " where (1) ";
if ($stx) {
    $sql_search .= " and ( ";
    switch ($sfl) {
        
        case 'mb_name' :
            //$sql_search .= " (X.{$sfl} like '%{$stx}') ";
			$sql_search .= " instr( X.{$sfl},'{$stx}' ) > 0 ";
            break;		
        default :
            //$sql_search .= " (X.{$sfl} like '{$stx}%') ";
			$sql_search .= " instr( X.{$sfl},'{$stx}' ) > 0 ";
            break;
    }
    $sql_search .= " ) ";
}

if ($pharm_status)
    $sql_search .= " and X.pharm_status = '$pharm_status' ";

switch($mode){
	
	case 'excel':
		
		$ExcelTitle = urlencode( "공급사" . date( "YmdHis" ) );
		header( "Content-type: application/vnd.ms-excel" );
		header( "Content-type: application/vnd.ms-excel; charset=utf-8" );
		header( "Content-Disposition: attachment; filename = " . $ExcelTitle . ".xls" );
		header( "Content-Description: PHP4 Generated Data" );
		print( "<meta http-equiv=\"Content-Type\" content=\"application/vnd.ms-excel; charset=utf-8\">" ); 
		
		?>
		<table border="1">
		<tr bgcolor="F7F7F7" height="30">
			<th rowspan="2">아이디</th>
			<th rowspan="2">거래처명</th>
			<th rowspan="2">대표자명</th>
			<th rowspan="2">대표전화번호</th>
			<th rowspan="2">담당자명</th>
			<th rowspan="2">담당자연락처</th>
			<th rowspan="2">담당자이메일</th>			
			<th rowspan="2">우편번호</th>
			<th rowspan="2">주소1</th>
			<th rowspan="2">주소2</th>
			<th rowspan="2">등록일</th>
			<th rowspan="2">배송비조건(~이상구매시)</th>
			<th rowspan="2">배송비적용금액</th>
			<th rowspan="2">배송비사용여부</th>
			<th colspan="6">담당자정보</th>
		</tr>
		<tr bgcolor="F7F7F7" height="30">
			<th>업무</th>
			<th>담당자명</th>
			<th>비고(직책)</th>
			<th>Tel</th>
			<th>HP</th>
			<th>이메일</th>
		</tr>
		<?php		

		$sql = "
				SELECT 
					X.* ,
					Y.mb_id_cnt ,
					CASE @vjob WHEN X.mb_id THEN @rownum:=@rownum+1 ELSE @rownum:=1 END as ranking , 
					@vjob:=X.mb_id as vjob , 
					IFNULL( case when @rownum = 1 then Y.mb_id_cnt ELSE 0 END , -1 ) AS row_span_cnt 
				FROM(
					SELECT a.*, b.Charge_Task , b.Charge_Name , b.Charge_Bigo , b.Charge_Tel , b.Charge_Hp , b.Charge_Email , b.Charge_RegDate
					from tbl_member a left join tbl_charge_info b ON a.mb_id = b.mb_id  AND b.DelYn = 'N' 
					where a.mb_type = 7 
				) AS X LEFT JOIN(
					select COUNT(a.mb_id) AS mb_id_cnt , a.mb_id from tbl_member a left join tbl_charge_info b ON a.mb_id = b.mb_id  AND b.DelYn = 'N' 
					where a.mb_type = 7 
					GROUP BY a.mb_id
				) AS Y ON X.mb_id = Y.mb_id , ( SELECT @vjob:='', @rownum:=0 ) as Z
				where 1=1 ".$sql_search ." 
				ORDER BY X.mb_datetime desc , X.Charge_RegDate asc
		";

		$res = sql_query($sql);
		if(!$res) exit('Cannot run query.');

		$i = 0;
		while($row = mysqli_fetch_array($res,MYSQL_ASSOC)) {

			$pharm_status       = $row["pharm_status"];
			$mb_referee         = $row["mb_referee"];
			$mb_joinroute       = $row["mb_joinroute"];

			$Charge_Task		= $row["Charge_Task"];
			$Charge_Name		= $row["Charge_Name"];
			$Charge_Bigo		= $row["Charge_Bigo"];
			$Charge_Tel			= $row["Charge_Tel"];
			$Charge_Hp			= $row["Charge_Hp"];
			$Charge_Email		= $row["Charge_Email"];
			$row_span_cnt		= $row["row_span_cnt"];
			$ranking			= $row["ranking"];
			
			switch ( $pharm_status ) {
				case "1" :
					$mb_status_str = "사용(대기)";
					break;
				case "2" :
					$mb_status_str = "사용(승인)";
					break;
				case "3" :
					$mb_status_str = "사용안함";
					break;
				case "N" :
					$mb_status_str = "신규가입";
					break;
				case "L" :
					$mb_status_str = "erp등록중";
					break;
				case "D" :
					$mb_status_str = "휴면";
					break;
			}

			$row_span_tag = 0;
			if( $row_span_cnt > 0 ){
				$row_span_tag = " rowspan={$row_span_cnt} ";
			}

			
			?>
			<tr bgcolor="">

				<? if( $row_span_cnt > 0 ){ ?>
				<td style="mso-number-format:\@" <?=$row_span_tag?>><?php echo $row["mb_id"];?></td>
				<td style="mso-number-format:\@" <?=$row_span_tag?>><?php echo $row["comp_name"];?></td>
				<td style="mso-number-format:\@" <?=$row_span_tag?>><?php echo $row["comp_ceo_name"];?></td>
				<td style="mso-number-format:\@" <?=$row_span_tag?>><?php echo $row["comp_tel"];?></td>
				<td style="mso-number-format:\@" <?=$row_span_tag?>><?php echo $row["comp_md_name"];?></td>
				<td style="mso-number-format:\@" <?=$row_span_tag?>><?php echo $row["comp_md_tel"];?></td>
				<td style="mso-number-format:\@" <?=$row_span_tag?>><?php echo $row["mb_email"];?></td>				
				<td style="mso-number-format:\@" <?=$row_span_tag?>><?php echo $row["mb_zip1"];?> <?php echo $row["mb_zip2"];?> </td>
				<td style="mso-number-format:\@" <?=$row_span_tag?>><?php echo $row["mb_addr1"];?></td>
				<td style="mso-number-format:\@" <?=$row_span_tag?>><?php echo $row["mb_addr2"];?></td>				
				<td style="mso-number-format:\@" <?=$row_span_tag?>><?php echo $row["mb_datetime"];?></td>
				<td style="mso-number-format:\@" <?=$row_span_tag?>><?php echo $row["comp_trans_compare"];?></td>
				<td style="mso-number-format:\@" <?=$row_span_tag?>><?php echo $row["comp_trans_price"];?></td>
				<td style="mso-number-format:\@" <?=$row_span_tag?>><?php echo $row["comp_trans_use"];?></td>
				<? } ?>

				<td style="mso-number-format:\@"><?=$Charge_Task?></td>
				<td style="mso-number-format:\@"><?=$Charge_Name?></td>
				<td style="mso-number-format:\@"><?=$Charge_Bigo?></td>
				<td style="mso-number-format:\@"><?=$Charge_Tel?></td>
				<td style="mso-number-format:\@"><?=$Charge_Hp?></td>
				<td style="mso-number-format:\@"><?=$Charge_Email?></td>

			</tr>
		<?php			
		} 
		break;
	
}

?>
</table>
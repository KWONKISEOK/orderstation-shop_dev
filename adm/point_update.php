<?php
$sub_menu = "200200";
include_once('./_common.php');

auth_check($auth[$sub_menu], 'w');

check_admin_token();

$mb_id = $_POST['mb_id'];
$po_point = $_POST['po_point'];
$po_content = $_POST['po_content'];
$expire = preg_replace('/[^0-9]/', '', $_POST['po_expire_term']);

$mb = get_member($mb_id);

if (!$mb['mb_id'])
    alert('존재하는 회원아이디가 아닙니다.', './point_list.php?'.$qstr);

if (($po_point < 0) && ($po_point * (-1) > $mb['mb_point']))
    alert('포인트를 깎는 경우 현재 포인트보다 작으면 안됩니다.', './point_list.php?'.$qstr);

insert_point($mb_id, $po_point, $po_content, '@passive', $mb_id, $member['mb_id'].'-'.uniqid(''), $expire);

/************* 관리자 로그 처리 START *************/
$al_data = sql_fetch("select * from {$g5['point_table']} where mb_id = '{$mb_id}' order by po_datetime desc" );

insert_admin_log(200,200200, '포인트 증감', '', $mb_id, '', $_SERVER['REQUEST_URI'], $al_data, '');

goto_url('./point_list.php?'.$qstr);
?>

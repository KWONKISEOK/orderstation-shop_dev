<?php
include_once('./_common.php');

$mode	= $_POST['mode'];

if ($stx) {
    $sql_search .= " and ( ";
    switch ($sfl) {
        
        case 'mb_name' :
            $sql_search .= " ({$sfl} like '%{$stx}') ";
            break;		
        default :
            $sql_search .= " ({$sfl} like '{$stx}%') ";
            break;
    }
    $sql_search .= " ) ";
}

if ($pharm_status)
    $sql_search .= " and pharm_status = '$pharm_status' ";

switch($mode){
	
	case 'excel':
		$ExcelTitle = urlencode( "영업사원" . date( "YmdHis" ) );
		header( "Content-type: application/vnd.ms-excel" );
		header( "Content-type: application/vnd.ms-excel; charset=utf-8" );
		header( "Content-Disposition: attachment; filename = " . $ExcelTitle . ".xls" );
		header( "Content-Description: PHP4 Generated Data" );
		print( "<meta http-equiv=\"Content-Type\" content=\"application/vnd.ms-excel; charset=utf-8\">" ); ?>
		<table border="1">
		<tr bgcolor="F7F7F7" height="30">
			<th>아이디</th>
			<th>소속(회사명)</th>
			<th>소속(소속팀)</th>
			<th>이름</th>
			<th>전화번호(모바일)</th>
			<th>이메일</th>
			<th>등록일</th>
		</tr>
		<?php		
		$sql = " select * 
						, (select code_nm from tbl_code where major_cd='A03' and minor_cd = left(a.pharm_manager_dept, 6)) as dept_name1
						, (select code_nm from tbl_code where major_cd='A03' and minor_cd = left(a.pharm_manager_dept, 9)) as dept_name2 
				from tbl_member a where a.mb_type = 5 ".$sql_search ." order by mb_datetime desc";

		$res = sql_query($sql);
		if(!$res) exit('Cannot run query.');

		$i = 0;
		while($row = mysqli_fetch_array($res,MYSQL_ASSOC)) {

			$pharm_status       = $row["pharm_status"];
			$mb_referee         = $row["mb_referee"];
			$mb_joinroute       = $row["mb_joinroute"];

			
			switch ( $pharm_status ) {
				case "1" :
					$mb_status_str = "사용(대기)";
					break;
				case "2" :
					$mb_status_str = "사용(승인)";
					break;
				case "3" :
					$mb_status_str = "사용안함";
					break;
				case "N" :
					$mb_status_str = "신규가입";
					break;
				case "L" :
					$mb_status_str = "erp등록중";
					break;
				case "D" :
					$mb_status_str = "휴면";
					break;
			}

			
			?>
			<tr bgcolor="">
				<td style="mso-number-format:\@"><?php echo $row["mb_id"];?></td>
				<td style="mso-number-format:\@"><?php echo $row["dept_name1"];?></td>
				<td style="mso-number-format:\@"><?php echo $row["dept_name2"];?></td>
				<td style="mso-number-format:\@"><?php echo $row["mb_name"];?></td>
				<td style="mso-number-format:\@"><?php echo $row["mb_hp"];?></td>
				<td style="mso-number-format:\@"><?php echo $row["mb_email"];?></td>			
				<td style="mso-number-format:\@"><?php echo $row["mb_datetime"];?></td>
			</tr>
		<?php			
		} 
		break;
	
}



	

?>
<?php
if (!defined('_GNUBOARD_')) exit;

$begin_time = get_microtime();

$files = glob(G5_ADMIN_PATH.'/css/admin_extend_*');
if (is_array($files)) {
    foreach ((array) $files as $k=>$css_file) {
        
        $fileinfo = pathinfo($css_file);
        $ext = $fileinfo['extension'];
        
        if( $ext !== 'css' ) continue;
        
        $css_file = str_replace(G5_ADMIN_PATH, G5_ADMIN_URL, $css_file);
        add_stylesheet('<link rel="stylesheet" href="'.$css_file.'">', $k);
    }
}

include_once(G5_PATH.'/head.sub.php');

function print_menu1($key, $no='')
{
    global $menu;

    $str = print_menu2($key, $no);

    return $str;
}

function print_menu2($key, $no='')
{
    global $menu, $auth_menu, $is_admin, $auth, $g5, $sub_menu;

    $str .= "<ul>";
    for($i=1; $i<count($menu[$key]); $i++)
    {
        //if ($is_admin != 'super' && (!array_key_exists($menu[$key][$i][0],$auth) || !strstr($auth[$menu[$key][$i][0]], 'r')))
        //    continue;

        if (($menu[$key][$i][4] == 1 && $gnb_grp_style == false) || ($menu[$key][$i][4] != 1 && $gnb_grp_style == true)) $gnb_grp_div = 'gnb_grp_div';
        else $gnb_grp_div = '';

        if ($menu[$key][$i][4] == 1) $gnb_grp_style = 'gnb_grp_style';
        else $gnb_grp_style = '';

        $current_class = '';

        if ($menu[$key][$i][0] == $sub_menu){
            $current_class = ' on';
        }
        if($is_admin == 'super' || strstr($auth[$menu[$key][$i][0]], 'r') ){
            $str .= '<li data-menu="' . $menu[$key][$i][0] . '"><a href="' . $menu[$key][$i][2] . '" class="gnb_2da ' . $gnb_grp_style . ' ' . $gnb_grp_div . $current_class . '">' . $menu[$key][$i][1] . '</a></li>';
            $auth_menu[$menu[$key][$i][0]] = $menu[$key][$i][1];
        }
    }
    $str .= "</ul>";

    return $str;
}
// 권한 체크
function check_menu2($key){
    global $menu, $is_admin, $auth;
    if ($is_admin == 'super')
        return true;

    for($i=1; $i<count($menu[$key]); $i++) {
        if (array_key_exists($menu[$key][$i][0],$auth) && strstr($auth[$menu[$key][$i][0]], 'r'))
            return true;
    }
        return false;
}

$adm_menu_cookie = array(
'container' => '',
'gnb'       => '',
'btn_gnb'   => '',
);

if( ! empty($_COOKIE['g5_admin_btn_gnb']) ){
    $adm_menu_cookie['container'] = 'container-small';
    $adm_menu_cookie['gnb'] = 'gnb_small';
    $adm_menu_cookie['btn_gnb'] = 'btn_gnb_open';
}
?>

<script>
var tempX = 0;
var tempY = 0;

function imageview(id, w, h)
{

    menu(id);

    var el_id = document.getElementById(id);

    //submenu = eval(name+".style");
    submenu = el_id.style;
    submenu.left = tempX - ( w + 11 );
    submenu.top  = tempY - ( h / 2 );

    selectBoxVisible();

    if (el_id.style.display != 'none')
        selectBoxHidden(id);
}
</script>

<div id="to_content"><a href="#container">본문 바로가기</a></div>

<header id="hd">
    <h1><?php echo $config['cf_title'] ?></h1>
    <div id="hd_top">
        <button type="button" id="btn_gnb" class="btn_gnb_close <?php echo $adm_menu_cookie['btn_gnb'];?>">메뉴</button>
		<?php if($is_admin=='super') { ?>
        <div id="logo"><a href="<?php echo G5_ADMIN_URL ?>"><img src="<?php echo G5_ADMIN_URL ?>/img/logo.png" alt="<?php echo $config['cf_title'] ?> 관리자"></a></div>
		<?php } else { ?>
		<div id="logo"><a href="#"><img src="<?php echo G5_ADMIN_URL ?>/img/logo.png" alt="<?php echo $config['cf_title'] ?> 관리자"></a></div>
	    <?php } ?>

        <div id="tnb">
            <ul>
                <li class="tnb_li"><a href="<?php echo G5_SHOP_URL ?>/" class="tnb_shop" target="_blank" title="쇼핑몰 바로가기">쇼핑몰 바로가기</a></li>
                <!-- <li class="tnb_li"><a href="<?php echo G5_URL ?>/" class="tnb_community" target="_blank" title="커뮤니티 바로가기">커뮤니티 바로가기</a></li> -->
				<?php if($is_admin =='super') { ?>
                <li class="tnb_li"><a href="<?php echo G5_ADMIN_URL ?>/service.php" class="tnb_service">부가서비스</a></li>
				<?php } ?>

                <li class="tnb_li"><button type="button" class="tnb_mb_btn">관리자<span class="./img/btn_gnb.png">메뉴열기</span></button>
                    <ul class="tnb_mb_area">
                        <li><a href="<?php echo G5_ADMIN_URL ?>/member_form.php?w=u&amp;mb_id=<?php echo $member['mb_id'] ?>">관리자정보</a></li>
                        <li id="tnb_logout"><a href="<?php echo G5_BBS_URL ?>/logout.php">로그아웃</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    <nav id="gnb" class="gnb_large <?php echo $adm_menu_cookie['gnb']; ?>">
        <h2>관리자 주메뉴</h2>
        <ul class="gnb_ul">
            <?php
			if($is_admin =='vender') { 
				
				$sql = " SELECT COUNT( DISTINCT(a.od_id ) ) AS cnt FROM tbl_shop_order_receiver a INNER JOIN tbl_shop_order_detail b 
						ON a.od_id = b.od_id and a.od_num = b.od_num WHERE b.comp_code = '".$member[comp_code]."' and a.od_status = '반품접수' 
						and a.od_return_confirm = 'N' ";
				$row = sql_fetch($sql);
				//반품접수건수
				$return_cnt_view = "(" .$row['cnt']. ")";				
			?>
			<li class="gnb_li on">
                <button type="button" class="btn_op menu-400 menu-order-1" title="쇼핑몰관리">쇼핑몰관리</button>
                <div class="gnb_oparea_wr">
                    <div class="gnb_oparea">
                        <h3>쇼핑몰관리</h3>
                        <ul>
						
						<li data-menu="400400"><a href="/adm/shop_admin/orderlist_vender.php" class="gnb_2da gnb_grp_style gnb_grp_div">주문내역</a></li>
						<li data-menu="400400"><a href="/adm/shop_admin/orderlist_vender.php?od_status=반품접수" class="gnb_2da gnb_grp_style gnb_grp_div">반품접수<?=$return_cnt_view?></a></li>
						<li data-menu="400400"><a href="/adm/shop_admin/orderlist_vender.php?od_status=반품승인" class="gnb_2da gnb_grp_style gnb_grp_div">반품승인</a></li>
						<li data-menu="400400"><a href="/adm/shop_admin/orderlist_vender.php?od_status=반품반려" class="gnb_2da gnb_grp_style gnb_grp_div">반품반려</a></li>
						<li data-menu="400400"><a href="/adm/shop_admin/orderlist_vender.php?od_status=반품완료" class="gnb_2da gnb_grp_style gnb_grp_div">반품완료</a></li>
						<li data-menu="400401"><a href="/adm/shop_order/vcalculate1.php" class="gnb_2da gnb_grp_style gnb_grp_div">실적관리</a></li>
						<li data-menu="400401"><a href="/adm/shop_order/vcalculate2.php" class="gnb_2da gnb_grp_style gnb_grp_div">정산관리</a></li>
						<li data-menu="400300"><hr/></li>
						<li data-menu="400300"><a href="/adm/shop_admin/itemlist_vender.php" class="gnb_2da  ">상품관리</a></li>
						<li data-menu="400660"><a href="/adm/shop_admin/itemqalist.php" class="gnb_2da  ">상품문의</a></li>
						<li data-menu="400650"><a href="/adm/shop_admin/itemuselist.php" class="gnb_2da  ">사용후기</a></li>
						<li data-menu="400620"><a href="/adm/shop_admin/itemstocklist.php" class="gnb_2da  ">상품재고관리</a></li>
						<li data-menu="400300"><hr/></li>
						<li data-menu="300830"><a href="/adm/venderlist.php" class="gnb_2da  ">공급사 정보공유</a></li>
						<li data-menu="400301"><a href="/adm/shop_admin/itemlist_vender2.php" class="gnb_2da  ">상품변경요청</a></li>
						<li data-menu="400300"><hr/></li>
						<li data-menu="300830"><a href="/adm/vender_form.php" class="gnb_2da  ">정보수정</a></li>
						<!-- <li data-menu="400610"><a href="/adm/shop_admin/itemtypelist.php" class="gnb_2da  ">상품유형관리</a></li>
						<li data-menu="400500"><a href="/adm/shop_admin/optionstocklist.php" class="gnb_2da  ">상품옵션재고관리</a></li>
						<li data-menu="400800"><a href="/adm/shop_admin/couponlist.php" class="gnb_2da  ">쿠폰관리</a></li>
						<li data-menu="400810"><a href="/adm/shop_admin/couponzonelist.php" class="gnb_2da  ">쿠폰존관리</a></li>
						<li data-menu="400750"><a href="/adm/shop_admin/sendcostlist.php" class="gnb_2da gnb_grp_style gnb_grp_div">추가배송비관리</a></li>
						<li data-menu="400410"><a href="/adm/shop_admin/inorderlist.php" class="gnb_2da gnb_grp_style ">미완료주문</a></li> -->
						</ul>                    
					</div>
                </div>
            </li>
			<?php } else if($is_admin =='sales') { ?>
			<li class="gnb_li on">
                <button type="button" class="btn_op menu-400 menu-order-1" title="쇼핑몰관리">영업관리</button>
                <div class="gnb_oparea_wr">
                    <div class="gnb_oparea">
                        <h3>영업관리</h3>
                        <ul>
						<li data-menu="200100"><a href="/adm/member_list_sales.php" class="gnb_2da  ">약국회원</a></li>
						<li data-menu="400300"><hr/></li>
						<li data-menu="400400"><a href="/adm/shop_admin/orderlist_sales.php" class="gnb_2da gnb_grp_style gnb_grp_div">주문내역</a></li>
						<li data-menu="400400"><a href="/adm/shop_order/scalculate1.php" class="gnb_2da gnb_grp_style gnb_grp_div">실적관리</a></li>
						<li data-menu="400300"><hr/></li>

						</ul>                    
					</div>
                </div>
            </li>
			<?php } else {
				$jj = 1;
				foreach($amenu as $key=>$value) {
					$href1 = $href2 = '';

					if ($menu['menu'.$key][0][2]) {
						$href1 = '<a href="'.$menu['menu'.$key][0][2].'" class="gnb_1da">';
						$href2 = '</a>';
					} else {
						continue;
					}

					$current_class = "";
					if (isset($sub_menu) && (substr($sub_menu, 0, 3) == substr($menu['menu'.$key][0][0], 0, 3)))
						$current_class = " on";

					$button_title = $menu['menu'.$key][0][1];
                    if( check_menu2('menu'.$key) ){
					?>
				<li class="gnb_li<?php echo $current_class;?>">
					<button type="button" class="btn_op menu-<?php echo $key; ?> menu-order-<?php echo $jj; ?>" title="<?php echo $button_title; ?>"><?php echo $button_title;?></button>
					<div class="gnb_oparea_wr">
						<div class="gnb_oparea">
							<h3><?php echo $menu['menu'.$key][0][1];?></h3>
							<?php echo print_menu1('menu'.$key);
						?>
						</div>
					</div>
				</li>
				<?php
                    }
				$jj++;
				}     //end foreach
			}
            ?>
        </ul>
    </nav>

</header>
<script>
jQuery(function($){

    var menu_cookie_key = 'g5_admin_btn_gnb';

    $(".tnb_mb_btn").click(function(){
        $(".tnb_mb_area").toggle();
    });

    $("#btn_gnb").click(function(){
        
        var $this = $(this);

        try {
            if( ! $this.hasClass("btn_gnb_open") ){
                set_cookie(menu_cookie_key, 1, 60*60*24*365);
            } else {
                delete_cookie(menu_cookie_key);
            }
        }
        catch(err) {
        }

        $("#container").toggleClass("container-small");
        $("#gnb").toggleClass("gnb_small");
        $this.toggleClass("btn_gnb_open");

    });

    $(".gnb_ul li .btn_op" ).click(function() {
        $(this).parent().addClass("on").siblings().removeClass("on");
    });

});
</script>


<div id="wrapper">

    <div id="container" class="<?php echo $adm_menu_cookie['container']; ?>">

        <h1 id="container_title"><?php echo $g5['title'] ?></h1>
        <div class="container_wr">
<?php
$sub_menu = '100111';
include_once('./_common.php');

/*************** 트랜잭션 관련 ****************/
$error_cnt = 0;
$ErrorMsg = "";
mysqli_autocommit($g5['connect_db'], false);
/*************** 트랜잭션 관련 ****************/

for ($i = 0; $i < count($_POST); $i++) {
    $j = $i + 1;
    $sql = " update tbl_stplat
                set st_content   = '{$_POST[st_.$i]}' , st_date = now()
                where  st_title = '이용약관$j'
               ";
    sql_query($sql);

    /*************** 트랜잭션 관련 ****************/
    if (mysqli_errno($g5['connect_db'])) {
        $error_cnt += 1;
    }
    /*************** 트랜잭션 관련 ****************/

}

/*************** 트랜잭션 관련 ****************/
if ($error_cnt > 0) {
    mysqli_rollback($g5['connect_db']);
    mysqli_close($g5['connect_db']);
    echo "
		<script>
			alert('데이터베이스의 에러로 인해 롤백되었습니다.');
			parent.document.getElementById('ErrorView').innerHTML='[" . $ErrorMsg . "]';
		</script>
		";
    exit;
} else {
    mysqli_commit($g5['connect_db']);
}
/*************** 트랜잭션 관련 ****************/


goto_url('./stplat_manage.php', false);
?>
 <?php
$sub_menu = "500300";
include_once('./_common.php');

auth_check($auth[$sub_menu], 'r');

$g5['title'] = '지역별 매출현황';
include_once(G5_ADMIN_PATH.'/admin.head.php');
include_once(G5_PLUGIN_PATH.'/jquery-ui/datepicker.php');

if( empty($start_date) && empty($end_date) ){
	$getWeeekDate = GetWeekInfo("1");
	$start_date = explode ("|",  $getWeeekDate )[0];
	$end_date = explode ("|",  $getWeeekDate )[1];
}

if( $start_date > $end_date ){
	echo "<script>";
	echo "alert('시작날짜가 종료날짜 보다 큽니다.날짜 선택을 다시해주세요.');";
	echo "location.href='./static_21.php';";
	echo "</script>";
}

$GetDateDiff = GetDateDiffInfo($start_date,$end_date);

?>
	
<form id="fsearch" name="fsearch" class="local_sch01 local_sch" action="?" method="get">

<div class="local_sch03  tbl_wrap" style="padding:10px;">
	<table>
	<tbody>
	<tr>
		<td>
			날짜선택 &nbsp;
			<input type="text" id="start_date"  name="start_date" value="<?php echo $start_date; ?>" class="frm_input" size="10" maxlength="10"> ~
			<input type="text" id="end_date"  name="end_date" value="<?php echo $end_date; ?>" class="frm_input" size="10" maxlength="10">	
			&nbsp;
			<input type="submit" value="검색" class="get_theme_confc btn btn_01">
			 <input type="button" value="엑셀 다운로드" class="get_theme_confc btn btn_02" style="cursor:pointer" onclick="javascript:ExcelDown();">
		</td>
	</tr>
	</tbody>
	</table>
</div>

</form>

<div class="tbl_head01 tbl_wrap">

<table>
<caption> 지역별 매출현황 </caption>
<thead>
<tr>
	<th scope="col" rowspan="2">지역</th>
	<th scope="col" colspan="4">이번주</th>
	<th scope="col" rowspan="2">지난주 매출</th>
	<th scope="col" rowspan="2">매출 건수</th>
	<th scope="col" rowspan="2">전년 동기간 매출</th>
	<th scope="col" rowspan="2">매출 건수</th>
	<th scope="col" rowspan="2">평균</th>
</tr>
<tr>
	<th>매출액</th>
	<th>매출 건수</th>
	<th>지난주대비</th>
	<th>전년대비</th>
</tr>
</thead>
<tbody>
<?
$cal_price_1 = 0;
$cal_price_2 = 0;
$sum_total_price_1=0;
$sum_total_cnt_1=0;
$sum_cal_price_1=0;
$sum_cal_price_2=0;
$sum_total_price_2=0;
$sum_total_cnt_2=0;
$sum_total_price_3=0;
$sum_total_cnt_3=0;
$sum_avg_price=0;


$result = sql_query("CALL SP_P_21('".$start_date."','".$end_date."',".$GetDateDiff.")");
for ($i=0; $row=sql_fetch_array($result); $i++) {

	$cal_price_1 = ( ( (float)$row["total_price_1"] - (float)$row["total_price_2"] ) * 100 ) / (float)$row["total_price_2"];
	$cal_price_2 = ( ( (float)$row["total_price_1"] - (float)$row["total_price_3"] ) * 100 ) / (float)$row["total_price_3"];
	$sum_total_price_1=$sum_total_price_1+$row["total_price_1"];
	$sum_total_cnt_1=$sum_total_cnt_1+$row["total_cnt_1"];
	$sum_cal_price_1=$sum_cal_price_1+$cal_price_1;
	$sum_cal_price_2=$sum_cal_price_2+$cal_price_2;
	$sum_total_price_2=$sum_total_price_2+$row["total_price_2"];
	$sum_total_cnt_2=$sum_total_cnt_2+$row["total_cnt_2"];
	$sum_total_price_3=$sum_total_price_3+$row["total_price_3"];
	$sum_total_cnt_3=$sum_total_cnt_3+$row["total_cnt_3"];
	$sum_avg_price=$sum_avg_price+$row["avg_price"];
	
?>
<tr>
	<td><?=$row["mb_addr1"]?></td>
	<td><?=number_format($row["total_price_1"])?></td>
	<td><?=$row["total_cnt_1"]?>건</td>
	<td><?=round($cal_price_1,1)?>%</td>
	<td><?=round($cal_price_2,1)?>%</td>
	<td><?=number_format($row["total_price_2"])?></td>
	<td><?=$row["total_cnt_2"]?>건</td>
	<td><?=number_format($row["total_price_3"])?></td>
	<td><?=$row["total_cnt_3"]?>건</td>
	<td><?=number_format($row["avg_price"])?></td>
<?
}
?>
</tr>
<tr>
<th>합계</th>
<td><?=number_format($sum_total_price_1)?></td>
<td><?=$sum_total_cnt_1?>건</td>
<td><?=round($sum_cal_price_1,1)?>%</td>
<td><?=round($sum_cal_price_2,1)?>%</td>
<td><?=number_format($sum_total_price_2)?></td>
<td><?=$sum_total_cnt_2?>건</td>
<td><?=number_format($sum_total_price_3)?></td>
<td><?=$sum_total_cnt_3?>건</td>
<td><?=number_format($sum_avg_price)?></td>

</tr>
</tbody>
<tbody>
</table>

</div>

<script>

$(function(){
    $("#start_date, #end_date").datepicker({ changeMonth: true, changeYear: true, dateFormat: "yy-mm-dd", showButtonPanel: true, yearRange: "c-99:c+99"});
});

function ExcelDown(){
	
var form = "<form action='static_21_excel.php' method='post'>"; 
		form += "<input type='hidden' name='ptype' value='excel' />";
		form += "<input type='hidden' name='start_date' value='"+$('#start_date').val()+"' />"; 	
		form += "<input type='hidden' name='end_date' value='"+$('#end_date').val()+"' />"; 		 
		form += "</form>"; 
		jQuery(form).appendTo("body").submit().remove(); 
}


</script>

<?php
include_once (G5_ADMIN_PATH.'/admin.tail.php');
?>

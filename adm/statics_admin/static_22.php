<?php
$sub_menu = "500600";
include_once('./_common.php');

auth_check($auth[$sub_menu], 'r');

$g5['title'] = 'ERP 정기주문 매출현황';
include_once(G5_ADMIN_PATH.'/admin.head.php');
include_once(G5_PLUGIN_PATH.'/jquery-ui/datepicker.php');

$now = date( "Ymd" );
if ( $list_year == "" ) {
    $list_year = year( $now );
}

if (empty($date_gubun)) {
    $date_gubun = 'od_date1';
}

?>

<form id="fsearch" name="fsearch" class="local_sch01 local_sch" action="?" method="get">

    <div class="local_sch03  tbl_wrap" style="padding:10px;">
        <table>
            <tbody>
            <tr>
                <td>
                    검색옵션&nbsp;:&nbsp;
                    연도선택&nbsp;
                    <select name="list_year" id="list_year">
                        <?php
                        for ( $x = 2015; $x <= year( $now ) + 1; $x ++ ) {
                            ?>
                            <option value="<?php echo $x; ?>"<?php if (  $list_year == $x ) {
                                echo "selected";
                            } ?>><?php echo $x; ?>년
                            </option>
                            <?php
                        }
                        ?>
                    </select>
                    &nbsp;
                    <input type="submit" value="검색" class="get_theme_confc btn btn_01">
                    &nbsp;
                    조회구분&nbsp;
                    <select name="date_gubun" id="date_gubun" >
                        <option value="od_date1" <?php echo get_selected($date_gubun, 'od_date1'); ?>>주문일</option>
                        <option value="period_date" <?php echo get_selected($date_gubun, 'period_date'); ?>>정기배송일</option>
                    </select>
                    월선택&nbsp;
                    <select name="list_month" id="list_month">
                        <option value="">==선택==</option>
                        <option value="1" <?php echo get_selected($list_month, '1'); ?>>1월</option>
                        <option value="2" <?php echo get_selected($list_month, '2'); ?>>2월</option>
                        <option value="3" <?php echo get_selected($list_month, '3'); ?>>3월</option>
                        <option value="4" <?php echo get_selected($list_month, '4'); ?>>4월</option>
                        <option value="5" <?php echo get_selected($list_month, '5'); ?>>5월</option>
                        <option value="6" <?php echo get_selected($list_month, '6'); ?>>6월</option>
                        <option value="7" <?php echo get_selected($list_month, '7'); ?>>7월</option>
                        <option value="8" <?php echo get_selected($list_month, '8'); ?>>8월</option>
                        <option value="9" <?php echo get_selected($list_month, '9'); ?>>9월</option>
                        <option value="10" <?php echo get_selected($list_month, '10'); ?>>10월</option>
                        <option value="11" <?php echo get_selected($list_month, '11'); ?>>11월</option>
                        <option value="12" <?php echo get_selected($list_month, '12'); ?>>12월</option>
                    </select>
                    &nbsp;
                    <!--<select name="os_gubun" id="os_gubun">
                        <option value="1" <?php /*echo get_selected($os_gubun, '1'); */?>>OS전체</option>
                        <option value="5" <?php /*echo get_selected($os_gubun, '5'); */?>>얼라이언스 약국</option>
                        <option value="is" <?php /*echo get_selected($os_gubun, 'is'); */?>>이즈브레 전체</option>
                        <option value="isa" <?php /*echo get_selected($os_gubun, 'isa'); */?>>이즈브레 얼라이언스</option>
                        <option value="isc" <?php /*echo get_selected($os_gubun, 'isc'); */?>>이즈브레 고객</option>
                    </select>-->
                    <input type="button" value="엑셀 다운로드" class="get_theme_confc btn btn_02" style="cursor:pointer" onclick="formSubmit(1);">
                    <br><br>
                    ※ 참고사항<br>
                    - 기본 검색 조건은 주문일 기준으로 정산, 미정산 금액 추출합니다.<br>
                    - 전체 주문 리스트를 조회하기 위해서는 엑셀다운로드를 이용해주세요.<br>
                    - 조회구분, 월선택은 엑셀다운로드 검색 옵션입니다.
                </td>
            </tr>
            </tbody>
        </table>
    </div>

</form>

<!--
<div class="local_sch03 tbl_wrap">
    <ul style="padding: 5px;">
        <li style="padding: 2px;color: red;">아래 내용은 현재 메뉴의 검색 조건입니다.</li>
        <li style="padding: 2px;">조회방법 : 주문일 or 정기배송일</li>
        <li style="padding: 2px;">주문상태는 '취소', '반품' 제외</li>
        <li style="padding: 2px;">이즈브레앱 주문 제외</li>
        <li style="padding: 2px;">S00603 S00604 상품 제외 (달콩 행복 SET(약국), 달콩 행복 SET)</li>
        <li style="padding: 2px;">주문구분 : 약국주문</li>
        <li style="padding: 2px;">정기주문 Y</li>
    </ul>
</div>
-->

<div class="tbl_head01 tbl_wrap" style="width: 70%">

    <table>
        <caption> 연매출확인 </caption>
        <thead>
        <tr>
            <th scope="col" colspan="5"><?php echo $list_year; ?>년</th>
        </tr>
        <tr>
            <th>월</th>
            <th width="150">OS 전체 건수</th>
            <th>ERP 정산</th>
            <th>OS 정산</th>
            <th>OS 미정산</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $sql = "CALL SP_P_22($list_year);";
        $result = sql_query($sql);
        ?>
        <?php for ($i=0; $row=sql_fetch_array($result); $i++) { ?>
            <tr>
                <td><?php echo $row['view_month']; ?></td>
                <td><?php echo number_format($row['tot_cnt_1']); ?> 건</td>
                <td><?php echo number_format($row['tot_price_1']); ?></td>
                <td><?php echo number_format($row['calculate_price']); ?></td>
                <td><?php echo number_format($row['no_calculate_price']); ?></td>
            </tr>
        <?php } ?>
        </tbody>
    </table>

</div>


<script>

    function formSubmit(type) {

        var form = "<form action='static_22_excel.php' method='post'>";
        form += "<input type='hidden' name='ptype' value='excel' />";
        form += "<input type='hidden' name='list_year' value='"+$('#list_year').val()+"' />";
        form += "<input type='hidden' name='list_month' value='"+$('#list_month').val()+"' />";
        form += "<input type='hidden' name='cate_gubun' value='"+$('#cate_gubun option:selected').val()+"' />";
        form += "<input type='hidden' name='date_gubun' value='"+$('#date_gubun option:selected').val()+"' />";
        form += "</form>";
        jQuery(form).appendTo("body").submit().remove();
    }
</script>

<?php
include_once (G5_ADMIN_PATH.'/admin.tail.php');
?>

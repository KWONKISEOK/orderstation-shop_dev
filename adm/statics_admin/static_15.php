<?php
$sub_menu = "500240";
include_once('./_common.php');

auth_check($auth[$sub_menu], 'r');

$g5['title'] = '얼라이언스 회원수현황';
include_once(G5_ADMIN_PATH.'/admin.head.php');

$now = date( "Ymd" );
if ( $list_year == "" ) {
	$list_year = year( $now );
}

// 소멸은 해당월에 소멸되는날짜가 있으면 가지고 온것임..
?>

<form id="fsearch" name="fsearch" class="local_sch01 local_sch" action="?" method="get">

<div class="local_sch03  tbl_wrap" style="padding:10px;">
    <table>
    <tbody>
	<tr>
        <td>
			연도선택 &nbsp;
			<select name="list_year" id="list_year">
				<?php
				for ( $x = 2015; $x <= year( $now ) + 1; $x ++ ) {
					?>
					<option value="<?php echo $x; ?>"<?php if (  $list_year == $x ) {
						echo "selected";
					} ?>><?php echo $x; ?>년
					</option>
					<?php
				}
				?>
			</select>		
			&nbsp;
			<input type="submit" value="검색" class="get_theme_confc btn btn_01">
			 <input type="button" value="엑셀 다운로드" class="get_theme_confc btn btn_02" style="cursor:pointer" onclick="javascript:ExcelDown();">
        </td>
    </tr>
    </tbody>
    </table>
</div>

</form>

<?
$InputRows_1=array();
$InputRows_2=array();
$InputRows_3=array();
$InputRows_4=array();
$InputRows_5=array();
$InputRows_6=array();
$InputRows_7=array();
$InputRows_8=array();
$InputRows_9=array();
$InputRows_10=array();
$InputRows_11=array();
$InputRows_12=array();

$result = sql_query("CALL SP_P_15('".$list_year."')");

for ($i=0; $row=sql_fetch_array($result); $i++) {
	
	if( $row["view_month"] == "1" ){ array_push($InputRows_1,$row); }
	if( $row["view_month"] == "2" ){ array_push($InputRows_2,$row); }
	if( $row["view_month"] == "3" ){ array_push($InputRows_3,$row); }
	if( $row["view_month"] == "4" ){ array_push($InputRows_4,$row); }
	if( $row["view_month"] == "5" ){ array_push($InputRows_5,$row); }
	if( $row["view_month"] == "6" ){ array_push($InputRows_6,$row); }
	if( $row["view_month"] == "7" ){ array_push($InputRows_7,$row); }
	if( $row["view_month"] == "8" ){ array_push($InputRows_8,$row); }
	if( $row["view_month"] == "9" ){ array_push($InputRows_9,$row); }
	if( $row["view_month"] == "10" ){ array_push($InputRows_10,$row); }
	if( $row["view_month"] == "11" ){ array_push($InputRows_11,$row); }
	if( $row["view_month"] == "12" ){ array_push($InputRows_12,$row); }

}

$sum_price_1 = 0;
$sum_price_2 = 0;
$sum_price_3 = 0;

for($i=1; $i<=12; $i++){

	$sum_price_1 += ${"InputRows_".$i}[0]["cnt_1"];
	$sum_price_2 += ${"InputRows_".$i}[0]["cnt_2"];
	$sum_price_3 += ${"InputRows_".$i}[0]["cnt_3"];

}

?>
<div class="tbl_head01 tbl_wrap">
    <table>
    <caption> 얼라이언스 연매출비교 </caption>
    <thead>
    <tr>
        <th scope="col" rowspan="2">구분</th>
        <th scope="col" colspan="13"><?=$list_year?>년</th>
    </tr>
    <tr>
        <th>01월</th>
        <th>02월</th>
        <th>03월</th>
        <th>04월</th>
        <th>05월</th>
        <th>06월</th>
        <th>07월</th>
        <th>08월</th>
        <th>09월</th>
        <th>10월</th>
        <th>11월</th>
        <th>12월</th>
        <th>합계</th>
    </tr>
    </thead>
    <tbody>

    <tr>
        <td>전체OS약국 앱회원수</td>
        <td><?=number_format($InputRows_1[0]["cnt_1"])?></td>
        <td><?=number_format($InputRows_2[0]["cnt_1"])?></td>
		<td><?=number_format($InputRows_3[0]["cnt_1"])?></td>
		<td><?=number_format($InputRows_4[0]["cnt_1"])?></td>
		<td><?=number_format($InputRows_5[0]["cnt_1"])?></td>
		<td><?=number_format($InputRows_6[0]["cnt_1"])?></td>
		<td><?=number_format($InputRows_7[0]["cnt_1"])?></td>
		<td><?=number_format($InputRows_8[0]["cnt_1"])?></td>
		<td><?=number_format($InputRows_9[0]["cnt_1"])?></td>
		<td><?=number_format($InputRows_10[0]["cnt_1"])?></td>
		<td><?=number_format($InputRows_11[0]["cnt_1"])?></td>
		<td><?=number_format($InputRows_12[0]["cnt_1"])?></td>
		<td><?=number_format($sum_price_1)?></td>
	</tr>
    <tr>
        <td>얼라이언스약국 앱회원수</td>
        <td><?=number_format($InputRows_1[0]["cnt_2"])?></td>
        <td><?=number_format($InputRows_2[0]["cnt_2"])?></td>
		<td><?=number_format($InputRows_3[0]["cnt_2"])?></td>
		<td><?=number_format($InputRows_4[0]["cnt_2"])?></td>
		<td><?=number_format($InputRows_5[0]["cnt_2"])?></td>
		<td><?=number_format($InputRows_6[0]["cnt_2"])?></td>
		<td><?=number_format($InputRows_7[0]["cnt_2"])?></td>
		<td><?=number_format($InputRows_8[0]["cnt_2"])?></td>
		<td><?=number_format($InputRows_9[0]["cnt_2"])?></td>
		<td><?=number_format($InputRows_10[0]["cnt_2"])?></td>
		<td><?=number_format($InputRows_11[0]["cnt_2"])?></td>
		<td><?=number_format($InputRows_12[0]["cnt_2"])?></td>
		<td><?=number_format($sum_price_2)?></td>
	</tr>
    <tr>
        <td>얼라이언스 비율</td>
        <td><?=$InputRows_1[0]["avg_1"]?>%</td>
        <td><?=$InputRows_2[0]["avg_1"]?>%</td>
		<td><?=$InputRows_3[0]["avg_1"]?>%</td>
		<td><?=$InputRows_4[0]["avg_1"]?>%</td>
		<td><?=$InputRows_5[0]["avg_1"]?>%</td>
		<td><?=$InputRows_6[0]["avg_1"]?>%</td>
		<td><?=$InputRows_7[0]["avg_1"]?>%</td>
		<td><?=$InputRows_8[0]["avg_1"]?>%</td>
		<td><?=$InputRows_9[0]["avg_1"]?>%</td>
		<td><?=$InputRows_10[0]["avg_1"]?>%</td>
		<td><?=$InputRows_11[0]["avg_1"]?>%</td>
		<td><?=$InputRows_12[0]["avg_1"]?>%</td>
		<td><?=round( ($sum_price_2/$sum_price_1)*100 , 2 )?>%</td>
	</tr>
    <tr>
        <td>얼라이언스 앱회원 보유약국수</td>
        <td><?=number_format($InputRows_1[0]["cnt_3"])?></td>
        <td><?=number_format($InputRows_2[0]["cnt_3"])?></td>
		<td><?=number_format($InputRows_3[0]["cnt_3"])?></td>
		<td><?=number_format($InputRows_4[0]["cnt_3"])?></td>
		<td><?=number_format($InputRows_5[0]["cnt_3"])?></td>
		<td><?=number_format($InputRows_6[0]["cnt_3"])?></td>
		<td><?=number_format($InputRows_7[0]["cnt_3"])?></td>
		<td><?=number_format($InputRows_8[0]["cnt_3"])?></td>
		<td><?=number_format($InputRows_9[0]["cnt_3"])?></td>
		<td><?=number_format($InputRows_10[0]["cnt_3"])?></td>
		<td><?=number_format($InputRows_11[0]["cnt_3"])?></td>
		<td><?=number_format($InputRows_12[0]["cnt_3"])?></td>
		<td><?=number_format($sum_price_3)?></td>
	</tr>

    </tbody>
    </table>
</div>

<script>

function ExcelDown(){
	
	var frm = document.fsearch;
	frm.action = "./static_15_excel.php";
	frm.submit();
	frm.action = "?";

}

</script>

<?php
include_once (G5_ADMIN_PATH.'/admin.tail.php');
?>

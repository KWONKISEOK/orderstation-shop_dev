<?php
$sub_menu = "500250";
include_once('./_common.php');

auth_check($auth[$sub_menu], 'r');

$g5['title'] = '얼라이언스 연매출비교';
include_once(G5_ADMIN_PATH.'/admin.head.php');

$now = date( "Ymd" );
if ( $list_year == "" ) {
	$list_year = year( $now );
}

// 소멸은 해당월에 소멸되는날짜가 있으면 가지고 온것임..
?>

<form id="fsearch" name="fsearch" class="local_sch01 local_sch" action="?" method="get">

<div class="local_sch03  tbl_wrap" style="padding:10px;">
    <table>
    <tbody>
	<tr>
        <td>
			연도선택 &nbsp;
			<select name="list_year" id="list_year">
				<?php
				for ( $x = 2015; $x <= year( $now ) + 1; $x ++ ) {
					?>
					<option value="<?php echo $x; ?>"<?php if (  $list_year == $x ) {
						echo "selected";
					} ?>><?php echo $x; ?>년
					</option>
					<?php
				}
				?>
			</select>		
			&nbsp;
			<input type="submit" value="검색" class="get_theme_confc btn btn_01">
			&nbsp;
				월선택 
			<select name="list_month" id="list_month">
				
				<option value="1" <?php echo get_selected($list_month, '1'); ?>>1</option>
				<option value="2" <?php echo get_selected($list_month, '2'); ?>>2</option>
				<option value="3" <?php echo get_selected($list_month, '3'); ?>>3</option>
				<option value="4" <?php echo get_selected($list_month, '4'); ?>>4</option>
				<option value="5" <?php echo get_selected($list_month, '5'); ?>>5</option>
				<option value="6" <?php echo get_selected($list_month, '6'); ?>>6</option>
				<option value="7" <?php echo get_selected($list_month, '7'); ?>>7</option>
				<option value="8" <?php echo get_selected($list_month, '8'); ?>>8</option>
				<option value="9" <?php echo get_selected($list_month, '9'); ?>>9</option>
				<option value="10" <?php echo get_selected($list_month, '10'); ?>>10</option>
				<option value="11" <?php echo get_selected($list_month, '11'); ?>>11</option>
				<option value="12" <?php echo get_selected($list_month, '12'); ?>>12</option>
			</select>월
			&nbsp;
			 <select name="os_gubun" id="os_gubun">
				<option value="1" <?php echo get_selected($os_gubun, '1'); ?>>OS전체</option>
				<option value="5" <?php echo get_selected($os_gubun, '5'); ?>>얼라이언스 약국</option>
				<option value="is" <?php echo get_selected($os_gubun, 'is'); ?>>이즈브레 전체</option>
				<option value="isa" <?php echo get_selected($os_gubun, 'isa'); ?>>이즈브레 얼라이언스</option>
				<option value="isc" <?php echo get_selected($os_gubun, 'isc'); ?>>이즈브레 고객</option>
			</select>
			 <input type="button" value="엑셀 다운로드" class="get_theme_confc btn btn_02" style="cursor:pointer" onclick="javascript:ExcelDown();">
			
        </td>
    </tr>
    </tbody>
    </table>
	
</div>

</form>

<?
$InputRows_1=array();
$InputRows_2=array();
$InputRows_3=array();
$InputRows_4=array();
$InputRows_5=array();
$InputRows_6=array();
$InputRows_7=array();
$InputRows_8=array();
$InputRows_9=array();
$InputRows_10=array();
$InputRows_11=array();
$InputRows_12=array();

$result = sql_query("CALL SP_P_16('".$list_year."')");

for ($i=0; $row=sql_fetch_array($result); $i++) {
	
	if( $row["view_month"] == "1" ){ array_push($InputRows_1,$row); }
	if( $row["view_month"] == "2" ){ array_push($InputRows_2,$row); }
	if( $row["view_month"] == "3" ){ array_push($InputRows_3,$row); }
	if( $row["view_month"] == "4" ){ array_push($InputRows_4,$row); }
	if( $row["view_month"] == "5" ){ array_push($InputRows_5,$row); }
	if( $row["view_month"] == "6" ){ array_push($InputRows_6,$row); }
	if( $row["view_month"] == "7" ){ array_push($InputRows_7,$row); }
	if( $row["view_month"] == "8" ){ array_push($InputRows_8,$row); }
	if( $row["view_month"] == "9" ){ array_push($InputRows_9,$row); }
	if( $row["view_month"] == "10" ){ array_push($InputRows_10,$row); }
	if( $row["view_month"] == "11" ){ array_push($InputRows_11,$row); }
	if( $row["view_month"] == "12" ){ array_push($InputRows_12,$row); }

}

$sum_price_1 = 0;
$sum_price_2 = 0;
$sum_price_3 = 0;
$sum_price_4 = 0;
$sum_price_5 = 0;
$sum_price_6 = 0;

for($i=1; $i<=12; $i++){

    $sum_price_all += ${"InputRows_".$i}[0]["all_tot_price"];
	$sum_price_1 += ${"InputRows_".$i}[0]["tot_price_1"];
	$sum_price_2 += ${"InputRows_".$i}[0]["tot_price_2"];
	$sum_price_3 += ${"InputRows_".$i}[0]["tot_cnt_1"];
	$sum_price_4 += ${"InputRows_".$i}[0]["tot_cnt_2"];
	$sum_price_5 += ${"InputRows_".$i}[0]["tot_price_3"];
	$sum_price_6 += ${"InputRows_".$i}[0]["tot_cnt_3"];
	$sum_price_7 += ${"InputRows_".$i}[0]["is_tot_price_1"];
	$sum_price_8 += ${"InputRows_".$i}[0]["is_tot_price_2"];
	$sum_price_9 += ${"InputRows_".$i}[0]["is_tot_price_3"];
    $sum_price_10 += ${"InputRows_".$i}[0]["is_tot_price_4"];

}

?>
<div class="tbl_head01 tbl_wrap">
    <table>
    <caption> 얼라이언스 연매출비교 </caption>
    <thead>
    <tr>
        <th scope="col" colspan="2" rowspan="2">구분</th>
        <th scope="col" colspan="13"><?=$list_year?>년</th>
    </tr>
    <tr>
        <th>01월</th>
        <th>02월</th>
        <th>03월</th>
        <th>04월</th>
        <th>05월</th>
        <th>06월</th>
        <th>07월</th>
        <th>08월</th>
        <th>09월</th>
        <th>10월</th>
        <th>11월</th>
        <th>12월</th>
        <th>합계</th>
    </tr>
    </thead>
    <tbody>

    <tr>
        <td>전체매출</td>
        <td>OS+이즈브레</td>
        <td><?=number_format($InputRows_1[0]["all_tot_price"])?></td>
        <td><?=number_format($InputRows_2[0]["all_tot_price"])?></td>
        <td><?=number_format($InputRows_3[0]["all_tot_price"])?></td>
        <td><?=number_format($InputRows_4[0]["all_tot_price"])?></td>
        <td><?=number_format($InputRows_5[0]["all_tot_price"])?></td>
        <td><?=number_format($InputRows_6[0]["all_tot_price"])?></td>
        <td><?=number_format($InputRows_7[0]["all_tot_price"])?></td>
        <td><?=number_format($InputRows_8[0]["all_tot_price"])?></td>
        <td><?=number_format($InputRows_9[0]["all_tot_price"])?></td>
        <td><?=number_format($InputRows_10[0]["all_tot_price"])?></td>
        <td><?=number_format($InputRows_11[0]["all_tot_price"])?></td>
        <td><?=number_format($InputRows_12[0]["all_tot_price"])?></td>
        <td><?=number_format($sum_price_all)?></td>
    </tr>
    <tr>
        <td rowspan="3">매출</td>
        <td>OS전체</td>
        <td><?=number_format($InputRows_1[0]["tot_price_1"])?></td>
        <td><?=number_format($InputRows_2[0]["tot_price_1"])?></td>
		<td><?=number_format($InputRows_3[0]["tot_price_1"])?></td>
		<td><?=number_format($InputRows_4[0]["tot_price_1"])?></td>
		<td><?=number_format($InputRows_5[0]["tot_price_1"])?></td>
		<td><?=number_format($InputRows_6[0]["tot_price_1"])?></td>
		<td><?=number_format($InputRows_7[0]["tot_price_1"])?></td>
		<td><?=number_format($InputRows_8[0]["tot_price_1"])?></td>
		<td><?=number_format($InputRows_9[0]["tot_price_1"])?></td>
		<td><?=number_format($InputRows_10[0]["tot_price_1"])?></td>
		<td><?=number_format($InputRows_11[0]["tot_price_1"])?></td>
		<td><?=number_format($InputRows_12[0]["tot_price_1"])?></td>
		<td><?=number_format($sum_price_1)?></td>
	</tr>
    <tr>
        <td>얼라이언스</td>
        <td><?=number_format($InputRows_1[0]["tot_price_2"])?></td>
        <td><?=number_format($InputRows_2[0]["tot_price_2"])?></td>
		<td><?=number_format($InputRows_3[0]["tot_price_2"])?></td>
		<td><?=number_format($InputRows_4[0]["tot_price_2"])?></td>
		<td><?=number_format($InputRows_5[0]["tot_price_2"])?></td>
		<td><?=number_format($InputRows_6[0]["tot_price_2"])?></td>
		<td><?=number_format($InputRows_7[0]["tot_price_2"])?></td>
		<td><?=number_format($InputRows_8[0]["tot_price_2"])?></td>
		<td><?=number_format($InputRows_9[0]["tot_price_2"])?></td>
		<td><?=number_format($InputRows_10[0]["tot_price_2"])?></td>
		<td><?=number_format($InputRows_11[0]["tot_price_2"])?></td>
		<td><?=number_format($InputRows_12[0]["tot_price_2"])?></td>
		<td><?=number_format($sum_price_2)?></td>
	</tr>
    <tr>
        <td>비율</td>
        <td><?=$InputRows_1[0]["price_avg"]?>%</td>
        <td><?=$InputRows_2[0]["price_avg"]?>%</td>
		<td><?=$InputRows_3[0]["price_avg"]?>%</td>
		<td><?=$InputRows_4[0]["price_avg"]?>%</td>
		<td><?=$InputRows_5[0]["price_avg"]?>%</td>
		<td><?=$InputRows_6[0]["price_avg"]?>%</td>
		<td><?=$InputRows_7[0]["price_avg"]?>%</td>
		<td><?=$InputRows_8[0]["price_avg"]?>%</td>
		<td><?=$InputRows_9[0]["price_avg"]?>%</td>
		<td><?=$InputRows_10[0]["price_avg"]?>%</td>
		<td><?=$InputRows_11[0]["price_avg"]?>%</td>
		<td><?=$InputRows_12[0]["price_avg"]?>%</td>
		<td><?=round( ($sum_price_2/$sum_price_1)*100 , 2 )?>%</td>
	</tr>
    <tr>
        <td rowspan="3">건수</td>
        <td>OS전체</td>
        <td><?=number_format($InputRows_1[0]["tot_cnt_1"])?></td>
        <td><?=number_format($InputRows_2[0]["tot_cnt_1"])?></td>
		<td><?=number_format($InputRows_3[0]["tot_cnt_1"])?></td>
		<td><?=number_format($InputRows_4[0]["tot_cnt_1"])?></td>
		<td><?=number_format($InputRows_5[0]["tot_cnt_1"])?></td>
		<td><?=number_format($InputRows_6[0]["tot_cnt_1"])?></td>
		<td><?=number_format($InputRows_7[0]["tot_cnt_1"])?></td>
		<td><?=number_format($InputRows_8[0]["tot_cnt_1"])?></td>
		<td><?=number_format($InputRows_9[0]["tot_cnt_1"])?></td>
		<td><?=number_format($InputRows_10[0]["tot_cnt_1"])?></td>
		<td><?=number_format($InputRows_11[0]["tot_cnt_1"])?></td>
		<td><?=number_format($InputRows_12[0]["tot_cnt_1"])?></td>
		<td><?=number_format($sum_price_3)?></td>
	</tr>
    <tr>
        <td>얼라이언스</td>
        <td><?=number_format($InputRows_1[0]["tot_cnt_2"])?></td>
        <td><?=number_format($InputRows_2[0]["tot_cnt_2"])?></td>
		<td><?=number_format($InputRows_3[0]["tot_cnt_2"])?></td>
		<td><?=number_format($InputRows_4[0]["tot_cnt_2"])?></td>
		<td><?=number_format($InputRows_5[0]["tot_cnt_2"])?></td>
		<td><?=number_format($InputRows_6[0]["tot_cnt_2"])?></td>
		<td><?=number_format($InputRows_7[0]["tot_cnt_2"])?></td>
		<td><?=number_format($InputRows_8[0]["tot_cnt_2"])?></td>
		<td><?=number_format($InputRows_9[0]["tot_cnt_2"])?></td>
		<td><?=number_format($InputRows_10[0]["tot_cnt_2"])?></td>
		<td><?=number_format($InputRows_11[0]["tot_cnt_2"])?></td>
		<td><?=number_format($InputRows_12[0]["tot_cnt_2"])?></td>
		<td><?=number_format($sum_price_4)?></td>
	</tr>
    <tr>
        <td>비율</td>
        <td><?=$InputRows_1[0]["cnt_avg"]?>%</td>
        <td><?=$InputRows_2[0]["cnt_avg"]?>%</td>
		<td><?=$InputRows_3[0]["cnt_avg"]?>%</td>
		<td><?=$InputRows_4[0]["cnt_avg"]?>%</td>
		<td><?=$InputRows_5[0]["cnt_avg"]?>%</td>
		<td><?=$InputRows_6[0]["cnt_avg"]?>%</td>
		<td><?=$InputRows_7[0]["cnt_avg"]?>%</td>
		<td><?=$InputRows_8[0]["cnt_avg"]?>%</td>
		<td><?=$InputRows_9[0]["cnt_avg"]?>%</td>
		<td><?=$InputRows_10[0]["cnt_avg"]?>%</td>
		<td><?=$InputRows_11[0]["cnt_avg"]?>%</td>
		<td><?=$InputRows_12[0]["cnt_avg"]?>%</td>
		<td><?=round( ($sum_price_4/$sum_price_3)*100 , 2 )?>%</td>
	</tr>
    <tr>
        <td rowspan="2">객단가</td>
        <td>OS전체</td>
        <td><?=number_format($InputRows_1[0]["tot_price_3"])?></td>
        <td><?=number_format($InputRows_2[0]["tot_price_3"])?></td>
		<td><?=number_format($InputRows_3[0]["tot_price_3"])?></td>
		<td><?=number_format($InputRows_4[0]["tot_price_3"])?></td>
		<td><?=number_format($InputRows_5[0]["tot_price_3"])?></td>
		<td><?=number_format($InputRows_6[0]["tot_price_3"])?></td>
		<td><?=number_format($InputRows_7[0]["tot_price_3"])?></td>
		<td><?=number_format($InputRows_8[0]["tot_price_3"])?></td>
		<td><?=number_format($InputRows_9[0]["tot_price_3"])?></td>
		<td><?=number_format($InputRows_10[0]["tot_price_3"])?></td>
		<td><?=number_format($InputRows_11[0]["tot_price_3"])?></td>
		<td><?=number_format($InputRows_12[0]["tot_price_3"])?></td>
		<td><?=number_format($sum_price_5)?></td>
	</tr>
    <tr>
        <td>얼라이언스</td>
        <td><?=number_format($InputRows_1[0]["tot_cnt_3"])?></td>
        <td><?=number_format($InputRows_2[0]["tot_cnt_3"])?></td>
		<td><?=number_format($InputRows_3[0]["tot_cnt_3"])?></td>
		<td><?=number_format($InputRows_4[0]["tot_cnt_3"])?></td>
		<td><?=number_format($InputRows_5[0]["tot_cnt_3"])?></td>
		<td><?=number_format($InputRows_6[0]["tot_cnt_3"])?></td>
		<td><?=number_format($InputRows_7[0]["tot_cnt_3"])?></td>
		<td><?=number_format($InputRows_8[0]["tot_cnt_3"])?></td>
		<td><?=number_format($InputRows_9[0]["tot_cnt_3"])?></td>
		<td><?=number_format($InputRows_10[0]["tot_cnt_3"])?></td>
		<td><?=number_format($InputRows_11[0]["tot_cnt_3"])?></td>
		<td><?=number_format($InputRows_12[0]["tot_cnt_3"])?></td>
		<td><?=number_format($sum_price_6)?></td>
	</tr>
	<tr>
        <td rowspan="4">이즈브레매출</td>
        <td>이즈브레 전체</td>
        <td><?=number_format($InputRows_1[0]["is_tot_price_1"])?></td>
        <td><?=number_format($InputRows_2[0]["is_tot_price_1"])?></td>
		<td><?=number_format($InputRows_3[0]["is_tot_price_1"])?></td>
		<td><?=number_format($InputRows_4[0]["is_tot_price_1"])?></td>
		<td><?=number_format($InputRows_5[0]["is_tot_price_1"])?></td>
		<td><?=number_format($InputRows_6[0]["is_tot_price_1"])?></td>
		<td><?=number_format($InputRows_7[0]["is_tot_price_1"])?></td>
		<td><?=number_format($InputRows_8[0]["is_tot_price_1"])?></td>
		<td><?=number_format($InputRows_9[0]["is_tot_price_1"])?></td>
		<td><?=number_format($InputRows_10[0]["is_tot_price_1"])?></td>
		<td><?=number_format($InputRows_11[0]["is_tot_price_1"])?></td>
		<td><?=number_format($InputRows_12[0]["is_tot_price_1"])?></td>
		<td><?=number_format($sum_price_7)?></td>
	</tr>
    <tr>
        <td>얼라이언스</td>
        <td><?=number_format($InputRows_1[0]["is_tot_price_2"])?></td>
        <td><?=number_format($InputRows_2[0]["is_tot_price_2"])?></td>
		<td><?=number_format($InputRows_3[0]["is_tot_price_2"])?></td>
		<td><?=number_format($InputRows_4[0]["is_tot_price_2"])?></td>
		<td><?=number_format($InputRows_5[0]["is_tot_price_2"])?></td>
		<td><?=number_format($InputRows_6[0]["is_tot_price_2"])?></td>
		<td><?=number_format($InputRows_7[0]["is_tot_price_2"])?></td>
		<td><?=number_format($InputRows_8[0]["is_tot_price_2"])?></td>
		<td><?=number_format($InputRows_9[0]["is_tot_price_2"])?></td>
		<td><?=number_format($InputRows_10[0]["is_tot_price_2"])?></td>
		<td><?=number_format($InputRows_11[0]["is_tot_price_2"])?></td>
		<td><?=number_format($InputRows_12[0]["is_tot_price_2"])?></td>
		<td><?=number_format($sum_price_8)?></td>
	</tr>
	<tr>
        <td>이즈브레 고객</td>
        <td><?=number_format($InputRows_1[0]["is_tot_price_3"])?></td>
        <td><?=number_format($InputRows_2[0]["is_tot_price_3"])?></td>
		<td><?=number_format($InputRows_3[0]["is_tot_price_3"])?></td>
		<td><?=number_format($InputRows_4[0]["is_tot_price_3"])?></td>
		<td><?=number_format($InputRows_5[0]["is_tot_price_3"])?></td>
		<td><?=number_format($InputRows_6[0]["is_tot_price_3"])?></td>
		<td><?=number_format($InputRows_7[0]["is_tot_price_3"])?></td>
		<td><?=number_format($InputRows_8[0]["is_tot_price_3"])?></td>
		<td><?=number_format($InputRows_9[0]["is_tot_price_3"])?></td>
		<td><?=number_format($InputRows_10[0]["is_tot_price_3"])?></td>
		<td><?=number_format($InputRows_11[0]["is_tot_price_3"])?></td>
		<td><?=number_format($InputRows_12[0]["is_tot_price_3"])?></td>
		<td><?=number_format($sum_price_9)?></td>
	</tr>
    <tr>
        <td>이즈브레 고객 (추천약국 설정 고객)</td>
        <td><?=number_format($InputRows_1[0]["is_tot_price_4"])?></td>
        <td><?=number_format($InputRows_2[0]["is_tot_price_4"])?></td>
        <td><?=number_format($InputRows_3[0]["is_tot_price_4"])?></td>
        <td><?=number_format($InputRows_4[0]["is_tot_price_4"])?></td>
        <td><?=number_format($InputRows_5[0]["is_tot_price_4"])?></td>
        <td><?=number_format($InputRows_6[0]["is_tot_price_4"])?></td>
        <td><?=number_format($InputRows_7[0]["is_tot_price_4"])?></td>
        <td><?=number_format($InputRows_8[0]["is_tot_price_4"])?></td>
        <td><?=number_format($InputRows_9[0]["is_tot_price_4"])?></td>
        <td><?=number_format($InputRows_10[0]["is_tot_price_4"])?></td>
        <td><?=number_format($InputRows_11[0]["is_tot_price_4"])?></td>
        <td><?=number_format($InputRows_12[0]["is_tot_price_4"])?></td>
        <td><?=number_format($sum_price_10)?></td>
    </tr>

    </tbody>
    </table>
	*이즈브레매출-얼라이언스 : 얼라이언스약국을 추천약국으로 지정한 OS회원이 이즈브레앱으로 주문<br>
	*이즈브레매출-이즈브레 고객 : 이즈브레앱으로 가입하여 주문
</div>

<script>

function ExcelDown(){
	
	
	var form = "<form action='static_16_excel.php' method='post'>"; 
		form += "<input type='hidden' name='ptype' value='excel' />";
		form += "<input type='hidden' name='list_year' value='"+$('#list_year').val()+"' />"; 	
		form += "<input type='hidden' name='list_month' value='"+$('#list_month option:selected').val()+"' />"; 
		form += "<input type='hidden' name='os_gubun' value='"+$('#os_gubun option:selected').val()+"' />"; 	
		form += "</form>"; 
		jQuery(form).appendTo("body").submit().remove(); 

}

</script>

<?php
include_once (G5_ADMIN_PATH.'/admin.tail.php');
?>

<?php
$sub_menu = "500320";
include_once('./_common.php');

auth_check($auth[$sub_menu], 'r');

$g5['title'] = '회원 현황';
include_once(G5_ADMIN_PATH.'/admin.head.php');
include_once(G5_PLUGIN_PATH.'/jquery-ui/datepicker.php');

$sql_common=" from {$g5['member_table']} a "; #뭘 의미하는지 모르겠지만 필요한거같음;;
if(! preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $fr_date) ) $fr_date = '';
if(! preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $to_date) ) $to_date = '';
if($fr_date =='') $fr_date = date("Y-m-d", strtotime('-1 month'));
if($to_date =='') $to_date = date("Y-m-d");
$now = date( "Ymd" );
if ( $list_year == "" ) {
	$list_year = year( $now );
}

// 소멸은 해당월에 소멸되는날짜가 있으면 가지고 온것임..

?>


<form id="fsearch" name="fsearch" class="local_sch01 local_sch" action="?" method="get">

<div class="local_sch03  tbl_wrap" style="padding:10px;">
    <table>
    <tbody>
	<tr>
        <td>
			연도선택 &nbsp;
			<select name="list_year" id="list_year">
				<?php
				for ( $x = 2015; $x <= year( $now ) + 1; $x ++ ) {
					?>
					<option value="<?php echo $x; ?>"<?php if (  $list_year == $x ) {
						echo "selected";
					} ?>><?php echo $x; ?>년
					</option>
					<?php
				}
				?>
			</select>		
			&nbsp;

			<input type="submit" value="검색" class="get_theme_confc btn btn_01">
			
			&nbsp; 월선택 &nbsp;
			<select name="mon" id="mon">
				<option value='1'>1월</option>
				<option value='2'>2월</option>
				<option value='3'>3월</option>
				<option value='4'>4월</option>
				<option value='5'>5월</option>
				<option value='6'>6월</option>
				<option value='7'>7월</option>
				<option value='8'>8월</option>
				<option value='9'>9월</option>
				<option value='10'>10월</option>
				<option value='11'>11월</option>
				<option value='12'>12월</option>
			</select> 
			&nbsp;
			
			 회원구분 &nbsp;
			<select name="member" id="member">
				<option value='0'>얼라이언스회원</option>
				<option value='1'>일반회원</option>
            </select>
				 &nbsp; 
				&nbsp;
			<input type="button" value="검색" class="get_theme_confc btn btn_01" style="cursor:pointer" onclick="formSubmit(1);"> 
			&nbsp;
			 <input type="button" value="엑셀 다운로드" class="get_theme_confc btn btn_02" style="cursor:pointer" onclick="javascript:ExcelDown();">
	   </td>
    </tr>
    </tbody>
    </table>
</div>

</form>

<div class="tbl_head01 tbl_wrap">
    <table>
    <caption> 회원 현황 </caption>
    <thead>
    <tr>
        <th scope="col" colspan="2" rowspan="2">구분</th>
        <th scope="col" colspan="13"><?=$list_year?>년</th>
    </tr>
    <tr>
        <th>01월</th>
        <th>02월</th>
        <th>03월</th>
        <th>04월</th>
        <th>05월</th>
        <th>06월</th>
        <th>07월</th>
        <th>08월</th>
        <th>09월</th>
        <th>10월</th>
        <th>11월</th>
        <th>12월</th>
		  <th>합계</th>
    </tr>
    </thead>
    <tbody>
	<?php #사용할 변수 선언
	
	$sp_01=0; #서팩2회원 mb_referee=3 and mb_type=0인 약국을 제외한 회원
	$sp_02=0;
	$sp_03=0;
	$sp_04=0;
	$sp_05=0;
	$sp_06=0;
	$sp_07=0;
	$sp_08=0;
	$sp_09=0;
	$sp_10=0;
	$sp_11=0;
	$sp_12=0; #12월 까지의 일반회원
	$total_sp=0;
	
	$mn_01=0; #모니터링
	$mn_02=0;
	$mn_03=0;
	$mn_04=0;
	$mn_05=0;
	$mn_06=0;
	$mn_07=0;
	$mn_08=0;
	$mn_09=0;
	$mn_10=0;
	$mn_11=0;
	$mn_12=0; #12월 까지 모니터링
	$total_mn=0;
	
	$gm_01=0; #01월까지의 일반 회원 [제네럴멤버]  --> 일반회원:서팩 모니터링 등 다 포함
	$gm_02=0;
	$gm_03=0;
	$gm_04=0;
	$gm_05=0;
	$gm_06=0;
	$gm_07=0;
	$gm_08=0;
	$gm_09=0;
	$gm_10=0;
	$gm_11=0;
	$gm_12=0; #12월 까지의 일반회원
	$total_gm=0;
	
	$al_01=0; #얼라이언스 회원 01월까지의= 회원수
	$al_02=0;
	$al_03=0;
	$al_04=0;
	$al_05=0;
	$al_06=0;
	$al_07=0;
	$al_08=0;
	$al_09=0;
	$al_10=0;
	$al_11=0;
	$al_12=0; #얼라회원 12월까지의 현황 =회원수
	$total_al=0;
	
	#비사용칸 표시를 위한 변수 선언 [휴먼 및 탈퇴]
		$hu_01=0; #01월의 휴면 회원 
		$hu_02=0;
		$hu_03=0;
		$hu_04=0;
		$hu_05=0;
		$hu_06=0;
		$hu_07=0;
		$hu_08=0;
		$hu_09=0;
		$hu_10=0;
		$hu_11=0;
		$hu_12=0; #12월 까지의 휴면회원
		
		$out_01=0; #01월 까지의 탈퇴 회원
		$out_02=0;
		$out_03=0;
		$out_04=0;
		$out_05=0;
		$out_06=0;
		$out_07=0;
		$out_08=0;
		$out_09=0;
		$out_10=0;
		$out_11=0;
		$out_12=0; #12월까지의 탈퇴회원
?>
<!--일반회원 수 구하기위한 쿼리 시작-->
<!--$sql = "select count(mb_leave_date)as out1 from tbl_member where mb_leave_date like '{$list_year}01%' ";
#$result=sql_fetch($sql);
#$out_01=$result['out1']; -->
<?php 
#$sql ="select count(*) as cnt from tbl_member WHERE mb_recommend not in ( select mb_id from tbl_member where mb_type  = '1' and mb_referee = '5')
#and mb_datetime between '{$list_year}-01-01' and '{$list_year}-01-31'";
   
#$result=sql_fetch($sql);
#$gm_01=$result['cnt'];

$result=sql_query("CALL SP_P_9022('". $list_year."')"); 
#echo $result2;
for($i=0 ; $row=sql_fetch_array($result) ; $i++){

	$al_01 = $row["a1"];
	$al_02 = $row["a2"];
	$al_03 = $row["a3"];
	$al_04 = $row["a4"];
	$al_05 = $row["a5"];
	$al_06 = $row["a6"];
	$al_07 = $row["a7"];
	$al_08 = $row["a8"];
	$al_09 = $row["a9"];
	$al_10 = $row["a10"];
	$al_11 = $row["a11"];
	$al_12 = $row["a12"]; 
	
	$sp_01 = $row["sp1"];
	$sp_02 = $row["sp2"];
	$sp_03 = $row["sp3"];
	$sp_04 = $row["sp4"];
	$sp_05 = $row["sp5"];
	$sp_06 = $row["sp6"];
	$sp_07 = $row["sp7"];
	$sp_08 = $row["sp8"];
	$sp_09 = $row["sp9"];
	$sp_10 = $row["sp10"];
	$sp_11 = $row["sp11"];
	$sp_12 = $row["sp12"]; 
	
	$mn_01 = $row["mn1"];
	$mn_02 = $row["mn2"];
	$mn_03 = $row["mn3"];
	$mn_04 = $row["mn4"];
	$mn_05 = $row["mn5"];
	$mn_06 = $row["mn6"];
	$mn_07 = $row["mn7"];
	$mn_08 = $row["mn8"];
	$mn_09 = $row["mn9"];
	$mn_10 = $row["mn10"];
	$mn_11 = $row["mn11"];
	$mn_12 = $row["mn12"]; 
	
	$gm_01 = $row["gm1"]; #직원포함한 일반회원
	$gm_02 = $row["gm2"];
	$gm_03 = $row["gm3"];
	$gm_04 = $row["gm4"];
	$gm_05 = $row["gm5"];
	$gm_06 = $row["gm6"];
	$gm_07 = $row["gm7"];
	$gm_08 = $row["gm8"];
	$gm_09 = $row["gm9"];
	$gm_10 = $row["gm10"];
	$gm_11 = $row["gm11"];
	$gm_12 = $row["gm12"]; 

	$hu_01 = $row["hu1"]; #휴면
	$hu_02 = $row["hu2"];
	$hu_03 = $row["hu3"];
	$hu_04 = $row["hu4"];
	$hu_05 = $row["hu5"];
	$hu_06 = $row["hu6"];
	$hu_07 = $row["hu7"];
	$hu_08 = $row["hu8"];
	$hu_09 = $row["hu9"];
	$hu_10 = $row["hu10"];
	$hu_11 = $row["hu11"];
	$hu_12 = $row["hu12"]; 

	$out_01 = $row["out1"]; #탈퇴
	$out_02 = $row["out2"];
	$out_03 = $row["out3"];
	$out_04 = $row["out4"];
	$out_05 = $row["out5"];
	$out_06 = $row["out6"];
	$out_07 = $row["out7"];
	$out_08 = $row["out8"];
	$out_09 = $row["out9"];
	$out_10 = $row["out10"];
	$out_11 = $row["out11"];
	$out_12 = $row["out12"]; 

}
?>


<?php
$total_al=$al_01+$al_02+$al_03+$al_04+$al_05+$al_06+$al_07+$al_08+$al_09+$al_10+$al_11+$al_12; #총 얼라이언스회원
$total_sp=$sp_01+$sp_02+$sp_03+$sp_04+$sp_05+$sp_06+$sp_07+$sp_08+$sp_09+$sp_10+$sp_11+$sp_12;#총 서팩 회원
$total_mn=$mn_01+$mn_02+$mn_03+$mn_04+$mn_05+$mn_06+$mn_07+$mn_08+$mn_09+$mn_10+$mn_11+$mn_12; #모니터링
$total_gm=$gm_01+$gm_02+$gm_03+$gm_04+$gm_05+$gm_06+$gm_07+$gm_08+$gm_09+$gm_10+$gm_11+$gm_12; # 직원포함해서 일반회원 수

$total_hu=$hu_01+$hu_02+$hu_03+$hu_04+$hu_05+$hu_06+$hu_07+$hu_08+$hu_09+$hu_10+$hu_11+$hu_12; #전체 휴면
$total_out=$out_01+$out_02+$out_03+$out_04+$out_05+$out_06+$out_07+$out_08+$out_09+$out_10+$out_11+$out_12; #전체 탈퇴

$sm_01=$al_01 + $sp_01 + $mn_01+$gm_01; # 1월소계 (small)
$sm_02=$al_02 + $sp_02 + $mn_02+$gm_02; 
$sm_03=$al_03 + $sp_03 + $mn_03+$gm_03; 
$sm_04=$al_04 + $sp_04 + $mn_04+$gm_04; 
$sm_05=$al_05 + $sp_05 + $mn_05+$gm_05; 
$sm_06=$al_06 + $sp_06 + $mn_06+$gm_06; 
$sm_07=$al_07 + $sp_07 + $mn_07+$gm_07; 
$sm_08=$al_08 + $sp_08 + $mn_08+$gm_08; 
$sm_09=$al_09 + $sp_09 + $mn_09+$gm_09; 
$sm_10=$al_10 + $sp_10 + $mn_10+$gm_10; 
$sm_11=$al_11 + $sp_11 + $mn_11+$gm_11; 
$sm_12=$al_12 + $sp_12 + $mn_12+$gm_12; 
$use_total=$total_al + $total_sp+$total_mn+$total_gm ;#사용의 합계의 소계영역 얼라이언스 합계아래칸

$no_sm01=$hu_01+$out_01;#비사용의 소계
$no_sm02=$hu_02+$out_02;
$no_sm03=$hu_03+$out_03;
$no_sm04=$hu_04+$out_04;
$no_sm05=$hu_05+$out_05;
$no_sm06=$hu_06+$out_06;
$no_sm07=$hu_07+$out_07;
$no_sm08=$hu_08+$out_08;
$no_sm09=$hu_09+$out_09;
$no_sm10=$hu_10+$out_10;
$no_sm11=$hu_11+$out_11;
$no_sm12=$hu_12+$out_12;

$total_year=$sm_01+$sm_02+$sm_03+$sm_04+$sm_05+$sm_06+$sm_07+$sm_08+$sm_09+$sm_10+$sm_11+$sm_12;#해당 년도의 합계 == 각각의 소계를 다 더한 값
$no_total=$total_hu+$total_out;#휴면 탈퇴 각 합계를 합친 것 소계영역
?>


<!--SELECT a.mb_id, a.mb_name, a.mb_datetime, b.pharm_name,b.pharm_custno
                 FROM tbl_member a , tbl_member  b 
                 WHERE a.mb_type='0' and a.mb_recommend IN (SELECT b.mb_id FROM tbl_member WHERE b.mb_type='1' AND b.mb_referee='5') 
                 and a.mb_datetime BETWEEN '2019-03-01' AND '2019-04-01';
#######2019 /3 의 얼라이언스 회원 내역 쭈르륵 뽑은 것
-->

        <td rowspan="7">사용</td>
		<!--<tr>
        <td>서팩 1회원</td> 
        <td><?=number_format()?></td>
        <td><?=number_format()?></td>
		<td><?=number_format()?></td>
		<td><?=number_format()?></td>
		<td><?=number_format()?></td>
		<td><?=number_format()?></td>
		<td><?=number_format()?></td>
		<td><?=number_format()?></td>
		<td><?=number_format()?></td>
		<td><?=number_format()?></td>
		<td><?=number_format()?></td>
		<td><?=number_format()?></td>
		<td><?=number_format()?></td>
	</tr>-->
		<tr>
        <td>OS회원(구 서팩2)</td> 
        <td><?=number_format($sp_01)?></td>
        <td><?=number_format($sp_02)?></td>
		<td><?=number_format($sp_03)?></td>
		<td><?=number_format($sp_04)?></td>
		<td><?=number_format($sp_05)?></td>
		<td><?=number_format($sp_06)?></td>
		<td><?=number_format($sp_07)?></td>
		<td><?=number_format($sp_08)?></td>
		<td><?=number_format($sp_09)?></td>
		<td><?=number_format($sp_10)?></td>
		<td><?=number_format($sp_11)?></td>
		<td><?=number_format($sp_12)?></td>
		<td><?=number_format($total_sp)?></td>
	</tr>
		<tr>
        <td>모니터링</td> 
        <td><?=number_format($mn_01)?></td>
        <td><?=number_format($mn_02)?></td>
		<td><?=number_format($mn_03)?></td>
		<td><?=number_format($mn_04)?></td>
		<td><?=number_format($mn_05)?></td>
		<td><?=number_format($mn_06)?></td>
		<td><?=number_format($mn_07)?></td>
		<td><?=number_format($mn_08)?></td>
		<td><?=number_format($mn_09)?></td>
		<td><?=number_format($mn_10)?></td>
		<td><?=number_format($mn_11)?></td>
		<td><?=number_format($mn_12)?></td>
		<td><?=number_format($total_mn)?></td>
	</tr>
	  <tr>
        <td>일반회원</td> <!-- 모니터링 직원 등의 회원도 포함-->
        <td><?=number_format($gm_01)?></td>
        <td><?=number_format($gm_02)?></td>
		<td><?=number_format($gm_03)?></td>
		<td><?=number_format($gm_04)?></td>
		<td><?=number_format($gm_05)?></td>
		<td><?=number_format($gm_06)?></td>
		<td><?=number_format($gm_07)?></td>
		<td><?=number_format($gm_08)?></td>
		<td><?=number_format($gm_09)?></td>
		<td><?=number_format($gm_10)?></td>
		<td><?=number_format($gm_11)?></td>
		<td><?=number_format($gm_12)?></td>
		<td><?=number_format($total_gm)?></td>
	</tr>
	  <tr>
        <td>얼라이언스</td>
        <td><?=number_format($al_01)?></td>
        <td><?=number_format($al_02)?></td>
		<td><?=number_format($al_03)?></td>
		<td><?=number_format($al_04)?></td>
		<td><?=number_format($al_05)?></td>
		<td><?=number_format($al_06)?></td>
		<td><?=number_format($al_07)?></td>
		<td><?=number_format($al_08)?></td>
		<td><?=number_format($al_09)?></td>
		<td><?=number_format($al_10)?></td>
		<td><?=number_format($al_11)?></td>
		<td><?=number_format($al_12)?></td>
		<td><?=number_format($total_al)?></td> <!-- 합계-->
	</tr> 

	  <tr class="local_sch03  tbl_wrap">
        <td>소계</td>
        <td><?=number_format($sm_01)?></td>
        <td><?=number_format($sm_02)?></td>
		<td><?=number_format($sm_03)?></td>
		<td><?=number_format($sm_04)?></td>
		<td><?=number_format($sm_05)?></td>
		<td><?=number_format($sm_06)?></td>
		<td><?=number_format($sm_07)?></td>
		<td><?=number_format($sm_08)?></td>
		<td><?=number_format($sm_09)?></td>
		<td><?=number_format($sm_10)?></td>
		<td><?=number_format($sm_11)?></td>
		<td><?=number_format($sm_12)?></td>
		<td><?=number_format($use_total)?></td>
	</tr>
	<tr class="local_sch03  tbl_wrap">
        <td ><?php echo$list_year?>년 합계</td> 
		<td colspan="12"> <?=number_format($total_year)?></td>
        <td>X</td>
        
	</tr>
    <tr>
         <td rowspan="4">비사용</td>
		<tr>
		<td>휴면</td>
      	<td><?=number_format($hu_01)?></td>
     	 <td><?=number_format($hu_02)?></td>
		<td><?=number_format($hu_03)?></td>
		<td><?=number_format($hu_04)?></td>
		<td><?=number_format($hu_05)?></td>
		<td><?=number_format($hu_06)?></td>
		<td><?=number_format($hu_07)?></td>
		<td><?=number_format($hu_08)?></td>
		<td><?=number_format($hu_09)?></td>
		<td><?=number_format($hu_10)?></td>
		<td><?=number_format($hu_11)?></td>
		<td><?=number_format($hu_12)?></td>
		<td><?=number_format($total_hu)?></td>
	</tr>

	<tr>
		<td>탈퇴</td>

        <td><?= number_format($out_01)?></td>
        <td><?= number_format($out_02)?></td>
		<td><?= number_format($out_03)?></td>
		<td><?= number_format($out_04)?></td>
		<td><?= number_format($out_05)?></td>
		<td><?= number_format($out_06)?></td>
		<td><?= number_format($out_07)?></td>
		<td><?= number_format($out_08)?></td>
		<td><?= number_format($out_09)?></td>
		<td><?= number_format($out_10)?></td>
		<td><?= number_format($out_11)?></td>
		<td><?= number_format($out_12)?></td>
		<td><?=number_format($total_out)?></td> <!--전체 탈퇴회원수-->
	</tr>

	<tr class="local_sch03  tbl_wrap">	
      <td>소계</td>
        <td><?=number_format($no_sm01)?></td>
		<td><?=number_format($no_sm02)?></td>
		<td><?=number_format($no_sm03)?></td>
		<td><?=number_format($no_sm04)?></td>
		<td><?=number_format($no_sm05)?></td>
		<td><?=number_format($no_sm06)?></td>
		<td><?=number_format($no_sm07)?></td>
		<td><?=number_format($no_sm08)?></td>
		<td><?=number_format($no_sm09)?></td>
		<td><?=number_format($no_sm10)?></td>
		<td><?=number_format($no_sm11)?></td>
		<td><?=number_format($no_sm12)?></td>
		<td><?=number_format($no_total)?></td>
	</tr>
    </tbody>
    </table>
</div>
<link rel="stylesheet" type="text/css" media="screen" href="/adm/jqGrid/css/ui.jqgrid.css" />  
<form name="forderlist" id="forderlist" method="post" >
<input type="hidden" id="mode" name="mode"  value="read" />




<div class=" tbl_wrap1">
<div id="preloader">
	 <img src="/adm/jqGrid/waiting.gif" align="absmiddle"> Loading...		
</div>
<table id="grid-table"></table>
<div id="grid-pager" ></div>

</div>   
</form>
<?php //$qstr ='comp_code='.$_GET['comp_code'].'&'.$qstr;
echo get_paging(G5_IS_MOBILE ? $config['cf_mobile_pages'] : $config['cf_write_pages'], $page, $total_page, "{$_SERVER['SCRIPT_NAME']}?$qstr&amp;page="); ?>

<script>

var selectedMonth=$("#mon").val();
var selectedyear=$("#list_year").val();
var selectedMember=$("#member").val();
</script>

<script src="/adm/jqGrid/js/i18n/grid.locale-ko.js"   type="text/javascript"></script>  
<script src="/adm/jqGrid/js/jquery.jqGrid.min.js"    type="text/javascript"></script> 
<script type="text/javascript"> 
	var grid_selector = "#grid-table";
	var pager_selector = "#grid-pager";

	$(document).ready(function () {

		$('#mon').on('change',function() {
			selectedMonth=$("#mon").val();
		});
		
		$('#list_year').on('change',function() {
			selectedyear=$("#list_year").val();
		});
		$('#member').on('change',function() {
			selectedMember=$("#member").val();
		});
		var parent_column = $(grid_selector).closest('[class*="col-"]');
		//resize to fit page size
		$(window).on('resize.jqGrid', function () {
			$(grid_selector).jqGrid( 'setGridWidth', parent_column.width() );
		})

		jQuery(grid_selector).jqGrid({
			datatype :  fnSearch,
			colNames: ['가입일자','회원ID','회원이름','약국이름','약국코드'],	
		colModel: [
					{ name: 'mb_datetime',      index: 'mb_datetime',      width: 70, align: "center", sorttype: "text" },  
				   { name: 'mb_id',      index: 'mb_id',      width: 55, align: "center" },    				  		 
				   { name: 'mb_name',      index: 'mb_name',      width: 50, align: "center", sorttype: "text" },    		     				  
				   { name: 'pharm_name',      index: 'pharm_name',      width: 60, align: "center", sorttype: "text"  },	   
				   { name: 'pharm_custno',      index: 'pharm_custno',      width: 40, align: "center", sorttype: "int" }, 			
				  ],
			viewrecords : true,
			rowNum:1000,
			rowList:[1000],
			pager : pager_selector,
			altRows: true,
			sortname: 'od_id',
			sortorder: "desc",
			height:310,
			multiboxonly: true,			
			autowidth: true,
            shrinkToFit: true
		});
	});

	function formSubmit(type) {
		if(type==1) {
			$('#mode').val('read');
			jQuery(grid_selector).trigger("reloadGrid");
		} else {


			var form = "<form action='../shop_order/test9022_1.php' method='post'>"; 
			
			form += "<input type='hidden' name='mode' value='excel' />"; 
			form += "<input type='hidden' name=''#mb_datetime'' value='"+$('#mb_datetime').val()+"' />";
			form += "<input type='hidden' name='mb_id' value='"+$('#mb_id').val()+"' />"; 
			form += "<input type='hidden' name='mb_name' value='"+$('#mb_name').val()+"' />";  
			form += "<input type='hidden' name='pharm_name' value='"+$('#pharm_name').val()+"' />"; 
			form += "<input type='hidden' name='pharm_custno' value='"+$('#pharm_custno').val()+"' />";
				
			form += "</form>"; 
			jQuery(form).appendTo("body").submit().remove(); 


		}
	}
	function fnSearch(pdata) {
		$("#preloader").show();
		var jqGridParam = '';
		for (var i in pdata) {
			jqGridParam += '&'+i+'='+pdata[i];
		}

		jqGridParam += '&searchMonth='+selectedMonth;
		jqGridParam += '&searchyear='+selectedyear;
		jqGridParam += '&searchMember='+selectedMember;

		$.ajax({
			url:'../shop_order/test9022_01.php',
			type:'POST',
			data:$('#forderlist').serialize()+jqGridParam,
			success: function(data) {

				var thegrid = jQuery(grid_selector)[0];
				thegrid.addJSONData(eval("("+data+")"));
			},

			complete: function(jsondata,stat) {
				$("#preloader").fadeOut("fast");
				//var thegrid = jQuery(grid_selector)[0];
				//updatePagerIcons(thegrid);			
			}
		});
	}
	function updatePagerIcons(table) {
		var replacement = 
		{
			'ui-icon-seek-first' : 'fa fa-angle-double-left bigger-140',
			'ui-icon-seek-prev' : 'fa fa-angle-left bigger-140',
			'ui-icon-seek-next' : 'fa fa-angle-right bigger-140',
			'ui-icon-seek-end' : 'fa fa-angle-double-right bigger-140'
		};
		$('.ui-pg-table:not(.navtable) > tbody > tr > .ui-pg-button > .ui-icon').each(function(){
			var icon = $(this);
			var $class = $.trim(icon.attr('class').replace('ui-icon', ''));
			
			if($class in replacement) icon.attr('class', 'ui-icon '+replacement[$class]);
		})
	}

	function ExcelDown(){
		
		var frm = document.fsearch;
		frm.action = "./static_9022_excel.php";
		frm.submit();
		frm.action = "?";

	}


</script>
<iframe name="iFrm" style="display: none;" ></iframe>

<?php
include_once (G5_ADMIN_PATH.'/admin.tail.php');
?>

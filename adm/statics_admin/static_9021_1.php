<?php
$sub_menu = "500310";
include_once('./_common.php');

auth_check($auth[$sub_menu], "r");

$g5['title'] = '포인트 및 쿠폰현황';
include_once(G5_ADMIN_PATH.'/admin.head.php');
include_once(G5_PLUGIN_PATH.'/jquery-ui/datepicker.php');

$now = date( "Ymd" );
if ( $list_year == "" ) {
	$list_year = year( $now );
}
?>

<link rel="stylesheet" type="text/css" media="screen" href="/adm/jqGrid/css/ui.jqgrid.css" />  


<form name="forderlist" id="forderlist" method="post" >
<input type="hidden" id="mode" name="mode"  value="read" />

<div class="local_sch03  tbl_wrap" style="padding:10px;">
    <table>
    <tbody>
	<tr>
	<br>
	검색구분 &nbsp;
	        <button type="button" style="border-radius:5px;" onclick="location.href='https://www.orderstation.co.kr/adm/statics_admin/static_9021.php' ">&nbsp;지급&nbsp;</button>&nbsp;
	        <button type="button" style="border-radius:5px;" onclick="location.href='https://www.orderstation.co.kr/adm/statics_admin/static_9021_2.php' ">&nbsp;사용&nbsp;</button>&nbsp;
	        <button type="button" style="border-radius:5px; background-color: #0000B7;" onclick="location.href='https://www.orderstation.co.kr/adm/statics_admin/static_9021_1.php' ">&nbsp;소멸&nbsp;</button>
<br><br>
        <td>
			연도선택 &nbsp;
			<select name="list_year" id="list_year">
				<?php
				for ( $x = 2018; $x <= year( $now ); $x ++ ) {
					?>
					<option value="<?php echo $x; ?>"<?php if (  $list_year == $x ) {
						echo "selected";
					} ?>><?php echo $x; ?>년
					</option>
					<?php
				}
				?>
			</select>		
			&nbsp;
			<input type="submit" value="검색" class="get_theme_confc btn btn_01">
        &nbsp 월선택 &nbsp;
	<?
	if($list_year=="2018")
	{
		?>
	<select name="mon" id="mon">
    <option value="9">9월</option>
	<option value="10">10월</option>
    <option value="11">11월</option>
	<option value="12">12월</option>
    </select> &nbsp
	<?
	}
	else{
	?>
	<select name="mon" id="mon">
	<?
	for($i=1;$i<=month( $now );$i++)
	{
		?>
		<option value= "<? echo $i ?>" > <? echo $i ?>월</option>
		<?
	}
	?>
	</select>
	<?
	}
	?>
	&nbsp;
	<input type="button" value="검색" class="get_theme_confc btn btn_01" style="cursor:pointer" onclick="formSubmit(1);"> 
        </td>
    </tr>
    </tbody>
    </table>
	<br>
</div>

<div class="tbl_head01 tbl_wrap">
    <table>
    <caption> 포인트 및 쿠폰현황 </caption>
    <thead>
    <tr>
        <th scope="col" colspan="2" rowspan="2">구분</th>
        <th scope="col" colspan="13"><?=$list_year?>년</th>
    </tr>
    <tr>
        <th>01월</th>
        <th>02월</th>
        <th>03월</th>
        <th>04월</th>
        <th>05월</th>
        <th>06월</th>
        <th>07월</th>
        <th>08월</th>
        <th>09월</th>
        <th>10월</th>
        <th>11월</th>
        <th>12월</th>
		<th>합계</th>
    </tr>
    </thead>
	
    <tbody>
	
	<?php

		/* 지급 합계 변수 */
		$a_m_1 = 0;
		$a_m_2 = 0;
		$a_m_3 = 0;
		$a_m_4 = 0;
		$a_m_5 = 0;
		$a_m_6 = 0;
		$a_m_7 = 0;
		$a_m_8 = 0;
		$a_m_9 = 0;
		$a_m_10 = 0;
		$a_m_11 = 0;
		$a_m_12 = 0;

		/* 사용 합계 변수 */
		$b_m_1 = 0;
		$b_m_2 = 0;
		$b_m_3 = 0;
		$b_m_4 = 0;
		$b_m_5 = 0;
		$b_m_6 = 0;
		$b_m_7 = 0;
		$b_m_8 = 0;
		$b_m_9 = 0;
		$b_m_10 = 0;
		$b_m_11 = 0;
		$b_m_12 = 0;

		/* 소멸 합계 변수 */
		$c_m_1 = 0;
		$c_m_2 = 0;
		$c_m_3 = 0;
		$c_m_4 = 0;
		$c_m_5 = 0;
		$c_m_6 = 0;
		$c_m_7 = 0;
		$c_m_8 = 0;
		$c_m_9 = 0;
		$c_m_10 = 0;
		$c_m_11 = 0;
		$c_m_12 = 0;

		$result = sql_query("CALL SP_P_11('".$list_year."')");
		for ($i=0; $row=sql_fetch_array($result); $i++) {

			if( $i == 0 ){
				$title = "포인트";
			}else if( $i == 3 ){
				$title = "쿠폰";			
			}else{
				$title = "";
			}

			$m_1 = $row["m_1"];
			$m_2 = $row["m_2"];
			$m_3 = $row["m_3"];
			$m_4 = $row["m_4"];
			$m_5 = $row["m_5"];
			$m_6 = $row["m_6"];
			$m_7 = $row["m_7"];
			$m_8 = $row["m_8"];
			$m_9 = $row["m_9"];
			$m_10 = $row["m_10"];
			$m_11 = $row["m_11"];
			$m_12 = $row["m_12"];

			if( $m_9 < 0 ){ $m_9 = $m_9 * -1; }
			if( $m_10 < 0 ){ $m_10 = $m_10 * -1; }
			if( $m_11 < 0 ){ $m_11 = $m_11 * -1; }

			if( $i == 0 || $i == 3 ){ // 포인트거나 쿠폰이면
				$sub_title = "지급";
				
				$a_m_1 = $a_m_1 + $m_1;
				$a_m_2 = $a_m_2 + $m_2;
				$a_m_3 = $a_m_3 + $m_3;
				$a_m_4 = $a_m_4 + $m_4;
				$a_m_5 = $a_m_5 + $m_5;
				$a_m_6 = $a_m_6 + $m_6;
				$a_m_7 = $a_m_7 + $m_7;
				$a_m_8 = $a_m_8 + $m_8;
				$a_m_9 = $a_m_9 + $m_9;
				$a_m_10 = $a_m_10 + $m_10;
				$a_m_11 = $a_m_11 + $m_11;
				$a_m_12 = $a_m_12 + $m_12;

			}else if( $i == 1 || $i == 4 ){
				$sub_title = "사용";	
				
				$b_m_1 = $b_m_1 + $m_1;
				$b_m_2 = $b_m_2 + $m_2;
				$b_m_3 = $b_m_3 + $m_3;
				$b_m_4 = $b_m_4 + $m_4;
				$b_m_5 = $b_m_5 + $m_5;
				$b_m_6 = $b_m_6 + $m_6;
				$b_m_7 = $b_m_7 + $m_7;
				$b_m_8 = $b_m_8 + $m_8;
				$b_m_9 = $b_m_9 + $m_9;
				$b_m_10 = $b_m_10 + $m_10;
				$b_m_11 = $b_m_11 + $m_11;
				$b_m_12 = $b_m_12 + $m_12;

			}else if( $i == 2 || $i == 5 ){
				$sub_title = "소멸";

				$c_m_1 = $c_m_1 + $m_1;
				$c_m_2 = $c_m_2 + $m_2;
				$c_m_3 = $c_m_3 + $m_3;
				$c_m_4 = $c_m_4 + $m_4;
				$c_m_5 = $c_m_5 + $m_5;
				$c_m_6 = $c_m_6 + $m_6;
				$c_m_7 = $c_m_7 + $m_7;
				$c_m_8 = $c_m_8 + $m_8;
				$c_m_9 = $c_m_9 + $m_9;
				$c_m_10 = $c_m_10 + $m_10;
				$c_m_11 = $c_m_11 + $m_11;
				$c_m_12 = $c_m_12 + $m_12;
			}
			
	?>
	
    <tr>
		<? if( !empty($title) ){ ?>
        <td rowspan="3"><?=$title?></td>
		<? } ?>
        <td><?=$sub_title?></td>
        <td><?=number_format($m_1)?></td>
        <td><?=number_format($m_2)?></td>
		<td><?=number_format($m_3)?></td>
		<td><?=number_format($m_4)?></td>
		<td><?=number_format($m_5)?></td>
		<td><?=number_format($m_6)?></td>
		<td><?=number_format($m_7)?></td>
		<td><?=number_format($m_8)?></td>
		<td><?=number_format($m_9)?></td>
		<td><?=number_format($m_10)?></td>
		<td><?=number_format($m_11)?></td>
		<td><?=number_format($m_12)?></td>
		<td><?=number_format($m_1+$m_2+$m_3+$m_4+$m_5+$m_6+$m_7+$m_8+$m_9+$m_10+$m_11+$m_12)?></td>
	</tr>
    <?
		}
	?>


    </tbody>
    </table>
	
	<br>
	
	

</div>


<div class=" tbl_wrap1">
<div id="preloader">
	 <img src="/adm/jqGrid/waiting.gif" align="absmiddle"> Loading...		
</div>
<table id="grid-table"></table>
<div id="grid-pager" ></div>


</div>   
</form>
<?php

?>

<?php //$qstr ='comp_code='.$_GET['comp_code'].'&'.$qstr;
echo get_paging(G5_IS_MOBILE ? $config['cf_mobile_pages'] : $config['cf_write_pages'], $page, $total_page, "{$_SERVER['SCRIPT_NAME']}?$qstr&amp;page="); ?>

<script>

var selectedMonth=$("#mon").val();
var selectedyear=$("#list_year").val();

</script>

<script src="/adm/jqGrid/js/i18n/grid.locale-ko.js"   type="text/javascript"></script>  
<script src="/adm/jqGrid/js/jquery.jqGrid.min.js"    type="text/javascript"></script> 
<script type="text/javascript"> 
	var grid_selector = "#grid-table";
	var pager_selector = "#grid-pager";

	$(document).ready(function () {
		
		$('#mon').on('change',function() {
			selectedMonth=$("#mon").val();
		});
		
		$('#list_year').on('change',function() {
			selectedyear=$("#list_year").val();
		});
		
		var parent_column = $(grid_selector).closest('[class*="col-"]');
		//resize to fit page size
		$(window).on('resize.jqGrid', function () {
			$(grid_selector).jqGrid( 'setGridWidth', parent_column.width() );
		})
			
		jQuery(grid_selector).jqGrid({
			datatype :  fnSearch,
			colNames: ['쿠폰만료일','ID','성명','쿠폰번호','쿠폰이름','쿠폰금액'],	
		colModel: [
		           { name: 'cp_end',      index: 'cp_end',      width: 80, align: "center", sorttype: "text" }, 
				   { name: 'mb_id',      index: 'mb_id',      width: 55, align: "center" },    
				   { name: 'mb_name' ,      index: 'mb_name',      width: 80, align: "center", sorttype: "text" },    
                   { name: 'cp_id',      index: 'cp_id',      width: 80, align: "center", sorttype: "text" },  	
                   { name: 'cp_subject',      index: 'cp_subject',      width: 80, align: "center", sorttype: "text" },				   
				   { name: 'cp_price',      index: 'cp_price',      width: 75, align: "center", sorttype: "text" },    				     				   
				  ],
			viewrecords : true,
			rowNum:1000,
			rowList:[1000,10000],
			pager : pager_selector,
			altRows: true,
			sortname: 'od_id',
			sortorder: "desc",
			height:300,
			multiboxonly: true,			
			autowidth: true,
            shrinkToFit: true
		});
		
	});
	
	function formSubmit(type) {
		if(type==1) {
			$('#mode').val('read');
			jQuery(grid_selector).trigger("reloadGrid");
		} else {


			var form = "<form action='../shop_order/cal_1.php' method='post'>"; 
						   
			
			form += "<input type='hidden' name='mode' value='excel' />"; 
			form += "<input type='hidden' name='cp_end' value='"+$('#cp_end').val()+"' />";
			form += "<input type='hidden' name='mb_id' value='"+$('#mb_id').val()+"' />"; 
			form += "<input type='hidden' name='mb_name' value='"+$('#mb_name').val()+"' />"; 			
			form += "<input type='hidden' name='cp_id' value='"+$('#cp_id').val()+"' />";
			form += "<input type='hidden' name='cp_subject' value='"+$('#cp_subject').val()+"' />";
			form += "<input type='hidden' name='cp_price' value='"+$('#cp_price').val()+"' />"; 
			
			form += "</form>"; 
			jQuery(form).appendTo("body").submit().remove(); 


		}
	}
		

	
	function fnSearch(pdata) {
		$("#preloader").show();
		var jqGridParam = '';
		for (var i in pdata) {
			jqGridParam += '&'+i+'='+pdata[i];
		}

		jqGridParam += '&searchMonth='+selectedMonth;
		jqGridParam += '&searchyear='+selectedyear;
		
		console.log(jqGridParam);
		
		$.ajax({
			url:'../shop_order/cal_1.php',
			type:'POST',
			data:$('#forderlist').serialize()+jqGridParam,
			success: function(data) {

				var thegrid = jQuery(grid_selector)[0];
				thegrid.addJSONData(eval("("+data+")"));
			},

			complete: function(jsondata,stat) {
				$("#preloader").fadeOut("fast");
				//var thegrid = jQuery(grid_selector)[0];
				//updatePagerIcons(thegrid);			
			}
		});
	}
	function updatePagerIcons(table) {
		var replacement = 
		{
			'ui-icon-seek-first' : 'fa fa-angle-double-left bigger-140',
			'ui-icon-seek-prev' : 'fa fa-angle-left bigger-140',
			'ui-icon-seek-next' : 'fa fa-angle-right bigger-140',
			'ui-icon-seek-end' : 'fa fa-angle-double-right bigger-140'
		};
		$('.ui-pg-table:not(.navtable) > tbody > tr > .ui-pg-button > .ui-icon').each(function(){
			var icon = $(this);
			var $class = $.trim(icon.attr('class').replace('ui-icon', ''));
			
			if($class in replacement) icon.attr('class', 'ui-icon '+replacement[$class]);
		})
	}

</script>


<!-- (s)hidden Frame -->
<iframe name="iFrm" style="display: none;" ></iframe>


<?php
include_once (G5_ADMIN_PATH.'/admin.tail.php');
?>

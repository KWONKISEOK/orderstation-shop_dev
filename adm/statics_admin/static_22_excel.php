<?php
include_once('./_common.php');

// POST
$ptype = $_POST['ptype'];
$list_year = $_POST['list_year'];
$list_month = $_POST['list_month'];
$date_gubun = $_POST['date_gubun'];

// 년도 예외처리
$now = date("Ymd");
if ($list_year == "") {
    $list_year = year($now);
}

// 월 예외처리 및 조회기간 설정
if ($list_month < 10) {
    $list_month = '0'.$list_month;
}
if ($list_month != 0) {
    $start_date = $list_year."-".$list_month;
    $end_date = $list_year."-".$list_month;
} else {
    $start_date = $list_year."-01";
    $end_date = $list_year."-12";
}

// 필드
$field = "
            (case when h.mb_referee='5' then '1' when h.mb_type = '0' and a.pharm_custno in(select pharm_custno from tbl_member where mb_type='1' and mb_referee='5') then '2' else 0 end) as order_g
            ,a.od_id 
            ,e.od_id as first_od_id
            ,a.mb_id
            ,a.od_name
            ,a.pharm_name
            ,a.pharm_manager
            ,a.od_name
            ,a.od_tel
            ,a.od_hp
            ,a.od_zip1
            ,a.od_zip2
            ,a.od_addr1
            ,a.od_addr2
            ,a.od_addr3
            ,a.od_addr_jibeon					 
            ,a.od_settle_case
            ,a.pharm_custno
            ,a.od_cart_price
            ,a.od_gubun
            ,a.od_mobile
            ,a.od_memo
            ,a.od_receipt_point
            ,a.od_receipt_price
            ,a.od_coupon
            ,(select mb_referee from tbl_member where mb_id = a.mb_id) mb_referee
            ,(select mb_name from tbl_member where mb_id= a.pharm_manager ) pharm_manager_name
            ,a.pharm_manager_dept
            ,(select mb_id from tbl_member where pharm_custno= a.pharm_custno and mb_type=1 and pharm_custno != '' limit 1) pharm_mb_id
            ,(select mb_name from tbl_member where pharm_custno= a.pharm_custno  and mb_type=1 and pharm_custno != '' limit 1) pharm_mb_name
            ,b.it_id
            ,b.io_id
            ,b.od_num
            ,b.it_name
            ,(select it_model from tbl_shop_item where it_id = b.it_id) it_model
            ,b.od_send_cost
            ,b.od_option
            ,b.od_period_yn
            ,b.od_period_cnt
            
            ,b.od_sale_price
            ,b.od_dodome_price
            ,b.od_supply_price
            ,b.od_drug_price
            ,b.od_incen
            
            ,b.od_itm_sale_price
            ,b.od_itm_dodome_price
            ,b.od_itm_supply_price
            ,b.od_itm_drug_price
            ,b.od_itm_incen
            
            ,b.od_total_sale_price
            ,b.od_total_dodome_price
            ,b.od_total_supply_price
            ,b.od_total_drug_price
            ,b.od_total_incen					 
            ,c.od_b_name
            ,c.od_b_tel
            ,c.od_b_hp
            ,c.od_b_zip1
            ,c.od_b_zip2
            ,c.od_b_addr1
            ,c.od_b_addr2
            ,c.od_b_addr3
            ,c.od_b_addr_jibeon
            ,(SELECT max(od_seq) FROM tbl_shop_order_receiver WHERE od_id = b.od_id) AS od_max_seq
            ,c.od_seq
            ,c.od_date1
            ,c.od_date2
            ,c.od_date3
            ,c.od_date4
            ,c.od_qty
            ,c.od_period_date
            ,(SELECT min(od_period_date) FROM tbl_shop_order_receiver WHERE od_id = b.od_id) as erp_calculate_date
            ,c.od_status
            ,c.od_pay_yn
            ,c.od_delivery_company
            ,c.od_invoice
            ,c.od_trans_memo
            /*,d.comp_name*/
            ,(SELECT comp_name FROM tbl_member WHERE comp_code=d.comp_code LIMIT 1) as comp_name
            ,e.mb_type
            ,(select ca_id from tbl_shop_item where it_id = b.it_id) ca_id
            ,case when h.mb_type = 1 then h.pharm_div else (select pharm_div from tbl_member where mb_id = h.mb_recommend limit 1 ) end as pharm_div
";

// 조회구분 값에 따른 테이블 설정
if ($date_gubun == "period_date") {
    $_from_table = "(SELECT od_id FROM tbl_shop_order_receiver WHERE od_period_date between '$start_date-01 00:00:00' and '$end_date-31 23:59:59' GROUP BY od_id) AS X INNER JOIN tbl_shop_order a on X.od_id=a.od_id";
} else {
    $_from_table = "tbl_shop_order a";
}

$sql  = " 
        SELECT 
          {$field}	
        FROM {$_from_table} 
          INNER JOIN tbl_shop_order_detail b on a.od_id = b.od_id 
          INNER JOIN tbl_shop_order_receiver c on b.od_id = c.od_id and b.od_num = c.od_num
          /*left join tbl_member d on b.comp_code = d.comp_code*/
          INNER JOIN tbl_shop_item d ON d.it_id = b.it_id
          LEFT JOIN (
            select b.mb_id , min(a.od_id) AS od_id , max(b.mb_type) as mb_type
            from tbl_shop_order a inner join tbl_member b ON a.mb_id = b.mb_id AND a.od_status != '취소'
            WHERE od_receipt_time != '0000-00-00 00:00:00' and mb_datetime <= od_receipt_time 
            GROUP BY b.mb_id							  
          ) e ON a.mb_id = e.mb_id AND a.od_id = e.od_id 
          LEFT JOIN tbl_member h ON a.mb_id = h.mb_id
        WHERE 1=1 ";
$sql .= " and c.od_status != '취소'  and instr( c.od_status , '반품' ) <= 0 and b.service_type is null
					 and not ( a.od_settle_case = '가상계좌' and c.od_pay_yn = 'N' ) and b.it_id not in('S00603','S00604')";
//$sql .= " and a.od_gubun=2 and b.od_period_yn='Y'"; // 약국의 정기주문만 조회하는 경우 사용
$sql .= " and b.od_period_yn='Y'";

if ($date_gubun != "period_date") {
    $sql .= " and  c.".$date_gubun." between '$start_date-01 00:00:00' and '$end_date-31 23:59:59' ";
}

switch($ptype){
    case 'excel':
        $ExcelTitle = urlencode( "ERP_정기주문_매출현황_" . date( "YmdHis" ) );
        header( "Content-type: application/vnd.ms-excel" );
        header( "Content-type: application/vnd.ms-excel; charset=utf-8" );
        header( "Content-Disposition: attachment; filename = " . $ExcelTitle . ".xls" );
        header( "Content-Description: PHP4 Generated Data" );
        print( "<meta http-equiv=\"Content-Type\" content=\"application/vnd.ms-excel; charset=utf-8\">" );

        ?>
        <table border="1">
            <!--
            <tr bgcolor="F7F7F7" height="30">
                <th colspan="5">주문정보</th>
                <th colspan="6">약국정보</th>
                <th colspan="6">주문자 정보</th>
                <th colspan="6">받는사람 정보</th>
                <th colspan="12">주문상품</th>
                <th rowspan="2">처리상태</th>
                <th colspan="2">정기배송</th>
                <th colspan="4">ERP 정산</th>
                <th colspan="3">결제정보</th>
                <th colspan="3">배송정보</th>
            </tr>
            -->
            <tr bgcolor="F7F7F7">
                <th>주문번호</th>
                <!--
                <th>주문구분</th>
                <th>주문형태</th>
                <th>주문종류</th>
                -->
                <th>주문일시</th>
                <!--
                <th>담당자명</th>
                <th>회사</th>
                <th>약국명</th>
                <th>약국코드</th>
                <th>아이디</th>
                <th>약사명</th>
                -->
                <th>수령자명</th>
                <!--
                <th>전화번호</th>
                <th>핸드폰</th>
                <th>우편번호</th>
                <th>주소</th>
                <th>배송주의사항</th>
                <th>선물메세지</th>
                -->
                <th>업체명</th>
                <th>상품명</th>
                <th>규격</th>
                <th>옵션명</th>
                <th>상품코드</th>
                <!--<th>소비자가</th>-->
                <!--<th>도도매가</th>-->
                <th>공급가</th>
                <!--
                <th>약국판매가</th>
                <th>수수료</th>
                <th>옵션(소비자가)</th>
                <th>옵션(도도매가)</th>
                <th>옵션(공급가)</th>
                <th>옵션(약국판매가)</th>
                <th>옵션(수수료)</th>
                -->
                <th>수량</th>
                <th>배송비</th>
                <!--
                <th>소비자합</th>
                <th>도도매합계</th>
                <th>약국판매가합</th>
                <th>수수료합</th>
                -->
                <th>공급가합</th>
                <th>합계</th>
                <th>처리상태</th>
                <th>정기주문 배송 일자</th>
                <th>정기주문 배송 회차</th>
                <!--
                <th>포인트</th>
                <th>쿠폰</th>
                <th>결제금액</th>
                -->
                <th>ERP 정산 월</th>
                <th>ERP 정산 금액</th>
                <th>OS 정산 월</th>
                <th>OS 정산 금액</th>
                <th>OS 미정산 금액</th>
                <th>배송회사</th>
                <th>송장번호</th>
                <th>배송일시</th>
                <th>주문자명</th>
                <th>주문자 아이디</th>
                <!--<th>전화번호</th>-->
                <th>주문자 핸드폰</th>
                <th>주문자 우편번호</th>
                <th>주문자 주소</th>
                <th>주문자 고객구분</th>
            </tr>
        <?php
        //debug2($sql);
        $res = sql_query($sql);
        if(!$res) exit('Cannot run query.');

        $i = 0;
        $temp_od_id = '';
        $od_row_flag = false;
        while($row = sql_fetch_array($res)) {
            if( $row["order_g"] == "0" ){
                $order_g = "일반주문";
            }else{
                $order_g="얼라이언스 주문";
            }

            if( $row["od_mobile"] == "1" ){
                $device_gubun = "앱주문";
            }else{
                $device_gubun="웹주문";
            }

            $od_gubun = '';
            switch($row['od_gubun']) {
                case '1':
                    $od_gubun = '고객주문';
                    break;
                case '2':
                    $od_gubun = '약국주문';
                    break;
                case '3':
                    $od_gubun = '일반주문';
                    break;
            }
            $pharm_manager_dept = '';

            if( $row["pharm_div"] == "1" ){
                $pharm_manager_dept = "태전약품";
            }else if( $row["pharm_div"] == "2" ){
                $pharm_manager_dept = "티제이팜";
            }
            $mb_referee_str = '';
            switch ( $row[mb_referee] ) {
                case "0" :
                    $mb_referee_str = "일반회원";
                    break;
                case "1" :
                    $mb_referee_str = "OS추천회원";
                    break;
                case "2" :
                    $mb_referee_str = "모니터링회원";
                    break;
                case "3" :
                    $mb_referee_str = "OS서팩2회원";
                    break;
                case "4" :
                    $mb_referee_str = "OS직원";
                    break;
                case "5" :
                    $mb_referee_str = "얼라이언스약국";
                    break;
            }

            //영업사원주문시 표현을 OS직원으로 표현해주기
            if( $row[mb_type] == 5 ){
                $mb_referee_str = "OS직원";
            }
            $it_code = $row["it_id"];
            if($row["io_id"] != '') $it_code = $row["io_id"];

            $od_seq = 0;

            if ($row["od_period_yn"] == 'Y') {
                $od_seq = $row["od_seq"];
                $od_total_sale_price = $row["od_total_sale_price"] / $row["od_period_cnt"];
                $od_total_dodome_price = $row["od_total_dodome_price"] / $row["od_period_cnt"];
                $od_total_drug_price = $row["od_total_drug_price"] / $row["od_period_cnt"];
                $od_total_incen = $row["od_total_incen"] / $row["od_period_cnt"];
                $od_total_supply_price = $row["od_total_supply_price"] / $row["od_period_cnt"];
            } else {
                $od_total_sale_price = $row["od_total_sale_price"];
                $od_total_dodome_price = $row["od_total_dodome_price"];
                $od_total_drug_price = $row["od_total_drug_price"];
                $od_total_incen = $row["od_total_incen"];
                $od_total_supply_price = $row["od_total_supply_price"];
            }

            $od_payment = 0;
            if($od_gubun == 3) {
                $od_payment = $row["od_receipt_price"];
            }

            if ($i == 0) {
                $temp_od_id = $row['od_id'];
                $line_bgcolor = " bgcolor='#fbf7f0' ";
            }

            if ($od_row_flag == false && $temp_od_id != $row['od_id']) {
                $line_bgcolor = "";
                $od_row_flag = true;
            } else if ($od_row_flag == true && $temp_od_id != $row['od_id']) {
                $line_bgcolor = " bgcolor='#fbf7f0' ";
                $od_row_flag = false;
            }
            $temp_od_id = $row['od_id'];

            if ($row['od_seq'] == 1) {
                $rowspan = " rowspan='".$row['od_max_seq']."' ";
            } else {
                $rowspan = "";
            }

            $os_calculate_price = 0;
            $os_no_calculate_price = 0;
            $os_no_calculate_chk = "";
            if ($row["od_date3"] != "") {
                $os_calculate_price = $od_total_supply_price + $row["od_send_cost"];
            } else {
                $os_no_calculate_price = $od_total_supply_price + $row["od_send_cost"];
                $os_no_calculate_chk = " color: red;";
            }

            echo "<tr ".$line_bgcolor.">";
            echo "<td style=\"mso-number-format:'\@';".$os_no_calculate_chk."\">" . $row["od_id"] . " </td>";
//            echo "<td style=\"mso-number-format:'\@';\">" . $od_gubun . " </td>";
//            echo "<td style=\"mso-number-format:'\@';\">" . $order_g . " </td>";
//            echo "<td style=\"mso-number-format:'\@';\">" . $device_gubun . " </td>";
            echo "<td style=\"mso-number-format:'\@';\">" . $row["od_date1"] . " </td>";

            echo "<td style=\"mso-number-format:'\@';\">" . $row["od_b_name"] . " </td>";
//            echo "<td style=\"mso-number-format:'\@';\">" . $row["od_b_tel"] . " </td>";
//            echo "<td style=\"mso-number-format:'\@';\">" . $row["od_b_hp"] . " </td>";
//            echo "<td style=\"mso-number-format:'\@';\">" . $row["od_b_zip1"] .  $row["od_b_zip2"]." </td>";
//            echo "<td style=\"mso-number-format:'\@';\">" . print_address($row['od_b_addr1'], $row['od_b_addr2'], $row['od_b_addr3'], $row['od_b_addr_jibeon']) . " </td>";
//            echo "<td style=\"mso-number-format:'\@';\">" . $row["od_trans_memo"] . " </td>";
//            echo "<td style=\"mso-number-format:'\@';\">" . $row["od_memo"] . " </td>";


            echo "<td style=\"mso-number-format:'\@';\">" . $row["comp_name"] . " </td>";
            echo "<td style=\"mso-number-format:'\@';\">" . $row["it_name"] . " </td>";
            echo "<td style=\"mso-number-format:'\@';\">" . $row["it_model"] . " </td>";
            echo "<td style=\"mso-number-format:'\@';\">" . $row["od_option"] . " </td>";
            echo "<td style=\"mso-number-format:'\@';\">" . $it_code . " </td>";
//            echo "<td style=\"mso-number-format:'\@';\">" . $row["od_sale_price"] . " </td>";
//            echo "<td style=\"mso-number-format:'\@';\">" . $row["od_dodome_price"] . " </td>";
            echo "<td style=\"mso-number-format:'\@';\">" . $row["od_supply_price"] . " </td>";
//            echo "<td style=\"mso-number-format:'\@';\">" . $row["od_drug_price"] . " </td>";
//            echo "<td style=\"mso-number-format:'\@';\">" . $row["od_incen"] . " </td>";
//            echo "<td style=\"mso-number-format:'\@';\">" . $row["od_itm_sale_price"] . " </td>";
//            echo "<td style=\"mso-number-format:'\@';\">" . $row["od_itm_dodome_price"] . " </td>";
//            echo "<td style=\"mso-number-format:'\@';\">" . $row["od_itm_supply_price"] . " </td>";
//            echo "<td style=\"mso-number-format:'\@';\">" . $row["od_itm_drug_price"] . " </td>";
//            echo "<td style=\"mso-number-format:'\@';\">" . $row["od_itm_incen"] . " </td>";

            echo "<td style=\"mso-number-format:'\@';\">" . $row["od_qty"] . " </td>";
            echo "<td style=\"mso-number-format:'\@';\">" . $row["od_send_cost"] . " </td>";
//            echo "<td style=\"mso-number-format:'\@';\">" . $od_total_sale_price . " </td>";
//            echo "<td style=\"mso-number-format:'\@';\">" . $od_total_dodome_price . " </td>";
//            echo "<td style=\"mso-number-format:'\@';\">" . $od_total_drug_price . " </td>";
//            echo "<td style=\"mso-number-format:'\@';\">" . $od_total_incen . " </td>";
            echo "<td style=\"mso-number-format:'\@';\">" . $od_total_supply_price . " </td>";
            echo "<td style=\"mso-number-format:'\@';\">" . ($od_total_supply_price + $row["od_send_cost"]) . " </td>";

            echo "<td style=\"mso-number-format:'\@';\">" . $row["od_status"] . " </td>";

            echo "<td style=\"mso-number-format:'\@';\">" . $row["od_period_date"] . " </td>";
            echo "<td style=\"mso-number-format:'\@';\">" . $od_seq . " </td>";

//            echo "<td style=\"mso-number-format:'\@';\">" . $row["od_receipt_point"] . " </td>";
//            echo "<td style=\"mso-number-format:'\@';\">" . $row["od_coupon"] . " </td>";
//            echo "<td style=\"mso-number-format:'\@';\">" . $od_payment . " </td>";

            //if ($rowspan != "") {
            echo "<td style=\"mso-number-format:'\@';\">" . date('m', strtotime($row["erp_calculate_date"])) . "월 </td>";
            echo "<td style=\"mso-number-format:'\@';\">" . ($od_total_supply_price + $row["od_send_cost"]) . " </td>";
            //}
            echo "<td style=\"mso-number-format:'\@';\">" . date('m', strtotime($row["od_period_date"])) . "월 </td>";
            echo "<td style=\"mso-number-format:'\@';\">" . $os_calculate_price . " </td>";
            echo "<td style=\"mso-number-format:'\@';\">" . $os_no_calculate_price . " </td>";

            echo "<td style=\"mso-number-format:'\@';\">" . $row["od_delivery_company"] . " </td>";
            echo "<td style=\"mso-number-format:'\@';\">" . $row["od_invoice"] . " </td>";
            echo "<td style=\"mso-number-format:'\@';\">" . $row["od_date3"] . " </td>";

            ### 주문자 정보 START ###
            echo "<td style=\"mso-number-format:'\@';\">" . $row["od_name"] . " </td>";
            echo "<td style=\"mso-number-format:'\@';\">" . $row["mb_id"] . " </td>";
//            echo "<td style=\"mso-number-format:'\@';\">" . $row["od_tel"] . " </td>";
            echo "<td style=\"mso-number-format:'\@';\">" . $row["od_hp"] . " </td>";
            echo "<td style=\"mso-number-format:'\@';\">" . $row["od_zip1"] .  $row["od_zip2"]." </td>";
            echo "<td style=\"mso-number-format:'\@';\">" . print_address($row['od_addr1'], $row['od_addr2'], $row['od_addr3'], $row['od_addr_jibeon']) . " </td>";
            echo "<td style=\"mso-number-format:'\@';\">" . $mb_referee_str . " </td>";
            ### 주문자 정보 END ###

            echo "</tr>";

            $i++;
        }  //와일문 닫힘
        break;


}




?>
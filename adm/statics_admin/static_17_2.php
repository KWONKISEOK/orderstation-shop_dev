 <?php
$sub_menu = "500260";
include_once('./_common.php');

auth_check($auth[$sub_menu], 'r');

if( empty($start_date) && empty($end_date) ){
	$getWeeekDate = GetWeekInfo("1");
	$start_date = explode ("|",  $getWeeekDate )[0];
	$end_date = explode ("|",  $getWeeekDate )[1];
}

$GetDateDiff = GetDateDiffInfo($start_date,$end_date);

?>
	

	<table>
    <caption> 주차별 매출현황 </caption>
    <thead>
    <tr>
        <th scope="col" rowspan="2">카테고리</th>
        <th scope="col" colspan="4">이번주</th>
        <th scope="col" rowspan="2">지난주 매출</th>
        <th scope="col" rowspan="2">매출 건수</th>
        <th scope="col" rowspan="2">전년 동기간 매출</th>
        <th scope="col" rowspan="2">매출 건수</th>
    </tr>
	<tr>
		<th>매출액</th>
		<th>매출 건수</th>
		<th>지난주대비</th>
		<th>전년대비</th>
	</tr>
    </thead>
    <tbody>
	<?
	$cal_price_1 = 0;
	$cal_price_2 = 0;

	$result = sql_query("CALL SP_P_17_2('".$start_date."','".$end_date."',".$GetDateDiff.")");
	for ($i=0; $row=sql_fetch_array($result); $i++) {

		$cal_price_1 = ( ( (float)$row["total_price_1"] - (float)$row["total_price_2"] ) * 100 ) / (float)$row["total_price_2"];
		$cal_price_2 = ( ( (float)$row["total_price_1"] - (float)$row["total_price_3"] ) * 100 ) / (float)$row["total_price_3"];
	?>
	<tr>
		<td><?=$row["ca_id"]?></td>
		<td><?=number_format($row["total_price_1"])?></td>
		<td><?=$row["total_cnt_1"]?>건</td>
		<td><?=round($cal_price_1)?>%</td>
		<td><?=round($cal_price_2)?>%</td>
		<td><?=number_format($row["total_price_2"])?></td>
		<td><?=$row["total_cnt_2"]?>건</td>
		<td><?=number_format($row["total_price_3"])?></td>
		<td><?=$row["total_cnt_3"]?>건</td>
	</tr>
    <?
	}
	?>
    </tbody>
    <tbody>
	</table>
<?php
$sub_menu = "901800";
include_once('./_common.php');

auth_check($auth[$sub_menu], 'r');

$g5['title'] = '회원별 매출현황';
include_once(G5_ADMIN_PATH.'/admin.head.php');
include_once(G5_PLUGIN_PATH.'/jquery-ui/datepicker.php');

if( empty($start_date) && empty($end_date) ){
	$getWeeekDate = GetWeekInfo("1");
	$start_date = explode ("|",  $getWeeekDate )[0];
	$end_date = explode ("|",  $getWeeekDate )[1];
}

if( $start_date > $end_date ){
	echo "<script>";
	echo "alert('시작날짜가 종료날짜 보다 큽니다.날짜 선택을 다시해주세요.');";
	echo "location.href='./static_18.php';";
	echo "</script>";
}

$GetDateDiff = GetDateDiffInfo($start_date,$end_date);

$Total_sum_1 = 0;
$Total_sum_2 = 0;
$Total_sum_3 = 0;
$InputRows=array();

$result = sql_query("CALL SP_P_18('".$start_date."','".$end_date."',".$GetDateDiff.")");

/* 고객주문 + 약국주문 + 일반주문 매출액합계 이번주,지난주,전년동기간 구해오기 */
for ($i=0; $row=sql_fetch_array($result); $i++) {

	$Total_sum_1 += $row["total_price_1"]; //이번주 매출액 합계
	$Total_sum_2 += $row["total_price_2"]; //지난주 매출액 합계 
	$Total_sum_3 += $row["total_price_3"]; //전년 매출액 합계

	array_push($InputRows,$row);
}

?>

<form id="fsearch" name="fsearch" class="local_sch01 local_sch" action="?" method="get">

<div class="local_sch03  tbl_wrap" style="padding:10px;">
    <table>
    <tbody>
	<tr>
        <td>
			날짜선택 &nbsp;
            <input type="text" id="start_date"  name="start_date" value="<?php echo $start_date; ?>" class="frm_input" size="10" maxlength="10"> ~
			<input type="text" id="end_date"  name="end_date" value="<?php echo $end_date; ?>" class="frm_input" size="10" maxlength="10">	
			&nbsp;
			<input type="submit" value="검색" class="get_theme_confc btn btn_01">
        </td>
    </tr>
    </tbody>
    </table>
</div>

</form>


<div class="tbl_head01 tbl_wrap">

    <table>
    <caption> 회원별 매출현황 </caption>
    <thead>
    <tr>
        <th scope="col" rowspan="3">구분</th>
        <th scope="col" colspan="7">이번주</th>
        <th scope="col" colspan="3">지난주</th>
        <th scope="col" colspan="3">전년 동기간</th>
    </tr>
	<tr>
		<th scope="col" rowspan="2">매출액</th>
		<th scope="col" rowspan="2">매출 건수</th>
		<th scope="col" rowspan="2">구성비</th>
		<th scope="col" colspan="2">지난주대비</th>
		<th scope="col" colspan="2">전년 동기간 대비</th>
		<th scope="col" rowspan="2">매출액</th>
		<th scope="col" rowspan="2">매출 건수</th>
		<th scope="col" rowspan="2">구성비</th>
		<th scope="col" rowspan="2">매출액</th>
		<th scope="col" rowspan="2">매출 건수</th>
		<th scope="col" rowspan="2">구성비</th>
	</tr>
	<tr>
		<th>매출액</th>
		<th>구성비</th>
		<th>매출액</th>
		<th>구성비</th>
	</tr>
    </thead>
    <tbody>
	
	<?	
	foreach($InputRows as $key => $value){

	$g_m_p = $value["total_price_1"]; //이번주 매출액
	$g_c_p = $value["total_cnt_1"]; //이번주 건수
	$g_g_p = round( ( $g_m_p / $Total_sum_1 ) * 100 ); //이번주 구성비

	$j_m_p = $value["total_price_2"]; //지난주 매출액
	$j_c_p = $value["total_cnt_2"]; //지난주 건수
	$j_g_p = round( ( $j_m_p / $Total_sum_2 ) * 100 ); //지난주 구성비
	
	$jn_m_p = $value["total_price_3"]; //전년 매출액
	$jn_c_p = $value["total_cnt_3"]; //전년 건수
	$jn_g_p = round( ( $jn_m_p / $Total_sum_3 ) * 100 ); //전년 구성비
	
	
	if($value["od_gubun"]=='일반주문'||$value["od_gubun"]=='고객주문') {
		
		$sum_g_m_p=$sum_g_m_p+$g_m_p;
		$sum_g_c_p=$sum_g_c_p+$g_c_p;
		$sum_g_g_p=$sum_g_g_p+$g_g_p;
		$sum_j_m_p=$sum_j_m_p+$j_m_p;
		$sum_j_c_p=$sum_j_c_p+$j_c_p;
		$sum_j_g_p=$sum_j_g_p+$j_g_p;
		$sum_jn_m_p=$sum_jn_m_p+$jn_m_p;
		$sum_jn_c_p=$sum_jn_c_p+$jn_c_p;
		$sum_jn_g_p=$sum_jn_g_p+$jn_g_p;
		
	if($value["od_gubun"]=='일반주문') {
		?>
		<tr>
		<td>소비자주문</td>
		<td><?=number_format($sum_g_m_p)?></td>
		<td><?=$sum_g_c_p?>건</td>
		<td><?=$sum_g_g_p?>%</td>
		<td>
		<?
			//지난주대비 매출액
			echo round( ( ( $sum_g_m_p / $sum_j_m_p ) - 1 ) * 100 ) . "%";
		?>
		</td>
		<td>
		<?
			//지난주대비 구성비
			echo round( ( ( $sum_g_g_p / $sum_j_g_p ) - 1 ) * 100 ) . "%";
		?>		
		</td>
		<td>
		<?
			//전년대비 매출액
			echo round( ( ( $sum_g_m_p / $sum_jn_m_p ) - 1 ) * 100 ) . "%";
		?>		
		</td>
		<td>
		<?
			//전년대비 구성비
			echo round( ( ( $sum_g_g_p / $sum_jn_g_p ) - 1 ) * 100 ) . "%";
		?>			
		</td>
		<td><?=number_format($sum_j_m_p)?></td>
		<td><?=$sum_j_c_p?>건</td>
		<td><?=$sum_j_g_p?>%</td>
		<td><?=number_format($sum_jn_m_p)?></td>
		<td><?=$sum_jn_c_p?>건</td>
		<td><?=$sum_jn_g_p?>%</td>
		
	</tr>
	<?
	  }
	} else if($value["od_gubun"]=='약국주문'||$value["od_gubun"]=='') {
	?>
	<tr>
		<td><?=$value["od_gubun"]?></td>
		<td><?=number_format($g_m_p)?></td>
		<td><?=$g_c_p?>건</td>
		<td><?=$g_g_p?>%</td>
		<td>
		<?
			//지난주대비 매출액
			echo round( ( ( $g_m_p / $j_m_p ) - 1 ) * 100 ) . "%";
		?>
		</td>
		<td>
		<?
			//지난주대비 구성비
			echo round( ( ( $g_g_p / $j_g_p ) - 1 ) * 100 ) . "%";
		?>		
		</td>
		<td>
		<?
			//전년대비 매출액
			echo round( ( ( $g_m_p / $jn_m_p ) - 1 ) * 100 ) . "%";
		?>		
		</td>
		<td>
		<?
			//전년대비 구성비
			echo round( ( ( $g_g_p / $jn_g_p ) - 1 ) * 100 ) . "%";
		?>			
		</td>
		<td><?=number_format($j_m_p)?></td>
		<td><?=$j_c_p?>건</td>
		<td><?=$j_g_p?>%</td>
		<td><?=number_format($jn_m_p)?></td>
		<td><?=$jn_c_p?>건</td>
		<td><?=$jn_g_p?>%</td>
	</tr>
	<?
	 } 
	}
	?>
	
    </tbody>
    <tbody>
	</table>

</div>


<script>

$(function(){
    $("#start_date, #end_date").datepicker({ changeMonth: true, changeYear: true, dateFormat: "yy-mm-dd", showButtonPanel: true, yearRange: "c-99:c+99"});
});

</script>

<?php
include_once (G5_ADMIN_PATH.'/admin.tail.php');
?>

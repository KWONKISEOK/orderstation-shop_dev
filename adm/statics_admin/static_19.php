<?php
$sub_menu = "500280";
include_once('./_common.php');

auth_check($auth[$sub_menu], 'r');

$g5['title'] = '요일별 매출현황';
include_once(G5_ADMIN_PATH.'/admin.head.php');
include_once(G5_PLUGIN_PATH.'/jquery-ui/datepicker.php');

if( empty($start_date) && empty($end_date) ){
	$getWeeekDate = GetWeekInfo("1");
	$start_date = explode ("|",  $getWeeekDate )[0];
	$end_date = explode ("|",  $getWeeekDate )[1];
}

if( $start_date > $end_date ){
	echo "<script>";
	echo "alert('시작날짜가 종료날짜 보다 큽니다.날짜 선택을 다시해주세요.');";
	echo "location.href='./static_19.php';";
	echo "</script>";
}

$GetDateDiff = GetDateDiffInfo($start_date,$end_date);

$Total_cnt_0 = 0;
$Total_cnt_1 = 0;
$Total_cnt_2 = 0;
$Total_cnt_3 = 0;
$Total_sum_0 = 0;
$Total_sum_1 = 0;
$Total_sum_2 = 0;
$Total_sum_3 = 0;

$InputRows=array();

$result = sql_query("CALL SP_P_19('".$start_date."','".$end_date."',".$GetDateDiff.")");

/* 고객주문 + 약국주문 + 일반주문 매출액합계 이번주,지난주,전년동기간 구해오기 */
for ($i=0; $row=sql_fetch_array($result); $i++) {
	
	$Total_cnt_0 += round( ($row["total_cnt_1"]+$row["total_cnt_2"]+$row["total_cnt_3"]) / 3 ); //이번주+지난주+전년 주문건 평균
	$Total_cnt_1 += $row["total_cnt_1"]; //이번주 주문건 합계
	$Total_cnt_2 += $row["total_cnt_2"]; //지난주 주문건 합계 
	$Total_cnt_3 += $row["total_cnt_3"]; //전년 주문건 합계

	$Total_sum_0 += round( ( $row["total_price_1"]+$row["total_price_2"]+$row["total_price_3"] ) / 3 ); //이번주+지난주+전년 매출액 평균
	$Total_sum_1 += $row["total_price_1"]; //이번주 매출액 합계
	$Total_sum_2 += $row["total_price_2"]; //지난주 매출액 합계 
	$Total_sum_3 += $row["total_price_3"]; //전년 매출액 합계

	array_push($InputRows,$row);
}

?>

<form id="fsearch" name="fsearch" class="local_sch01 local_sch" action="?" method="get">

<div class="local_sch03  tbl_wrap" style="padding:10px;">
    <table>
    <tbody>
	<tr>
        <td>
			날짜선택 &nbsp;
            <input type="text" id="start_date"  name="start_date" value="<?php echo $start_date; ?>" class="frm_input" size="10" maxlength="10"> ~
			<input type="text" id="end_date"  name="end_date" value="<?php echo $end_date; ?>" class="frm_input" size="10" maxlength="10">	
			&nbsp;
			<input type="submit" value="검색" class="get_theme_confc btn btn_01">
			 <input type="button" value="엑셀 다운로드" class="get_theme_confc btn btn_02" style="cursor:pointer" onclick="javascript:ExcelDown();">
        </td>
    </tr>
    </tbody>
    </table>
</div>

</form>


<div class="tbl_head01 tbl_wrap">

    <table>
    <caption> 요일별 매출현황 </caption>
    <thead>
    <tr>
        <th scope="col" rowspan="3">요일</th>
        <th scope="col" colspan="8">주문건</th>
        <th scope="col" colspan="8">매출액</th>
    </tr>
	<tr>
		<th scope="col" colspan="4">주문건</th>
		<th scope="col" colspan="4">구성비</th>
		<th scope="col" colspan="4">매출액</th>
		<th scope="col" colspan="4">구성비</th>
	</tr>
	<tr>
		<th>평균</th>
		<th>이번주</th>
		<th>지난주</th>
		<th>전년동기간</th>
		<th>평균</th>
		<th>이번주</th>
		<th>지난주</th>
		<th>전년동기간</th>
		<th>평균</th>
		<th>이번주</th>
		<th>지난주</th>
		<th>전년동기간</th>
		<th>평균</th>
		<th>이번주</th>
		<th>지난주</th>
		<th>전년동기간</th>
	</tr>
    </thead>
    <tbody>
	<?
	foreach($InputRows as $key => $value){

		$avg_cnt = round( ($value["total_cnt_1"]+$value["total_cnt_2"]+$value["total_cnt_3"]) / 3 );
		$avg_price = round( ( $value["total_price_1"]+$value["total_price_2"]+$value["total_price_3"] ) / 3 )
	?>
	<tr>
		<td><?=$value["day_of_week"]?></td>
		<td><?=$avg_cnt?></td>
		<td><?=$value["total_cnt_1"]?></td>
		<td><?=$value["total_cnt_2"]?></td>
		<td><?=$value["total_cnt_3"]?></td>
		<td><?=round( ( $avg_cnt / $Total_cnt_0 ) * 100)?>%</td>
		<td><?=round( ( $value["total_cnt_1"] / $Total_cnt_1 ) * 100)?>%</td>
		<td><?=round( ( $value["total_cnt_2"] / $Total_cnt_2 ) * 100)?>%</td>
		<td><?=round( ( $value["total_cnt_3"] / $Total_cnt_3 ) * 100)?>%</td>
		<td><?=number_format( $avg_price )?></td>
		<td><?=number_format($value["total_price_1"])?></td>
		<td><?=number_format($value["total_price_2"])?></td>
		<td><?=number_format($value["total_price_3"])?></td>
		<td><?=round( ( $avg_price / $Total_sum_0 ) * 100 )?>%</td>
		<td><?=round( ( $value["total_price_1"] / $Total_sum_1 ) * 100)?>%</td>
		<td><?=round( ( $value["total_price_2"] / $Total_sum_2 ) * 100)?>%</td>
		<td><?=round( ( $value["total_price_3"] / $Total_sum_3 ) * 100)?>%</td>
	</tr>
	<?
	}
	?>
	<tr>
		<td><strong>Total</strong></td>
		<td><?=$Total_cnt_0?></td>
		<td><?=$Total_cnt_1?></td>
		<td><?=$Total_cnt_2?></td>
		<td><?=$Total_cnt_3?></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td><?=number_format( $Total_sum_0 )?></td>
		<td><?=number_format( $Total_sum_1 )?></td>
		<td><?=number_format( $Total_sum_2 )?></td>
		<td><?=number_format( $Total_sum_3 )?></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
    </tbody>
    <tbody>
	</table>

</div>


<script>

$(function(){
    $("#start_date, #end_date").datepicker({ changeMonth: true, changeYear: true, dateFormat: "yy-mm-dd", showButtonPanel: true, yearRange: "c-99:c+99"});
});

function ExcelDown(){
	
	var form = "<form action='static_19_excel.php' method='post'>"; 
		form += "<input type='hidden' name='ptype' value='excel' />";
		form += "<input type='hidden' name='start_date' value='"+$('#start_date').val()+"' />"; 	
		form += "<input type='hidden' name='end_date' value='"+$('#end_date').val()+"' />"; 		 
		form += "</form>"; 
		jQuery(form).appendTo("body").submit().remove(); 

}

</script>

<?php
include_once (G5_ADMIN_PATH.'/admin.tail.php');
?>

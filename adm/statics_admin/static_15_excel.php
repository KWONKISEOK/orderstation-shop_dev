<?php
$sub_menu = "500240";
include_once('./_common.php');

auth_check($auth[$sub_menu], 'r');

$now = date( "Ymd" );
if ( $list_year == "" ) {
	$list_year = year( $now );
}

$InputRows_1=array();
$InputRows_2=array();
$InputRows_3=array();
$InputRows_4=array();
$InputRows_5=array();
$InputRows_6=array();
$InputRows_7=array();
$InputRows_8=array();
$InputRows_9=array();
$InputRows_10=array();
$InputRows_11=array();
$InputRows_12=array();

$result = sql_query("CALL SP_P_15('".$list_year."')");

for ($i=0; $row=sql_fetch_array($result); $i++) {
	
	if( $row["view_month"] == "1" ){ array_push($InputRows_1,$row); }
	if( $row["view_month"] == "2" ){ array_push($InputRows_2,$row); }
	if( $row["view_month"] == "3" ){ array_push($InputRows_3,$row); }
	if( $row["view_month"] == "4" ){ array_push($InputRows_4,$row); }
	if( $row["view_month"] == "5" ){ array_push($InputRows_5,$row); }
	if( $row["view_month"] == "6" ){ array_push($InputRows_6,$row); }
	if( $row["view_month"] == "7" ){ array_push($InputRows_7,$row); }
	if( $row["view_month"] == "8" ){ array_push($InputRows_8,$row); }
	if( $row["view_month"] == "9" ){ array_push($InputRows_9,$row); }
	if( $row["view_month"] == "10" ){ array_push($InputRows_10,$row); }
	if( $row["view_month"] == "11" ){ array_push($InputRows_11,$row); }
	if( $row["view_month"] == "12" ){ array_push($InputRows_12,$row); }

}

$sum_price_1 = 0;
$sum_price_2 = 0;
$sum_price_3 = 0;

for($i=1; $i<=12; $i++){

	$sum_price_1 += ${"InputRows_".$i}[0]["cnt_1"];
	$sum_price_2 += ${"InputRows_".$i}[0]["cnt_2"];
	$sum_price_3 += ${"InputRows_".$i}[0]["cnt_3"];

}

$ExcelTitle = urlencode( "얼라이언스_회원수현황_" . date( "YmdHis" ) );

header( "Content-type: application/vnd.ms-excel" );
header( "Content-type: application/vnd.ms-excel; charset=utf-8" );
header( "Content-Disposition: attachment; filename = " . $ExcelTitle . ".xls" );
header( "Content-Description: PHP4 Generated Data" );
print( "<meta http-equiv=\"Content-Type\" content=\"application/vnd.ms-excel; charset=utf-8\">" ); 	

?>

<style>
	td {
	   mso-number-format:"\@";
	}
</style>

<table border="1">
<thead>
<tr bgcolor="F7F7F7" height="30">
	<th scope="col" rowspan="2">구분</th>
	<th scope="col" colspan="13"><?=$list_year?>년</th>
</tr>
<tr bgcolor="F7F7F7" height="30">
	<th>01월</th>
	<th>02월</th>
	<th>03월</th>
	<th>04월</th>
	<th>05월</th>
	<th>06월</th>
	<th>07월</th>
	<th>08월</th>
	<th>09월</th>
	<th>10월</th>
	<th>11월</th>
	<th>12월</th>
	<th>합계</th>
</tr>
</thead>
<tbody>

<tr>
	<td>전체OS약국 앱회원수</td>
	<td><?=number_format($InputRows_1[0]["cnt_1"])?></td>
	<td><?=number_format($InputRows_2[0]["cnt_1"])?></td>
	<td><?=number_format($InputRows_3[0]["cnt_1"])?></td>
	<td><?=number_format($InputRows_4[0]["cnt_1"])?></td>
	<td><?=number_format($InputRows_5[0]["cnt_1"])?></td>
	<td><?=number_format($InputRows_6[0]["cnt_1"])?></td>
	<td><?=number_format($InputRows_7[0]["cnt_1"])?></td>
	<td><?=number_format($InputRows_8[0]["cnt_1"])?></td>
	<td><?=number_format($InputRows_9[0]["cnt_1"])?></td>
	<td><?=number_format($InputRows_10[0]["cnt_1"])?></td>
	<td><?=number_format($InputRows_11[0]["cnt_1"])?></td>
	<td><?=number_format($InputRows_12[0]["cnt_1"])?></td>
	<td><?=number_format($sum_price_1)?></td>
</tr>
<tr>
	<td>얼라이언스약국 앱회원수</td>
	<td><?=number_format($InputRows_1[0]["cnt_2"])?></td>
	<td><?=number_format($InputRows_2[0]["cnt_2"])?></td>
	<td><?=number_format($InputRows_3[0]["cnt_2"])?></td>
	<td><?=number_format($InputRows_4[0]["cnt_2"])?></td>
	<td><?=number_format($InputRows_5[0]["cnt_2"])?></td>
	<td><?=number_format($InputRows_6[0]["cnt_2"])?></td>
	<td><?=number_format($InputRows_7[0]["cnt_2"])?></td>
	<td><?=number_format($InputRows_8[0]["cnt_2"])?></td>
	<td><?=number_format($InputRows_9[0]["cnt_2"])?></td>
	<td><?=number_format($InputRows_10[0]["cnt_2"])?></td>
	<td><?=number_format($InputRows_11[0]["cnt_2"])?></td>
	<td><?=number_format($InputRows_12[0]["cnt_2"])?></td>
	<td><?=number_format($sum_price_2)?></td>
</tr>
<tr>
	<td>얼라이언스 비율</td>
	<td><?=$InputRows_1[0]["avg_1"]?>%</td>
	<td><?=$InputRows_2[0]["avg_1"]?>%</td>
	<td><?=$InputRows_3[0]["avg_1"]?>%</td>
	<td><?=$InputRows_4[0]["avg_1"]?>%</td>
	<td><?=$InputRows_5[0]["avg_1"]?>%</td>
	<td><?=$InputRows_6[0]["avg_1"]?>%</td>
	<td><?=$InputRows_7[0]["avg_1"]?>%</td>
	<td><?=$InputRows_8[0]["avg_1"]?>%</td>
	<td><?=$InputRows_9[0]["avg_1"]?>%</td>
	<td><?=$InputRows_10[0]["avg_1"]?>%</td>
	<td><?=$InputRows_11[0]["avg_1"]?>%</td>
	<td><?=$InputRows_12[0]["avg_1"]?>%</td>
	<td><?=round( ($sum_price_2/$sum_price_1)*100 , 2 )?>%</td>
</tr>
<tr>
	<td>얼라이언스 앱회원 보유약국수</td>
	<td><?=number_format($InputRows_1[0]["cnt_3"])?></td>
	<td><?=number_format($InputRows_2[0]["cnt_3"])?></td>
	<td><?=number_format($InputRows_3[0]["cnt_3"])?></td>
	<td><?=number_format($InputRows_4[0]["cnt_3"])?></td>
	<td><?=number_format($InputRows_5[0]["cnt_3"])?></td>
	<td><?=number_format($InputRows_6[0]["cnt_3"])?></td>
	<td><?=number_format($InputRows_7[0]["cnt_3"])?></td>
	<td><?=number_format($InputRows_8[0]["cnt_3"])?></td>
	<td><?=number_format($InputRows_9[0]["cnt_3"])?></td>
	<td><?=number_format($InputRows_10[0]["cnt_3"])?></td>
	<td><?=number_format($InputRows_11[0]["cnt_3"])?></td>
	<td><?=number_format($InputRows_12[0]["cnt_3"])?></td>
	<td><?=number_format($sum_price_3)?></td>
</tr>

</tbody>
</table>

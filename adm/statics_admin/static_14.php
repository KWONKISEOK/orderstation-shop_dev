<?php
$sub_menu = "500230";
include_once('./_common.php');

auth_check($auth[$sub_menu], 'r');

$g5['title'] = '얼라이언스 기간별매출';
include_once(G5_ADMIN_PATH.'/admin.head.php');
include_once(G5_PLUGIN_PATH.'/jquery-ui/datepicker.php');

if( empty($start_date) && empty($end_date) ){
	$getWeeekDate = GetWeekInfo("1");
	$start_date = explode ("|",  $getWeeekDate )[0];
	$end_date = explode ("|",  $getWeeekDate )[1];
}

if( $start_date > $end_date ){
	echo "<script>";
	echo "alert('시작날짜가 종료날짜 보다 큽니다.날짜 선택을 다시해주세요.');";
	echo "location.href='./static_17.php';";
	echo "</script>";
}

?>

<form id="fsearch" name="fsearch" class="local_sch01 local_sch" action="?" method="get">

<div class="local_sch03  tbl_wrap" style="padding:10px;">
    <table>
    <tbody>
	<tr>
        <td>
			날짜선택 &nbsp;
            <input type="text" id="start_date"  name="start_date" value="<?php echo $start_date; ?>" class="frm_input" size="10" maxlength="10"> ~
			<input type="text" id="end_date"  name="end_date" value="<?php echo $end_date; ?>" class="frm_input" size="10" maxlength="10">	
			&nbsp;
			<input type="submit" value="검색" class="get_theme_confc btn btn_01">
			<select name="os_gubun" id="os_gubun" >
				<option value="1" <?php echo get_selected($os_gubun, '1'); ?>>OS전체</option>
				<option value="5" <?php echo get_selected($os_gubun, '5'); ?>>얼라이언스 약국</option>
			</select>
			 <input type="button" value="엑셀 다운로드" class="get_theme_confc btn btn_02" style="cursor:pointer" onclick="javascript:ExcelDown();">
        </td>
    </tr>
    </tbody>
    </table>
</div>

</form>

<div class="tbl_head01 tbl_wrap">

	<table>
    <caption> 주차별 매출현황 </caption>
    <thead>
    <tr>
        <th scope="col" rowspan="2" width="10%">구분</th>
        <th scope="col" colspan="3">OS매출액</th>
        <th scope="col" colspan="3">OS주문건수</th>
		<th scope="col" rowspan="2" width="10%">가동처수</th>
    </tr>
	<tr>
		<th>전체</th>
		<th>웹주문</th>
		<th>앱주문</th>
		<th>전체</th>
		<th>웹주문</th>
		<th>앱주문</th>
	</tr>
    </thead>
    <tbody>
	<?
	$cal_price_1 = 0;
	$cal_price_2 = 0;

	$result = sql_query("CALL SP_P_14('".$start_date."','".$end_date."')");
	$row=sql_fetch_array($result);
	
	?>
	<tr>
		<td>OS전체</td>
		<td><?=number_format($row["tot_price_1"]);?></td>
		<td><?=number_format($row["tot_web_price_1"]);?></td>
		<td><?=number_format($row["tot_app_price_1"]);?></td>
		<td><?=number_format($row["tot_cnt_1"]);?></td>
		<td><?=number_format($row["tot_web_cnt_1"]);?></td>
		<td><?=number_format($row["tot_app_cnt_1"]);?></td>
		<td><?=number_format($row["gadong_1"]);?></td>
	</tr>
	<tr>
		<td>얼라이언스 약국</td>
		<td><?=number_format($row["tot_price_2"]);?></td>
		<td><?=number_format($row["tot_web_price_2"]);?></td>
		<td><?=number_format($row["tot_app_price_2"]);?></td>
		<td><?=number_format($row["tot_cnt_2"]);?></td>
		<td><?=number_format($row["tot_web_cnt_2"]);?></td>
		<td><?=number_format($row["tot_app_cnt_2"]);?></td>
		<td><?=number_format($row["gadong_2"]);?></td>
	</tr>
	<tr>
		<td>얼라이언스 비율</td>
		<td><?=$row["price_avg"];?>%</td>
		<td><?=$row["web_price_avg"];?>%</td>
		<td><?=$row["app_price_avg"];?>%</td>
		<td><?=$row["cnt_avg"];?>%</td>
		<td><?=$row["web_cnt_avg"];?>%</td>
		<td><?=$row["app_cnt_avg"];?>%</td>
		<td><?=$row["gadong_cnt_avg"];?>%</td>
	</tr>
   
    </tbody>
    <tbody>
	</table>

</div>

<script>

$(function(){
    $("#start_date, #end_date").datepicker({ changeMonth: true, changeYear: true, dateFormat: "yy-mm-dd", showButtonPanel: true, yearRange: "c-99:c+99"});
});

function ExcelDown(){
	
var form = "<form action='static_14_excel.php' method='post'>"; 
		form += "<input type='hidden' name='ptype' value='excel' />";
		form += "<input type='hidden' name='start_date' value='"+$('#start_date').val()+"' />"; 	
		form += "<input type='hidden' name='end_date' value='"+$('#end_date').val()+"' />"; 		
		form += "<input type='hidden' name='os_gubun' value='"+$('#os_gubun option:selected').val()+"' />"; 
		form += "</form>"; 
		jQuery(form).appendTo("body").submit().remove(); 

}

</script>

<?php
include_once (G5_ADMIN_PATH.'/admin.tail.php');
?>

<?php
$sub_menu = "500310";
include_once('./_common.php');

auth_check($auth[$sub_menu], "r");


$now = date( "Ymd" );
if ( $list_year == "" ) {
	$list_year = year( $now );
}

$ExcelTitle = urlencode( "포인트_및_쿠폰현황" . date( "YmdHis" ) );

header( "Content-type: application/vnd.ms-excel" );
header( "Content-type: application/vnd.ms-excel; charset=utf-8" );
header( "Content-Disposition: attachment; filename = " . $ExcelTitle . ".xls" );
header( "Content-Description: PHP4 Generated Data" );
print( "<meta http-equiv=\"Content-Type\" content=\"application/vnd.ms-excel; charset=utf-8\">" ); 	

?>

<style>
	td {
	   mso-number-format:"\@";
	}
</style>


<table border="1">
<thead>
<tr bgcolor="F7F7F7" height="30">
	<th scope="col" colspan="2" rowspan="2">구분</th>
	<th scope="col" colspan="13"><?=$list_year?>년</th>
</tr>
<tr bgcolor="F7F7F7" height="30">
	<th>01월</th>
	<th>02월</th>
	<th>03월</th>
	<th>04월</th>
	<th>05월</th>
	<th>06월</th>
	<th>07월</th>
	<th>08월</th>
	<th>09월</th>
	<th>10월</th>
	<th>11월</th>
	<th>12월</th>
	<th>합계</th>
</tr>
</thead>
<tbody>

<?php

	/* 지급 합계 변수 */
	$a_m_1 = 0;
	$a_m_2 = 0;
	$a_m_3 = 0;
	$a_m_4 = 0;
	$a_m_5 = 0;
	$a_m_6 = 0;
	$a_m_7 = 0;
	$a_m_8 = 0;
	$a_m_9 = 0;
	$a_m_10 = 0;
	$a_m_11 = 0;
	$a_m_12 = 0;

	/* 사용 합계 변수 */
	$b_m_1 = 0;
	$b_m_2 = 0;
	$b_m_3 = 0;
	$b_m_4 = 0;
	$b_m_5 = 0;
	$b_m_6 = 0;
	$b_m_7 = 0;
	$b_m_8 = 0;
	$b_m_9 = 0;
	$b_m_10 = 0;
	$b_m_11 = 0;
	$b_m_12 = 0;

	/* 소멸 합계 변수 */
	$c_m_1 = 0;
	$c_m_2 = 0;
	$c_m_3 = 0;
	$c_m_4 = 0;
	$c_m_5 = 0;
	$c_m_6 = 0;
	$c_m_7 = 0;
	$c_m_8 = 0;
	$c_m_9 = 0;
	$c_m_10 = 0;
	$c_m_11 = 0;
	$c_m_12 = 0;

	$result = sql_query("CALL SP_P_11('".$list_year."')");
	for ($i=0; $row=sql_fetch_array($result); $i++) {

		if( $i == 0 ){
			$title = "포인트";
		}else if( $i == 3 ){
			$title = "쿠폰";			
		}else{
			$title = "";
		}

		$m_1 = $row["m_1"];
		$m_2 = $row["m_2"];
		$m_3 = $row["m_3"];
		$m_4 = $row["m_4"];
		$m_5 = $row["m_5"];
		$m_6 = $row["m_6"];
		$m_7 = $row["m_7"];
		$m_8 = $row["m_8"];
		$m_9 = $row["m_9"];
		$m_10 = $row["m_10"];
		$m_11 = $row["m_11"];
		$m_12 = $row["m_12"];

		if( $m_9 < 0 ){ $m_9 = $m_9 * -1; }
		if( $m_10 < 0 ){ $m_10 = $m_10 * -1; }
		if( $m_11 < 0 ){ $m_11 = $m_11 * -1; }

		if( $i == 0 || $i == 3 ){ // 포인트거나 쿠폰이면
			$sub_title = "지급";
			
			$a_m_1 = $a_m_1 + $m_1;
			$a_m_2 = $a_m_2 + $m_2;
			$a_m_3 = $a_m_3 + $m_3;
			$a_m_4 = $a_m_4 + $m_4;
			$a_m_5 = $a_m_5 + $m_5;
			$a_m_6 = $a_m_6 + $m_6;
			$a_m_7 = $a_m_7 + $m_7;
			$a_m_8 = $a_m_8 + $m_8;
			$a_m_9 = $a_m_9 + $m_9;
			$a_m_10 = $a_m_10 + $m_10;
			$a_m_11 = $a_m_11 + $m_11;
			$a_m_12 = $a_m_12 + $m_12;

		}else if( $i == 1 || $i == 4 ){
			$sub_title = "사용";	
			
			$b_m_1 = $b_m_1 + $m_1;
			$b_m_2 = $b_m_2 + $m_2;
			$b_m_3 = $b_m_3 + $m_3;
			$b_m_4 = $b_m_4 + $m_4;
			$b_m_5 = $b_m_5 + $m_5;
			$b_m_6 = $b_m_6 + $m_6;
			$b_m_7 = $b_m_7 + $m_7;
			$b_m_8 = $b_m_8 + $m_8;
			$b_m_9 = $b_m_9 + $m_9;
			$b_m_10 = $b_m_10 + $m_10;
			$b_m_11 = $b_m_11 + $m_11;
			$b_m_12 = $b_m_12 + $m_12;

		}else if( $i == 2 || $i == 5 ){
			$sub_title = "소멸";

			$c_m_1 = $c_m_1 + $m_1;
			$c_m_2 = $c_m_2 + $m_2;
			$c_m_3 = $c_m_3 + $m_3;
			$c_m_4 = $c_m_4 + $m_4;
			$c_m_5 = $c_m_5 + $m_5;
			$c_m_6 = $c_m_6 + $m_6;
			$c_m_7 = $c_m_7 + $m_7;
			$c_m_8 = $c_m_8 + $m_8;
			$c_m_9 = $c_m_9 + $m_9;
			$c_m_10 = $c_m_10 + $m_10;
			$c_m_11 = $c_m_11 + $m_11;
			$c_m_12 = $c_m_12 + $m_12;
		}
		
?>

<tr>
	<? if( !empty($title) ){ ?>
	<td rowspan="3"><?=$title?></td>
	<? } ?>
	<td><?=$sub_title?></td>
	<td><?=number_format($m_1)?></td>
	<td><?=number_format($m_2)?></td>
	<td><?=number_format($m_3)?></td>
	<td><?=number_format($m_4)?></td>
	<td><?=number_format($m_5)?></td>
	<td><?=number_format($m_6)?></td>
	<td><?=number_format($m_7)?></td>
	<td><?=number_format($m_8)?></td>
	<td><?=number_format($m_9)?></td>
	<td><?=number_format($m_10)?></td>
	<td><?=number_format($m_11)?></td>
	<td><?=number_format($m_12)?></td>
	<td><?=number_format($m_1+$m_2+$m_3+$m_4+$m_5+$m_6+$m_7+$m_8+$m_9+$m_10+$m_11+$m_12)?></td>
</tr>
<?
	}
?>

</tbody>
</table>
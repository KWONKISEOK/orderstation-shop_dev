<?php
$sub_menu = "500260";
include_once('./_common.php');

auth_check($auth[$sub_menu], 'r');

$g5['title'] = '주차별 매출현황';
include_once(G5_ADMIN_PATH.'/admin.head.php');
include_once(G5_PLUGIN_PATH.'/jquery-ui/datepicker.php');

if( empty($start_date) && empty($end_date) ){
	$getWeeekDate = GetWeekInfo("1");
	$start_date = explode ("|",  $getWeeekDate )[0];
	$end_date = explode ("|",  $getWeeekDate )[1];
}

if( $start_date > $end_date ){
	echo "<script>";
	echo "alert('시작날짜가 종료날짜 보다 큽니다.날짜 선택을 다시해주세요.');";
	echo "location.href='./static_17_1.php';";
	echo "</script>";
}

$GetDateDiff = GetDateDiffInfo($start_date,$end_date);

?>

<form id="fsearch" name="fsearch" class="local_sch01 local_sch" action="?" method="get">

<div class="local_sch03  tbl_wrap" style="padding:10px;">
    <table>
    <tbody>
	<tr>
        <td>
			날짜선택 &nbsp;
            <input type="text" id="start_date"  name="start_date" value="<?php echo $start_date; ?>" class="frm_input" size="10" maxlength="10"> ~
			<input type="text" id="end_date"  name="end_date" value="<?php echo $end_date; ?>" class="frm_input" size="10" maxlength="10">	
			&nbsp;
			<input type="submit" value="검색" class="get_theme_confc btn btn_01">
			&nbsp;&nbsp;
           <select name="cate_gubun" id="cate_gubun" >
				<option value="">==선택==</option>
				<option value="10" <?php echo get_selected($cate_gubun, '10'); ?>>푸드매니지</option>
				<option value="20" <?php echo get_selected($cate_gubun, '20'); ?>>홈매니지</option>
				<option value="30" <?php echo get_selected($cate_gubun, '30'); ?>>라이프매니지</option>
				<option value="40" <?php echo get_selected($cate_gubun, '40'); ?>>뷰티매니지</option><!--5는 오피스매니지-->
				<option value="60" <?php echo get_selected($cate_gubun, '60'); ?>>OS브랜드관</option>
				<option value="70" <?php echo get_selected($cate_gubun, '70'); ?>>반려동물</option>
			</select>

		 <input type="button" value="엑셀 다운로드" class="get_theme_confc btn btn_02" style="cursor:pointer" onclick="formSubmit(1);">

    </tr>
    </tbody>
    </table>
</div>

</form>


<h2 class="h2_frm"><?=$start_date?>~<?=$end_date?> 매출실적</h2>

<div class="tbl_head01 tbl_wrap">

    <table>
    <caption> 주차별 매출현황 </caption>
    <thead>
    <tr>
        <th scope="col" colspan="4">이번주</th>
        <th scope="col" rowspan="2">지난주 매출</th>
        <th scope="col" rowspan="2">매출 건수</th>
        <th scope="col" rowspan="2">전년 동기간 매출</th>
        <th scope="col" rowspan="2">매출 건수</th>
    </tr>
	<tr>
		<th>매출액</th>
		<th>매출 건수</th>
		<th>지난주대비</th>
		<th>전년대비</th>
	</tr>
    </thead>
    <tbody>
	<?
	$cal_price_1 = 0;
	$cal_price_2 = 0;

	$result = sql_query("CALL SP_P_17_1('".$start_date."','".$end_date."',".$GetDateDiff.")");
	for ($i=0; $row=sql_fetch_array($result); $i++) {

		$cal_price_1 = ( ( (float)$row["total_price_1"] - (float)$row["total_price_2"] ) * 100 ) / (float)$row["total_price_2"];
		$cal_price_2 = ( ( (float)$row["total_price_1"] - (float)$row["total_price_3"] ) * 100 ) / (float)$row["total_price_3"];
	?>
	<tr>
		<td><?=number_format($row["total_price_1"])?></td>
		<td><?=$row["total_cnt_1"]?>건</td>
		<td><?=round($cal_price_1)?>%</td>
		<td><?=round($cal_price_2)?>%</td>
		<td><?=number_format($row["total_price_2"])?> (<?=$row["total_cnt_2"]?>건)</td>
		<td><?=$row["total_cnt_2"]?>건</td>
		<td><?=number_format($row["total_price_3"])?> (<?=$row["total_cnt_3"]?>건)</td>
		<td><?=$row["total_cnt_3"]?>건</td>
	</tr>
    <?
	}
	?>
    </tbody>
    <tbody>
	</table>

</div>


<h2 class="h2_frm">카테고리별 매출실적</h2>

<div class="tbl_head01 tbl_wrap tabContent">

	<center><img src="/img/loading.gif"/></center>

</div>

<script>

$(function(){
    $("#start_date, #end_date").datepicker({ changeMonth: true, changeYear: true, dateFormat: "yy-mm-dd", showButtonPanel: true, yearRange: "c-99:c+99"});
});

/* php 에서 프로시저 두개를 동시에 호출하지 못하는 이유로 Ajax로 따로 호출해줬음.. */
$.ajax({
	type: "POST",
	url: "./static_17_2.php?start_date=<?=$start_date?>&end_date=<?=$end_date?>",
	contentType: "application/x-www-form-urlencoded; charset=UTF-8",
	error : function(error) {
		alert("에러");
	},
	success: function (data){

		$(".tabContent").empty();
		$(".tabContent").html(data);		

	}
});

function formSubmit(type) {

	//$where[] = " c.od_date1 between '$start_date 00:00:00' and '$end_date 23:59:59' ";

	var form = "<form action='static_17_1_01.php' method='post'>"; 
		form += "<input type='hidden' name='ptype' value='excel' />";
		form += "<input type='hidden' name='start_date' value='"+$('#start_date').val()+"' />"; 	
		form += "<input type='hidden' name='end_date' value='"+$('#end_date').val()+"' />"; 		
		form += "<input type='hidden' name='cate_gubun' value='"+$('#cate_gubun option:selected').val()+"' />"; 
		form += "</form>"; 
		jQuery(form).appendTo("body").submit().remove(); 
}
</script>

<?php
include_once (G5_ADMIN_PATH.'/admin.tail.php');
?>

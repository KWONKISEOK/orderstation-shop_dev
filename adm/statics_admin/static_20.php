<?php
$sub_menu = "500290";
include_once('./_common.php');

auth_check($auth[$sub_menu], 'r');

$g5['title'] = '시간대별 매출현황';
include_once(G5_ADMIN_PATH.'/admin.head.php');
include_once(G5_PLUGIN_PATH.'/jquery-ui/datepicker.php');

if( empty($start_date) && empty($end_date) ){
	$getWeeekDate = GetWeekInfo("1");
	$start_date = explode ("|",  $getWeeekDate )[0];
	$end_date = explode ("|",  $getWeeekDate )[1];
}

if( $start_date > $end_date ){
	echo "<script>";
	echo "alert('시작날짜가 종료날짜 보다 큽니다.날짜 선택을 다시해주세요.');";
	echo "location.href='./static_20.php';";
	echo "</script>";
}

$GetDateDiff = GetDateDiffInfo($start_date,$end_date);

$Total_cnt_0 = 0;
$Total_cnt_1 = 0;
$Total_cnt_2 = 0;
$Total_cnt_3 = 0;
$Total_sum_0 = 0;
$Total_sum_1 = 0;
$Total_sum_2 = 0;
$Total_sum_3 = 0;
$F_Total_0 = 0;
$F_Total_1 = 0;
$F_Total_2 = 0;
$F_Total_3 = 0;
$F_Total_4 = 0;
$F_Total_5 = 0;
$F_Total_6 = 0;
$F_Total_7 = 0;

?>

<form id="fsearch" name="fsearch" class="local_sch01 local_sch" action="?" method="get">

<div class="local_sch03  tbl_wrap" style="padding:10px;">
    <table>
    <tbody>
	<tr>
        <td>
			날짜선택 &nbsp;
            <input type="text" id="start_date"  name="start_date" value="<?php echo $start_date; ?>" class="frm_input" size="10" maxlength="10"> ~
			<input type="text" id="end_date"  name="end_date" value="<?php echo $end_date; ?>" class="frm_input" size="10" maxlength="10">	
			&nbsp;
			<input type="submit" value="검색" class="get_theme_confc btn btn_01">
			 <input type="button" value="엑셀 다운로드" class="get_theme_confc btn btn_02" style="cursor:pointer" onclick="javascript:ExcelDown();">
        </td>
    </tr>
    </tbody>
    </table>
</div>

</form>


<div class="tbl_head01 tbl_wrap">

    <table>
    <caption> 시간대별 매출현황 </caption>
    <thead>
    <tr>
        <th scope="col" rowspan="2">시간대</th>
        <th scope="col" rowspan="2">상세시간대</th>
        <th scope="col" colspan="4">주문건</th>
        <th scope="col" colspan="4">매출액</th>
    </tr>
	<tr>
		<th width="10%">평균</th>
		<th width="10%">이번주</th>
		<th width="10%">지난주</th>
		<th width="10%">전년동기간</th>
		<th width="10%">평균</th>
		<th width="10%">이번주</th>
		<th width="10%">지난주</th>
		<th width="10%">전년동기간</th>
	</tr>
    </thead>
    <tbody>
	<?
		$result = sql_query("CALL SP_P_20('".$start_date."','".$end_date."',".$GetDateDiff.")");
		for ($i=0; $row=sql_fetch_array($result); $i++) {

			$t_rowspan = "";
			$t_subject = "";

			if($i == 0){
				$t_rowspan = " rowspan='8' ";
				$t_subject = " 00~06시 ";
			}else if($i == 7){
				$t_rowspan = " rowspan='7' ";
				$t_subject = " 07~12시 ";
			}else if($i == 13){
				$t_rowspan = " rowspan='7' ";
				$t_subject = " 13~18시 ";
			}else if($i == 19){
				$t_rowspan = " rowspan='6' ";
				$t_subject = " 19~23시 ";
			}

			$Total_cnt_0 += round( ( $row["total_cnt_1"]+$row["total_cnt_2"]+$row["total_cnt_3"] ) / 3 , 1 );
			$Total_cnt_1 += $row["total_cnt_1"];
			$Total_cnt_2 += $row["total_cnt_2"];
			$Total_cnt_3 += $row["total_cnt_3"];

			$Total_sum_0 += round( ( $row["total_price_1"]+$row["total_price_2"]+$row["total_price_3"] ) / 3 );
			$Total_sum_1 += $row["total_price_1"];
			$Total_sum_2 += $row["total_price_2"];
			$Total_sum_3 += $row["total_price_3"];

	?>
	<tr>
		<? if( $i == 0 || $i == 7 || $i == 13 || $i == 19 ){ ?>
		<td <?=$t_rowspan?> ><strong><?=$t_subject?></strong></td>
		<? } ?>
		<td>
			<?
			if( strlen($row["cd_date_hour"]) == 1 ){
				echo "0".$row["cd_date_hour"];
			}else{
				echo $row["cd_date_hour"];
			}
			?>
		</td>
		<td><?=round( ( $row["total_cnt_1"]+$row["total_cnt_2"]+$row["total_cnt_3"] ) / 3 , 1 )?></td>
		<td><?=$row["total_cnt_1"]?></td>
		<td><?=$row["total_cnt_2"]?></td>
		<td><?=$row["total_cnt_3"]?></td>
		<td><?=number_format( round( ( $row["total_price_1"]+$row["total_price_2"]+$row["total_price_3"] ) / 3 ) )?></td>
		<td><?=number_format($row["total_price_1"])?></td>
		<td><?=number_format($row["total_price_2"])?></td>
		<td><?=number_format($row["total_price_3"])?></td>		
	</tr>
	<? if( $i == 6 || $i == 12 || $i == 18 || $i == 23 ){ ?>
	<tr>
		<td><strong>Sub-Total</strong></td>
		<td><strong><?=$Total_cnt_0?></strong></td>
		<td><strong><?=$Total_cnt_1?></strong></td>
		<td><strong><?=$Total_cnt_2?></strong></td>
		<td><strong><?=$Total_cnt_3?></strong></td>
		<td><strong><?=number_format( $Total_sum_0 )?></strong></td>
		<td><strong><?=number_format( $Total_sum_1 )?></strong></td>
		<td><strong><?=number_format( $Total_sum_2 )?></strong></td>
		<td><strong><?=number_format( $Total_sum_3 )?></strong></td>		
	</tr>
	<? 
		$F_Total_0 += $Total_cnt_0;
		$F_Total_1 += $Total_cnt_1;
		$F_Total_2 += $Total_cnt_2;
		$F_Total_3 += $Total_cnt_3;
		$F_Total_4 += $Total_sum_0;
		$F_Total_5 += $Total_sum_1;
		$F_Total_6 += $Total_sum_2;
		$F_Total_7 += $Total_sum_3;

		$Total_cnt_0 = 0;
		$Total_cnt_1 = 0;
		$Total_cnt_2 = 0;
		$Total_cnt_3 = 0;
		$Total_sum_0 = 0;
		$Total_sum_1 = 0;
		$Total_sum_2 = 0;
		$Total_sum_3 = 0;	
	} 
	?>
    <?
	}
	?>
	<tr>
		<td colspan="2"><strong>Total</strong></td>
		<td><strong><?=$F_Total_0?></strong></td>
		<td><strong><?=$F_Total_1?></strong></td>
		<td><strong><?=$F_Total_2?></strong></td>
		<td><strong><?=$F_Total_3?></strong></td>
		<td><strong><?=number_format( $F_Total_4 )?></strong></td>
		<td><strong><?=number_format( $F_Total_5 )?></strong></td>
		<td><strong><?=number_format( $F_Total_6 )?></strong></td>
		<td><strong><?=number_format( $F_Total_7 )?></strong></td>
	</tr>
    </tbody>
    <tbody>
	</table>

</div>


<script>

$(function(){
    $("#start_date, #end_date").datepicker({ changeMonth: true, changeYear: true, dateFormat: "yy-mm-dd", showButtonPanel: true, yearRange: "c-99:c+99"});
});

function ExcelDown(){
	
		var form = "<form action='static_20_excel.php' method='post'>"; 
		form += "<input type='hidden' name='ptype' value='excel' />";
		form += "<input type='hidden' name='start_date' value='"+$('#start_date').val()+"' />"; 	
		form += "<input type='hidden' name='end_date' value='"+$('#end_date').val()+"' />"; 		 
		form += "</form>"; 
		jQuery(form).appendTo("body").submit().remove(); 

}

</script>

<?php
include_once (G5_ADMIN_PATH.'/admin.tail.php');
?>

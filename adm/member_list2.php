<?php
$sub_menu = "200110";
include_once('./_common.php');

auth_check($auth[$sub_menu], 'r');

$sql_common = " from tbl_member_user a, tbl_member b ";

$sql_search = " where (1) ";
if ($stx) {
    $sql_search .= " and ( ";
    switch ($sfl) {
        
        case 'mb_name' :
        case 'pharm_custno' :
            $sql_search .= " ({$sfl} like '%{$stx}') ";
            break;
		case 'pharm_name' :
            $sql_search .= " ({$sfl} like '%{$stx}%') ";
            break;
        default :
            $sql_search .= " ({$sfl} like '{$stx}%') ";
            break;
    }
    $sql_search .= " ) ";
}

$sql_search .= " and a.mb_id = b.mb_id   ";

if ($pharm_id) {
    $sql_search .= " and a.mb_id = '$pharm_id' ";
}


if (!$sst) {
    $sst = "ad_datetime";
    $sod = "desc";
}


$sql_order = " order by {$sst} {$sod} ";

$sql = " select count(*) as cnt {$sql_common} {$sql_search} {$sql_order} ";
$row = sql_fetch($sql);
$total_count = $row['cnt'];

$rows = $config['cf_page_rows'];
$total_page  = ceil($total_count / $rows);  // 전체 페이지 계산
if ($page < 1) $page = 1; // 페이지가 없으면 첫 페이지 (1 페이지)
$from_record = ($page - 1) * $rows; // 시작 열을 구함


$listall = '<a href="'.$_SERVER['SCRIPT_NAME'].'" class="ov_listall">전체목록</a>';

$g5['title'] = '약국 소비자 회원관리';
include_once('./admin.head.php');

$sql = " select a.* , b.pharm_name
		{$sql_common} {$sql_search} {$sql_order} limit {$from_record}, {$rows} ";
//echo $sql;

$result = sql_query($sql);

$colspan = 16;
?>

<div class="local_ov01 local_ov">
    <?php echo $listall ?>
    <span class="btn_ov01"><span class="ov_txt">총회원수 </span><span class="ov_num"> <?php echo number_format($total_count) ?>명 </span></span>
    <!-- <a href="?sst=mb_intercept_date&amp;sod=desc&amp;sfl=<?php echo $sfl ?>&amp;stx=<?php echo $stx ?>" class="btn_ov01"> <span class="ov_txt">차단 </span><span class="ov_num"><?php echo number_format($intercept_count) ?>명</span></a>
    <a href="?sst=mb_leave_date&amp;sod=desc&amp;sfl=<?php echo $sfl ?>&amp;stx=<?php echo $stx ?>" class="btn_ov01"> <span class="ov_txt">탈퇴  </span><span class="ov_num"><?php echo number_format($leave_count) ?>명</span></a> -->
</div>

<form id="fsearch" name="fsearch" class="local_sch01 local_sch" method="get">


<div class="local_sch03  tbl_wrap" style="padding:10px;">
    <table>
   
    <tbody>
	
    </tr>
	
		<th scope="row">검색</th>
        <td>
            <select name="sfl" id="sfl" >
			<option value="ad_name" <?php echo get_selected($sfl, 'ad_name'); ?>>이름</option>
			<!-- <option value="mb_id" <?php echo get_selected($sel_field, 'mb_id'); ?>>아이디</option> -->
			
			</select>
			<label for="stx" class="sound_only">검색어</label>
			<input type="text" name="stx" value="<?php echo $stx ?>" id="stx" class=" frm_input">
			<input type="submit" value="검색" class="get_theme_confc btn btn_01">

			<input type="button" value="엑셀 다운로드" class="get_theme_confc btn btn_02" style="cursor:pointer" onclick="formSubmit(1);">
        </td>
		
    </tr>
	
    </tbody>
    </table>
</div>



</form>

<!-- <div class="local_desc01 local_desc">
    <p>
        회원자료 삭제 시 다른 회원이 기존 회원아이디를 사용하지 못하도록 회원아이디, 이름, 닉네임은 삭제하지 않고 영구 보관합니다.
    </p>
</div> -->


<form name="fmemberlist" id="fmemberlist" action="" onsubmit="return fmemberlist_submit(this);" method="post">
<input type="hidden" name="sst" value="<?php echo $sst ?>">
<input type="hidden" name="sod" value="<?php echo $sod ?>">
<input type="hidden" name="sfl" value="<?php echo $sfl ?>">
<input type="hidden" name="stx" value="<?php echo $stx ?>">
<input type="hidden" name="page" value="<?php echo $page ?>">
<input type="hidden" name="token" value="">

<div class="tbl_head01 tbl_wrap">
    <table>
    <caption><?php echo $g5['title']; ?> 목록</caption>
    <thead>
    <tr>
        <th width="10"></th>
        <th scope="col" id="mb_list_id" >담당약국</th>
		<th scope="col" id="mb_list_name">이름</th>

		<th scope="col" id="mb_list_auth">연락처</th>
		<th scope="col" id="mb_list_auth">등록일</th>
		<th scope="col" id="mb_list_mng">관리</th>

    </tr>
    </thead>
    <tbody>
    <?php
    for ($i=0; $row=sql_fetch_array($result); $i++) {

       $bg = 'bg'.($i%2);
	   $s_mod = '<a href="./member_form2.php?'.$qstr.'&amp;w=u&amp;m=3&amp;ad_id='.$row['ad_id'].'" class="btn btn_03">수정</a>';
    ?>
    <tr class="<?php echo $bg; ?>">        
        <td ></td>
		<td align="center"><?php echo $row[pharm_name]; ?></td>
		<td align="center"><?php echo $row[ad_name]; ?></td>
		<td align="center"><?php echo $row[ad_hp]; ?></td>
		<td align="center"><?php echo substr($row[ad_datetime], 0, 10); ?></td>
		<td width="150">
			<?php echo $s_mod ?>			
		</td>
	</tr>
    <?php
    }
    if ($i == 0)
        echo "<tr><td colspan=\"".$colspan."\" class=\"empty_table\">자료가 없습니다.</td></tr>";
    ?>
    </tbody>
    </table>
</div>
</form>

<?php 
$qstr ='pharm_id='.$_GET['pharm_id'].'&'.$qstr;
echo get_paging(G5_IS_MOBILE ? $config['cf_mobile_pages'] : $config['cf_write_pages'], $page, $total_page, '?'.$qstr.'&amp;page='); ?>

<script>
function formSubmit(type) {
	var form = "<form action='member_list2_01.php' method='post'>"; 
		form += "<input type='hidden' name='mode' value='excel' />"; 
	form += "<input type='hidden' name='pharm_id' value='"+$('#pharm_id').val()+"' />"; 
	form += "<input type='hidden' name='sfl' value='"+$('#sfl option:selected').val()+"' />"; 
	form += "<input type='hidden' name='stx' value='"+$('#stx').val()+"' />"; 
	form += "</form>"; 
	jQuery(form).appendTo("body").submit().remove(); 
}

function fmemberlist_submit(f)
{
    if (!is_checked("chk[]")) {
        alert(document.pressed+" 하실 항목을 하나 이상 선택하세요.");
        return false;
    }

    if(document.pressed == "선택삭제") {
        if(!confirm("선택한 자료를 정말 삭제하시겠습니까?")) {
            return false;
        }
    }

    return true;
}
function fnLogin(id) {
	if(confirm(id+" 로 로그인하시겠습니까?")) {
		location.href='/bbs/login_check_admin.php?mb_id=' + id;
	}
}
</script>

<?php
include_once ('./admin.tail.php');
?>

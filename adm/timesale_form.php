<?php
$sub_menu = '100400';
include_once('./_common.php');

auth_check($auth[$sub_menu], 'r');

$g5['title'] = '타임세일설정';
include_once('./admin.head.php');
include_once(G5_PLUGIN_PATH.'/jquery-ui/datepicker.php');
?>

<section>

    <form name="frm" method="post" action="./timesale_form_update.php">
	<fieldset id="fsendmailtest" style="text-align:left;">
		<input type="radio" name="cf_time_sale_use" value="Y" <?=get_checked($config['cf_time_sale_use'],"Y")?>> 배너노출
		<input type="radio" name="cf_time_sale_use" value="N" <?=get_checked($config['cf_time_sale_use'],"N")?>> 배너비노출
		&nbsp;&nbsp;<br><br>
		이벤트코드값 : <input type="text" name="cf_time_sale_it_id" value="<?=$config['cf_time_sale_it_id']?>"/>
		&nbsp;&nbsp;<br><br>
		시작일:<input type="text" name="cf_time_sale_startdate" id="cf_time_sale_startdate" value="<?=$config['cf_time_sale_startdate']?>" maxlength="10"/>
		~종료일:<input type="text" name="cf_time_sale_enddate" id="cf_time_sale_enddate" value="<?=$config['cf_time_sale_enddate']?>" maxlength="10"/>
		<input type="submit" value="저장" class="btn_submit"><br>
		( 오늘날짜가 셋팅날짜 범위에 있을때 타임세일상품으로 체크된 상품들을 판매 가능하고 범위안에 없을경우 "당일 오전 11:00~12:00, 15:00~16:00 에만 구매 가능한 상품입니다." 라는 문구가 상세페이지에 띄워집니다. )
	</fieldset>
    </form>

</section>

<script>

$(function(){
    $("#cf_time_sale_startdate, #cf_time_sale_enddate").datepicker({ changeMonth: true, changeYear: true, dateFormat: "yy-mm-dd", showButtonPanel: true, yearRange: "c-99:c+99"});
});

</script>

<?php
include_once('./admin.tail.php');
?>

<?php
$sub_menu = '300860';
include_once('./_common.php');

if ( $w == "u"){
   
    $sql = " select * from tbl_isapp_movie where wr_id = '$wr_id' ";
    $wr_row = sql_fetch($sql);

	$wr_file_1 = $wr_row["wr_file_1"];
	$wr_file_3 = $wr_row["wr_file_3"];
}

$movie_dir = G5_DATA_PATH.'/movie';
if(!is_dir($movie_dir)) {
	@mkdir($movie_dir, G5_DIR_PERMISSION);
	@chmod($movie_dir, G5_DIR_PERMISSION);
}

// 썸네일 파일삭제
if ( $movie_img_del && $wr_row["wr_file_1"] ) {
    $movie_img = $movie_dir.'/'.$wr_row["wr_file_1"];
    @unlink($movie_img);
	$wr_file_1 = "";
}
// 동영상 파일삭제
if ( $movie_file_del && $wr_row["wr_file_3"] ) {
    $movie_file = $movie_dir.'/'.$wr_row["wr_file_3"];
    @unlink($movie_file);
	$wr_file_3 = "";
}
// 썸네일업로드
if ($_FILES['wr_file_1']['name']) {
    if($wr_row["wr_file_1"]) {
        $movie_img = $movie_dir.'/'.$wr_row["wr_file_1"];
        @unlink($movie_img);
    }
    $wr_file_1 = it_img_upload($_FILES['wr_file_1']['tmp_name'], $_FILES['wr_file_1']['name'], $movie_dir);
	$wr_file_1 = str_replace($movie_dir, '', $wr_file_1);
}
// 동영상업로드
if ($_FILES['wr_file_3']['name']) {
 
    $prepend = '';

    // 동일한 이름의 파일이 있으면 파일명 변경
    if(is_file($movie_dir.'/'.$_FILES['wr_file_3']['name'])) {

        for($i=0; $i<20; $i++) {
            $prepend = str_replace('.', '_', microtime(true)).'_';

            if(is_file($movie_dir.'/'.$prepend.$_FILES['wr_file_3']['name'])) {
                usleep(mt_rand(100, 10000));
                continue;
            } else {
                break;
            }
        }
    }

	upload_file($_FILES['wr_file_3']['tmp_name'], $prepend.$_FILES['wr_file_3']['name'], $movie_dir);
    $wr_file_3 = $movie_dir.'/'.$prepend.$_FILES['wr_file_3']['name'];
	$wr_file_3 = str_replace($movie_dir, '', $wr_file_3);
}

$sql_common = " wr_subject    = '$wr_subject',
                wr_content    = '$wr_content',
                mb_id		  = '{$member['mb_id']}',
				wr_file_1	  = '{$wr_file_1}',
				wr_file_2	  = '{$wr_file_2}',
				wr_file_3	  = '{$wr_file_3}',
                wr_name       = '{$member['mb_name']}' ";

if ($w == ""){

    $row = $wr_row;
    if ($row['wr_id'])
        alert("이미 같은 ID로 등록된 내용이 있습니다.");

    $sql = " insert tbl_isapp_movie
                set wr_id = '$wr_id',
                    $sql_common ";
    sql_query($sql);

}else if ($w == "u"){

    $sql = " update tbl_isapp_movie
                set $sql_common
              where wr_id = '$wr_id' ";
    sql_query($sql);

}else if ($w == "d"){

    $sql = " delete from tbl_isapp_movie where wr_id = '$wr_id' ";
    sql_query($sql);

}

goto_url("./isapp_movie_list.php");

?>

<?php
if (!defined('G5_USE_SHOP') || !G5_USE_SHOP) return;

$menu['menu500'] = array (
    array('500000', '실적관리', G5_ADMIN_URL.'/shop_order/calculate.php', 'shop_stats'),
    array('500110', '정산', G5_ADMIN_URL.'/shop_order/calculate1.php', 'sst_order_stats'),
    array('500100', '배송일기준 정산', G5_ADMIN_URL.'/shop_order/calculate2.php', 'sst_rank'),

    array('500230', '얼라이언스 기간별매출', '/adm/statics_admin/static_14.php', 'Static14') ,
    array('500240', '얼라이언스 회원수현황', '/adm/statics_admin/static_15.php', 'Static15') ,
    array('500250', '얼라이언스 연매출비교', '/adm/statics_admin/static_16.php', 'Static16') ,
    array('500260', '주차별 매출현황', '/adm/statics_admin/static_17_1.php', 'Static17_1'),

    array('500120', '일매출요약', G5_ADMIN_URL.'/shop_order/calculate3.php', 'sst_print_order', 1),
    array('500400', '주매출요약', G5_ADMIN_URL.'/shop_order/calculate4.php', 'sst_stock_sms', 1),
    array('500300', '월매출요약', G5_ADMIN_URL.'/shop_order/calculate5.php', 'scf_event'),
    array('500310', '가동처현황', G5_ADMIN_URL.'/shop_order/calculate6.php', 'scf_event_mng'),
    array('500500', '3사 팀별 가동률,매출', G5_ADMIN_URL.'/shop_order/calculate7.php', 'scf_banner', 1),
    array('500140', '상품별매출', G5_ADMIN_URL.'/shop_order/calculate8.php', 'sst_wish'),
    //array('500210', '담당자별매출', G5_ADMIN_URL.'/shop_order/calculate9.php', 'sst_compare', 1),
	//array('500220', '월마감현황', G5_ADMIN_URL.'/shop_order/calculate10.php', 'sst_compare1', 1),

    array('500600', 'ERP 정기주문 매출현황', G5_ADMIN_URL.'/statics_admin/static_22.php', ''),

    array('', '-----------------------', '#', ''),

    //array('901100', '포인트 및 쿠폰현황', '/adm/statics_admin/static_11.php', 'Static11') ,
    array('500270', '회원별 매출현황', '/adm/statics_admin/static_18.php', 'Static18'),
    //array('500280', '요일별 매출현황', '/adm/statics_admin/static_19.php', 'Static19'),
    //array('500290', '시간대별 매출현황', '/adm/statics_admin/static_20.php', 'Static20'),
    //array('500300', '지역별 매출현황', '/adm/statics_admin/static_21.php', 'Static21'),
	array('500310', '포인트 및 쿠폰현황',G5_ADMIN_URL. '/statics_admin/static_9021.php', 'Static9021') , 
	array('500320', '회원현황',G5_ADMIN_URL. '/statics_admin/static_9022.php', 'Static9022')
);
?>
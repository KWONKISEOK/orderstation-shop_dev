<?php
$sub_menu = "200100";
include_once('./_common.php');

auth_check($auth[$sub_menu], 'r');

$pharm_manager = $member['mb_id'];

$sql_common = " from {$g5['member_table']} a ";

$sql_search = " where (1) ";
if ($stx) {
    $sql_search .= " and ( ";
    switch ($sfl) {
        
        case 'mb_name' :
        case 'pharm_custno' :
            $sql_search .= " ({$sfl} like '%{$stx}') ";
            break;
		case 'pharm_name' :
            $sql_search .= " ({$sfl} like '%{$stx}%') ";
            break;
        default :
            $sql_search .= " ({$sfl} like '{$stx}%') ";
            break;
    }
    $sql_search .= " ) ";
}


$sql_search .= " and pharm_manager = '$pharm_manager' ";
$sql_search .= " and mb_type = '1' ";

//상태
if ($pharm_status) {
    $sql_search .= " and pharm_status = '$pharm_status' ";
}
if ($mb_referee) {
    $sql_search .= " and mb_referee = '$mb_referee' ";
}
if ($pharm_div) {
    $sql_search .= " and pharm_div = '$pharm_div' ";
}

if ($mb_pay_yn) {
    $sql_search .= " and mb_pay_yn = '$mb_pay_yn' ";
}
if ($pharm_status) {
    $sql_search .= " and pharm_status = '$pharm_status' ";
}

if ($sales_id) {
    $sql_search .= " and pharm_manager = '$sales_id' ";
}





if (!$sst) {
    $sst = "mb_datetime";
    $sod = "desc";
}

$sql_order = " order by {$sst} {$sod} ";

$sql = " select count(*) as cnt {$sql_common} {$sql_search} {$sql_order} ";
$row = sql_fetch($sql);
$total_count = $row['cnt'];

$rows = $config['cf_page_rows'];
$total_page  = ceil($total_count / $rows);  // 전체 페이지 계산
if ($page < 1) $page = 1; // 페이지가 없으면 첫 페이지 (1 페이지)
$from_record = ($page - 1) * $rows; // 시작 열을 구함
/*
// 탈퇴회원수
$sql = " select count(*) as cnt {$sql_common} {$sql_search} and mb_leave_date <> '' {$sql_order} ";
$row = sql_fetch($sql);
$leave_count = $row['cnt'];

// 차단회원수
$sql = " select count(*) as cnt {$sql_common} {$sql_search} and mb_intercept_date <> '' {$sql_order} ";
$row = sql_fetch($sql);
$intercept_count = $row['cnt'];
*/
$listall = '<a href="'.$_SERVER['SCRIPT_NAME'].'" class="ov_listall">전체목록</a>';

$g5['title'] = '약국회원관리';
include_once('./admin.head.php');

$sql = " select a.* 
                ,(select count(*) from tbl_member_user where mb_id = a.mb_id) as user_cnt
                ,(select count(*) from tbl_member where mb_recommend = a.mb_id) as user_cnt2
				,(select mb_name from tbl_member where mb_id = a.pharm_manager and mb_type=5 limit 1)  as sales_name
		{$sql_common} {$sql_search} {$sql_order} limit {$from_record}, {$rows} ";
//echo $sql;

$result = sql_query($sql);

$colspan = 16;
?>

<div class="local_ov01 local_ov">
    <?php echo $listall ?>
    <span class="btn_ov01"><span class="ov_txt">총회원수 </span><span class="ov_num"> <?php echo number_format($total_count) ?>명 </span></span>
    <!-- <a href="?sst=mb_intercept_date&amp;sod=desc&amp;sfl=<?php echo $sfl ?>&amp;stx=<?php echo $stx ?>" class="btn_ov01"> <span class="ov_txt">차단 </span><span class="ov_num"><?php echo number_format($intercept_count) ?>명</span></a>
    <a href="?sst=mb_leave_date&amp;sod=desc&amp;sfl=<?php echo $sfl ?>&amp;stx=<?php echo $stx ?>" class="btn_ov01"> <span class="ov_txt">탈퇴  </span><span class="ov_num"><?php echo number_format($leave_count) ?>명</span></a> -->
</div>

<form id="fsearch" name="fsearch" class="local_sch01 local_sch" method="get">


<div class="local_sch03  tbl_wrap" style="padding:10px;">
    <table>
   
    <tbody>
	<tr>
        <th scope="row">결제구분</th>
        <td>
			<select name="mb_pay_yn" id="mb_pay_yn"  >
			    <option value="">==전체==</option>
				<option value="Y" <?php echo get_selected($mb_pay_yn, 'Y'); ?>>오더스테이션결제</option>
				<option value="N" <?php echo get_selected($mb_pay_yn, 'N'); ?>>직거래</option>
			</select>
			
        </td>
		
		<th scope="row">거래구분</th>
        <td>
            <select name="pharm_div" id="pharm_div" >
				<option value="">==전체==</option>
				<option value="1" <?php echo get_selected($pharm_div, '1'); ?>>태전약품</option>
				<option value="2" <?php echo get_selected($pharm_div, '2'); ?>>티제이팜</option>
			</select>
        </td>
	
		<th scope="row">사용여부</th>
        <td>
           <select name="pharm_status" id="pharm_status" >
				<option value="">==전체==</option>
				<option value="N" <?php echo get_selected($pharm_status, 'N'); ?>>신규가입</option>
				<option value="L" <?php echo get_selected($pharm_status, 'L'); ?>>ERP등록중</option>
				<option value="1" <?php echo get_selected($pharm_status, '1'); ?>>사용대기</option>
				<option value="2" <?php echo get_selected($pharm_status, '2'); ?>>사용(승인)</option>
				<option value="3" <?php echo get_selected($pharm_status, '3'); ?>>사용안함</option>
			</select>
        </td>
		
		<th scope="row">검색</th>
        <td>
            <select name="sfl" id="sfl" >
			<option value="pharm_name" <?php echo get_selected($sfl, 'pharm_name'); ?>>약국명</option>
			<option value="mb_name" <?php echo get_selected($sfl, 'mb_name'); ?>>이름</option>
			<option value="mb_id" <?php echo get_selected($sfl, 'mb_id'); ?>>아이디</option>
			<option value="pharm_custno" <?php echo get_selected($sfl, 'pharm_custno'); ?>>약국코드</option>
			
			</select>
			<label for="stx" class="sound_only">검색어</label>
			<input type="text" name="stx" value="<?php echo $stx ?>" id="stx" class=" frm_input">
			<!-- <input type="submit" class="btn_submit" value="검색"> -->
        </td>

		<td>
			 <input type="submit" value="검색" class="get_theme_confc btn btn_01">

		</td>
    </tr>

	
    </tbody>
    </table>
</div>



</form>

<!-- <div class="local_desc01 local_desc">
    <p>
        회원자료 삭제 시 다른 회원이 기존 회원아이디를 사용하지 못하도록 회원아이디, 이름, 닉네임은 삭제하지 않고 영구 보관합니다.
    </p>
</div> -->


<form name="fmemberlist" id="fmemberlist" action="" onsubmit="return fmemberlist_submit(this);" method="post">
<input type="hidden" name="sst" value="<?php echo $sst ?>">
<input type="hidden" name="sod" value="<?php echo $sod ?>">
<input type="hidden" name="sfl" value="<?php echo $sfl ?>">
<input type="hidden" name="stx" value="<?php echo $stx ?>">


<input type="hidden" name="page" value="<?php echo $page ?>">
<input type="hidden" name="token" value="">

<div class="tbl_head01 tbl_wrap">
    <table>
    <caption><?php echo $g5['title']; ?> 목록</caption>
    <thead>
    <tr>
        <th width="10"></th>
      
		<th scope="col" id="mb_list_auth">약국명</th>
		
		<th scope="col" id="mb_list_auth">결제구분</th>
        <th scope="col" id="mb_list_auth">거래구분</th>
        <th scope="col" id="mb_list_mobile">약국코드</th>
        <th scope="col" id="mb_list_auth">아이디</th>
		<th scope="col" id="mb_list_auth">이름</th>
		<th scope="col" id="mb_list_auth">연락처</th>
		<th scope="col" id="mb_list_sms">상태</a></th>		
		<th scope="col" id="mb_list_mng">관리</th>
    </tr>
    </thead>
    <tbody>
    <?php
    for ($i=0; $row=sql_fetch_array($result); $i++) {

        // 접근가능한 그룹수
        $sql2 = " select count(*) as cnt from {$g5['group_member_table']} where mb_id = '{$row['mb_id']}' ";
        $row2 = sql_fetch($sql2);
        $group = '';
        if ($row2['cnt'])
            $group = '<a href="./boardgroupmember_form.php?mb_id='.$row['mb_id'].'">'.$row2['cnt'].'</a>';

        if ($is_admin == 'group') {
            $s_mod = '';
        } else {
            $s_mod = '<a href="./member_form.php?'.$qstr.'&amp;w=u&amp;mb_id='.$row['mb_id'].'" class="btn btn_03">수정</a>';
        }
        $s_grp = '<a href="./boardgroupmember_form.php?mb_id='.$row['mb_id'].'" class="btn btn_02">그룹</a>';

        $leave_date = $row['mb_leave_date'] ? $row['mb_leave_date'] : date('Ymd', G5_SERVER_TIME);
        $intercept_date = $row['mb_intercept_date'] ? $row['mb_intercept_date'] : date('Ymd', G5_SERVER_TIME);

        $mb_nick = get_sideview($row['mb_id'], get_text($row['mb_nick']), $row['mb_email'], $row['mb_homepage']);

        $mb_id = $row['mb_id'];
        $leave_msg = '';
        $intercept_msg = '';
        $intercept_title = '';
        if ($row['mb_leave_date']) {
            $mb_id = $mb_id;
            $leave_msg = '<span class="mb_leave_msg">탈퇴함</span>';
        }
        else if ($row['mb_intercept_date']) {
            $mb_id = $mb_id;
            $intercept_msg = '<span class="mb_intercept_msg">차단됨</span>';
            $intercept_title = '차단해제';
        }
        if ($intercept_title == '')
            $intercept_title = '차단하기';

        //$address = $row['mb_zip1'] ? print_address($row['mb_addr1'], $row['mb_addr2'], $row['mb_addr3'], $row['mb_addr_jibeon']) : '';

        $bg = 'bg'.($i%2);
		
		$mb_id           = $row["mb_id"];
		//$mb_pass         = $row["mb_pass"];
		$mb_name         = $row["mb_name"];
		$mb_tel          = $row["mb_tel"];
		$pharm_name   = $row["pharm_name"];
		//$mb_pharm_number = $row["mb_pharm_number"];
		//$visitdate        = $row["visitdate"];
		//$regdate_str      = date( 'y-m-d', strtotime( $row["regdate"] ) );
		$sales_name       = $row["sales_name"];
		$mb_status       = $row["pharm_status"];
		$user_cnt         = $row["user_cnt"];
		$user_cnt2        = $row["user_cnt2"];
		$pharm_custno       = $row["pharm_custno"];
		$pharm_div          = $row["pharm_div"];
		$mb_pay_yn          = $row["mb_pay_yn"];
		$pharm_sido         = $row["pharm_sido"];
		$pharm_gugun        = $row["pharm_gugun"];
		$pharm_dong         = $row["pharm_dong"];
		$pharm_look         = $row["pharm_look"];
		$pharm_lat          = $row["pharm_lat"];
		$pharm_lng          = $row["pharm_lng"];

		$mb_div_str      = "";

		switch ( $pharm_div ) {
			case "1":
				$pharm_div_str = "태전약품";
				break;
			case "2":
				$pharm_div_str = "티제이팜";
				break;
		}

		$mb_pay_yn_str = "";

		switch ( $mb_pay_yn ) {
			case "Y" :
				$mb_pay_yn_str = "오더스테이션결제";
				break;
			case "N" :
				$mb_pay_yn_str = "직거래";
				break;
		}

		$mb_status_str = "";

		switch ( $mb_status ) {
			case "1" :
				$mb_status_str = "사용(대기)";
				break;
			case "2" :
				$mb_status_str = "사용(승인)";
				break;
			case "3" :
				$mb_status_str = "사용안함";
				break;
			case "n" :
				$mb_status_str = "신규가입";
				break;
			case "l" :
				$mb_status_str = "erp등록중";
				break;
		}

		//$mb_tel = callnumbercheck( $mb_tel );

		switch ( $pharm_look ) {
			case "0" :
				$pharm_look_str = "미선택";
				break;
			case "1" :
				$pharm_look_str = "지역";
				break;
			case "2" :
				$pharm_look_str = "전체";
				break;
		}

		$map_str = "등록";

		if ( $map_lat == "" ) {
			$map_str = "미등록";
		}

		//$here = "";

		///if ( $_request["here"] == $mb_id ) {
		//	$here = "<font color=\"#ff0000\">→</font>";
		//}

		$mb_control = $row["pharm_control"];
    ?>

    <tr class="<?php echo $bg; ?>">
        
        <td ></td>
		
		<td align="center"
			<?php if ( $mb_control == "Y" ) { ?> style="color:#f00" <?php } ?>><?php echo $pharm_name; ?>(<?php echo $row["pharm_custno"];?>)</td>
		
		<td align="center"><?php echo $mb_pay_yn_str; ?></td>
		<td align="center"><?php echo $pharm_div_str; ?></td>
		<td align="center"><?php echo $pharm_custno; ?></td>
		<td align="center"><?php echo $mb_id; ?></td>
		<td align="center"><?php echo $mb_name; ?></td>
		<td align="center"><?php echo $mb_tel; ?></td>		
		<td align="center"><?php echo $mb_status_str; ?></td>
        
		<td width="150">
			<?php echo $s_mod ?>
			<?php if($row['mb_type'] > 0) {?>
			<a href="javascript:fnLogin('<?php echo $row['mb_id'];?>');" class="btn btn_02">Login</a>
			<?php } ?>
		</td>

	</tr>

    <?php
    }
    if ($i == 0)
        echo "<tr><td colspan=\"".$colspan."\" class=\"empty_table\">자료가 없습니다.</td></tr>";
    ?>
    </tbody>
    </table>
</div>


</form>

<?php 
$qstr ='m=1&sales_id='.$_GET['sales_id'].'&'.$qstr;
echo get_paging(G5_IS_MOBILE ? $config['cf_mobile_pages'] : $config['cf_write_pages'], $page, $total_page, "{$_SERVER['SCRIPT_NAME']}?$qstr&amp;page="); ?>
<script>
function fmemberlist_submit(f)
{
    if (!is_checked("chk[]")) {
        alert(document.pressed+" 하실 항목을 하나 이상 선택하세요.");
        return false;
    }

    if(document.pressed == "선택삭제") {
        if(!confirm("선택한 자료를 정말 삭제하시겠습니까?")) {
            return false;
        }
    }

    return true;
}
function fnLogin(id) {
	if(confirm(id+" 로 로그인하시겠습니까?")) {
		location.href='/bbs/login_check_admin.php?mb_id=' + id;
	}
}
</script>

<?php
include_once ('./admin.tail.php');
?>

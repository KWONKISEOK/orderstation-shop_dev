<?php
if (!defined('_GNUBOARD_')) exit;

/*
// 081022 : CSRF 방지를 위해 코드를 작성했으나 효과가 없어 주석처리 함
if (!get_session('ss_admin')) {
    set_session('ss_admin', true);
    goto_url('.');
}
*/

// 스킨디렉토리를 SELECT 형식으로 얻음
function get_skin_select($skin_gubun, $id, $name, $selected='', $event='')
{
    global $config;

    $skins = array();

    if(defined('G5_THEME_PATH') && $config['cf_theme']) {
        $dirs = get_skin_dir($skin_gubun, G5_THEME_PATH.'/'.G5_SKIN_DIR);
        if(!empty($dirs)) {
            foreach($dirs as $dir) {
                $skins[] = 'theme/'.$dir;
            }
        }
    }

    $skins = array_merge($skins, get_skin_dir($skin_gubun));

    $str = "<select id=\"$id\" name=\"$name\" $event>\n";
    for ($i=0; $i<count($skins); $i++) {
        if ($i == 0) $str .= "<option value=\"\">선택</option>";
        if(preg_match('#^theme/(.+)$#', $skins[$i], $match))
            $text = '(테마) '.$match[1];
        else
            $text = $skins[$i];

        $str .= option_selected($skins[$i], $selected, $text);
    }
    $str .= "</select>";
    return $str;
}

// 모바일 스킨디렉토리를 SELECT 형식으로 얻음
function get_mobile_skin_select($skin_gubun, $id, $name, $selected='', $event='')
{
    global $config;

    $skins = array();

    if(defined('G5_THEME_PATH') && $config['cf_theme']) {
        $dirs = get_skin_dir($skin_gubun, G5_THEME_MOBILE_PATH.'/'.G5_SKIN_DIR);
        if(!empty($dirs)) {
            foreach($dirs as $dir) {
                $skins[] = 'theme/'.$dir;
            }
        }
    }

    $skins = array_merge($skins, get_skin_dir($skin_gubun, G5_MOBILE_PATH.'/'.G5_SKIN_DIR));

    $str = "<select id=\"$id\" name=\"$name\" $event>\n";
    for ($i=0; $i<count($skins); $i++) {
        if ($i == 0) $str .= "<option value=\"\">선택</option>";
        if(preg_match('#^theme/(.+)$#', $skins[$i], $match))
            $text = '(테마) '.$match[1];
        else
            $text = $skins[$i];

        $str .= option_selected($skins[$i], $selected, $text);
    }
    $str .= "</select>";
    return $str;
}


// 스킨경로를 얻는다
function get_skin_dir($skin, $skin_path=G5_SKIN_PATH)
{
    global $g5;

    $result_array = array();

    $dirname = $skin_path.'/'.$skin.'/';
    if(!is_dir($dirname))
        return;

    $handle = opendir($dirname);
    while ($file = readdir($handle)) {
        if($file == '.'||$file == '..') continue;

        if (is_dir($dirname.$file)) $result_array[] = $file;
    }
    closedir($handle);
    sort($result_array);

    return $result_array;
}


// 테마
function get_theme_dir()
{
    $result_array = array();

    $dirname = G5_PATH.'/'.G5_THEME_DIR.'/';
    $handle = opendir($dirname);
    while ($file = readdir($handle)) {
        if($file == '.'||$file == '..') continue;

        if (is_dir($dirname.$file)) {
            $theme_path = $dirname.$file;
            if(is_file($theme_path.'/index.php') && is_file($theme_path.'/head.php') && is_file($theme_path.'/tail.php'))
                $result_array[] = $file;
        }
    }
    closedir($handle);
    natsort($result_array);

    return $result_array;
}


// 테마정보
function get_theme_info($dir)
{
    $info = array();
    $path = G5_PATH.'/'.G5_THEME_DIR.'/'.$dir;

    if(is_dir($path)) {
        $screenshot = $path.'/screenshot.png';
        if(is_file($screenshot)) {
            $size = @getimagesize($screenshot);

            if($size[2] == 3)
                $screenshot_url = str_replace(G5_PATH, G5_URL, $screenshot);
        }

        $info['screenshot'] = $screenshot_url;

        $text = $path.'/readme.txt';
        if(is_file($text)) {
            $content = file($text, false);
            $content = array_map('trim', $content);

            preg_match('#^Theme Name:(.+)$#i', $content[0], $m0);
            preg_match('#^Theme URI:(.+)$#i', $content[1], $m1);
            preg_match('#^Maker:(.+)$#i', $content[2], $m2);
            preg_match('#^Maker URI:(.+)$#i', $content[3], $m3);
            preg_match('#^Version:(.+)$#i', $content[4], $m4);
            preg_match('#^Detail:(.+)$#i', $content[5], $m5);
            preg_match('#^License:(.+)$#i', $content[6], $m6);
            preg_match('#^License URI:(.+)$#i', $content[7], $m7);

            $info['theme_name'] = trim($m0[1]);
            $info['theme_uri'] = trim($m1[1]);
            $info['maker'] = trim($m2[1]);
            $info['maker_uri'] = trim($m3[1]);
            $info['version'] = trim($m4[1]);
            $info['detail'] = trim($m5[1]);
            $info['license'] = trim($m6[1]);
            $info['license_uri'] = trim($m7[1]);
        }

        if(!$info['theme_name'])
            $info['theme_name'] = $dir;
    }

    return $info;
}


// 테마설정 정보
function get_theme_config_value($dir, $key='*')
{
    $tconfig = array();

    $theme_config_file = G5_PATH.'/'.G5_THEME_DIR.'/'.$dir.'/theme.config.php';
    if(is_file($theme_config_file)) {
        include($theme_config_file);

        if($key == '*') {
            $tconfig = $theme_config;
        } else {
            $keys = array_map('trim', explode(',', $key));
            foreach($keys as $v) {
                $tconfig[$v] = trim($theme_config[$v]);
            }
        }
    }

    return $tconfig;
}


// 회원권한을 SELECT 형식으로 얻음
function get_member_level_select($name, $start_id=0, $end_id=10, $selected="", $event="")
{
    global $g5;

    $str = "\n<select id=\"{$name}\" name=\"{$name}\"";
    if ($event) $str .= " $event";
    $str .= ">\n";
    for ($i=$start_id; $i<=$end_id; $i++) {
        $str .= '<option value="'.$i.'"';
        if ($i == $selected)
            $str .= ' selected="selected"';
        $str .= ">{$i}</option>\n";
    }
    $str .= "</select>\n";
    return $str;
}


// 회원아이디를 SELECT 형식으로 얻음
function get_member_id_select($name, $level, $selected="", $event="")
{
    global $g5;

    $sql = " select mb_id from {$g5['member_table']} where mb_level >= '{$level}' ";
    $result = sql_query($sql);
    $str = '<select id="'.$name.'" name="'.$name.'" '.$event.'><option value="">선택안함</option>';
    for ($i=0; $row=sql_fetch_array($result); $i++)
    {
        $str .= '<option value="'.$row['mb_id'].'"';
        if ($row['mb_id'] == $selected) $str .= ' selected';
        $str .= '>'.$row['mb_id'].'</option>';
    }
    $str .= '</select>';
    return $str;
}

// 권한 검사
function auth_check($auth, $attr, $return=false)
{
    global $is_admin;

    if ($is_admin == 'super') return;
	if ($is_admin == 'vender') return;
	if ($is_admin == 'sales') return;

    if (!trim($auth)) {
        $msg = '이 메뉴에는 접근 권한이 없습니다.\\n\\n접근 권한은 최고관리자만 부여할 수 있습니다.';
        if($return)
            return $msg;
        else
            alert($msg, G5_URL.'/index.php?url=' . urlencode(G5_ADMIN_URL));
    }

    $attr = strtolower($attr);

    if (!strstr($auth, $attr)) {
        if ($attr == 'r') {
            $msg = '읽을 권한이 없습니다.';
            if($return)
                return $msg;
            else
                alert($msg);
        } else if ($attr == 'w') {
            $msg = '입력, 추가, 생성, 수정 권한이 없습니다.';
            if($return)
                return $msg;
            else
                alert($msg);
        } else if ($attr == 'd') {
            $msg = '삭제 권한이 없습니다.';
            if($return)
                return $msg;
            else
                alert($msg);
        } else {
            $msg = '속성이 잘못 되었습니다.';
            if($return)
                return $msg;
            else
                alert($msg);
        }
    }
}


// 작업아이콘 출력
function icon($act, $link='', $target='_parent')
{
    global $g5;

    $img = array('입력'=>'insert', '추가'=>'insert', '생성'=>'insert', '수정'=>'modify', '삭제'=>'delete', '이동'=>'move', '그룹'=>'move', '보기'=>'view', '미리보기'=>'view', '복사'=>'copy');
    $icon = '<img src="'.G5_ADMIN_PATH.'/img/icon_'.$img[$act].'.gif" title="'.$act.'">';
    if ($link)
        $s = '<a href="'.$link.'">'.$icon.'</a>';
    else
        $s = $icon;
    return $s;
}


// rm -rf 옵션 : exec(), system() 함수를 사용할 수 없는 서버 또는 win32용 대체
// www.php.net 참고 : pal at degerstrom dot com
function rm_rf($file)
{
    if (file_exists($file)) {
        if (is_dir($file)) {
            $handle = opendir($file);
            while($filename = readdir($handle)) {
                if ($filename != '.' && $filename != '..')
                    rm_rf($file.'/'.$filename);
            }
            closedir($handle);

            @chmod($file, G5_DIR_PERMISSION);
            @rmdir($file);
        } else {
            @chmod($file, G5_FILE_PERMISSION);
            @unlink($file);
        }
    }
}

// 입력 폼 안내문
function help($help="")
{
    global $g5;

    $str  = '<span class="frm_info">'.str_replace("\n", "<br>", $help).'</span>';

    return $str;
}

// 출력순서
function order_select($fld, $sel='')
{
    $s = '<select name="'.$fld.'" id="'.$fld.'">';
    for ($i=1; $i<=100; $i++) {
        $s .= '<option value="'.$i.'" ';
        if ($sel) {
            if ($i == $sel) {
                $s .= 'selected';
            }
        } else {
            if ($i == 50) {
                $s .= 'selected';
            }
        }
        $s .= '>'.$i.'</option>';
    }
    $s .= '</select>';

    return $s;
}

// 불법접근을 막도록 토큰을 생성하면서 토큰값을 리턴
function get_admin_token()
{
    $token = md5(uniqid(rand(), true));
    set_session('ss_admin_token', $token);

    return $token;
}

// 관리자가 자동등록방지를 사용해야 할 경우
function get_admin_captcha_by($type='get'){
    
    $captcha_name = 'ss_admin_use_captcha';

    if($type === 'remove'){
        set_session($captcha_name, '');
    }

    return get_session($captcha_name);
}

//input value 에서 xss 공격 filter 역할을 함 ( 반드시 input value='' 타입에만 사용할것 )
function get_sanitize_input($s, $is_html=false){

    if(!$is_html){
        $s = strip_tags($s);
    }

    $s = htmlspecialchars($s, ENT_QUOTES, 'utf-8');

    return $s;
}

// POST로 넘어온 토큰과 세션에 저장된 토큰 비교
function check_admin_token()
{
    $token = get_session('ss_admin_token');
    set_session('ss_admin_token', '');

    if(!$token || !$_REQUEST['token'] || $token != $_REQUEST['token'])
        alert('올바른 방법으로 이용해 주십시오.', G5_URL);

    return true;
}

// 관리자 페이지 referer 체크
function admin_referer_check($return=false)
{
    $referer = trim($_SERVER['HTTP_REFERER']);
    if(!$referer) {
        $msg = '정보가 올바르지 않습니다.';

        if($return)
            return $msg;
        else
            alert($msg, G5_URL);
    }

    $p = @parse_url($referer);

    $host = preg_replace('/:[0-9]+$/', '', $_SERVER['HTTP_HOST']);
    $msg = '';

    if($host != $p['host']) {
        $msg = '올바른 방법으로 이용해 주십시오.';
    }

    if( $p['path'] && ! preg_match( '/\/'.preg_quote(G5_ADMIN_DIR).'\//i', $p['path'] ) ){
        $msg = '올바른 방법으로 이용해 주십시오';
    }

    if( $msg ){
        if($return) {
            return $msg;
        } else {
            alert($msg, G5_URL);
        }
    }
}

// 접근 권한 검사
if (!$member['mb_id'])
{
    alert('로그인 하십시오.', G5_BBS_URL.'/login.php?url=' . urlencode(G5_ADMIN_URL));
}
else if ($is_admin != 'super' and $is_admin != 'vender' and $is_admin != 'sales')
{
    $auth = array();
    $sql = " select au_menu, au_auth from {$g5['auth_table']} where mb_id = '{$member['mb_id']}' ";
    $result = sql_query($sql);
    for($i=0; $row=sql_fetch_array($result); $i++)
    {
        $auth[$row['au_menu']] = $row['au_auth'];
    }

	//echo '1111111111111'.$is_admin;



    if (!$i)
    {
        //alert('최고관리자 또는 관리권한이 있는 회원만 접근 가능합니다.', G5_URL);
    }
}
function ViewBank( $code ) {
	$arrBank = array(
		"02" => "한국산업은행",
		"03" => "기업은행",
		"04" => "국민은행",
		"05" => "외환은행",
		"06" => "국민은행(구 주택은행)",
		"07" => "수협중앙회",
		"11" => "농협중앙회",
		"12" => "단위농협",
		"16" => "축협중앙회",
		"20" => "우리은행",
		"21" => "구)조흥은행",
		"22" => "상업은행",
		"23" => "제일은행",
		"24" => "한일은행",
		"25" => "서울은행",
		"26" => "구)신한은행",
		"27" => "한미은행",
		"31" => "대구은행",
		"32" => "부산은행",
		"34" => "광주은행",
		"35" => "제주은행",
		"37" => "전북은행",
		"38" => "강원은행",
		"39" => "경남은행",
		"41" => "비씨카드",
		"45" => "새마을금고",
		"48" => "신용협동조합중앙회",
		"50" => "상호저축은행",
		"53" => "씨티은행",
		"54" => "홍콩상하이은행",
		"55" => "도이치은행",
		"56" => "ABN 암로",
		"70" => "신안상호저축은행",
		"71" => "우체국",
		"81" => "하나은행",
		"83" => "평화은행",
		"87" => "신세계",
		"88" => "신한(통합)은행"
	);

	return $arrBank[ $code ];
}
/**
 * 자리수 추가
 */
function addString( $str ) {
	$strRlt = $str;

	if ( strlen( $str ) == 1 ) {
		$strRlt = "0" . $str;
	} elseif ( strlen( $str ) == 0 ) {
		$strRlt = "00";
	}

	return $strRlt;
}
function year( $date ) {
	// 20180310
	return substr( $date, 0, 4 );
}

function month( $date ) {
	// 20180310
	return substr( $date, 4, 2 );
}

function day( $date ) {
	// 20180310
	return substr( $date, 6, 2 );
}
function DateFormat( $strDate, $mode = null ) {
	if ( $strDate != "" ) {
		if ( ! $mode) {
			return date( 'Y-m-d', strtotime( $strDate ) );
		} elseif ( $mode == "M" ) {
			return date( 'Y-m-d H:i:s', strtotime( $strDate ) );
		}

	} else {
		return "";
	}
}


/*
// 관리자의 아이피, 브라우저와 다르다면 세션을 끊고 관리자에게 메일을 보낸다.
$admin_key = md5($member['mb_datetime'] . get_real_client_ip() . $_SERVER['HTTP_USER_AGENT']);
if (get_session('ss_mb_key') !== $admin_key) {

    session_destroy();

    include_once(G5_LIB_PATH.'/mailer.lib.php');
    // 메일 알림
    mailer($member['mb_nick'], $member['mb_email'], $member['mb_email'], 'XSS 공격 알림', $_SERVER['REMOTE_ADDR'].' 아이피로 XSS 공격이 있었습니다.<br><br>관리자 권한을 탈취하려는 접근이므로 주의하시기 바랍니다.<br><br>해당 아이피는 차단하시고 의심되는 게시물이 있는지 확인하시기 바랍니다.'.G5_URL, 0);

    alert_close('정상적으로 로그인하여 접근하시기 바랍니다.');
}
*/

@ksort($auth);

// 가변 메뉴
unset($auth_menu);
unset($menu);
unset($amenu);
$tmp = dir(G5_ADMIN_PATH);
$menu_files = array();

while ($entry = $tmp->read()) {

	if($is_admin == 'super') {
		if (!preg_match('/^admin.menu([0-9]{3}).*\.php$/', $entry, $m))
			continue;  // 파일명이 menu 으로 시작하지 않으면 무시한다.
	}
	
	if($is_admin == 'vender') {
		if (!preg_match('/^admin.vender.php$/', $entry, $m))
			continue;  // 파일명이 menu 으로 시작하지 않으면 무시한다.
	}
	if($is_admin == 'sales') {
		if (!preg_match('/^admin.vender.php$/', $entry, $m))
			continue;  // 파일명이 menu 으로 시작하지 않으면 무시한다.
	}

	if($is_admin == '') {
		if (!preg_match('/^admin.menu([0-9]{3}).*\.php$/', $entry, $m))
			continue;  // 파일명이 menu 으로 시작하지 않으면 무시한다.
	}
	
    $amenu[$m[1]] = $entry;
    $menu_files[] = G5_ADMIN_PATH.'/'.$entry;

	//print_r( $amenu);
}
@asort($menu_files);
foreach($menu_files as $file){
    include_once($file);
}
@ksort($amenu);

$arr_query = array();
if (isset($sst))  $arr_query[] = 'sst='.$sst;
if (isset($sod))  $arr_query[] = 'sod='.$sod;
if (isset($sfl))  $arr_query[] = 'sfl='.$sfl;
if (isset($stx))  $arr_query[] = 'stx='.$stx;
if (isset($page)) $arr_query[] = 'page='.$page;
$qstr = implode("&amp;", $arr_query);

// 관리자에서는 추가 스크립트는 사용하지 않는다.
//$config['cf_add_script'] = '';


//오늘날짜가 몇주째 날짜인지 가지고 오기
//기준점 날짜는 매 주차별 수요일 ~ 화요일
// 1 -> 해당주차 날짜 리턴 , 2 -> 날짜사이간격 리턴
function GetWeekInfo( $GetParam ){

	// 년과 월은 변수
	$yyyy = year(date( "Ymd" ));
	$mm = (int)month(date( "Ymd" ));

	// 운년체크.
	$endday = array(1=>31, 28, 31, 30 , 31, 30, 31, 31, 30 ,31 ,30, 31);
	// 윤년 계산 부분이다. 4년에 한번꼴로 2월이 28일이 아닌 29일이 있다.
	if( $yyyy%4 == 0 && $yyyy%100 != 0 || $yyyy%400 == 0 ) {
		$endday[2] = 29; // 조건에 적합할 경우 28을 29로 변경
	}

	//mktime(시, 분, 초, 월, 일, 년)
	$m_start = mktime(0, 0, 0, $mm, 1, $yyyy);
	$m_end = mktime(0, 0, 0, $mm, $endday[$mm], $yyyy);

	$week = array();
	$weeks = $m_start;

	$today =  date("Y-m-d");

	//기준 : 매 주차별 수요일 ~ 화요일
	for($i = 0; date("n", $weeks) <= date("n", $m_end); $i++) {

		$wk = date("w", $weeks);
		$week_first = $weeks-($wk*86400)+( 86400 * 3 ); //수요일
		$week_last = $week_first+(6*86400); //화요일

		$m_week = date("Y-m-d",$week_first)."|".date("Y-m-d",$week_last);

		if( date("Y-m-d",$week_first) <= $today && date("Y-m-d",$week_last) >= $today ){

			$date1 = new DateTime(date("Y-m-d",$week_first));
			$date2 = new DateTime(date("Y-m-d",$week_last));
			$date3 = date_diff($date1, $date2);
			$datediff = $date3->days;
			
			if( $GetParam == "1" ){
				return $m_week;			
			}else if( $GetParam == "2" ){
				return $datediff;			
			}

			break;
		}

		$weeks = $week_first + (86400 * 7);

        if ($i == 3) return GetWeekInfo_new($GetParam);
	}

}

function GetWeekInfo_new( $GetParam ){

    // 년과 월은 변수
    $yyyy = year(date( "Ymd" ));
    $mm = (int)month(date( "Ymd" ));

    // 운년체크.
    $endday = array(1=>31, 28, 31, 30 , 31, 30, 31, 31, 30 ,31 ,30, 31);
    // 윤년 계산 부분이다. 4년에 한번꼴로 2월이 28일이 아닌 29일이 있다.
    if( $yyyy%4 == 0 && $yyyy%100 != 0 || $yyyy%400 == 0 ) {
        $endday[2] = 29; // 조건에 적합할 경우 28을 29로 변경
    }

    //mktime(시, 분, 초, 월, 일, 년)
    $m_start = mktime(0, 0, 0, $mm, 1, $yyyy);
    $m_end = mktime(0, 0, 0, $mm, $endday[$mm], $yyyy);

    $week = array();
    $weeks = $m_start;

    $today =  date("Y-m-d");

    //기준 : 매 주차별 수요일 ~ 화요일
    $wk = date("w", $weeks);
    $week_first = $weeks-($wk*86400)+( 86400 * 3 ); //수요일
    $week_last = $week_first+(6*86400); //화요일

    $m_week = date("Y-m-d",$week_first)."|".date("Y-m-d",$week_last);

    $date1 = new DateTime(date("Y-m-d",$week_first));
    $date2 = new DateTime(date("Y-m-d",$week_last));
    $date3 = date_diff($date1, $date2);
    $datediff = $date3->days;

    if( $GetParam == "1" ){
        return $m_week;
    }else if( $GetParam == "2" ){
        return $datediff;
    }

}

function GetDateDiffInfo( $GetParam1 , $GetParam2 ){

	$date1 = new DateTime($GetParam1);
	$date2 = new DateTime($GetParam2);
	$date3 = date_diff($date1, $date2);
	$datediff = $date3->days;

	return $datediff;
}

function TransServiceName($GetValue){

	$Msg = "";

	if( $GetValue == "B" ){
		$Msg = "이즈브레앱";		
	}else{
		$Msg = "오더스테이션";			
	}

	return $Msg;

}

function get_pharm_list($sel) {
    $str = '';
    $sql = "select pharm_name, pharm_custno, mb_id
          from tbl_member where mb_type=1 and pharm_custno != '' ";

    $res = sql_query($sql);

    while($row = sql_fetch_array($res)) {
        $str = $str."<option value='".$row['mb_id']."'";
        if($sel ==$row['pharm_name']) $str = $str."selected";
        $str = $str.">";
        $str = $str.$row['pharm_name']."(".$row['pharm_custno'].")</option>";
    }
    return $str;
}

// 관리자 권한 확인
function adm_auth_check($menu_id, $admin_id) {

    global $g5;

    $sql_common = " from {$g5['auth_table']} a left join {$g5['member_table']} b on (a.mb_id=b.mb_id) ";
    $sql_search = " where a.mb_id = '$admin_id'and a.au_menu like '$menu_id%'";

    $sql = "select a.au_auth
             {$sql_common}
             {$sql_search}";

    $result = sql_fetch_result($sql);

    $flag = false;
    foreach($result as $key => $row) {
        if ($flag == false && strlen($row['au_auth']) > 0) {
            $flag = true;
            if ($row['au_auth'] == "r,w,d") {
                $flag = "b";
            }
        }
    }
    return $flag;
}

// 관리자 메인 접근 권한
function adm_main($mb_id){
    global $g5, $is_admin;

    $sql_common = " from {$g5['auth_table']}";
    $sql_search = " where mb_id = '$mb_id'";

    $sql = "select mb_id
             {$sql_common}
             {$sql_search}";

    $result = sql_fetch_result($sql);

    $flag = false;

    if($is_admin == 'super' || $is_admin == 'vender' || $is_admin == 'sales'){
        $flag = true;
    }

    foreach ($result as $row){
        if(strlen($row['mb_id'])>0){
            $flag = true;
        }
    }
    return $flag;
}
?>
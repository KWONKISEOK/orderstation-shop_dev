<?php
$sub_menu = "200100";
include_once('./_common.php');

auth_check($auth[$sub_menu], 'w');

$mb_id = $member[mb_id];

$mb = get_member($mb_id);
if (!$mb['mb_id'])
    alert('존재하지 않는 회원자료입니다.');

$required_mb_id = 'readonly';
$required_mb_password = '';
$html_title = '수정';

$mb['mb_name'] = get_text($mb['mb_name']);
$mb['mb_nick'] = get_text($mb['mb_nick']);
$mb['mb_email'] = get_text($mb['mb_email']);
$mb['mb_homepage'] = get_text($mb['mb_homepage']);
$mb['mb_birth'] = get_text($mb['mb_birth']);
$mb['mb_tel'] = get_text($mb['mb_tel']);
$mb['mb_hp'] = get_text($mb['mb_hp']);
$mb['mb_addr1'] = get_text($mb['mb_addr1']);
$mb['mb_addr2'] = get_text($mb['mb_addr2']);
$mb['mb_addr3'] = get_text($mb['mb_addr3']);
$mb['mb_signature'] = get_text($mb['mb_signature']);
$mb['mb_recommend'] = get_text($mb['mb_recommend']);
$mb['mb_profile'] = get_text($mb['mb_profile']);


$comp_trans_compare = get_text($mb['comp_trans_compare']);
$comp_trans_price = get_text($mb['comp_trans_price']);
$comp_trans_use = get_text($mb['comp_trans_use']);


// 본인확인방법
switch ($mb['mb_certify']) {
    case 'hp':
        $mb_certify_case = '휴대폰';
        $mb_certify_val = 'hp';
        break;
    case 'ipin':
        $mb_certify_case = '아이핀';
        $mb_certify_val = 'ipin';
        break;
    case 'admin':
        $mb_certify_case = '관리자 수정';
        $mb_certify_val = 'admin';
        break;
    default:
        $mb_certify_case = '';
        $mb_certify_val = 'admin';
        break;
}

// 본인확인
$mb_certify_yes = $mb['mb_certify'] ? 'checked="checked"' : '';
$mb_certify_no = !$mb['mb_certify'] ? 'checked="checked"' : '';

// 성인인증
$mb_adult_yes = $mb['mb_adult'] ? 'checked="checked"' : '';
$mb_adult_no = !$mb['mb_adult'] ? 'checked="checked"' : '';

//메일수신
$mb_mailling_yes = $mb['mb_mailling'] ? 'checked="checked"' : '';
$mb_mailling_no = !$mb['mb_mailling'] ? 'checked="checked"' : '';

// SMS 수신
$mb_sms_yes = $mb['mb_sms'] ? 'checked="checked"' : '';
$mb_sms_no = !$mb['mb_sms'] ? 'checked="checked"' : '';

// 정보 공개
$mb_open_yes = $mb['mb_open'] ? 'checked="checked"' : '';
$mb_open_no = !$mb['mb_open'] ? 'checked="checked"' : '';


if ($mb['mb_intercept_date']) $g5['title'] = "차단된 ";
else $g5['title'] .= "";
$g5['title'] .= '회원 ' . $html_title;
include_once('./admin.head.php');

// add_javascript('js 구문', 출력순서); 숫자가 작을 수록 먼저 출력됨
add_javascript(G5_POSTCODE_JS, 0);    //다음 주소 js
?>
<script type="text/javascript"
        src="https://openapi.map.naver.com/openapi/v3/maps.js?clientId=iQB4Bbn7JFBS2Qb0UVSY&submodules=geocoder"></script>


<div class="tbl_frm01 tbl_wrap">

    <form name="fmember" id="fmember" action="./vender_form_update.php" onsubmit="return fmember_submit(this);"
          method="post" enctype="multipart/form-data">
        <input type="hidden" name="w" value="<?php echo $w ?>">
        <input type="hidden" name="sfl" value="<?php echo $sfl ?>">
        <input type="hidden" name="stx" value="<?php echo $stx ?>">
        <input type="hidden" name="sst" value="<?php echo $sst ?>">
        <input type="hidden" name="sod" value="<?php echo $sod ?>">
        <input type="hidden" name="page" value="<?php echo $page ?>">
        <input type="hidden" name="token" value="">
        <input type="hidden" name="mb_type" value="7">

        <table>
            <caption><?php echo $g5['title']; ?></caption>
            <colgroup>
                <col class="grid_4">
                <col>
                <col class="grid_4">
                <col>
            </colgroup>
            <tbody>
            <? if ($mb['mb_type'] == 7) { ?>
                <tr style="display:none;">
                    <th scope="row"><label for="mb_id">묶음 상품 배송비 상한가<?php echo $sound_only ?></label></th>
                    <td colspan="3">
                        <input type="text" name="comp_trans_compare" value="<?= $comp_trans_compare ?>"
                               class="required frm_input"/>&nbsp;이상구매시&nbsp;
                        <input type="text" name="comp_trans_price" value="<?= $comp_trans_price ?>"
                               class="required frm_input"/>&nbsp;원 배송비 적용&nbsp;
                        <input type="radio" name="comp_trans_use"
                               value="Y" <?php echo get_checked($comp_trans_use, "Y"); ?>>사용
                        <input type="radio" name="comp_trans_use"
                               value="N" <?php echo get_checked($comp_trans_use, "N"); ?>>미사용
                    </td>
                </tr>
            <? } ?>
            <tr>
                <th scope="row"><label for="mb_id">아이디<?php echo $sound_only ?></label></th>
                <td>
                    <input type="text" name="mb_id" value="<?php echo $mb['mb_id'] ?>"
                           id="mb_id" <?php echo $required_mb_id ?>
                           class="frm_input <?php echo $required_mb_id_class ?>" size="15" maxlength="20">
                </td>
                <th scope="row"><label for="mb_password">비밀번호<?php echo $sound_only ?></label></th>
                <td><input type="password" name="mb_password" id="mb_password" <?php echo $required_mb_password ?>
                           class="frm_input <?php echo $required_mb_password ?>" size="15" maxlength="20"></td>
            </tr>
            <tr>
                <th scope="row"><label for="mb_name">이름(실명)<strong class="sound_only">필수</strong></label></th>
                <td><input type="text" name="mb_name" value="<?php echo $mb['mb_name'] ?>" id="mb_name" required
                           class="required frm_input" size="15" maxlength="20"></td>
                <th scope="row"><label for="mb_nick">닉네임<strong class="sound_only">필수</strong></label></th>
                <td><input type="text" name="mb_nick" value="<?php echo $mb['mb_nick'] ?>" id="mb_nick" required
                           class="required frm_input" size="15" maxlength="20"></td>
            </tr>
            <tr>
                <th scope="row"><label for="mb_email">E-mail</label></th>
                <td><input type="text" name="mb_email" value="<?php echo $mb['mb_email'] ?>" id="mb_email"
                           maxlength="100" class=" frm_input email" size="30"></td>
                <th scope="row"><label for="mb_homepage">홈페이지</label></th>
                <td><input type="text" name="mb_homepage" value="<?php echo $mb['mb_homepage'] ?>" id="mb_homepage"
                           class="frm_input" maxlength="255" size="15"></td>
            </tr>
            <tr>
                <th scope="row"><label for="mb_hp">휴대폰번호</label></th>
                <td><input type="text" name="mb_hp" value="<?php echo $mb['mb_hp'] ?>" id="mb_hp" class="frm_input"
                           size="15" maxlength="20"></td>
                <th scope="row"><label for="mb_tel">전화번호</label></th>
                <td><input type="text" name="mb_tel" value="<?php echo $mb['mb_tel'] ?>" id="mb_tel" class="frm_input"
                           size="15" maxlength="20"></td>
            </tr>
            <tr>
                <th scope="row">주소</th>
                <td colspan="3" class="td_addr_line">
                    <label for="mb_zip" class="sound_only">우편번호</label>
                    <input type="text" name="mb_zip" value="<?php echo $mb['mb_zip1'] . $mb['mb_zip2']; ?>" id="mb_zip"
                           class="frm_input readonly" size="5" maxlength="6">
                    <button type="button" class="btn_frmline"
                            onclick="win_zip('fmember', 'mb_zip', 'mb_addr1', 'mb_addr2', 'mb_addr3', 'mb_addr_jibeon');">
                        주소 검색
                    </button>
                    <br>
                    <input type="text" name="mb_addr1" value="<?php echo $mb['mb_addr1'] ?>" id="mb_addr1"
                           class="frm_input readonly" size="60">
                    <label for="mb_addr1">기본주소</label><br>
                    <input type="text" name="mb_addr2" value="<?php echo $mb['mb_addr2'] ?>" id="mb_addr2"
                           class="frm_input" size="60">
                    <label for="mb_addr2">상세주소</label>
                    <br>
                    <input type="text" name="mb_addr3" value="<?php echo $mb['mb_addr3'] ?>" id="mb_addr3"
                           class="frm_input" size="60">
                    <label for="mb_addr3">참고항목</label>
                    <input type="hidden" name="mb_addr_jibeon" value="<?php echo $mb['mb_addr_jibeon']; ?>"><br>
                </td>
            </tr>
            <tr>
                <th scope="row">메일 수신</th>
                <td>
                    <input type="radio" name="mb_mailling" value="1"
                           id="mb_mailling_yes" <?php echo $mb_mailling_yes; ?>>
                    <label for="mb_mailling_yes">예</label>
                    <input type="radio" name="mb_mailling" value="0" id="mb_mailling_no" <?php echo $mb_mailling_no; ?>>
                    <label for="mb_mailling_no">아니오</label>
                </td>
                <th scope="row"><label for="mb_sms_yes">SMS 수신</label></th>
                <td>
                    <input type="radio" name="mb_sms" value="1" id="mb_sms_yes" <?php echo $mb_sms_yes; ?>>
                    <label for="mb_sms_yes">예</label>
                    <input type="radio" name="mb_sms" value="0" id="mb_sms_no" <?php echo $mb_sms_no; ?>>
                    <label for="mb_sms_no">아니오</label>
                </td>
            </tr>
            <tr>
                <th scope="row">정보 공개</th>
                <td colspan="3">
                    <input type="radio" name="mb_open" value="1" id="mb_open_yes" <?php echo $mb_open_yes; ?>>
                    <label for="mb_open_yes">예</label>
                    <input type="radio" name="mb_open" value="0" id="mb_open_no" <?php echo $mb_open_no; ?>>
                    <label for="mb_open_no">아니오</label>
                </td>
            </tr>

            <tr>
                <th scope="row"><label for="mb_memo">메모</label></th>
                <td colspan="3"><textarea name="mb_memo" id="mb_memo"><?php echo $mb['mb_memo'] ?></textarea></td>
            </tr>

            </tbody>
        </table>

        <h3 style="padding:10px;font-size:1.2em;">
            공급사 추가정보(관리자가 승인완료한 데이터)
            <? if (!empty($mb['comp_charge_datetime_2'])) { ?>
                승인완료일:<?= $mb['comp_charge_datetime_2'] ?>
            <? } ?>
        </h3>
        <table>
            <tr>
                <th scope="row">담당자정보</th>
                <td colspan='3'>
                    <br>
                    <table style="width:80%;" border="0">
                        <tr>
                            <th style="text-align:center;width:60px;">업무</th>
                            <th style="text-align:center;width:120px;">담당자명</th>
                            <th style="text-align:center;width:220px;">비고(직책)</th>
                            <th style="text-align:center;width:180px;">Tel</th>
                            <th style="text-align:center;width:180px;">HP</th>
                            <th style="text-align:center;width:180px;">이메일</th>
                            <th style="text-align:center;width:80px;"></th>
                        </tr>
                        <?
                        $tr_sql = " select * from tbl_charge_info where DelYn = 'N' and mb_id = '{$mb_id}' order by Charge_RegDate asc ";
                        $tr_result = sql_query($tr_sql);
                        $tr_result_cnt = sql_num_rows($tr_result);

                        if ($tr_result_cnt == 0) {

                            ?>
                            <tr>
                                <td colspan="7">
                                    <br>
                                    <center>데이터가 없습니다.</center>
                                    <br>
                                </td>
                            </tr>
                            <?

                        } else {

                            $i = 1;
                            while ($tr_row = mysqli_fetch_array($tr_result)) {

                                ?>
                                <tr>
                                    <td style="text-align:center;width:60px;">
                                        <?
                                        if ($tr_row["Charge_Task"] == "CS") { ?>
                                            CS
                                        <?
                                        } else if ($tr_row["Charge_Task"] == "영업") { ?>
                                            영업
                                        <?
                                        } else if ($tr_row["Charge_Task"] == "출고") { ?>
                                            출고
                                        <?
                                        } else if ($tr_row["Charge_Task"] == "정산") { ?>
                                            정산
                                        <?
                                        } ?>
                                    </td>
                                    <td style="text-align:center;">
                                        <?= $tr_row["Charge_Name"] ?>
                                    </td>
                                    <td style="text-align:center;">
                                        <?= $tr_row["Charge_Bigo"] ?>
                                    </td>
                                    <td style="text-align:center;">
                                        <?= $tr_row["Charge_Tel"] ?>
                                    </td>
                                    <td style="text-align:center;">
                                        <?= $tr_row["Charge_Hp"] ?>
                                    </td>
                                    <td style="text-align:center;">
                                        <?= $tr_row["Charge_Email"] ?>
                                    </td>
                                    <td style="text-align:center;">

                                    </td>
                                </tr>
                                <?
                                $i++;

                            }

                        }

                        ?>

                    </table>
                    <br>
                    <br>
                </td>
            </tr>
            <tr>
                <th scope="row"><font color="#ff0000"></font> 업체명
                </td>
                <td><?php echo $mb['comp_name']; ?></td>
                <th scope="row" style="display:none;"></th>
                <td style="display:none;"></td>
            </tr>
            <tr>
                <th scope="row"> 최소주문금액</th>
                <td><?php echo $mb['comp_limit']; ?></td>
                <th scope="row"> 브랜드명</th>
                <td><?php echo $mb['comp_brand']; ?></td>

            </tr>
            <tr>
                <th scope="row"> 대표자명</th>
                <td><?php echo $mb['comp_ceo_name']; ?></td>
                <th scope="row"> 대표전화번호</th>
                <td><?php echo $mb['comp_tel']; ?></td>
            </tr>
            <tr>
                <th scope="row"> 담당자명</th>
                <td><?php echo $mb['comp_md_name']; ?></td>
                <th scope="row"> 담당자연락처</th>
                <td><?php echo $mb['comp_md_tel']; ?></td>
            </tr>
            <tr>
                <th scope="row"> 주문시간</th>
                <td colspan="3"><?php echo nl2br($mb['comp_order_time']); ?></td>
            </tr>
            <tr>
                <th scope="row"> 배송시간</th>
                <td colspan="3"><?php echo nl2br($mb['comp_trans_time']); ?></td>
            </tr>
        </table>

        <div class="btn_fixed_top">
            <!-- <a href="javascript:history.go(-1);" class="btn btn_02">목록</a>-->
            <input type="submit" value="정보수정" class="btn_submit btn" accesskey='s'>
        </div>

    </form>


    <form name="frm2" method="post"/>

    <div id="wrap_list">

    </div>

    </form>

</div>


<script>
    fnMbType();

    function fnMbType() {
        var mb_id = "<?php echo $mb_id;?>";
        var mb_type = "7";
        var $wrap_list;
        $wrap_list = $("#wrap_list");

        //$("#preloader").show();
        $wrap_list.html("");
        $.ajax({
            type: "post",
            url: "vender_form_type.php",
            data: {mb_id: mb_id, mb_type: mb_type},
            dataType: "text",
            cache: false,
            success: function (resultText) {

                $wrap_list.html(resultText);

                //$("#preloader").hide();
            }
        });
    }


    //onChangeAddr(1,'');
    //setTimeout(function(){ onChangeAddr(2,'');}, 100);
    function onChangeAddr(step, val) {
        var sido = $("#pharm_sido");
        var gugun = $("#pharm_gugun");
        var dong = $("#pharm_dong");
        var obj = "";

        if (step == 1) {
            obj = gugun;
            gugun.empty().data('option');
            gugun.append("<option value=''>시/군/구</option>");
        }
        if (step == 2) {
            obj = dong;
        }
        dong.empty().data('option');
        dong.append("<option value=''>동/면/읍</option>");
        $.ajax({
            type: "post",
            dataType: "xml",
            url: "/addr.php",
            data: {"mem_sido": sido.val(), "mem_gugun": gugun.val(), "step": step},
            success: function (data) {
                var row = $(data).find("ROW").find("CELL");
                var size = row.length;
                for (var i = 0; i < size; i++) {
                    var cell = row.eq(i);
                    $("<option></option>").text(cell.find("NAME").text()).attr("value", cell.find("NAME").text()).appendTo(obj);
                }

                if (step == 1) {
                    gugun.val(val);
                }
                if (step == 2) {
                    dong.val(val);
                }
            },
            error: function (request, status, errorThrown) {
                //alert(errorThrown);
            }
        });
    }


    function fmember_submit(f) {

        return true;
    }

    function AddRow() {

        var tr_data = "<tr>";
        tr_data += "	<td style=\"text-align:center;width:60px;\">";
        tr_data += "		<input type=\"hidden\" name=\"Charge_Idx[]\" value=\"0\">";
        tr_data += "		<input type=\"hidden\" name=\"DelYn[]\" value=\"N\">";
        tr_data += "		<select name=\"Charge_Task[]\">";
        tr_data += "			<option value=\"CS\">CS</option>";
        tr_data += "			<option value=\"영업\">영업</option>";
        tr_data += "			<option value=\"출고\">출고</option>";
        tr_data += "			<option value=\"정산\">정산</option>";
        tr_data += "		</select>";
        tr_data += "	</td>";
        tr_data += "	<td style=\"text-align:center;\"><input type=\"text\" name=\"Charge_Name[]\" maxlength='50' class=\"frm_input\" style=\"width:120px;\"></td>";
        tr_data += "	<td style=\"text-align:center;\"><input type=\"text\" name=\"Charge_Bigo[]\" maxlength='100' class=\"frm_input\" style=\"width:220px;\"></td>";
        tr_data += "	<td style=\"text-align:center;\"><input type=\"text\" name=\"Charge_Tel[]\" maxlength='50' class=\"frm_input\" style=\"width:180px;\"></td>";
        tr_data += "	<td style=\"text-align:center;\"><input type=\"text\" name=\"Charge_Hp[]\" maxlength='50' class=\"frm_input\" style=\"width:180px;\"></td>";
        tr_data += "	<td style=\"text-align:center;\"><input type=\"text\" name=\"Charge_Email[]\" maxlength='50' class=\"frm_input\" style=\"width:180px;\"></td>";
        tr_data += "	<td style=\"text-align:center;\">";
        tr_data += "       <button type=\"button\" class=\"btn_submit btn\" onclick=\"javascript:DelRow(this);\">삭제</button>";
        tr_data += "	</td>";
        tr_data += "</tr>	";

        $('#group_table').append(tr_data);

    }

    function DelRow(gobj) {

        var tr = $(gobj).parent().parent();
        tr.find('input[name^=DelYn]').val("Y"); //개별삭제
        tr.css("display", "none");

    }

    function SendReg() {

        var frm = document.frm2;

        if (!confirm('승인요청하시겠습니까?')) {
            return false;
        }
        frm.target = "proc_frm";
        frm.method = "post";
        frm.action = "./Send_Proc.php";
        frm.submit();

    }

</script>

<div style="display:none;">
    <iframe src="" name="proc_frm" height="0"></iframe>
</div>

<?php
include_once('./admin.tail.php');
?>

<?php
$sub_menu = "200300";
include_once('./_common.php');

if ($w == 'u' || $w == 'd')
    check_demo();

auth_check($auth[$sub_menu], 'w');

check_admin_token();

$mail_id = $_POST['ma_id'];

if ($w == '') {
    $sql = " insert {$g5['mail_table']}
                set 
                     ma_subject = '{$_POST['ma_subject']}',
                     ma_content = '{$_POST['ma_content']}',
                     ma_time = '".G5_TIME_YMDHIS."',
                     ma_ip = '{$_SERVER['REMOTE_ADDR']}' ";
    sql_query($sql);
    $mail_id = sql_insert_id();
    $u = "추가";
} else if ($w == 'u') {
    $al_data_old = sql_fetch("select * from {$g5['mail_table']} where ma_id = '{$mail_id}'");
    $sql = " update {$g5['mail_table']}
                set ma_subject = '{$_POST['ma_subject']}',
                     ma_content = '{$_POST['ma_content']}',
                     ma_time = '".G5_TIME_YMDHIS."',
                     ma_ip = '{$_SERVER['REMOTE_ADDR']}'
                where ma_id = '{$mail_id}' ";
    sql_query($sql);
    $u = "수정";
}

/************* 관리자 로그 처리 START *************/
$al_data = sql_fetch("select * from {$g5['mail_table']} where ma_id = '{$mail_id}' ");
insert_admin_log(200, 200300, '메일 내용 ' . $u, '', $mail_id, '', $_SERVER['REQUEST_URI'], $al_data, $al_data_old);


goto_url('./mail_list.php');
?>

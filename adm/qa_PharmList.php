<?php
$sub_menu = '300870';
include_once('./_common.php');

auth_check($auth[$sub_menu], "r");


$g5['title'] = '약국 가입문의';
include_once (G5_ADMIN_PATH.'/admin.head.php');

$sql_common = " FROM tbl_qa_pharm";

// 테이블의 전체 레코드수만 얻음
$sql = " select count(*) as cnt " . $sql_common;
$row = sql_fetch($sql);
$total_count = $row['cnt'];
$rows = $config['cf_page_rows'];
$total_page  = ceil($total_count / $rows);  // 전체 페이지 계산
if ($page < 1) { $page = 1; } // 페이지가 없으면 첫 페이지 (1 페이지)
$from_record = ($page - 1) * $rows; // 시작 열을 구함

$sql = "select * $sql_common order by qa_datetime desc limit $from_record, {$config['cf_page_rows']} ";
$result = sql_query($sql);
?>

<div class="local_ov01 local_ov">
    <?php if ($page > 1) {?><a href="<?php echo $_SERVER['SCRIPT_NAME']; ?>">처음으로</a><?php } ?>
    <span class="btn_ov01"><span class="ov_txt">전체 내용</span><span class="ov_num"> <?php echo $total_count; ?>건</span></span>
</div>

<div class="tbl_head01 tbl_wrap">
    <table>
    <caption><?php echo $g5['title']; ?> 목록</caption>
    <thead>
    <tr>
        <th scope="col">번호</th>
        <th scope="col">약국명</th>
		<th scope="col">약사님 성함</th>
        <th scope="col">전화번호</th>
		<th scope="col">약국주소</th>
        <th scope="col">약국 연락처</th>
		<th scope="col">메일주소</th>
		<th scope="col">첨부파일1</th>
        <th scope="col">첨부파일2</th>
		<th scope="col">문의날짜</th>
    </tr>
    </thead>
    <tbody>
    <?php 
	$data_url = G5_DATA_PATH;
		for ($i=0; $row=sql_fetch_array($result); $i++) {
        $bg = 'bg'.($i%2);
		
    ?>
    <tr class="<?php echo $bg; ?>">
        <td class="td_id"><?php echo $row['qa_id']; ?></td>
        <td class="td_left"><?php echo htmlspecialchars2($row['qa_1']); ?></td>
		<td class=""><?php echo htmlspecialchars2($row['qa_2']); ?></td>
		<td class=""><?php echo htmlspecialchars2($row['qa_3']); ?></td>
		<td class=""><?php echo htmlspecialchars2($row['qa_4']); ?></td>
		<td class=""><?php echo htmlspecialchars2($row['qa_5']); ?></td>
		<td class=""><?php echo htmlspecialchars2($row['qa_subject']); ?></td>
		<td class=""><a href="<?echo G5_DATA_URL;?>/qa/pharm/<?php echo htmlspecialchars2($row['qa_file1']); ?>" download="<?php echo htmlspecialchars2($row['qa_source1']); ?>"><?php echo htmlspecialchars2($row['qa_source1']); ?></a></td>
		<td class=""><a href="<?echo G5_DATA_URL;?>/qa/pharm/<?php echo htmlspecialchars2($row['qa_file2']); ?>" download="<?php echo htmlspecialchars2($row['qa_source2']); ?>"><?php echo htmlspecialchars2($row['qa_source2']); ?></a></td>
		<td class=""><?php echo htmlspecialchars2($row['qa_datetime']); ?></td>
    </tr>
    <?php
    }
    if ($i == 0) {
        echo '<tr><td colspan="10" class="empty_table">자료가 한건도 없습니다.</td></tr>';
    }
    ?>
    </tbody>
    </table>
</div>

<?php echo get_paging(G5_IS_MOBILE ? $config['cf_mobile_pages'] : $config['cf_write_pages'], $page, $total_page, "{$_SERVER['SCRIPT_NAME']}?$qstr&amp;page="); ?>

<?php
include_once (G5_ADMIN_PATH.'/admin.tail.php');
?>

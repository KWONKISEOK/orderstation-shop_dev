<?php
$sub_menu = "200100";
include_once("./_common.php");
include_once(G5_LIB_PATH."/register.lib.php");
include_once(G5_LIB_PATH.'/thumbnail.lib.php');

if ($w == 'u')
    check_demo();

auth_check($auth[$sub_menu], 'w');

check_admin_token();

$mb_id = trim($_POST['mb_id']);

$mb_zip1 = substr($_POST['mb_zip'], 0, 3);
$mb_zip2 = substr($_POST['mb_zip'], 3);
$mb_type = $_POST['mb_type'];

$sql_common = "  mb_name = '{$_POST['mb_name']}'
                 , mb_nick = '{$_POST['mb_nick']}'
                 , mb_email = '{$_POST['mb_email']}'
                 , mb_homepage = '{$_POST['mb_homepage']}'
                 , mb_birth = '{$_POST['mb_birth']}'
				 , mb_tel = '{$_POST['mb_tel']}'
                 , mb_hp = '{$mb_hp}'
                 , mb_zip1 = '$mb_zip1'
                 , mb_zip2 = '$mb_zip2'
                 , mb_addr1 = '{$_POST['mb_addr1']}'
                 , mb_addr2 = '{$_POST['mb_addr2']}'
                 , mb_addr3 = '{$_POST['mb_addr3']}'
                 , mb_addr_jibeon = '{$_POST['mb_addr_jibeon']}'
                 , mb_signature = '{$_POST['mb_signature']}'
                 , mb_memo = '{$_POST['mb_memo']}'
                 , mb_mailling = '{$_POST['mb_mailling']}'
                 , mb_sms = '{$_POST['mb_sms']}'
                 , mb_open = '{$_POST['mb_open']}'
                 , mb_profile = '{$_POST['mb_profile']}'
               ";



    $mb = get_member($mb_id);
    if (!$mb['mb_id'])
        alert('존재하지 않는 회원자료입니다.');

    if ($is_admin !== 'super' && is_admin($mb['mb_id']) === 'super' ) {
        alert('최고관리자의 비밀번호를 수정할수 없습니다.');
    }

    if ($mb_password)
        $sql_password = " , mb_password = '".get_encrypt_string($mb_password)."' ";
    else
        $sql_password = "";

    if ($passive_certify)
        $sql_certify = " , mb_email_certify = '".G5_TIME_YMDHIS."' ";
    else
        $sql_certify = "";

    $sql = " update {$g5['member_table']}
                set {$sql_common}
                     {$sql_password}
                     {$sql_certify}
                where mb_id = '{$mb_id}' ";


	//echo "<pre>".$sql."</pre>";
	//exit;
    sql_query($sql);


goto_url('./vender_form.php?'.$qstr.'&amp;w=u&amp;mb_id='.$mb_id, false);
?>
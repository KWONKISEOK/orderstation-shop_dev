<?php

include_once("./_common.php");

$mb_id		= $_POST['mb_id'];
$mb_type	= $_POST['mb_type'];

//공급사 tbl_company
function sfSalesRs($code_type, $sel) {
	
	$sql = "select mb_id, mb_name
			  from tbl_member where mb_type = 5";
	

	$res = sql_query($sql);

	while($row = sql_fetch_array($res)) {
		$str = $str."<option value='".$row['mb_id']."'"; 
		if($sel ==$row['mb_id']) $str = $str."selected";
		$str = $str.">";
		$str = $str.$row['mb_name']."</option>";
	}	
	return $str;
}


?>

<?php if($mb_type == 0) { // 일반회원 

$mb = get_member($mb_id);
$sido = $mb['pharm_sido'];
	$gugun = $mb['pharm_gugun'];
	$dong = $mb['pharm_dong'];

$rmb = get_member($mb['mb_recommend']);


?>


	<h3 style="padding:10px;font-size:1.2em;">일반회원 추가정보</h3>
	<table>
	<tr>
            <th scope="row">지역분류</th>
            <td colspan="3"><select name="pharm_sido" id="pharm_sido" onchange="onChangeAddr(1, 1)">
              <option value="">시/도</option>
				<option value="강원도" <?php if($sido=='강원도') echo 'selected';?> >강원도</option>
				<option value="경기도" <?php if($sido=='경기도') echo 'selected';?> >경기도</option>						
				<option value="경상남도" <?php if($sido=='경상남도') echo 'selected';?> >경상남도</option>						
				<option value="경상북도" <?php if($sido=='경상북도') echo 'selected';?> >경상북도</option>
				<option value="광주광역시" <?php if($sido=='광주광역시') echo 'selected';?> >광주광역시</option>
				<option value="대구광역시" <?php if($sido=='대구광역시') echo 'selected';?> >대구광역시</option>
				<option value="대전광역시" <?php if($sido=='대전광역시') echo 'selected';?> >대전광역시</option>
				<option value="부산광역시" <?php if($sido=='부산광역시') echo 'selected';?> >부산광역시</option>
				<option value="서울특별시" <?php if($sido=='서울특별시') echo 'selected';?> >서울특별시</option>
				<option value="세종특별자치시" <?php if($sido=='세종특별자치시') echo 'selected';?> >세종특별자치시</option>
				<option value="울산광역시" <?php if($sido=='울산광역시') echo 'selected';?> >울산광역시</option>
				<option value="인천광역시" <?php if($sido=='인천광역시') echo 'selected';?> >인천광역시</option>
				<option value="전라남도" <?php if($sido=='전라남도') echo 'selected';?> >전라남도</option>
				<option value="전라북도" <?php if($sido=='전라북도') echo 'selected';?> >전라북도</option>
				<option value="제주특별자치도" <?php if($sido=='제주특별자치도') echo 'selected';?> >제주특별자치도</option>
				<option value="충청남도" <?php if($sido=='충청남도') echo 'selected';?> >충청남도</option>
				<option value="충청북도" <?php if($sido=='충청북도') echo 'selected';?> >충청북도</option>
     
            </select>
            
            <select name="pharm_gugun" id="pharm_gugun" onchange="onChangeAddr(2,2)">
				<option value="">=시구군=</option>
				<?php if($sido) { 
					$sql = " select distinct gugun
							  from tbl_addr
							 where sido = '$sido' order by gugun";
					$result = sql_query($sql);
					while($row = mysqli_fetch_array($result)) { ?>
					    
						<option value="<?php echo $row[gugun]; ?>" <?php if($gugun ==$row[gugun]) echo "selected"; ?> ><?php echo $row[gugun]; ?></option>
					<?php } 
					} else {?>
					  <option value="">=시구군=</option>
					 <?php } ?>
            </select>
            <select name="pharm_dong" id="pharm_dong">
			<option value="">=읍면동=</option>
			<?php if($gugun) { 
					$sql = " select distinct dong
							  from tbl_addr
							 where gugun = '$gugun' and dong != '' order by dong";
					$result = sql_query($sql);

					while($row1 = mysqli_fetch_array($result)) { ?>
					    
						<option value="<?php echo $row1['dong']; ?>" <?php if($dong == $row1['dong']) echo "selected"; ?> ><?php echo $row1['dong']; ?></option>
					<?php } 
					} else {?>
					  <option value="">=읍면동=</option>
					 <?php } ?>
              
            </select>

            </td>
          </tr>

          <tr>
            <th scope="row">나의 관리 약사</th>
            <td colspan="3">

			<input type="hidden" name="mb_recommend" value="<?php $mb['mb_recommend'];?>" >
        <?php
		if($rmb) { ?>
        약사이름 : <?php echo $rmb['mb_name'];?><br>
        아이디 : <?php echo $rmb['mb_id'];?><br>        
        약국명 : <?php echo $rmb['pharm_name'];?><br>
		<?php } else { ?>

		관리약사가 업습니다.
        <?php } ?>
              </td>
          </tr>
		  <th scope="row">사용여부</th>
            <td colspan="3">
             <input type="radio" name="pharm_status" id="pharm_status1" value="2" <?php if($mb['pharm_status'] =="2") echo 'checked';?>  >
              <label for="pharm_div1">사용(승인)</label>
              <input type="radio" name="pharm_status" id="pharm_status2" value="3" <?php if($mb['pharm_status'] =="3") echo 'checked';?>  >
              <label for="pharm_div2">사용안함</label>
              </td>

          <tr>
            <th scope="row">추천회원구분</th>
            <td colspan="3">
            <label><input type="radio" name="mb_referee" value="0" <?php if($mb['mb_referee'] =="0") echo 'checked';?> >일반회원</label>
            <label><input type="radio" name="mb_referee" value="1" <?php if($mb['mb_referee'] =="1") echo 'checked';?>  >os추천회원</label>
            <label><input type="radio" name="mb_referee" value="2" <?php if($mb['mb_referee'] =="2") echo 'checked';?>  >모니터링회원</label>
            <label><input type="radio" name="mb_referee" value="3" <?php if($mb['mb_referee'] =="3") echo 'checked';?>  >os회원</label>
            <label><input type="radio" name="mb_referee" value="4" <?php if($mb['mb_referee'] =="4") echo 'checked';?>  >os직원</label>
            </td>
          </tr>

	</table>
	<?php } ?>


	<?php if($mb_type == 1) { // 약국회원 
	$mb = get_member($mb_id);

	$sido = $mb['pharm_sido'];
	$gugun = $mb['pharm_gugun'];
	$dong = $mb['pharm_dong'];

	?>


	<h3 style="padding:10px;font-size:1.2em;">약국회원 추가정보</h3>
	<table>

	
	<tr>
            <th scope="row"><font color="#ff0000">*</font> 약국명</th>
            <td><input type="text" name="pharm_name" id="pharm_name" size="20" value="<?php echo $mb['pharm_name'];?>" maxlength="20" class="frm_input"></td>
            <th scope="row"><font color="#ff0000">*</font> 사업자등록번호</th>
            <td><input type="text" name="pharm_number" id="pharm_number" class="frm_input" value="<?php echo $mb['pharm_number'];?>"></td>
          </tr>
          <tr>
            <th scope="row"><font color="#ff0000">*</font> 약사면허번호</th>
            <td><input name="pharm_license" type="text" id="pharm_license" class="frm_input" size="20" value="<?php echo $mb['pharm_license'];?>"></td>
            <th scope="row"><font color="#ff0000">*</font> 요양기관번호</td>
            <td><input name="pharm_sanatorium" type="text" id="pharm_sanatorium" class="frm_input" size="20" value="<?php echo $mb['pharm_sanatorium'];?>"></td>
          </tr>
          <tr>
            <th scope="row"><font color="#ff0000">*</font> 약국전화번호</th>
            <td><input name="pharm_tel" type="text" id="pharm_tel" size="20" maxlength="20"  class="frm_input" value="<?php echo $mb['pharm_tel'];?>">
           </td>
            <th scope="row"><font color="#ff0000">*</font> 약국fax</th>
            <td><input name="pharm_fax" type="text" id="pharm_fax" size="20" maxlength="20"  class="frm_input" value="<?php echo $mb['pharm_fax'];?>">
           </td>
          </tr>

		  <tr>
            <th scope="row">휴무일</th>
            <td colspan="3"><input type="text" name="pharm_holiday" id="pharm_holiday" size="20" class="frm_input" value="<?php echo $mb['pharm_holiday'];?>" maxlength="100"></td>
          </tr>
          <tr>
            <th scope="row">약국소개</th>
            <td colspan="3"><input type="text" name="pharm_intro" id="pharm_intro" style="width:99%;" class="frm_input"  value="<?php echo $mb['pharm_intro'];?>" maxlength="500"></td>
          </tr>


		  <tr>
        <th scope="row">약국코드</th>
        <td>
            <?php echo $mb['pharm_custno'] ?>
           
        </td>
		<th scope="row">사용여부</th>
            <td >
             <input type="radio" name="pharm_status" id="pharm_status1" value="2" <?php if($mb['pharm_status'] =="2") echo 'checked';?>  >
              <label for="pharm_div1">사용(승인)</label>
              <input type="radio" name="pharm_status" id="pharm_status2" value="3" <?php if($mb['pharm_status'] =="3") echo 'checked';?>  >
              <label for="pharm_div2">사용안함</label>
              </td>
        
    </tr>

 <tr>
            <th scope="row">가입경로</th>
            <td colspan="3">
            <span style="display:inline-block; width:180px;"><input name="mb_joinroute" id="mb_joinroute1" type="radio" value="01" <?php if($mb['mb_joinroute'] =="01") echo 'checked';?>> <label for="mb_joinroute1">티제이팜 영업사원추천</label>
           </span>
            
            <span style="display:inline-block; width:180px;"><input name="mb_joinroute" id="mb_joinroute2" type="radio" value="02" <?php if($mb['mb_joinroute'] =="02") echo 'checked';?> > <label for="mb_joinroute2">태전약품 영업사원추천</label>
           </span>
            
            <span style="display:inline-block; width:180px;"><input name="mb_joinroute" id="mb_joinroute3" type="radio" value="03" <?php if($mb['mb_joinroute'] =="03") echo 'checked';?> > <label for="mb_joinroute3">오엔케이 영업사원추천</label>
           </span>
            
            <span style="display:inline-block; width:180px;"><input name="mb_joinroute" id="mb_joinroute4" type="radio" value="04" <?php if($mb['mb_joinroute'] =="04") echo 'checked';?> > <label for="mb_joinroute4">박람회 등</label>
           </span>
            
            <span style="display:inline-block; width:180px;"><input name="mb_joinroute" id="mb_joinroute5" type="radio" value="05" <?php if($mb['mb_joinroute'] =="05") echo 'checked';?> > <label for="mb_joinroute5">주변(약사님)추천</label>
           </span>
            
            <span style="display:inline-block; width:180px;"><input name="mb_joinroute" id="mb_joinroute6" type="radio" value="06" <?php if($mb['mb_joinroute'] =="06") echo 'checked';?> > <label for="mb_joinroute6">고객센터 추천</label>
           </span>
            
            <span style="display:inline-block; width:180px;"><input name="mb_joinroute" id="mb_joinroute7" type="radio" value="07" <?php if($mb['mb_joinroute'] =="07") echo 'checked';?> > <label for="mb_joinroute7">보도자료(네이버 등)</label>
           </span>
            
            <span style="display:inline-block; width:180px;"><input name="mb_joinroute" id="mb_joinroute8" type="radio" value="08" <?php if($mb['mb_joinroute'] =="08") echo 'checked';?> > <label for="mb_joinroute8">기타</label>
           </span>
            </td>
          </tr>


	 <tr>
            <th scope="row">담당영업사원</th>
            <td >
            
            
            <select name="pharm_manager" id="pharm_manager" style="width:180px;">
                <option value="">=영업사원=</option>
				<?php echo sfSalesRs('', $mb['pharm_manager']);?>
                
              </select></td>
			  <th scope="row">거래구분</th>
            <td >
             <input type="radio" name="pharm_div" id="pharm_div1" value="1" <?php if($mb['pharm_div'] =="1") echo 'checked';?>  >
              <label for="pharm_div1">태전약품</label>
              <input type="radio" name="pharm_div" id="pharm_div2" value="2" <?php if($mb['pharm_div'] =="2") echo 'checked';?>  >
              <label for="pharm_div2">티제이팜</label>
              </td>
          </tr>
          
        
          <tr>
            <th scope="row">결제구분</th>
            <td >
             <input type="radio" name="mb_pay_yn" id="pharm_pay1" value="Y" <?php if($mb['mb_pay_yn'] =="Y") echo 'checked';?> >
              <label for="pharm_pay1">오더스테이션결제</label>
              <input type="radio" name="mb_pay_yn" id="pharm_pay2" value="N" <?php if($mb['mb_pay_yn'] =="N") echo 'checked';?>>
              <label for="pharm_pay2">직거래</label>
              </td>
     <th scope="row">검색노출여부</th>
            <td >
             <label><input type="radio" name="pharm_search" id="pharm_search1" value="Y" <?php if($mb['pharm_search'] =="Y") echo 'checked';?> >
              노출</label>
              <label><input type="radio" name="pharm_search" id="pharm_search2" value="N" <?php if($mb['pharm_search'] =="N") echo 'checked';?>>
              비노출</label> (* 소비자가 약국검색할시 노출여부)
              </td>
          </tr>


	<tr>
            <th scope="row">지역분류</th>
            <td colspan="3"><select name="pharm_sido" id="pharm_sido" onchange="onChangeAddr(1, 1)">
              <option value="">시/도</option>
				<option value="강원도" <?php if($sido=='강원도') echo 'selected';?> >강원도</option>
				<option value="경기도" <?php if($sido=='경기도') echo 'selected';?> >경기도</option>						
				<option value="경상남도" <?php if($sido=='경상남도') echo 'selected';?> >경상남도</option>						
				<option value="경상북도" <?php if($sido=='경상북도') echo 'selected';?> >경상북도</option>
				<option value="광주광역시" <?php if($sido=='광주광역시') echo 'selected';?> >광주광역시</option>
				<option value="대구광역시" <?php if($sido=='대구광역시') echo 'selected';?> >대구광역시</option>
				<option value="대전광역시" <?php if($sido=='대전광역시') echo 'selected';?> >대전광역시</option>
				<option value="부산광역시" <?php if($sido=='부산광역시') echo 'selected';?> >부산광역시</option>
				<option value="서울특별시" <?php if($sido=='서울특별시') echo 'selected';?> >서울특별시</option>
				<option value="세종특별자치시" <?php if($sido=='세종특별자치시') echo 'selected';?> >세종특별자치시</option>
				<option value="울산광역시" <?php if($sido=='울산광역시') echo 'selected';?> >울산광역시</option>
				<option value="인천광역시" <?php if($sido=='인천광역시') echo 'selected';?> >인천광역시</option>
				<option value="전라남도" <?php if($sido=='전라남도') echo 'selected';?> >전라남도</option>
				<option value="전라북도" <?php if($sido=='전라북도') echo 'selected';?> >전라북도</option>
				<option value="제주특별자치도" <?php if($sido=='제주특별자치도') echo 'selected';?> >제주특별자치도</option>
				<option value="충청남도" <?php if($sido=='충청남도') echo 'selected';?> >충청남도</option>
				<option value="충청북도" <?php if($sido=='충청북도') echo 'selected';?> >충청북도</option>
     
            </select>
            
            <select name="pharm_gugun" id="pharm_gugun" onchange="onChangeAddr(2,2)">
				<option value="">=시구군=</option>
				<?php if($sido) { 
					$sql = " select distinct gugun
							  from tbl_addr
							 where sido = '$sido' order by gugun";
					$result = sql_query($sql);
					while($row = mysqli_fetch_array($result)) { ?>
					    
						<option value="<?php echo $row[gugun]; ?>" <?php if($gugun ==$row[gugun]) echo "selected"; ?> ><?php echo $row[gugun]; ?></option>
					<?php } 
					} else {?>
					  <option value="">=시구군=</option>
					 <?php } ?>
            </select>
            <select name="pharm_dong" id="pharm_dong">
			<option value="">=읍면동=</option>
			<?php if($gugun) { 
					$sql = " select distinct dong
							  from tbl_addr
							 where gugun = '$gugun' and dong != '' order by dong";
					$result = sql_query($sql);

					while($row1 = mysqli_fetch_array($result)) { ?>
					    
						<option value="<?php echo $row1['dong']; ?>" <?php if($dong == $row1['dong']) echo "selected"; ?> ><?php echo $row1['dong']; ?></option>
					<?php } 
					} else {?>
					  <option value="">=읍면동=</option>
					 <?php } ?>
              
            </select>

            </td>
          </tr>
          <tr>
            <th scope="row"></th>
            <td colspan="3">  

				<div id="map-canvas" style="width:100%; height:400px;"></div>
				좌표 : <input type="text" name="map_lat" id="map_lat" value="<?php echo $mb['pharm_map_lat'];?>" readonly> * <input type="text" name="map_lng" id="map_lng" value="<?php echo $mb['pharm_map_lng'];?>" readonly> 
				



				<script>


				var map = new naver.maps.Map("map-canvas", {
					center: new naver.maps.LatLng(37.3595316, 127.1052133),
					zoom: 10,
					mapTypeControl: true
				});

				var infoWindow = new naver.maps.InfoWindow({
					anchorSkew: true
				});

				map.setCursor('pointer');

				// search by tm128 coordinate
				function searchCoordinateToAddress(latlng) {
					var tm128 = naver.maps.TransCoord.fromLatLngToTM128(latlng);

					infoWindow.close();

					naver.maps.Service.reverseGeocode({
						location: tm128,
						coordType: naver.maps.Service.CoordType.TM128
					}, function(status, response) {
						if (status === naver.maps.Service.Status.ERROR) {
							return alert('Something Wrong!');
						}

						var items = response.result.items,
							htmlAddresses = [];

						for (var i=0, ii=items.length, item, addrType; i<ii; i++) {
							item = items[i];
							addrType = item.isRoadAddress ? '[도로명 주소]' : '[지번 주소]';

							document.getElementById("map_lat").value = item.point.x;
							document.getElementById("map_lng").value = item.point.y;

							htmlAddresses.push((i+1) +'. '+ addrType +' '+ item.address);
							htmlAddresses.push('&nbsp&nbsp&nbsp -> '+ item.point.x +','+ item.point.y);
						}

						infoWindow.setContent([
								'<div style="padding:10px;min-width:200px;line-height:150%;">',
								'<h4 style="margin-top:5px;">검색 좌표 : '+ response.result.userquery +'</h4><br />',
								htmlAddresses.join('<br />'),
								'</div>'
							].join('\n'));

						infoWindow.open(map, latlng);
					});
				}

				// result by latlng coordinate
				function searchAddressToCoordinate(address) {

					naver.maps.Service.geocode({
						address: address
					}, function(status, response) {
						if (status === naver.maps.Service.Status.ERROR) {
							//return alert('Something Wrong!');
						}

						var item = response.result.items[0],
							addrType = item.isRoadAddress ? '[도로명 주소]' : '[지번 주소]',
							point = new naver.maps.Point(item.point.x, item.point.y);

							document.getElementById("map_lat").value = item.point.x;
							document.getElementById("map_lng").value = item.point.y;

						infoWindow.setContent([
								'<div style="padding:10px;min-width:200px;line-height:150%;">',
								'<h4 style="margin-top:5px;">검색 주소 : '+ response.result.userquery +'</h4><br />',
								addrType +' '+ item.address +'<br />',
								'&nbsp&nbsp&nbsp -> '+ point.x +','+ point.y,
								'</div>'
							].join('\n'));


						map.setCenter(point);
						infoWindow.open(map, point);
					});
				}

				function initGeocoder() {

					map.addListener('click', function(e) {
						searchCoordinateToAddress(e.coord);
					});

					$('#mb_addr1').on('keydown', function(e) {
						var keyCode = e.which;

						if (keyCode === 13) { // Enter Key
							searchAddressToCoordinate($('#mb_addr1').val());
						}
					});

					$('#submit').on('click', function(e) {
						e.preventDefault();

						searchAddressToCoordinate($('#mb_addr1').val());
					});

					searchAddressToCoordinate('<?php echo $mb['mb_addr1'];?>');
				}

				naver.maps.onJSContentLoaded = initGeocoder;


				</script>


			</td>
          </tr>
		  <tr>
            <th scope="row">추천회원구분</th>
            <td >
            <label><input type="radio" name="mb_referee" value="0" <?php if($mb['mb_referee'] =="0") echo 'checked';?>>일반회원</label>
            <label><input type="radio" name="mb_referee" value="3" <?php if($mb['mb_referee'] =="3") echo 'checked';?>>os회원</label>
            <label><input type="radio" name="mb_referee" value="5" <?php if($mb['mb_referee'] =="5") echo 'checked';?>>얼라이언스약국</label>
              
            </td>
            <th scope="row">추천여부</th>
            <td >
              <label><input type="radio" name="pharm_look" id="pharm_look1" value="0" <?php if($mb['pharm_look'] =="0") echo 'checked';?>>
              미선택</label>
              <label><input type="radio" name="pharm_look" id="pharm_look2" value="1" <?php if($mb['pharm_look'] =="1") echo 'checked';?>>
              지역노출</label>
              <label><input type="radio" name="pharm_look" id="pharm_look3" value="2" <?php if($mb['pharm_look'] =="2") echo 'checked';?>>
              전체노출</label>
              </td>
          </tr>


		  <tr>
            <th scope="row">개인소득정보</th>
            <td >
			  - 은행코드 : <?php echo $mb['pharm_bank_code'];?><br/>
              - 계좌번호 : <?php echo $mb['pharm_bank_num'];?><br/>
			  - 예금주 : <?php echo $mb['pharm_bank_user'];?><br/>
			  - 동의일 : <?php echo $mb['pharm_bank_agree_date'];?><br/>
			 
              
            </td>
            <th scope="row"></th>
            <td >
              
             </td>
          </tr>


          <!-- <tr>
            <th scope="row"><font color="#ff0000">*</font> 문전병(의)원</th>
            <td colspan="3">
            <span style="display:inline-block; width:100px;"><input name="mb_pharm_type" id="mb_pharm_type1" type="checkbox" value="01" > <label for="mb_pharm_type1">내과</label>
           </span>
            
            <span style="display:inline-block; width:100px;"><input name="mb_pharm_type" id="mb_pharm_type2" type="checkbox" value="02" > <label for="mb_pharm_type2">외과</label>
           </span>
            
            <span style="display:inline-block; width:100px;"><input name="mb_pharm_type" id="mb_pharm_type3" type="checkbox" value="03" > <label for="mb_pharm_type3">안과</label>
           </span>
            
            <span style="display:inline-block; width:100px;"><input name="mb_pharm_type" id="mb_pharm_type4" type="checkbox" value="04" > <label for="mb_pharm_type4">이비인후과</label>
           </span>
            
            <span style="display:inline-block; width:100px;"><input name="mb_pharm_type" id="mb_pharm_type5" type="checkbox" value="05" > <label for="mb_pharm_type5">흉부외과</label>
           </span>
            
            <span style="display:inline-block; width:100px;"><input name="mb_pharm_type" id="mb_pharm_type6" type="checkbox" value="06" > <label for="mb_pharm_type6">가정의학과</label>
           </span>
            
            <span style="display:inline-block; width:100px;"><input name="mb_pharm_type" id="mb_pharm_type7" type="checkbox" value="07" > <label for="mb_pharm_type7">소아과</label>
           </span>
            
            <span style="display:inline-block; width:100px;"><input name="mb_pharm_type" id="mb_pharm_type8" type="checkbox" value="08" > <label for="mb_pharm_type8">치과</label>
           </span>
            
            <span style="display:inline-block; width:100px;"><input name="mb_pharm_type" id="mb_pharm_type9" type="checkbox" value="09" > <label for="mb_pharm_type9">성형외과</label>
           </span>
            
            <span style="display:inline-block; width:100px;"><input name="mb_pharm_type" id="mb_pharm_type10" type="checkbox" value="10" > <label for="mb_pharm_type10">정신과</label>
           </span>
            
            <span style="display:inline-block; width:100px;"><input name="mb_pharm_type" id="mb_pharm_type11" type="checkbox" value="11" > <label for="mb_pharm_type11">산부인과</label>
           </span>
            
            <span style="display:inline-block; width:100px;"><input name="mb_pharm_type" id="mb_pharm_type12" type="checkbox" value="12" > <label for="mb_pharm_type12">피부과</label>
           </span>
            
            <span style="display:inline-block; width:100px;"><input name="mb_pharm_type" id="mb_pharm_type13" type="checkbox" value="13" > <label for="mb_pharm_type13">방사선과</label>
           </span>
            
            <span style="display:inline-block; width:100px;"><input name="mb_pharm_type" id="mb_pharm_type14" type="checkbox" value="14" > <label for="mb_pharm_type14">신경외과</label>
           </span>
            
            <span style="display:inline-block; width:100px;"><input name="mb_pharm_type" id="mb_pharm_type15" type="checkbox" value="15" > <label for="mb_pharm_type15">종합병원</label>
           </span>
            
            <span style="display:inline-block; width:100px;"><input name="mb_pharm_type" id="mb_pharm_type16" type="checkbox" value="16" > <label for="mb_pharm_type16">한의원</label>
           </span>
            
            <span style="display:inline-block; width:100px;"><input name="mb_pharm_type" id="mb_pharm_type17" type="checkbox" value="17" > <label for="mb_pharm_type17">비뇨기과</label>
           </span>
            
            <span style="display:inline-block; width:100px;"><input name="mb_pharm_type" id="mb_pharm_type18" type="checkbox" value="18" > <label for="mb_pharm_type18">기타</label>
           </span>
            </td>
            
          </tr> -->
		  
	<tr>
            <th><font color="#FF0000">*</font> 약국매출비중</th>
            <td colspan="3">처방의약품/일반의약품의 대략적인 매출 비중을 숫자로 입력해주세요(입력하신 숫자 합은 100이 되어야 합니다.)<br>
            <br>
            <span style="color:#F00">처방의약품</span><input name="pharm_account1" type="text" id="pharm_account1" size="4" maxlength="2" style="ime-mode:disabled" onKeyPress="onlyNumber()" value="<?php echo $mb['pharm_account1'];?>">
            %&nbsp;&nbsp;&nbsp;&nbsp;<span style="color:#F00">일반의약품</span>
            <input name="pharm_account2" type="text" id="pharm_account2" size="4" maxlength="2" style="ime-mode:disabled" onKeyPress="onlyNumber()" value="<?php echo $mb['pharm_account2'];?>">
            %</td>
            
          </tr>

	</table>
	<?php } ?>

	<?php if($mb_type == 5) { // 영업자 
	$mb = get_member($mb_id);
	$dept = substr($mb['pharm_manager_dept'], 0 , 6);
	$dept2 = $mb['pharm_manager_dept'];
	?>


	<h3 style="padding:10px;font-size:1.2em;">영업자 추가정보</h3>
	<table>

	
          <tr>
            <th scope="row"><font color="#ff0000">*</font> 소속</td>
            <td>
				<select name="dept" id="dept" >
					<option value="">- 1차 카테고리 -</option>					
					<option value="002001" <?php if($dept == '002001') echo 'selected';?>>태전약품</option>					
					<option value="002002" <?php if($dept == '002002') echo 'selected';?>>티제이팜</option>					
					<option value="002003" <?php if($dept == '002003') echo 'selected';?>>오엔케이</option>					
				  </select>
				  <select name="dept2" id="dept2" >
					<?php if($dept2) { 
					$sql = " select minor_cd, code_nm
							  from tbl_code
							 where major_cd='A03'
							   and minor_cd like '$dept%' 
							   and ref_cd1 = '1'";
					$result = sql_query($sql);
					while($row = mysqli_fetch_array($result)) { ?>
						<option value="<?php echo $row[minor_cd]; ?>" <?php if($dept2 ==$row[minor_cd]) echo "selected"; ?> ><?php echo $row[code_nm]; ?></option>
					<?php } 
					} else {?>
					  <option value="">- 2차 카테고리 -</option>
					 <?php } ?>
				  </select>
				  <input type="hidden" name="pharm_manager_dept" id="pharm_manager_dept" />
			</td>
			 <th scope="row">사용여부</th>
			  <td >
				 <input type="radio" name="pharm_status" id="pharm_status1" value="2" <?php if($mb['pharm_status'] =="2") echo 'checked';?>  >
				  <label for="pharm_div1">사용(승인)</label>
				  <input type="radio" name="pharm_status" id="pharm_status2" value="3" <?php if($mb['pharm_status'] =="3") echo 'checked';?>  >
				  <label for="pharm_div2">사용안함</label>
              </td>

			
          </tr>
		  

		
</table>
<script>
$( "#dept" ).change(function() {			
	var dept = $("#dept option:selected").val() ;

	$.ajax({
		type: "GET",
		url: "./member_dept.php",
		data: "dept="+dept, 
		dataType: "text",
		cache: false,
		success: function(resultText) {
			resultText = "<option value=''>- 2차 카테고리 -</option>" + resultText
			$("#dept2").html(resultText);
		}
	});
});
</script>
<?php } ?>

	<?php if($mb_type == 7) { // 공급사 
	$mb = get_member($mb_id);
	?>


	<h3 style="padding:10px;font-size:1.2em;">
		공급사 추가정보(관리자가 승인완료한 데이터)
		<? if(!empty($mb['comp_charge_datetime_2'])){ ?>
		승인완료일:<?=$mb['comp_charge_datetime_2']?>
		<? } ?>
	</h3>
	<table>
		  <tr>
			<th scope="row">담당자정보</th>
			  <td colspan='3'>
					<br>
					<table style="width:80%;" border="0" id="group_table">
					<tr>
						<th style="text-align:center;width:60px;">업무</th>
						<th style="text-align:center;width:120px;">담당자명</th>
						<th style="text-align:center;width:220px;">비고(직책)</th>
						<th style="text-align:center;width:180px;">Tel</th>
						<th style="text-align:center;width:180px;">HP</th>
						<th style="text-align:center;width:180px;">이메일</th>
						<th style="text-align:center;width:60px;"><a href="javascript:AddRow();" class="btn btn_02">추가</a></th>
					</tr>
					<?
					$tr_sql = " select * from tbl_charge_info where DelYn = 'N' and mb_id = '{$mb_id}' order by Charge_RegDate asc ";
					$tr_result = sql_query($tr_sql);
					$tr_result_cnt = sql_num_rows($tr_result);
					
					//while($row = mysqli_fetch_array($result)) { 

					if( $tr_result_cnt == 0){

					?>
						<tr>
							<td style="text-align:center;width:60px;">
								<input type="hidden" name="Charge_Idx[]" value="0">
								<input type="hidden" name="DelYn[]" value="N">
								<select name="Charge_Task[]">
									<option value="CS">CS</option>
									<option value="영업">영업</option>
									<option value="출고">출고</option>
									<option value="정산">정산</option>
								</select>
							</td>
							<td style="text-align:center;"><input type="text" name="Charge_Name[]" maxlength="50" class="frm_input" style="width:120px;"></td>
							<td style="text-align:center;"><input type="text" name="Charge_Bigo[]" maxlength="100" class="frm_input" style="width:220px;"></td>
							<td style="text-align:center;"><input type="text" name="Charge_Tel[]" maxlength="50" class="frm_input" style="width:180px;"></td>
							<td style="text-align:center;"><input type="text" name="Charge_Hp[]" maxlength="50" class="frm_input" style="width:180px;"></td>
							<td style="text-align:center;"><input type="text" name="Charge_Email[]" maxlength="50" class="frm_input" style="width:180px;"></td>
							<td></td>
						</tr>
					<? 

					}else{ 
						
						$i = 1;
						while($tr_row = mysqli_fetch_array($tr_result)) { 

					?>
						<tr>
							<td style="text-align:center;width:60px;">
								<input type="hidden" name="Charge_Idx[]" value="<?=$tr_row["Charge_Idx"]?>">
								<input type="hidden" name="DelYn[]" value="<?=$tr_row["DelYn"]?>">
								<select name="Charge_Task[]">
									<option value="CS" <?if( $tr_row["Charge_Task"] == "CS" ){?>selected<?}?> >CS</option>
									<option value="영업" <?if( $tr_row["Charge_Task"] == "영업" ){?>selected<?}?> >영업</option>
									<option value="출고" <?if( $tr_row["Charge_Task"] == "출고" ){?>selected<?}?> >출고</option>
									<option value="정산" <?if( $tr_row["Charge_Task"] == "정산" ){?>selected<?}?> >정산</option>
								</select>
							</td>
							<td style="text-align:center;">
								<input type="text" name="Charge_Name[]" maxlength="50" class="frm_input" style="width:120px;" value="<?=$tr_row["Charge_Name"]?>">
							</td>
							<td style="text-align:center;">
								<input type="text" name="Charge_Bigo[]" maxlength="100" class="frm_input" style="width:220px;" value="<?=$tr_row["Charge_Bigo"]?>">
							</td>
							<td style="text-align:center;">
								<input type="text" name="Charge_Tel[]" maxlength="50" class="frm_input" style="width:180px;" value="<?=$tr_row["Charge_Tel"]?>">
							</td>
							<td style="text-align:center;">
								<input type="text" name="Charge_Hp[]" maxlength="50" class="frm_input" style="width:180px;" value="<?=$tr_row["Charge_Hp"]?>">
							</td>
							<td style="text-align:center;">
								<input type="text" name="Charge_Email[]" maxlength="50" class="frm_input" style="width:180px;" value="<?=$tr_row["Charge_Email"]?>">
							</td>
							<td style="text-align:center;">
							<? if($i > 1){ ?>
								<button type="button" class="btn_submit btn" onclick="javascript:DelRow(this);">삭제</button>
							<? } ?>
							</td>
						</tr>
					<?
							$i++;
					
						}

					}
						
					?>

					</table>
					<br>
					<div style="width:80%;text-align:center;">
						<a href="javascript:ChargeReg();" class="btn btn_03">담당자 저장</a>
					</div>
					<br>
			  </td>
		  </tr>
		  <tr>
			<th scope="row">사용여부</th>
			  <td colspan='3' >
				 <input type="radio" name="pharm_status" id="pharm_status1" value="2" <?php if($mb['pharm_status'] =="2") echo 'checked';?>  >
				  <label for="pharm_div1">사용(승인)</label>
				  <input type="radio" name="pharm_status" id="pharm_status2" value="3" <?php if($mb['pharm_status'] =="3") echo 'checked';?>  >
				  <label for="pharm_div2">사용안함</label>
              </td>

		  </tr>
          <tr>
            <th scope="row"><font color="#ff0000">*</font> 업체명</td>
            <td><input type="text" name="comp_name" size="25" value="<?php echo $mb['comp_name'];?>" maxlength="25"  class="frm_input"></td>
			<th scope="row"> 공급사코드</th>
            <td><input type="text" name="comp_code"  class="frm_input" value="<?php echo $mb['comp_code'];?>" maxlength="5"></td>
          </tr>
		  <tr>
            <th scope="row"> 최소주문금액</th>
            <td ><input type="text" name="comp_limit" size="25" class="frm_input" value="<?php echo $mb['comp_limit'];?>" maxlength="25"></td>
			<th scope="row"> 브랜드명</th>
            <td ><input type="text" name="comp_brand" size="25" class="frm_input" value="<?php echo $mb['comp_brand'];?>" maxlength="25"></td>
			
          </tr>
		  <tr>
            <th scope="row"> 대표자명</th>
            <td><input type="text" name="comp_ceo_name" size="20" value="<?php echo $mb['comp_ceo_name'];?>" maxlength="20" class="frm_input"></td>
            <th scope="row"> 대표전화번호</th>
            <td><input type="text" name="comp_tel" value="<?php echo $mb['comp_tel'];?>" maxlength="20" size="20" class="frm_input">
            </td>
          </tr>
		  <tr>
            <th scope="row"> 담당자명</th>
            <td><input type="text" name="comp_md_name" size="20" value="<?php echo $mb['comp_md_name'];?>" maxlength="20" class="frm_input"> </td>
            <th scope="row"> 담당자연락처</th>
            <td><input type="text" name="comp_md_tel" value="<?php echo $mb['comp_md_tel'];?>"maxlength="20" size="20" class="frm_input">
            </td>
          </tr>

		<!-- <tr>
			<th scope="row"><font color="#ff0000">*</font> 배송비 상한가</th>
			<td colspan="3" style="background-color:#fff;">
				<input type="radio" name="cmp_delivery_choice" value="0" checked>묶음상품
				<input type="radio" name="cmp_delivery_choice" value="1" >전체상품 
				<input type="text" name="cmp_delivery_max" size="10" value="20000" maxlength="20" style="ime-mode:disabled;"   onkeyup="this.value=this.value.replace(/[^0-9]/g,'')">
				원 이상구매시
				<input type="text" name="cmp_delivery_charge" size="10" value="0" maxlength="20" style="ime-mode:disabled;"    onkeyup="this.value=this.value.replace(/[^0-9]/g,'')"> 원 배송비 적용
				<input type="checkbox" name="cmp_delivery_use" value="y" checked >사용
			</td>
		  </tr> -->
          <!-- <tr>
            <th scope="row"><font color="#ff0000">*</font> 포인트금액 설정</th>
            <td>
				<input type="text" name="cmp_price" size="10" value="0" maxlength="20" style="ime-mode:disabled;" class="frm_input"  >
				원 이상일때
				<input type="text" name="cmp_point" size="10" value="0" maxlength="20" style="ime-mode:disabled;" class="frm_input"  >
				포인트 부여
				<input type="checkbox" name="cmp_point_per" value="y"  >%적용
				<br/><font color='red'>ex( % 적용시 소수점 형식 으로 적용해주세요 )</font>
			</td>
            <th scope="row"><font color="#ff0000">*</font> 웹/앱설정</th>
            <td>
				<input type="checkbox" name="cmp_point_web" value="y"  >웹
				<input type="checkbox" name="cmp_point_app" value="y"  >앱
			</td>
          </tr> -->

          <!-- <tr>
            <th scope="row">사업자등록번호</th>
            <td colspan="3"><input type="text" name="cmp_number1" size="3" value="" maxlength="3" style="ime-mode:disabled;" onkeypress="onlynumber()" onkeyup="inputnext(this,frm1.cmp_number2)">
           - <input type="text" name="cmp_number2" size="2" value="" maxlength="2" style="ime-mode:disabled;" onkeypress="onlynumber()" onkeyup="inputnext(this,frm1.cmp_number3)">
           - <input type="text" name="cmp_number3" size="5" value="" maxlength="5" style="ime-mode:disabled;" onkeypress="onlynumber()">
            </td>
          </tr>
          <tr>
            <th scope="row"> 업태</th>
            <td><input type="text" name="cmp_conditions" size="40" value="" maxlength="50" class="frm_input"></td>
            <th scope="row"> 종목</th>
            <td><input type="text" name="cmp_biz_type" size="40" value="" maxlength="50" class="frm_input"></td>
          </tr>
          <tr>
            <th scope="row"> 대표자명</th>
            <td><input type="text" name="cmp_ceo_name" size="20" value="최용국" maxlength="20" class="frm_input"></td>
            <th scope="row"> 대표전화번호</th>
            <td><input type="text" name="cmp_tel_1" value="02-1111-1111" maxlength="20" size="20" class="frm_input">
            </td>
          </tr>
          <tr>
            <th scope="row"> 담당자명</th>
            <td><input type="text" name="cmp_md_name" size="20" value="문성민 주임" maxlength="20" class="frm_input"> </td>
            <th scope="row"> 담당자연락처</th>
            <td><input type="text" name="cmp_md_tel_1" value="010-1111-1111"maxlength="20" size="20" class="frm_input">
            </td>
          </tr> -->
          <!-- <tr>
            <td> 사용여부</td>
            <td colspan="3"><input type="radio" name="cmp_status" value="1"  checked >
              예
              <input type="radio" name="cmp_status" value="2" >
              아니오</td>
          </tr> -->
          <!-- <tr>
			<th scope="row"> 배송구분</th>
			<td colspan="3"><input type="radio" name="cmp_trans_type" value="1" >
              택배
              </td>
		  </tr> -->
		   <!-- <tr>
			<th scope="row"> 최소주문금액</th>
			<td ><input type="text" name="comp_limit" class="frm_input" size="50" value="" maxlength="50"></td>
		  </tr> -->
		  <tr>
			<th scope="row"> 주문시간</th>
			<td colspan="3"><textarea name="comp_order_time" style="width:95%; height:50px"><?php echo $mb['comp_order_time'];?></textarea></td>
		  </tr>
		  <tr>
			<th scope="row"> 배송시간</th>
			<td colspan="3"><textarea name="comp_trans_time" style="width:95%; height:50px"><?php echo $mb['comp_trans_time'];?></textarea></td>
		  </tr>
</table>
	<?php } ?>



<!--담당업업사원 검색기능-->
<script src="http://code.jquery.com/jquery.min.js"></script>
<link href="./select2.css?20190115" rel="stylesheet"/>
<script type="text/javascript" src="./select2.js?20190115"></script>
<script>
$(document).ready(function () {
	$("#pharm_manager").select2();
});
</script>

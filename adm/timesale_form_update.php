<?php
$sub_menu = "100400";
include_once('./_common.php');

check_demo();

auth_check($auth[$sub_menu], 'w');

if ($is_admin != 'super')
    alert('최고관리자만 접근 가능합니다.');

check_admin_token();

if( empty($_POST['cf_time_sale_it_id']) ){
    alert('상품코드값을 입력해주세요');
	goto_url('./timesale_form.php', false);
	exit;
}else{
	$sql = " update {$g5['config_table']} set cf_time_sale_use = '{$_POST['cf_time_sale_use']}' , cf_time_sale_it_id = '{$_POST['cf_time_sale_it_id']}'
			, cf_time_sale_startdate = '{$_POST['cf_time_sale_startdate']}' , cf_time_sale_enddate = '{$_POST['cf_time_sale_enddate']}' 
	";
	sql_query($sql);
}

goto_url('./timesale_form.php', false);
?>
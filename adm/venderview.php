<?php
$sub_menu = '300830';
include_once('./_common.php');
include_once(G5_EDITOR_LIB);

auth_check($auth[$sub_menu], "w");



$html_title = "공급사정보공유";
$g5['title'] = $html_title.' 내용보기';


    $sql = " select * from tbl_write_vender where wr_id = '$wr_id' ";
    $wr = sql_fetch($sql);
    if (!$wr['wr_id'])
        alert('등록된 자료가 없습니다.');


include_once (G5_ADMIN_PATH.'/admin.head.php');
?>

<form name="frmcontentform" action="./venderformupdate.php" onsubmit="return frmcontentform_check(this);" method="post" enctype="MULTIPART/FORM-DATA" >

<div class="tbl_frm01 tbl_wrap">
    <table>
    <caption><?php echo $g5['title']; ?> </caption>
    <colgroup>
        <col class="grid_4">
        <col>
    </colgroup>
    <tbody>

    <tr>
        <th scope="row"><label for="wr_subject">제목</label></th>
        <td><?php echo htmlspecialchars2($wr['wr_subject']); ?></td>
    </tr>
	<? if( !empty($wr['wr_2']) ){ ?>
    <tr>
        <th scope="row"><label for="wr_subject">첨부파일</label></th>
        <td>
			<?=$wr['wr_1']?> &nbsp;<a href="./venderfiledown.php?wr_id=<?=$wr['wr_id']?>&tname=<?=$wr['wr_2']?>"><strong>[다운로드]</strong></a>
		</td>
    </tr>
	<? } ?>
    <tr>
        <th scope="row">내용</th>
        <td><?php echo  nl2br($wr['wr_content']); ?></td>
    </tr>
   
    </tbody>
    </table>
</div>

<div class="btn_fixed_top">
    <a href="./venderlist.php" class="btn btn_02">목록</a>
</div>

</form>

<?php
// [KVE-2018-2089] 취약점 으로 인해 파일 경로 수정시에만 자동등록방지 코드 사용


$comp_code = $member['comp_code'];

//전체 공지일때...
if( $wr['wr_4'] == "A" && $member['mb_type'] == "7" ){

	$sql = " select count(wr_id) as cnt from tbl_write_vender_choice where wr_id = '{$wr_id}' and wr_comp_code = '{$comp_code}' and wr_gubun ='A' ";
	$wr_row = sql_fetch($sql);

	//없으면 넣어주고 있으면 해당되는거 전부 update
	if( $wr_row["cnt"] == 0 ){
	
		$sql2 = " insert into tbl_write_vender_choice(wr_id , wr_comp_code , wr_confirm_yn , wr_del_yn , wr_confirm_time,wr_gubun)
				 values('{$wr_id}','{$comp_code}','Y','N',now(),'A') ";

		sql_query($sql2);

	}else{
	
		$sql2 = " update tbl_write_vender_choice set wr_confirm_time = now() , wr_confirm_yn = 'Y'
					where wr_id = '{$wr_id}' and wr_comp_code = '{$comp_code}' and wr_gubun ='A' ";
		sql_query($sql2);

	}

}

//선택 공지일때...
if( $wr['wr_4'] == "C" && $member['mb_type'] == "7" ){

	$sql2 = " update tbl_write_vender_choice set wr_confirm_time = now() , wr_confirm_yn = 'Y'
				where wr_id = '{$wr_id}' and wr_comp_code = '{$comp_code}' and wr_gubun ='C' ";
	sql_query($sql2);

}

//admin.tail.php 파일에 있는 ViewPopup 함수는 게시판 확인페이지에서는 다시 부르면 안된다.
$ViewPopupYN = "N";

?>
<script>


<?php
include_once (G5_ADMIN_PATH.'/admin.tail.php');
?>

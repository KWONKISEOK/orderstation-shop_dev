<?php
$sub_menu = '300840';
include_once('./_common.php');
include_once(G5_EDITOR_LIB);

auth_check($auth[$sub_menu], "w");



$html_title = "공급사정보공유";
$g5['title'] = $html_title.' 관리';

if ($w == "u")
{
    $html_title .= " 수정";
    $readonly = " readonly";

    $sql = " select * from tbl_write_news where wr_id = '$wr_id' ";
    $wr = sql_fetch($sql);
    if (!$wr['wr_id'])
        alert('등록된 자료가 없습니다.');
}
else
{
    $html_title .= ' 입력';
    $wr['wr_html'] = 2;
    $wr['wr_skin'] = 'basic';
    $wr['wr_mobile_skin'] = 'basic';
}

include_once (G5_ADMIN_PATH.'/admin.head.php');
?>

<form name="frmcontentform" action="./healthformupdate.php" onsubmit="return frmcontentform_check(this);" method="post" enctype="MULTIPART/FORM-DATA" >
<input type="hidden" name="w" value="<?php echo $w; ?>">
<input type="hidden" name="wr_id" value="<?php echo $wr['wr_id']; ?>">
<input type="hidden" name="wr_html" value="1">
<input type="hidden" name="token" value="">

<div class="tbl_frm01 tbl_wrap">
    <table>
    <caption><?php echo $g5['title']; ?> 목록</caption>
    <colgroup>
        <col class="grid_4">
        <col>
    </colgroup>
    <tbody>

    <tr>
        <th scope="row"><label for="wr_subject">제목</label></th>
        <td><input type="text" name="wr_subject" value="<?php echo htmlspecialchars2($wr['wr_subject']); ?>" id="wr_subject" required class="frm_input required" size="90"></td>
    </tr>
    <tr>
        <th scope="row">내용</th>
        <td><?php echo editor_html('wr_content', get_text($wr['wr_content'], 0)); ?></td>
    </tr>
   
    </tbody>
    </table>
</div>

<div class="btn_fixed_top">
    <a href="./healthlist.php" class="btn btn_02">목록</a>
    <input type="submit" value="확인" class="btn btn_submit" accesskey="s">
</div>

</form>

<?php
// [KVE-2018-2089] 취약점 으로 인해 파일 경로 수정시에만 자동등록방지 코드 사용
?>
<script>


function frmcontentform_check(f)
{
    errmsg = "";
    errfld = "";

    <?php echo get_editor_js('wr_content'); ?>
    <?php echo chk_editor_js('wr_content'); ?>
   
	check_field(f.wr_subject, "제목을 입력하세요.");
    check_field(f.wr_content, "내용을 입력하세요.");

    if (errmsg != "") {
        alert(errmsg);
        errfld.focus();
        return false;
    }
    
  
    return true;
}
</script>

<?php
include_once (G5_ADMIN_PATH.'/admin.tail.php');
?>

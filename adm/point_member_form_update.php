<?php
$sub_menu = "200200";
include_once("./_common.php");
include_once(G5_LIB_PATH."/register.lib.php");
include_once(G5_LIB_PATH.'/thumbnail.lib.php');

auth_check($auth[$sub_menu], 'w');

check_admin_token();

$mb_id = trim($_POST['mb_id']);
$mb_zip1 = substr($_POST['mb_zip'], 0, 3);
$mb_zip2 = substr($_POST['mb_zip'], 3);
$mb_type = $_POST['mb_type'];

$sql_common = "  mb_name = '{$_POST['mb_name']}',
                 mb_nick = '{$_POST['mb_nick']}',
                 mb_email = '{$_POST['mb_email']}',
                 mb_homepage = '{$_POST['mb_homepage']}',
                 mb_birth = '{$_POST['mb_birth']}',
				 mb_tel = '{$_POST['mb_tel']}',
                 mb_hp = '{$mb_hp}',
                 mb_certify = '{$mb_certify}',
                 mb_adult = '{$mb_adult}',
                 mb_zip1 = '$mb_zip1',
                 mb_zip2 = '$mb_zip2',
                 mb_addr1 = '{$_POST['mb_addr1']}',
                 mb_addr2 = '{$_POST['mb_addr2']}',
                 mb_addr3 = '{$_POST['mb_addr3']}',
                 mb_addr_jibeon = '{$_POST['mb_addr_jibeon']}',
                 mb_signature = '{$_POST['mb_signature']}',
                 mb_leave_date = '{$_POST['mb_leave_date']}',
                 mb_intercept_date='{$_POST['mb_intercept_date']}',
                 mb_memo = '{$_POST['mb_memo']}',
                 mb_mailling = '{$_POST['mb_mailling']}',
                 mb_sms = '{$_POST['mb_sms']}',
                 mb_open = '{$_POST['mb_open']}',
                 mb_profile = '{$_POST['mb_profile']}',
				 mb_type = '{$_POST['mb_type']}',
                 mb_level = '{$_POST['mb_level']}',
                 mb_recommend = '{$_POST['mb_recommend']}'";

if ($mb_type == '0') {
    $sql_common .= " ,mb_referee = '{$_POST['mb_referee']}'";
    $sql_common .= " ,pharm_status		= '{$_POST['pharm_status']}'";
    $sql_common .= " ,pharm_sido		= '{$_POST['pharm_sido']}'";
    $sql_common .= " ,pharm_gugun		= '{$_POST['pharm_gugun']}'";
    $sql_common .= " ,pharm_dong		= '{$_POST['pharm_dong']}'";

}

if ($w == '') {
    $mb = get_member($mb_id);
    if ($mb['mb_id'])
        alert('이미 존재하는 회원아이디입니다.\\nＩＤ : '.$mb['mb_id'].'\\n이름 : '.$mb['mb_name'].'\\n닉네임 : '.$mb['mb_nick'].'\\n메일 : '.$mb['mb_email']);

    sql_query(" insert into {$g5['member_table']} set mb_id = '{$mb_id}', mb_password = '".get_encrypt_string($mb_password)."', mb_datetime = '".G5_TIME_YMDHIS."', mb_ip = '{$_SERVER['REMOTE_ADDR']}', mb_email_certify = '".G5_TIME_YMDHIS."', {$sql_common} ");

    sql_query("insert into tbl_member_promotion set mb_id='{$mb_id}'");

    /************* 관리자 로그 처리 START *************/
    $al_data = sql_fetch("select * from tbl_member_promotion where mb_id = '{$mb_id}' ");
    insert_admin_log(200,200200, '프로모션 회원 등록', '', $mb_id, '', $_SERVER['REQUEST_URI'], $al_data, '');
}

goto_url('./point_list.php', false);
?>